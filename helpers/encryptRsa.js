import  JSEncrypt  from "../public/backupjsencrypt/tes/jsencrypt";

export const encryptRsa = (userId) =>{
    const encrypt = new JSEncrypt();
    const RSA_PUBLIC_KEY = `
    -----BEGIN PUBLIC KEY-----
    MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCipAV5BicvV9tjYWhOGlNEtxcp
    WtOF4qNksMrcbZ25tMcb5SK2DvtukzBZW7gmc0ZQX2w6tEsw8cIglt8a0sXQ2OSb
    JcvTd1ds5YScMuhmZ1V3BIQgTPM6Yjb4FAAfFl903oF+5K5g0M35Ccdb4X1ssrQQ
    F7bEcQoU90SdBTqzEQIDAQAB
    -----END PUBLIC KEY-----`
    encrypt.setPublicKey(RSA_PUBLIC_KEY)
    const encrypted = encrypt.encrypt(userId.toString());
    return encodeURIComponent(encrypted.replaceAll('\n',''));
  }