export const validateProfileData = (userProfile) => {
    const {districtId, mobilePhoneNumber, postalCode, provinceId, subDistrictId, address} = userProfile || {}
    
    return Boolean(districtId && mobilePhoneNumber && postalCode && provinceId && subDistrictId && address)
}