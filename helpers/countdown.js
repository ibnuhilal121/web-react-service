export const handleCountDown = (sec) => {
    let hours = Math.floor(sec / 3600);
    let divisorForMinutes = sec % 3600;
    let minutes = Math.floor(divisorForMinutes / 60);
    let divisorForSeconds = sec % 60;
    let seconds = Math.ceil(divisorForSeconds);

    return {
        hours,
        minutes,
        seconds
    }
}