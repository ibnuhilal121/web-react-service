// only number
export const numeric = /^[0-9]*$/

// internet message format (email)
export const RFC5322 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

// min 8 chars alphanumeric and min 1 char capital case (character allowed)
export const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d)(?=.*[.@$!%*?&_^#~,-]).{8,}$/


export const passwordNonSpecialChar = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\d).{8,}$/

// export const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/

// will match any phone number that starts with "08"
export const phoneNumberValidation = /^08\d{0,}$/

// will match any phone number that has between 9 and 14 digits
export const phoneLength = /^\d{10,14}$/