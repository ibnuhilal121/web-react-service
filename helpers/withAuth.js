import Cookies from "js-cookie";
import { useEffect, useState } from "react";


const withAuth = Component => {
  const Auth = (props) => {

    const [login, setLogin] = useState(false);

    useEffect(() => {
      const isLoggedIn = sessionStorage.getItem("keyMember") || Cookies.get("keyMember")
      setLogin(isLoggedIn)

      if (!isLoggedIn) {
        if (window.location.href.includes('/member/kredit/detail/?id=')) {
          localStorage.setItem('urlFromEmail', window.location.href)
          window.location.replace(`${window.location.origin}?stat=relog`);
        }else if(window.location.href.includes('/member/profesional')) {
          localStorage.setItem('urlFromEmail', window.location.href)
          window.location.replace(`${window.location.origin}?stat=relog`);
        }
        else {
          window.location.replace("/");
        }
      }
    }, [])

    if (!login) return (<></>);

    return (
      <Component {...props} />
    );
  };

  // Copy getInitial props so it will run as well
  if (Component.getInitialProps) {
    Auth.getInitialProps = Component.getInitialProps;
  }

  return Auth;
};

export default withAuth;