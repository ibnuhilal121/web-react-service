import CryptoJS from 'crypto-js'

export const generateKey = (userId) => {
    const encodedMemberID = CryptoJS.HmacSHA256(`${userId}`, `2SiUTlulI0ndS4GLz`).toString(CryptoJS.enc.Base64)
    const base64s =encodedMemberID.replaceAll("+",'-').replaceAll("/",'_').replace(/=/g,'').trim()

    return base64s
}