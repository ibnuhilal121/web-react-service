import { PDFDocument } from 'pdf-lib';
import { base64File } from "../public/resume/base64";
import rupiahFormater from './rupiahFormater';
import moment from 'moment';
const getMonth = (month) => {
    switch (month) {
        case 0:
            return "Januari";
        case 1:
            return "Februari";
        case 2:
            return "Maret";
        case 3:
            return "April";
        case 4:
            return "Mei";
        case 5:
            return "Juni";
        case 6:
            return "Juli";
        case 7:
            return "Agustus";
        case 8:
            return "September";
        case 9:
            return "Oktober";
        case 10:
            return "November";
        case 11:
            return "Desember";
        default:
            break;
    }
}
const singkatKata = (word, len) => {
    let a = word?.split(" ")
    let isSingkat = false
    let counter = 0
    let s = ""
    a?.forEach((word, i) => {
        counter = s.length + word.length + (a.length-(i+1)) + 2
        if(counter > len) {
            isSingkat = true
        }
        if(counter <= len && !isSingkat) {
            s += word + " "
        } else {
            s += word[0]+" "
        }
    })
    return s.slice(0,len)
}
const truncate = (str) => {
    const arr = str.split(" ")
    let cuttingWord = undefined;
    let maxLength = 0

    arr.every((word, i) => {
        // maxLength > 35 ? cuttingWord = i : null
        maxLength += word.length

        console.log({maxLength});
        if(maxLength > 35) {
            cuttingWord = i
            return false
        }
    })
    console.log('lalalal', {firstLine: arr.slice(0,maxLength+1).join(" "),
        secLine: arr.slice(maxLength + 1, arr.length).join(" ")})
    return {
        firstLine: arr.slice(0,maxLength+1).join(" "),
        secLine: arr.slice(maxLength + 1, arr.length).join(" ")
    }
}



export const createPDF = async (kprId, detailKPR, memberDetail, dokPersyaratan, ekycData) => {
    try {
        let idReq = kprId.substring(0, 5)
        const base64 = base64File

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var tgl = today.getDate();
        var bulan = today.getMonth();
        var time = (today.getHours() < 10 ? "0" + today.getHours() : today.getHours() ) + ":" + (today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes() )

        var date = yyyy+"-"+mm+"-"+dd+" " + time;
        today = dd + "/" + mm + "/" + yyyy;
        var dateTime = today + "%20" + time;
        console.log({today});

        const tgl_lahir = detailKPR.tgl_lhr?.split("T")[0]

        // Load a PDF with form fields
        const pdfDoc = await PDFDocument.load(base64)

        // Get the form containing all the fields
        const form = pdfDoc.getForm()

        // Get all fields in the PDF by their names
        form.getTextField('TANGGAL').setFontSize(8) 
		    form.getTextField('CABANG').setFontSize(8)
		    form.getTextField('ALAMAT_PERUSAHAAN').setFontSize(8)
		    form.getTextField('DATA_SI_ALAMAT_PERUSAHAAN').setFontSize(8)
		    form.getTextField('NAMA_LENGKAP').setFontSize(8)
		    form.getTextField('NO_KTP').setFontSize(8)
		    form.getTextField('TGL_LAHIR').setFontSize(8)
		    form.getTextField('BLN_LAHIR').setFontSize(8)
		    form.getTextField('THN_LAHIR').setFontSize(8)
		    form.getTextField('TEMPAT_LAHIR').setFontSize(8)
		    form.getTextField('KELURAHAN').setFontSize(8)
		    form.getTextField('KECAMATAN').setFontSize(8)
		    form.getTextField('RT').setFontSize(8)
		    form.getTextField('RW').setFontSize(8)
		    form.getTextField('KOTA').setFontSize(8)
		    form.getTextField('KODEPOS').setFontSize(8)
		    form.getTextField('KODEPOS_PERUSAHAAN').setFontSize(8)
		    form.getTextField('NO_TELP').setFontSize(8)
		    form.getTextField('EMAIL').setFontSize(8)
		    form.getTextField('NO_HP1').setFontSize(8)
		    form.getTextField('NO_HP2').setFontSize(8)
		    form.getTextField('KOTA_PERUSAHAAN').setFontSize(8)
		    form.getTextField('NAMA_PERUSAHAAN').setFontSize(8)
		    form.getTextField('BIDANG_USAHA').setFontSize(8)
		    form.getTextField('ALAMAT_RUMAH').setFontSize(8)
		    form.getTextField('HARGA_JUAL').setFontSize(8)
		    form.getTextField('UANG_MUKA').setFontSize(8)
		    form.getTextField('JUMLAH_KREDIT_DIMOHON').setFontSize(8)
		    form.getTextField('JUMLAH_ANGSURAN').setFontSize(8)
		    form.getTextField('JANGKA_WAKTU_KREDIT').setFontSize(8)
		    form.getTextField('PENGHASILAN_BULANAN').setFontSize(8)
		    form.getTextField('DATA_SI_NAMA_LENGKAP').setFontSize(8)
		    form.getTextField('DATA_SI_NO_KTP').setFontSize(8)
		    form.getTextField('DATA_SI_TEMPAT_LAHIR').setFontSize(8)
		    form.getTextField('ALAMAT_AGUNGAN').setFontSize(8)
		    form.getTextField('AGUNGAN_PENJUAL').setFontSize(8)
		    form.getTextField('AGUNGAN_BLOK').setFontSize(8)
		    form.getTextField('AGUNGAN_KELURAHAN').setFontSize(8)
		    form.getTextField('AGUNGAN_KECAMATAN').setFontSize(8)
		    form.getTextField('AGUNGAN_KODEPOS').setFontSize(8)
		    form.getTextField('AGUNGAN_KOTA').setFontSize(8)
		    form.getTextField('AGUNGAN_LUAS_BANGUNAN').setFontSize(8)
		    form.getTextField('ALAMAT_AGUNGAN_2').setFontSize(8)
		    form.getTextField('TTD_PASANGAN_PEMOHON').setFontSize(8)
		    form.getTextField('TTD_PASANGAN_PEMOHON').setAlignment(1)
		    form.getTextField('TTD_PEMOHON_ZCR2').setFontSize(8)
		    form.getTextField('TTD_PEMOHON_ZCR2').setAlignment(1)
		    form.getTextField('NAMA_TEMPAT').setFontSize(5)
		    form.getTextField('NAMA_TEMPAT').setAlignment(1)
		    form.getTextField('TGL_BLN').setFontSize(8)
		    form.getTextField('THN').setFontSize(8)
			form.getTextField('NAMA_IBU').setFontSize(8)
        
        // Fill in the basic info fields
        form.getTextField('TANGGAL').setText(today)
        form.getTextField('CABANG').setText(detailKPR.n_cbg ? detailKPR.n_cbg : "")
        form.getTextField('ALAMAT_PERUSAHAAN').setText(detailKPR.ALAMAT_PEKERJAAN ? detailKPR.ALAMAT_PEKERJAAN : "")
        form.getTextField('DATA_SI_ALAMAT_PERUSAHAAN').setText(detailKPR.ALAMAT_PEKERJAAN_PASANGAN ? detailKPR.ALAMAT_PEKERJAAN_PASANGAN : "")
        form.getTextField('NAMA_LENGKAP').setText(detailKPR.nm_lgkp ? detailKPR.nm_lgkp : "")
        form.getTextField('NAMA_IBU').setText(ekycData?.biologicalMotherMaidenName ? ekycData?.biologicalMotherMaidenName : "")
        form.getTextField('NO_KTP').setText(detailKPR.no_ktp ? " "+detailKPR.no_ktp?.split("").join("   ") : "")
        form.getTextField('ALAMAT_RUMAH').setText(detailKPR.almt ? detailKPR.almt : "")
        form.getTextField('TGL_LAHIR').setText(detailKPR.tgl_lhr ? tgl_lahir?.split("-")[2]?.split("").join("    ") : "")
        form.getTextField('BLN_LAHIR').setText(detailKPR.tgl_lhr ? tgl_lahir?.split("-")[1]?.split("").join("    ") : "")
        form.getTextField('THN_LAHIR').setText(detailKPR.tgl_lhr ? tgl_lahir?.split("-")[0]?.split("").join("   ") : "")
        form.getTextField('TEMPAT_LAHIR').setText(detailKPR.tpt_lhr ? detailKPR.tpt_lhr : "")
        form.getTextField('RT').setText(detailKPR.rt ? detailKPR.rt?.toString()?.split("").join("   ") : "")
        form.getTextField('RW').setText(detailKPR.rt ? detailKPR.rw?.toString()?.split("").join("   ") : "")
        form.getTextField('KELURAHAN').setText(detailKPR.n_kel ? detailKPR.n_kel : "")
        form.getTextField('KECAMATAN').setText(detailKPR.n_kec ? detailKPR.n_kec : "")
        form.getTextField('KOTA').setText(detailKPR.n_kot ? detailKPR.n_kot : "")
        form.getTextField('KOTA_PERUSAHAAN').setText(detailKPR.n_kotPekerjaan ? detailKPR.n_kotPekerjaan : "")
        form.getTextField('NAMA_PERUSAHAAN').setText(detailKPR.nm_prshn ? detailKPR.nm_prshn : "")
        form.getTextField('KODEPOS_PERUSAHAAN').setText(detailKPR.KODEPOS_PEKERJAAN ? " "+detailKPR.KODEPOS_PEKERJAAN?.toString()?.split("").join("   ") : "")
        form.getTextField('KODEPOS').setText(detailKPR.pos ? " "+detailKPR.pos?.toString()?.split("").join("   ") : "")
        form.getTextField('EMAIL').setText(detailKPR.eml ? detailKPR.eml : "")
        form.getTextField('NO_TELP').setText(detailKPR?.no_tlp ?" "+ detailKPR?.no_tlp?.split("").map((a,i)=> i == 3 ? a+ "    ": a).join("   ") : "")
        form.getTextField('NO_HP1').setText(detailKPR.no_hp1 ? " "+detailKPR.no_hp1?.split("").join("   ") : "")
        form.getTextField('NO_HP2').setText(detailKPR.no_hp2 ? " "+detailKPR.no_hp2?.split("").join("   ") : "")
        form.getTextField('BIDANG_USAHA').setText(detailKPR.bidangUsaha ? detailKPR.bidangUsaha : "")
        form.getTextField('HARGA_JUAL').setText(detailKPR?.hargaProperti ? rupiahFormater(`${detailKPR?.hargaProperti}`) : "")
        form.getTextField('UANG_MUKA').setText(detailKPR.uangMuka ? rupiahFormater(`${detailKPR.uangMuka}`) : "")
        form.getTextField('JUMLAH_KREDIT_DIMOHON').setText(detailKPR.nl_pgjn ? rupiahFormater(`${detailKPR.nl_pgjn}`) : "")
        form.getTextField('JUMLAH_ANGSURAN').setText(rupiahFormater(Math.round(eval(detailKPR.nl_pgjn+"/"+detailKPR.jangkaWaktu))))
        form.getTextField('JANGKA_WAKTU_KREDIT').setText(detailKPR?.jangkaWaktu?.split("").join("   "))
        form.getTextField('PENGHASILAN_BULANAN').setText(detailKPR.pghsln ? `${detailKPR.pghsln}` : "")
        form.getTextField('DATA_SI_NAMA_LENGKAP').setText(detailKPR.nm_lgkp_psgn ? detailKPR.nm_lgkp_psgn : "")
        form.getTextField('DATA_SI_NO_KTP').setText(detailKPR.no_id_psgn ? " "+detailKPR.no_id_psgn?.split("").join("    ") : "")
        form.getTextField('DATA_SI_TEMPAT_LAHIR').setText(detailKPR.tpt_lhr_psgn ? detailKPR.tpt_lhr_psgn : "")
        detailKPR.almt_prpt && form.getTextField('ALAMAT_AGUNGAN').setText(detailKPR.almt_prpt ? truncate(detailKPR.almt_prpt)?.firstLine : "")
        detailKPR.almt_prpt && form.getTextField('ALAMAT_AGUNGAN_2').setText(detailKPR.almt_prpt ? truncate(detailKPR.almt_prpt)?.secLine : "")
        form.getTextField('AGUNGAN_PENJUAL').setText(detailKPR.n_dev ? detailKPR.n_dev : "")
        form.getTextField('AGUNGAN_BLOK').setText(detailKPR.kavling?.blk ? detailKPR.kavling.blk : "")
        form.getTextField('AGUNGAN_KELURAHAN').setText(detailKPR.kavling?.n_kel ? detailKPR.kavling.n_kel : "")
        form.getTextField('AGUNGAN_KECAMATAN').setText(detailKPR.kavling?.n_kec ? detailKPR.kavling.n_kec : "")
        form.getTextField('AGUNGAN_KODEPOS').setText(detailKPR.kavling?.pos ? detailKPR.kavling.pos?.toString()?.split("").join("   ") : "")
        form.getTextField('AGUNGAN_KOTA').setText(detailKPR.kavling?.n_kot ? detailKPR.kavling.n_kot : "")
        form.getTextField('AGUNGAN_LUAS_BANGUNAN').setText(detailKPR.kavling?.ls_bgn ? detailKPR.kavling.ls_bgn + " m²" : "")
        form.getTextField('TTD_PEMOHON_ZCR2').setText(singkatKata(detailKPR?.nm_lgkp.toUpperCase(), 26))
        console.log(detailKPR.almt_prpt);
        // dalam listing
        if(idReq == "KPRST") {
            form.getTextField('HARGA_JUAL').setText(detailKPR?.hrg_jl ? rupiahFormater(`${detailKPR?.hrg_jl}`) : "")
            form.getTextField('UANG_MUKA').setText(detailKPR.uang_mka ? rupiahFormater(`${detailKPR.uang_mka}`) : "")
            form.getTextField('JUMLAH_ANGSURAN').setText(rupiahFormater(Math.round(eval(detailKPR.nl_pgjn+"/"+detailKPR.jng_wkt))))
            form.getTextField('JANGKA_WAKTU_KREDIT').setText(detailKPR?.jng_wkt?.toString()?.split("").join("   "))
            
        }

        // Jenis kelamin
        const jenisKelamin = {
            0: "PEREMPUAN",
            1: "LAKI",

        }
        memberDetail?.jk && form.getCheckBox("GENDER_"+ jenisKelamin[memberDetail?.jk]).check()
        

        // Jenis Pekerjaan
        const jenisPekerjaan = {
            10: "BUMN",
            12: "BUMN",
            20: "WIRASWASTA",
            25: "SWASTA",
            30: "PNS",
            50: "PMDN",
            40: "PROFESSIONAL"
        }
        memberDetail?.jns_pkrjn && form.getCheckBox(`PROFESI_${jenisPekerjaan[memberDetail?.jns_pkrjn]}`).check()
        // Status Menikah
        const statusMenikah = {
            0: "MENIKAH",
            10: "MENIKAH",
            20: "LAJANG",
            30: "JANDA",
            50: "JANDA",
            60: "LAJANG",
            1: "LAJANG",
            2: "MENIKAH",
            3: "JANDA",
            5: "JANDA",
            6: "LAJANG"
        }
        detailKPR?.st_mnkh && form.getCheckBox(`STATUS_${statusMenikah[detailKPR?.st_mnkh]}`).check()


        // Religion
        ekycData?.religion && form.getCheckBox(`AGAMA_${ekycData?.religion}`).check();
        // enableReadOnly


        // keperluan kredit

        const dlmListing = {
            1: "ada",
            3: "ada",
            4: "ada",
            6: "ada",
            8: "ada"

        }
        const luarListing = {
            1: "ada",
            2: "ada",
            3: "ada"

        }
        if(detailKPR?.typ_pgjn == 1) {
            dlmListing[detailKPR?.sft_krdt] && form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').check() 
        } else {
            luarListing[detailKPR?.sft_krdt] && form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').check()
            form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').enableReadOnly
            form.getCheckBox('KEPERLUAN_KREDIT_KONSUMTIF').enableReadOnly
        }



        // Sistem Pembayaran
        const sistemPembayaran = {
            10: "AGF",
            20: "KOLEKTIF",
            30: "PAYROLL",
        }
        detailKPR?.pembayaranAngsuran && sistemPembayaran[detailKPR?.pembayaranAngsuran] && form.getCheckBox(`PEMBAYARAN_${sistemPembayaran[detailKPR?.pembayaranAngsuran]}`).check()
        detailKPR?.sft_krdt && sistemPembayaran[detailKPR?.sft_krdt] && form.getCheckBox(`PEMBAYARAN_${sistemPembayaran[detailKPR?.sft_krdt]}`).check()
        // Sumber dana utama ()
        const sumberDanaUtama = {
            10: "GAJI",
            12: "GAJI",
            25: "GAJI",
            30: "GAJI",
            20: "GAJI",
            50: "GAJI",
            40: "GAJI",
            // 20: "HASIL_USAHA",
            // 50: "INVESTASI",
            // 40: "LAINNYA",
        }
        memberDetail?.jns_pkrjn && form.getCheckBox(`SOF_${sumberDanaUtama[memberDetail?.jns_pkrjn]}`).check();

        form.getTextField('SOF_LAINNYA_FIELD').setFontSize(8);
        // memberDetail?.jns_pkrjn == 40 && form.getTextField(`SOF_${sumberDanaUtama[memberDetail?.jns_pkrjn]}_FIELD`).setText(jenisPekerjaan[memberDetail?.jns_pkrjn]);


        // Penghasilan Per bulan
        memberDetail?.pghsln && form.getTextField('PENGHASILAN_BULANAN').setText(rupiahFormater(memberDetail?.pghsln));

        // Penghasilan Per Tahun
        form.getTextField('PENGHASILAN_TAHUNAN').setFontSize(8);
        memberDetail?.pghsln && form.getTextField('PENGHASILAN_TAHUNAN').setText(rupiahFormater(eval(`${memberDetail?.pghsln}*12`)));

        // Data Agunan Dalam Listing

        // checklist dokumen
        // 1. slip penghasilan
        // dokPersyaratan?.dok_penghasilan ? form.getCheckBox("SLIP_ADA").check() : form.getCheckBox("SLIP_TDK").check()

        // // 2. KTP
        // dokPersyaratan?.ktp_pemohon ? form.getCheckBox("KTP_ADA").check() : form.getCheckBox("KTP_TDK").check()

        // // 3. Surat Nikah
        // dokPersyaratan?.surat_nikah ? form.getCheckBox("NIKAH_ADA").check() : form.getCheckBox("CERAI_TDK").check()

        // // 4. SK
        // dokPersyaratan?.sk ? form.getCheckBox("SK_ADA").check() : form.getCheckBox("SK_TDK").check()

        // // 5. Rekening
        // dokPersyaratan?.rekening ? form.getCheckBox("REKENING_ADA").check() : form.getCheckBox("REKENING_TDK").check()

        // // 6. NPWP
        // dokPersyaratan?.npwp ? form.getCheckBox("NPWP_ADA").check() : form.getCheckBox("NPWP_TDK").check()

        // // 7. Akta Pendirian
        // dokPersyaratan?.akta_pendirian ? form.getCheckBox("PENDIRIAN_ADA").check() : form.getCheckBox("PENDIRIAN_TDK").check()

        // // 8. Perizinan
        // dokPersyaratan?.izin ? form.getCheckBox("IZIN_ADA").check() : form.getCheckBox("PERIZINAN_TDK").check()

        // // 9. Laporan Keuangan
        // dokPersyaratan?.laporan_keuangan ? form.getCheckBox("KEUANGAN_ADA").check() : form.getCheckBox("KEUANGAN_TDK").check()

        // // 10. Surat Pernyataan lain-lain
        // dokPersyaratan?.lain_lain ? form.getCheckBox("PERNYATAAN_ADA").check() : form.getCheckBox("PERNYATAAN_TDK").check()

        // // 11. Surat Pernyataan belum memiliki rumah
        // dokPersyaratan?.blm_memiliki_rumah ? form.getCheckBox("PENRNYATAAN_BLMRUMAH_ADA").check() : form.getCheckBox("PERNYATAAN_BLMRUMAH_TDK").check()

        // // 12. Surat pernyataan permohonan kpr bersubsidi
        // dokPersyaratan?.kpr_subsidi ? form.getCheckBox("KPR_ADA").check() : form.getCheckBox("KPR_TDK").check()

        // // 13. Dokumen Jaminan
        // dokPersyaratan?.jaminan ? form.getCheckBox("JAMINAN_ADA").check() : form.getCheckBox("JAMINAN_TDK").check()

        // // 14. KK
        // dokPersyaratan?.kartu_keluarga ? form.getCheckBox("KK_ADA").check() : form.getCheckBox("KK_TDK").check()

        // // nama & ttd
        // dokPersyaratan?.nm_lgkp && form.getTextField('TTD_PEMOHON').setText(detailKPR?.nm_lgkp.toUpperCase())
        // dokPersyaratan?.nm_lgkp_psgn && form.getTextField('TTD_PASANGAN_PEMOHON').setText(detailKPR?.nm_lgkp_psgn)

        const keys = Object.keys(detailKPR)
            
            if (keys.includes("fl_ktp")) {
                form.getCheckBox("KTP_ADA").check()
            } else {
                form.getCheckBox("KTP_TDK").check()
            }

            if (keys.includes("fl_buku_nikah")) {
                form.getCheckBox("NIKAH_ADA").check()
            } else {
                form.getCheckBox("CERAI_TDK").check()
            }

            if (keys.includes("fl_kk")) {
                form.getCheckBox("KK_ADA").check()
            } else {
                form.getCheckBox("KK_TDK").check()
            }

            if (keys.includes("fl_rek_koran")) {
                form.getCheckBox("REKENING_ADA").check()
            } else {
                form.getCheckBox("REKENING_TDK").check()
            }

            if (keys.includes("fl_slp_pghsln")) {
                form.getCheckBox("SLIP_ADA").check()
            } else {
                form.getCheckBox("SLIP_TDK").check()
            }
            
            if (keys.includes("fl_sk")) {
                form.getCheckBox("SK_ADA").check()
            } else {
                form.getCheckBox("SK_TDK").check()
            }

            if (keys.includes("fl_npwp")) {
                form.getCheckBox("NPWP_ADA").check()
            } else {
                form.getCheckBox("NPWP_TDK").check()
            }

            if (keys.includes("fl_pendirian")) {
                form.getCheckBox("PENDIRIAN_ADA").check()
            } else {
                form.getCheckBox("PENDIRIAN_TDK").check()
            }

            if (keys.includes("fl_izin")) {
                form.getCheckBox("IZIN_ADA").check()
            } else {
                form.getCheckBox("PERIZINAN_TDK").check()
            }

            if (keys.includes("fl_keuangan")) {
                form.getCheckBox("KEUANGAN_ADA").check()
            } else {
                form.getCheckBox("KEUANGAN_TDK").check()
            }

            if (keys.includes("fl_pernyataan")) {
                form.getCheckBox("PERNYATAAN_ADA").check()
            } else {
                form.getCheckBox("PERNYATAAN_TDK").check()
            }

            if (keys.includes("fl_pernyataan_blmrumah")) {
                form.getCheckBox("PERNYATAAN_BLMRUMAH_ADA").check()
            } else {
                form.getCheckBox("PERNYATAAN_BLMRUMAH_TDK").check()
            }

            if (keys.includes("fl_kpr")) {
                form.getCheckBox("KPR_ADA").check()
            } else {
                form.getCheckBox("KPR_TDK").check()
            }

            if (keys.includes("fl_jaminan")) {
                form.getCheckBox("JAMINAN_ADA").check()
            } else {
                form.getCheckBox("JAMINAN_TDK").check()
            }

        // tempat, cabang, tanggal
        const dateMomen = moment(new Date())
        form.getTextField('NAMA_TEMPAT').setText(detailKPR?.n_cbg ? detailKPR?.n_cbg?.toUpperCase() : "KOSONG")
        form.getTextField('TGL_BLN').setText(`${tgl} ${getMonth(bulan)}`)
        form.getTextField('THN').setText(dateMomen.format("YY"))

        // enable read only TEXTFIELD
        const formFields = form.getFields();
        formFields.forEach(field => {

            field.enableReadOnly()

        })

        // get dataUrl from html element
        // const dataUrl = document.getElementById("QR").toDataURL("image/jpeg")

        // const pages = pdfDoc.getPages();
        // const firstPage = pages[3];
        // const jpgImage = await pdfDoc.embedJpg(dataUrl);

        // Get the width and height of the first page
        // const { width, height } = firstPage.getSize();
        // put the Image
        // firstPage.drawImage( jpgImage , {
        //     x: 460,
        //     y: height / 2 - 250,
        //     width: 70,
        //     height: 70,
        //     opacity: 0.75,
        // })

            
        

          
        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();
        return pdfBytes

            // Trigger the browser to download the PDF document
        // download(pdfBytes, "resume_kpr.pdf", "application/pdf");
        // setIsLoading(false)
    } catch(err){
        console.log("error generate pdf : ", err)
        
    }
}