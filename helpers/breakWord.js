export const breakWord = (text, max) => {
  if (text?.length > max) return `${text.substring(0, max)}...`;
  return text;
};