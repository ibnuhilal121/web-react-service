export const types = [
    {
        slug: "rumah",
        label: "Rumah",
        ct: "Cluster",
        id: "1",
    },
    {
        slug: "apartemen",
        label: "Apartemen",
        ct: "Tower",
        id: "2",
    },
    {
        slug: "rumah_bekas",
        label: "Rumah Bekas",
        ct: "Cluster",
        id: "3",
    },
    {
        slug: "lelang",
        label: "Lelang",
        ct: "Cluster",
        id: "Lelang",
    },
    {
        slug: "apartemen_bekas",
        label: "Apartemen Bekas",
        ct: "Tower",
        id: "5",
    },
    {
        slug: "ruko",
        label: "Ruko",
        id: "6",
    },
    {
        slug: "kantor",
        label: "Kantor",
        id: "7",
    },
];

export function useSlugToId() {
    return (slug) => types.find((val) => val.slug === slug)?.id || "";
}

export function useIdToSlug() {
    return (id) => types.find((val) => val.id == id)?.slug || "";
}

export function useIdToLabel() {
    
    return (id) => {
        if(isNaN(id)) {
            return id
        } else {
            return types.find((val) => val.id == id)?.label || "Lain-lain";
        }
        // types.find((val) => val.id == id)?.label || "Lain-lain";
    }
}

export function useIdToCT() {
    return (id) => types.find((val) => val.id == id)?.ct || "";
}
