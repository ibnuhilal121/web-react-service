import Cookies from "js-cookie";
import { getVerifyStatus } from "../services/auth";
import { useEffect, useState } from "react";
import { useAppContext } from "../context";


const verifiedMember = Component => {
  const Auth = (props) => {

    const [login, setLogin] = useState(false);
    const {userProfile} = useAppContext();
    

    useEffect(() => {
      if(userProfile?.isVerified){
        setLogin(userProfile?.isVerified)
      } 
      if(userProfile?.isVerified != true) {window.location.replace("/member/verified");}
}, [])
    return (
      <Component {...props} />
    );
  };

  // Copy getInitial props so it will run as well
  if (Component.getInitialProps) {
    Auth.getInitialProps = Component.getInitialProps;
  }

  return Auth;
};

export default verifiedMember;