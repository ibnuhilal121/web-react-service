import { 
    GoogleAuthProvider, 
    signInWithPopup, 
    TwitterAuthProvider,
    FacebookAuthProvider,
    fetchSignInMethodsForEmail,
    signInWithEmailAndPassword,
    signInWithRedirect
} from "firebase/auth";
import { auth } from "../utils/FirebaseSetup";
import { defaultHeaders } from "../utils/defaultHeaders";
import Cookies from "js-cookie";
import { getGenerateLinkPreApproval, getGenerateHomeServiceToken, getGenerateProfesionaListingToken, getGenerateHomeServiceLink, getGenerateProfesionaListingLink} from "../services/konten";


const googleProvider = new GoogleAuthProvider();
const twitterProvider = new TwitterAuthProvider();
const facebookProvider = new FacebookAuthProvider();



const getProviderForProviderId = (id) => {
    switch (id) {
        case googleProvider.providerId:
            return googleProvider;
        case twitterProvider.providerId:
            return twitterProvider;    
        default:
            break;
    }
}

export const googleLogin = async (setUserKey, setUserProfile,customFunction) => {

    const doLogin = async(payload) => {
        try {
          const memberLogin = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login/google`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                AccessKey_App: localStorage.getItem("accessKey"),
                'Access-Control-Allow-Origin': '*',
                ...defaultHeaders
            },
            body: new URLSearchParams(payload),
          });
          const resMemberLogin = await memberLogin.json();
          return Promise.resolve(resMemberLogin);
        } catch (error) {
            console.log('err adhin',error)
          return Promise.reject(error);
        };
      };
    // Step 1.
    // User tries to sign in to Google.
    signInWithPopup(auth, googleProvider).then((res) => {
        const payload = {
            GoogleID: res.user.uid,
            Name: res.user.displayName,
            Email: res.user.email,
            Image: res.user.photoURL,
          };
          
          doLogin(payload)
           
            .then(async (data) => {
                console.log('log adhin')
              const tempProfile = {
                n: data.Profile[0].n,
                gmbr: data.Profile[0].gmbr,
                id: data.Profile[0].id,
                agen: data.Profile[0].is_agn,
                idAgen: data.Profile[0].i_agn,
                namaAgensi: data.Profile[0].n_agn,
                email:data.Profile[0].e
              };

              //body untuk dapat generate url
            const preApproval = {
                NAMA: data.Profile[0].n,
                EMAIL: data.Profile[0].e
              }
              //body untuk dapat token home services
              const HomeServicesToken = {
                grant_type: "client_credentials",
                client_id: "BTNProperti",
                client_secret: "8d14a409384eed23adbe474d46fcd04a"
              }
  
              const ProfesionaListingToken = {
                grant_type: "client_credentials",
                client_id: "BTNProperti",
                client_secret: "8d14a409384eed23adbe474d46fcd04a"
              }
  
              const HomeServicesLink = {
                "platform_id": "53239e80-54d5-11ed-89b5-506b8d8a08aa",
                "webview": "HOME_SERVICE",
                "user": {
                  "name": data.Profile[0].n,
                  "msisdn": data.Profile[0].no_hp,
                  "email": data.Profile[0].e
                }
              }
  
              const ProfesionaListingLink = {
                "platform_id": "53239e80-54d5-11ed-89b5-506b8d8a08aa",
                "webview": "PROFESIONALISTING",
                "user": {
                  "name": data.Profile[0].n,
                  "msisdn": data.Profile[0].no_hp,
                  "email": data.Profile[0].e
                }
              }
              // console.log("ini data Pre approal", preApproval);
              // console.log("ini data Home Service token", HomeServicesToken);
              // console.log("ini data Professional token ", ProfesionaListingToken);
              // console.log("ini data HomeServicesLink: ", HomeServicesLink);
              // console.log("ini data Professional Listring, ", ProfesionaListingLink);
              sessionStorage.setItem("user", JSON.stringify(tempProfile));
              console.log("tes 1.2");
              sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
              console.log("tes 2");
              Cookies.set("user", JSON.stringify(tempProfile));
              console.log("tes 3");
              Cookies.set("keyMember", JSON.stringify(data.AccessKey));
              console.log("tes 4");
              // setUserProfile(data.Profile[0]);
              console.log("tes 5");
              let approval = await getGenerateLinkPreApproval(preApproval);
              console.log("ini response approval : ", approval);
              let home_token = await getGenerateHomeServiceToken(HomeServicesToken);
              console.log("ini response home token : ", home_token);
              let profesiona_listing_token = await getGenerateProfesionaListingToken(ProfesionaListingToken);
              console.log("ini profesional listing token: ", profesiona_listing_token);
              localStorage.setItem("generate-token",home_token.data.access_token);
              localStorage.setItem("generate-token-profesiona-listing",profesiona_listing_token.data.access_token);
              let home_service_link = await getGenerateHomeServiceLink(JSON.stringify(HomeServicesLink));
              let profesiona_listing_link = await getGenerateProfesionaListingLink(JSON.stringify(ProfesionaListingLink));
  
  
              //console menampilkan generate url
              console.log(approval.url)
  
              //console menampilkan token home services
              console.log(home_token.data.access_token)
  
               //console menampilkan token home services
               console.log(home_service_link)
               console.log(home_service_link.data.link)
  
              localStorage.setItem("generate-url",approval.url);
              localStorage.setItem("url-home-services",home_service_link.data.link);
              localStorage.setItem("url-profesiona-listing",profesiona_listing_link.data.link);
              // console.log(homeServiceLink)
              console.log(getGenerateHomeServiceToken)
              // console.log(getGenerateLinkPreApproval(preApproval))
              console.log("sini 7")
              const navigate = localStorage.getItem("navigate");
              const title = localStorage.getItem("title");
              setUserKey(data.AccessKey);
              customFunction()
            })
            .catch((error) => {
              console.log("error : ", error);
            }).finally(()=>{
                console.log('not redirect');
                const navigate = localStorage.getItem("navigate");
                const title = localStorage.getItem("title");

              //   if(!window.location.href.includes('/property/tipe/detail/?id')){
              //     console.log('redirect')
              //     window.location.replace("/member/profil");
              // }
               
                // if(!window.location.href.includes('/property/tipe/detail/?id')){
                //     console.log('redirect')
                    if(navigate){
                      if (title == "Virtual Expo") {
                        window.location.assign(navigate + localStorage.getItem("generate-url"))
                        localStorage.removeItem("navigate")
                        console.log(navigate + approval.url);
                      } else if(title == "Pre Approval"){
                        window.location.assign(navigate + "/link/" + approval.url)
                        localStorage.removeItem("navigate")
                        console.log(navigate + approval.url);
                      } else if(title == "Home Service"){
                        window.location.assign(localStorage.getItem("url-home-services"))
                        localStorage.removeItem("navigate")
                        console.log(home_service_link.data.link);
                      } else if(title == "Jasa Profesional"){
                        window.location.assign(localStorage.getItem("url-profesiona-listing"))
                        localStorage.removeItem("navigate")
                        console.log(profesiona_listing_link.data.link);
                      } else {
                        window.location.assign(navigate)
                      }
                    } else {
                      window.location.replace("/member/profil");
                    }; 
                // }
            })
        
           
               
        })
    .catch(function(error) {
        // An error happened.
        if (error.code === 'auth/account-exists-with-different-credential') {
        // Step 2.
        // User's email already exists.
        // The pending Google credential.
        var pendingCred = GoogleAuthProvider.credentialFromError(error);
        // The provider account's email address.
        var email = error.customData.email;
        // Get sign-in methods for this email.
        fetchSignInMethodsForEmail(auth, email).then(function(methods) {
            // Step 3.
            // If the user has several sign-in methods,
            // the first method in the list will be the "recommended" method to use.
            if (methods[0] === 'password') {
            // Asks the user their password.
            // In real scenario, you should handle this asynchronously.
            var password = promptUserForPassword(); // TODO: implement promptUserForPassword.
            signInWithEmailAndPassword(auth, email, password).then(function(result) {
                // Step 4a.
                return result.user.linkWithCredential(pendingCred);
            }).then(function() {
                // Google account successfully linked to the existing Firebase user.
                goToApp();
            });
            return;
            }
            // All the other cases are external providers.
            // Construct provider object for that provider.
            // TODO: implement getProviderForProviderId.
            var provider = getProviderForProviderId(methods[0]);
            // At this point, you should let the user know that they already have an account
            // but with a different provider, and let them validate the fact they want to
            // sign in with this provider.
            // Sign in to provider. Note: browsers usually block popup triggered asynchronously,
            // so in real scenario you should ask the user to click on a "continue" button
            // that will trigger the signInWithPopup.
            signInWithRedirect(auth, provider).then(function(result) {
            // Remember that the user may have signed in with an account that has a different email
            // address than the first one. This can happen as Firebase doesn't control the provider's
            // sign in flow and the user is free to login using whichever account they own.
            // Step 4b.
            // Link to Google credential.
            // As we have access to the pending credential, we can directly call the link method.
            result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred) {
                // Google account successfully linked to the existing Firebase user.
                goToApp();
            });
            });
        });
        }
    })
    // signInWithPopup(auth, googleProvider)
    // .then(res => {
    //     const credential = GoogleAuthProvider.credentialFromResult(res);
    //     const token = credential?.accessToken;

    //     const user = res.user;
    //     console.log({
    //         credential,
    //         token,
    //         user
    //     })
    // })
    // .catch(err => {
    //     const errorCode = err.code;
    //     const errMessage = err.message;
    //     const email = err.email;
    //     const credential = GoogleAuthProvider.credentialFromError(err);
    //     console.log({
    //         errorCode,
    //         errMessage,
    //         email,
    //         credential
    //     });
    // })
    // .finally(() => {
    //     window.location.reload();
    // })
};

export const twitterLogin = () => {
    // Step 1.
    // User tries to sign in to Google.
    signInWithPopup(auth, twitterProvider)
    .then(res => window.location.reload())
    .catch(function(error) {
        // An error happened.
        if (error.code === 'auth/account-exists-with-different-credential') {
        // Step 2.
        // User's email already exists.
        // The pending Google credential.
        var pendingCred = TwitterAuthProvider.credentialFromError(error);
        // The provider account's email address.
        var email = error.customData.email;
        // console.log('~ error credential : ', TwitterAuthProvider.credentialFromError(error))
        // Get sign-in methods for this email.
        fetchSignInMethodsForEmail(auth, email).then(function(methods) {
            // Step 3.
            // If the user has several sign-in methods,
            // the first method in the list will be the "recommended" method to use.
            if (methods[0] === 'password') {
            // Asks the user their password.
            // In real scenario, you should handle this asynchronously.
            var password = promptUserForPassword(); // TODO: implement promptUserForPassword.
            signInWithEmailAndPassword(auth, email, password).then(function(result) {
                // Step 4a.
                return result.user.linkWithCredential(pendingCred);
            }).then(function() {
                // Google account successfully linked to the existing Firebase user.
                goToApp();
            });
            return;
            }
            // All the other cases are external providers.
            // Construct provider object for that provider.
            // TODO: implement getProviderForProviderId.
            var provider = getProviderForProviderId(methods[0]);
            // At this point, you should let the user know that they already have an account
            // but with a different provider, and let them validate the fact they want to
            // sign in with this provider.
            // Sign in to provider. Note: browsers usually block popup triggered asynchronously,
            // so in real scenario you should ask the user to click on a "continue" button
            // that will trigger the signInWithPopup.
            signInWithRedirect(auth, provider).then(function(result) {
            // Remember that the user may have signed in with an account that has a different email
            // address than the first one. This can happen as Firebase doesn't control the provider's
            // sign in flow and the user is free to login using whichever account they own.
            // Step 4b.
            // Link to Google credential.
            // As we have access to the pending credential, we can directly call the link method.
            result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred) {
                // Google account successfully linked to the existing Firebase user.
                goToApp();
            });
            });
        });
        }
    })
    // signInWithPopup(auth, twitterProvider)
    // .then(res => {
    //     console.log('~ result : ', res)
    //     const credential = TwitterAuthProvider.credentialFromResult(res);
    //     const token = credential?.accessToken;
    //     const email = res._tokenResponse.email

    //     const user = res.user;
    //     console.log({
    //         credential,
    //         token,
    //         user,
    //         email
    //     })
    // })
    // .catch(err => {
    //     const errorCode = err.code;
    //     const errMessage = err.message;
    //     const email = err.email;
    //     const credential = TwitterAuthProvider.credentialFromError(err);
    //     console.log({
    //         errorCode,
    //         errMessage,
    //         email,
    //         credential
    //     });
    // })
};

export const facebookLogin = () => {
    // Step 1.
    // User tries to sign in to Google.
    signInWithPopup(auth, facebookProvider)
    .then(res => {
        const credential = FacebookAuthProvider.credentialFromResult(res)
        console.log('~ result from facebook : ', credential)
        console.log('~ access token facebook : ', credential.accessToken)
        sessionStorage.setItem('token_facebook', JSON.stringify(credential))

        window.location.reload()
    })
    .catch(function(error) {
        // An error happened.
        if (error.code === 'auth/account-exists-with-different-credential') {
        // Step 2.
        // User's email already exists.
        // The pending Google credential.
        var pendingCred = FacebookAuthProvider.credentialFromError(error);
        // The provider account's email address.
        var email = error.customData.email;
        // Get sign-in methods for this email.
        fetchSignInMethodsForEmail(auth, email).then(function(methods) {
            // Step 3.
            // If the user has several sign-in methods,
            // the first method in the list will be the "recommended" method to use.
            if (methods[0] === 'password') {
            // Asks the user their password.
            // In real scenario, you should handle this asynchronously.
            var password = promptUserForPassword(); // TODO: implement promptUserForPassword.
            signInWithEmailAndPassword(auth, email, password).then(function(result) {
                // Step 4a.
                return result.user.linkWithCredential(pendingCred);
            }).then(function() {
                // Google account successfully linked to the existing Firebase user.
                goToApp();
            });
            return;
            }
            // All the other cases are external providers.
            // Construct provider object for that provider.
            // TODO: implement getProviderForProviderId.
            var provider = getProviderForProviderId(methods[0]);
            // At this point, you should let the user know that they already have an account
            // but with a different provider, and let them validate the fact they want to
            // sign in with this provider.
            // Sign in to provider. Note: browsers usually block popup triggered asynchronously,
            // so in real scenario you should ask the user to click on a "continue" button
            // that will trigger the signInWithPopup.
            signInWithRedirect(auth, provider).then(function(result) {
            // Remember that the user may have signed in with an account that has a different email
            // address than the first one. This can happen as Firebase doesn't control the provider's
            // sign in flow and the user is free to login using whichever account they own.
            // Step 4b.
            // Link to Google credential.
            // As we have access to the pending credential, we can directly call the link method.
            result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred) {
                // Google account successfully linked to the existing Firebase user.
                goToApp();
            });
            });
        });
        }
    })
    // signInWithPopup(auth, facebookProvider)
    // .then(res => {
    //     console.log('~ result : ', res)
    //     const credential = TwitterAuthProvider.credentialFromResult(res);
    //     const token = credential?.accessToken;
    //     const email = res._tokenResponse.email

    //     const user = res.user;
    //     console.log({
    //         credential,
    //         token,
    //         user,
    //         email
    //     })
    // })
    // .catch(err => {
    //     const errorCode = err.code;
    //     const errMessage = err.message;
    //     const email = err.email;
    //     const credential = FacebookAuthProvider.credentialFromError(err);
    //     console.log({
    //         errorCode,
    //         errMessage,
    //         email,
    //         credential
    //     });
    // })
};

export const logout = () => {
    auth.signOut();
    console.log("log out sukses");
};