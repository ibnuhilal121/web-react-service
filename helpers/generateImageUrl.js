export const generateImageUrl = (url) => {
  if (!url) return "/images/thumb-placeholder.png";

  let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  fullUrl = fullUrl.replaceAll(" ", "%20");

  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/thumb-placeholder.png";
  }
};