<?php
$engine = new StringTemplate\Engine;
$head_data = array();

$this->load->view('layout/header', $head_data);



$configKPR = $this->Mdumum->getsystemconfig();

// Get ID Tipe Rumah Populer dari config
$TmpListProperti = "<div style=\"background:  url('{image}') no-repeat center center/cover; border-radius: 10px; height: 270px;\" class=\"item-product\"><div class=\"card revamp-card revamp-card-layer\">{ribbon}<div class=\"card-body\"><div class=\"developer-info revamp-developer-info\"> <a href=\"{url_developer}\" class=\"card-link revamp-link-developer\">{developer}</a> </div> <div class=\"card-title\"> <a href=\"{url}\" class=\"link-main revamp-proper-title\">{perumahan}</a> </div><h2 class=\"card-subtitle green\">&nbsp;</h2>  <div class=\"krp-info row revamp-fullrow\"> <div class=\"col-md-12 col-sm-12 col-12\"> <div class=\"kpr-label revamp-reset-marginbottom\"><div class=\"card-subtitle revamp-right-cardinfo revamp-reset-marginbottom\">{lokasi}</div></div><div class=\"card-subtitle revamp-reset-marginbottom green\"><h1 class=\"price-product revamp-right-cardinfo revamp-price-cardinfo\">Rp {harga}</h1></div></div></div></div></div></div>";
$TmpPromo = "<a href='{url_promo}' target='_blank' class='badge-promo-link'><div class=\"badge-promo\"><span>PROMO</span></div></a>";
$HTMLTipeRumahPopuler = array();
$HTMLTipeRumahTerbaru = array();
$HTMLTipeRumahLelang = array();
$HTMLTipeRumahSubsidi = array();
$TmpListPropertiBackUp = "<div style=\"background:  url('/assets/img/sampleproject.jpg') no-repeat center center/cover;\" class=\"item-product\"> <div class=\"card\"> <div class=\"card-image\">{ribbon}<a href=\"#\" class=\"btn-favorite  {favorit}\"  data-favorited='{favorited}' data-properti='{id}' data-style='zoom-in'><i class=\"fa fa-heart-o\"></i></a> <a href=\"#\"><a href=\"{url}\"><img class=\"card-img-top img-item-properti\" src=\"{image}\" alt=\"\"></a></a> <a href=\"#\" class=\"badge badge-secondary\">{tipe_rumah}</a> {promo}</div><div class=\"card-body\"> <div class=\"card-title\"> <a href=\"{url}\" class=\"link-main\">{perumahan}</a> </div><div class=\"card-subtitle\">{lokasi}</div><h2 class=\"card-subtitle green\">{sisa_kavling} Kavling Tersedia</h2> <h1 class=\"price-product\">Rp {harga}</h1> <div class=\"krp-info row\"> <div class=\"col-md-6 col-sm-6 col-6\"> <div class=\"kpr-label\">KPR mulai dari :</div><div class=\"card-subtitle green\">{kpr_cicilan}/Bulan</div></div><div class=\"col-md-6 col-sm-6 col-6\"> <div class=\"kpr-label\">Suku Bunga :</div><div class=\"card-subtitle green\">{bunga}%</div></div></div><div class=\"developer-info\"> <a href=\"{url_developer}\" class=\"card-link\">{developer}</a> </div></div><div class=\"card-footer\"> <ul class=\"list-inline\"> <li class=\"list-inline-item\"> <label>LT:</label> <span>{luas_tanah}m<sup>2</sup></span> </li><li class=\"list-inline-item\"> <label>LB:</label> <span>{luas_bangunan}m<sup>2</sup></span> </li><li class=\"list-inline-item\"> <i class=\"assets icon-kamar-tidur\"></i> <strong>{km_tidur}</strong> </li><li class=\"list-inline-item\"> <i class=\"assets icon-kamar-mandi\"></i> <strong>{km_mandi}</strong> </li></ul> </div></div></div>";
$TmpListPropertiSubsidi = "<div style=\"background:  url('{image}') no-repeat center center/cover; border-radius: 10px; height: 270px;\" class=\"item-product\"><div class=\"revamp-subsidi-ribbon\"><p class=\"revamp-subsiditext-ribbon\">Subsidi</p></div><div class=\"card revamp-card revamp-card-layer\"><div class=\"card-body\"><div class=\"developer-info revamp-developer-info\"> <a href=\"{url_developer}\" class=\"card-link revamp-link-developer\">{developer}</a> </div> <div class=\"card-title\"> <a href=\"{url}\" class=\"link-main revamp-proper-title\">{perumahan}</a> </div><h2 class=\"card-subtitle green\">&nbsp;</h2>  <div class=\"krp-info row revamp-fullrow\"> <div class=\"col-md-12 col-sm-12 col-12\"> <div class=\"kpr-label revamp-reset-marginbottom\"><div class=\"card-subtitle revamp-right-cardinfo revamp-reset-marginbottom\">{lokasi}</div></div><div class=\"card-subtitle revamp-reset-marginbottom green\"><h1 class=\"price-product revamp-right-cardinfo revamp-price-cardinfo\">Rp {harga}</h1></div></div></div></div></div></div>";


// Banner Backgroud

$Banner = $this->Mdbanner->getdata(["hlmn" => "index"]);
//echo json_encode($Banner);
$BackgroundMain = "background:transparent url('/assets/img/bg-home-low.jpg?=v1') no-repeat scroll center center/cover";
if ($Banner && !$Banner["IsError"]) {
    $Banner = array_filter($Banner["Data"], function ($val) {
        return ($val["lks"] == "mainbg");
    });
    $LsBanner = array();
    foreach ($Banner as $item) {
        $LsBanner[] = $item;
    }
    $Banner = $LsBanner[0];
    if (!empty($Banner['isi'])) {
        $BackgroundMain = "background:transparent url('" . $Banner["isi"] . "') no-repeat scroll center center/cover";
    }
    // $BackgroundMain="background:transparent url('".$Banner["isi"]."') no-repeat scroll center center/cover";
}


// Get Tipe Rumah Populer
$TipeRumahPopuler = $this->Mdumum->getpropertipopuler("SemuaPopuler");
//var_dump($TipeRumahPopuler);
//die;

if ($TipeRumahPopuler && !$TipeRumahPopuler["IsError"]) {
    foreach ($TipeRumahPopuler["Data"] as $item) {

        if (!isset($_SESSION["MemberFavorit"])) {
            $_SESSION["MemberFavorit"] = array();
        }
        $IsFavorited = (in_array($item["id"], $_SESSION["MemberFavorit"], TRUE) ? true : false);
        $Promo = $engine->render($TmpPromo, ["url_promo" => $item["url_promo"]]);
        $HTMLTipeRumahPopuler[] = $engine->render($TmpListProperti, array(
            "url" => $this->Publicfunction->CreateURL($item["n_perum"] . "-tipe-" . $item["n"], $item["id"], "/tipe-rumah/"),
            "image" => $this->Publicfunction->ParseImageArray($item["lst_gmbr"], 1),
            "ribbon" => $this->Publicfunction->CreateRibbonProperti($item["st_ssd"], $item["jns"]),
            "favorit" => ($IsFavorited ? "selected" : ""),
            "favorited" => ($IsFavorited ? "true" : "false"),
            "id" => $item["id"],
            "tipe_rumah" => $item["n"],
            "perumahan" => ($item["n_perum"] ? $item["n_perum"] : "&nbsp;"),
            "lokasi" => $this->Publicfunction->CreateAlamat(null, $item["n_prop"], $item["n_kot"]),
            "sisa_kavling" => $item["jm_kvlng"],
            "harga" => $this->Publicfunction->FormatAngka($item["hrg"]),
            "kpr_cicilan" => $this->Publicfunction->FormatAngkaKMB($item["pmt_konvensional"]),
            "bunga" => $item["sk_bga"],
            "developer" => $item["n_dev"] . $this->Publicfunction->DeveloperCrown($item["st_premium_dev"]),
            "url_developer" => $this->Publicfunction->CreateURL($item["n_dev"], $item["i_dev"], "/developer/"),
            "luas_tanah" => $item["ls_tnh"],
            "luas_bangunan" => $item["ls_bgn"],
            "km_tidur" => $item["km_tdr"],
            "km_mandi" => $item["km_mnd"],
            "promo" => ($item["st_promo"] == 1 ? $Promo : "")
        ));
    }
}

// Get Tipe Rumah Terbaru
$TipeRumahPopuler = $this->Mdumum->getpropertipopuler("PropertiTerbaru");

if ($TipeRumahPopuler && !$TipeRumahPopuler["IsError"]) {
    foreach ($TipeRumahPopuler["Data"] as $item) {

        if (!isset($_SESSION["MemberFavorit"])) {
            $_SESSION["MemberFavorit"] = array();
        }
        $IsFavorited = (in_array($item["id"], $_SESSION["MemberFavorit"], TRUE) ? true : false);

        $Promo = $engine->render($TmpPromo, ["url_promo" => $item["url_promo"]]);
        $HTMLTipeRumahTerbaru[] = $engine->render($TmpListProperti, array(
            "url" => $this->Publicfunction->CreateURL($item["n_perum"] . "-tipe-" . $item["n"], $item["id"], "/tipe-rumah/"),
            "image" => $this->Publicfunction->ParseImageArray($item["lst_gmbr"], 1),
            "ribbon" => $this->Publicfunction->CreateRibbonProperti($item["st_ssd"], $item["jns"]),
            "favorit" => ($IsFavorited ? "selected" : ""),
            "favorited" => ($IsFavorited ? "true" : "false"),
            "id" => $item["id"],
            "tipe_rumah" => $item["n"],
            "perumahan" => ($item["n_perum"] ? $item["n_perum"] : "&nbsp;"),
            "lokasi" => $this->Publicfunction->CreateAlamat(null, $item["n_prop"], $item["n_kot"]),
            "sisa_kavling" => $item["jm_kvlng"],
            "harga" => $this->Publicfunction->FormatAngka($item["hrg"]),
            "kpr_cicilan" => $this->Publicfunction->FormatAngkaKMB($item["pmt_konvensional"]),
            "bunga" => $item["sk_bga"],
            "developer" => $item["n_dev"] . $this->Publicfunction->DeveloperCrown($item["st_premium_dev"]),
            "url_developer" => $this->Publicfunction->CreateURL($item["n_dev"], $item["i_dev"], "/developer/"),
            "luas_tanah" => $item["ls_tnh"],
            "luas_bangunan" => $item["ls_bgn"],
            "km_tidur" => $item["km_tdr"],
            "km_mandi" => $item["km_mnd"],
            "promo" => ($item["st_promo"] == 1 ? $Promo : "")
        ));
    }
}


// Get Tipe Subsidi
$TipeRumahPopuler = $this->Mdumum->getpropertipopuler("PropertiSubsidi");
// die;

if ($TipeRumahPopuler && !$TipeRumahPopuler["IsError"]) {
    foreach ($TipeRumahPopuler["Data"] as $item) {

        if (!isset($_SESSION["MemberFavorit"])) {
            $_SESSION["MemberFavorit"] = array();
        }
        $IsFavorited = (in_array($item["id"], $_SESSION["MemberFavorit"], TRUE) ? true : false);

        $Promo = $engine->render($TmpPromo, ["url_promo" => $item["url_promo"]]);
        $HTMLTipeRumahSubsidi[] = $engine->render($TmpListProperti, array(
            "url" => $this->Publicfunction->CreateURL($item["n_perum"] . "-tipe-" . $item["n"], $item["id"], "/tipe-rumah/"),
            "image" => $this->Publicfunction->ParseImageArray($item["lst_gmbr"], 1),
            "ribbon" => $this->Publicfunction->CreateRibbonProperti($item["st_ssd"], $item["jns"]),
            "favorit" => ($IsFavorited ? "selected" : ""),
            "favorited" => ($IsFavorited ? "true" : "false"),
            "id" => $item["id"],
            "tipe_rumah" => $item["n"],
            "perumahan" => ($item["n_perum"] ? $item["n_perum"] : "&nbsp;"),
            "lokasi" => $this->Publicfunction->CreateAlamat(null, $item["n_prop"], $item["n_kot"]),
            "sisa_kavling" => $item["jm_kvlng"],
            "harga" => $this->Publicfunction->FormatAngka($item["hrg"]),
            "kpr_cicilan" => $this->Publicfunction->FormatAngkaKMB($item["pmt_konvensional"]),
            "bunga" => $item["sk_bga"],
            "developer" => $item["n_dev"] . $this->Publicfunction->DeveloperCrown($item["st_premium_dev"]),
            "url_developer" => $this->Publicfunction->CreateURL($item["n_dev"], $item["i_dev"], "/developer/"),
            "luas_tanah" => $item["ls_tnh"],
            "luas_bangunan" => $item["ls_bgn"],
            "km_tidur" => $item["km_tdr"],
            "km_mandi" => $item["km_mnd"],
            "promo" => ($item["st_promo"] == 1 ? $Promo : "")
        ));
    }
}


// Get Hunian Terpilih
$HunianTerpilih = $this->Mdumum->gethunianpilihan();
$TmpListHunianTerpilih = '<div class="item-selection"> <div class="card revamp-card-height" style="background: url(\'{image}\') no-repeat center center/cover; border-radius: 10px; padding: 2px;"><img class="revamp-badge4d" src="/assets/img/badge4d.svg" /> <div class="card-image revamp-card-image"> <a href="{url}"><img class="card-img-top revamp-card-pilihan" src="{image}" alt=""></a> </div><div class="card-body revamp-card-body"> <a class="revamp-card-body-a" href="{url}"><h5 class="card-title revamp-card-title">{judul}</h5> <p class="card-subtitle revamp-pilihan-content">{desc}</p></a><br /><a href="{url}" class="revamp-card-link card-link">Lihat Properti</a> </div></div></div>';
$HTMLHunianTerpilih = array();

if ($HunianTerpilih && !$HunianTerpilih["IsError"]) {
    foreach ($HunianTerpilih["Data"] as $item) {
        $HTMLHunianTerpilih[] = $engine->render($TmpListHunianTerpilih, array(
            "url" => $item["lnk"],
            "image" => $this->Publicfunction->ParseImage($item["gmbr"]),
            "judul" => $item["jdl"],
            "desc" => $item["dsk"],
        ));
    }
}


// Get Berita Populer
$TmpListBerita = '<div class="item-news"> <div class="card revamp-cards-border-radius"> <div class="card-image revamp-cards-border-radius-img"> <a class="revamp-cards-border-radius-img" href="{url}"><img class="card-img-top img-item-blog revamp-cards-border-radius-img" src="{image}" alt=""></a> </div><div class="card-body"> <h1 class="card-title"><a href="{url}" class="link-main">{judul}</a></h1> <p>{desc}</p><a href="{url}" class="card-link">Baca Lebih Lanjut</a> </div></div></div>';
$HTMLBeritaPopuler = array();
$HTMLBeritaInfoTerbaru = array();
$HTMLBeritaInfoPameran = array();
$HTMLBeritaBTNHousing = array();

$BeritaPopuler = $this->Mdblog->berita_populer();
if ($BeritaPopuler && !$BeritaPopuler["IsError"]) {
    foreach ($BeritaPopuler["Data"] as $item) {
        $HTMLBeritaPopuler[] = $engine->render($TmpListBerita, array(
            "url" => $this->Publicfunction->CreateURL($item["seo"], $item["id"], "/blog/"),
            "image" => $this->Publicfunction->ParseImage($item["gmbr"]),
            "judul" => $item["jdl"],
            "desc" => $this->Publicfunction->StrLimitWord($item["rgksn"], 20)
        ));
    }
}


$BeritaPopuler = $this->Mdblog->berita_populer(null, 5);
if ($BeritaPopuler && !$BeritaPopuler["IsError"]) {
    foreach ($BeritaPopuler["Data"] as $item) {
        $HTMLBeritaInfoTerbaru[] = $engine->render($TmpListBerita, array(
            "url" => $this->Publicfunction->CreateURL($item["seo"], $item["id"], "/blog/"),
            "image" => $this->Publicfunction->ParseImage($item["gmbr"], "blog"),
            "judul" => $item["jdl"],
            "desc" => $this->Publicfunction->StrLimitWord($item["rgksn"], 20)
        ));
    }
}


$BeritaPopuler = $this->Mdblog->berita_populer(null, 7);
if ($BeritaPopuler && !$BeritaPopuler["IsError"]) {
    foreach ($BeritaPopuler["Data"] as $item) {
        $HTMLBeritaInfoPameran[] = $engine->render($TmpListBerita, array(
            "url" => $this->Publicfunction->CreateURL($item["seo"], $item["id"], "/blog/"),
            "image" => $this->Publicfunction->ParseImage($item["gmbr"], "blog"),
            "judul" => $item["jdl"],
            "desc" => $this->Publicfunction->StrLimitWord($item["rgksn"], 20)
        ));
    }
}

$BeritaPopuler = $this->Mdblog->berita_populer(null, 8);
if ($BeritaPopuler && !$BeritaPopuler["IsError"]) {
    foreach ($BeritaPopuler["Data"] as $item) {
        $HTMLBeritaBTNHousing[] = $engine->render($TmpListBerita, array(
            "url" => $this->Publicfunction->CreateURL($item["seo"], $item["id"], "/blog/"),
            "image" => $this->Publicfunction->ParseImage($item["gmbr"], "blog"),
            "judul" => $item["jdl"],
            "desc" => $this->Publicfunction->StrLimitWord($item["rgksn"], 20)
        ));
    }
}

//    echo json_encode($HTMLTipeRumahPopuler);


$TempDeveloper = "<div class=\"col-lg-2 col-3 {class} mt-3\"><a class=\"revamp-link-devchoice\" href=\"{url}\"><div class=\"revamp-devchoice\"><div class=\"revamp-img-devchoice\" style=\"background: url('{gambar}') no-repeat center center/contain;\">&nbsp;</div></div></a></div>";
$HTMLDeveloper = array();

$Developer = $this->Mdumum->getdeveloperpopuler();
if ($Developer && !$Developer["IsError"]) {
    $i = 1;
    foreach ($Developer["Data"] as $item) {
        $HTMLDeveloper[] = $engine->render($TempDeveloper, array(
            "url" => $this->Publicfunction->CreateURL($item["n"], $item["id"], "/developer/"),
            "gambar" => $this->Publicfunction->ParseImage($item["lgo"]),
            "class" => ($i < 4 ? "" : "d-sm-none d-lg-block"),
        ));
        $i++;
    }
}

// testimonial
$testimonial = $this->Mdumum->gettestimonial();
$TempTestimoial = '<div class="item"><div class="shadow-effect"><img class="rounded-circle" src="{gambar}" alt=""><p>{content}</p></div><div class="testimonial-name">{name}</div></div>';
if ($testimonial && !$testimonial["IsError"]) {
    $testimonial = $testimonial["Data"];
    //    shuffle($testimonial);
    foreach ($testimonial as $key => $item) {
        $HTMLTestimonial[] = $engine->render($TempTestimoial, array(
            "gambar" => $this->Publicfunction->ParseImage($item["g_memb"], "member"),
            "content" => $item["tstmoni"],
            "name" => ($item["n_memb"]),
        ));
    }
}
?>


<content>
    <div class="content-main revamp-contentmain" style="<?= $BackgroundMain ?>">
        <div class="container">
            <div class="paralax revamp-paralax">
                <div class="motto">
                    <h1 class="tagline-home">Temukan Rumah & KPR Idaman</h1>
                    <h2 class="tagline-home">Karena Hidup Gak Cuma Tentang Hari Ini</h2>
                </div>

                <div class="main-form revamp-main-form">
                    <form action="/properti.html" class="frm-validate">
                        <div class="row">
                            <div class="col-md-10 revamp-radius-search revamp-searchform-padding">
                                <div class="field-form row no-gutters revamp-radius-search">
                                    <div class="col-md-12 form-group input-wrap revamp-radius-search">
                                        <input type="text" class="revamp-radius-search searchform-revamp form-control typeahead" name="search[keyword]" autocomplete="off" placeholder="Cari berdasarkan keyword, kota, developer, atau proyek perumahan.">
                                        <input type="hidden" name="search[use-keyword]" value="true">
                                        <input type="hidden" name="search[kota]">
                                        <input type="hidden" name="search[kota-nama]">
                                        <input type="hidden" name="search[provinsi]">
                                        <input type="hidden" name="search[provinsi-nama]">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 revamp-searchbutton-padding">
                                <button type="submit" class="btn btn-block btn-main revamp-radius-searchbutton"><img class="revamp-imgbtn" src="/assets/img/search-btn.png" alt="" /></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="content-feature revamp-content-feature">
        <div class="container">
            <div class="feature-card revamp-feature-card">
                <p class="revamp-featurecard-tagline">Bagaimana Bank BTN bisa membantu Anda</p>
                <div class="row">
                    <div class="col-md-4">
                        <a href="/ajukan-kpr.html" class="link-main">
                            <div class="card revamp-card" data-onhover="true" data-onhoverclass="colored">
                                <div class="card-body revamp-card-body">
                                    <span class="revamp-assets-cardkpr icon"></span>
                                    <h4 class="card-title">Ajukan KPR</h4>
                                    <p class="card-text">Bank BTN dapat membantu anda memiliki hunian dengan kemudahan
                                        pengajuan kredit secara online.</p>
                                    <h6 class="card-subtitle mb-2 revamp-more-cardbtn">Ajukan Sekarang</h6>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="/blog/kategori/btn-housing-index-11.html" class="link-main">
                            <div class="card revamp-card" data-onhover="true" data-onhoverclass="colored">
                                <div class="card-body revamp-card-body">
                                    <span class="revamp-assets-cardhfc icon"></span>
                                    <h4 class="card-title">Housing Finance Center</h4>
                                    <p class="card-text">Bank BTN berkomitmen menjadi pusat pembelajaran dan riset
                                        perumahan melalui Housing Finance Center</p>
                                    <h6 class="card-subtitle mb-2 revamp-more-cardbtn">Pelajari Lebih Lanjut</h6>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">

                        <a href="/developer-ajukan.html" class="link-main">
                            <div class="card revamp-card" data-onhover="true" data-onhoverclass="colored">
                                <div class="card-body revamp-card-body">
                                    <span class="revamp-assets-carddev icon"></span>
                                    <h4 class="card-title">Developer BTN</h4>
                                    <p class="card-text">Bank BTN bekerjasama dengan banyak Developer dalam membantu
                                        penyediaan perumahan bagi masyarakat</p>
                                    <h6 class="card-subtitle mb-2 revamp-more-cardbtn">Daftar Developer</h6>
                                </div>
                            </div>
                        </a>


                    </div>
                </div>
            </div>
        </div>

        <div class="content-list top">
            <div class="container">
                <section class="product-area revamp-section">
                    <p class="revamp-featurecard-tagline">Jelajahi BTN Properti dan Temukan Hunian Impianmu</p>
                    <p class="revamp-featurecard-tagline-text">BTN Properti menjadi platform pertama milik Bank yang
                        bekerjasama dengan developer di seluruh Indonesia yang membantu masyarakat Indonesia dalam
                        kepemilikan rumah melalui Bank BTN. Temukan hunian impianmu bersama properti-properti pilihan
                        Bank BTN.</p>

                    <div class="tab-content revamp-tab-content" id="tab-product-area">
                        <div class="tab-pane fade show active" id="tab-properti-popular" role="tabpanel">
                            <a href="/properti/populer.html?lihat[Sort]=jm_vw&lihat[jns]=" class="link-main">
                                <div class="list-product row">
                                    <div class=" col-md-3">
                                        <div class="sub-product-area">
                                            <p class="title-sub-product">Properti Terpopuler BTN Properti</p>
                                            <p class="desc-sub-product">Temukan properti-properti populer BTN Properti
                                                pilihan pengunjung.</p>
                                            <a href="/properti/populer.html?lihat[Sort]=jm_vw&lihat[jns]=">
                                                <p class="more-sub-product">Lihat Selengkapnya</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="owl-carousel owl-carousel-slide green-nav ">
                                            <?php
                                            echo join("", $HTMLTipeRumahPopuler);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="tab-content revamp-tab-content" id="tab-product-area">
                        <div class="tab-pane fade show active" id="tab-properti-popular" role="tabpanel">
                            <div class="list-product row">

                                <!-- <a href="/properti/populer.html?lihat[Sort]=t_ins&lihat[jns]=" class=""> -->
                                <div class="col-md-9">
                                    <div class="owl-carousel owl-carousel-slide green-nav ">
                                        <?php
                                        echo join("", $HTMLTipeRumahTerbaru);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="sub-product-area">
                                        <p class="title-sub-product">Properti Terbaru BTN Properti</p>
                                        <p class="desc-sub-product">Temukan properti terbaru BTN Properti dari
                                            developer-developer pilihan Bank BTN</p>
                                        <a href="/properti/populer.html?lihat[Sort]=t_ins&lihat[jns]=">
                                            <p class="more-sub-product">Lihat Selengkapnya</p>
                                        </a>
                                    </div>
                                </div>
                                <!-- </a> -->
                            </div>
                        </div>
                    </div>

                    <div class="tab-content revamp-tab-content" id="tab-product-area">
                        <div class="tab-pane fade show active" id="tab-properti-popular" role="tabpanel">
                            <div class="list-product row">
                                <div class="col-md-3">
                                    <div class="sub-product-area">
                                        <p class="title-sub-product">Properti Subsidi BTN Properti</p>
                                        <p class="desc-sub-product">Temukan properti-properti bersubsidi dukungan
                                            pemerintah untuk MBR.</p>
                                        <a href="/properti/populer.html?lihat[Sort]=jm_vw&lihat[jns]=2" class>
                                            <p class="more-sub-product">Lihat Selengkapnya</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="owl-carousel owl-carousel-slide green-nav ">
                                        <?php
                                        echo join("", $HTMLTipeRumahSubsidi);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
					
					<div class="tab-content revamp-tab-content" id="tab-product-area">
                        <div class="tab-pane fade show active" id="tab-properti-popular" role="tabpanel">
                            <div class="list-product row">

                                <!-- <a href="/properti/populer.html?lihat[Sort]=t_ins&lihat[jns]=" class=""> -->
                                <div class="col-md-9">
                                    <div class="owl-carousel owl-carousel-slide green-nav ">
                                        <?php
                                        echo join("", $HTMLHunianTerpilih);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="sub-product-area">
                                        <p class="title-sub-product">4D Pilihan Bank BTN</p>
                                        <p class="desc-sub-product">Temukan properti pilihan Bank BTN lengkap dengan fitur 4D</p>
                                        <img class="revamp-4dicon" width="100" src="/assets/img/4d.svg" />
                                    </div>
                                </div>
                                <!-- </a> -->
                            </div>
                        </div>
                    </div>

                    <p class="revamp-featuresearch-tagline">Atau cari properti lainnya</p>

                    <div class="main-form revamp-main-form">
                        <form action="/properti.html" class="frm-validate">
                            <div class="row">
                                <div class="col-md-10 revamp-radius-search revamp-searchform-padding">
                                    <div class="field-form row no-gutters revamp-radius-search">
                                        <div class="col-md-12 form-group input-wrap revamp-radius-search">
                                            <input style="font-size: 14px !important;" type="text" class="revamp-radius-search searchform-revamp form-control typeahead" name="search[keyword]" autocomplete="off" placeholder="Cari berdasarkan keyword, kota, developer, atau proyek perumahan.">
                                            <input type="hidden" name="search[use-keyword]" value="true">
                                            <input type="hidden" name="search[kota]">
                                            <input type="hidden" name="search[kota-nama]">
                                            <input type="hidden" name="search[provinsi]">
                                            <input type="hidden" name="search[provinsi-nama]">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 revamp-searchbutton-padding">
                                    <button type="submit" class="btn btn-block btn-main revamp-radius-searchbutton"><img class="revamp-imgbtn" src="/assets/img/search-btn.png" alt="" /></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </section>
            </div>
        </div>


        <div class="content-list revamp-content-list d-xl-none d-sm-block d-block">
            <div class="container">
                <div class="calculation-card">
                    <div class="row">

                        <div class="col-md-6 right-content text-center">
                            <div class="wrapper-image revamp-wrapper-image-sim">
                                <div class="content-0">
                                    <span class='assets-calc-image image'></span>
                                    <h3 class="revamp-title-simulasi">Simulasi BTN Properti</h3>
                                    <p class="text-muted revamp-text-simulasi">Bank BTN memahami kebutuhan konsumen
                                        dalam kepemilikan hunian. Hitung penghasilan Anda dan dapatkan hunian yang
                                        sesuai dengan pendapatan bulanan berdasarkan jangka waktu yang Anda inginkan.
                                        Anda juga bisa melakukan perhitungan angsuran dari jumlah kredit yang Anda
                                        inginkan.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 left-content">
                            <div class="media">
                                <img class="d-flex mr-3 revamp-calculate-img" src="/assets/img/calculate.png" alt="">
                                <div class="media-body">
                                    <h2 class="mt-4 title revamp-calculate-title"><a target="_blank" style="color: #3B4144 !important;" href="/hitung-harga-properti-maksimal.html">Hitung Harga Properti
                                            Maksimal</a></h2>
                                    Hitung berapa jumah harga maksimal properti yang ideal untuk Anda
                                    <p class="more-calculate"><a target="_blank" href="/hitung-harga-properti-maksimal.html">Selengkapnya &#8594;</a></p>
                                </div>
                            </div>
                            <div class="media">
                                <img class="d-flex mr-3 revamp-calculate-img" src="/assets/img/simulate.png" alt="">
                                <div class="media-body">
                                    <h2 class="mt-4 title revamp-calculate-title"><a target="_blank" style="color: #3B4144 !important;" href="/simulasi-kpr.html">Simulasi
                                            KPR</a></h2>
                                    Hitung berapa jumlah angsuran dan pembayaran pertama untuk rencana KPR Anda.
                                    <p class="more-calculate"><a target="_blank" href="/simulasi-kpr.html">Selengkapnya
                                            &#8594;</a></p>
                                </div>
                            </div>
                            <div class="media">
                                <img class="d-flex mr-3 revamp-calculate-img" src="/assets/img/marketprice.png" alt="">
                                <div class="media-body">
                                    <h2 class="mt-4 title revamp-calculate-title"><a target="_blank" style="color: #3B4144 !important;" href="/harga-pasar.html">Harga Pasar</a>
                                    </h2>
                                    Hitung berapa jumah harga maksimal properti yang ideal untuk Anda
                                    <p class="more-calculate"><a target="_blank" href="/harga-pasar.html">Selengkapnya
                                            &#8594;</a></p>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="content-list revamp-content-list d-xl-block d-sm-none d-none">
            <div class="container">
                <section class="perhitungan-kpr">
                    <div class="kpr-calculation">
                        <div class="tab-content revamp-background-calc" id="tab-kpr-calculation">
                            <div class="tab-pane fade show active" id="tab-kpr-calculation-1" role="tabpanel">
                                <div class="row">

                                    <div class="col-md-6 right-content text-center">
                                        <div class="wrapper-image revamp-wrapper-image-sim">
                                            <div class="content-0">
                                                <span class='assets-calc-image image'></span>
                                                <h3 class="revamp-title-simulasi">Simulasi BTN Properti</h3>
                                                <p class="text-muted revamp-text-simulasi">Bank BTN memahami kebutuhan
                                                    konsumen dalam kepemilikan hunian. Hitung penghasilan Anda dan
                                                    dapatkan hunian yang sesuai dengan pendapatan bulanan berdasarkan
                                                    jangka waktu yang Anda inginkan. Anda juga bisa melakukan
                                                    perhitungan angsuran dari jumlah kredit yang Anda inginkan.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 left-content">
                                        <div class="media">
                                            <img class="d-flex mr-3 revamp-calculate-img" src="/assets/img/calculate.png" alt="">
                                            <div class="media-body">
                                                <h2 class="mt-4 title revamp-calculate-title"><a target="_blank" style="color: #3B4144 !important;" href="/hitung-harga-properti-maksimal.html">Hitung Harga
                                                        Properti Maksimal</a></h2>
                                                Hitung berapa jumah harga maksimal properti yang ideal untuk Anda
                                                <p class="more-calculate"><a target="_blank" href="/hitung-harga-properti-maksimal.html">Selengkapnya
                                                        &#8594;</a></p>
                                            </div>
                                        </div>
                                        <div class="media">
                                            <img class="d-flex mr-3 revamp-calculate-img" src="/assets/img/simulate.png" alt="">
                                            <div class="media-body">
                                                <h2 class="mt-4 title revamp-calculate-title"><a target="_blank" style="color: #3B4144 !important;" href="/simulasi-kpr.html">Simulasi KPR</a></h2>
                                                Hitung berapa jumlah angsuran dan pembayaran pertama untuk rencana KPR
                                                Anda.
                                                <p class="more-calculate"><a target="_blank" href="/simulasi-kpr.html">Selengkapnya &#8594;</a></p>
                                            </div>
                                        </div>
                                        <div class="media">
                                            <img class="d-flex mr-3 revamp-calculate-img" src="/assets/img/marketprice.png" alt="">
                                            <div class="media-body">
                                                <h2 class="mt-4 title revamp-calculate-title"><a target="_blank" style="color: #3B4144 !important;" href="/harga-pasar.html">Harga Pasar</a></h2>
                                                Hitung berapa jumah harga maksimal properti yang ideal untuk Anda
                                                <p class="more-calculate"><a target="_blank" href="/harga-pasar.html">Selengkapnya &#8594;</a></p>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="content-list">
            <div class="container">
                <section class="product-selection">
                    <h1 class="title" style="text-align: center">Berita & Informasi</h1>

                    <div class="tab-menu">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tab-news-popular" role="tab" aria-expanded="true"><span>Berita Populer</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-news-info" role="tab"><span>Info
                                        Terbaru</span></a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="tab" href="#tab-news-pameran" role="tab"><span>Info
                                        Pameran</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-news-btn" role="tab"><span>BTN Housing
                                        Finance Center</span></a>
                            </li>
                            <li class="nav-item right-menu">
                                <a class="nav-link" href="/blog.html"><span>Lihat Semua Tips & Berita</span></a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content" id="tab-news">
                        <div class="tab-pane fade show active" id="tab-news-popular" role="tabpanel">
                            <div class="list-news">
                                <div class="owl-carousel owl-carousel-slide green-nav ">
                                    <?php
                                    echo join("", $HTMLBeritaPopuler);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-news-info" role="tabpanel">
                            <div class="list-news">
                                <div class="owl-carousel owl-carousel-slide green-nav ">
                                    <?php
                                    echo join("", $HTMLBeritaInfoTerbaru);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-news-pameran" role="tabpanel">
                            <div class="list-news">
                                <div class="owl-carousel owl-carousel-slide green-nav ">
                                    <?php
                                    echo join("", $HTMLBeritaInfoPameran);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-news-btn" role="tabpanel">
                            <div class="list-news">
                                <div class="owl-carousel owl-carousel-slide green-nav ">
                                    <?php
                                    echo join("", $HTMLBeritaBTNHousing);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>


        <div class="content-bottom content-list">
            <div class="container">
                <div class="panel-card revamp-panelcard">
                    <a href="/developer.html" class="btn-link pull-right link-title-right">Lihat Semua Developer</a>
                    <h1 class="title bold">Pengembang pilihan Bank BTN</h1>

                    <div class="developer-logo-list row">
                        <?= join("", $HTMLDeveloper) ?>
                    </div>
                </div>

                <!-- TESTIMONIALS -->
                <div class="testimonials">
                    <div class="container">

                        <div class="row">
                            <div class="col-sm-12">
                                <div id="customers-testimonials" class="owl-carousel">
                                    <?= join("", $HTMLTestimonial) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END OF TESTIMONIALS -->

            </div>
        </div>


    </div>
</content>
<?php
$footer_data = array(
    "FooterSosmed" => true
);
$this->load->view('layout/footer', $footer_data);
?>

<script>
    $(function(e) {
        $(".product-area .dropdown-menu .dropdown-item").on("click", function(e) {
            var elm = $(this),
                prt = elm.closest(".dropdown"),
                btn = prt.find(".dropdown-toggle"),
                filter = elm.data("filter"),
                target = $("#tab-product-area");
            var option = {
                "rumah": "Rumah",
                "apartemen": "Apartemen",
                "all": "Semua"
            }
            btn.html(" Tipe Properti : " + option[filter])
            prt.find(".dropdown-item").removeClass("selected")
            elm.addClass("selected")

            if (target.hasClass('loading')) {
                target.loadingOverlay('remove');
            } else {
                target.loadingOverlay({
                    loadingText: 'Sedang memuat data'
                });
            };

            $.ajax({
                url: '/ajax/properti/propertipopuler',
                dataType: 'json',
                data: {
                    type: filter
                },
                type: 'post',
                success: function(output) {
                    target.loadingOverlay('remove');
                    $('#tab-product-area #tab-properti-popular .owl-carousel-slide')
                        .remove();
                    $('#tab-product-area #tab-properti-terbaru .owl-carousel-slide')
                        .remove();
                    $('#tab-product-area #tab-properti-subsidi .owl-carousel-slide')
                        .remove();
                    $('#tab-product-area #tab-properti-popular .list-product').html(
                        "<div class=\"owl-carousel owl-carousel-slide green-nav\">" +
                        output.Populer + "</div>")
                    $('#tab-product-area #tab-properti-terbaru .list-product').html(
                        "<div class=\"owl-carousel owl-carousel-slide green-nav\">" +
                        output.Terbaru + "</div>")
                    $('#tab-product-area #tab-properti-subsidi .list-product').html(
                        "<div class=\"owl-carousel owl-carousel-slide green-nav\">" +
                        output.Subsidi + "</div>")

                    $('#tab-product-area .owl-carousel-slide').owlCarousel({
                        margin: 50,
                        nav: true,
                        loop: false,
                        responsive: {
                            1200: {
                                items: 3
                            },
                            // breakpoint from 768 up
                            768: {
                                items: 2
                            }
                        },
                        navText: ["", ""]

                    });

                }
            });


        })
        //  TESTIMONIALS CAROUSEL HOOK
        $('#customers-testimonials').owlCarousel({
            loop: true,
            center: true,
            items: 3,
            margin: 0,
            autoplay: true,
            dots: true,
            autoplayTimeout: 8500,
            smartSpeed: 450,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1170: {
                    items: 2
                }
            }
        });
        $('#jenisbunga').on('change', function() {
            //            alert($('#jenisbunga').val());
            switch ($('#jenisbunga').val()) {
                case "anuitas":
                    $("#descAnuitas").show();
                    $("#descFlat").hide();
                    break;
                case "flat":
                    $("#descAnuitas").hide();
                    $("#descFlat").show();
                    break;
                default:
                    $("#descAnuitas").show();
                    $("#descFlat").hide();
            }
        });
    })
</script>