import React, { useState, useEffect, useRef, createRef } from "react";
import Layout from "../components/Layout";
import Image from "next/image";
import qs from "qs";
import Hero from "../components/static/Hero";
import Panduan from "../components/static/Panduan";
import Features from "../components/section/Features";
import Cerita from "../components/section/Cerita";
import Bantuan from "../components/section/Bantuan";
import BannerDownload from "../components/section/BannerDownload";
import PropertyTerbaru from "../components/section/PropertyTerbaru";
import PropertyPopuler from "../components/section/PropertyPopuler";
import PropertySubsidi from "../components/section/PropertySubsidi";
import HomeNews from "../components/section/HomeNews";
import getHeaderWithAccessKey from "../utils/getHeaderWithAccessKey";
import { useAppContext } from "../context";
import { useRouter } from "next/router";
import PopUp from "../components/section/PopUp";
import { getGenerateLinkPreApproval, getReGenerateLinkPreApproval, getGenerateHomeServiceToken, getGenerateProfesionaListingToken, getGenerateHomeServiceLink, getGenerateProfesionaListingLink} from "../services/konten";
import { defaultHeaders } from "../utils/defaultHeaders";

export default function index() {
  const [loaded, setLoaded] = useState(false);
  const [tipeRumah, setTipeRumah] = useState([]);
  const [listProperti, setListProperti] = useState([]);
  const [propSubsidi, setPropSubsidi] = useState([]);
  const { userKey } = useAppContext();
  const [visiblePopUp, setVisiblePopup] = useState(true);
  const router = useRouter();
  const { stat } = router.query;
  const btnRef = createRef();

  useEffect(async () => {
    if ("keyMember" in sessionStorage) {
      const storagepreApproval = localStorage.getItem("body-preapproval");
      let preApproval = JSON.parse(storagepreApproval);
      console.log(preApproval)
      let approval = await getGenerateLinkPreApproval(preApproval);
      console.log(approval)
      localStorage.setItem("generate-url",approval.url);
    
      const storagehomeservicetoken = localStorage.getItem("body-homeservice-token");
      let homeserviceToken = JSON.parse(storagehomeservicetoken);
      console.log(homeserviceToken)
      let home_token = await getGenerateHomeServiceToken(homeserviceToken);
      console.log(home_token)
      localStorage.setItem("generate-token",home_token.data.access_token);
    
      const storagprofesionalistingtoken = localStorage.getItem("body-profesionallisting-token");
      let profesionalistingToken = JSON.parse(storagprofesionalistingtoken);
      console.log(profesionalistingToken)
      let profesiona_listing_token = await getGenerateProfesionaListingToken(profesionalistingToken);
      console.log(profesiona_listing_token)
      localStorage.setItem("generate-token-profesiona-listing",profesiona_listing_token.data.access_token);
    
      const storagehomeservicelink = localStorage.getItem("body-homeservice-link");
      let homeserviceLink = JSON.parse(storagehomeservicelink);
      console.log(homeserviceLink)
      let home_service_link = await getGenerateHomeServiceLink(JSON.stringify(homeserviceLink));
      console.log(home_service_link)
      localStorage.setItem("url-home-services",home_service_link.data.link);
      
      const storagprofesionalistinglink = localStorage.getItem("body-profesionallisting-link");
      let profesionalistingLink = JSON.parse(storagprofesionalistinglink);
      console.log(profesionalistingLink)
      let profesiona_listing_link = await getGenerateProfesionaListingLink(JSON.stringify(profesionalistingLink));
      console.log(profesiona_listing_link)
      localStorage.setItem("url-profesiona-listing",profesiona_listing_link.data.link);
    } else {
      localStorage.removeItem("keyMember");
          localStorage.removeItem("generate-url");
          localStorage.removeItem("generate-token");
          localStorage.removeItem("generate-token-profesiona-listing");
          localStorage.removeItem("body-homeservice-link");
          localStorage.removeItem("body-homeservice-token");
          localStorage.removeItem("body-profesionallisting-token");
          localStorage.removeItem("body-profesionallisting-link");
          localStorage.removeItem("body-preapproval");
          localStorage.removeItem("url-home-services");
          localStorage.removeItem("url-profesiona-listing");
      // alert('no');
    }
    }, []);
    

  useEffect(() => {
    if (localStorage.getItem("accessKey")) {
      getAllData();
    }
  }, []);

  const getAllData = async () => {
    setLoaded(false);
    try {
      const newQueryObj = {
        Limit:4,
        Page:1,
        Jns:"",
        Sort:"jm_vw DESC"
      }
      const populer = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/search?${qs.stringify(newQueryObj)}`, {
        method: "GET",
        headers:{
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
        // body: new URLSearchParams({
        //   Limit: 4,
        //   Page: 1,
        //   jns: "",
        //   Sort: "jm_vw DESC",
        // }),
      });
      const dataPopuler = await populer.json();
      if (!dataPopuler.IsError) {
        setTipeRumah(dataPopuler.data);
      } else {
        throw {
          message: dataPopuler.ErrToUser,
        };
      }
      const newQueryObj2 = {
        Limit: 4,
        Page: 1,
        Sort: "t_ins DESC",
      }
      const terbaru = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/search?${qs.stringify(newQueryObj2)}`, {
        method: "GET",
        headers:{
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
        // body: new URLSearchParams({
        //   Limit: 4,
        //   Page: 1,
        //   Sort: "t_ins DESC",
        // }),
      });
      const dataTerbaru = await terbaru.json();
      if (!dataTerbaru.IsError) {
        setListProperti(dataTerbaru.data);
      } else {
        throw {
          message: dataTerbaru.ErrToUser,
        };
      }
      const newQueryObj3 = {
        Limit: 4,
          Page: 1,
          jns: 2,
      }
      const subsidi = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/search?${qs.stringify(newQueryObj3)}`, {
        method: "GET",
        headers:{
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
        // body: new URLSearchParams({
        //   Limit: 4,
        //   Page: 1,
        //   jns: 2,
        //   // Sort: 'jm_vw DESC'
        // }),
      });
      const dataSubsidi = await subsidi.json();
      if (!dataSubsidi.IsError) {
        setPropSubsidi(dataSubsidi.data);
      } else {
        throw {
          message: dataSubsidi.ErrToUser,
        };
      }
    } catch (error) {
      console.log("Header " + JSON.stringify(getHeaderWithAccessKey()));
      console.log("Error fetch properti search " + error.message);
    } finally {
      setLoaded(true);
    }
  };

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
          minWidth: "100%",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <div>
      <Layout title="home">
        <Hero />
        <Panduan />
        <Features />
        <PropertyPopuler tipeRumah={tipeRumah} />
        <PropertyTerbaru listProperti={listProperti} />
        <PropertySubsidi propSubsidi={propSubsidi} />
        <Cerita />
        <HomeNews />
        <Bantuan userKey={userKey} />
        <BannerDownload />
        {process.env.NEXT_PUBLIC_POP_UP_VIRTUAL_EXPO === 'true' && <PopUp />}
      </Layout>
    </div>
  );
}
