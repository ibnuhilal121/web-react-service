import Layout from "../components/Layout";
import Breadcrumb from "../components/section/Breadcrumb";
import { useMediaQuery } from "react-responsive";
import FloatMenu from "../components/FloatMenu";

const setup = {
  title: {
    text: 'Kebijakan Privasi',
    href: '#syarat'
  },
  lists: [
    {
      text: 'Perolehan dan Pengumpulan Data Pengguna',
      href: '#perolehan_data'
    },
    {
      text: 'Penggunaan Data',
      href: '#penggunaan_data'
    },
    {
      text: 'Pengungkapan Data Pribadi Pengguna',
      href: '#pengungkapan_data'
    },
    {
      text: 'Cookies',
      href: '#cookies'
    },
    {
      text: 'Penyimpanan dan Penghapusan Informas',
      href: '#penyimpanan'
    },
    {
      text: 'Pembaruan Kebijakan Privasi',
      href: '#pembaharuan'
    },
  ]
};

export default function index() {
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });

  return (
    <Layout title="Kebijakan Privasi">
      <section id="privasi">
        <Breadcrumb active="Kebijakan Privasi" id="privasi-breadcrumb" />
        <section
          className="mb-5"
          data-bs-spy="scroll"
          data-bs-target="#menu_sidebar"
        >
          <div className="container">
            <div className="row g-0 d-lg-flex justify-content-start">
              {isTabletOrMobile ?
              (
                <FloatMenu setup={setup} />
              ) :
              (
                <div
                  id="privacy_sidebar"
                  className="col-md-4 col-lg-3"
                  style={{
                    padding: "6px",
                    padding: "0px",
                    marginRight: "24px",
                    marginBottom: "30px",
                  }}
                >
                  <div
                    id="menu_sidebar"
                    className="list-group sticky-top"
                    style={{
                      backgroundColor: "#FAFAFA",
                      boxShadow: "0px 4px 16px rgba(0, 0, 0, 0.1)",
                      borderRadius: "8px",
                      padding: "16px",
                      color: "#00193E",
                    }}
                  >
                    <a
                      className="list-group-item-action"
                      href="#syarat"
                      style={{
                        marginBottom: "16px",
                        marginRight: "26px",
                        fontSize: "16px",
                        fontFamily: "FuturaBT ",
                        fontWeight: 700,
                        color: "#00193E",
                      }}
                    >
                      Kebijakan Privasi
                    </a>
                    <a
                      className="list-group-item-action"
                      href="#perolehan_data"
                      style={{
                        marginBottom: "8px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        fontSize: "14px",
                        paddingRight: "8px",
                        paddingLeft: "8px",
                        color: "#00193E",
                      }}
                    >
                      Perolehan dan Pengumpulan Data Pengguna
                    </a>
                    <a
                      className="list-group-item-action"
                      href="#penggunaan_data"
                      style={{
                        marginBottom: "8px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        fontSize: "14px",
                        paddingRight: "8px",
                        paddingLeft: "8px",
                        color: "#00193E",
                      }}
                    >
                      Penggunaan Data
                    </a>
                    <a
                      className="list-group-item-action"
                      href="#pengungkapan_data"
                      style={{
                        marginBottom: "8px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        fontSize: "14px",
                        paddingRight: "8px",
                        paddingLeft: "8px",
                        color: "#00193E",
                      }}
                    >
                      Pengungkapan Data Pribadi Pengguna
                    </a>
                    <a
                      className="list-group-item-action"
                      href="#cookies"
                      style={{
                        marginBottom: "8px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        fontSize: "14px",
                        paddingRight: "8px",
                        paddingLeft: "8px",
                        color: "#00193E",
                      }}
                    >
                      Cookies
                    </a>
                    <a
                      className="list-group-item-action"
                      href="#penyimpanan"
                      style={{
                        marginBottom: "8px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        fontSize: "14px",
                        paddingRight: "8px",
                        paddingLeft: "8px",
                        color: "#00193E",
                      }}
                    >
                      Penyimpanan dan Penghapusan Informasi
                    </a>
                    <a
                      className="list-group-item-action"
                      href="#pembaharuan"
                      style={{
                        marginBottom: "0px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        fontSize: "14px",
                        paddingRight: "8px",
                        paddingLeft: "8px",
                        color: "#00193E",
                      }}
                    >
                      Pembaruan Kebijakan Privasi
                    </a>
                  </div>
                </div>
              )}
              <div className="col-12 col-md">
                <div
                  className="scrollspy-example"
                  style={{
                    color: "#00193E",
                    fontFamily: "Helvetica",
                    fontWeight: 400,
                  }}
                >
                  <h3
                    id="syarat"
                    style={{
                      fontSize: "36px",
                      color: "#00193E",
                      fontFamily: "FuturaBT",
                      fontWeight: 700,
                      marginBottom: "24px",
                    }}
                  >
                    Kebijakan Privasi
                  </h3>
                  <p
                    style={{
                      color: "#00193E",
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                  >
                    Adanya Kebijakan Privasi ini adalah komitmen nyata dari Bank
                    BTN untuk menghargai dan melindungi setiap data atau
                    informasi pribadi Pengguna situs www.btnproperti.co.id,
                    situs-situs turunannya, serta aplikasi gawai BTN Properti
                    (selanjutnya disebut sebagai Situs).
                  </p>

                  <p
                    style={{
                      color: "#00193E",
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                  >
                    Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari
                    situs BTN Properti sebagaimana tercantum dalam Syarat
                    &Ketentuan dan informasi lain yang tercantum di Situs)
                    menetapkan dasar atas perolehan, pengumpulan, pengolahan,
                    penganalisisan, penampilan, pembukaan, dan/atau segala
                    bentuk pengelolaan yang terkait dengan data atau informasi
                    yang Pengguna berikan kepada Bank BTN atau yang BTN Properti
                    kumpulkan dari Pengguna, termasuk data pribadi Pengguna,
                    baik pada saat Pengguna melakukan pendaftaran di Situs,
                    mengakses Situs, maupun mempergunakan layanan-layanan pada
                    Situs (selanjutnya disebut sebagai data).
                  </p>

                  <p
                    style={{
                      color: "#00193E",
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                  >
                    Dengan mengakses dan/atau mempergunakan layanan BTN
                    Properti, Pengguna menyatakan bahwa setiap data Pengguna
                    merupakan data yang benar dan sah, serta Pengguna memberikan
                    persetujuan kepada Bank BTN untuk memperoleh, mengumpulkan,
                    menyimpan, mengelola dan mempergunakan data tersebut
                    sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat
                    dan Ketentuan BTN Properti.
                  </p>

                  <h4
                    id="perolehan_data"
                    style={{
                      fontFamily: "FuturaBT",
                      fontSize: "24px",
                      color: "#00193E",
                      fontWeight: 700,
                    }}
                  >
                    A. Perolehan dan Pengumpulan Data Pengguna
                  </h4>
                  <p>
                    Bank BTN mengumpulkan data Pengguna dengan tujuan untuk
                    memproses transaksi Pengguna, mengelola dan memperlancar
                    proses penggunaan Situs, serta tujuan-tujuan lainnya selama
                    diizinkan oleh peraturan perundang-undangan yang berlaku.
                    Adapun data Pengguna yang dikumpulkan adalah sebagai
                    berikut:
                  </p>

                  <ol>
                    <li>
                      Data yang diserahkan secara mandiri oleh Pengguna,
                      termasuk namun tidak terbatas pada data yang diserahkan
                      pada saat Pengguna:
                      <ul style={{ listStyle: "disc" }}>
                        <li>
                          Membuat atau memperbarui akun BTN Properti, termasuk
                          diantaranya nama pengguna (username), alamat email,
                          nomor telepon, password, alamat, foto, dan lain-lain;
                        </li>
                        <li>
                          Menghubungi Bank BTN, termasuk melalui layanan
                          konsumen BTN Properti;
                        </li>
                        <li>
                          Mengisi survei yang dikirimkan oleh Bank BTN atau atas
                          nama BTN Properti;
                        </li>
                        <li>
                          Melakukan interaksi dengan Pengguna lainnya melalui
                          fitur pesan, diskusi produk, ulasan, rating, Pusat
                          Resolusi dan sebagainya;
                        </li>
                        <li>
                          Mempergunakan layanan-layanan pada Situs, termasuk
                          data transaksi pemesanan unit properti (Rumah ataupun
                          Apartemen) yang detil, diantaranya tipe, jumlah
                          dan/atau keterangan dari unit properti (Rumah ataupun
                          Apartemen) yang dibeli, data diri, kanal pembayaran
                          yang digunakan, jumlah transaksi, tanggal dan waktu
                          transaksi, serta detil transaksi lainnya;
                        </li>
                        <li>
                          Mengisi data-data pembayaran pada saat Pengguna
                          melakukan aktivitas transaksi pembayaran melalui
                          Situs, termasuk namun tidak terbatas pada data
                          rekening bank, virtual account, mobile banking,
                          internet banking, dan/atau
                        </li>
                        <li>
                          Menggunakan fitur yang membutuhkan izin akses terhadap
                          perangkat Pengguna.
                        </li>
                      </ul>
                    </li>

                    <li>
                      Data yang terekam pada saat Pengguna mempergunakan Situs,
                      termasuk namun tidak terbatas pada:
                      <ul style={{ listStyle: "disc" }}>
                        <li>
                          Data lokasi riil atau perkiraannya seperti alamat IP,
                          lokasi Wi-Fi, geo-location, dan sebagainya;
                        </li>
                        <li>
                          Data berupa waktu dari setiap aktivitas Pengguna,
                          termasuk aktivitas pendaftaran, login, transaksi, dan
                          lain sebagainya;
                        </li>
                        <li>
                          Data penggunaan atau preferensi Pengguna, diantaranya
                          interaksi Pengguna dalam menggunakan Situs, pilihan
                          yang disimpan, serta pengaturan yang dipilih. Data
                          tersebut diperoleh menggunakan cookies, pixel tags,
                          dan teknologi serupa yang menciptakan dan
                          mempertahankan pengenal unik;
                        </li>
                        <li>
                          Data perangkat, diantaranya jenis perangkat yang
                          digunakan untuk mengakses Situs, termasuk model
                          perangkat keras, sistem operasi dan versinya,
                          perangkat lunak, nama file dan versinya, pilihan
                          bahasa, pengenal perangkat unik, pengenal iklan, nomor
                          seri, informasi gerakan perangkat, dan/atau informasi
                          jaringan seluler;
                        </li>
                        <li>
                          Data catatan (log), diantaranya catatan pada server
                          yang menerima data seperti alamat IP perangkat,
                          tanggal dan waktu akses, fitur aplikasi atau laman
                          yang dilihat, proses kerja aplikasi dan aktivitas
                          sistem lainnya, jenis peramban, dan/atau situs atau
                          layanan pihak ketiga yang Anda gunakan sebelum
                          berinteraksi dengan Situs.
                        </li>
                      </ul>
                    </li>
                    <li>
                      Data yang diperoleh dari sumber lain, termasuk:
                      <ul style={{ listStyle: "disc" }}>
                        <li>
                          Mitra Bank BTN yang turut membantu BTN properti dalam
                          mengembangkan dan menyajikan layanan-layanan dalam
                          Situs kepada Pengguna, antara lain
                          Developer/Pengembang dan agen, Profesional, dan
                          mitra-mitra lainnya.
                        </li>
                        <li>
                          Mitra usaha Bank BTN tempat Pengguna membuat atau
                          mengakses akun BTN Properti, seperti layanan media
                          sosial, atau situs/aplikasi yang menggunakan API BTN
                          properti atau yang digunakan BTN Properti;
                        </li>
                        <li>Penyedia layanan finansial;</li>
                        <li>Penyedia layanan pemasaran;</li>
                        <li>Sumber yang tersedia secara umum.</li>
                      </ul>
                    </li>
                  </ol>

                  <p>
                    Bank BTN dapat menggabungkan data yang diperoleh dari sumber
                    tersebut dengan data lain yang dimilikinya.
                  </p>

                  <h4
                    id="penggunaan_data"
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontSize: "24px",
                      color: "#00193E",
                    }}
                  >
                    B. Penggunaan Data
                  </h4>
                  <p>
                    Bank BTN dapat menggunakan keseluruhan atau sebagian data
                    yang diperoleh dan dikumpulkan dari Pengguna sebagaimana
                    disebutkan dalam bagian sebelumnya untuk hal-hal sebagai
                    berikut:
                  </p>

                  <ol>
                    <li>
                      Memproses segala bentuk permintaan, aktivitas maupun
                      transaksi yang dilakukan oleh Pengguna melalui Situs,
                      termasuk untuk keperluan pemesanan unit properti (Rumah
                      ataupun Apartemen) oleh Pengguna.
                    </li>
                    <li>
                      Penyediaan fitur-fitur untuk memberikan, mewujudkan,
                      memelihara dan memperbaiki produk dan layanan kami,
                      termasuk:
                      <ul style={{ listStyle: "disc" }}>
                        <li>
                          Menawarkan, memperoleh, menyediakan, atau
                          memfasilitasi layanan marketplace, asuransi,
                          pembiayaan, pinjaman, maupun produk-produk lainnya
                          melalui Situs;
                        </li>
                        <li>
                          Memungkinkan fitur untuk mempribadikan akun BTN
                          Properti Pengguna, seperti Favorit Properti dan Simpan
                          Pencariandan/atau
                        </li>
                        <li>
                          Melakukan kegiatan internal yang diperlukan untuk
                          menyediakan layanan pada situs/aplikasi BTN Properti,
                          seperti pemecahan masalah software, bug, permasalahan
                          operasional, melakukan analisis data, pengujian, dan
                          penelitian, dan untuk memantau dan menganalisis
                          kecenderungan penggunaan dan aktivitas.
                        </li>
                      </ul>
                    </li>
                    <li>
                      Membantu Pengguna pada saat berkomunikasi dengan Layanan
                      Pelanggan BTN Properti, diantaranya untuk:
                      <ul style={{ listStyle: "disc" }}>
                        <li>Memeriksa dan mengatasi permasalahan Pengguna;</li>
                        <li>
                          Mengarahkan pertanyaan Pengguna kepada petugas Layanan
                          Pelanggan yang tepat untuk mengatasi permasalahan dan
                        </li>
                        <li>
                          Mengawasi dan memperbaiki tanggapan Layanan Pelanggan
                          BTN Properti.
                        </li>
                      </ul>
                    </li>
                    <li>
                      Menghubungi Pengguna melalui email, surat, telepon, fax,
                      dan lain-lain, termasuk namun tidak terbatas, untuk
                      membantu dan/atau menyelesaikan proses transaksi maupun
                      proses penyelesaian kendala.
                    </li>
                    <li>
                      Menggunakan informasi yang diperoleh dari Pengguna untuk
                      tujuan penelitian, analisis, pengembangan dan pengujian
                      produk guna meningkatkan keamanan dan keamanan
                      layanan-layanan pada Situs, serta mengembangkan fitur dan
                      produk baru.
                    </li>
                    <li>
                      Menginformasikan kepada Pengguna terkait produk, layanan,
                      promosi, studi, survei, berita, perkembangan terbaru,
                      acara dan lain-lain, baik melalui Situs maupun melalui
                      media lainnya. Bank BTN juga dapat menggunakan informasi
                      tersebut untuk mempromosikan dan memproses kontes dan
                      undian, memberikan hadiah, serta menyajikan iklan dan
                      konten yang relevan tentang layanan BTN Properti dan mitra
                      usahanya.
                    </li>
                    <li>
                      Melakukan monitoring ataupun investigasi terhadap
                      transaksi-transaksi mencurigakan atau transaksi yang
                      terindikasi mengandung unsur kecurangan atau pelanggaran
                      terhadap Syarat dan Ketentuan atau ketentuan hukum yang
                      berlaku, serta melakukan tindakan-tindakan yang diperlukan
                      sebagai tindak lanjut dari hasil monitoring atau
                      investigasi transaksi tersebut.
                    </li>
                    <li>
                      Dalam keadaan tertentu, Bank BTN mungkin perlu untuk
                      menggunakan ataupun mengungkapkan data Pengguna untuk
                      tujuan penegakan hukum atau untuk pemenuhan persyaratan
                      hukum dan peraturan yang berlaku, termasuk dalam hal
                      terjadinya sengketa atau proses hukum antara Pengguna dan
                      Bank BTN.
                    </li>
                  </ol>

                  <h4
                    id="pengungkapan_data"
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontSize: "24px",
                      color: "#00193E",
                    }}
                  >
                    C. Pengungkapan Data Pribadi Pengguna
                  </h4>
                  <p>
                    Bank BTN menjamin tidak ada penjualan, pengalihan,
                    distribusi atau meminjamkan data pribadi Anda kepada pihak
                    ketiga lain, tanpa terdapat izin dari Anda, kecuali dalam
                    hal-hal sebagai berikut:
                  </p>

                  <ol>
                    <li>
                      Dibutuhkan adanya pengungkapan data Pengguna kepada mitra
                      atau pihak ketiga lain yang membantu Bank BTN dalam
                      menyajikan layanan pada Situs dan memproses segala bentuk
                      aktivitas Pengguna dalam Situs, termasuk memproses
                      transaksi, verifikasi pembayaran, pemesanan unit properti
                      (Rumah ataupun Apartemen), dan lain-lain.
                    </li>
                    <li>
                      Bank BTN dapat menyediakan informasi yang relevan kepada
                      mitra usaha sesuai dengan persetujuan Pengguna untuk
                      menggunakan layanan mitra usaha, termasuk diantaranya
                      aplikasi atau situs lain yang telah saling
                      mengintegrasikan API atau layanannya, atau mitra usaha
                      yang mana Bank BTN telah bekerjasama untuk mengantarkan
                      promosi, kontes, atau layanan yang dikhususkan.
                    </li>
                    <li>
                      Dibutuhkan adanya komunikasi antara mitra BTN Properti
                      (seperti Developer/Pengembang dan agen, Profesional, dan
                      lain-lain) dengan Pengguna dalam hal penyelesaian kendala
                      maupun hal-hal lainnya.
                    </li>
                    <li>
                      Bank BTN dapat menyediakan informasi yang relevan kepada
                      vendor, konsultan, mitra pemasaran, firma riset, atau
                      penyedia layanan sejenis.
                    </li>
                    <li>
                      Pengguna menghubungi Bank BTN melalui media publik seperti
                      blog, media sosial, dan fitur tertentu pada Situs,
                      komunikasi antara Pengguna dan Bank BTN mungkin dapat
                      dilihat secara publik.
                    </li>
                    <li>
                      Bank BTN dapat membagikan informasi Pengguna kepada anak
                      perusahaan dan afiliasinya untuk membantu memberikan
                      layanan atau melakukan pengolahan data untuk dan atas nama
                      Bank BTN.
                    </li>
                    <li>
                      Bank BTN mengungkapkan data Pengguna dalam upaya mematuhi
                      kewajiban hukum dan/atau adanya permintaan yang sah dari
                      aparat penegak hukum.
                    </li>
                  </ol>

                  <h4
                    id="cookies"
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontSize: "24px",
                      color: "#00193E",
                    }}
                  >
                    D. Cookies
                  </h4>

                  <ol>
                    <li>
                      Cookies adalah file kecil yang secara otomatis akan
                      mengambil tempat di dalam perangkat Pengguna yang
                      menjalankan fungsi dalam menyimpan preferensi maupun
                      konfigurasi Pengguna selama mengunjungi suatu situs.
                    </li>
                    <li>
                      Cookies tersebut tidak diperuntukkan untuk digunakan pada
                      saat melakukan akses data lain yang Pengguna miliki di
                      perangkat komputer Pengguna, selain dari yang telah
                      Pengguna setujui untuk disampaikan.
                    </li>
                    <li>
                      Walaupun secara otomatis perangkat komputer Pengguna akan
                      menerima cookies, Pengguna dapat menentukan pilihan untuk
                      melakukan modifikasi melalui pengaturan browser Pengguna
                      yaitu dengan memilih untuk menolak cookies (pilihan ini
                      dapat membatasi layanan optimal pada saat melakukan akses
                      ke Situs).
                    </li>
                    <li>
                      BTN Properti menggunakan fitur Google Analytics
                      Demographics and Interest. Data yang kami peroleh dari
                      fitur tersebut, seperti umur, jenis kelamin, minat
                      Pengguna dan lain-lain, akan kami gunakan untuk
                      pengembangan Situs dan konten BTN properti. Jika tidak
                      ingin data Anda terlacak oleh Google Analytics, Anda dapat
                      menggunakan Add-On Google Analytics Opt-Out Browser.
                    </li>
                    <li>
                      Bank BTN dapat menggunakan fitur-fitur yang disediakan
                      oleh pihak ketiga dalam rangka meningkatkan layanan dan
                      konten BTN Properti, termasuk diantaranya ialah
                      penyesuaian dan penyajian iklan kepada setiap Pengguna
                      berdasarkan minat atau riwayat kunjungan. Jika Anda tidak
                      ingin iklan ditampilkan berdasarkan penyesuaian tersebut,
                      maka Anda dapat mengaturnya melalui browser.
                    </li>
                  </ol>

                  <h4
                    id="penyimpanan"
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontSize: "24px",
                      color: "#00193E",
                    }}
                  >
                    E. Penyimpanan dan Penghapusan Informasi
                  </h4>

                  <ol>
                    <li>
                      Perangkat seluler pada umumnya (iOS, Android, dan
                      sebagainya) memiliki pengaturan sehingga aplikasi BTN
                      Properti tidak dapat mengakses data tertentu tanpa
                      persetujuan dari Pengguna. Perangkat iOS akan memberikan
                      pemberitahuan kepada Pengguna saat aplikasi BTN Properti
                      pertama kali meminta akses terhadap data tersebut,
                      sedangkan perangkat Android akan memberikan pemberitahuan
                      kepada Pengguna saat aplikasi BTN Properti pertama kali
                      dimuat. Dengan menggunakan aplikasi dan memberikan akses
                      terhadap aplikasi, Pengguna dianggap memberikan
                      persetujuannya terhadap Kebijakan Privasi.
                    </li>
                    <li>
                      Setelah transaksi pemesanan unit properti (Rumah ataupun
                      Apartemen) berhasil, Developer/Pengembang dan agen maupun
                      Pengguna memiliki kesempatan untuk memberikan penilaian
                      dan ulasan terhadap satu sama lain. Informasi ini mungkin
                      dapat dilihat secara publik dengan persetujuan Pengguna.
                    </li>
                  </ol>

                  <p>
                    Pengguna dapat mengakses dan mengubah informasi berupa
                    alamat email, nomor telepon, tanggal lahir, jenis kelamin,
                    daftar alamat, metode pembayaran, dan rekening bank melalui
                    fitur Pengaturan pada Situs.
                  </p>

                  <p>
                    <br />
                    Pengguna dapat menarik diri dari informasi atau notifikasi
                    berupa buletin, ulasan, diskusi produk, pesan pribadi, atau
                    pesan pribadi dari Admin yang dikirimkan oleh BTN Properti
                    melalui fitur Pengaturan pada Situs. Bank BTN tetap dapat
                    mengirimkan pesan atau email berupa keterangan transaksi
                    atau informasi terkait akun Pengguna.
                  </p>

                  <p>
                    <br />
                    Sejauh diizinkan oleh ketentuan yang berlaku, Pengguna dapat
                    menghubungi Bank BTN untuk melakukan penarikan persetujuan
                    terhadap perolehan, pengumpulan, penyimpanan, pengelolaan
                    dan penggunaan data Pengguna. Apabila terjadi demikian maka
                    Pengguna memahami konsekuensi bahwa Pengguna tidak dapat
                    menggunakan layanan Situs maupun layanan BTN Properti
                    lainnya.
                  </p>

                  <p>
                    Bank BTN akan menyimpan informasi selama akun Pengguna tetap
                    aktif dan dapat melakukan penghapusan sesuai dengan
                    ketentuan peraturan hukum yang berlaku.
                  </p>

                  <h4
                    id="pembaharuan"
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontSize: "24px",
                      color: "#00193E",
                    }}
                  >
                    F. Pembaruan Kebijakan Privasi
                  </h4>
                  <p>
                    Bank BTN dapat sewaktu-waktu melakukan perubahan atau
                    pembaruan terhadap Kebijakan Privasi ini. Bank BTN
                    menyarankan agar Pengguna membaca secara seksama dan
                    memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu
                    untuk mengetahui perubahan apapun. Dengan tetap mengakses
                    dan menggunakan layanan Situs maupun layanan BTN Properti
                    lainnya, maka Pengguna dianggap menyetujui
                    perubahan-perubahan dalam Kebijakan Privasi.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
    </Layout>
  );
}
