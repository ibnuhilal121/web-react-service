import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import { useAppContext } from "../../context";

export default function VerifikasiDocument () {
    const {userProfile} = useAppContext()
    const router = useRouter();
    const {query} = router;
    const { kprId } = query;
    const [data, setData] = useState({})

    function timeConverter(UNIX_timestamp){
        var a = UNIX_timestamp * 1000;
        const date = new Date(UNIX_timestamp)
        var months = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        console.log({date});
        const year = date.getFullYear()
        const month = date.getMonth()
        const tgl = date.getDate()
        const hours = date.getHours()
        const minutes = date.getMinutes()
        return `${tgl} ${months[month]} ${year} ${hours.toString().length > 1 ? hours : "0" + hours}.${minutes.toString().length > 1 ? minutes : "0" + minutes}`;
      }

    const getDetail = async () => {
        try {
            const response = await axios.post(process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE + '/getDocumentVerification', {
                kprId
            });
            if(response.data.status.toLowerCase() == "error") {
              router.push("/404")
            }else {
              setData(response.data.data)
            }
        } catch (error) {
            console.log(error);
            router.push("/404")
            
        }
        
    };

    useEffect(() => {
        if(kprId) {
            getDetail()
            
        }
    }, [kprId])
  return (
    <main className="py-2 container-fluid">
        <div className="container pading-verifikasi-doc">
            <img src="/images/Logo.svg" width={208} height={85} style={{marginBottom: 20}} />
            <h1 className="m-0">Verifikasi Dokumen</h1>
            <hr className="mt-lg-4 mb-lg-4 mt-md-1 mb-md-1"/>
            <h4 className="mb-3"><strong className="d-inline-block">Dokumen</strong></h4>
            <div className="row" style={{marginBottom: 48}}>
                <div className="col-lg-12">
                    <div className="card border-verifikasi-doc">
                        <div className="card-body p-0">
                            <table className="table-document-info table m-0">
                                <tbody>
                                    <tr>
                                        <th className="padding-judul lebar-judul">ID</th>
                                        <td className="padding-judul">{data?.kprId}</td>
                                    </tr>
                                    <tr>
                                        <th className="padding-judul lebar-judul">Dokumen</th>
                                        <td className="padding-judul">{data?.documentName}</td>
                                    </tr>
                                    <tr>
                                        <th className="padding-judul lebar-judul">Pemohon</th>
                                        <td className="padding-judul">{data?.documentOwnerName}</td>
                                    </tr>
                                    <tr>
                                        <th className="padding-judul lebar-judul">Tanggal Buat</th>
                                        <td className="padding-judul">{timeConverter(data?.documentUploadedAt)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <p className="text-center"  style={{marginBottom: 32}}>Telah ditandatangani oleh pengguna <strong>BTN Properti</strong> sebagai berikut:</p>
            <div className="row"  style={{marginBottom: 16}}>
                <div className="col-lg-12">
                    <h4 className="mb-3"><strong className="d-inline-block">Penandatangan</strong></h4>
                        <div className="card mb-3 border-verifikasi-doc">
                            <div className="card-body p-0">
                                <table className="table-document-info table m-0">
                                    <tbody>
                                        <tr>
                                            <th className="padding-judul lebar-judul">Nama</th>
                                            <td className="padding-judul">{data?.documentOwnerName}</td>
                                        </tr>
                                        <tr>
                                            <th className="padding-judul lebar-judul">E-mail</th>
                                            <td className="padding-judul">{data?.documentOwnerEmail}</td>
                                        </tr>
                                        <tr>
                                            <th className="padding-judul lebar-judul">Tanggal Tandatangan</th>
                                            <td className="padding-judul">{timeConverter(data?.documentSignedAt)}</td>
                                        </tr>
                                        <tr>
                                            <th className="padding-judul lebar-judul">PSrE</th>
                                            <td className="padding-judul">TekenAja</td>
                                        </tr>
                                        <tr>
                                            <th className="padding-judul lebar-judul">URL Verifikasi</th>
                                            <td className="padding-judul"><a href="https://tte.kominfo.go.id/verifyPDF" target="_blank">https://tte.kominfo.go.id/verifyPDF</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <p className="text-center mb-3"></p>
                </div>
            </div>
            <h4 className="text-center mb-5">
                <i className="far fa-check-circle mr-2 text-success"></i> <strong>Adalah benar dan tercatat dalam audit trail kami.</strong>
            </h4>
            <div className="text-center">
                <img src="/icons/master-logo-tekenaja-blue.png" alt="logo tekenaja" style={{width: 200 }} />
                <p>Supported by <strong>PT Djelas Tandatangan Bersama</strong></p>
            </div>
            
        </div>
    </main>
  )
}