/* eslint-disable react/jsx-key */
import { useRouter } from "next/router";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import data_faq from "../../../sample_data/data_faq";
import Link from "next/link";
import ChevronRight from "../../../components/element/icons/chevron_right";
import styles from "../../../styles/Faq.module.scss";
import ChatIcon from "../../../components/element/icons/ChatIcon";
import { useEffect, useState } from "react";
import { fetchFAQ } from "../../../services/faq";
import SearchIcon from "../../../components/element/icons/SearchIcon";
import { useMediaQuery } from "react-responsive";

const topics = {
  1: "General",
  2: "Developer",
  3: "User",
  4: "Admin",
  5: "Login",
  6: "Profil",
  7: "Pengajuan Kredit",
  8: "Membeli Properti",
  9: "Pencarian",
};

export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const isSmallDevice = useMediaQuery({ query: `(max-width: 900px)` });
  const router = useRouter();
  const { id } = router.query;
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    fetchFAQ("", id)
      .then((res) => {
        if (res) {
          setData(res);
        }
      })
      .catch((error) => {})
      .finally(() => {
        setLoaded(true);
      });
  }, []);

  const NotFound = () => {
    return (
      <div
        style={{
          paddingTop: 100,
          textAlign: "center",
        }}
      >
        <SearchIcon />
        <div
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "16px",
            lineHeight: "150%",
            marginTop: 40,
          }}
        >
          Maaf, Keyword yang kamu cari tidak ditemukan
        </div>
      </div>
    );
  };

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout title="FAQ">
      <Breadcrumb
        active={topics[id] || "Pencarian"}
        classes={{ page: styles.breadcrumb }}
      >
        <li className="breadcrumb-item">
          <Link href="/faq">FAQ</Link>
        </li>
      </Breadcrumb>

      <div className={`${styles.main}`}>
        <div className={`d-md-none`}>
          <section className="">
            <div className="container">
              <div className="row">
                <div className="col-md-6 offset-md-3">
                  <h3 className="title_page">FAQ</h3>
                  <div className="faq_search" style={{ marginBottom: 0 }}>
                    <div className=" d-flex justify-content-between align-items-center">
                      <div className="w-100">
                        <div className="form-group mb-0">
                          <label>Cari</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Masukkan keyword pertanyaan"
                            aria-label="Masukkan keyword pertanyaan"
                            aria-describedby="basic-addon2"
                          />
                        </div>
                      </div>
                      <div className="item">
                        <button
                          type="submit"
                          className="btn btn-main btn-circle"
                        >
                          <i className="bi bi-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <section className="mb-5">
          <div className="container">
            <div className="generale_faq">
              <div className="row">
                <div className="col-md-3">
                  <div className={`d-none d-md-block ${styles.left_desktop}`}>
                    <div className={`sticky-top ${styles.sticky_title}`}>
                      <h3
                        className="text-capitalize"
                        style={{
                          fontSize: "20px",

                          color: "#00193E",
                        }}
                      >
                        {topics[id] || "Pencarian"}
                      </h3>
                    </div>
                  </div>
                  <div className={`d-md-none ${styles.left_mobile}`}>
                    <div>
                      <div className="">
                        <ChatIcon />
                      </div>
                      <div className="">
                        <div>General</div>
                        <div>
                          <Link href="/faq">
                            <div className={`${styles.link_semua_topik}`}>
                              <div>Lihat Semua Topik</div>
                              <div
                                className={`${styles.link_semua_topik_icon}`}
                              >
                                <ChevronRight />
                              </div>
                            </div>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-9 px-4">
                  <div className="faq_general w-100" id="accordionExample"
                   style={{maxWidth:"656px"}}>
                    {data.map((item, index) => (
                      <Link href={{ 
                        pathname:`/faq/detail/`,
                        query: { id: item.ID } 
                      }} passHref>
                        <div
                          key={index}
                          className={`d-flex justify-content-between py-4 ${styles.general_item}`}
                        >
                          <div className="pe-3">{item.PERTANYAAN}</div>
                          <div>
                            <ChevronRight />
                          </div>
                          </div>
                        </Link>
                      ))
                    // ) : NotFound()
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
}
