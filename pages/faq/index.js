import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import FaqPopuler from "../../components/section/FaqPopuler";
import ItemTopik from "../../components/static/ItemTopik";
import ChatIcon from "../../components/element/icons/ChatIcon";
import BuildingIcon from "../../components/element/icons/BuildingIcon";
import PersonIcon from "../../components/element/icons/PersonIcon";
import GearBoxIcon from "../../components/element/icons/GearBoxIcon";
import DoorOpenIcon from "../../components/element/icons/DoorOpenIcon";
import SmileIcon from "../../components/element/icons/SmileIcon";
import CardIcon from "../../components/element/icons/CardIcon";
import ShoppingBasketIcon from "../../components/element/icons/ShoppingBasketIcon";
import { fetchFAQ } from "../../services/faq";
import { useRouter } from "next/router";

export default function index() {
  const [data, setData] = useState(null);
  const [loaded, setLoaded] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [loadData, setLoadData] = useState(false);

  useEffect(() => {
    fetchFAQ()
    .then(res => {
      if (res) {
        setData(res);
      }
    })
    .catch(error => {})
    .finally(() => {
      setLoaded(true);
    })
  }, []);

  const getKeywordFAQ = () => {
    setLoadData(true);
    fetchFAQ(keyword)
    .then(res => {
      if (res) {
        setData(res);
      } else {
        setData([]);
      };
    })
    .catch(error => {})
    .finally(() => {
      setLoadData(false);
    })
  }

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout title="FAQ" isLoaderOpen={loadData}>
      <Breadcrumb active="FAQ" />
      <section className="mb-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-7">
              <h3 className="title_page">FAQ</h3>
              <div className="faq_search">
                <div className=" d-flex justify-content-between align-items-center">
                  <div className="w-100">
                    <div className="form-control mb-0">
                      <label>Cari</label>
                      <input style={{display: "flex"}}
                        type="text"
                        className="form-control d-sm-flex"
                        placeholder="Masukkan keyword pertanyaan"
                        aria-label="Masukkan keyword pertanyaan"
                        aria-labelledby="basic-addon2"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="item">
                    <button 
                      type="submit" 
                      className="btn btn-main circle"
                      onClick={getKeywordFAQ}
                      style={{
                        alignItems:"center"
                      }}
                    >
                      <i className="bi bi-search"
                      style={{
                        transform:"scale(1.4)",
                        display:"flex",
                        alignSelf:"center"
                      }}
                      ></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <h4 className="text-center title_section">Topik Pertanyaan</h4>
              <div className="row">
                <ItemTopik slug="1" title="General" Icon={ChatIcon} />
                <ItemTopik
                  slug="2"
                  title="Developer"
                  Icon={BuildingIcon}
                />
                <ItemTopik slug="3" title="User" Icon={PersonIcon} />
                <ItemTopik slug="4" title="Admin" Icon={GearBoxIcon} />
                <ItemTopik slug="5" title="Login" Icon={DoorOpenIcon} />
                <ItemTopik slug="6" title="Profil" Icon={SmileIcon} />
                <ItemTopik
                  slug="7"
                  title="Pengajuan Kredit"
                  Icon={CardIcon}
                />
                <ItemTopik
                  slug="8"
                  title="Membeli Properti"
                  Icon={ShoppingBasketIcon}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <FaqPopuler data={data} />
    </Layout>
  );
}
