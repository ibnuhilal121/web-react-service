import { useRouter } from 'next/router'
import Layout from '../../components/Layout'
import Breadcrumb from '../../components/section/Breadcrumb'
import LikeIcon from '../../components/element/icons/LikeIcon';
import DislikeIcon from '../../components/element/icons/DislikeIcon';
import { useEffect, useState } from 'react';
import { detailFAQ } from '../../services/faq';
import parse from 'html-react-parser';
import LikeOutlineIcon from '../../components/element/icons/LikeOutlineIcon';
import DislikeOutlineIcon from '../../components/element/icons/DislikeOutlineIcon';
import { actionLike } from '../../services/faq';
import { useAppContext } from '../../context';

export default function index() {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();
    const { id } = router.query;
    const [data, setData] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const {userProfile, userKey} = useAppContext();

    const getData = () => {
        setLoaded(false);
        detailFAQ(id, userProfile?.id)
        .then(res => {
            if (res) {
                setData(res);
            };
        })
        .catch(error => {})
        .finally(() => {
            setLoaded(true);
        });
    }

    useEffect(() => {
        getData();
    }, []);

    const handleLike = () => {
        actionLike('like', id, userProfile.id)
        .then(res => {
            getData();
        })
        .catch(error => {})
    };

    const handleDislike = () => {
        actionLike('dislike', id, userProfile.id)
        .then(res => {
            getData();
        })
        .catch(error => {})
    };

    if (!loaded) {
        return (
            <div
                style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh",
                }}
            >
                <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
            </div>
        );
    }

    return (
        <Layout title="FAQ">
            <Breadcrumb active="FAQ"/>
            <section className="mb-5">
                <div className="container">
                    
                    <div className="row">
                        <div className="col-md-3 order-last order-md-first ">
                            <div className="sticky-md-top">
                                <div className="card card_faq_tanya">
                                    <h4>Kamu memiliki Pertanyaan lainnya?</h4>
                                    <div><a className="btn btn-main" onClick={() => router.push('/tools/konsultasi?org=faq')}>Tanyakan Sekarang</a></div>
                                </div>
                            </div>
                            
                        </div>
                        <div className="col-md-9 order-first order-md-last detail_faq_right">
                            <h5>{data[0].PERTANYAAN}</h5>
                            <div className="desc pb-3">
                                <p>{data[0].JAWABAN ? parse(data[0].JAWABAN) : ''}</p>
                            </div>
                            <hr/>
                            <div className="d-flex align-items-center mb-5 pt-3 justify-content-between justify-content-md-start">
                                <h6 className="mb-0">Apakah jawaban ini membantu?</h6>
                                <div className="row gx-3">
                                    <div className="col">
                                        <div 
                                            className="like_dislike" 
                                            onClick={(userKey && !data[0].LIKE) ? handleLike : null}
                                            style={(userKey && !data[0].LIKE) ? {cursor: 'pointer'} : {}}
                                        >
                                            {(data[0].LIKE && userKey) ?
                                            <LikeIcon /> : <LikeOutlineIcon />}
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div 
                                            className="like_dislike" 
                                            onClick={(userKey && !data[0].DISLIKE) ? handleDislike : null}
                                            style={(userKey && !data[0].DISLIKE) ? {cursor: 'pointer'} : {}}
                                        >
                                            {(data[0].DISLIKE && userKey) ?
                                            <DislikeIcon /> : <DislikeOutlineIcon />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    )
}
