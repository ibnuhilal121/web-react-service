import { useRouter } from "next/router";
import Link from "next/link";
import Layout from "../../../../components/Layout";
import BreadcrumbSecondary from "../../../../components/section/BreadcrumbSecondary";
import React, { useEffect, useState } from "react";
import Lottie from 'react-lottie'
import * as animationData from "../../../../public/animate_home.json"
import { appLogin } from "../../../../services/master";
import { defaultHeaders } from "../../../../utils/defaultHeaders";

export default function Subscribe() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  const [text, setText] = useState("");
  const [subscribe, setSubscribe] = useState({});
  const [loadingDetails, setLoadingDetails] = useState(false);
  const [email, setEmail] = useState("")
  const [code,setCode] = useState(null);
  const router = useRouter();


  // const getDataSubs = async () => {
  //   try {
  //     const keyResponse = await appLogin();
  //     if (!keyResponse.IsError) {
  //       localStorage.setItem("accessKey", keyResponse.AccessKey);
  //     } else {
  //       throw keyResponse;
  //     }
  //     const payload = {
  //       id: id
  //     };
  //     const dataSubs = await fetch(
  //       `${process.env.NEXT_PUBLIC_API_HOST}/newsletter/show`,
  //       {
  //         method: "POST",
  //         headers: {
  //           "Access-Control-Allow-Origin": "*",
  //           "Content-Type": "application/x-www-form-urlencoded",
  //           AccessKey_App: keyResponse.AccessKey,
  //         },
  //         body: new URLSearchParams(payload),
  //       }
  //     );
  //     const responseSubs = await dataSubs.json();
  //     if (responseSubs.IsError) {
  //       throw responseSubs.ErrToUser;
  //     } else {
  //       setEmail(responseSubs.Data[0].e);
  //       if (responseSubs.Data[0].st == 1) {
  //         setText("Anda telah berhasil berlangganan newsletter untuk menghentikan langganan klik di sini")
  //       } else {
  //         setText("Anda telah berhasil berhenti berlangganan newsletter untuk berlangganan klik di sini")
  //       }
  //       console.log("ini data subs", responseSubs.Data[0])
  //     }
  //     // ========== Get Data Subs ==============
  //   } catch (error) {
  //     console.log("error get data : ", error);
  //   } finally {

  //   }
  // };

  const goSubscribe = async (id) => {
    try {
      const keyResponse = await appLogin();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw keyResponse;
      }
      const payload = {
        id: id.split(".")[0],
        st: 0
      };
      const dataSubs = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/newsletter/update/status`,
        {
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            AccessKey_App: keyResponse.AccessKey,
            ...defaultHeaders
          },
          body: new URLSearchParams(payload),
        }
      );
      const responseSubs = await dataSubs.json();
      if (responseSubs.IsError) {
        throw responseSubs.ErrToUser;
      } else {
        console.log("response update status", responseSubs)
      }
      // ========== Get Data Subs ==============
    } catch (error) {
      console.log("error get data : ", error);
    } finally {

    }
  };

  useEffect(() => {
    if(router.isReady){
      goSubscribe(router.query.id);
      setCode(router.query.id);
    }
    goSubscribe();
  }, [])
  return (
    <Layout title="Newsletter" isLoaderOpen={loadingDetails}>
      <BreadcrumbSecondary active="Newsletter">
      </BreadcrumbSecondary>
      <div className='aktivasi-member-container'>
        <div className="aktivasi-member-wrapper" style={{ maxWidth: "90%" }}>
          <div style={{ display: 'flex', flexDirection: "row" }} className="aktivasi-member-flex">
            <div className='col-md-5'>
              <Lottie
                options={defaultOptions}
                height={300}
                isStopped={false}
                isPaused={false}
                style={{ margin: 0, width: "100%" }}
              />
            </div>
            <div className='col-md-6 pt-5'>
              <div className="aktivasi-member-text-group text-start">
                <h1 className="aktivasi-member-title">Subscribe Newsletter</h1>
                <p className="aktivasi-member-text" style={{ fontFamily: "Helvetica", fontSize: "14px" }}>
                  Anda telah berhasil berhenti berlangganan newsletter untuk berlangganan klik
                  <Link href={`/newsletter/konfigurasi/subscribe/${code}`}> di sini </Link>
                </p>
              </div>
            </div>

          </div>
        </div>

      </div>
    </Layout>
  )
}