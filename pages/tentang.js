import SlideSejarah from "../components/content/SlideSejarah";
import Layout from "../components/Layout";
import BannerDownload from "../components/section/BannerDownload";
import Breadcrumb from "../components/section/Breadcrumb";
import Slidemitra from "../components/section/SlideMitra";
import SlideTestimoni from "../components/section/SlideTestimoni";
import Link from "next/link";
import getHeaderWithAccessKey from "../utils/getHeaderWithAccessKey";
import React, { useEffect, useState } from "react";
import SejarahSlide from "../components/carousel/SejarahSlide";
import { defaultHeaders } from "../utils/defaultHeaders";

export default function index() {
  const [tentang, setTentang] = useState("");
  const [loading, setLoading] = useState(true);
  const [step, setStep] = useState(0);

  useEffect(() => {
    getTentangData();
    getTestimoni();
    getDeveloperData();
  }, []);

  /* ------------------------------ TENTANG DATA ------------------------------ */
  const getTentangData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/tentang/show`;

      const res = await fetch(endpoint, {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
      });

      setLoading(true);
      const resData = await res.json();
      if (!resData.IsError) {
        console.log("GET DATA", resData.data);
        setTentang(resData.data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      // console.log(error.message);
    } finally {
      setLoading(false);
    }
  };


  /* ---------------------------- MITRA & DEVELOPER --------------------------- */
  const [developerData, setDeveloperData] = useState([]);

  const getDeveloperData = async () => {
    try {
      const params = `Page=1&Limit=18&Sort=1`;
      const endpoint =
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/developer/show?` + params;
      const res = await fetch(endpoint, {
        method: "GET",
        headers: {
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET
        },
      });
      const resData = await res.json();
      if (!resData.data)
        throw { message: "Maaf Keyword Yang Kamu Cari Tidak ditemukan" };
      if (!resData.IsError) {
        console.log("GET DATA", resData);
        setDeveloperData(resData.data)
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      // console.log(error.message);
    } finally {
      // setIsFetching(false);
    }
  };


  /* -------------------------------- TESTIMONI ------------------------------- */
  const [testimoniData, setTestimoniData] = useState([]);

  const getTestimoni = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/testimoni/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_App: localStorage.getItem("accessKey"),
          ...defaultHeaders
        },
        body: new URLSearchParams({
          is_pblsh: 1,
          Limit: 4
        }),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        console.log("SDFSDFSDF", resData.Data);
        setTestimoniData(resData.Data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      // console.log(error.message);
    } finally {
      // setSendMessage(false);
    }
  };


  return (
    <Layout title="Tentang" isLoaderOpen={loading}>
      <Breadcrumb active="Tentang" />
      <section id="tentang">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-lg-6 tentang_left">
              <h4 className="title">Tentang BTN Properti</h4>
              <p>
                {/* {" "}
                <Link href="/">
                  <strong>BTN Properti.co.id</strong>
                </Link>{" "} */}
                {tentang.deskripsi}
              </p>
              <div className="row">
                <div className="col-md-4" style={{ marginBottom: '16px' }}>
                  <div className="card card_stitistik">
                    <div className="jumlah mb-3 d-flex align-items-center">
                      <img
                        src="/images/icons/tentang_developer.png"
                        className="img-fluid"
                      />
                      <h3 className="ms-2 mb-0 overflow-hidden">{tentang.developer_jumlah}</h3>
                    </div>
                    <h4>List Developer</h4>
                    <p>
                      {tentang.developer}
                    </p>
                  </div>
                </div>
                <div className="col-md-4" style={{ marginBottom: '16px' }}>
                  <div className="card card_stitistik">
                    <div className="jumlah mb-3 d-flex align-items-center">
                      <img
                        src="/images/icons/tentang_rumah.png"
                        className="img-fluid"
                      />
                      <h3 className="ms-2 mb-0 overflow-hidden">{tentang.proyek_jumlah}</h3>
                    </div>
                    <h4>Proyek Perumahan</h4>
                    <p>
                      {tentang.proyek}
                    </p>
                  </div>
                </div>
                <div className="col-md-4" style={{ marginBottom: '16px' }}>
                  <div className="card card_stitistik">
                    <div className="jumlah mb-3 d-flex align-items-center">
                      <img
                        src="/images/icons/tentang_unit.png"
                        className="img-fluid"
                      />
                      <h3 className="ms-2 mb-0 overflow-hidden">{tentang.tipe_jumlah}</h3>
                    </div>
                    <h4>Tipe Unit</h4>
                    <p>
                      {tentang.tipe}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-6" style={{ marginBottom: '16px' }}>
              <img src="/images/bg/tentang.png" className="img-fluid w-100" />
            </div>
          </div>
        </div>
      </section>
      <section id="visi_misi">
        <div className="container">
          <div className="row">
            <div className="col-md-6 mb-2 mb-lg-0">
              <div className="card">
                <div className="card-header d-flex align-items-center">
                  <img
                    src="/images/icons/tentang_visi.png"
                    className="img-fluid"
                  />
                  <h5 className="mb-0">Visi</h5>
                </div>
                <div className="card-body">
                  {tentang.visi}
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="card">
                <div className="card-header d-flex align-items-center">
                  <img
                    src="/images/icons/tentang_misi.png"
                    className="img-fluid"
                  />
                  <h5 className="mb-0">Misi</h5>
                </div>
                <div className="card-body">
                  {tentang.misi}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="sejarah">
        <div className="container">
          <h4
            className="text-center"
            style={{
              fontFamily: "FuturaBT",
              fontWeight: "700",
              fontSize: "32px",
            }}
          >
            Sejarah Singkat BTN Properti
          </h4>
          <div className="list_sejarah mb-5">
            <SejarahSlide data={tentang.sejarah} step={step} setStep={setStep} />
            <SlideSejarah data={tentang.sejarah} step={step} setStep={setStep} />
          </div>
        </div>
      </section>
      <section id="mitra">
        <div className="container">
          <h4
            className="title"
            style={{
              fontFamily: "FuturaBT",
              fontWeight: "700",
              fontSize: "32px",
            }}
          >
            Developer dan Mitra <br /> BTN Properti
          </h4>
          <div className="col-md-10 offset-md-1 slide_new_custom">
            <Slidemitra data={developerData} />
          </div>
        </div>
      </section>
      {testimoniData && (
        <section id="testimoni">
          <div className="container">
            <div className="col-md-10 offset-md-1 text-center">
              <h4 className="title">Testimoni</h4>
              <p>BTN Properti telah membantu banyak keluarga Indonesia mewujudkan hunian impian mereka.<br />
                Yuk cek apa kata mereka di sini!
              </p>
            </div>
            <div className="col-md-8 offset-md-2 py-3">
              <SlideTestimoni data={testimoniData} />
            </div>
          </div>
        </section>
      )}
      <BannerDownload />
    </Layout>
  );
}

const renderHTML = (rawHTML) =>
  React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });
