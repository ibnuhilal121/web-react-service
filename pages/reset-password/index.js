import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react'
import ResetPasswordNew from '../../components/auth/ResetPasswordNew'
import Layout from '../../components/Layout'
import BreadcrumbSecondary from "../../components/section/BreadcrumbSecondary";
import Hero from '../../components/static/Hero'

function index() {
  const router = useRouter();
  const [code,setCode] = useState(null);

  useEffect(()=>{
    if(router.isReady){
      setCode(router.query.id);
    }
  },[router.isReady])

  return (
    <Layout title="ubah password">
       <BreadcrumbSecondary active="Ubah Password"  />
       {code && <ResetPasswordNew title={"Ubah Password"}  subTitle={"Silakan ganti password kamu di sini"} code={code.split(".")[0]} />}
    </Layout>
  )
}

export default index