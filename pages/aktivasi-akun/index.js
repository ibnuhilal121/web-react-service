import React, { useEffect, useState, useRef } from "react";
import Layout from "../../components/Layout";
import { passwordNonSpecialChar } from "../../helpers/regex";
import { useMediaQuery } from "react-responsive";
import styles from './index.module.css';
import Lottie from "react-lottie";
import * as animationData from "../../public/animate_home.json";

export default function AktivasiAkun() {
  const isTablet = useMediaQuery({ query: `(max-width: 1000px)`})
  const isMobile = useMediaQuery({ query: `(max-width: 768px)`})
  const tinyMobile = useMediaQuery({ query: `(max-width: 428px)`})
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [isPasswordInvalid, setPasswordInvalid] = useState(false)
  const [msgPasswordInvalid, setMsgPasswordInvalid] = useState('')
  const [isPasswordConfirmed, setPasswordConfirmed] = useState(true)
  const btnModalSukses = useRef()

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const validatePassword = () => {
    if (!password) {
      setPasswordInvalid(true)
      setMsgPasswordInvalid('Isian tidak boleh kosong')
      return false
    } else {
      if (!passwordNonSpecialChar.test(password)) {
        setPasswordInvalid(true)
        setMsgPasswordInvalid('Password minimal 8 karakter berupa alphanumerik serta huruf besar dan huruf kecil')
        return false
      } else {
        setPasswordInvalid(false)
        setMsgPasswordInvalid('')
        return true
      }    
    }
  }

  const validateConfirmPassword = () => {
    if (!confirmPassword || password !== confirmPassword) {
      setPasswordConfirmed(false)
      return false
    } else {
      setPasswordConfirmed(true)
      return true
    }
  }

  const submitAkun = (type) => {
    let isPasswordValid
    let isConfirmValid
    if (type == 'password') {
      isPasswordValid = validatePassword()
    } else if (type == 'confirm') {
      isConfirmValid = validateConfirmPassword()
    } else if (type == 'submit') {
      isPasswordValid = validatePassword()
      isConfirmValid = validateConfirmPassword()
      if (isPasswordValid && isConfirmValid) {
        console.log('~ submit aktivasi')
        btnModalSukses.current.click()

      }
    }
  }

  return (
    <Layout>
      <div 
        className={"container d-flex flex-column justify-content-center align-items-center " + `${styles.containerAktivasi}`}
      >
        <h5 className={`${styles.fontTitle}`}>
          Aktivasi Akun
        </h5>
        <p className={`${styles.fontDescription}`}>
          Setelah proses aktivasi selesai, Anda resmi menjadi member BTN Property
        </p>
        <div className={"floating-label-wrap " + `${isMobile ? styles.fullWidth : isTablet ? styles.thirdWidth : styles.halfWidth}`} style={{ marginTop: 20 }}>
          <input
            type="text"
            className={`floating-label-field ${styles.fontInput}`}
            placeholder="Nama Lengkap"
            style={{ backgroundColor: '#EDF4FF' }}
            value={"Contoh Nama"}
            disabled={true}
          />
          <label className={`floating-label ${styles.fontLabel}`}>
            Nama Lengkap :{" "}
          </label>
        </div>
        <div className={"floating-label-wrap " + `${isMobile ? styles.fullWidth : isTablet ? styles.thirdWidth : styles.halfWidth}`}>
          <input
            type="email"
            className={`floating-label-field ${styles.fontInput}`}
            placeholder="Email"
            style={{ backgroundColor: '#EDF4FF' }}
            value={"Contoh Email"}
            disabled={true}
          />
          <label className={`floating-label ${styles.fontLabel}`}>
            Email :{" "}
          </label>
        </div>
        <div className={"floating-label-wrap " + `${isMobile ? styles.fullWidth : isTablet ? styles.thirdWidth : styles.halfWidth}`}>
          <input
            type="text"
            className={`floating-label-field ${styles.fontInput}`}
            placeholder="No HP"
            style={{ backgroundColor: '#EDF4FF' }}
            value={"Contoh Nomor Handphone"}
            disabled={true}
          />
          <label className={`floating-label ${styles.fontLabel}`}>
            No HP :{" "}
          </label>
        </div>
        <div className={"floating-label-wrap " + `${isMobile ? styles.fullWidth : isTablet ? styles.thirdWidth : styles.halfWidth}`}>
          <input
            type="password"
            className={`floating-label-field ${styles.fontInput} ${isPasswordInvalid ? styles.colorError : ''}`}
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onBlur={() => submitAkun('password')}
            style={isPasswordInvalid ? { border: '1px solid #DE2925' } : {}}
          />
          <label className={`floating-label ${styles.fontLabel}`}>
            Password :{" "}
          </label>
          {isPasswordInvalid && (
            <div className={`error-input-kpr ${styles.fontError} ${styles.colorError}`}>
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              {msgPasswordInvalid}
            </div>
          )}
        </div>
        <div className={"floating-label-wrap " + `${isMobile ? styles.fullWidth : isTablet ? styles.thirdWidth : styles.halfWidth}`}>
          <input
            type="password"
            className={`floating-label-field ${styles.fontInput} ${!isPasswordConfirmed ? styles.colorError : ''}`}
            placeholder="Konfirmasi Password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            style={!isPasswordConfirmed ? { border: '1px solid #DE2925' } : {}}
          />
          <label className={`floating-label ${styles.fontLabel}`}>
            Konfirmasi Password :{" "}
          </label>
          {!isPasswordConfirmed && (
            <div className={`error-input-kpr ${styles.fontError} ${styles.colorError}`}>
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Konfirmasi password tidak sesuai
            </div>
          )}
        </div>
        <button
          className={"btn btn-main m-3 m-lg-0 " + `${isMobile ? styles.fullWidth : isTablet ? styles.thirdWidth : styles.halfWidth}`}
          style={{
            height: 48,
            padding: 0,
            fontFamily: "FuturaHvBt",
            fontSize: 14,
            fontWeight: 700,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: "200px",
          }}
          onClick={() => submitAkun('submit')}
          disabled={!password || !confirmPassword}
        >
          Simpan
        </button>
      </div>

      <div
        className="modal fade"
        id="modalSuccessActivation"
        aria-hidden="true"
        aria-labelledby="modalSuccessActivation"
        tabIndex="-1"
      >
        <div className="modal-dialog" style={tinyMobile ? { marginTop: '100px' } : {}}>
          <div
            className="modal-content"
            style={{
              maxWidth: "600px",
              minHeight: "576px",
              borderRadius: "8px",
            }}
          >
            <div className="modal-body d-flex justify-content-center align-items-center">
              <div className="text-center">
                <h4 className={`${styles.fontTitle} ${styles.titleModal}`} style={{ fontWeight: 'bold' }}>Terima Kasih</h4>
                <div className={`${styles.subtitleModal}`}>
                  Aktivasi Akun <span style={{ color: '#0061a7' }}>Berhasil</span>
                </div>
                <div
                  className="desc text-center"
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: "12px",
                    fontWeight: 400,
                    color: "#666666",
                    marginBottom: "20px"
                  }}
                >
                  Selamat! Aktivasi akun Anda berhasil. Anda telah menjadi member BTN Properti.
                  <br />
                  Anda bisa melakukan login ke area member BTN Properti.
                </div>
                <Lottie
                  options={defaultOptions}
                  height={250}
                  width={350}
                  isStopped={false}
                  isPaused={false}
                />
                <button
                  className={"btn btn-main " + `${styles.fullWidth}`}
                  style={{
                    height: 48,
                    padding: 0,
                    fontFamily: "Helvetica",
                    fontSize: 14,
                    fontWeight: 700,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: "200px",
                    marginTop: "20px"
                  }}
                  data-bs-dismiss="modal"
                  data-bs-target="#modalSuccessActivation"
                >
                  Oke
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <button
        style={{ display: "none" }}
        data-bs-target="#modalSuccessActivation"
        data-bs-toggle="modal"
        ref={btnModalSukses}
      >
        Modal Button
      </button>
    </Layout>
  )
}