import React, { useEffect } from 'react'
import { useRouter } from 'next/router'

export default function index() {
    const router = useRouter()
    const {asPath} = router
    const splitUrl = asPath.split("/")

    console.log({asPath});

    useEffect(() => {
        if(router.isReady) {
            if(splitUrl[1] == "reset-password") {
                router.push("/reset-password/?id="+ splitUrl[2])
            }
            else if(splitUrl[1] == "konfirmasi-pendaftaran") {
                router.push("/konfirmasi-pendaftaran/?id="+ splitUrl[2])
            }
            else if(splitUrl[1] == "newsletter") {
                if(splitUrl[3] == "subscribe") {
                    router.push("/newsletter/konfigurasi/subscribe/?id="+ splitUrl[4])
                }
                else if(splitUrl[3] == "unsubscribe") {
                    router.push("/newsletter/konfigurasi/unsubscribe/?id="+ splitUrl[4])
                }
            }
        }
    }, [])

  return (
    <div
    style={{
      color: "rgb(0, 0, 0)",
      background: "rgb(255, 255, 255)",
      fontFamily:
        '-apple-system, BlinkMacSystemFont, Roboto, "Segoe UI", "Fira Sans", Avenir, "Helvetica Neue", "Lucida Grande", sans-serif',
      height: "100vh",
      textAlign: "center",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    <div>
      <style dangerouslySetInnerHTML={{ __html: "body { margin: 0 }" }} />
      <h1
        style={{
          display: "inline-block",
          borderRight: "1px solid rgba(0, 0, 0, 0.3)",
          margin: "0px 20px 0px 0px",
          padding: "10px 23px 10px 0px",
          fontSize: 24,
          fontWeight: 500,
          verticalAlign: "top"
        }}
      >
        404
      </h1>
      <div
        style={{
          display: "inline-block",
          textAlign: "left",
          lineHeight: 49,
          height: 49,
          verticalAlign: "middle"
        }}
      >
        <h2
          style={{
            fontSize: 14,
            fontWeight: "normal",
            lineHeight: 4,
            margin: 0,
            padding: 0
          }}
        >
          This page could not be found.
        </h2>
      </div>
    </div>
  </div>
  )
}
