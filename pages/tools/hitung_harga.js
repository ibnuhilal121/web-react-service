import { useState } from "react";
import NumberFormat from "react-number-format";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import { hitungHargaMax } from "../../services/hitungHargaMax";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function HitungHarga() {
  const router = useRouter();

  const [complete, setComplete] = useState(0);
  const [penghasilanTotal, setPenghasilan] = useState(null);
  const [pengeluaran, setPengeluaran] = useState(null);
  const [waktu, setWaktu] = useState("");
  const [isCalculating, setCalculating] = useState(false);
  const [maxPrice, setMaxPrice] = useState(null);
  const [area, setArea] = useState("");

  console.log(penghasilanTotal);
  console.log(pengeluaran);

  const cekHarga = (event) => {
    event.preventDefault();
    setCalculating(true);
    hitungHargaMax(penghasilanTotal.slice(3).replaceAll(".", ""), pengeluaran.slice(3).replaceAll(".", ""), waktu)
      .then((res) => {
        if (res.status) {
          setMaxPrice(res.data);
        } else {
          throw res;
        }
      })
      .catch((error) => {})
      .finally(() => {
        setCalculating(false);
        setComplete(1);
      });
  };
  return (
    <Layout title="Hitung Harga" isLoaderOpen={isCalculating}>
      <Breadcrumb active="Hitung Harga" />
      <section id="hitung_harga" className="mb-5">
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <div className="text-center">
                <h4 id="title_hitung_harga" className="title_page mb-0" style={{ fontFamily: "FuturaBt" }}>
                  Hitung Harga Properti Maksimal
                </h4>
                <img id="harga-img-mobile" src="/images/acc/hitung_harga.png" className="img-fluid" />
                <p
                  id="paragraf-hitungharga"
                  style={{
                    fontFamily: "Helvetica",
                    color: "#666666",
                    fontSize: "16px",
                    marginTop: "16px",
                  }}
                >
                  Masukkan penghasilan, cicilan, dan jangka waktu untuk mengetahui harga properti ideal bagi kamu.
                </p>
              </div>

              <div className="card card_hitung_harga">
                <div className="card-body p-0 ">
                  <div className="row d-md-flex align-items-center">
                    <div id="harga_image_on" className="col-md-6 pb-0 ">
                      <div className="text-center">
                        <img src="/images/acc/hitung_harga.png" className="img-fluid" />
                        <p
                          className="mt-3"
                          id="paragraf-hitungharga-on"
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: "400",
                            lineHeight: "22.4px",
                            color: "#00193E",
                            fontSize: "14px",
                          }}
                        >
                          Masukkan penghasilan, cicilan, dan jangka waktu untuk mengetahui harga properti ideal bagi kamu.
                        </p>
                        <p id="ket_hitung_harga" className="mt-3">
                          Lengkapi setiap field yang ada di samping dan klik hitung.
                        </p>
                      </div>
                    </div>
                    <div
                      className="col-md-6 p-1 py-md-4"
                      style={{
                        borderRight: "1px solid #eeeeee",
                      }}
                    >
                      <form
                      id="form_hitung_harga"
                        className="row g-3"
                        onSubmit={cekHarga}
                        style={{
                          padding: "36px 10px 38px 30px",
                        }}
                      >
                        <div className="col-md-12 " style={{}}>
                          <div
                            className="floating-label-wrap"
                            style={{
                              marginBottom: "32px",
                            }}
                          >
                            <NumberFormat
                              allowNegative={false}
                              thousandSeparator="."
                              decimalSeparator=","
                              className="floating-label-field"
                              placeholder="Penghasilan Total"
                              prefix="      "
                              type="text"
                              value={penghasilanTotal}
                              decimalScale={2}
                              onChange={(e) => setPenghasilan(e.target.value)}
                            />
                            <label htmlFor="penghasilan" className="floating-label">
                              Penghasilan Total
                              <span
                                style={{
                                  fontSize: "80%",
                                  fontWeight: 300,
                                  marginLeft: "5px",
                                }}
                              >
                                Per Bulan
                              </span>
                            </label>
                            {penghasilanTotal &&
                              (penghasilanTotal.includes("-") ? (
                                <p
                                  style={{
                                    height: "0px",
                                    position: "relative",
                                    bottom: "34px",
                                    right: "0px",
                                    marginLeft: "16px",
                                    fontFamily: "Helvetica",
                                    fontWeight: 700,
                                    color: "#00193e",
                                  }}
                                >
                                  -Rp
                                </p>
                              ) : (
                                <p
                                  style={{
                                    height: "0px",
                                    position: "relative",
                                    bottom: "34px",
                                    right: "0px",
                                    marginLeft: "16px",
                                    fontFamily: "Helvetica",
                                    fontWeight: 700,
                                    color: "#00193e",
                                  }}
                                >
                                  Rp
                                </p>
                              ))}
                          </div>
                          <div
                            className="floating-label-wrap"
                            style={{
                              marginBottom: "32px",
                            }}
                          >
                            <NumberFormat
                              allowNegative={false}
                              thousandSeparator="."
                              decimalSeparator=","
                              decimalScale={2}
                              className="floating-label-field"
                              placeholder="Pengeluaran"
                              prefix="      "
                              value={pengeluaran}
                              onChange={(e) => setPengeluaran(e.target.value)}
                            />
                            <label htmlFor="pengeluaran" className="floating-label">
                              Pengeluaran
                              <span
                                style={{
                                  fontSize: "80%",
                                  fontWeight: 300,
                                  marginLeft: "5px",
                                }}
                              >
                                Per Bulan
                              </span>
                            </label>
                            {pengeluaran &&
                              (pengeluaran.includes("-") ? (
                                <p
                                  style={{
                                    height: "0px",
                                    position: "relative",
                                    bottom: "34px",
                                    right: "0px",
                                    marginLeft: "16px",
                                    fontFamily: "Helvetica",
                                    fontWeight: 700,
                                    color: "#00193e",
                                  }}
                                >
                                  -Rp
                                </p>
                              ) : (
                                <p
                                  style={{
                                    height: "0px",
                                    position: "relative",
                                    bottom: "34px",
                                    right: "0px",
                                    marginLeft: "16px",
                                    fontFamily: "Helvetica",
                                    fontWeight: 700,
                                    color: "#00193e",
                                  }}
                                >
                                  Rp
                                </p>
                              ))}
                          </div>

                          {pengeluaran != 0 && penghasilanTotal != 0 && Number(pengeluaran?.replaceAll(".", "").substr(3)) >= Number(penghasilanTotal?.replaceAll(".", "").substr(3)) && (
                            <p
                              style={{
                                position: "relative",
                                bottom: "30px",
                                height: "0px",
                                fontFamily: "Helvetica",
                                fontWeight: 400,
                                color: "red",
                                marginBottom: "0px",
                              }}
                            >
                              Isi kurang dari nilai sebelumnya
                            </p>
                          )}
                          <div
                            className="floating-label-wrap"
                            style={{
                              marginBottom: "40px",
                              marginTop: "12px",
                            }}
                          >
                            <select id="waktu" className="floating-label-select custom-select-profile form-select " value={waktu} onChange={(e) => setWaktu(e.target.value)} required>
                              <option value="">Jangka Waktu</option>
                              <option value="1">1 Tahun</option>
                              <option value="2">2 Tahun</option>
                              <option value="3">3 Tahun</option>
                              <option value="4">4 Tahun</option>
                              <option value="5">5 Tahun</option>
                              <option value="6">6 Tahun</option>
                              <option value="7">7 Tahun</option>
                              <option value="8">8 Tahun</option>
                              <option value="9">9 Tahun</option>
                              <option value="10">10 Tahun</option>
                              <option value="11">11 Tahun</option>
                              <option value="12">12 Tahun</option>
                              <option value="13">13 Tahun</option>
                              <option value="14">14 Tahun</option>
                              <option value="15">15 Tahun</option>
                              <option value="16">16 Tahun</option>
                              <option value="17">17 Tahun</option>
                              <option value="18">18 Tahun</option>
                              <option value="19">19 Tahun</option>
                              <option value="20">20 Tahun</option>
                              <option value="21">21 Tahun</option>
                              <option value="22">22 Tahun</option>
                              <option value="23">23 Tahun</option>
                              <option value="24">24 Tahun</option>
                              <option value="25">25 Tahun</option>
                              <option value="26">26 Tahun</option>
                              <option value="27">27 Tahun</option>
                              <option value="28">28 Tahun</option>
                              <option value="29">29 Tahun</option>
                              <option value="30">30 Tahun</option>
                            </select>
                            <label htmlFor="waktu" className="floating-label">
                              Jangka Waktu
                            </label>
                          </div>
                        </div>

                        <div id="hitung_harga_button" className="col-12 col-md-6 p-0" style={{ width: "250px", height: "52px" }}>
                          <button
                            type="submit"
                            className="btn btn-main px-5 d-block w-100"
                            style={{
                              fontFamily: "Helvetica",
                              fontWeight: 700,
                              fontSize: "16px",
                              height: "52px",
                            }}
                            disabled={!penghasilanTotal || !pengeluaran || !waktu || Number(pengeluaran?.replaceAll(".", "").substr(3)) >= Number(penghasilanTotal?.replaceAll(".", "").substr(3))}
                          >
                            {complete == "1" ? "Hitung Ulang" : "Hitung"}
                          </button>
                        </div>
                      </form>
                    </div>
                    <div className="col-md-6 p-1 p-md-4">
                      {complete == "0" && (
                        <div id="harga_image" className="text-center">
                          <img id="harga-img" src="/images/acc/hitung_harga.png" className="img-fluid" />
                          <p id="ket_hitung_harga" className="mt-3">Lengkapi setiap field yang ada di samping dan klik hitung.</p>
                        </div>
                      )}
                      {complete == "1" && (
                        <div id="harga_hasil" className="d-flex justify-content-center flex-column">
                          <h5 style={{ fontWeight: 700 }}>Harga Properti Maksimal Kamu</h5>
                          <h3 style={{ fontWeight: 700 }}>Rp {maxPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</h3>
                          <p
                            style={{
                              fontFamily: "Helvetica",
                              fontWeight: "400",
                              marginBottom: "32px",
                            }}
                          >
                            Langsung cari di area yang kamu inginkan
                          </p>
                          <div className="floating-label-wrap d-flex location-form">
                            <div
                              className="input-group text-center"
                              style={{
                                height: "48px",
                                margin: "auto",
                                color: "#666666",
                              }}
                            >
                              <input
                                type="text"
                                className="form-control border-end-0 floating-label-field "
                                placeholder="Lokasi"
                                aria-label="Lokasi"
                                aria-describedby="basic-addon2"
                                value={area}
                                onChange={(e) => setArea(e.target.value)}
                                style={{
                                  fontFamily: "Helvetica",
                                  fontSize: "14px",
                                  fontWeight: 400,
                                  borderTopLeftRadius: "8px",
                                  borderBottomLeftRadius: "8px",
                                  border: "1px solid #aaaaaa",
                                  color: "#666666",
                                  width:"1%"
                                }}
                              />
                              <label htmlFor="lokasi" className="floating-label text-start" style={{ marginTop: "-6px" }}>
                                Lokasi
                              </label>
                              <span
                                className="input-group-text bg-transparent border-start-0"
                                id="basic-addon2"
                                style={{
                                  height: "48px",
                                  borderBottomRightRadius: "8px",
                                  borderTopRightRadius: "8px",
                                  border: "1px solid #aaaaaa",
                                  cursor: "pointer",
                                }}
                                onClick={() => router.push(`/property/?budget=${maxPrice}&tab=perumahan&lokasi=${area}`)} 
                              >
                                <img src="/icons/icons/search.svg" alt="" />
                              </span>
                            </div>
                          </div>
                          {/* <div className="floating-label-wrap text-start mt-5">
                                                    <input type="search" className="floating-label-field" id="lokasi" placeholder="Lokasi" required/>
                                                    <label htmlFor="lokasi" className="floating-label">Lokasi</label>
                                                </div> */}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
