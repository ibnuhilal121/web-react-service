import React, { useState, useEffect } from "react";
import KonsultasiPertanyaan from "../../components/form/KonsultasiPertanyaan";
import KonsultasiSearch from "../../components/form/KonsultasiSearch";

import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import KonsultasiList from "../../components/section/KonsultasiList";
import useSWR from "swr";

export default function index({ consultations }) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);
  const [paging, setPaging] = useState(null);
  const limit = 3;
  const [page, setPage] = useState(1);
  const [startRange, setStartRange] = useState(1);
  const [endRange, setEndRange] = useState(null);
  const [category, setCategory] = useState("");
  const [keyword, setKeyword] = useState("");
  const [loadingKirim, setLoadingKirim] = useState(false);

  useEffect(() => {
    setLoading(true);
    getKonsultasi();
  }, [page]);

  const getKonsultasi = () => {
    
    fetch(
      `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/konsultasi/show?Page=${page}&Limit=${limit}&i_kat=${category}&kywd=${keyword}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          'X-Content-Type-Options': 'nosniff',
          'X-Frame-Options': 'SAMEORIGIN',
          'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
          'Permissions-Policy': 'geolocation=(), browsing-topics=()',
          'Referrer-Policy': 'origin-when-cross-origin'
        },
      }
    )
      .then(async (response) => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        return data;
      })
      .then((data) => {
        if (data.status) {
          setData(data.data);
          setPaging(data.pagging);
          if (!endRange) {
            if (data.pagging.JmlHalTotal > 10) {
              setEndRange(10);
            } else {
              setEndRange(data.pagging.JmlHalTotal);
            }
          } else {
            if (page > endRange) {
              setStartRange(page);
              if (data.pagging.JmlHalTotal > page + 10) {
                setEndRange(page + 9);
              }
            } else if (page < startRange) {
              setStartRange(page - 9);
              if (data.pagging.JmlHalTotal > page + 10) {
                setEndRange(page);
              }
            }
          }
        } else {
          setData([]);
          setPaging(null);
          setPage(1);
          setStartRange(1);
          setEndRange(null);
        }
      })
      .catch((error) => {
        setData([]);
        setPaging(null);
        setPage(1);
        setStartRange(1);
        setEndRange(null);
        console.log("error show konsultasi");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  if (loading) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout title="Konsultasi" isLoaderOpen={loadingKirim}>
      <Breadcrumb active="Konsultasi KPR" />
      <section className="mb-5">
        <div className="container">
          <h3 className="text-center title_page">Konsultasi</h3>
          <KonsultasiSearch
            keyword={keyword}
            setKeyword={setKeyword}
            category={category}
            setCategory={setCategory}
            getKonsultasi={getKonsultasi}
          />
          <KonsultasiPertanyaan setLoadingKirim={setLoadingKirim} />
          <KonsultasiList
            data={data}
            paging={paging}
            startRange={startRange}
            setPage={setPage}
            endRange={endRange}
            getKonsultasi={getKonsultasi}
          />
          {console.log("ini data konsultasi", data)}
        </div>
      </section>
    </Layout>
  );
}
