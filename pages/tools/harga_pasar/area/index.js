import { useRouter } from "next/router";
import Link from "next/link";
import ItemHargaPasar from "../../../../components/data/ItemHargaPasar";
import HargaPasarFilter from "../../../../components/form/HargaPasarFilter";
import Layout from "../../../../components/Layout";
import Breadcrumb from "../../../../components/section/Breadcrumb";
import data_all from "../../../../sample_data/data_harga_pasar";
import React, { useEffect, useState } from "react";
import getHeaderWithAccessKey from "../../../../utils/getHeaderWithAccessKey";
import PaginationNew from "../../../../components/data/PaginationNew";

export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  const { id, pg } = router.query;
  const [dataFilter, setDataFilter] = useState([]);
  const [paginationData, setPaginationData] = useState({});
  const [hargaPasar, setHargaPasar] = useState([]);
  const [isFetching, setIsFetching] = useState(false);

  const [reqBody, setReqBody] = useState({
    Page: 1,
    Limit: 9,
    sort: dataFilter.sort,
    bln: dataFilter.mounth,
    thn: dataFilter.year,
    ls_tnh_min: dataFilter.panjangTanah,
    ls_tnh_max: dataFilter.lebarTanah,
    ls_bgn_min: dataFilter.panjangBangunan,
    ls_bgn_max: dataFilter.lebarBangunan,
  });

  console.log({reqBody, pg});
  
  const sorting = (id)=>{
    if (dataFilter.sort){
      return dataFilter.sort
    }
    switch (id){
      case "termahal" :
      return "hrg DESC"
   
      case "termurah" :
      return "hrg ASC"
     
      case "terpopuler" :
      return "j_vw DESC"
      default:
        return dataFilter.sort
    }
  }

  const getHargaPasarData = async () => {
    try {
      
      setIsFetching(true);
      const params = `Page=${reqBody.Page}&Limit=10&Sort=${sorting(id)}&kywd=${""}&bln=${reqBody.bln}&thn=${reqBody.thn}&ls_tnh_min=${reqBody.ls_tnh_min}&ls_tnh_max=${reqBody.ls_tnh_max}&ls_bgn_min=${reqBody.ls_bgn_min}&ls_bgn_max=${reqBody.ls_bgn_max}
      `;
      const endpoint =
        `${process.env.NEXT_PUBLIC_API_HOST}/hargapasar/show?` + params;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({}),
      });
      const resData = await res.json();
      // if (!resData.data) throw { message: "Data tidak ditemukan" };
      if (!resData.IsError) {
        console.log("GET DATA", resData.Data);
        setHargaPasar(resData.Data);
        setPaginationData(resData.Paging);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setIsFetching(false);
      // setLoaded(true);
    }
  };

  useEffect(() => {
    if (router.isReady) {
      handleSearch();
    }
  }, [dataFilter]);
  useEffect(() => {
    if (router.isReady) {
      navigation();
    }
  }, [dataFilter]);
  useEffect(() => {
    if (router.isReady) {
      getHargaPasarData();
    }
  }, [reqBody]);

  const navigation = ()=>{
    switch(dataFilter.sort){
      case 'j_vw DESC' :
        router.push("/tools/harga_pasar/area/?id=terpopuler&pg=1")
        break;
      case "thn DESC":
          router.push("/tools/harga_pasar/area/?id=terbaru&pg=1")
          break;
      case "hrg ASC":
        router.push("/tools/harga_pasar/area/?id=termurah&pg=1")
        break;
      case "hrg DESC":
        router.push("/tools/harga_pasar/area/?id=termahal&pg=1")
        break;
     
      default: 
      router.push(`/tools/harga_pasar/area/?id=${id}&pg=${reqBody.Page}`)
    }
  }

  const handleSearch = (e) => {
    e?.preventDefault();
    setReqBody(() => ({
      Page: 1,
      Limit: 10,
      bln: dataFilter.mounth,
      thn: dataFilter.year,
      ls_tnh_min: dataFilter.panjangTanah,
      ls_tnh_max: dataFilter.lebarTanah,
      ls_bgn_min: dataFilter.panjangBangunan,
      ls_bgn_max: dataFilter.lebarBangunan,
    }));
  };
  useEffect(() => {
    if(router.isReady) {
      setReqBody({
        ...reqBody,
        Page: pg
      })
    }
  }, [])

  // const Sorting = hargaPasar.sort((a, b) => {
  //   return b.j_vw - a.j_vw;
  // });

  // console.log("ini id pagenya", Sorting);

  return (
    <Layout title={id ? id : "Harga Pasar" + " | BTN properti"}>
      <Breadcrumb active={id}>
        <li className="breadcrumb-item">
          <Link href="/tools/harga_pasar/?pg=1">Harga Pasar</Link>
        </li>
      </Breadcrumb>

      <section id="post_detail" className="mb-5">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <HargaPasarFilter setDataFilter={setDataFilter} id={id} />
            </div>
            <div className="col-md-9 list_harga_pasar">
              <h4 className="title">Harga Pasar {id}</h4>
              {hargaPasar.length !== 0 &&
                  !isFetching &&
                  hargaPasar.map((data, index) => (
                    <div key={index} className="col-md-12">
                      <ItemHargaPasar data={data} />
                    </div>
                  ))}

              {/* {id === "termurah"
                ? hargaPasar
                    .sort((a, b) => {
                      return a.hrg - b.hrg;
                    })
                    .map((data, index) => (
                      <div key={index} className="col-md-12">
                        <ItemHargaPasar data={data} />
                      </div>
                    ))
                : ""}

              {id === "terpopuler"
                ? hargaPasar
                    .sort((a, b) => {
                      return b.j_vw - a.j_vw;
                    })
                    .map((data, index) => (
                      <div key={index} className="col-md-12">
                        <ItemHargaPasar data={data} />
                      </div>
                    ))
                : ""} */}
                <PaginationNew
                  length={paginationData.JmlHalTotal}
                  current={Number(pg)}
                  onChangePage={(e) => {
                    setReqBody((prevData) => ({ ...prevData, Page: e }));
                    router.push(`/tools/harga_pasar/area/?id=${id}&pg=${e}`)
                    window.scrollTo(0, 0);
                  }}
                />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
