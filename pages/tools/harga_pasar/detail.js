import { useRouter } from "next/router";
import HargaPasarChart from "../../../components/content/HargaPasarChart";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import getHeaderWithAccessKey from "../../../utils/getHeaderWithAccessKey";
import NumberFormat from "react-number-format";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";


export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  const { id } = router.query ;
  const { dataId } = router.query;
  const [yearsData, setYearsData] = useState([]);
  const [hargaPasar, setHargaPasar] = useState([]);
  const [display, setDisplay] = useState(false);
  const [option, setOption] = useState([]);
  const [place, setPlace] = useState(`${id}`);
  const slice = place.split(",");
  
  console.log(dataId);

  console.log(slice[1]);

  const bulan = [
    { id: 1, value: "Januari" },
    { id: 2, value: "Februari" },
    { id: 3, value: "Maret" },
    { id: 4, value: "April" },
    { id: 5, value: "Mei" },
    { id: 6, value: "Juni" },
    { id: 7, value: "Juli" },
    { id: 8, value: "Agustus" },
    { id: 9, value: "September" },
    { id: 10, value: "Oktober" },
    { id: 11, value: "November" },
    { id: 12, value: "Desember" },
  ];

  console.log("ini data tahun", yearsData.thn0 / 10000000);
  const getHargaPasarData = async () => {

    const params = `Page=1&Limit=1&S&id=${dataId}`;
    console.log(params)
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/hargapasar/show?/` + params;

      const res = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({}),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        console.log("GET DATA", resData.Data);
        setHargaPasar(resData.Data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  const getyears3Data = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/hargapasar/show/statisticyear`;

      const res = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          n_kot: slice[0],
          n_kec: slice[1],
          n_kel: slice[2],
        }),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        // console.log("3 years statistic,", resData);
        console.log("GET DATA year", resData.Data[0]);
        setYearsData(resData.Data[0]);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  useEffect(() => {
    getHargaPasarData();
    console.log(getHargaPasarData)
    getyears3Data();
  }, []);



  return (
    <Layout title={id + " | BTN properti"}>
      <Breadcrumb active={id}>
        <li className="breadcrumb-item">
          <Link href="/tools/harga_pasar/?pg=1">Harga Pasar</Link>
        </li>
      </Breadcrumb>

      <section id="post_detail" className="mb-5">
        <div className="container">
          {hargaPasar
            .filter((item) => item.kywd === id)
            .map((item, idx) => (
              <>
                <div className="row">
                  <div className="col-md-3">
                    <div className="detail_harga_pasar">
                      <h4>{id}</h4>
                      <label>Periode</label>
                      <p>
                        {bulan
                          .filter((bulan) => bulan.id === item.bln)
                          .map((bulan) => bulan.value)}{" "}
                        {item.thn}
                      </p>
                      <label>Luas Tanah</label>
                      <p>
                        {item.ls_tnh}
                        m²
                      </p>
                      <label>Harga Tanah Per Meter</label>
                      <p>
                        <NumberFormat
                          value={item.nl_min_tnh}
                          displayType={"text"}
                          prefix={"Rp"}
                          thousandSeparator="."
                          decimalSeparator=","
                        />
                      </p>
                      <label>
                        Total Harga Tanah
                        <OverlayTrigger id={idx} placement="top" 
                         overlay={
                          <Tooltip id="tooltip-top" style={{position:'absolute'}}>
                            {
                              <div style={{maxWidth:200}}>
                                Total Harga Tanah adalah harga tanah per meter yang dikalikan dengan luas tanah
                              </div>
                            }
                          </Tooltip>
                        }
                        >
                          <img
                          style={{ marginLeft: "8px" }}
                          src="/icons/icons/Vector.svg"
                          data-bs-placement="top"
                         
                        />
                        </OverlayTrigger>

                      </label>
                      <p>
                        <NumberFormat
                          value={item.nl_min_tnh * item.ls_tnh}
                          displayType={"text"}
                          prefix={"Rp"}
                          thousandSeparator="."
                          decimalSeparator=","
                        />
                      </p>
                      <label>Luas Bangunan</label>
                      <p>
                        {item.ls_bgn}
                        m²
                      </p>
                      <label>Harga Bangunan Per Meter</label>
                      <p>
                        <NumberFormat
                          value={item.nl_min_bgn}
                          displayType={"text"}
                          prefix={"Rp"}
                          thousandSeparator="."
                          decimalSeparator=","
                        />{" "}
                      </p>
                      <label>
                      Harga Total Wajar Bangunan
                        <OverlayTrigger id={idx} placement="top" 
                         overlay={
                          <Tooltip id="tooltip-top" style={{position:'absolute'}}>
                            {
                              <div style={{maxWidth:200}}>
                         
                                Total Harga Bangunan adalah harga bangunan 
                                per meter yang dikalikan dengan luas bangunan
                              </div>
                            }
                          </Tooltip>
                        }
                        >
                          <img
                          style={{ marginLeft: "8px" }}
                          src="/icons/icons/Vector.svg"
                          data-bs-placement="top"
                         
                        />
                        </OverlayTrigger>
                      </label>
                      <p>
                        <NumberFormat
                          value={item.nl_wjr_bgn * item.ls_bgn}
                          displayType={"text"}
                          prefix={"Rp"}
                          thousandSeparator="."
                          decimalSeparator=","
                        />
                      </p>
                    </div>
                  </div>
                  <div className="col-md-9">
                    <div className="card card_grafik">
                      <div className="card-body">
                        <HargaPasarChart
                          th0={yearsData.thn0 / 1}
                          th1={yearsData.thn1 / 1}
                          th2={yearsData.thn2 / 1}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </>
            ))}
        </div>
      </section>
    </Layout>
  );
}
