import Pagination from "../../../components/data/Pagination";
import HargaPasarFilter from "../../../components/form/HargaPasarFilter";
import HargaPasarFilterModal from "../../../components/form/HargaPasarFilterModal";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import data_all from "../../../sample_data/data_harga_pasar";
import Link from "next/link";
import ItemHargaPasar from "../../../components/data/ItemHargaPasar";
import React, { useEffect, useState, useRef } from "react";
import getHeaderWithAccessKey from "../../../utils/getHeaderWithAccessKey";
import PaginationNew from "../../../components/data/PaginationNew";
import { useMediaQuery } from "react-responsive";
import router, { useRouter } from "next/router";
import NotFound from "../../../components/element/NotFound";

export default function index() {
  const { pg } = useRouter().query
  const [isFetching, setIsFetching] = useState(false);
  const [dataFilter, setDataFilter] = useState({sort:"thn DESC"});
  const [hargaPasar, setHargaPasar] = useState([]);
  const [filterItem, setFilterItem] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [search, setSearch] = useState("");
  const [display, setDisplay] = useState(false);
  const wrapperRef = useRef(null);
  const [filterActive, setFilterActive] = useState(false);
  const [paginationData, setPaginationData] = useState({});
  const [showFilter, setShowFilter] = useState(false);
  const [judulSort,setJudulSort] = useState("Terbaru")
  // const [urutkan, setUrutkan] = useState(1);
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 768px)` });
  const closeBtnMobile = useRef();

  const [reqBody, setReqBody] = useState({
    Page: 1,
    Limit: 9,
    sort: dataFilter.sort,
    bln: dataFilter.mounth,
    thn: dataFilter.year,
    ls_tnh_min: dataFilter.panjangTanah,
    ls_tnh_max: dataFilter.lebarTanah,
    ls_bgn_min: dataFilter.panjangBangunan,
    ls_bgn_max: dataFilter.lebarBangunan,
  });

  console.log("Pagination data",paginationData);
  console.log("req Body",reqBody);
  const getHargaPasarData = async () => {
    try {
      setIsFetching(true);
      const params = `Page=${reqBody.Page}&Limit=10&Sort=${reqBody.sort}&kywd=${search}&bln=${reqBody.bln}&thn=${reqBody.thn}&ls_tnh_min=${reqBody.ls_tnh_min}&ls_tnh_max=${reqBody.ls_tnh_max}&ls_bgn_min=${reqBody.ls_bgn_min}&ls_bgn_max=${reqBody.ls_bgn_max}
      `;

      const endpoint =
        `${process.env.NEXT_PUBLIC_API_HOST}/hargapasar/show?` + params;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        // body: new URLSearchParams(reqBody),
      });
      const resData = await res.json();
      // if (!resData.data) throw { message: "Data tidak ditemukan" };
      if (!resData.IsError) {
        console.log("GET DATA", resData.Data);
        setHargaPasar(resData.Data);
        console.log("Pagination", resData.Paging);
        setPaginationData(resData.Paging);
        closeBtnMobile?.current?.click();
        console.log(closeBtnMobile)
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
      setHargaPasar([]);
    } finally {
      setIsFetching(false);
      // setLoaded(true);
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      console.log("Enter Masuk");
      console.log("ini body yang masuk:", reqBody);
      handleSearch();
      // setReqBody(() => ({
      //   Page: 1,
      //   kywd: search,
      //   Limit: 10,
      //   sort: dataFilter.sort,
      //   bln: dataFilter.mounth,
      //   thn: dataFilter.year,
      //   ls_tnh_min: dataFilter.panjangTanah,
      //   ls_tnh_max: dataFilter.lebarTanah,
      //   ls_bgn_min: dataFilter.panjangBangunan,
      //   ls_bgn_max: dataFilter.lebarBangunan,
      // }));
    }
  };

  useEffect(() => {
    if(router.isReady) {
      setReqBody({
        ...reqBody,
        Page: pg || 1
      })
    }
  }, [])

  useEffect(() => {
    if(!isTabletOrMobile){
      // handleSearch();
    }
  }, [dataFilter]);

  useEffect(() => {
    getHargaPasarData();
    switch(dataFilter.sort) {
      case 'j_vw DESC' :
        setJudulSort("Relevan")
        break;
      case "thn DESC":
        setJudulSort("Terbaru")
        break;
      case "hrg ASC":
        setJudulSort("Termurah")
        break;
      case "hrg DESC":
        setJudulSort("Termahal")
        break;
      default: 
      setJudulSort("Terbaru")
    }
  
  }, [reqBody]);

  console.log("ini urutkan", dataFilter);
  // const handleChangeFilter = (e) => {
  //   if (keyword !== "") {
  //     const results = hargaPasar.filter((item) => {
  //       return item.kywd.toLowerCase().startsWith(keyword.toLowerCase());
  //     });

  //     setFilterItem(results);
  //   } else {
  //     setFilterItem(hargaPasar);
  //   }
  //   setSearch(keyword);
  // };

  // useEffect(() => {
  //   setKeyword("");
  // }, [filterItem]);

  const handleSearch = (e) => {
    e?.preventDefault();
    setReqBody(() => ({
      Page: 1,
      kywd: search,
      Limit: 10,
      sort: dataFilter.sort,
      bln: dataFilter.mounth,
      thn: dataFilter.year,
      ls_tnh_min: dataFilter.panjangTanah,
      ls_tnh_max: dataFilter.lebarTanah,
      ls_bgn_min: dataFilter.panjangBangunan,
      ls_bgn_max: dataFilter.lebarBangunan,
    }));
    router.push("/tools/harga_pasar/?pg=1")
  };

  return (
    <Layout title="Hitung Pasar">
      <Breadcrumb active="Harga Pasar" />
      <section className="mb-5">
        <div className="container" ref={wrapperRef}>
          <div className="d-flex flex-column flex-lg-row flex-md-row">
            <div style={{ display: "flex", justifyContent: "center" }}>
              <div
                className="filter-button"
                data-bs-toggle="modal"
                data-bs-target="#modalFilter"
              >
                <img src="/icons/icons/icon_filter.svg" className="img-fluid" />
                Filter
              </div>
            </div>
            <div id="#modal-filter-hrg" className="me-2" style={{ display: isTabletOrMobile ? "none" : "block" }}>
              <HargaPasarFilter setDataFilter={setDataFilter} />
            </div>
            <div className="me-2">
              <HargaPasarFilterModal
                setDataFilter={setDataFilter}
                onSearch={handleSearch}
                setReqBody={setReqBody}
                refBtn={closeBtnMobile}
              />
            </div>
            <div className="list_harga_pasar">
              <h4>Harga Pasar</h4>
              <p>
                Cari dan temukan harga pasaran atau harga rata-rata tanah dan
                bangunan di daerah yang kamu minati.
              </p>
              <div className="input-group search_property_top mb-4">
                <input
                  type="text"
                  className="form-control border-end-0"
                  placeholder="Cari Kota/Kabupaten, Kecamatan, Kelurahan"
                  onKeyPress={handleKeyPress}
                  aria-label=""
                  onChange={(e) => setSearch(e.target.value)}
                  value={search}
                  style={{
                    fontFamily: "Helvetica",
                    fontStyle: "normal",
                    fontWeight: "400",
                    fontSize: "16px",
                    lineHeight: "150%",
                    color: "#666666",
                  }}
                />
                <button
                  className="cari"
                  type="button"
                  onClick={handleSearch}
                  style={{ fontSize: "17.49px", marginLeft: "15px" }}
                >
                  <i className="bi bi-search"></i>
                </button>
              </div>
              {/* <form>
              </form> */}
              <div className="d-flex flex-column flex-lg-row">
                <div
                  className="me-0 me-lg-2"
                  id="card_filter_hrg"
                  //style={{ height: "170px" }}
                >
                  <Link href={{pathname: "/tools/harga_pasar/area", query: {id: "terpopuler", pg: 1}}}>
                    <div
                      className="item_area area-populer"
                      style={{ paddingTop: "20px" }}
                    >
                      <svg
                        width="52"
                        height="52"
                        viewBox="0 0 52 52"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          width="52"
                          height="52"
                          rx="26"
                          fill="#0061A7"
                          fillOpacity="0.2"
                        />
                        <path
                          d="M26 31.27L32.18 35L30.54 27.97L36 23.24L28.81 22.63L26 16L23.19 22.63L16 23.24L21.46 27.97L19.82 35L26 31.27Z"
                          fill="#0061A7"
                        />
                      </svg>
                      <h5
                        className="mb-0 area-populerharga"
                        style={{
                          fontFamily: "FuturaBT",
                          fontWeight: "700",
                          fontSize: "20px",
                        }}
                      >
                        Area Terpopuler
                      </h5>
                      <p className="mb-0 paragraf-populer">
                        Temukan harga pasar tanah dan bangunan di area yang
                        paling banyak dicari.
                      </p>
                    </div>
                  </Link>
                </div>
                <div
                  className="me-0 me-lg-2"
                  id="card_filter_hrg"
                  //style={{ height: "170px" }}
                >
                  <Link href={{pathname: "/tools/harga_pasar/area", query: {id: "termahal", pg: 1}}}>
                    <div
                      className="item_area termahal-area"
                      style={{ paddingTop: "20px" }}
                    >
                      <svg
                        width="52"
                        height="52"
                        viewBox="0 0 52 52"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          width="52"
                          height="52"
                          rx="26"
                          fill="#0061A7"
                          fillOpacity="0.2"
                        />
                        <path
                          d="M21.82 32.18H25.3L22.42 26.44C24.16 25.82 25.22 24.36 25.22 22.44C25.22 19.92 23.42 18 20.62 18H15V32.18H18.16V26.9H19.28L21.82 32.18ZM18.16 24.18V20.74H20.02C21.32 20.74 22.02 21.4 22.02 22.46C22.02 23.46 21.32 24.18 20.02 24.18H18.16Z"
                          fill="#0061A7"
                        />
                        <path
                          d="M30.3791 35.98V31.4C30.8391 31.96 31.7991 32.4 33.0791 32.4C35.8591 32.4 37.8191 30.22 37.8191 27.18C37.8191 24.2 36.0791 22 33.1991 22C31.7791 22 30.6991 22.62 30.2791 23.26V22.24H27.3391V35.98H30.3791ZM34.8391 27.2C34.8391 28.86 33.7791 29.68 32.5791 29.68C31.3991 29.68 30.3391 28.84 30.3391 27.2C30.3391 25.54 31.3991 24.74 32.5791 24.74C33.7791 24.74 34.8391 25.54 34.8391 27.2Z"
                          fill="#0061A7"
                        />
                      </svg>
                      <h5
                        className="mb-0 area-termahal"
                        style={{
                          fontFamily: "FuturaBT",
                          fontWeight: "700",
                          fontSize: "20px",
                        }}
                      >
                        Area Termahal
                      </h5>
                      <p className="mb-0 paragraf-termahal">
                        Temukan harga pasar tanah dan bangunan di area dengan
                        harga paling mahal.
                      </p>
                    </div>
                  </Link>
                </div>
                <div id="card_filter_hrg">
                  <Link href={{pathname: "/tools/harga_pasar/area", query: {id: "termurah", pg: 1}}}>
                    <div
                      className="item_area termurah-area"
                      style={{ paddingTop: "20px" }}
                    >
                      <svg
                        width="52"
                        height="52"
                        viewBox="0 0 52 52"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          width="52"
                          height="52"
                          rx="26"
                          fill="#0061A7"
                          fillOpacity="0.2"
                        />
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M33.535 20.2674C33.535 15.7087 29.8262 12 25.2675 12C20.7088 12 17 15.7087 17 20.2674C17 24.8261 20.7088 28.5349 25.2675 28.5349C29.8262 28.5349 33.535 24.8261 33.535 20.2674ZM23.414 23.0979H25.156L23.7143 20.2247C24.5853 19.9144 25.1159 19.1835 25.1159 18.2225C25.1159 16.9611 24.2149 16 22.8133 16H20V23.0979H21.5819V20.455H22.1425L23.414 23.0979ZM21.5819 19.0934V17.3715H22.5129C23.1637 17.3715 23.5141 17.7019 23.5141 18.2325C23.5141 18.733 23.1637 19.0934 22.5129 19.0934H21.5819ZM27.6984 25V22.7075C27.9287 22.9878 28.4093 23.208 29.05 23.208C30.4416 23.208 31.4227 22.1168 31.4227 20.5951C31.4227 19.1035 30.5517 18.0022 29.1101 18.0022C28.3993 18.0022 27.8586 18.3126 27.6484 18.6329V18.1224H26.1767V25H27.6984ZM29.931 20.6051C29.931 21.436 29.4004 21.8465 28.7997 21.8465C28.209 21.8465 27.6784 21.426 27.6784 20.6051C27.6784 19.7742 28.209 19.3738 28.7997 19.3738C29.4004 19.3738 29.931 19.7742 29.931 20.6051ZM31.4525 28.5352C31.4525 28.5352 31.4525 28.5352 31.4525 28.5352H31.4525V28.5352ZM31.4525 28.5352C29.8874 29.5707 28.013 30.1748 25.9999 30.1748C23.9868 30.1748 22.1124 29.5708 20.5473 28.5352V32.1259H17.7569V33.312L25.9999 39.9993L34.2429 33.312V32.1259H31.4525V28.5352Z"
                          fill="#0061A7"
                        />
                      </svg>
                      <h5
                        className="mb-0 area-termurah"
                        style={{
                          fontFamily: "FuturaBT",
                          fontWeight: "700",
                          fontSize: "20px",
                        }}
                      >
                        Area Termurah
                      </h5>
                      <p className="mb-0 paragraf-termurah">
                        Temukan harga pasar tanah dan bangunan di area dengan
                        harga paling murah.
                      </p>
                    </div>
                  </Link>
                </div>
              </div>
              <div className="row justify-content-center">
                <div className="col-12">
                  <h4 className="title">Harga Pasar {judulSort}</h4>
                </div>

                {hargaPasar.length !== 0 && !isFetching ? 
                  hargaPasar.map((data, index) => (
                    <div key={index} className="col-md-12">
                      <ItemHargaPasar data={data} />
                    </div>
                  )):
                  <div className="col-6 text-center">
                    <NotFound message="Maaf, belum ada area yang kamu cari"/>
                  </div>
                  }
              </div>
              {hargaPasar.length !== 0 && !isFetching && (
                <PaginationNew
                  length={paginationData.JmlHalTotal}
                  current={Number(reqBody.Page)}
                  onChangePage={(e) => {
                    setReqBody((prevData) => ({ ...prevData, Page: e }));
                    router.push('/tools/harga_pasar/?pg=' + e );
                    window.scrollTo(0, 0);
                  }}
                />
              )}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
