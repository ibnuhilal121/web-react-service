import React, {useEffect, useState, useRef} from "react";
import ItemComparasi from "../../components/data/ItemComparasi";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import WidgetKomparasi from "../../components/section/WidgetKomparasi";
import AddComparasi from "../../components/static/AddComparasi";
import data_comparasi from '../../sample_data/data_komparasi'
import Cookies from "js-cookie";
import { useAppContext } from '../../context';
import { useRouter } from "next/router";
import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import Slider from "react-slick";
import { useMediaQuery } from "react-responsive";
import Link from 'next/link';
import NumberFormat from 'react-number-format';
import CardComparasi from "../../components/CardComparasi";

export default function index() {
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
    const router = useRouter();
    const { userKey } = useAppContext();
    const { query } = router;
    const [isLoading, setIsLoading] = useState(false);
    const [dataValue, setDataValue] = useState(null);
    const [addValue, setAddValue] = useState(0);
    const [dataActive, setDataActive] = useState(0);
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const refSlider1 = useRef(null);
    const refSlider2 = useRef(null);
    const [showBar, setShowBar] = useState(false);
    const [refHeight ,setRefHeight] = useState({
        cluster         : 0,
        alamat          : 0,
        flexFasilitas   : 0,
        area            : 0,
    });
    const value = query.data

    const settingsUp = {
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        focusOnSelect: true,
        dots: true,
        afterChange: current => setDataActive(current)
    };

    const settingsDown = {
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        focusOnSelect: true,
        afterChange: current => setDataActive(current)
    };

    useEffect(() => {
        setNav1(refSlider1.current);
        setNav2(refSlider2.current);
    }, []);

    useEffect(() => {
        if (typeof window !== 'undefined') {
            window.addEventListener("scroll", () => {
                if (window.pageYOffset > 300) {
                    setShowBar(true);
                } else {
                    setShowBar(false);
                }
            });
        }
    }, [])

    useEffect(() => {
        console.log("value nya", value)
        if(value?.length > 4){
            ///handling one length array from query////
            let array = [];
            array.push(value);
                setAddValue(array.length);
        }else if(value?.length <= 4){
                setAddValue(value.length);
        } else {
            setAddValue(0);
        }
        submitCompare()
    },[value])

    const submitCompare = async () => {
        const payload = {
            AppKey: process.env.NEXT_PUBLIC_APP_KEY,
            AppSecret: process.env.NEXT_PUBLIC_APP_SECRET
          }
          // GET NEW ACCESS KEY
          const keyEndpoint = `${process.env.NEXT_PUBLIC_API_HOST}/app/login`;
          const accessKey = await fetch(keyEndpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Access-Control-Allow-Origin': '*',
                'X-Content-Type-Options': 'nosniff',
                'X-Frame-Options': 'SAMEORIGIN',
                'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
                'Permissions-Policy': 'geolocation=(), browsing-topics=()',
                'Referrer-Policy': 'origin-when-cross-origin'
            },
            body: new URLSearchParams(payload)
          });
          const keyResponse = await accessKey.json();
          if (!keyResponse.IsError) {
            localStorage.setItem("accessKey", keyResponse.AccessKey);
          } else {
            throw {
              message : keyResponse.ErrToUser
            };
          };
          // GET NEW ACCESS KEY
          
		try {
            setIsLoading(true);
            console.log("value compare ", value)
            const payload = {
                lst_id: value,
                Sort: 'n ASC'
            };
			//e.preventDefault();
			console.log("payload",payload)
			const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/unit/compare`;
			const submitData = await fetch(endpoint, {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
					AccessKey_App: keyResponse.AccessKey,
                    'X-Content-Type-Options': 'nosniff',
                    'X-Frame-Options': 'SAMEORIGIN',
                    'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
                    'Permissions-Policy': 'geolocation=(), browsing-topics=()',
                    'Referrer-Policy': 'origin-when-cross-origin'
				},
                body: new URLSearchParams(payload)
			});
			const response = await submitData.json();
			console.log("Response",response)
			if (!response.IsError) {
                setIsLoading(false);
				console.log("Success", response.Data);
                setDataValue(response.Data)
				//window.location.reload();
			} else {
                setIsLoading(false);
				throw {
				  message: response.ErrToUser
				};
			};
		} catch (error) {
			alert(error.message);
            setIsLoading(false);
		} finally {
			// setLoaded(true);
		}
	}

    const initRefHeight = (key, value) => {
        if (key == "cluster" && value > refHeight.cluster){
            setRefHeight(previousInputs => ({ ...previousInputs, [key]: value}))
        } else if (key == "alamat" && value > refHeight.alamat){
            setRefHeight(previousInputs => ({ ...previousInputs, [key]: value}))
        } else if (key == "flexFasilitas" && value > refHeight.flexFasilitas){
            setRefHeight(previousInputs => ({ ...previousInputs, [key]: value}))
        } else if (key == "area" && value > refHeight.area){
            setRefHeight(previousInputs => ({ ...previousInputs, [key]: value}))
        }
    }

    return (
        <Layout title="Daftar Perbandingan | BTN Properti" isLoaderOpen={isLoading}>
            <Breadcrumb active="Komparasi"/>
            <section className="mb-5">
                <div className="container">
                    <h5 className="title_page text-center mb-5">Komparasi</h5>
                    {isTabletOrMobile ?
                    (
                        <div className="d-flex flex-column">
                            <div 
                                className={`row list_property fixed-top ${dataValue?.length > 0 ? 'slider-card-komparasi' : ''}`}
                                style={showBar ? {} : { display: 'none' }}
                            >
                                <Slider asNavFor={nav2} ref={slider => refSlider1.current = slider} {...settingsDown}>
                                    {
                                        [...new Array(4).keys()].map((index) => {
                                            return (
                                                <Col xs={6} key={index}>
                                                    <CardComparasi 
                                                        data={dataValue ? dataValue[index] : null} 
                                                        style={{ margin: '0 5px' }} 
                                                        onClick={index < dataValue?.length ? () => setDataActive(index) : null} 
                                                    />
                                                </Col>
                                            )
                                        })
                                    }
                                </Slider>
                            </div>
                            <div  className={`row list_property ${dataValue?.length > 0 ? 'slider-komparasi' : ''}`}>
                                <Slider asNavFor={nav1} ref={slider => refSlider2.current = slider} {...settingsUp}>
                                    {
                                        [...new Array(4).keys()].map((index) => {
                                            return (
                                                <Col xs={6} key={index}>
                                                    <AddComparasi 
                                                        data={dataValue ? dataValue[index] : null} 
                                                        style={{ margin: '0 5px' }} 
                                                        onClick={index < dataValue?.length ? () => setDataActive(index) : null} 
                                                    />
                                                </Col>
                                            )
                                        })
                                    }
                                </Slider>
                            </div>
                            {(dataValue && dataValue.length > 0 && dataValue[dataActive]) &&
                            (
                                <ItemComparasi image={`don't show`} changeHeight={initRefHeight} initHeight={refHeight} data={dataValue[dataActive]} index={0} />
                            )}
                        </div>
                    ) :
                    (
                        <div  className="row list_property">
                            { dataValue?.map((data, index) =>
                                <div key={index}  className="col-md-3">
                                    <ItemComparasi changeHeight={initRefHeight} initHeight={refHeight} data={data} index={index} />
                                </div>
                            )}
                            {addValue == 0 ? (
                                <>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                </>
                                ) : addValue == 1 ? (
                                <>
                                    <div className="col-md-3">
                                    <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                </>
                                ) : addValue == 2 ? (
                                <>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                </>
                                ) : addValue == 3 ? (
                                <>
                                    <div className="col-md-3">
                                        <AddComparasi style={{ margin: '0 5px' }} />
                                    </div>
                                </>
                                ) : null
                            }
                        </div>
                    )}
                </div>
            </section>
            
        </Layout>
    )
}
