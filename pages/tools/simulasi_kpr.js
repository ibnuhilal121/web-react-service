import SimulasiChart from "../../components/content/SimulasiChart";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import { SimulasiPdf } from "../../components/content/SimulasiPdf";
import { useRef } from "react";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from '../../utils/defaultHeaders'

let settings;

if (typeof window !== "undefined") {
  settings = JSON.parse(sessionStorage.getItem("settings"));
}

export default function index() {
  const router = useRouter();
  const resultView = useRef();
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 768px)` });
  const [maxTahunPinjaman, setMaxTahunPinjaman] = useState(
    settings?.MaxTahunPinjamanKonvNonSubsidi
  );
  const [sukuBungaError, setSukuBungaError] = useState(false);

  const [tipeKpr, setTipeKpr] = useState(
    router.query["jeniskpr"] ? router.query["jeniskpr"] : "kprKonvensional"
  );
  const [jenisSukuBunga, setJenisSukuBunga] = useState(
    router.query["simulasi[jenisbunga]"]
      ? router.query["simulasi[jenisbunga]"]
      : "anuitas"
  );
  const [jenisSubsidi, setJenisSubsidi] = useState(
    router.query["simulasi[jenissubsidi]"]
      ? router.query["simulasi[jenissubsidi]"]
      : "0"
  );
  const [hargaProperti, setHargaProperti] = useState(
    router.query["simulasi[hargaproperti]"]
      ? router.query["simulasi[hargaproperti]"].split(".").join("")
      : ""
  );
  const [uangMuka, setUangMuka] = useState(
    router.query["simulasi[uangmuka]"]
      ? router.query["simulasi[uangmuka]"].split(".").join("")
      : ""
  );
  const [persenUangMuka, setPersenUangMuka] = useState(
    router.query["simulasi[prosentasiuangmuka]"]
      ? router.query["simulasi[prosentasiuangmuka]"]
      : "15"
  );
  const [sukuBunga, setSukuBunga] = useState(
    router.query["simulasi[sukubunga]"]
      ? router.query["simulasi[sukubunga]"]
      : jenisSubsidi == 1 ? settings?.SukuBungaKonvSubsidi : settings?.SukuBungaKonvNonSubsidi
  );
  const [lamaPinjaman, setLamaPinjaman] = useState(
    router.query["simulasi[lamapinjaman]"]
      ? router.query["simulasi[lamapinjaman]"]
      : router.query["simulasi[jenissubsidi]"] == "1" ? "20" : "30"
  );
  const [masaKreditFix, setMasaKreditFix] = useState(
    router.query["simulasi[masakreditfix]"]
      ? router.query["simulasi[masakreditfix]"]
      : "1"
  );
  const [sukuBungaFloating, setSukuBungaFloating] = useState(
    router.query["simulasi[sukubungafloating]"]
      ? router.query["simulasi[sukubungafloating]"]
      : settings?.SukuBungaFloating
  );
  const [errorUangMuka, setErrorUangMuka] = useState(false)
  const handleChange = (e) => {
    setTipeKpr(e.target.value);
    resetField();
  };

  useEffect(() => {
    if (router.query["simulasi[sukubungafloating]"]) {
      setSukuBungaFloating(router.query["simulasi[sukubungafloating]"]);
    }

    if (router.query["simulasi[jenisbunga]"]) {
      setJenisSukuBunga(router.query["simulasi[jenisbunga]"]);
    }

    if (router.query["simulasi[jenissubsidi]"]) {
      setJenisSubsidi(router.query["simulasi[jenissubsidi]"]);
    }

    if (router.query["simulasi[hargaproperti]"]) {
      setHargaProperti(
        router.query["simulasi[hargaproperti]"].replaceAll(".", "")
      );
    }

    if (router.query["simulasi[uangmuka]"]) {
      setUangMuka(router.query["simulasi[uangmuka]"].replaceAll(".", ""));
    }

    if (router.query["simulasi[prosentasiuangmuka]"]) {
      setPersenUangMuka(router.query["simulasi[prosentasiuangmuka]"]);
    }

    if (router.query["simulasi[sukubunga]"]) {
      setSukuBunga(router.query["simulasi[sukubunga]"]);
    }

    if (router.query["simulasi[lamapinjaman]"]) {
      setLamaPinjaman(router.query["simulasi[lamapinjaman]"]);
    }

    if (router.query["simulasi[masakreditfix]"]) {
      setMasaKreditFix(router.query["simulasi[masakreditfix]"]);
    }
  }, [router.query]);

  useEffect(() => {
    if (tipeKpr == "kprKonvensional") {
      if (jenisSubsidi == 0) {
        setSukuBunga(settings?.SukuBungaKonvNonSubsidi);
        setMaxTahunPinjaman(settings?.MaxTahunPinjamanKonvNonSubsidi);
        setLamaPinjaman(settings?.MaxTahunPinjamanKonvNonSubsidi);
        setPersenUangMuka(settings?.ProsentaseUangMukaKonvNonSubsidi);
        const percentage =
          (Number(hargaProperti) *
            Number(settings?.ProsentaseUangMukaKonvNonSubsidi)) /
          100;
        setUangMuka(Math.round(percentage));
      } else {
        setSukuBunga(settings?.SukuBungaKonvSubsidi);
        setMaxTahunPinjaman(settings?.MaxTahunPinjamanKonvSubsidi);
        setLamaPinjaman(settings?.MaxTahunPinjamanKonvSubsidi);
        setPersenUangMuka(settings?.ProsentaseUangMukaKonvSubsidi);
        const percentage =
          (Number(hargaProperti) *
            Number(settings?.ProsentaseUangMukaKonvSubsidi)) /
          100;
        setUangMuka(Math.round(percentage));
      }
    } else {
      if (jenisSubsidi == 0) {
        setSukuBunga(settings?.ProsentaseUangMukaSyariahNonSubsidi);
        setMaxTahunPinjaman(settings?.MaxTahunPinjamanSyariahNonSubsidi);
        setLamaPinjaman(settings?.MaxTahunPinjamanSyariahNonSubsidi);
        setPersenUangMuka(settings?.ProsentaseUangMukaSyariahNonSubsidi);
        const percentage =
          (Number(hargaProperti) *
            Number(settings?.ProsentaseUangMukaSyariahNonSubsidi)) /
          100;
        setUangMuka(Math.round(percentage));
      } else {
        setSukuBunga(settings?.ProsentaseUangMukaSyariahSubsidi);
        setMaxTahunPinjaman(settings?.MaxTahunPinjamanSyariahSubsidi);
        setLamaPinjaman(settings?.MaxTahunPinjamanSyariahSubsidi);
        setPersenUangMuka(settings?.ProsentaseUangMukaSyariahSubsidi);
        const percentage =
          (Number(hargaProperti) *
            Number(settings?.ProsentaseUangMukaSyariahSubsidi)) /
          100;
        setUangMuka(Math.round(percentage));
      }
    }
  }, [jenisSubsidi, tipeKpr]);

  useEffect(() => {
    if (router.query["simulasi[prosentasiuangmuka]"]) {
      setPersenUangMuka(router.query["simulasi[prosentasiuangmuka]"])
    }
    if (router.query["simulasi[lamapinjaman]"]) {
      setLamaPinjaman(router.query["simulasi[lamapinjaman]"])
    }

  }, []);

  
  /* ------------------------------ FETCHING CICILAN MULAI DARI ------------------------------ */
  const [cicilan, setCicilan] = useState({});

  useEffect(() => {
    getCicilanData()
  }, []);

  const getCicilanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/simulasi/fast`;

      const res = await fetch(endpoint, {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
      });

      const resData = await res.json();
      if (!resData.IsError) {
        setCicilan(resData.data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      // console.log(error.message);
    } finally {
      // setLoading(false);
    }
  };

  /* ---------------------------- FETCHING SIMULASI --------------------------- */
  const [simulasiData, setSimulasiData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  // untuk tab Visualisasi Data
  const [detailDataPerYear, setDetailDataPerYear] = useState([]);
  const [selectedYear, setSelectedYear] = useState("1");
  const [visualisasiData, setVisualisasiData] = useState({
    sisaPinjaman: null,
    porsiPokok: null,
    porsiBunga: null,
    angsuran: null,
    bunga: null,
  });

  useEffect(() => {
    console.log(detailDataPerYear);
    if (!detailDataPerYear.length) return;

    const thisYearData = detailDataPerYear[Number(selectedYear) - 1];
    const lastMonthData = thisYearData[thisYearData.length - 1];
    if (lastMonthData) {
      setVisualisasiData({
        sisaPinjaman: lastMonthData.sisa_pinjaman,
        porsiPokok: lastMonthData.porsi_pokok,
        porsiBunga: lastMonthData.porsi_bunga,
        angsuran: lastMonthData.angsuran_perbulan,
        bunga: lastMonthData.suku_bunga,
      });
    }
  }, [selectedYear, detailDataPerYear]);

  useEffect(() => {
    if (router.query["jeniskpr"]) {
      getSimulasiData();
    }
  }, [router]);

  useEffect(() => {
    setSukuBungaError(!sukuBunga || Number(sukuBunga) == 0)
  }, [sukuBunga])

  const resetField = () => {
    setJenisSukuBunga("anuitas");
    setJenisSubsidi("");
    setHargaProperti("");
    setUangMuka("");
    setPersenUangMuka("15");
    setSukuBunga(settings?.SukuBungaKonvNonSubsidi);
    setLamaPinjaman("30");
    setMasaKreditFix("1");
    setSukuBungaFloating(settings?.SukuBungaFloating);
    setSimulasiData(null);
  };

  const getSimulasiData = async () => {
    const jenisSimulasi = router.query["jeniskpr"]; //(0=Konvensional, 1=Syariah)
    /*
    const jenisSukuBunga = router.query["simulasi[jenisbunga]"]; //(anuitas, flat, syariah)
    const isSubsidi = router.query["simulasi[jenissubsidi]"]; //(0=Tidak, 1=Ya)
    const harga = router.query["simulasi[hargaproperti]"]; //number
    const uangMuka = router.query["simulasi[uangmuka]"]; //number
    const sukuBunga = router.query["simulasi[sukubunga]"]; //number
    const lamaPinjam = router.query["simulasi[lamapinjaman]"]; //number
    const masaKreditFix = 1; //number
    const sukuBungaFloating = 13.5; //number
     

    const reqBody = {
      jns_simulasi: jenisSimulasi,
      jns_sk_bga: jenisSukuBunga,
      is_ssd: isSubsidi,
      hrg: harga,
      uang_mka: uangMuka,
      sk_bga: sukuBunga,
      lm_pnjm: lamaPinjam,
      ms_krdt_fix: masaKreditFix,
      sk_bga_flting: sukuBungaFloating,
    };*/

    try {
      const endpointConventional = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/simulasi`;
      const endpointSyariah = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/simulasi/syariah`;
      const isUsingSyariah = jenisSimulasi == "kprSyariah";

      const requestBodyConventional = {
        jns_simulasi: jenisSimulasi === "kprKonvensional" ? 0 : 1, //(0=Konvensional, 1=Syariah)
        jns_sk_bga: jenisSukuBunga, //(anuitas, flat, syariah)
        is_ssd: jenisSubsidi, //(0=Tidak, 1=Ya)
        hrg: hargaProperti.split(".").join(""),
        uang_mka: uangMuka.split(".").join(""),
        sk_bga: sukuBunga,
        lm_pnjm: lamaPinjaman,
        ms_krdt_fix: masaKreditFix,
        sk_bga_flting: sukuBungaFloating,
      };

      const requestBodySyariah = {
        jns_simulasi: jenisSimulasi === "kprKonvensional" ? 0 : 1,
        is_ssd: jenisSubsidi,
        hrg: hargaProperti.split(".").join(""),
        lm_pnjm: lamaPinjaman,
        uang_mka: uangMuka.split(".").join(""),
        sk_bga: settings?.SukuBungaSyariahNonSubsidi,
        margin_total: sukuBungaFloating,
      };

      setIsLoading(true);
      const res = await fetch(
        isUsingSyariah ? endpointSyariah : endpointConventional,
        {
          method: "POST",
          headers: getHeaderWithAccessKey(),
          body: new URLSearchParams(
            isUsingSyariah ? requestBodySyariah : requestBodyConventional
          ),
        }
      );
      const resData = await res.json();
      setIsLoading(false);
      if (!resData.IsError) {
        console.log("GET DATA", resData.Data[0]);
        setSimulasiData(resData.Data[0]);
        setUangMuka(resData.Data[0].rincian.uang_muka);
        setSukuBunga(sukuBunga)
        // setPersenUangMuka(
        //   (
        //     (resData.Data[0].rincian.uang_muka * 100) /
        //     Number(hargaProperti)
        //   ).toFixed(2)
        // );
        const dataPerYear = splitMonthData(resData.Data[0].detail);
        setDetailDataPerYear(dataPerYear);
        isTabletOrMobile && resultView.current?.scrollIntoView()
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  const generateTahunDropdown = (max) => {
    const row = [];
    for (let i = 1; i <= max; i++) {
      row.push(<option value={i}>{i} Tahun</option>);
    }
    return <>{row}</>;
  };

  const templateResult = (
    <div className="col-md col-xl-8">
      <div className="">
        <div className="list_simulasi">
          <div className="card-body without-pd revamp-card-imgdev px-md-2 px-lg-3">
            <div className="card-block revamp-card-imgdev">
              <div className="kalkulasi-simulasi">
                <div className="tab-menu revamp-card-imgdev">
                  <ul id="nav_ajukan_kpr" className="nav nav-tabs">
                    <li className="nav-item">
                      <a
                        href="#"
                        className="nav-link active me-1 ms-0 me-md-1 px-md-0 px-lg-3"
                        data-bs-toggle="tab"
                        data-bs-target="#tab-rincian-pinjaman"
                        role="tab"
                        aria-expanded="true"
                        style={{width: isTabletOrMobile ? 133 : "auto"}}
                      >
                        <span>Rincian Pinjaman</span>
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        href="#"
                        className="nav-link me-1 ms-0 me-md-1 px-md-0 ps-md-1 px-lg-3"
                        data-bs-toggle="tab"
                        data-bs-target="#tab-detail-angsuran"
                        role="tab"
                        aria-expanded="false"
                        style={{width: isTabletOrMobile ? 123 : "auto"}}
                      >
                        <span>Detail Angsuran</span>
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        href="#"
                        className="nav-link me-1 ms-0 me-md-1 px-md-1 px-lg-3"
                        data-bs-toggle="tab"
                        data-bs-target="#tab-visualisasi-angsuran"
                        role="tab"
                        aria-expanded="false"
                        style={{width: isTabletOrMobile ? 157 : "auto"}}
                      >
                        <span>Visualisasi Angsuran</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="tab-content" id="tab-kalkulasi-simulasi">
                  <div
                    className="tab-pane fade active show"
                    id="tab-rincian-pinjaman"
                    role="tabpanel"
                  >
                    <div className="tab-body tab-rincian-biaya">
                      <div className="kalkulasi-simulasi-section">
                        <div className="kalkulasi-simulasi-header">
                          <h5
                            className="kalkulasi-simulasi-title font-sm-5"
                            style={{
                              fontFamily: "FuturaBT",
                              fontSize: "16px",
                              fontWeight: 700,
                              lineHeight: "24px",
                            }}
                          >
                            {" "}
                            Informasi Detail{" "}
                          </h5>
                        </div>
                        <div className="kalkulasi-simulasi-body">
                          <ul className="list-inline list-unstyled">
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>
                                  {router.query["jeniskpr"] == "kprKonvensional"
                                    ? "Suku bunga"
                                    : "Margin"}
                                  /Tahun{" "}
                                </label>
                              </div>
                              <div className="float-right">
                                <span>
                                  {tipeKpr == "kprSyariah" ? sukuBunga : simulasiData?.rincian?.suku_bunga}%
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>
                                  {router.query["jeniskpr"] == "kprKonvensional"
                                    ? "Suku bunga"
                                    : "Margin"}{" "}
                                  floating/ Tahun{" "}
                                </label>
                              </div>
                              <div className="float-right">
                                <span>
                                  {simulasiData?.rincian?.suku_bunga_float}%
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Kredit Fix </label>
                              </div>
                              <div className="float-right">
                                <span>
                                  {simulasiData?.rincian?.kredit_fix} Bulan
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Lama Pinjaman </label>
                              </div>
                              <div className="float-right">
                                <span>
                                  {simulasiData?.rincian?.lama_pinjam} Bulan
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Jumlah Pinjaman Maksimal </label>
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp{" "}
                                  {simulasiData?.rincian?.jml_pinjam_max.toLocaleString(
                                    "id"
                                  )}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Uang Muka ( DP ) </label>
                                <CustomTooltips
                                  message={
                                    roundUangMuka(simulasiData?.rincian?.notes?.uang_mka)
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp{" "}
                                  {Number(
                                    simulasiData?.rincian?.uang_muka
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="kalkulasi-simulasi-section">
                        <div className="kalkulasi-simulasi-header">
                          <h5
                            className="kalkulasi-simulasi-title font-sm-5"
                            style={{
                              fontFamily: "FuturaBT",
                              fontSize: "16px",
                              fontWeight: 700,
                              lineHeight: "24px",
                            }}
                          >
                            {" "}
                            Biaya Bank{" "}
                          </h5>
                        </div>
                        <div className="kalkulasi-simulasi-body">
                          <ul className="list-inline list-unstyled">
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Appraisal </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_bank_appraisal
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_bank_appraisal
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Administrasi </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes?.b_bank_admin
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_bank_admin
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Proses </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes?.b_bank_proses
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_bank_proses
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Provisi </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes?.b_bank_provisi
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_bank_provisi
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Asuransi </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_bank_asuransi
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_bank_asuransi
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label
                                  style={{
                                    fontWeight: "700",
                                    fontFamily: "Helvetica",
                                  }}
                                >
                                  Total Biaya Bank{" "}
                                </label>
                              </div>
                              <div className="float-right">
                                <span
                                  style={{
                                    fontWeight: "700",
                                    fontFamily: "Helvetica",
                                  }}
                                >
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.total_biaya_bank
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="kalkulasi-simulasi-section">
                        <div className="kalkulasi-simulasi-header">
                          <h5
                            className="kalkulasi-simulasi-title font-sm-5"
                            style={{
                              fontFamily: "FuturaBT",
                              fontSize: "16px",
                              fontWeight: 700,
                              lineHeight: "24px",
                            }}
                          >
                            {" "}
                            Biaya Notaris{" "}
                          </h5>
                        </div>
                        <div className="kalkulasi-simulasi-body">
                          <ul className="list-inline list-unstyled">
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Akte Jual Beli </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_notaris_aktejualbeli
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian
                                      ?.b_notaris_aktejualbeli
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Bea Balik Nama </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_notaris_baliknama
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_notaris_baliknama
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Akta SKMHT </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_notaris_akte_skmht
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_notaris_akte_skmht
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Akta APHT </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_notaris_akte_apht
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_notaris_akte_apht
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Perjanjian HT </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_notaris_perjanjian_ht
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian
                                      ?.b_notaris_perjanjian_ht
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label>Cek Sertifikat ZNT, PNBP HT </label>
                                <CustomTooltips
                                  message={
                                    simulasiData?.rincian?.notes
                                      ?.b_notaris_cek_sertif
                                  }
                                />
                              </div>
                              <div className="float-right">
                                <span>
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.b_notaris_cek_sertif
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label
                                  style={{
                                    fontWeight: "700",
                                    fontFamily: "Helvetica",
                                  }}
                                >
                                  Total Biaya Notaris{" "}
                                </label>
                              </div>
                              <div className="float-right">
                                <span
                                  style={{
                                    fontWeight: "700",
                                    fontFamily: "Helvetica",
                                  }}
                                >
                                  Rp
                                  {Math.round(
                                    simulasiData?.rincian?.total_biaya_notaris
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="kalkulasi-simulasi-section">
                        <div className="kalkulasi-simulasi-header p-0"></div>
                        <div className="kalkulasi-simulasi-body-total">
                          <ul className="list-inline list-unstyled">
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label
                                  className="font-sm-5 font-weight-semibold"
                                  style={{
                                    fontFamily: "Helvetica",
                                    color: "#00193E",
                                    fontSize: "16px",
                                    fontWeight: 700,
                                  }}
                                >
                                  Angsuran Per Bulan{" "}
                                </label>
                              </div>
                              <div className="float-right">
                                <span
                                  className="font-sm-5 font-weight-semibold"
                                  style={{
                                    fontFamily: "Helvetica",
                                    color: "#00193E",
                                    fontSize: "16px",
                                    fontWeight: 700,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  Rp.{" "}
                                  {Math.round(
                                    Math.round(
                                      (Number(
                                        simulasiData?.rincian?.angsuran_perbulan
                                      ) /
                                        100) *
                                        100
                                    )
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                            <li className="list-inline-item">
                              <div className="float-left">
                                <label
                                  className="font-sm-5 font-weight-semibold"
                                  style={{
                                    fontFamily: "Helvetica",
                                    color: "#00193E",
                                    fontSize: "16px",
                                    fontWeight: 700,
                                  }}
                                >
                                  Pembayaran Pertama{" "}
                                </label>
                                <br />
                                <div className="kalkulasi-simulasi-body">
                                  <small
                                    className="font-sm-1 opacity-8"
                                    style={{
                                      fontFamily: "Helvetica",
                                      fontWeight: 400,
                                      color: "#666666",
                                      fontSize: "12px",
                                      paddingTop: "8px",
                                    }}
                                  >
                                    ( Angsuran + DP + Total Biaya Bank + Total
                                    Biaya Notaris )
                                  </small>
                                </div>
                              </div>
                              <div className="float-right">
                                <span
                                  className="font-sm-5 font-weight-semibold font-ruby"
                                  style={{
                                    fontFamily: "Helvetica",
                                    color: "#00193E",
                                    fontSize: "16px",
                                    fontWeight: 700,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  Rp.{" "}
                                  {Math.round(
                                    simulasiData?.rincian?.pembayaran_pertama
                                  ).toLocaleString("id")}
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="tab-detail-angsuran"
                    role="tabpanel"
                  >
                    <div className="tab-body tab-rincian-angsuran">
                      <div className="detail-angsuran">
                        <div className="kalkulasi-simulasi-section">
                          <div className="kalkulasi-simulasi-body">
                            <div className="table-angsuran">
                              <div className="table-responsive table-scroll">
                                <table className="table table-striped ">
                                  <thead>
                                    <tr>
                                      <th
                                        className="tb-bulan"
                                        // style={{
                                        //   width: "12%",
                                        //   textAlign: "center",
                                        // }}
                                      >
                                        Bulan
                                      </th>
                                      <th className="tb-sisa">Sisa Pinjaman</th>
                                      <th className="tb-porsi">Porsi Pokok</th>
                                      <th className="tb-porsi-bunga">
                                        {router.query["jeniskpr"] ==
                                        "kprKonvensional"
                                          ? "Porsi Bunga"
                                          : "Porsi Margin"}
                                      </th>
                                      <th className="tb-angsuran">Angsuran</th>
                                      <th
                                        className="tb-bunga"
                                        // style={{
                                        //   width: "12%",
                                        // }}
                                      >
                                        {/* {router.query["jeniskpr"] ==
                                        "kprKonvensional"
                                          ? "Bunga"
                                          : "Margin"} */}
                                          {router.query["jeniskpr"] == "kprKonvensional"
                                    ? "Bunga"
                                    : "Margin"}{" "}
                                  {/* floating/ Tahun{" "} */}
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {simulasiData?.detail &&
                                      simulasiData?.detail.map((data) => (
                                        <tr key={data.bulan}>
                                          <td
                                            className="tb-bulan"
                                            // style={{
                                            //   width: "12%",
                                            //   textAlign: "center",
                                            // }}
                                          >
                                            {data.bulan}
                                          </td>
                                          <td className="tb-sisa">
                                            Rp.{" "}
                                            {Math.round(
                                              Math.round(
                                                Number(data.sisa_pinjaman) / 100
                                              ) * 100
                                            ).toLocaleString("id")}
                                          </td>
                                          <td className="tb-porsi">
                                            Rp.{" "}
                                            {Math.round(
                                              Math.round(
                                                Number(data.porsi_pokok) / 100
                                              ) * 100
                                            ).toLocaleString("id")}
                                          </td>
                                          <td className="tb-porsi-bunga">
                                            Rp.{" "}
                                            {Math.round(
                                              Math.round(
                                                Number(data.porsi_bunga) / 100
                                              ) * 100
                                            ).toLocaleString("id")}
                                          </td>
                                          <td className="tb-angsuran">
                                            Rp.{" "}
                                            {Math.round(
                                              Math.round(
                                                Number(data.angsuran_perbulan) /
                                                  100
                                              ) * 100
                                            ).toLocaleString("id")}
                                          </td>
                                          <td
                                            className="tb-bunga"
                                            // style={{
                                            //   width: "12%",
                                            // }}
                                          >
                                            {/* {data.suku_bunga}% */}
                                            {simulasiData?.rincian?.suku_bunga_float}%
                                          </td>
                                        </tr>
                                      ))}
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="tab-visualisasi-angsuran"
                    role="tabpanel"
                  >
                    <div className="row align-items-center">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="form-control-label">Tahun</label>
                          <div className="input-select">
                            <select
                              className="form-select"
                              name="simulasi[masakreditfix]"
                              style={{ width: "138px", height: "48px" }}
                              value={selectedYear}
                              onChange={(e) => setSelectedYear(e.target.value)}
                            >
                              {detailDataPerYear.length &&
                                detailDataPerYear.map((data, i) => (
                                  <option key={i} value={i + 1}>
                                    {i + 1}
                                  </option>
                                ))}
                            </select>
                          </div>
                        </div>
                        <div className="detail_angsuran">
                          <h6>Sisa Pinjaman</h6>
                          <p>
                            Rp
                            {Number(
                              Math.round(visualisasiData?.sisaPinjaman)
                            ).toLocaleString("id")}
                          </p>
                          <h6>Porsi Pokok</h6>
                          <p>
                            Rp
                            {Number(Math.round(visualisasiData?.porsiPokok)).toLocaleString(
                              "id"
                            )}{" "}
                          </p>
                          <h6>
                            {router.query["jeniskpr"] == "kprKonvensional"
                              ? "Porsi Bunga"
                              : "Porsi Margin"}
                          </h6>
                          <p>
                            Rp
                            {Number(Math.round(visualisasiData?.porsiBunga)).toLocaleString(
                              "id"
                            )}{" "}
                          </p>
                          <h6>Angsuran</h6>
                          <p>
                            Rp
                            {Number(Math.round(visualisasiData?.angsuran)).toLocaleString(
                              "id"
                            )}{" "}
                          </p>
                          <h6>
                            {router.query["jeniskpr"] == "kprKonvensional"
                              ? "Bunga"
                              : "Margin"}
                          </h6>
                          <p>
                            {/* {Number(visualisasiData?.suku_bunga).toLocaleString(
                              "id"
                            )}
                            %{" "} */}
                            {simulasiData?.rincian?.suku_bunga_float}%
                          </p>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="row align-items-center">
                          <div
                            className="col"
                            style={{
                              alignSelf: "start",
                              marginTop: "15px",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                              }}
                            >
                              <div
                                style={{
                                  width: 22,
                                  height: 22,
                                  borderRadius: 60,
                                  backgroundColor: "#D9E8FF",
                                  marginRight: "10px",
                                }}
                              />
                              <div
                                style={{
                                  fontSize: "14px",
                                  fontFamily: "FuturaBT",
                                  fontWeight: "700",
                                }}
                              >
                                <>Porsi Pokok</>
                              </div>
                            </div>
                            <div
                              style={{
                                marginLeft: "35px",
                                fontSize: 16,
                                marginBottom: 21,
                                fontFamily: "Helvetica",
                                fontWeight: "400",

                                color: "#00193E",
                              }}
                            >
                              {(
                                String(
                                  getPorsiPokokPercentage(
                                    Number(visualisasiData?.porsiBunga),
                                    Number(visualisasiData?.porsiPokok)
                                  )
                                ) + "%"
                              ).replace("NaN%", "0")}
                            </div>

                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                fontSize: 14,
                              }}
                            >
                              <div
                                style={{
                                  width: 22,
                                  height: 22,
                                  borderRadius: 60,
                                  backgroundColor: "#0B4773",
                                  marginRight: "10px",
                                }}
                              />
                              <div
                                style={{
                                  fontSize: "14px",
                                  fontFamily: "FuturaBT",
                                  fontWeight: "700",
                                }}
                              >
                                <>
                                  {router.query["jeniskpr"] == "kprKonvensional"
                                    ? "Porsi Bunga"
                                    : "Porsi Margin"}
                                </>
                              </div>
                            </div>
                            <div
                              style={{
                                marginLeft: "35px",
                                fontSize: 16,
                                fontFamily: "Helvetica",
                                fontWeight: "400",

                                color: "#00193E",
                              }}
                            >
                              {(
                                String(
                                  getPorsiBungaPercentage(
                                    Number(visualisasiData?.porsiBunga),
                                    Number(visualisasiData?.porsiPokok)
                                  )
                                ) + "%"
                              ).replace("NaN%", "0")}
                            </div>
                          </div>
                          <div className="col-md-8 px-5 px-sm-0">
                            <SimulasiChart
                              porsiPokok={getPorsiPokokPercentage(
                                Number(visualisasiData?.porsiBunga),
                                Number(visualisasiData?.porsiPokok)
                              )}
                              porsiBunga={getPorsiBungaPercentage(
                                Number(visualisasiData?.porsiBunga),
                                Number(visualisasiData?.porsiPokok)
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="kalkulasi-simulasi-section-catatan">
              <div className="kalkulasi-simulasi-body pt-0">
                <div
                  className="row"
                  style={{ width: "100%", "--bs-gutter-x": 0 }}
                >
                  <div className="col-sm-5">
                    <p
                      className="font-sm-1 pe-sm-3"
                      style={{ fontWeight: "400" }}
                    >
                      {" "}
                      Catatan: Perhitungan ini adalah hasil perkiraaan aplikasi
                      KPR secara umum. Data perhitungan di atas dapat berbeda
                      dengan perhitungan bank. Untuk perhitungan yang akurat,
                      silakan hubungi kantor cabang kami.{" "}
                    </p>
                  </div>
                  <div className="col-sm-7">
                    <div
                      className="row"
                      style={{
                        maxWidth: "428px",
                      }}
                    >
                      <div
                        className="col-5 col-md-6"
                        style={{
                          padding: "0px",
                          paddingRight: "6px",
                        }}
                      >
                        <Link href="/property/tipe">
                          <a
                            href="#"
                            className="btn btn-main btn_rounded"
                            style={{
                              height: 48,
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              fontFamily: "Helvetica",
                              fontWeight: 700,
                            }}
                          >
                            Pilih Unit
                          </a>
                        </Link>
                      </div>
                      <div
                        className="col-5 col-md-6"
                        style={{
                          padding: "0px",
                          paddingLeft: "6px",
                        }}
                      >
                        <SimulasiPdf
                          tipe={
                            tipeKpr === "kprKonvensional"
                              ? "Konvensional"
                              : "Syariah"
                          }
                          simulasiData={simulasiData}
                        />
                        {/* <a
                          href="#"
                          className="btn btn-outline-main btn_rounded w-100"
                          style={{
                            width: 248,
                            height: 48,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            fontFamily: "Helvetica",
                            fontWeight: 700,
                          }}
                        >
                          Download PDF
                        </a> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  const templateNoResult = (
    <div className="col col-xl-8">
      <div className="row text-center" style={{ marginTop: 182 }}>
        <div className="col">
          <img src="/images/no-data.svg" style={{ marginBottom: 32 }} />
          <p
            style={{
              fontFamily: "Helvetica",
              fontWeight: 700,
              fontSize: 16,
              color: "#000000",
            }}
          >
            Kamu belum memiliki perhitungan.
            <br />
            Silakan mengisi simulasi KPR di samping untuk mengetahui hasilnya.
          </p>
        </div>
      </div>
    </div>
  );

  return (
    <Layout title="Simulasi KPR" isLoaderOpen={isLoading}>
      <Breadcrumb active="Simulasi KPR" />
      <div className="container mb-5">
        <section id="simulasi-kpr" className="content-detail-body mt-3">
          <div className="row">
            <div className="col-12 mb-3">
              <h4
                className="title text-center"
                style={{
                  fontFamily: "FuturaBT",
                  fontSize: "32px",
                  fontWeight: 700,
                }}
              >
                Simulasi KPR
              </h4>
            </div>
          </div>
          <div className="row">
            <div
              className="col-md-12 col-xl-4 responsive-kpr"
              // style={{ height: "1223px" }}
            >
              <div className="card widget_form">
                <div
                  className="card-body pt-0 form-respon"
                  // style={{ width: "308px" }}
                >
                  <div className="card-block">
                    <div className="form-simulasi">
                      <form
                        className="form-default"
                        id="form-simulasi-kpr"
                        action="#"
                        noValidate="novalidate"
                        onSubmit={(e) => {
                          if (sukuBungaError) {
                            e.preventDefault()
                          };
                        }}
                      >
                        <input
                          type="hidden"
                          name="simulasi[jeniskpr]"
                          value="0"
                        />
                        <div className="tipe_kpr row mb-3 pt-3 px-0">
                          <label
                            className="form-control-label"
                            style={{
                              marginBottom: "10px",
                              fontFamily: "Futura",
                              fontSize: "16px",
                              fontWeight: "bold",
                              marginRight: "10px",
                              lineHeight: "150%",
                            }}
                          >
                            Tipe KPR
                          </label>
                          <ul className="list-unstyled list-radio-vertical">
                            <li className="list-item">
                              <label
                                htmlFor="jenis_kpr"
                                className="radio-main icheck-label icheck[0m9cm]"
                              >
                                <div className="d-flex align-items-center">
                                  <div className="d-flex  align-self-start">
                                    <input
                                      style={{
                                        marginTop: "8px",
                                        boxSizing: "border-box",
                                        height: "20px",
                                        width: "20px",
                                        position: "unset",
                                        // color: "#AAAAAA",
                                      }}
                                      className="form-check-input"
                                      type="radio"
                                      name="jeniskpr"
                                      id="kprKonf"
                                      onChange={handleChange}
                                      value="kprKonvensional"
                                      checked={tipeKpr == "kprKonvensional"}
                                    />
                                    {/* <div className="iradio_square-blue icheck-item icheck[0m9cm] ">
                                      <input
                                        type="radio"
                                        name="jeniskpr"
                                        className="mr-3 mt-2 KprKonvensional"
                                        value="kprKonvensional"
                                        style={{ width: 20, height: 21.4 }}
                                        onChange={handleChange}
                                        checked={tipeKpr == "kprKonvensional"}
                                      />
                                    </div> */}
                                    <img
                                      className=""
                                      style={{
                                        width: "36px",
                                        height: "32.4px",
                                        margin: "2px 20px 0 16px",
                                      }}
                                      src="/images/acc/kpr-konvensional.png"
                                      alt=""
                                    />
                                  </div>
                                  <div className="media-body">
                                    <h5 className="list-item-title mt-1 ">
                                      KPR Konvensional
                                      <CustomTooltips message="KPR Konvensional adalah pinjaman pemilikan rumah dengan bunga float rendah" />
                                    </h5>
                                    <span className="list-item-subtitle text-green KprKonvensionalMulai">
                                      Cicilan mulai dari
                                      <br /> Rp{cicilan?.konvensional || "-"}/bulan
                                    </span>
                                    <div className="suku_bunga">
                                      Suku bunga{" "}
                                      {settings?.SukuBungaKonvNonSubsidi}%
                                    </div>
                                  </div>
                                </div>
                              </label>
                            </li>
                            <hr
                              style={{
                                margin: "24px 0 17px 0",
                                backgroud: "#EEEEEE",
                                opacity: 0.1,
                              }}
                            />
                            <li className="list-item">
                              <label
                                htmlFor="jenis_kpr"
                                className="radio-main icheck-label icheck[tz90d]"
                              >
                                <div className="d-flex align-items-center">
                                  <div className="d-flex  align-self-start">
                                    <input
                                      style={{
                                        marginTop: "8px",
                                        boxSizing: "border-box",
                                        height: "20px",
                                        width: "20px",
                                        position: "unset",
                                        // color: "#AAAAAA",
                                      }}
                                      className="form-check-input"
                                      type="radio"
                                      name="jeniskpr"
                                      id="kprKonf"
                                      onChange={handleChange}
                                      value="kprSyariah"
                                      checked={tipeKpr == "kprSyariah"}
                                    />
                                    {/* <div className="iradio_square-blue icheck-item icheck[tz90d]">
                                      <input
                                        type="radio"
                                        name="jeniskpr"
                                        className="mr-3 mt-2 KprSyariah"
                                        value="kprSyariah"
                                        style={{ width: 20, height: 21.4 }}
                                        onChange={handleChange}
                                        checked={tipeKpr == "kprSyariah"}
                                      />
                                    </div> */}
                                    <img
                                      className=""
                                      style={{
                                        width: "36px",
                                        height: "32.4px",
                                        margin: "2px 20px 0 16px",
                                      }}
                                      src="/images/acc/kpr-syariah.png"
                                      alt=""
                                    />
                                  </div>
                                  <div className="media-body">
                                    <h5 className="list-item-title mt-1">
                                      KPR Syariah{" "}
                                      <CustomTooltips message="KPR Syariah adalah KPR yang ditujukan untuk masyarakat dengan nilai - nilai syariah" />
                                    </h5>
                                    <span className="list-item-subtitle text-green KprSyariahMulai">
                                      Cicilan mulai dari
                                      <br /> Rp{cicilan?.syariah || "-"}/bulan
                                    </span>
                                    <div className="suku_bunga">
                                      Margin{" "}
                                      {settings?.SukuBungaSyariahNonSubsidi}%
                                    </div>
                                  </div>
                                </div>
                              </label>
                            </li>
                          </ul>
                        </div>

                        <div className="row flex-md-column">
                          <div className=" col-lg-12">
                            {tipeKpr === "kprSyariah" ? (
                              <div
                                className="form-group form-mini "
                                // style={{ width: "276px" }}
                              >
                                <label className="form-control-label">
                                  Jenis Margin
                                </label>
                                <div className="input-select">
                                  <select
                                    className="form-select"
                                    name="simulasi[jenisbunga]"
                                    style={{
                                      fontSize: 14,
                                      fontFamily: "Helvetica",
                                    }}
                                    value={jenisSukuBunga}
                                    onChange={(e) =>
                                      setJenisSukuBunga(e.target.value)
                                    }
                                  >
                                    <option value="anuitas">
                                      Anuitas Berjenjang
                                    </option>
                                    <option value="flat">Flat</option>
                                  </select>
                                </div>
                              </div>
                            ) : (
                              <div
                                // style={{ width: "276px" }}
                                className="form-group form-mini"
                              >
                                <label className="form-control-label">
                                  Jenis Suku Bunga
                                </label>
                                <div
                                  className="input-select form-mini"
                                  style={{ height: "48px" }}
                                >
                                  <select
                                    className="form-select"
                                    name="simulasi[jenisbunga]"
                                    style={{
                                      fontSize: 14,
                                      fontFamily: "Helvetica",
                                    }}
                                    value={jenisSukuBunga}
                                    onChange={(e) =>
                                      setJenisSukuBunga(e.target.value)
                                    }
                                  >
                                    <option value="anuitas">Anuitas</option>
                                    <option value="flat">Flat</option>
                                  </select>
                                </div>
                              </div>
                            )}

                            <div className="form-group">
                              <label className="form-control-label">
                                Jenis Subsidi
                              </label>
                              <div
                                className="input-select form-mini"
                                style={{ height: "48px" }}
                              >
                                <select
                                  className="form-select"
                                  name="simulasi[jenissubsidi]"
                                  data-max-jangka-subsidi="20"
                                  data-max-jangka-nonsubsidi="30"
                                  data-uangmuka-nonsubsidi="15"
                                  data-uangmuka-subsidi="1"
                                  data-suku-nonsubsidi="6.75"
                                  data-suku-subsidi="5"
                                  style={{
                                    fontSize: 14,
                                    fontFamily: "Helvetica",
                                  }}
                                  value={jenisSubsidi}
                                  onChange={(e) =>
                                    setJenisSubsidi(e.target.value)
                                  }
                                >
                                  <option value="0">Non Subsidi</option>
                                  <option value="1">Subsidi</option>
                                </select>
                              </div>
                            </div>

                            <div className="form-group">
                              <label
                                className="form-control-label"
                                style={{
                                  width: "151px",
                                }}
                              >
                                Harga Beli Properti
                              </label>
                              <div
                                className="input-group search_property_bunga_floating form-mini"
                                style={{ height: "48px" }}
                              >
                                <div className="currency">
                                  <span className="">Rp </span>
                                </div>
                                <input
                                  type="text"
                                  name="simulasi[hargaproperti]"
                                  data-trigger-uangmuka="true"
                                  data-releated="[name='simulasi[prosentasiuangmuka]']"
                                  data-priceformat-notag="true"
                                  // value="0"
                                  pattern="[0-9]"
                                  placeholder="0"
                                  className="form-control required revamp-hargabeli-form bg-putih"
                                  value={Number(hargaProperti).toLocaleString(
                                    "id"
                                  )}
                                  onChange={(e) => {
                                    const value = e.target.value
                                      .split(".")
                                      .join("");
                                    if (checkIsNumber(value)) {
                                      setHargaProperti(value);
                                      const percentage =
                                        (Number(value) *
                                          Number(persenUangMuka)) /
                                        100;
                                      setUangMuka(Math.round(percentage));
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div
                              className="form-group inp-uangmuka"
                              data-releated="[name='simulasi[hargaproperti]']"
                            >
                              <label className="form-control-label">
                                Uang Muka
                              </label>
                              <div
                                className="input-group search_property_bunga_floating form-mini"
                                style={{ height: "48px" }}
                              >
                                <div className="currency">
                                  <span className="">Rp </span>
                                </div>
                                <input
                                  type="text"
                                  name="simulasi[uangmuka]"
                                  data-priceformat-notag="true"
                                  placeholder="0"
                                  className="form-control required bg-putih tambahan-w "
                                  value={Number(uangMuka).toLocaleString("id")}
                                  onChange={(e) => {
                                    const value = e.target.value
                                      .split(".")
                                      .join("");
                                    if (checkIsNumber(value)) {
                                      if(Number(hargaProperti) < Number(value)){
                                        return setErrorUangMuka(true);
                                      } else if(Number(hargaProperti) >= Number(value)){
                                        setErrorUangMuka(false);
                                      setUangMuka(Math.round(value));
                                      setErrorUangMuka(false);
                                      const percentage =
                                        (Number(value) /
                                          Number(hargaProperti)) *
                                        100;
                                      if (
                                        isNaN(percentage) ||
                                        percentage == Infinity
                                      ) {
                                        setPersenUangMuka("0");
                                        return;
                                      }
                                      setPersenUangMuka(Math.round(percentage));
                                    }}
                                  }}
                                />
                                <div
                                  style={{ borderRight: "1px solid black" }}
                                />
                                <input
                                  type="text"
                                  name="simulasi[prosentasiuangmuka]"
                                  value={persenUangMuka}
                                  placeholder="0"
                                  className="form-control required number persen bg-putih"
                                  onChange={(e) => {
                                    if (checkIsNumber(e.target.value)) {
                                      setPersenUangMuka(e.target.value);
                                      const resultUangMuka =
                                        (Number(hargaProperti) *
                                          Number(e.target.value)) /
                                        100;
                                      setUangMuka(Math.round(resultUangMuka));
                                    }
                                  }}
                                />
                                <div className="">
                                  <span
                                    style={{
                                      color: "#00193E",
                                      position: "absolute",
                                      right: "10px",
                                      bottom: "11px",
                                    }}
                                  >
                                    %{" "}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    color: "#00193E",
                                    position: "absolute",
                                    right: "80px",
                                    bottom: "1px",
                                    height: "43px",

                                    borderRight:
                                      "1px solid rgba(238, 238, 238, 1)",
                                  }}
                                ></div>
                              </div>
                              <label
                                className="form-text text-muted"
                                style={{
                                  fontFamily: "Helvetica",
                                  fontSize: "12px",
                                  color: "#AAAAAA !important",
                                }}
                              >
                                <div
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontStyle: "normal",
                                    fontSize: "12px",
                                    color: "#AAAAAA",
                                    fontWeight: "normal",
                                    lineHeight: "18px",
                                    width: "267px",
                                  }}
                                >
                                  Uang muka diambil dari {persenUangMuka}% harga properti yang
                                  Anda inginkan{" "}
                                </div>
                                {errorUangMuka && <p style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}><i className="bi bi-exclamation-circle-fill" style={{marginRight: 5}}></i>Uang muka tidak boleh lebih dari harga properti</p>}
                              </label>
                            </div>
                          </div>
                          <div className=" col-lg-12">
                            {tipeKpr === "kprKonvensional" ? (
                              <div className="form-group">
                                <label className="form-control-label">
                                  Suku Bunga Per Tahun{" "}
                                  <CustomTooltips message="Suku Bunga Bank fix selama 20 tahun" />
                                </label>
                                <div
                                  className="input-group search_property_bunga form-mini"
                                  style={{ height: "48px" }}
                                >
                                  <input
                                    type="text"
                                    name="simulasi[sukubunga]"
                                    placeholder="0"
                                    // value="6.75"
                                    className="form-control required number"
                                    style={{ fontFamily: "Helvetica" }}
                                    value={sukuBunga}
                                    onChange={(e) => {
                                      setSukuBunga(validDigits(e.target.value));
                                    }}
                                  />
                                  <div className="">
                                    <h1
                                      style={{
                                        fontSize: "14px",
                                        fontFamily: "Helvetica",
                                        fontWeight: "700",
                                        color: "#00193E",
                                      }}
                                    >
                                      % Per Tahun{" "}
                                    </h1>
                                  </div>
                                </div>
                                {sukuBungaError && <p style={{ fontFamily: "Helvetica", fontSize: 12, color: "#dc3545" }}>Isian tidak boleh kosong</p>}
                                {/* <small className="form-text text-muted">Suku Bunga Bank fix selama 20 tahun </small> */}
                              </div>
                            ) : (
                              ""
                            )}

                            {tipeKpr === "kprSyariah" &&
                              jenisSukuBunga === "flat" && (
                                <div className="form-group">
                                  <label className="form-control-label">
                                    Margin Bank Per Tahun
                                    <CustomTooltips message="Margin Bank fix selama 20 tahun" />
                                  </label>
                                  <div className="input-group search_property_bunga_floating">
                                    <input
                                      type="text"
                                      name="simulasi[sukubungafloating]"
                                      placeholder="13.5"
                                      // value="13.5"
                                      className="form-control required number revamp-sukubunga-form"
                                      value={sukuBungaFloating}
                                      onChange={(e) => {
                                        setSukuBungaFloating(
                                          validDigits(e.target.value)
                                        );
                                      }}
                                    />
                                    <div className="">
                                      <h1
                                        style={{
                                          fontSize: "14px",
                                          fontFamily: "Helvetica",
                                          fontWeight: 700,
                                          lineHeight: "18.2px",
                                          color: "#00193E",
                                        }}
                                      >
                                        % Per Tahun{" "}
                                      </h1>
                                    </div>
                                  </div>
                                  <small
                                    className="form-text"
                                    style={{
                                      fontFamily: "Helvetica",
                                      fontSize: "12px",
                                      color: "#AAAAAA !important",
                                    }}
                                  >
                                    Margin Bank fix selama 20 tahun
                                  </small>
                                </div>
                              )}

                            <div className="form-group">
                              <label className="form-control-label">
                                Lama Pinjaman
                              </label>
                              <div
                                className="input-select form-mini"
                                style={{ height: "48px" }}
                              >
                                <select
                                  className="form-select"
                                  name="simulasi[lamapinjaman]"
                                  style={{
                                    fontSize: 14,
                                    fontFamily: "Helvetica",
                                  }}
                                  value={lamaPinjaman}
                                  onChange={(e) =>
                                    setLamaPinjaman(e.target.value)
                                  }
                                >
                                  {generateTahunDropdown(maxTahunPinjaman)}
                                </select>
                              </div>
                            </div>
                            {tipeKpr === "kprKonvensional" &&
                            jenisSukuBunga !== "flat" ? (
                              <div className="form-group">
                                <label className="form-control-label">
                                  Masa Kredit Fix
                                </label>
                                <div
                                  className="input-select form-mini"
                                  style={{ height: "48px" }}
                                >
                                  <select
                                    className="form-select"
                                    name="simulasi[masakreditfix]"
                                    style={{
                                      fontSize: 14,
                                      fontFamily: "Helvetica",
                                    }}
                                    value={masaKreditFix}
                                    onChange={(e) =>
                                      setMasaKreditFix(e.target.value)
                                    }
                                    >
                                      {generateTahunDropdown(maxTahunPinjaman)}

                                  </select>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}

                            {tipeKpr === "kprKonvensional" ? (
                              <div className="form-group">
                                <label className="form-control-label">
                                  Suku Bunga Floating
                                </label>
                                <div
                                  className="input-group search_property_bunga_floating form-mini"
                                  style={{ height: "48px" }}
                                >
                                  <input
                                    type="text"
                                    name="simulasi[sukubungafloating]"
                                    placeholder="13.5"
                                    // value="13.5"
                                    className="form-control required number revamp-sukubunga-form bg-putih"
                                    value={sukuBungaFloating}
                                    onChange={(e) => {
                                      setSukuBungaFloating(
                                        validDigits(e.target.value)
                                      );
                                    }}
                                  />
                                  <div className="">
                                    <h1
                                      style={{
                                        fontSize: "14px",
                                        fontFamily: "Helvetica",
                                        fontWeight: 700,
                                        lineHeight: "18.2px",
                                        color: "#00193E",
                                      }}
                                    >
                                      % Per Tahun{" "}
                                    </h1>
                                  </div>
                                </div>
                                <small
                                  className="form-text"
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontSize: "12px",
                                    color: "#AAAAAA !important",
                                  }}
                                >
                                  Setelah 1 tahun akan mengikuti Suku Bunga Bank
                                  BI yang berlaku saat itu{" "}
                                </small>
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="col-md-12">
                            <div className="form-group mb-0">
                              <div className="show-block">
                                <button
                                  type="submit"
                                  data-style="zoom-in"
                                  className="btn btn-main btn_rounded w-100"
                                  style={{
                                    width: 276,
                                    height: 48,
                                  }}
                                  ref={resultView}
                                >
                                  <span
                                    className="ladda-label"
                                    style={{
                                      fontFamily: "Helvetica",
                                      fontSize: "14px",
                                      fontWeight: 700,
                                    }}
                                  >
                                    Hitung
                                  </span>
                                  <span className="ladda-spinner"></span>
                                  <div className="ladda-progress"></div>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* render by kondisi dr input simulasi tipe KPR */}
            {!simulasiData ? templateNoResult : templateResult}
            {/* render by kondisi dr input simulasi tipe KPR. templateNoResult if the input is nothing */}
            {/* {!router.query["ajukan[jeniskpr]"] ? templateResult : ""} */}
          </div>
        </section>
      </div>
    </Layout>
  );
}

/* -------------------------------- TOOLTIPS -------------------------------- */
const CustomTooltips = ({ message }) => (
  <OverlayTrigger
    trigger={
      typeof window !== "undefined" && window.innerWidth <= 768
        ? "click"
        : "hover"
    }
    placement={
      typeof window !== "undefined" && window.innerWidth <= 768
        ? "bottom"
        : "right"
    }
    overlay={
      <Tooltip
        id={
          typeof window !== "undefined" && window.innerWidth <= 768
            ? "Tooltip-bottom"
            : "Tooltip-right"
        }
      >
        {message}
      </Tooltip>
    }
  >
    <img
      src="/icons/icons/Vector.svg"
      data-bs-toggle="tooltip"
      data-bs-placement={
        typeof window !== "undefined" && window.innerWidth <= 768
          ? "bottom"
          : "right"
      }
      className="ms-2"
    />
  </OverlayTrigger>
);

/* ---------------------------- HELPER FUNCTIONS ---------------------------- */

const splitMonthData = (monthArray) => {
  let i,
    j,
    chunk = 12;
  let temporary = [];
  for (i = 0, j = monthArray?.length; i < j; i += chunk) {
    temporary.push(monthArray?.slice(i, i + chunk));
  }
  return temporary;
};

const getPorsiBungaPercentage = (porsiBunga, porsiPokok) => {
  return Math.round((porsiBunga / (porsiBunga + porsiPokok)) * 100);
};

const getPorsiPokokPercentage = (porsiBunga, porsiPokok) => {
  return Math.round((porsiPokok / (porsiBunga + porsiPokok)) * 100);
};

export const checkIsNumber = (value) => {
  const re = /^[0-9\b]+$/;
  if (value === "" || re.test(value)) return true;
  return false;
};

function validDigits(n) {
  return n.replace(/[^\d.]+/g, "");
}

const roundUangMuka = (res) => {
  if (!res) return
  const splitted = res.split("%")
  if(splitted.includes("NaN")){ 
    splitted[0] = 0;
  }
  else{
    splitted[0] = Math.trunc(Number(splitted[0]) * 100) / 100;
  }
  return splitted.join("%")
}
