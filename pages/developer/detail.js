import { useRouter } from "next/router";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import Link from "next/link";
import data_all from "../../sample_data/data_property_developer";
import ItemPerumahan from "../../components/data/ItemPerumahan";
import PaginationNew from "../../components/data/PaginationNew";
import { useEffect, useRef, useState } from "react";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import { useAppContext } from "../../context";
import Lottie from "react-lottie";
import styles from "../../components/element/modal_confirmation//modal_confirmation.module.scss";
import * as animationData from "../../public/animate_home.json";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../../utils/defaultHeaders";
//import Link from "next/link";

export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  // const { id } = router.query;
  const query = router.query.id;
  const { userProfile } = useAppContext();
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 768px)` });
  const closeMessage = useRef(null);

  console.log("ID", router.query);
  let id = "";
  if (query) {
    let path = query.split("-");
    id = path[path.length - 1];
  }

  /* -------------------------- FETCH DEVELOPER DATA -------------------------- */
  const [isFetching, setIsFetching] = useState(false);
  const [developerDetail, setDeveloperDetail] = useState([]);
  const [reqBody, setReqBody] = useState({
    id,
  });
  const [modalPesan, setModalPesan] = useState(false);
  const [modalSuc, setModalSuc] = useState(false);
  const [loading, setLoading] = useState(false);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  console.log("modal sukses", modalSuc);
  console.log(modalPesan);

  const closeModalPesan = () => {
    setModalPesan(false);
    setModalSuc(true);
    handleSendMessage();
    // console.log("Terima kasih telah mengirim pesan ke developer");
  };

  // FETCH ON FIRST LOAD AND WHEN reqBody is changed
  // useEffect(() => {
  //     if (id) {
  //         getDeveloperData();
  //     }
  // }, [reqBody]);

  // const getDeveloperData = async () => {
  //     try {
  //     setIsFetching(true);
  //     const endpoint = "http://172.16.3.216:8081/developer/show";
  //     const res = await fetch(endpoint, {
  //         method: 'POST',
  //         headers: getHeaderWithAccessKey(),
  //         body: new URLSearchParams(reqBody)
  //     });
  //     const resData = await res.json();
  //     if (!resData.IsError) {
  //         console.log("GET DATA", resData);
  //         setDeveloperDetail(resData.Data[0]);
  //     } else {
  //         throw {
  //         message: resData.ErrToUser
  //         };
  //     };
  //     } catch (error) {
  //     console.log(error.message);
  //     } finally {
  //     setIsFetching(false);
  //     }
  // }

  // FETCH ON FIRST LOAD AND WHEN reqBody is changed
  useEffect(() => {
    if (id) {
      getDeveloperData();
    }
  }, [id]);

  const getDeveloperData = async () => {
    try {
      setIsFetching(true);
      const params = `developer_id=${id}`;
      const endpoint =
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getDeveloperById?` +
        params;
      const res = await fetch(endpoint, {
        method: "GET",
        headers: {
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
      });
      const resData = await res.json();
      if (!resData.IsError) {
        setDeveloperDetail(resData.data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setIsFetching(false);
    }
  };

  /* --------------------------- FETCH LIST PROPERTI -------------------------- */
  const [listProperti, setListProperti] = useState([]);
  const [isFetchingProperti, setIsFetchingProperti] = useState(true);
  const [paginationData, setPaginationData] = useState({});
  const [reqBodyProperti, setReqBodyProperti] = useState({
    Page: 1,
    Limit: 9,
  });

  useEffect(() => {
    if (id) {
      getListProperti();
    }
  }, [reqBodyProperti, id]);

  const getListProperti = async () => {
    try {
      setIsFetchingProperti(true);
      setListProperti([]);
      const params = `dev_id=${id}&Page=${reqBodyProperti.Page}&Limit=${reqBodyProperti.Limit}`;
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getProperByDevId?${params}`,
        {
          method: "GET",
          headers: {
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
          },
        }
      );
      const resData = await res.json();
      setIsFetchingProperti(false);
      if (!resData.IsError) {
        setListProperti(resData.data);
        setPaginationData(resData.pagging);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log("Error fetch properti search " + error.message);
    } finally {
      // setIsFetchingProperti(false);
    }
  };

  /* ----------------------- SEND MEESSAGE TO DEVELOPER ----------------------- */
  const [judulPesan, setJudulPesan] = useState("");
  const [isiPesan, setIsiPesan] = useState("");
  const [sendMessage, setSendMessage] = useState(false);
  const { userKey } = useAppContext();

  // useEffect(() => {
  //     if (id && isiPesan && judulPesan && sendMessage) {
  //         sendMessageToDeveloper();
  //         console.log('====================================');
  //         console.log("ACC KIRIM");
  //         console.log('====================================');
  //     } else {
  //         console.log('====================================');
  //         console.log("TIDAK KIRIM");
  //         console.log('====================================');
  //     }
  // }, [sendMessage]);

  const sendMessageToDeveloper = async () => {
    try {
      setSendMessage(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/member/pesan/insert`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          i_pnrm: "D" + id,
          jdl: judulPesan,
          psn: isiPesan,
          i_pgrm: userProfile.id
        }),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        // console.log(resData.Output);
        setJudulPesan("");
        setIsiPesan("");
        closeMessage?.current?.click();
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setSendMessage(false);
    }
  };

  const handleSendMessage = () => {
    if (id && isiPesan && judulPesan) {
      sendMessageToDeveloper();
    } else {
      setModalSuc(false);
      console.log("Judul dan Pesan tidak boleh kosong!");
    }
  };

  return (
    <Layout
      title="Daftar Developer | BTN Properti"
      isLoaderOpen={isFetching || isFetchingProperti}
    >
      <Breadcrumb active={developerDetail.NAMA}>
        <li className="breadcrumb-item">
          <Link href="/developer">list developer</Link>
        </li>
      </Breadcrumb>
      <section className="mb-5">
        <div className="container" style={{ mt: "16px" }}>
          <div className="row">
            <div className="col-md-4 col-lg-3" style={{ padding: "6px" }}>
              <div className="sticky-top">
                <div className="card card_left_developer ">
                  <div className="card-body">
                    <center>
                      <img
                        src={generateImageUrl(developerDetail.LOGO)}
                        className="img-fluid"
                      />
                    </center>
                    <h5
                      className="nama"
                      style={{
                        fontFamily: "FuturaBT",
                        fontWeight: "700",
                        textAlign: "start",
                      }}
                    >
                      {developerDetail.NAMA}
                    </h5>
                    <ul className="nav flex-column nav_info">
                      <li className="nav-item">
                        {/* <Link href=""> */}
                        <a
                          className="nav-link"
                          style={{ paddingBottom: "0px" }}
                        >
                          <i style={{ paddingRight: "13px" }}>
                            <img
                              src="/images/icons/room_contact.svg"
                              style={{
                                verticalAlign: "-20px",
                                alignItems: "center",
                                height: "auto",
                                width: "auto",
                                backgroundColor: "white",
                              }}
                            />
                          </i>
                          {developerDetail.ALAMAT}
                        </a>
                        {/* </Link> */}
                      </li>
                      <li className="nav-item">
                        {/* <Link href=""> */}
                        <a
                          className="nav-link"
                          style={{ paddingTop: "0px", paddingBottom: "0px" }}
                        >
                          <i style={{ paddingRight: "13px" }}>
                            <img
                              src="/images/icons/phone_contact.svg"
                              style={{
                                verticalAlign: "-20px",
                                alignItems: "center",
                                height: "auto",
                                width: "auto",
                                backgroundColor: "white",
                              }}
                            />
                          </i>
                          {developerDetail.NO_TELP}
                        </a>
                        {/* </Link> */}
                      </li>
                      <li className="nav-item">
                        {/* <Link href=""> */}
                        <a className="nav-link" style={{ paddingTop: "0px" }}>
                          <i style={{ paddingRight: "13px" }}>
                            <img
                              src="/images/icons/email_contact.svg"
                              style={{
                                verticalAlign: "-20px",
                                alignItems: "center",
                                height: "auto",
                                width: "auto",
                                backgroundColor: "white",
                              }}
                            />
                          </i>
                          {developerDetail.EMAIL}
                        </a>
                        {/* </Link> */}
                      </li>
                    </ul>
                    <button
                      type="button"
                      className="btn btn-outline-primary btn_rounded mt-3 w-100"
                      onClick={userKey ? () => setModalPesan(true) : null}
                      data-bs-toggle={!userKey ? "modal" : null}
                      data-bs-target={userKey ? "#kirim_pesan" : "#modalLogin"}
                      style={{
                        width: "276px",
                        height: "48px",
                        fontFamily: "FuturaBT",
                        fontSize: "14px",
                        fontWeight: "700",
                      }}
                    >
                      Kirim Pesan
                    </button>
                  </div>
                </div>
                <div
                  id="newslater_id_ajukan"
                  className="card card_newslater "
                  style={{ marginTop: "16px", padding: "22px 16px", display: isTabletOrMobile ? "none" : "flex" }}
                >
                  <div
                    className="card-body card-mitra"
                    style={{
                      fontFamily: "FuturaBT",
                      fontSize: "20px",
                      fontWeight: "700",
                      fontStyle: "normal",
                      lineHeight: "150%",
                      padding: "0px",
                    }}
                  >
                    <h4
                      className="title text-start"
                      style={{
                        fontFamily: "FuturaBT",
                        fontSize: "20px",
                        fontWeight: "700",
                        fontStyle: "normal",
                        lineHeight: "150%",
                        marginBottom: "25px",
                      }}
                    >
                      Ingin Menjadi Mitra Developer BTN Properti?
                    </h4>

                    <Link href="/developer/ajukan">
                      <a
                        className="btn btn-outline-white mt-0 btn_circle "
                        style={{
                          fontFamily: "Helvetica",
                          fontStyle: "normal",
                          fontWeight: 700,
                          fontSize: 16,
                          lineHeight: "160%",
                        }}
                      >
                        Daftar Sekarang
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-8 col-lg-9" id="detailListDeveloperTab">
              <div className="tab-menu revamp-card-imgdev">
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link active"
                      data-bs-toggle="tab"
                      data-bs-target="#tab-profil"
                      role="tab"
                    >
                      <span>Profil</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link"
                      data-bs-toggle="tab"
                      data-bs-target="#tab-properti"
                      role="tab"
                    >
                      <span>List Properti</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="tab-content">
                <div
                  className="tab-pane fade show active"
                  id="tab-profil"
                  role="tabpanel"
                >
                  <h3
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontStyle: "normal",
                      fontSize: "24px",
                      lineHeight: "150%",
                      color: "#00193E",
                    }}
                  >
                    Tentang Developer
                  </h3>
                  <div className="desc">
                    <p
                      style={{
                        fontSize: "14px",
                        fontFamily: "Helvetica",
                        lineHeight: "24px",
                        color: "#00193E",
                        fontWeight: "400",
                      }}
                    >
                      {developerDetail.DESKRIPSI
                        ? developerDetail.DESKRIPSI
                        : "Tidak ada data"}
                    </p>
                  </div>
                  <h3
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontStyle: "normal",
                      fontSize: "24px",
                      lineHeight: "150%",
                      color: "#00193E",
                    }}
                  >
                    Spesialisasi dan Pelayanan
                    {/* <p /> */}
                  </h3>
                  <div className="desc">
                    <ul
                      style={{
                        fontSize: "14px",
                        fontFamily: "Helvetica",
                        lineHeight: "24px",
                        color: "#00193E",
                        fontWeight: "400",
                      }}
                    >
                      {developerDetail.SPESIALIS_PELAYANAN ? developerDetail.SPESIALIS_PELAYANAN === "[]" ? <li>Tidak ada data</li> : (
                        JSON.parse(developerDetail.SPESIALIS_PELAYANAN).map((item) => (
                          <li>{item}</li>
                        ))
                      ):(
                        <li>Tidak ada data</li>
                      )}
                    </ul>
                  </div>
                  <h3
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontStyle: "normal",
                      fontSize: "24px",
                      lineHeight: "150%",
                      color: "#00193E",
                    }}
                  >
                    Wilayah dan Tanah
                    {/* <p /> */}
                  </h3>
                  <div className="desc">
                    <ul
                      style={{
                        fontSize: "14px",
                        fontFamily: "Helvetica",
                        lineHeight: "24px",
                        color: "#00193E",
                        fontWeight: "400",
                      }}
                    >
                      {developerDetail.WILAYAH ? developerDetail.WILAYAH === "[]" ? <li>Tidak ada data</li> : (
                        JSON.parse(developerDetail.WILAYAH).map((item) => (
                          <li>{item}</li>
                        ))
                      ) : (
                        <li>Tidak ada data</li>
                      )}
                    </ul>
                  </div>
                </div>
                <div
                  className="tab-pane fade show"
                  id="tab-properti"
                  role="tabpanel"
                >
                  <h3
                    id="title_list_detail"
                    style={{
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      fontStyle: "normal",
                      fontSize: "24px",
                      lineHeight: "150%",
                      color: "#00193E",
                    }}
                  >
                    List Properti
                  </h3>
                  <h5 className="title_section"></h5>
                  <div className="row">
                    {listProperti &&
                      listProperti.map((data, index) => (
                        <div key={index} className="col-md-4 col-6 mb-4">
                          <ItemPerumahan
                            data={data}
                            action="false"
                            detail="false"
                          />
                        </div>
                      ))}
                  </div>
                  {listProperti && (
                    <PaginationNew
                      length={paginationData.jumlahHalTotal}
                      current={Number(paginationData.halKe)}
                      onChangePage={(e) => {
                        setReqBodyProperti((prevData) => ({
                          ...prevData,
                          Page: e,
                        }));
                        window.scrollTo(0, 0);
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {modalPesan && (
        <div className={styles.boxModal}>
          <div
            className={styles.boxModal__Modal}
            style={{ padding: "0px", borderRadius: "8px", marginBottom: "0px" }}
          >
            <div className="modal-content" id="kirimpesanpopup">
              <div
                className="modal-body modal-p-dev"
                id="modalpesan"
                style={{ padding: "16% 8%" }}
              >
                <div
                  style={{ position: "absolute", right: "31px", top: "31px" }}
                >
                  <div
                    onClick={() => {
                      setModalPesan(false);
                      setJudulPesan("");
                      setIsiPesan("");
                    }}
                    type="button"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  >
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                        fill="#0061A7"
                      />
                    </svg>
                  </div>
                </div>
                <div className="text-center">
                  <h5
                    className="modal-title title_page"
                    id="kirim_pesanLabel"
                    style={{ marginBottom: "16px" }}
                  >
                    Kirim Pesan
                  </h5>
                  <p
                    className="isi-form-dev"
                    style={{
                      fontFamily: "Helvetica"
                    }}
                    // style={{
                    //   marginBottom: "32px",
                    //   fontFamily: "FuturaBT",
                    //   fontSize: "16px",
                    //   fontWeight: "400",
                    // }}
                  >
                    Isi form di bawah untuk terhubung dengan developer.
                  </p>
                </div>
                <div className="floating-label-wrap mt-5">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="pesan_title"
                    placeholder="Judul"
                    required
                    value={judulPesan}
                    onChange={(e) => setJudulPesan(e.target.value)}
                    style={{
                      color: "rgb(0, 25, 62)",
                      fontFamily: "Helvetica",
                      fontSize: "14px",
                      fontWeight: "400",
                    }}
                  />
                  <label
                    htmlFor="pesan_title"
                    className="floating-label"
                    style={{
                      fontFamily: "FuturaBT",
                      fontSize: "14px",
                      fontWeight: "700",
                      textAlign: "left",
                    }}
                  >
                    Judul
                  </label>
                </div>
                <div className="floating-label-wrap  ">
                  <textarea
                    className="floating-label-field kirim-pesan floating-label-textarea"
                    placeholder="Pesan"
                    rows="5"
                    required
                    // style={{ minHeight: "206px" }}
                    style={{
                      minHeight: "208px",
                      fontFamily: "Helvetica",
                      fontSize: "14px",
                      fontWeight: "400",
                      color: "rgb(0, 25, 62)",
                      lineHeight: "22px",
                    }}
                    value={isiPesan}
                    onChange={(e) => setIsiPesan(e.target.value)}
                  ></textarea>
                  <label
                    htmlFor="pesan_title"
                    className="floating-label"
                    style={{
                      fontFamily: "FuturaBT",
                      fontSize: "14px",
                      fontWeight: "700",
                      textAlign: "left",
                    }}
                  >
                    Pesan
                  </label>
                </div>
                <button
                  type="submit"
                  className="btn btn-main button-pesan"
                  //onClick={handleSendMessage}
                  onClick={() => closeModalPesan()}
                  style={{ width: "480px", height: "48px" }}
                  disabled={!isiPesan || !judulPesan}
                >
                  {sendMessage ? "Mengirim pesan.." : "Kirim"}
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
      {modalSuc && (
        <div className={styles.boxModal} id="kirim-pesan-modal-confirm">
          <div className={`w-100 ${styles.boxModal__Modal}`}>
            <div className="text-center">
              <div className="row justify-content-end text-end">
              <button
                type="button"
                style={{ background: "none", border: "none",width:"auto" }}
                data-bs-dismiss="modal"
                onClick={() => {
                  setModalSuc(false);
                  //router.reload();
                }}
              >
                <svg
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                    fill="#0061A7"
                  />
                </svg>
              </button>
              </div>
              <h4 className="modal-title mb-3">Terima Kasih</h4>

              {loading && (
                <div
                  className="spinner-border text-primary"
                  style={{ marginLeft: 10 }}
                  role="status"
                  aria-hidden="true"
                />
              )}
              <Lottie
                options={defaultOptions}
                height={250}

                isStopped={false}
                isPaused={false}
                max-width={350}
              />
              <div
                className="desc text-center mb-5"
                style={{ fontFamily: "Helvetica", fontWeight: 400 }}
              >
                Terima kasih telah mengirim pesan ke developer.
              </div>
              <div className={`button-kembali ${styles.boxModal__Modal__Footer}`}>
                <button
                  onClick={() => {
                    setModalSuc(false);
                    router.push("/");
                  }}
                  type="buttom"
                  className="btn btn-main form-control btn-back px-5"
                  style={{
                    maxWidth: "490px",
                    height: "48px",
                    fontSize: "14px",
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    backgroundColor: "#0061A7",
                  }}
                >
                  Kembali ke Beranda
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </Layout>
  );
}

const generateImageUrl = (url) => {
  if (!url) return "/images/thumb-placeholder.png";

  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.replace(
    "1|",
    ""
  )}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/thumb-placeholder.png";
  }
};
