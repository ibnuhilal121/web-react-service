import { useEffect, useRef, useState } from "react";
import ItemDeveloper from "../../components/data/ItemDeveloper";
import PaginationNew from "../../components/data/PaginationNew";
import DeveloperFilter from "../../components/form/DeveloperFilter";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import data_developer from "../../sample_data/data_developer_all";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import NotFound from "../../components/element/NotFound";
import Link from "next/link";
import DeveloperFilterModal from "../../components/form/DeveloperFilterModal";
import router, { useRouter } from "next/router";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function index() {
  /* -------------------------- FETCH DEVELOPER DATA -------------------------- */
  const { pg } = useRouter().query
  const [isFetching, setIsFetching] = useState(false);
  const [listDeveloper, setListDeveloper] = useState([]);
  const [paginationData, setPaginationData] = useState({});
  const [searchValue, setSearchValue] = useState("");
  const [jenisProperti, setJenisProperti] = useState("1%2C2%2C6%2C7");
  const [urutkan, setUrutkan] = useState(1);
  const [reqBody, setReqBody] = useState({
    Page: 1,
    Limit: 9,
    ...urutkan,
    ...jenisProperti,
  });
  const closeModalRef = useRef();

  // FETCH ON FIRST LOAD AND WHEN reqBody is changed
  useEffect(() => {
    if(pg) {
      setReqBody({
        ...reqBody,
        Page: pg
      })
    }
  }, [])

  useEffect(() => {
    router.push('/developer/?pg=' + reqBody.Page)
    getDeveloperData();
  }, [reqBody, jenisProperti, urutkan]);

  const getDeveloperData = async () => {
    try {
      setIsFetching(true);
      const params = `Page=${reqBody.Page}&Limit=9&Sort=${urutkan}&jns=${jenisProperti}&kywd=${searchValue}`;
      const endpoint =
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/developer/show?` +
        params;
      const res = await fetch(endpoint, {
        method: "GET",
        headers: {
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET
        },
      });
      const resData = await res.json();
      if (!resData.data && !resData.pagging)
        throw { message: "Maaf Keyword Yang Kamu Cari Tidak ditemukan" };
      if (!resData.IsError) {
        console.log("GET DATA", resData);
        setListDeveloper(resData.data);
        console.log("Pagination", resData.pagging);
        setPaginationData(resData.pagging);
        closeModalRef.current?.click();
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      setListDeveloper([]);
      // console.log(error.message);
    } finally {
      setIsFetching(false);
      closeModalRef.current?.click();
    }
  };

  /* ---------------------------- HANDLER FUNCTIONS --------------------------- */
  const handleSearch = () => {
    setReqBody((prevData) => ({
      Page: 1,
      kywd: searchValue,
      Limit: 9,
      ...urutkan,
      ...jenisProperti,
    }));
  };

  const pressEnter = (e) => {
    if (e.key === 'Enter') {
      handleSearch();
    };
  };

  /* ---------------------------------- VIEWS --------------------------------- */
  return (
    <Layout title="Daftar Developer | BTN Properti" isLoaderOpen={isFetching}>
      {/* <Breadcrumb classes="FuturaStyleA" active="List Developer" /> */}
      <Breadcrumb active="List Developer" />
      <section className="mb-5">
        <div className="container">
          <div className="row">
            <div className="col-md-3" style={{ padding: "6px" }}>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <div
                  className="filter-button"
                  data-bs-toggle="modal"
                  data-bs-target="#modalFilterDeveloper"
                >
                  <img
                    src="/icons/icons/icon_filter.svg"
                    className="img-fluid"
                  />
                  Filter Properti
                </div>
              </div>
              <div className="me-2 d-md-block d-none">
                <DeveloperFilter
                  setJenisProperti={setJenisProperti}
                  setUrutkan={setUrutkan}
                  setReqBody={setReqBody}
                />
              </div>
              <div className="me-2">
                <DeveloperFilterModal
                  setJenisProperti={setJenisProperti}
                  setUrutkan={setUrutkan}
                  onSearch={handleSearch}
                  closeRef={closeModalRef}
                />
              </div>
            </div>

            <div className="col-md-9">
              <div className="search_listdeveloper mb-4">
                <input
                  type="text"
                  className="form-control search_listdeveloper_input"
                  placeholder="Cari lokasi, nama perumahan, atau nama developer"
                  aria-label=""
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  onKeyPress={pressEnter}
                />
                <button
                  className="search_listdeveloper_button"
                  type="button"
                  onClick={handleSearch}
                >
                  <img src="/images/icons/Search.svg" />
                </button>
              </div>

              <div className="row">
                {listDeveloper.length !== 0 &&
                  !isFetching &&
                  listDeveloper.map((data, index) => (
                    <div key={index} className="col-6 col-md-4 mb-4">
                      <ItemDeveloper data={data} />
                    </div>
                  ))}
              </div>

              {listDeveloper.length !== 0 && !isFetching && (
                <div className="row">
                  <div className="col-12">
                    <PaginationNew
                      length={paginationData.jumlahHalTotal}
                      current={Number(reqBody.Page)}
                      onChangePage={(e) => {
                        setReqBody((prevData) => ({ ...prevData, Page: e }));
                        if (typeof window !== "undefined") {
                          window.scrollTo(0, 0);
                        }
                      }}
                    />
                  </div>
                </div>
              )}

              {listDeveloper.length === 0 && (
                <NotFound
                  message={
                    isFetching
                      ? "Sedang memuat.."
                      : "Maaf, keyword yang kamu cari tidak ditemukan"
                  }
                />
              )}
              <div
                id="card_newslater_developer_on"
                className="card card_newslater "
                style={{
                  minHeight: "183px",
                  marginTop: 16,
                  backgroundColor: "#0061a7",
                }}
              >
                <div
                  className="card-body"
                  style={{
                    fontFamily: "Futura",
                    fontSize: "20px",
                    fontWeight: "bold",
                    fontStyle: "normal",
                    lineHeight: "150%",
                  }}
                >
                  <h4
                    className="title text-start"
                    style={{
                      fontFamily: "FuturaBT",
                      fontSize: "20px",
                      fontWeight: "700",
                      fontStyle: "normal",
                      lineHeight: "150%",
                    }}
                  >
                    Ingin Menjadi Mitra Developer BTN Properti?
                  </h4>
                  <Link href="/developer/ajukan">
                    <a
                      className="btn btn-outline-white mt-38 btn_circle"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        fontFamily: "Helvetica",
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: 16,
                        lineHeight: "160%",
                        width: 163,
                        height: 50,
                        position: "static",
                        justifyContent: "center",
                        marginTop: "28px",
                        padding: "0px 0px 0px",
                      }}
                    >
                      <div>Daftar Sekarang</div>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
