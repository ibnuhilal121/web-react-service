import Link from "next/link";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import React, { useState } from "react";
import { useCallback, useEffect, createRef, useRef } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import Lottie from "react-lottie";
import styles from "../../components/element/modal_confirmation//modal_confirmation.module.scss";
import * as animationData from "../../public/animate_home.json";
import { useRouter } from "next/router";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function Ajukan() {
  const recaptchaRef = React.createRef();
  const router = useRouter();
  const [validated, setValidated] = useState(false);
  const [valueCheckbox, SetValueCheckbox] = useState("");
  const [valueNama, SetValueNama] = useState("");
  const [valueDeskripsi, SetValueDeskripsi] = useState("");
  const [valueEmail, SetValueEmail] = useState("");
  const [valueNamaKontakPerson, setValueNamaKontakPerson] = useState("");
  const [valueKontakPerson, SetValueKontakPerson] = useState("");
  const [valueNpwp, setValueNpwp] = useState("");
  const [valueAlamat, SetValueAlamat] = useState("");
  const [valueKodePos, setValueKodePos] = useState("");
  const [valueNoFax, SetvalueNoFax] = useState("");
  const [valueWebsite, SetvalueWebsite] = useState("");
  // const [valueProvinsi, SetvalueProvinsi] = useState("");
  // const [valueKota, SetvalueKota] = useState("");
  // const [valueKecamatan, SetvalueKecamatan] = useState("");
  // const [valueKelurahan, SetvalueKelurahan] = useState("");
  const [selectedImage, setSelectedImage] = useState();
  const [imageConverted, setImageConverted] = useState("");

  const [namaError, setNamaError] = useState(false);
  const [deskripsiError, setDeskripsiError] = useState(false);
  const [namaKontakError, setNamaKontakError] = useState(false);
  const [kontakError, setKontakError] = useState(false);
  const [npwpError, setNpwpError] = useState(false);
  const [alamatError, setAlamatError] = useState(false);
  const [provinsiError, setProvinsiError] = useState(false);
  const [kotaError, setKotaError] = useState(false);
  const [kecamatanError, setKecamatanError] = useState(false);
  const [kelurahanError, setKelurahanError] = useState(false);
  const [kodePosError, setKodePosError] = useState(false);
  const [persetujuanError, setPersetujuanError] = useState(false);
  const [captchaError, setCaptchaError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [imageError,setImageError] = useState(false)

  const namaInput = useRef(null);
  const emailInput = useRef(null);
  const kontakPersonInput = useRef(null);

  const [imageLoading, setImageLoading] = useState(false);
  const [verificationLoading, setVerificationLoading] = useState(false);
  const [finishLoading, setFinishLoading] = useState(false);

  const [modalSuc, setModalSuc] = useState(false);

  const [logoSize, setLogoSize] = useState({});
  const [loading, setLoading] = useState(false);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const imageChange = (e) => {
    if (!e.target.files[0]) return;

    const img = new Image();
    img.src = URL.createObjectURL(e.target.files[0]);
    img.onload = function () {
      if (this.width < 400 || this.height < 400) {
        console.log("Ukuran gambar min 400px X 400px.");
      }
    };

    if ((e.target.files[0].type.includes("jpeg") || e.target.files[0].type.includes("jpg") || e.target.files[0].type.includes("png")) && (e.target.files[0].size / (1024*1024)).toFixed(2) < 3) {
      if (e.target.files && e.target.files.length > 0) {
        img.onload = function () {
          const wid = this.width;
          const hei = this.height;
          setLogoSize({ width: wid, height: hei });
          if (wid < 400 || hei < 400) {
            console.log("Ukuran gambar min 400px X 400px.");
          } else {
            setSelectedImage(e.target.files[0]);
          }
        };
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = function () {
          setImageConverted(reader.result);
        };

        reader.onerror = function (error) {
          console.log("error attach image : ", error);
        };
      }
      setImageError(false);
    } else {
      setImageError(true)
    }
  };
  // This function will be triggered when the "Remove This Image" button is clicked
  const removeSelectedImage = () => {
    setSelectedImage();
  };

  const ChangeValueCheckbox = (e) => {
    SetValueCheckbox(e.target.checked);
    setPersetujuanError(!e.target.checked);
  };

  useEffect(() => {
    if (valueNama && valueEmail && valueKontakPerson && valueNpwp && !emailError) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  }, [valueNama, valueEmail, valueKontakPerson, valueNpwp]);

  const handleSubmit = (event) => {
    event.preventDefault();

    console.log(logoSize);
    const value = recaptchaRef.current.getValue();
    const validateError = () => {
      if (!valueNama) setNamaError(true);
      if (!valueNamaKontakPerson) setNamaKontakError("Isian tidak boleh kosong");
      if (valueNamaKontakPerson && valueNamaKontakPerson == valueNama) setNamaKontakError("Tidak boleh sama dengan nama developer");
      if (!valueEmail) setEmailError("Isian tidak boleh kosong");
      if (!valueDeskripsi) setDeskripsiError(true);
      if (!valueKontakPerson) setKontakError(true);
      if (!valueNpwp) setNpwpError("Isian tidak boleh kosong");
      if (!valueAlamat) setAlamatError(true);
      if (!selectedProvince) setProvinsiError(true);
      if (!selectedCity) setKotaError(true);
      if (!selectedKecamatan) setKecamatanError(true);
      if (!selectedKelurahan) setKelurahanError(true);
      if (!valueKodePos) setKodePosError(true);
      if (!valueCheckbox) setPersetujuanError(true);
      setCaptchaError(!value);
    };
    validateError();

    if (!value) return;
    if (
      !valueNama ||
      namaError ||
      !valueEmail ||
      emailError ||
      !valueDeskripsi ||
      deskripsiError ||
      !valueNamaKontakPerson ||
      namaKontakError ||
      !valueKontakPerson ||
      kontakError ||
      !valueNpwp ||
      npwpError ||
      !valueAlamat ||
      alamatError ||
      !selectedProvince ||
      provinsiError ||
      !selectedCity ||
      kotaError ||
      !selectedKecamatan ||
      kecamatanError ||
      !selectedKelurahan ||
      kelurahanError ||
      !valueKodePos ||
      kodePosError ||
      !valueCheckbox ||
      persetujuanError ||
      captchaError
    )
      return;

    if (imageConverted) {
      const setting = {
        Size: `${logoSize.width}x${logoSize.height}`,
        Type: selectedImage.name.split(".").pop().toLocaleLowerCase(),
        Quality: "75",
        WithWatermark: false,
        PositionWatermark: "center",
      };

      const uploadPayload = {
        FolderName: "developer-pengajuan",
        FileBase64: imageConverted.split("base64,")[1],
        TipeUpload: 1,
        Setting: JSON.stringify(setting),
      };

      setImageLoading(true);
      fetch(`${process.env.NEXT_PUBLIC_API_HOST}/image/upload`, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams(uploadPayload),
      })
        .then(async (response) => {
          const data = await response.json();
          if (!response.ok) {
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
          }
          return data;
        })
        .then((data) => {
          if (!data.IsError) {
            return data.Output;
          } else {
            throw `error upload file : ${data.ErrToUser}`;
          }
        })
        .then((output) => {
          const payload = {
            n: valueNama,
            lgo: output,
            dsk: valueDeskripsi,
            eml: valueEmail,
            no_tlp: valueKontakPerson,
            no_fax: valueNoFax,
            web: valueWebsite,
            almt: valueAlamat,
            pos: valueKodePos,
            i_prop: selectedProvince,
            i_kot: selectedCity,
            i_kec: selectedKecamatan,
            i_kel: selectedKelurahan,
            lat: "",
            lon: "",
          };

          setVerificationLoading(true);
          return fetch(`${process.env.NEXT_PUBLIC_API_HOST}/developer/insert`, {
            method: "POST",
            headers: getHeaderWithAccessKey(),
            body: new URLSearchParams(payload),
          });
        })

        .then(async (response) => {
          const data = await response.json();
          if (!response.ok) {
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
          }
          return data;
        })
        .then((data) => {
          if (!data.IsError) {
            // console.log(data.Output);
            // window.location.reload();
            getSendDeveloperFinal();
          } else {
            throw data.ErrToUser;
          }
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setImageLoading(false);
          setVerificationLoading(false);
        });
    } else {
      const payload = {
        n: valueNama,
        lgo: "",
        dsk: valueDeskripsi,
        eml: valueEmail,
        no_tlp: valueKontakPerson,
        no_fax: valueNoFax,
        web: valueWebsite,
        almt: valueAlamat,
        pos: valueKodePos,
        i_prop: selectedProvince,
        i_kot: selectedCity,
        i_kec: selectedKecamatan,
        i_kel: selectedKelurahan,
        lat: "",
        lon: "",
      };

      setVerificationLoading(true);
      fetch(`${process.env.NEXT_PUBLIC_API_HOST}/developer/insert`, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams(payload),
      })
        .then(async (response) => {
          const data = await response.json();
          setVerificationLoading(false);
          if (!response.ok) {
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
          }
          return data;
        })
        .then((data) => {
          if (!data.IsError) {
            getSendDeveloperFinal();
          } else {
            throw data.ErrToUser;
          }
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setImageLoading(false);
          setVerificationLoading(false);
        });
    }
  };

  const getSendDeveloperFinal = async () => {
    try {
      setFinishLoading(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/developer/insert`;
      const response = await fetch(endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          devname: valueNama,
          devnpwp: valueNpwp,
          address: valueAlamat,
          phone: valueKontakPerson,
          devcontactperson: valueNamaKontakPerson,
        }),
      });
      const res = await response.json();
      if (!res.IsError) {
        setModalSuc(true);
      } else {
        throw {
          message: res.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setFinishLoading(false);
    }
  };

  /* ----------------------------- FETCH ADDRESS ----------------------------- */
  const [allProvince, setAllProvince] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState("");
  const [allCity, setAllCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState("");
  const [allKecamatan, setAllKecamatan] = useState([]);
  const [selectedKecamatan, setSelectedKecamatan] = useState("");
  const [allKelurahan, setAllKelurahan] = useState([]);
  const [selectedKelurahan, setSelectedKelurahan] = useState("");
  const [allKodePos, setAllKodePos] = useState([]);
  const [selectedKodePos, setSelectedKodePos] = useState("");

  /* --------------------- GET ALL PROVINCE ON FIRST LOAD --------------------- */
  useEffect(() => {
    getAllProvinceData();
  }, []);

  const getAllProvinceData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/propinsi/show`;
      const propinsi = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          Sort: "n ASC",
        }),
      });
      const dataPropinsi = await propinsi.json();
      if (!dataPropinsi.IsError) {
        console.log("GET PROPINSI", dataPropinsi.Data);
        setAllProvince(dataPropinsi.Data);
      } else {
        throw {
          message: dataPropinsi.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  /* ------------------ GET CITIES WHEN PROVINCE IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedProvince) {
      getCityData();
    } else {
      setAllCity([]);
      setAllKecamatan([]);
      setAllKelurahan([]);
    }

    setSelectedCity("");
    setSelectedKecamatan("");
    setSelectedKelurahan("");
    setValueKodePos("");
  }, [selectedProvince]);

  const getCityData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kota/show`;
      const cities = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_prop: selectedProvince,
          Sort: "n ASC",
        }),
      });
      const dataCities = await cities.json();
      if (!dataCities.IsError) {
        console.log("GET CITY", dataCities.Data);
        setAllCity(dataCities.Data);
      } else {
        throw {
          message: dataCities.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  /* ------------------- GET KECAMATAN WHEN CITY IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedCity) {
      getKecamatanData();
    } else {
      setAllKecamatan([]);
      setAllKelurahan([]);
    }

    setSelectedKecamatan("");
    setSelectedKelurahan("");
    setValueKodePos("");
  }, [selectedCity]);

  const getKecamatanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kecamatan/show`;
      const kecamatan = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_kot: selectedCity,
          Sort: "n ASC",
        }),
      });
      const dataKecamatan = await kecamatan.json();
      if (!dataKecamatan.IsError) {
        console.log("GET KECAMATAN", dataKecamatan.Data);
        setAllKecamatan(dataKecamatan.Data);
      } else {
        throw {
          message: dataKecamatan.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  /* ------------------- GET KELURAHAN WHEN KECAMATAN IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedKecamatan) {
      getKelurahanData();
    } else {
      setAllKelurahan([]);
    }

    setSelectedKelurahan("");
    setValueKodePos("");
  }, [selectedKecamatan]);

  const getKelurahanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kelurahan/show`;
      const kelurahan = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_kec: selectedKecamatan,
          Sort: "n ASC",
        }),
      });
      const dataKelurahan = await kelurahan.json();
      if (!dataKelurahan.IsError) {
        console.log("GET KELURAHAN", dataKelurahan.Data);
        setAllKelurahan(dataKelurahan.Data);
      } else {
        throw {
          message: dataKelurahan.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  return (
    <Layout title="Daftar Developer | BTN Properti" isLoaderOpen={imageLoading || verificationLoading || finishLoading}>
      <Breadcrumb active="Pengajuan">
        <li className="breadcrumb-item">
          <Link href="/developer">List developer</Link>
        </li>
      </Breadcrumb>
      <div className="container mb-5">
        <div className="col-md-10 offset-md-1">
          <div className="text-center">
            <h3 className="title_page" style={{ marginBottom: "16px" }}>
              Formulir Pengajuan Developer
            </h3>
            <p
              style={{
                fontFamily: "Helvetica",
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "16px",
                lineHeight: "150%",
                color: "#000000",
              }}
            >
              Anda pengembang perumahan dan belum bergabung di BTN Properti? Segera daftar secara online dan tanpa biaya (gratis) !. <br />
              Input identitas Anda pada form di bawah ini dan rasakan berbagai manfaat bergabung di BTN Properti.
            </p>
          </div>
          <form id="form-ajukan-dev" className="form-default pt-4" encType="multipart/form-data" method="POST" noValidate onSubmit={handleSubmit} action="#">
            <div className="row mt-5">
              <div className="col-md-6">
                <div className="floating-label-wrap mb-4">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="ajukan_nama"
                    placeholder="Nama"
                    value={valueNama}
                    onChange={(e) => {
                      SetValueNama(e.target.value);
                      setNamaError(!e.target.value);
                      setNamaKontakError(false);
                      if (e.target.value && e.target.value == valueNamaKontakPerson) setNamaKontakError("Tidak boleh sama dengan nama developer");
                    }}
                    autoComplete="off"
                    required
                    ref={namaInput}
                  />

                  <label htmlFor="ajukan_nama" className="floating-label">
                    Nama
                  </label>
                  {namaError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <textarea
                    className="floating-label-field"
                    id="Deskripsi"
                    name="deskripsi"
                    placeholder="Deskripsi "
                    style={{ height: "208px" }}
                    required
                    defaultValue={""}
                    value={valueDeskripsi}
                    onChange={(e) => {
                      SetValueDeskripsi(e.target.value);
                      setDeskripsiError(!e.target.value);
                    }}
                  />
                  <label htmlFor="Deskripsi" className="floating-label">
                    Deskripsi
                  </label>
                  {deskripsiError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <textarea
                    className="floating-label-field"
                    id="alamat"
                    name="alamat"
                    placeholder="alamat "
                    style={{ minHeight: "208px" }}
                    required
                    defaultValue={""}
                    value={valueAlamat}
                    onChange={(e) => {
                      SetValueAlamat(e.target.value);
                      setAlamatError(!e.target.value);
                    }}
                  />
                  <label htmlFor="alamat" className="floating-label">
                    alamat
                  </label>
                  {alamatError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>

                <div className="floating-label-wrap hide-kodepos ">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="kodepos"
                    placeholder="kode pos"
                    maxLength="5"
                    required
                    value={valueKodePos}
                    onChange={(e) => {
                      const numOnly = new RegExp("^[0-9]+$");
                      const value = e.target.value;
                      if (numOnly.test(value) || value === "") {
                        setValueKodePos(value);
                        setKodePosError(!e.target.value);
                      }
                    }}
                  />
                  <label htmlFor="kodepos" className="floating-label" maxLength="5">
                    kode pos
                  </label>
                  {kodePosError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
              </div>
              <div className="col-md-6">
                <div className="floating-label-wrap mb-4">
                  <input
                    type="email"
                    className="floating-label-field"
                    id="email"
                    placeholder="Email"
                    onChange={(e) => {
                      const value = e.target.value;
                      const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                      if (!validEmail.test(value)) {
                        setEmailError("Isi dengan alamat email");
                      }
                      if (value === "") {
                        setEmailError("Isian tidak boleh kosong");
                      }
                      if (validEmail.test(value)) {
                        setEmailError(null);
                      }
                      SetValueEmail(value);
                    }}
                    autoComplete="off"
                    required
                    ref={emailInput}
                  />
                  <label htmlFor="email" className="floating-label">
                    Email
                  </label>
                  {emailError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      {emailError}
                    </p>
                  )}
                </div>

                <div className="floating-label-wrap mb-4">
                  <input type="text"
                    className="floating-label-field"
                    id="npwp"
                    placeholder="NPWP"
                    maxLength="16"
                    onChange={(e) => {
                      const numOnly = new RegExp("^[0-9]+$");
                      const value = e.target.value;
                      if (numOnly.test(value) || value === "") {
                        setValueNpwp(value);
                        if (e.target.value.length < 15) {
                          setNpwpError("Nomor NPWP minimal 15 karakter");
                        } else {
                          setNpwpError(false);
                        }
                        if (!e.target.value) {
                          setNpwpError("Isian tidak boleh kosong");
                        }
                      }
                    }}
                    value={valueNpwp}
                    autoComplete="off"
                    required
                  />
                  <label htmlFor="npwp" className="floating-label">
                    NPWP
                  </label>
                  {npwpError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      {npwpError}
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="kontak_person"
                    placeholder="Nama Kontak Person"
                    onChange={(e) => {
                      setValueNamaKontakPerson(e.target.value);
                      setNamaKontakError(false);
                      if (!e.target.value) setNamaKontakError("Isian tidak boleh kosong");
                      if (e.target.value && e.target.value == valueNama) setNamaKontakError("Tidak boleh sama dengan nama developer");
                    }}
                    value={valueNamaKontakPerson}
                    // onChange={(e) => SetValueKontakPerson(e.target.value)}
                    autoComplete="off"
                    required
                  />
                  <label htmlFor="kontak_person" className="floating-label">
                    Nama Kontak Person
                  </label>
                  {namaKontakError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      {namaKontakError}
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="kontak_person"
                    placeholder="nomor kontak person"
                    maxLength="13"
                    onChange={(e) => {
                      const numOnly = new RegExp("^[0-9]+$");
                      const value = e.target.value;
                      if (numOnly.test(value) || value === "") {
                        SetValueKontakPerson(value);
                        setKontakError(!e.target.value);
                      }
                    }}
                    value={valueKontakPerson}
                    // onChange={(e) => SetValueKontakPerson(e.target.value)}
                    autoComplete="off"
                    required
                    ref={kontakPersonInput}
                  />
                  <label htmlFor="kontak_person" className="floating-label">
                    nomor kontak person
                  </label>
                  {kontakError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="nomor_fax"
                    maxLength="11"
                    placeholder="nomor fax"
                    required
                    //maxLength="11"
                    onChange={(e) => {
                      const numOnly = new RegExp("^[0-9]+$");
                      const value = e.target.value;
                      if (numOnly.test(value) || value === "") {
                        SetvalueNoFax(value);
                      }
                    }}
                    value={valueNoFax}
                  />
                  <label htmlFor="nomor_fax" className="floating-label">
                    nomor fax
                  </label>
                </div>

                <div className="floating-label-wrap mb-4">
                  <input type="text" className="floating-label-field" id="website" placeholder="website" required value={valueWebsite} onChange={(e) => SetvalueWebsite(e.target.value)} />
                  <label htmlFor="website" className="floating-label">
                    website
                  </label>
                </div>
                <div className="floating-label-wrap 4">
                  <select
                    className="form-select floating-label-select custom-select"
                    name="ajukan[i_prop]"
                    id="provinsi"
                    placeholder="Pilih provinsi tempat tinggal Anda"
                    required
                    onChange={(e) => {
                      setSelectedProvince(e.target.value);
                      setProvinsiError(!e.target.value);
                    }}
                    value={selectedProvince}
                    style={{paddingTop: 6, paddingBottom: 6}}
                  >
                    <option value="">Pilih Provinsi </option>
                    {allProvince.length && allProvince.map((province) => <option value={province.id}>{province.n}</option>)}
                  </select>
                  <label className="floating-label">Provinsi </label>
                  {provinsiError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <select
                    className="form-select floating-label-select custom-select "
                    name="ajukan[i_kot]"
                    id="kota"
                    required
                    onChange={(e) => {
                      setSelectedCity(e.target.value);
                      setKotaError(!e.target.value);
                    }}
                    value={selectedCity}
                    style={{paddingTop: 6, paddingBottom: 6}}
                  >
                    <option value="">Pilih Kabupaten/Kota </option>
                    {allCity.length && allCity.map((city) => <option value={city.id}>{city.n}</option>)}
                  </select>
                  <label className="floating-label">Kab / Kota </label>
                  {kotaError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <select
                    className="form-select floating-label-select custom-select "
                    name="ajukan[i_kec]"
                    id="kecamatan"
                    required
                    onChange={(e) => {
                      setSelectedKecamatan(e.target.value);
                      setKecamatanError(!e.target.value);
                    }}
                    value={selectedKecamatan}
                    style={{paddingTop: 6, paddingBottom: 6}}
                  >
                    <option value="">Pilih Kecamatan </option>
                    {allKecamatan.length && allKecamatan.map((kecamatan) => <option value={kecamatan.id}>{kecamatan.n}</option>)}
                  </select>
                  <label className="floating-label">Kecamatan </label>
                  {kecamatanError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap mb-4">
                  <select
                    className="form-select floating-label-select custom-select "
                    name="ajukan[i_kel]"
                    id="kelurahan"
                    required
                    onChange={(e) => {
                      setSelectedKelurahan(e.target.value);
                      setKelurahanError(!e.target.value);
                      setValueKodePos(allKelurahan.find((kel) => kel.id == e.target.value).pos);
                      setKodePosError(false);
                    }}
                    value={selectedKelurahan}
                    style={{paddingTop: 6, paddingBottom: 6}}
                  >
                    <option value="">Pilih Desa/Kelurahan </option>
                    {allKelurahan.length && allKelurahan.map((kelurahan) => <option value={kelurahan.id}>{kelurahan.n}</option>)}
                  </select>
                  <label className="floating-label">Kelurahan </label>
                  {kelurahanError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
                <div className="floating-label-wrap tampil-kodepos ">
                  <input
                    type="text"
                    className="floating-label-field"
                    id="kodepos"
                    placeholder="kode pos"
                    maxLength="5"
                    required
                    value={valueKodePos}
                    onChange={(e) => {
                      const numOnly = new RegExp("^[0-9]+$");
                      const value = e.target.value;
                      if (numOnly.test(value) || value === "") {
                        setValueKodePos(value);
                        setKodePosError(!e.target.value);
                      }
                    }}
                  />
                  <label htmlFor="kodepos" className="floating-label" maxLength="5">
                    kode pos
                  </label>
                  {kodePosError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Isian tidak boleh kosong
                    </p>
                  )}
                </div>
              </div>
            </div>

            <div className="form-group">
              <label
                className="form-control-label"
                style={{
                  fontFamily: "FuturaBT",
                  fontStyle: "normal",
                  fontWeight: "700",
                  fontSize: "24px",
                  marginBottom: "24px",
                  lineHeight: "1.5",
                }}
              >
                Unggah Dokumen untuk Validasi Data
              </label>
              <div className="d-flex mb-5">
                {selectedImage && (
                  <div>
                    <img
                      id="ajukan_img"
                      src={URL.createObjectURL(selectedImage)}
                      className="img-fluid"
                      alt="Thumb"
                      style={{
                        borderRadius: 8,
                        border: "1px solid #AAAAAA",
                        height: "160px",
                        width: "160px",
                      }}
                    />
                    {/* <button onClick={removeSelectedImage}>Remove Image</button> */}
                  </div>
                )}
                {!selectedImage && <img id="ajukan_img" src="/images/icons/thumb.png" className="img-fluid" width="160px" style={{ width: "160px", height: "160px" }} />}
                <div style={selectedImage && {marginLeft:"28px", } }>
                  <h6
                    style={{
                      fontFamily: "FuturaBT",
                      fontStyle: "normal",
                      fontWeight: "700",
                      fontSize: "16px",
                      marginBottom: "8px",
                    }}
                  >
                    Logo Developer
                  </h6>
                  <p
                    style={{
                      fontFamily: "Helvetica",
                      fontStyle: "normal",
                      fontWeight: "400",
                      fontSize: "16px",
                      marginBottom: "16px",
                    }}
                  >
                    Upload gambar (jpg, jpeg, atau png) dengan ukuran maks 3MB.
                    <br />
                    Gambar diusahakan berbentuk persegi dengan ukuran min 400px X 400px..
                  </p>
                  {imageError && (
                    <div className="error-input-kpr mb-3">
                      <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
                      Gambar harus berupa jpg, jpeg, atau png dengan ukuran maks 3MB
                    </div>
                  )}
                  <label
                    htmlFor="upload"
                    className="btn btn-main"
                    style={{
                      width: "158px",
                      height: "48px",
                      fontFamily: "Helvetica",
                      color: "white",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    {selectedImage ? "Ganti File" : "Pilih File"}
                  </label>
                </div>
              </div>
              <input type="file" className="form-control" accept="image/jpeg,image/jpg,image/png" required="" id="upload" onChange={(e) => imageChange(e)} hidden />
            </div>

            <div className="syarat-me mt-3">
              <div className="row g-2">
                <div className="col-auto">
                  <input
                    style={{
                      width: "24px",
                      height: "24px",
                      marginRight: "8px",
                    }}
                    type="checkbox"
                    id="checkbox_syarat"
                    name="Register[syarat]"
                    // value={valueCheckbox}
                    onChange={ChangeValueCheckbox}
                    checked={valueCheckbox}
                  />
                </div>
                <div className="col">
                  <label
                    htmlFor="checkbox_syarat"
                    style={{
                      fontFamily: "Helvetica",
                      fontStyle: "normal",
                      fontWeight: "normal",
                      fontSize: "16px",
                      lineHeight: "160%",
                      color: "#00193E",
                    }}
                  >
                    <span className="">
                      Semua informasi dalam formulir online ini telah saya isi dengan lengkap dan sebenar-benarnya. Untuk ini kami menyatakan tunduk pada ketentuan-ketentuan yang berlaku di Bank BTN termasuk diantaranya untuk melakukan
                      verifikasi atas data-data yang tertulis di atas.
                    </span>
                  </label>
                  {persetujuanError && (
                    <p
                      style={{
                        color: "#dc3545",
                        fontFamily: "Helvetica",
                        fontSize: 12,
                      }}
                    >
                      Persetujuan wajib dicentang
                    </p>
                  )}
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 mt-3">
                <ReCAPTCHA ref={recaptchaRef} size="normal" sitekey="6Lel4Z4UAAAAAOa8LO1Q9mqKRUiMYl_00o5mXJrR" onChange={() => setCaptchaError(false)} />
                {captchaError && (
                  <p
                    style={{
                      color: "#dc3545",
                      fontFamily: "Helvetica",
                      fontSize: 12,
                    }}
                  >
                    Captcha wajib di centang
                  </p>
                )}
              </div>
            </div>
            <div className="d-flex mt-3 justify-content-center ajukan-developer">
              {/* <Link href="/"> */}
              <button
                type="submit"
                className="btn btn-main"
                id="submit"
                // onClick={() => setModalSuc(true)}
                data-bs-toogle="modal"
                data-bs-target="#modalSuccessRegister"
                style={{
                  width: "100%",
                  maxWidth: "480px",
                  minHeight: "48px",
                  marginTop: "50px",
                }}
                // onClick={validateForm}
                // disabled={!validated}
              >
                <span
                  className="ladda-label"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    fontSize: "14px",
                  }}
                >
                  Ajukan Sebagai Developer{" "}
                </span>
                <span className="ladda-spinner" />
              </button>
              {/* </Link> */}
            </div>
            {modalSuc && (
              <div className={`p-2 ${styles.boxModal}`} style={{zIndex:"9999"}}>
                <div className={`px-0 py-3 ${styles.boxModal__Modal}`} style={{maxHeight:"620px",top:"50%"}}>
                  <div className="text-center">
                    <button
                      type="button"
                      style={{ background: "none", border: "none",width:"100%",textAlign:"end" }}
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      data-bs-target="#modalSuccessRegister"
                      onClick={() => {
                        setModalSuc(false);
                        router.reload();
                      }}
                      className="px-4"
                    >
                      <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                          fill="#0061A7"
                        />
                      </svg>
                    </button>
                    <div className="px-2 px-md-4 ">
                    <h4 className="modal-title mb-3">Terima Kasih</h4>

                    {loading && <div className="spinner-border text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />}
                    <Lottie options={defaultOptions} height={290} width={"100%"} isStopped={false} isPaused={false} />
                    <div className="desc text-center mb-5" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                      Pengajuan Developer sudah kami terima dan akan kami lakukan proses verifikasi data yang telah anda kirimkan. Silakan periksa email secara berkala untuk melihat status pengajuan.
                    </div>
                    <div className={styles.boxModal__Modal__Footer}>
                      <Link href="/">
                        <button
                          type="buttom"
                          className="btn btn-main form-control btn-back px-5"
                          style={{
                            width: "490px",
                            height: "48px",
                            fontSize: "14px",
                            fontFamily: "Helvetica",
                            fontWeight: 700,
                            backgroundColor: "#0061A7",
                          }}
                        >
                          Kembali ke Beranda
                        </button>
                      </Link>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </Layout>
  );
}
