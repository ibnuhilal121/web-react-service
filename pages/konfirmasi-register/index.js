import Lottie from "react-lottie";
import animationData from "../../public/animate_home.json";
import { useRouter } from "next/router";
import Link from "next/link";
import Layout from "../../components/Layout";
import BreadcrumbSecondary from "../../components/section/BreadcrumbSecondary";
import { useMediaQuery } from 'react-responsive';
import React, { useEffect, useState } from "react";
import ResetPasswordNew from "../../components/auth/ResetPasswordNew"
import ConfirmSuccess from "../../components/auth/confirm-success";
import ConfirmFailed from "../../components/auth/confirm-failed";


const Success = (()=>{
    const [loadingDetails, setLoadingDetails] = useState(false);
    const [isSuccess,setIsSuccess] = useState(true);
    const router = useRouter();
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice",
        },
      };
    return(
        <Layout title="Konfirmasi Pendaftaran" isLoaderOpen={loadingDetails}>
            <BreadcrumbSecondary active="Verifikasi">
                <li
                className="breadcrumb-item d-flex justify-content-center align-items-center"
                style={{
                    fontSize: "12px",
                    fontFamily: "Helvetica",
                    fontWeight: "bold",
                    lineHeight: "150%",
                }}
                >
                    <Link
                        href="/"
                        style={{ fontFamily: "Helvetica", fontWeight: "bold" }}
                    >
                        Beranda
                    </Link>
                    <Link
                        href="/profile"
                        style={{ fontFamily: "Helvetica", fontWeight: "bold" }}
                    >
                        Member
                    </Link>
                </li>
                {/* <li className="breadcrumb-item">
                <Link href="/promo">promo</Link>
                </li> */}
            </BreadcrumbSecondary>
            <div className="row px-md-5 justify-content-center">
                <div className="col-lg-4">
                    <Lottie
                    options={defaultOptions}
                    // height={550}
                    // width={550}
                    isStopped={false}
                    isPaused={false}
                    />    
                </div>
                <div className="col-lg-5 p-5 px-lg-3">
                    {/* komponen aktivasi berhasil atau gagal  */}
                    
                    {isSuccess ? <ConfirmSuccess/> : <ConfirmFailed /> }
                </div>
               
            </div>
        </Layout>
    )
})
export default Success;