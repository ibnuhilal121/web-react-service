const validateCaptcha = (response_key) => {
  return new Promise((resolve, reject) => {
    const secret_key = '6LehLz0dAAAAADGL1lSkt6vWw7fYNR7RSsGwqF1H'

    const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${response_key}`

    fetch(url, {
      method: 'post'
    })
      .then((response) => response.json())
      .then((google_response) => {
        if (google_response.success == true) {
          resolve(true)
        } else {
          resolve(false)
        }
      })
      .catch((err) => {
        console.log(err)
        resolve(false)
      })
  })
}

export default async function captchaHandler(req, res) {
  if ((await validateCaptcha(req.headers['captcha']))) {
    res.status(200).json({valid: true})
  } else {
    res.status(200).json({valid: false})
  }
  delete req.headers['captcha']
}