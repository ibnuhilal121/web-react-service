
export const getAccessKey = () => {
    if (typeof window !== 'undefined') 
        return localStorage.getItem('accessKey');
};
 
const getHeaderWithAccessKey = () => {
    const accessKey = getAccessKey();
    return {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': process.env.APP_KEY,
    }
}

export default getHeaderWithAccessKey