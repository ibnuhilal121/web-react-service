// // Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// import FormData from 'form-data'

// import axios from "axios"
// import { createPDF } from "../../helpers/createPdf"
// import { Readable } from "stream"
// export default async function handler(req, res) {
//   let endpoint = `${process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE}/upload`
//   if(req.method == "POST") {
//     const {detailKPR, memberDetail, ekycData} = req.body
    
//     const pdfBytes = await createPDF(detailKPR?.id, detailKPR, memberDetail, null, ekycData)
    
//     const pdfStream = new Readable({
//       read() {
//         this.push(pdfBytes);
//         this.push(null);
//       },
//     });

//     const signature = JSON.stringify([{
//       email: memberDetail?.e,
//   }])
//     const formdata = new FormData()
    
//     formdata.append('document', pdfStream, {
//       contentType: 'application/pdf',
//       filename: 'test.pdf',
//     })
//     formdata.append("userId", memberDetail?.id);
//     formdata.append("kprId", detailKPR?.id);
//     formdata.append("documentOwnerName", memberDetail?.n);
//     formdata.append("signature", signature);


    
//     await axios.post(endpoint, formdata, {
//       headers: {
//         'Content-Type': `multipart/form-data; boundary=${formdata._boundary}`
//       }
//     })
//     .then(data => {
//       console.log("berhasil", data);
//       res.status(200).json(data.data)
//     })
//     .catch(err => {
//       console.log("gagal", err.response);
//       res.status(500).json(err.response.data)
//     })
    

//     // res.setHeader('Content-Type', 'application/pdf');
//     // res.setHeader('Content-Disposition', `attachment; filename=test.pdf`)
//     // res.send(pdfStream)
//     // res.status(200).json({status: "Success"})

//   }
  

// }