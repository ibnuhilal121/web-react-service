import NextAuth from "next-auth";
import { session, signIn } from "next-auth/client";
import Providers from "next-auth/providers";

const useSecureCookies = process.env.NEXTAUTH_URL.startsWith('https://');
const cookiePrefix = useSecureCookies ? '__Secure-' : '';
const tokenPrefix = useSecureCookies ? '__Host-' : '';

const providers = [
  Providers.Google({
    clientId: process.env.NEXT_PUBLIC_GOOGLE_ID,
    clientSecret: process.env.NEXT_PUBLIC_GOOGLE_SECRET,
    authorizationUrl: 'https://accounts.google.com/o/oauth2/v2/auth?prompt=consent&access_type=offline&response_type=code'
  }),
  Providers.Twitter({
    clientId: process.env.NEXT_PUBLIC_TWITTER_ID,
    clientSecret: process.env.NEXT_PUBLIC_TWITTER_SECRET
  }),
  Providers.Facebook({
    clientId: process.env.NEXT_PUBLIC_FACEBOOK_ID,
    clientSecret: process.env.NEXT_PUBLIC_FACEBOOK_SECRET
  })
];

const callbacks = {};
callbacks["jwt"] = async(token, user, account, profile, isNewUser) => {
  if (user) {
    token.uid = user.id;
    token.account = account;
  };
  return token;
  // return Promise.resolve(token);
};

callbacks["session"] = async(session, user, token) => {
  session.user.user = user;
  session.user.uid = user.uid;
  session.user.provider = user.account.provider;
  return session;
}

const cookies = {};
cookies["csrfToken"] = {
  name: `${tokenPrefix}next-auth.csrf-token`,
  options: {
    httpOnly: true,
    sameSite: 'lax',
    path: '/',
    secure: useSecureCookies,
    domain: typeof window !== 'undefined' ? window.location.hostname == 'localhost' ? 'localhost' : `.${window.location.hostname}` : null
  }
};

cookies["sessionToken"] = {
  name: `${cookiePrefix}next-auth.session-token`,
  options: {
    httpOnly: true,
    sameSite: 'lax',
    path: '/',
    secure: useSecureCookies,
    domain: typeof window !== 'undefined' ? window.location.hostname == 'localhost' ? 'localhost' : `.${window.location.hostname}` : null
  }
};

cookies["callbackUrl"] = {
  name: `${cookiePrefix}next-auth.callback-url`,
  options: {
    httpOnly: true,
    sameSite: 'lax',
    path: '/',
    secure: useSecureCookies,
    domain: typeof window !== 'undefined' ? window.location.hostname == 'localhost' ? 'localhost' : `.${window.location.hostname}` : null
  }
};

// cookies["domains"] = [ "localhost", "172.15.30.70", "btnproperti.co.id", "infosyssolusiterpadu.com" ];
// cookies["subdomains"] = true;


const options = {
  providers,
  callbacks,
  cookies
};

export default (req, res) => NextAuth(req, res, options)