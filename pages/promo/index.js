/* eslint-disable react/jsx-key */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
import ItemPromo from "../../components/data/ItemPromo";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import NotFound from "../../components/element/NotFound";
import data_promo from "../../sample_data/data_promo";
import Link from "next/link";
import { useRouter } from "next/router";
import { doFetchKategoriPromo, doFetchPromos } from "../../services/konten";
import { useEffect, useState } from "react";

import BreadcrumbSecondary from "../../components/section/BreadcrumbSecondary";
import { generateImageUrl } from "../../utils/generateImageUrl";
import { useMediaQuery } from "react-responsive";
export default function index() {
  const router = useRouter();
  const { kategori } = router.query;
  const [loading, setLoading] = useState(false);
  const [kategoriPromo, setKategoriPromo] = useState([]);
  const [promos, setPromos] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [payload, setPayload] = useState({
    Limit: 8,
    Page: 1,
    i_kat: kategori ? kategori : "",
  });
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1024px)" });

  console.log("Param", kategori);

  // HANDLER FOR STATE
  function loadingHandler(bool) {
    setLoading(bool);
  }

  const changePayload = (attribut, newVal) => {
    setPayload((prevValue) => {
      return {
        ...prevValue,
        [attribut]: newVal,
      };
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      const resKat = await doFetchKategoriPromo();
      // console.log("+++ resKat",resKat);
      setKategoriPromo(resKat.Data);
    };
    fetchData();
  }, []);

  useEffect(() => {
    // console.log("+++ payload", payload);
    const fetchData = async () => {
      const resPromos = await doFetchPromos(loadingHandler, payload);
      // console.log("+++ resPromos",resPromos);
      if (resPromos.Data) {
        const { Data, Paging } = resPromos;
        const newArr = [];
        Data.map((item) => {
          const date = new Date(item.t_pst).toLocaleString("id-ID");
          newArr.push({
            id: item.id,
            images: item.gmbr,
            title: item.jdl,
            kategori: item.n_kat,
            date: date,
            content: item.pmsg,
            seo: item.seo
          });
        });
        setPromos(newArr);
        setTotalPages(Paging.JmlHalTotal);
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
      }else{
        setPromos([]);
      }
    };
    fetchData();
  }, [payload]);

  return (
    <Layout title="Promo" isLoaderOpen={loading}>
      <BreadcrumbSecondary active="Promo" />
      <section className="mb-5">
        <div className="container">
          <ul className="nav navbar_kategori justify-content-center" style={{ minHeight: "48px" }}>
            <li className="nav-item">
              <Link
                href={{
                  pathname: "/promo",
                  // query: { kategori: "semua" },
                }}
              >
                <a
                  className={
                    payload.i_kat === "" ? "nav-link active" : "nav-link"
                  }
                  onClick={() => changePayload("i_kat", "")}
                  aria-current="page"
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 14,
                    fontStyle: "normal",
                    fontWeight: "700",
                  }}
                >
                  Semua
                </a>
              </Link>
            </li>
            {kategoriPromo.map((item) => {
              return (
                <li className="nav-item">
                  <Link
                    href={{
                      pathname: "/promo",
                      // query: { kategori:  item.n},
                    }}
                  >
                    <a
                      className={
                        payload.i_kat === item.id || kategori == item.id
                          ? "nav-link active"
                          : "nav-link"
                      }
                      aria-current="page"
                      onClick={() => changePayload("i_kat", item.id)}
                      style={{
                        fontFamily: "Helvetica",
                        fontSize: 14,
                        fontStyle: "normal",
                        fontWeight: "700",
                      }}
                    >
                      {item.n}
                    </a>
                  </Link>
                </li>
              );
            })}
          </ul>
          <div
            className="d-flex flex-column"
            style={{
              fontFamily: "Helvetica",
              fontSize: 20,
              fontStyle: "normal",
              fontWeight: "700",
              lineHeight: "150%",
            }}
          >
            <div
              id="promo_card"
              className="row p-0"
              // style={{ paddingLeft: isTabletOrMobile ? "20px" : null }}
              key={index}
            >
              { promos == 0 ? 
                <div className="mb-5">
                 <NotFound message={"Promo tidak ditemukan"} />
                </div>
                  :
                promos.map((data, index) => (
                  <div className=" p-1 col-sm-6 col-md-4 col-lg-3">
                    <ItemPromo data={data} />
                  </div>
              ))}
            </div>
            <div>
            {promos != 0 ?
              <nav aria-label="Page navigation example" id="promoPagination">
                <ul className="pagination justify-content-center">
                  <li
                    className={
                      payload.Page > 1 ? "page-item" : "page-item disabled"
                    }
                  >
                    <a
                      className="page-link"
                      href="#"
                      onClick={() => changePayload("Page", payload.Page - 1)}
                    >
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M14.94 15.06L11.8867 12L14.94 8.94L14 8L10 12L14 16L14.94 15.06Z"
                          fill="#0061A7"
                        />
                      </svg>
                    </a>
                  </li>
                  {[...Array(totalPages)].map((elementInArray, index) => (
                    <li
                      className={
                        payload.Page === index + 1
                          ? "page-item active"
                          : "page-item"
                      }
                      onClick={() => changePayload("Page", index + 1)}
                    >
                      <a className="page-link" href="#">
                        {index + 1}
                      </a>
                    </li>
                  ))}
                  <li
                    className={
                      payload.Page < totalPages
                        ? "page-item"
                        : "page-item disabled"
                    }
                  >
                    <a
                      className="page-link"
                      href="#"
                      onClick={() => changePayload("Page", payload.Page + 1)}
                    >
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M10 15.06L13.0533 12L10 8.94L10.94 8L14.94 12L10.94 16L10 15.06Z"
                          fill="#0061A7"
                        />
                      </svg>
                    </a>
                  </li>
                </ul>
              </nav>
              : ""}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
