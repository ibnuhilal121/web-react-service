import { useRouter } from "next/router";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import PromoTerkait from "../../components/section/PromoTerkait";
import ShareSosmed from "../../components/static/ShareSosmed";
import Link from "next/link";
import BreadcrumbSecondary from "../../components/section/BreadcrumbSecondary";
import { useMediaQuery } from 'react-responsive';
import React, { useEffect, useState } from "react";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1024px)' })
  const router = useRouter();
  const { id } = router.query;
  const [details, setDetails] = useState({});
  const [promoTerkait, setPromoTerkait] = useState([]);
  const [loadingDetails, setLoadingDetails] = useState(true);

  useEffect(() => {
    if (id) {
      getPromoDetails();
    }
  }, [id]);

  const getPromoDetails = async () => {
    try {
      let path = id.split('-')
      let idPromo = path[path.length - 1]
      const payload = {
        id: idPromo
      };
      setLoadingDetails(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/promo/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams(payload)
      });
      const resData = await res.json();
      if (!res.IsError) {
        console.log("GET RES", resData);
        setDetails(resData.Data[0]);
        getPromoTerkait(resData.Data[0].i_kat)
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoadingDetails(false);
    }
  };

  const getPromoTerkait = async (i_kat) => {
    try {
      setLoadingDetails(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/promo/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({ i_kat, Limit: 5 })
      });
      const resData = await res.json();
      if (!res.IsError) {
        console.log("GET TERKAIT", resData);
        
        const filtered = resData.Data.filter(i => i.id != id).splice(0, 4);
        const newArr = [];
        filtered.map((item) => {
          const date = new Date(item.t_pst).toLocaleString("id-ID");
          newArr.push({
            id: item.id,
            images: item.gmbr,
            title: item.jdl,
            kategori: item.n_kat,
            kategoriId: item.i_kat,
            date: date,
            content: item.pmsg,
          });
        });
        setPromoTerkait(newArr)
        console.log("Filtered", filtered);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoadingDetails(false);
    }
  };


  return (
    <Layout title="Promo" isLoaderOpen={loadingDetails}>
      <BreadcrumbSecondary active={details.jdl}>
        <li
          className="breadcrumb-item d-flex justify-content-center align-items-center"
          style={{
            fontSize: "12px",
            fontFamily: "Helvetica",
            fontWeight: "bold",
            lineHeight: "150%",
          }}
        >
          <Link
            href="/promo"
            style={{ fontFamily: "Helvetica", fontWeight: "bold" }}
          >
            promo
          </Link>
        </li>
        {/* <li className="breadcrumb-item">
          <Link href="/promo">promo</Link>
        </li> */}
      </BreadcrumbSecondary>
      <section id="post_detail" className="mb-5">
        <div className="container">
          <div className="row">
            <div id="title_post" className="col-md-8 offset-md-2 text-center">
              <div className="title_post">{details.jdl}</div>
              <div className="d-flex justify-content-center meta_post mt-3">
                <h6
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 14,
                    fontStyle: "normal",
                    fontWeight: "700",
                    lineHeight: "150%",
                  }}
                >
                  {details.n_kat}
                </h6>
                <div
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 14,
                    fontStyle: "normal",
                    fontWeight: "700px",
                    lineHeight: "150%",
                  }}
                >
                  / {getDate(details.t_pst)}
                </div>
              </div>
              <ShareSosmed judul={details.jdl} setCopied={() => { }} />
            </div>
            <div id="image_post" >
              {isImageValid(details.gmbr) && (
                <img
                id="imgdetail"
                  src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/` + details.gmbr.replace("1|", "")}
                />
              )}
            </div>
            <div id="content_post" className="col-md-8 offset-md-2">
              {renderHTML(details.dsk)}
              {/* <h5>
                Program Amazing June – KPR/KPA Platinum (Peruntukkan calon
                debitur)
              </h5>
              <p>
                <strong>Jenis Produk</strong>: KPR/KPA
                Indent/ReadyStock/Secondhand
                <br />
                <strong>Jangka Waktu </strong>: Sesuai ketentuan yang berlaku
                <br />
                <strong>Uang Muka </strong>: Sesuai ketentuan yang berlaku
                <br />
                <strong>Suku Bunga</strong>: Sesuai ketentuan yang berlaku
                <br />
                <strong>Gimmick Marketing </strong>:<br />
                Discount biaya Provisi 50%
                <br />
                Bebas biaya administrasi
                <br />
                Bebas biaya appraisal (management stock / appraisal internal)
                <br />
                <strong>Jangka Waktu </strong>: Berlaku untuk akad kredit s.d 30
                Juni 2021
                <br />
              </p>
              <h5>Program Amazing June – KPR/KPA Take Over</h5>
              <p>
                Program Amazing June &ndash; KPR/KPA Take Over
                <br />
                Peruntukan : KPR/KPA Take Over
                <br />
                Ketentuan :<br />
                Berlaku untuk agunan KPR/KPA Ready Stock dengan nilai plafon
                bebas dapat seusia dengan plafond awal atau dapat dilakukannya
                Top Up.
                <br />
                Pemohon telah menjadi debitur di Bank lain sekurang kurangnya 1
                (satu) tahun dan selama menjadi debitur (minimal 1 tahun
                terakhir) tidak pernah menunggak/lancar.
                <br />
                Jangka Waktu :<br />
                Maksimal 30 tahun untuk KPR
                <br />
                Maksimal 20 tahun untuk KPA
                <br />
                LTV : Sesuai dengan ketentuan yang berlaku
                <br />
                Suku Bunga : Sesuai dengan ketentuan yang berlaku.
                <br />
                Gimmick Program :<br />
                Bebas Biaya Provisi
                <br />
                Bebas Biaya Administrasi
                <br />
                Bebas Biaya Appraisal (dilakukan oleh internal Bank)
                <br />
                Hadiah Langsung Logam Mulia (Emas) s.d 5 gram dengan ketentuan
                sebagai berikut ;<br />
                Plafond Hadiah Konversi :<br />
                Mulai dari Rp 350 jt s.d Rp 500 jt ---- Maksimal Rp 1.100.000,-
                --- 1 Gram Emas
                <br />
                &gt; Rp 500 jt s.d Rp 1 Milyar ---- Maksimal Rp 2.500.000,- ---
                2 Gram Emas
                <br />
                &gt; Rp 1 Milyar ---- Maksimal Rp 5.000.000,- ---- 5 Gram Emas
                <br />
                Jangka Waktu : Berlaku untuk akad kredit s.d 30 September 2021
              </p>

              <div
                className="d-flex justify-content-center w-100"
                style={{ marginTop: "64px", width: 276, height: 48 }}
              >
                <div style={{position:"absolute", display:"flex", width: isTabletOrMobile ? "95%" : "276px"}}>
                  <Link href="/ajukan_kpr">
                    <button
                      type="submit"
                      data-style="zoom-in"
                      className="btn btn-main"
                      style={{
                        width: "100%",
                        height: "48px ",
                      }}
                    >
                      <span
                        className="ladda-label"
                        style={{
                          fontFamily: "Helvetica",
                          fontSize: "14px",
                          fontWeight: 700,
                          width: "276px",
                          //width: "48px",
                        }}
                      >
                        Ajukan KPR
                      </span>
                      <span className="ladda-spinner"></span>
                      <div className="ladda-progress"></div>
                    </button>
                  </Link>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </section>
      {promoTerkait[0] && <PromoTerkait data={promoTerkait} />}
    </Layout>
  );
}

const renderHTML = (rawHTML) =>
  React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });

const getDate = (stringDate) => {
  const date = new Date(stringDate)
  var options = {
    year: "numeric",
    month: "long",
    day: "numeric"
  };

  return date.toLocaleDateString("id", options);
}

const isImageValid = (url) => {
  if (!url) return null;

  let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + url.replace("1|", "");
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;

  return isImageExist;
};