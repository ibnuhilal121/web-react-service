import Layout from "../components/Layout";
import Breadcrumb from "../components/section/Breadcrumb";
import Link from 'next/link'

export default function index() {
    return (
        <Layout title="Sitemap">
            <Breadcrumb active="Sitemap"/>
            <div className="container">
            <div className="card mb-5">
                <div className="card-body">
                <dl>
                    <dd>
                    <ul>
                        <li>
                        <strong>Home</strong>
                        <ul>
                            <li>
                                <Link href="/">BTN Properti</Link>
                            </li>
                        </ul>
                        </li>
                        <li>
                        <strong>Pengajuan Online</strong>
                        <ul>
                            <li>
                                <Link href="/ajukan_kpr">Ajukan Kredit</Link>
                            </li>
                            <li>
                                <Link href="/blog/?pg=1&kategori=12">Tips Pengajuan KPR</Link>
                            </li>
                            <li>
                            <Link href="/property">Data Perumahan</Link>
                            </li>
                            <li>
                            <Link href="/developer">Data Developer Mitra BTN</Link>
                            </li>
                            <li>
                            <Link href="/tools/simulasi_kpr">Simulasi KPR</Link>
                            </li>
                            <li>
                            <Link href="/tools/hitung_harga">Hitung Harga Properti Maksimal</Link>
                            </li>
                        </ul>
                        </li>
                        <li>
                        <strong>Profesional</strong>
                        <ul>
                            <li>
                            <Link href="/member/profesional">Cari Profesional</Link>
                            </li>
                        </ul>
                        </li>
                        <li>
                        <strong>Berita &amp;Info</strong>
                        <ul>
                            <li>
                            <Link href="/blog/?pg=1&kategori=">Berita Terbaru</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=">Info Terbaru</Link>
                            </li>
                            <li>
                            <Link href="/promo/?kategori=2">Info Pameran</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=8">Daily Report</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=9">Makro Update</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=10">Jadwal Pelatihan</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=11">BTN Housing Index</Link>
                            </li>
                            <li>
                            <Link href="/blog#emaillangganan0">Langganan via Email</Link>
                            </li>
                        </ul>
                        </li>
                        <li>
                        <strong>Edukasi</strong>
                        <ul>
                            <li>
                            <Link href="/blog/?pg=1&kategori=4">Informasi Terkait Properti</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=1">Peraturan Pemerintah</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=2">Peraturan Bank Indonesia</Link>
                            </li>
                            <li>
                            <Link href="/blog/?pg=1&kategori=3">Peraturan Bank Tabungan Negara</Link>
                            </li>
                        </ul>
                        </li>
                        <li>
                        <strong>Tentang</strong>
                        <ul>
                            <li>
                            <Link href="/terms">Syarat &amp;Ketentuan</Link>
                            </li>
                            <li>
                            <Link href="/faq">FAQ</Link>
                            </li>
                            <li>
                            <a href="https://www.btn.co.id/id/Locations?LocationType=e8fd9eed-05f6-49e7-b894-7631893ff71c&City=&Keyword=" target="_blank" rel="noopener">Kantor Cabang</a>
                            </li>
                            <li>
                            <Link href="/sitemap">Sitemap</Link>
                            </li>
                        </ul>
                        </li>
                    </ul>
                    </dd>
                </dl>
                </div>
            </div>
            </div>
        </Layout>
    )
}
