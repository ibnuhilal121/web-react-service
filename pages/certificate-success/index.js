import React from 'react'
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import Lottie from "react-lottie";
import * as animationData from "../../public/animate_home.json";

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

function index() {
  return (
    <Layout title="Sukses Aktivasi | BTN Properti">
			<Breadcrumb active="Sukses" />
			<div className="dashboard-content mb-5">
				<div className="container">
				<div className="row justify-content-center align-items-center px-4">
					<div className="col-md-5 text-start">
					<Lottie
						options={defaultOptions}
						isStopped={false}
						isPaused={false}
					/>
					</div>
					<div className="col-md-5">
					<h1
						style={{
						fontFamily: "FuturaBT",
						fontWeight: 700,
						color: "rgb(0, 25, 62)",
						}}
					>
						Aktivasi Digital Certificate Berhasil !
					</h1>
					<p
						style={{
						fontFamily: "Helvetica",
						}}
					>
						Selesaikan verifikasi digital dokumen pengajuan di halaman profil <br/>
                        di list pengajuan.
					</p>
					<a
                        href={`/?stat=relog`}
						className="btn btn-outline-main btn_rounded  w-100"
						style={{
						maxWidth: "300px",
						padding: "10px",
						fontFamily: "Helvetica",
						fontSize: "14px",
						fontWeight: 700,
						display: "flex",
						justifyContent: "center",
                        margin: "0 auto",
						alignItems: "center",
						borderRadius: "200px",
						}}
					>
						Beranda
					</a>
					</div>
				</div>
				</div>
			</div>
			</Layout>
  )
}

export default index