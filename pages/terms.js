import Layout from "../components/Layout";
import Breadcrumb from "../components/section/Breadcrumb";
import getHeaderWithAccessKey from "../utils/getHeaderWithAccessKey";
import React, { useEffect, useState } from "react";
import FloatMenu from "../components/FloatMenu";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../utils/defaultHeaders";

const setup = {
  title: {
    text: 'Syarat dan Ketentuan',
    href: '#syarat'
  },
  lists: [
    {
      text: 'Definisi',
      href: '#definisi'
    },
    {
      text: 'Pengguna Situs BTN Properti',
      href: '#pengguna'
    },
    {
      text: 'Developer/Pengembang atau Agen BTN Properti',
      href: '#developer'
    },
    {
      text: 'Profesional',
      href: '#profesional'
    },
    {
      text: 'Transaksi Pembelian',
      href: '#pembelian'
    },
    {
      text: 'Transaksi Penjualan',
      href: '#penjualan'
    },
    {
      text: 'Komisi',
      href: '#komisi'
    },
    {
      text: 'Harga',
      href: '#harga'
    },
    {
      text: 'Konten',
      href: '#konten'
    },
    {
      text: 'Promo',
      href: '#promo'
    },
    {
      text: 'Pusat Bantuan',
      href: '#bantuan'
    },
    {
      text: 'Pelepasan',
      href: '#pelepasan'
    },
    {
      text: 'Ganti Rugi',
      href: '#ganti_rugi'
    },
    {
      text: 'Pilihan Hukum',
      href: '#pilihan_hukum'
    },
    {
      text: 'Pembaharuan',
      href: '#pembaharuan'
    },
    {
      text: 'Ketentuan Lain',
      href: '#ketentuan_lain'
    },
  ]
};

export default function index() {
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const [syarat, setSyarat] = useState({
    definisi: "",
    deskripsi: "",
    developer: "",
    ganti_rugi: "",
    harga: "",
    ketentuan_lain: "",
    komisi: "",
    konten: "",
    pelepasan: "",
    pembaharuan: "",
    pengguna_situs: "",
    pilihan_hukum: "",
    profesional: "",
    promo: "",
    pusat_bantuan: "",
    transaksi_pembelian: "",
    transaksi_penjualan: "",
  });
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getTentangData();
  }, []);

  const getTentangData = async () => {
    setLoading(true);
    try {
      // const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/app/settings/show`;
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/term/show`;

      const res = await fetch(endpoint, {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
      });
      const resData = await res.json();
      setLoading(false);
      if (!resData.IsError) {
        console.log("GET DATA", resData);
        setSyarat(resData.data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
      setLoading(false);
    } finally {
      // setLoaded(true);
    }
  };

  return (
    <Layout title="Term of Use" isLoaderOpen={isLoading}>
      <Breadcrumb active="Term of Use" />
      <section className="mb-5">
        <div className="container">
          <div className="row">
            {isTabletOrMobile ?
            (
              <FloatMenu setup={setup} />
            ) :
            (
              <div className="col-md-3">
                <div id="menu_sidebar" className="sticky-top card-term">
                  <a
                    className="list-group-item list-group-item-action no-border p-2"
                    href="#syarat"
                  >
                    <h5
                      style={{
                        fontSize: "16px",
                        marginRight: "8px",
                      }}
                    >
                      Syarat dan Ketentuan
                    </h5>
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#definisi"
                  >
                    Definisi
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#pengguna"
                  >
                    Pengguna Situs BTN Properti
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#developer"
                  >
                    Developer/Pengembang atau Agen BTN Properti
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#profesional"
                  >
                    Profesional
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#pembelian"
                  >
                    Transaksi Pembelian
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#penjualan"
                  >
                    Transaksi Penjualan
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#komisi"
                  >
                    Komisi
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#harga"
                  >
                    Harga
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#konten"
                  >
                    Konten
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#promo"
                  >
                    Promo
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#bantuan"
                  >
                    Pusat Bantuan
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#pelepasan"
                  >
                    Pelepasan
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#ganti_rugi"
                  >
                    Ganti Rugi
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#pilihan_hukum"
                  >
                    Pilihan Hukum
                  </a>
                  <a
                    className="list-group-item list-group-item-action no-border"
                    href="#pembaharuan"
                  >
                    Pembaharuan
                  </a>
                  <a
                    style={{ backgroundColor: "#fafafa", color: "#00193E" }}
                    className="list-group-item list-group-item-action no-border"
                    href="#ketentuan_lain"
                  >
                    Ketentuan Lain
                  </a>
                </div>
              </div>
            )}
            <div className="col-md-9 term-content">
              <div
                data-bs-spy="scroll"
                data-bs-target="#menu_sidebar"
                data-bs-offset="0"
                className="scrollspy-example"
                tabIndex="0"
                //dangerouslySetInnerHTML={{__html: `${syarat}`}}
              >
                
                {/* {syarat} */}

                <h2 className="term_title" id="syarat">
                  Syarat dan Ketentuan
                </h2>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.deskripsi}`}}></p>
                {/* <p>
                  Selamat datang di{" "}
                  <a
                    style={{ color: "#0061A7", fontWeight: "bold" }}
                    href="www.btnproperti.co.id."
                  >
                    www.btnproperti.co.id
                  </a>
                  .
                </p>
                <p>
                  Syarat &ketentuan yang ditetapkan di bawah ini mengatur
                  pemakaian jasa yang ditawarkan oleh PT. Bank Tabungan Negara
                  (Persero) Tbk. terkait penggunaan situs www.btnproperti.co.id.
                  Pengguna disarankan membaca dengan seksama karena dapat
                  berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
                </p>
                <p>
                  Dengan mendaftar dan/atau menggunakan situs
                  www.btnpropert.co.id, maka pengguna dianggap telah membaca,
                  mengerti, memahami dan menyutujui semua isi dalam Syarat
                  &ketentuan. Syarat &ketentuan ini merupakan bentuk kesepakatan
                  yang dituangkan dalam sebuah perjanjian yang sah antara
                  Pengguna dengan PT. Bank Tabungan Negara (Persero) Tbk. Jika
                  pengguna tidak menyetujui salah satu, sebagian, atau seluruh
                  isi Syarat &ketentuan, maka pengguna tidak diperkenankan
                  menggunakan layanan di{" "}
                  <a
                    style={{ color: "#0061A7", fontWeight: "bold" }}
                    href="www.btnproperti.co.id."
                  >
                    www.btnproperti.co.id
                  </a>
                  .
                </p> */}

                <h4 id="definisi">A. Definisi</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.definisi}`}}>
                  {/* <ol>
                    <li>
                      Bank Tabungan Negara (Persero) Tbk. adalah Badan Usaha
                      Milik Negara yang bergerak di bidang jasa perbankan yang
                      menyediakan jasa layanan perbankan termasuk namun tidak
                      terbatas pada layanan dana dan Kredit/Pembiayaan Konsumer.
                      Selanjutnya disebut Bank BTN.
                    </li>
                    <li>Situs BTN Properti adalah www.btnproperti.co.id.</li>
                    <li>
                      Syarat &ketentuan adalah perjanjian antara Pengguna dan
                      Bank BTN yang berisikan seperangkat peraturan yang
                      mengatur hak, kewajiban, tanggung jawab pengguna dan Bank
                      BTN, serta tata cara penggunaan sistem layanan BTN
                      Properti.
                    </li>
                    <li>
                      Pengguna adalah pihak yang menggunakan layanan BTN
                      Properti, termasuk namun tidak terbatas pada pembeli,
                      penjual, developer/pengembang, agen, professional, ataupun
                      pihak lain yang sekedar berkunjung ke Situs BTN Properti.
                    </li>
                    <li>
                      Pembeli adalah Pengguna terdaftar yang melakukan
                      permintaan atas properti (rumah ataupun apartemen) yang
                      dijual oleh Developer/Pengembang di Situs BTN Properti.
                    </li>
                    <li>
                      Penjual adalah Pengguna terdaftar yang melakukan tindakan
                      melakukan penawaran atas suatu properti (rumah ataupun
                      apartemen) kepada para Pengguna Situs BTN Properti.
                    </li>
                    <li>
                      Developer/pengembang adalah pihak ketiga rekanan Bank BTN
                      yang menjual dan mempromosikan unit propertinya (rumah
                      ataupun apartemen) melalui situs BTN Properti.
                    </li>
                    <li>
                      Agen adalah pengguna terdaftar yang menjual dan
                      mempromosikan unit propertinya (rumah ataupun apartemen)
                      melalui situs BTN Properti baik yang berbentuk
                      perseorangan ataupun badan usaha.
                    </li>
                    <li>
                      Professional adalah pengguna terdaftar yang memiliki usaha
                      di bidang jasa yang memasarkan dan mempromosikan jasanya
                      melalui situs BTN Properti baik yang berbentuk
                      perseorangan ataupun badan usaha.
                    </li>
                    <li>
                      Properti adalah Rumah ataupun Apartemen yang dapat
                      dimiliki oleh seseorang yang dibuktikan secara resmi
                      dengan surat-surat kepemilikan.
                    </li>
                    <li>
                      Rekening Pengguna adalah rekening milik pengguna terdaftar
                      yang digunakan untuk melakukan transaksi pembayaran di
                      situs BTN Properti.
                    </li>
                    <li>
                      Rekening Developer/Pengembang adalah rekening milik
                      developer/pengembang rekanan Bank BTN yang terdaftar dan
                      digunakan untuk melakukan transaksi pembayaran di situs
                      BTN Properti.
                    </li>
                  </ol> */}
                </p>
                <h4 id="pengguna">B. Pengguna Situs BTN Properti</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.pengguna_situs}`}}></p>
                {/* <p>
                  Syarat dan ketentuan berikut di bawah ini berlaku pada dan
                  mengatur seluruh aktivitas yang terdapat di situs BTN
                  Properti. Mohon dibaca dengan teliti sebelum Anda mendaftarkan
                  diri dan berpartisipasi pada situs BTN Properti. Dengan
                  mengakses situs BTN Properti dan bergabung di BTN Properti
                  maka Anda menyatakan bahwa Anda setuju untuk mematuhi
                  peraturan-peraturan di bawah ini&nbsp;:
                </p>

                <ol>
                  <li>
                    Pengguna dengan ini menyatakan bahwa pengguna adalah orang
                    yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah
                    perjanjian yang sah menurut hukum.
                  </li>
                  <li>
                    BTN Properti tidak mmemungut biaya pendaftaran kepada
                    pengguna.
                  </li>
                  <li>
                    Informasi terkait dengan data user merupakan rahasia user
                    &amp;admin dan tidak dapat dibagikan kecuali dengan salah
                    satu keadaan berikut berlaku :
                  </li>
                </ol>

                <ol>
                  <li>Dengan Persetujuan User</li>
                </ol>

                <p>
                  BTN Properti akan membagi informasi pribadi dengan perusahaan,
                  organisasi, dan individu di luar situs BTN Properti jika
                  mendapatkan persetujuan dari User untuk melakukannya.
                </p>

                <ol>
                  <li>Untuk Pemrosesan Eksternal</li>
                </ol>

                <p>
                  BTN Properti memberikan informasi pribadi ke afiliasi atau
                  business maupun orang tepercaya lainnya berdasarkan petunjuk
                  admin.
                </p>

                <ol>
                  <li>Untuk Tujuan Hukum</li>
                </ol>

                <p>
                  BTN Properti akan membagikan informasi pribadi dengan
                  perusahaan, organisasi, dan individu di luar situs BTN
                  Properti jika berkeyakinan dengan itikad baik bahwa akses,
                  penggunaan, penyimpanan, atau pengungkapan informasi tersebut
                  perlu untuk :
                </p>

                <ul>
                  <li>
                    Memenuhi hukum, peraturan, dan proses hukum yang berlaku
                    atau permintaan pemerintah yang wajib dipenuhi.
                  </li>
                  <li>
                    Menegakkan Persyaratan Layanan yang tersedia, termasuk
                    penyelidikan pelanggaran potensial.
                  </li>
                  <li>
                    Mendeteksi, mencegah, atau menangani penipuan, keamanan,
                    atau masalah teknis.
                  </li>
                  <li>
                    Melindungi dari ancaman terhadap hak, properti atau keamanan
                    situs BTN Properti, pengguna kami atau publik seperti yang
                    diharuskan atau diizinkan oleh hukum.
                  </li>
                </ul>

                <ol>
                  <li>
                    Untuk melindungi akun situs BTN Properti Anda, kerahasiaan
                    sandi Anda harus tetap terjaga. Anda bertanggung jawab atas
                    aktivitas yang terjadi pada atau melalui akun Anda. Anda
                    dihimbau untuk tidak menggunakan kembali sandi akun situs
                    BTN Properti Anda pada aplikasi pihak ketiga.
                  </li>
                  <li>
                    Konten data (harga, spesifikasi, tampilan dan sisa unit
                    stok) yang ada dalam situs BTN Properti dapat berubah-ubah
                    sesuai dengan kebijakan Developer/Pengembang tanpa ada
                    pemberitahuan sebelumnya.
                  </li>
                  <li>
                    Harga Unit Rumah hanya merupakan referensi. Segala perubahan
                    dan beban tambahan yang terjadi sepenuhnya akan dibebankan
                    kepada pembeli.
                  </li>
                  <li>
                    Batas waktu booking unit rumah adalah 3 (tiga) hari kerja.
                    Pengguna diwajibkan melakukan pembayaran kepada
                    Developer/Pengembang dalam waktu tersebut dan wajib
                    menghubungi Developer/Pengembang yang bersangkutan sebelum
                    dan sesudah melakukan pembayaran.
                  </li>
                  <li>
                    Resiko yang terjadi dalam proses pengajuan kredit dan
                    booking unit rumah adalah tanggung jawab pengguna tersebut,
                    dan bukan menjadi tanggungjawab BTN Properti.
                  </li>
                  <li>
                    BTN Properti tidak dapat dituntut untuk segala pernyataan,
                    kekeliruan, ketidaktepatan atau kekurangan pada segala
                    konten yang ada pada situs BTN Properti.
                  </li>
                  <li>
                    Berkaitan dengan forum/konsultasi yang ada pada situs BTN
                    Properti, BTN Properti tidak bertanggung jawab atas
                    ketepatan isi informasi dari peserta forum/konsultasi
                    tersebut, dan tidak akan mempunyai tanggung jawab hukum
                    untuk hasil diskusi di forum. Apabila Anda berpartisipasi
                    pada forum/konsultasi, Anda menjamin bahwa Anda tidak akan :
                  </li>
                </ol>

                <ul>
                  <li>
                    Merusak nama baik, mengancam, melecehkan atau menghina orang
                    lain.
                  </li>
                  <li>
                    Mengeluarkan pernyataan yang berbau SARA (Suku, Agama, Ras).
                  </li>
                  <li>
                    Menyarankan tindakan melanggar hukum atau berdiskusi yang
                    mengarahkan pada tindakan melanggar hukum.
                  </li>
                  <li>
                    Menerbitkan dan menyebarkan materi yang melanggar hak cipta
                    pihak ketiga atau melanggar hukum.
                  </li>
                  <li>
                    Menerbitkan atau menyebarkan materi dan bahasa yang bersifat
                    pornografi, vulgar dan tidak etis.
                  </li>
                </ul>

                <ol>
                  <li>
                    BTN Properti berhak untuk menghapus pesan yang dianggap
                    tidak sesuai dan melanggar konteks. Apabila Anda menemukan
                    pesan yang tidak sesuai dan melanggar, harap hubungi Admin
                    melalui Hubungi Kami.
                  </li>
                  <li>
                    Apabila Anda hendak menghubungi BTN Properti baik berupa
                    masukan, saran, pertanyaan umum, harap menggunakan fasilitas
                    hubungi kami yang ada di dalam situs BTN Properti.
                  </li>
                  <li>
                    Untuk dapat menggunakan fasilitas booking online, Anda harus
                    terdaftar sebagai anggota, setuju untuk memberikan informasi
                    yang sebenarnya. Ketika mendaftar, anda dianggap telah
                    setuju dengan aturan-aturan yang mengatur didalamnya.
                  </li>
                  <li>
                    Kami tidak melakukan updating content unit stok pada hari
                    libur dan libur nasional.
                  </li>
                  <li>
                    Apabila Anda tidak mampu memenuhi peraturan-peraturan yang
                    dicantumkan diatas, BTN Properti berhak untuk melakukan
                    tindakan lebih lanjut. Bagi para pihak yang melanggar
                    peraturan-peraturan ini dapat dihentikan keanggotaannya atau
                    aksesnya tanpa peringatan terlebih dahulu.
                  </li>
                  <li>
                    Peraturan-peraturan yang disebut di atas dapat dimodifikasi
                    dari waktu ke waktu oleh BTN Properti tanpa pemberitahuan.
                  </li>
                </ol> */}

                <h4 id="developer">
                  C. Developer/Pengembang atau Agen BTN Properti
                </h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.developer}`}}></p>
                {/* <p>
                  Syarat dan ketentuan berikut di bawah ini berlaku pada dan
                  mengatur seluruh aktivitas yang terdapat di situs BTN
                  Properti. Mohon dibaca dengan teliti sebelum berpartisipasi
                  pada situs BTN Properti. Dengan mengakses situs BTN Properti
                  dan bergabung di BTN Properti, maka Developer/Pengembang atau
                  agen menyatakan setuju untuk mematuhi peraturan-peraturan di
                  bawah ini :
                </p>

                <ol>
                  <li>Ruang Lingkup Developer/Pengembang atau agen:</li>
                </ol>

                <ul>
                  <li>Melakukan Update dan Upload unit stok properti.</li>
                  <li>
                    Melakukan Respond dan Monitoring aktivitas booking unit stok
                    properti.
                  </li>
                  <li>Monitoring status unit stok properti.</li>
                </ul>

                <ol>
                  <li>
                    Developer/Pengembang atau agen wajib melakukan pergantian
                    password dengan password pribadi pada saat mendapatkan
                    user-ID pada kesempatan pertama.
                  </li>
                  <li>
                    Developer/Pengembang atau agen harus memelihara, menyimpan,
                    dan melindungi user-ID serta password pribadi yang menjadi
                    tanggungjawabnya.
                  </li>
                  <li>
                    Developer/Pengembang atau agen bertanggung jawab atas semua
                    kegiatan/aktivitas di sistem aplikasi yang dilakukan di
                    bawah user-ID dan password-nya.
                  </li>
                  <li>
                    Developer/Pengembang atau agen harus memilih password yang
                    efektif sehingga sulit ditebak/disingkap serta
                    memperlakukannya secara rahasia agar terhindar terhadap
                    penyalahgunaan user-ID yang tidak berhak.
                  </li>
                  <li>
                    Menghubungi Kantor Cabang Bank BTN atau Admin BTN Properti
                    untuk melakukan reset password, apabila user-ID
                    terkunci/terlupa disertai dengan surat resmi yang
                    ditandatangani oleh Direktur/Pimpinan Developer/Pengembang
                    atau agen.
                  </li>
                  <li>
                    Developer/Pengembang atau agen wajib melaporkan setiap kali
                    terjadi kasus pembobolan pengamanan pada kesempatan pertama
                    kepada Kantor Cabang Bank BTN atau Admin BTN Properti.
                  </li>
                  <li>
                    Memastikan dan bertanggung jawab atas kebenaran konten data
                    unit stok (harga, informasi spesifikasi, tampilan dan
                    status) yang ada dalam situs BTN Properti.
                  </li>
                  <li>
                    Menghubungi Kantor Cabang bank BTN yang sudah bekerja sama
                    dengan Developer/Pengembang atau agen sesuai dengan proyek
                    perumahannya, apabila pengembang melakukan pengkinian
                    (update) atau perubahan konten data unit stok (harga,
                    informasi spesifikasi, tampilan dan status) pada situs BTN
                    Properti sehingga perubahan yang dilakukan dapat tercantum
                    (publish) pada situs BTN Properti.
                  </li>
                  <li>
                    Melakukan perubahan status unit stok menjadi terpesan atau
                    booked atau pesana diterima pada situs BTN Properti maksimal
                    1 hari kerja setelah unit stok tersebut sudah dipesan dan
                    masih available melalui situs BTN Properti.
                  </li>
                  <li>
                    Menyampaikan informasi kelengkapan data pemesanan unit stok
                    termasuk booking fee kepada pemohon yang melakukan pemesanan
                    atau booking unit stok melalui situs BTN Properti maksimal 1
                    hari kerja.
                  </li>
                  <li>
                    Melakukan perubahan status unit stok menjadi terjual atau
                    sold pada situs BTN Properti maksimal 3 hari kerja setelah
                    unit stok tersebut terjual tanpa melalui situs BTN Properti.
                  </li>
                  <li>
                    Memastikan bahwa setiap materi yang diusulkan untuk dapat
                    tercantum (publish) di situs BTN Properti baik berupa
                    tulisan, gambar maupun materi multimedia lainnya tidak
                    melanggar hak cipta, paten, merek atau hak pribadi dan
                    kepemilikan intelektual lainnya dari pihak ketiga, dan
                    diterbitkan hanya dengan izin daripada pihak ketiga
                    tersebut.
                  </li>
                  <li>
                    Memastikan pencantuman foto pada situs BTN Properti sesuai
                    dengan ketentuan sebagai berikut :
                  </li>
                </ol>

                <ul>
                  <li>
                    Foto yang dipasang adalah benar foto properti yang
                    diiklankan atau dipasarkan (Maksimal 5 foto).
                  </li>
                  <li>
                    Foto menunjukkan properti pengembang yang dijual dan
                    kualitas foto harus bagus, jelas, serta tidak pecah atau
                    buram.
                  </li>
                  <li>
                    Foto dapat berupa foto pada brosur pemasaran, foto desain,
                    foto 3D, foto bangunan yang sedang dibangun dan denah rumah,
                    serta siteplan pada satu lokasi perumahan.
                  </li>
                  <li>
                    Tidak diperbolehkan menyampaikan beberapa foto (multiple
                    foto thumbnail) dalam satu foto file.
                  </li>
                </ul>

                <p style={{ fontWeight: "700" }}>
                  Developer/Pengembang atau agen dilarang
                </p>

                <p>Menggunakan user yang digunakan untuk :</p>

                <ul>
                  <li>
                    Merusak nama baik, mengancam, melecehkan atau menghina orang
                    lain (developer/pengembang lain).
                  </li>
                  <li>
                    Mengeluarkan pernyataan yang berbau SARA (Suku, Agama, Ras).
                    <br />
                    Melakukan tindakan melanggar hukum atau menyarankan dan
                    berdiskusi yang mengarahkan pada tindakan melanggar hukum.
                  </li>
                  <li>
                    Menerbitkan dan menyebarkan materi yang melanggar hak cipta
                    pihak ketiga atau melanggar hukum.
                  </li>
                  <li>
                    Menerbitkan atau menyebarkan materi dan bahasa yang bersifat
                    pornografi, vulgar dan tidak etis.
                  </li>
                  <li>
                    Menjual unit kavling yang telah di pesan atau berstatus
                    &quot;Booked&quot; oleh user pengunjung situs BTN Properti
                    di luar situs.
                  </li>
                  <li>
                    Menyebarluaskan informasi rahasia, baik berupa data atau
                    informasi, baik financial, sharing password atau lain
                    sebagainya berkaitan dengan debitur/konsumen baik identitas,
                    finansial maupun non finansial, sebagaimana dimaksud
                    peraturan perundang-undangan yang berlaku.
                  </li>
                </ul>

                <p style={{ fontWeight: "700" }}>Sanksi</p>

                <p>
                  Apabila Developer/Pengembang atau agen tidak mampu memenuhi
                  peraturan-peraturan yang dicantumkan di atas, Admin situs BTN
                  Properti berhak untuk melakukan tindakan :
                </p>

                <ul>
                  <li>
                    Pemberitahuan secara tertulis kepada Developer/Pengembang
                    atau agen.
                  </li>
                  <li>
                    Apabila pemberitahuan tersebut tidak ditindaklanjuti dalam
                    waktu 1 hari kerja, Admin situs BTN Properti berhak
                    menghentikan keanggotaan atau akses tanpa peringatan
                    terlebih dahulu.
                  </li>
                  <li>
                    Ketentuan lain mengacu syarat dan ketentuan pada situs BTN
                    Properti dan peraturan perundangan yang berlaku.
                  </li>
                </ul> */}

                <h4 id="profesional">D. Profesional</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.profesional}`}}></p>
                {/* <p>
                  Syarat dan ketentuan berikut di bawah ini berlaku pada dan
                  mengatur seluruh aktivitas yang terdapat di situs BTN
                  Properti. Mohon dibaca dengan teliti sebelum berpartisipasi
                  pada situs BTN Properti. Dengan mengakses situs BTN Properti
                  dan bergabung di BTN Properti, maka Profesional menyatakan
                  setuju untuk mematuhi peraturan-peraturan di bawah ini :
                </p>

                <ol>
                  <li>Ruang Lingkup Profesional:</li>
                </ol>

                <ul>
                  <li>
                    Melakukan Update dan Upload Informasi Bisnis, Layanan,
                    Profil, dan Portofolio.
                  </li>
                  <li>
                    Melakukan Respond dan Monitoring aktivitas penyediaan jasa
                    dan layanan yang berkaitan dengan properti (Rumah ataupun
                    Apartemen).
                  </li>
                </ul>

                <ol>
                  <li>
                    Profesional wajib melakukan pergantian password dengan
                    password pribadi pada saat mendapatkan user-ID pada
                    kesempatan pertama.
                  </li>
                  <li>
                    Profesional harus memelihara, menyimpan, dan melindungi
                    user-ID serta password pribadi yang menjadi
                    tanggungjawabnya.
                  </li>
                  <li>
                    Profesional bertanggung jawab atas semua kegiatan/aktivitas
                    di sistem aplikasi yang dilakukan di bawah user-ID dan
                    password-nya.
                  </li>
                  <li>
                    Profesional harus memilih password yang efektif sehingga
                    sulit ditebak/disingkap serta memperlakukannya secara
                    rahasia agar terhindar terhadap penyalahgunaan user-ID yang
                    tidak berhak.
                  </li>
                  <li>
                    Menghubungi Kantor Cabang Bank BTN atau Admin BTN Properti
                    untuk melakukan reset password, apabila user-ID
                    terkunci/terlupa disertai dengan surat resmi yang
                    ditandatangani oleh Profesional.
                  </li>
                  <li>
                    Profesional wajib melaporkan setiap kali terjadi kasus
                    pembobolan pengamanan pada kesempatan pertama kepada Kantor
                    Cabang Bank BTN atau Admin BTN Properti.
                  </li>
                  <li>
                    Memastikan dan bertanggung jawab atas kebenaran konten data
                    profesional (Informasi Bisnis, Layanan, Profil, dan
                    Portofolio, Biaya) yang ada dalam situs BTN Properti.
                  </li>
                  <li>
                    Memastikan bahwa setiap materi yang diusulkan untuk dapat
                    tercantum (publish) di situs BTN Properti baik berupa
                    tulisan, gambar maupun materi multimedia lainnya tidak
                    melanggar hak cipta, paten, merek atau hak pribadi dan
                    kepemilikan intelektual lainnya dari pihak ketiga, dan
                    diterbitkan hanya dengan izin daripada pihak ketiga
                    tersebut.
                  </li>
                  <li>
                    Memastikan pencantuman foto pada situs BTN Properti sesuai
                    dengan ketentuan sebagai berikut :
                  </li>
                </ol>

                <ul>
                  <li>
                    Foto yang dipasang adalah benar foto properti yang
                    diiklankan atau dipasarkan (Maksimal 5 foto).
                  </li>
                  <li>
                    Foto menunjukkan properti pengembang yang dijual dan
                    kualitas foto harus bagus, jelas, serta tidak pecah atau
                    buram.
                  </li>
                  <li>
                    Foto dapat berupa foto pada brosur pemasaran, foto desain,
                    foto 3D, foto portofolio dan layanan.
                  </li>
                  <li>
                    Tidak diperbolehkan menyampaikan beberapa foto (multiple
                    foto thumbnail) dalam satu foto file.
                  </li>
                </ul>

                <p>Profesional dilarang</p>

                <p>Menggunakan user yang digunakan untuk :</p>

                <ul>
                  <li>
                    Merusak nama baik, mengancam, melecehkan atau menghina orang
                    lain (developer/pengembang lain).
                  </li>
                  <li>
                    Mengeluarkan pernyataan yang berbau SARA (Suku, Agama, Ras).
                  </li>
                  <li>
                    Melakukan tindakan melanggar hukum atau menyarankan dan
                    berdiskusi yang mengarahkan pada tindakan melanggar hukum.
                  </li>
                  <li>
                    Menerbitkan dan menyebarkan materi yang melanggar hak cipta
                    pihak ketiga atau melanggar hukum.
                  </li>
                  <li>
                    Menerbitkan atau menyebarkan materi dan bahasa yang bersifat
                    pornografi, vulgar dan tidak etis.
                  </li>
                  <li>
                    Menyebarluaskan informasi rahasia, baik berupa data atau
                    informasi, baik financial, sharing password atau lain
                    sebagainya berkaitan dengan debitur/konsumen baik identitas,
                    finansial maupun non finansial, sebagaimana dimaksud
                    peraturan perundang-undangan yang berlaku.
                  </li>
                </ul>

                <p>Sanksi</p>

                <p>
                  Apabila Profesional tidak mampu memenuhi peraturan-peraturan
                  yang dicantumkan di atas, Admin situs BTN Properti berhak
                  untuk melakukan tindakan:
                </p>

                <ul>
                  <li>Pemberitahuan secara tertulis kepada Profesional.</li>
                  <li>
                    Apabila pemberitahuan tersebut tidak ditindaklanjuti dalam
                    waktu 1 hari kerja, Admin situs BTN Properti berhak
                    menghentikan keanggotaan atau akses tanpa peringatan
                    terlebih dahulu.
                  </li>
                </ul>

                <p>
                  Ketentuan lain mengacu syarat dan ketentuan pada situs BTN
                  Properti dan peraturan perundangan yang berlaku.
                </p> */}

                <h4 id="pembelian">E. Transaksi Pembelian</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.transaksi_pembelian}`}}></p>
                {/* <ol>
                  <li>
                    Pengguna wajib bertransaksi melalui prosedur transaksi yang
                    telah ditetapkan oleh Bank BTN. Pengguna melakukan
                    pembayaran dengan menggunakan metode pembayaran yang
                    sebelumnya telah dipilih oleh Pengguna, dan kemudian Bank
                    BTN akan meneruskan dana ke pihak Developer/Pengembang atau
                    agen apabila tahapan transaksi pemesanan unit properti
                    (Rumah ataupun Apartemen) pada sistem BTN Properti telah
                    selesai.
                  </li>
                  <li>
                    Saat melakukan pemesanan unit properti (Rumah ataupun
                    Apartemen), Pengguna menyetujui bahwa:
                  </li>
                </ol>

                <ul>
                  <li>
                    Pengguna bertanggung jawab untuk membaca, memahami, dan
                    menyetujui informasi/deskripsi keseluruhan unit properti
                    (Rumah ataupun Apartemen) sebelum membuat tawaran atau
                    komitmen untuk melakukan pemesanan unit properti (Rumah
                    ataupun Apartemen).
                  </li>
                  <li>
                    Pengguna masuk ke dalam kontrak yang mengikat secara hukum
                    untuk memesan unit properti (Rumah ataupun Apartemen) ketika
                    Pengguna pemesanan unit properti (Rumah ataupun Apartemen).
                  </li>
                  <li>
                    Bank BTN tidak mengalihkan kepemilikan secara hukum atas
                    unit properti (Rumah ataupun Apartemen) dari
                    Developer/Pengembang atau agen kepada Pengguna.
                  </li>
                </ul>

                <ol>
                  <li>
                    Pengguna memahami dan menyetujui bahwa ketersediaan unit
                    properti (Rumah ataupun Apartemen) merupakan tanggung jawab
                    Developer/Pengembang atau agen yang menawarkan unit properti
                    (Rumah ataupun Apartemen) tersebut. Terkait ketersediaan
                    stok unit properti (Rumah ataupun Apartemen) dapat berubah
                    sewaktu-waktu, sehingga dalam keadaan stok unit properti
                    (Rumah ataupun Apartemen) kosong, maka Developer/Pengembang
                    atau agen akan menolak pemesanan, dan pembayaran atas
                    pemesanan unit properti (Rumah ataupun Apartemen) yang
                    bersangkutan dikembalikan kepada Pengguna.
                  </li>
                  <li>
                    Pengguna memahami sepenuhnya dan menyetujui bahwa segala
                    transaksi yang dilakukan antar Pengguna dan
                    Developer/Pengembang atau agen selain melalui Bank BTN atau
                    BTN Properti dan/atau tanpa sepengetahuan Bank BTN (melalui
                    fasilitas/jaringan pribadi, pengiriman pesan, pengaturan
                    transaksi khusus diluar situs Bank BTN atau upaya lainnya)
                    adalah merupakan tanggung jawab pribadi dari Pengguna.
                  </li>
                  <li>
                    Bank BTN memiliki kewenangan sepenuhnya untuk menolak
                    pembayaran pemesanan unit properti (Rumah ataupun Apartemen)
                    tanpa pemberitahuan terlebih dahulu.
                  </li>
                  <li>
                    Pembayaran oleh Pengguna wajib dilakukan segera
                    (selambat-lambatnya dalam batas waktu 3 hari) setelah
                    Pengguna melakukan pemesanan unit properti (Rumah ataupun
                    Apartemen). Jika dalam batas waktu tersebut pembayaran atau
                    konfirmasi pembayaran belum dilakukan oleh Pengguna, Bank
                    BTN memiliki kewenangan untuk membatalkan transaksi
                    dimaksud. Pengguna tidak berhak mengajukan klaim atau
                    tuntutan atas pembatalan transaksi tersebut.
                  </li>
                  <li>
                    Pengguna menyetujui untuk tidak memberitahukan atau
                    menyerahkan bukti pembayaran dan/atau data pembayaran kepada
                    pihak lain selain Bank BTN. Dalam hal terjadi kerugian
                    akibat pemberitahuan atau penyerahan bukti pembayaran
                    dan/atau data pembayaran oleh Pengguna kepada pihak lain,
                    maka hal tersebut akan menjadi tanggung jawab Pengguna.
                  </li>
                  <li>
                    Pengguna wajib melakukan konfirmasi pemesanan unit properti
                    (Rumah ataupun Apartemen) kepada Developer/Pengembang atau
                    agen dan Admin BTN Properti, setelah melakukan pembayaran
                    booking fee atas pemesanan unit properti (Rumah ataupun
                    Apartemen) untuk memastikan pemesanan unit properti (Rumah
                    ataupun Apartemen) sekaligus pengajuan KPR segera diproses
                    ke Kantor Cabang Bank BTN.
                  </li>
                  <li>
                    Setelah adanya konfirmasi pemesanan unit properti (Rumah
                    ataupun Apartemen), maka dana pihak Pembeli yang dikirimkan
                    ke Rekening resmi BTN Properti akan di lanjut kirimkan ke
                    pihak Developer/Pengembang atau agen (transaksi dianggap
                    selesai).
                  </li>
                  <li>
                    Pengguna memahami dan menyetujui bahwa setiap klaim yang
                    dilayangkan setelah adanya konfirmasi pemesanan unit
                    properti (Rumah ataupun Apartemen) adalah bukan menjadi
                    tanggung jawab Bank BTN. Kerugian yang timbul setelah adanya
                    konfirmasi pemesanan unit properti (Rumah ataupun Apartemen)
                    menjadi tanggung jawab Pengguna secara pribadi.
                  </li>
                  <li>
                    Pengguna memahami dan menyetujui bahwa setiap masalah
                    pemesanan unit properti (Rumah ataupun Apartemen) yang
                    disebabkan keterlambatan pembayaran adalah merupakan
                    tanggung jawab dari Pengguna.
                  </li>
                  <li>
                    Pengguna memahami dan menyetujui bahwa masalah keterlambatan
                    proses pembayaran dan biaya tambahan yang disebabkan oleh
                    perbedaan bank yang Pengguna pergunakan dengan bank Rekening
                    resmi BTN Properti adalah tanggung jawab Pengguna secara
                    pribadi.
                  </li>
                  <li>
                    Pengembalian dana dari Bank BTN kepada Pengguna hanya dapat
                    dilakukan jika dalam keadaan-keadaan tertentu berikut ini:
                  </li>
                </ol>

                <ul>
                  <li>
                    Kelebihan pembayaran dari Pengguna atas pemesanan unit
                    properti (Rumah ataupun Apartemen),
                  </li>
                  <li>
                    Masalah pemesanan unit properti (Rumah ataupun Apartemen)
                    telah teridentifikasi secara jelas dari Developer/Pengembang
                    atau agen yang mengakibatkan pemesanan unit properti (Rumah
                    ataupun Apartemen) tidak dapat diproses,
                  </li>
                  <li>
                    Developer/Pengembang atau agen tidak bisa menyanggupi
                    pemesanan unit properti (Rumah ataupun Apartemen) karena
                    kehabisan stok, perubahan harga, maupun penyebab lainnya,
                  </li>
                  <li>
                    Penyelesaian permasalahan melalui Pusat Bantuan berupa
                    keputusan untuk pengembalian dana kepada Pengguna atau hasil
                    keputusan dari pihak Bank BTN.
                  </li>
                </ul>

                <ol>
                  <li>
                    Apabila terjadi proses pengembalian dana, maka pengembalian
                    akan dilakukan melalui Rekening milik Pengguna yang akan
                    bertambah sesuai dengan jumlah pengembalian dana.
                  </li>
                  <li>
                    Bank BTN berwenang mengambil keputusan atas
                    permasalahan-permasalahan transaksi yang belum terselesaikan
                    akibat tidak adanya kesepakatan penyelesaian, baik antara
                    Developer/Pengembang atau agen dan Pengguna, dengan melihat
                    bukti-bukti yang ada. Keputusan Bank BTN adalah keputusan
                    akhir yang tidak dapat diganggu gugat dan mengikat pihak
                    Developer/Pengembang atau agen dan Pengguna untuk
                    mematuhinya.
                  </li>
                  <li>
                    Apabila Pengguna memilih menggunakan metode pembayaran
                    transfer bank atau virtual account, maka Pengguna akan
                    mennerima kode unik virtual account untuk mempermudah proses
                    verifikasi pembayaran. Dalam hal pembayaran telah
                    diverifikasi maka proses pembayaran terhadap pemesanan unit
                    properti (Rumah ataupun Apartemen) kan terupdate secara
                    otomatis.
                  </li>
                  <li>
                    Pengguna wajib melakukan pembayaran dengan nominal yang
                    sesuai dengan jumlah tagihan yang tertera pada halaman
                    pembayaran. Bank BTN tidak bertanggungjawab atas kerugian
                    yang dialami Pengguna apabila melakukan pembayaran yang
                    tidak sesuai dengan jumlah tagihan yang tertera pada halaman
                    pembayaran.
                  </li>
                </ol> */}

                <h4 id="penjualan">F. Transaksi Penjualan</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.transaksi_penjualan}`}}></p>
                {/* <ol>
                  <li>
                    Developer/Pengembang dan agen dilarang memanipulasi harga
                    Properti (Rumah ataupun Apartemen) dengan tujuan apapun.
                  </li>
                  <li>
                    Developer/Pengembang dan agen dilarang melakukan penawaran /
                    berdagang Properti (Rumah ataupun Apartemen) yang dilarang
                    atau yang tidak sesuai dengan ketentuan yang telah
                    ditetapkan.
                  </li>
                  <li>
                    Developer/Pengembang dan agen wajib memberikan foto dan
                    informasi produk Properti (Rumah ataupun Apartemen) dengan
                    lengkap dan jelas sesuai dengan kondisi dan kualitasnya.
                    Apabila terdapat ketidaksesuaian antara foto dan informasi
                    produk Properti (Rumah ataupun Apartemen) yang diunggah oleh
                    Developer/Pengembang dan agen dengan produk yang diterima
                    oleh Pengguna, maka Bank BTN berhak membatalkan/menahan dana
                    transaksi.
                  </li>
                  <li>
                    Dalam menggunakan Fasilitas &quot;Judul Produk/Iklan
                    Properti&quot;, &quot;Foto Produk/Iklan Properti&quot;, dan
                    &quot;Deskripsi Produk&quot;, Developer/Pengembang dan agen
                    dilarang membuat peraturan bersifat klausula baku yang tidak
                    memenuhi peraturan perundang-undangan yang berlaku di
                    Indonesia. Jika terdapat pertentangan antara catatan
                    dan/atau deskripsi produk Properti (Rumah ataupunApartemen)
                    dengan Syarat &amp;Ketentuan BTN Properti, maka peraturan
                    yang berlaku adalah Syarat &amp;Ketentuan BTN Properti.
                  </li>
                  <li>
                    Developer/Pengembang dan agen wajib memberikan balasan untuk
                    menerima atau menolak pemesanan unit Properti (Rumah ataupun
                    Apartemen) dari pihak Pengguna dalam batas waktu 3 hari
                    terhitung sejak adanya notifikasi pesanan unit property
                    (Rumah ataupun Apartemen) dari BTN Properti. Jika dalam
                    batas waktu tersebut tidak ada balasan dari
                    Developer/Pengembang dan agen maka secara otomatis pesanan
                    akan dibatalkan.
                  </li>
                  <li>
                    Demi menjaga kenyamanan Pengguna dalam bertransaksi,
                    Developer/Pengembang dan agen memahami dan menyetujui bahwa
                    Bank BTN berhak melakukan moderasi Developer/Pengembang dan
                    agen apabila Developer/Pengembang dan agen melakukan
                    penolakan, pembatalan dan/atau tidak merespon pesanan unit
                    Properti (Rumah ataupun Apartemen) milik Pengguna dengan
                    dugaan untuk memanipulasi transaksi, pelanggaran atas Syarat
                    dan Ketentuan, dan/atau kecurangan atau penyalahgunaan
                    lainnya.
                  </li>
                  <li>
                    Developer/Pengembang dan agen memahami dan menyetujui bahwa
                    pembayaran atas pemesanan unit properti (Rumah ataupun
                    Apartemen) akan dikembalikan sepenuhnya ke Pengguna apabila
                    transaksi dibatalkan dan/atau transaksi tidak berhasil
                    dan/atau ketentuan lain yang diatur dalam Syarat
                    &amp;Ketentuan.
                  </li>
                  <li>
                    Bank BTN memiliki kewenangan untuk menahan pembayaran dana
                    sampai waktu yang tidak ditentukan apabila terdapat
                    permasalahan dan klaim dari pihak Pengguna terkait proses
                    pemesanan unit property (Rumah ataupun Apartemen).
                    Pembayaran baru akan dilanjut kirimkan kepada
                    Developer/Pengembang dan agen apabila permasalahan tersebut
                    telah selesai.
                  </li>
                  <li>
                    Bank BTN berwenang untuk membatalkan transaksi dan/atau
                    menahan dana transaksi dalam hal: (i) jika properti dan
                    deskripsi properti tidak sesuai/tidak jelas dengan properti
                    yang dipesan oleh pengguna(ii) jika ditemukan adanya
                    manipulasi transaksidan/atau (iii)
                  </li>
                  <li>
                    Developer/Pengembang dan agen menyalahi transaksi yang tidak
                    sesuai dengan Syarat dan Ketentuan BTN Properti.
                    <br />
                    Developer/Pengembang dan agen memahami dan menyetujui bahwa
                    Pajak Penghasilan Developer/Pengembang dan agen akan
                    dilaporkan dan diurus sendiri oleh masing-masing
                    Developer/Pengembang dan agen sesuai dengan ketentuan pajak
                    yang berlaku di peraturan perundang-undangan di Indonesia.
                  </li>
                  <li>
                    Bank BTN berwenang mengambil keputusan atas
                    permasalahan-permasalahan transaksi yang belum terselesaikan
                    akibat tidak adanya kesepakatan penyelesaian, baik antara
                    Developer/Pengembang dan agen dan Pengguna, dengan melihat
                    bukti-bukti yang ada. Keputusan Bank BTN adalah keputusan
                    akhir yang tidak dapat diganggu gugat dan mengikat pihak
                    Developer/Pengembang dan agen dan Pengguna untuk
                    mematuhinya.
                  </li>
                </ol> */}

                <h4 id="komisi">G. Komisi</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.komisi}`}}></p>
                {/* <ol>
                  <li>
                    Hingga saat ini, Bank BTN belum memberlakukan sistem komisi
                    untuk setiap transaksi yang dilakukan melalui BTN Properti.
                  </li>
                  <li>
                    Apabila dikemudian hari akan diberlakukan sistem komisi
                    dalam transaksi melalui BTN Properti, maka akan dilakukan
                    sosialisasi terlebih dahulu jangka waktu selambat-lambatnya
                    1 (satu) pekan sebelum sistem komisi dinyatakan berlaku oleh
                    Bank BTN.
                  </li>
                </ol> */}

                <h4 id="harga">H. Harga</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.harga}`}}></p>
                {/* <ol>
                  <li>
                    Harga Properti (rumah ataupun apartemen) yang terdapat dalam
                    situs BTN Properti adalah harga yang ditetapkan oleh
                    Developer/Pengembang dan Agen. Developer/Pengembang dan Agen
                    dilarang memanipulasi harga Properti dengan cara apapun.
                  </li>
                  <li>
                    Developer/Pengembang dan Agen dilarang menetapkan harga yang
                    tidak wajar pada Properti (rumah ataupun apartemen) yang
                    ditawarkan melalui Situs BTN Properti. Bank BTN berhak untuk
                    melakukan tindakan pemeriksaan, penundaan, atau penurunan
                    konten atas dasar penetapan harga yang tidak wajar.
                  </li>
                  <li>
                    Pengguna memahami dan menyetujui bahwa kesalahan keterangan
                    harga dan informasi lainnya yang disebabkan tidak
                    terbaharuinya halaman situs BTN Properti dikarenakan
                    browser/ISP yang dipakai Pengguna adalah tanggung jawab
                    Pengguna.
                  </li>
                  <li>
                    Developer/Pengembang dan Agen memahami dan menyetujui bahwa
                    kesalahan ketik yang menyebabkan keterangan harga atau
                    informasi lain menjadi tidak benar/sesuai adalah tanggung
                    jawab Developer/Pengembang dan Agen. Perlu diingat dalam hal
                    ini, apabila terjadi kesalahan pengetikan keterangan harga
                    Properti yang tidak disengaja, Developer/Pengembang dan Agen
                    berhak menolak pesanan Properti yang dilakukan oleh
                    Pengguna.
                  </li>
                  <li>
                    Pengguna memahami dan menyetujui bahwa setiap masalah
                    dan/atau perselisihan yang terjadi akibat ketidaksepahaman
                    antara Developer/Pengembang dan Agen serta Pengguna tentang
                    harga bukanlah merupakan tanggung jawab Bank BTN.
                  </li>
                  <li>
                    Dengan melakukan pemesanan melalui BTN Properti, Pengguna
                    menyetujui untuk membayar total biaya yang harus dibayarkan
                    sebagaimana tertera dalam halaman pembayaran, yang terdiri
                    dari biaya booking fee serta biaya-biaya lain yang mungkin
                    timbul dan akan diuraikan secara tegas dalam halaman
                    pembayaran. Pengguna setuju untuk melakukan pembayaran
                    melalui metode pembayaran yang telah dipilih sebelumnya oleh
                    Pengguna.
                  </li>
                  <li>
                    Situs BTN Properti untuk saat ini hanya melayani transaksi
                    jual beli Properti dalam mata uang Rupiah.
                  </li>
                </ol> */}

                <h4 id="konten">I. Konten</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.konten}`}}></p>
                {/* <ol>
                  <li>
                    Dalam menggunakan setiap fitur dan/atau layanan BTN
                    Properti, Pengguna dilarang untuk mengunggah atau
                    mempergunakan kata-kata, komentar, gambar, atau konten
                    apapun yang mengandung unsur SARA, diskriminasi, merendahkan
                    atau menyudutkan orang lain, vulgar, bersifat ancaman, atau
                    hal-hal lain yang dapat dianggap tidak sesuai dengan nilai
                    dan norma sosial. BTN Properti berhak melakukan tindakan
                    yang diperlukan atas pelanggaran ketentuan ini, antara lain
                    penghapusan konten, moderasi konten, pemblokiran akun, dan
                    lain-lain.
                  </li>
                  <li>
                    Pengguna dilarang mempergunakan foto/gambar Properti yang
                    memiliki watermark yang menandakan hak kepemilikan orang
                    lain.
                  </li>
                  <li>
                    Penguna dengan ini memahami dan menyetujui bahwa
                    penyalahgunaan foto/gambar yang di unggah oleh Pengguna
                    adalah tanggung jawab Pengguna secara pribadi.
                  </li>
                  <li>
                    Ketika Pengguna menggungah ke Situs BTN Properti dengan
                    konten atau posting konten, Pengguna memberikan BTN Properti
                    hak non-eksklusif, di seluruh dunia, secara terus-menerus,
                    tidak dapat dibatalkan, bebas royalti, disublisensikan (
                    melalui beberapa tingkatan ) hak untuk melaksanakan setiap
                    dan semua hak cipta, publisitas , merek dagang , hak basis
                    data dan hak kekayaan intelektual yang Pengguna miliki dalam
                    konten, di media manapun yang dikenal sekarang atau di masa
                    depan. Selanjutnya, untuk sepenuhnya diizinkan oleh hukum
                    yang berlaku, Anda mengesampingkan hak moral dan berjanji
                    untuk tidak menuntut hak-hak tersebut terhadap Bank BTN.
                  </li>
                  <li>
                    Pengguna menjamin bahwa tidak melanggar hak kekayaan
                    intelektual dalam mengunggah konten Pengguna kedalam situs
                    BTN Properti. Setiap Pengguna dengan ini bertanggung jawab
                    secara pribadi atas pelanggaran hak kekayaan intelektual
                    dalam mengunggah konten di Situs BTN Properti.
                  </li>
                  <li>
                    Meskipun kami mencoba untuk menawarkan informasi yang dapat
                    diandalkan, kami tidak bisa menjanjikan bahwa produk katalog
                    properti (rumah ataupun apartemen) akan selalu akurat dan
                    terbarui, dan Pengguna setuju bahwa Pengguna tidak akan
                    meminta Bank BTN bertanggung jawab atas ketimpangan dalam
                    produk katalog properti (rumah ataupun apartemen). Produk
                    katalog properti (rumah ataupun apartemen) mungkin termasuk
                    hak cipta, merek dagang atau hak milik lainnya.
                  </li>
                  <li>
                    Konten atau materi yang akan ditampilkan atau ditayangkan
                    pada Situs BTN Properti akan tunduk pada Ketentuan Situs
                    serta peraturan hukum yang berlaku.
                  </li>
                  <li>
                    Bank BTN berhak untuk sewaktu-waktu menurunkan konten atau
                    materi yang terdapat pada BTN Properti yang dianggap
                    melanggar Syarat dan Ketentuan Situs serta peraturan hukum
                    yang berlaku.
                  </li>
                </ol> */}

                <h4 id="promo">J. Promo</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.promo}`}}></p>
                {/* <ol>
                  <li>
                    Bank BTN sewaktu-waktu dapat mengadakan kegiatan promosi
                    (selanjutnya disebut sebagai &ldquo;Promo&rdquo;) dengan
                    Syarat dan Ketentuan yang mungkin berbeda pada masing-masing
                    kegiatan Promo. Pengguna dihimbau untuk membaca dengan
                    seksama Syarat dan Ketentuan Promo tersebut.
                  </li>
                  <li>
                    Bank BTN berhak, tanpa pemberitahuan sebelumnya, melakukan
                    tindakan-tindakan yang diperlukan pencabutan Promo,
                    membatalkan transaksi, menahan dana, serta hal-hal lainnya
                    jika ditemukan adanya manipulasi dan pelanggaran maupun
                    pemanfaatan Promo untuk keuntungan pribadi Pengguna, maupun
                    indikasi kecurangan atau pelanggaran pelanggaran Syarat dan
                    Ketentuan BTN Properti dan ketentuan hukum yang berlaku di
                    wilayah negara Indonesia.
                  </li>
                  <li>
                    Pengguna hanya boleh menggunakan 1 (satu) akun BTN Properti
                    untuk mengikuti setiap promo Bank BTN. Jika ditemukan
                    pembuatan lebih dari 1 (satu) akun oleh 1 (satu) Pengguna
                    yang sama dan/atau nomor handphone yang sama dan/atau alamat
                    yang sama dan/atau ID pelanggan yang sama dan/atau identitas
                    pembayaran yang sama dan/atau riwayat transaksi yang sama,
                    maka Pengguna tidak berhak mendapatkan manfaat dari promo
                    BTN Properti.
                  </li>
                  <li>
                    Bank BTN dapat mengadakan kegiatan promosi yang bekerjasama
                    dengan pihak ketiga seperti Pengembang/Developer,
                    Professional, atau situs sejenis untuk mempromosikan program
                    promo yang sedang berjalan.
                  </li>
                  <li>
                    Bank BTN sewaktu-waktu dapat menyebarkan promo yang sedang
                    berjalan mmelalui media promosi yang dimiliki BTN Properti
                    seperti sms blast, email newsletter, dan notifikasi aplikasi
                    mobile.
                  </li>
                </ol> */}

                <h4 id="bantuan">K. Pusat Bantuan</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.pusat_bantuan}`}}></p>
                {/* <ol>
                  <li>
                    Pusat Bantuan merupakan fitur yang disediakan oleh BTN
                    Properti untuk memfasilitasi kendala dan masalah yang
                    dihadapi oleh pengguna.
                  </li>
                  <li>Pusat bantuan BTN Properti terdiri dari:</li>
                  <li>
                    Customer Service Live Chat merupakan layanan pesan
                    profesional secara online dan live di mana customer dapat
                    menanyakan sesuatu atau mengadukan suatu kendala terkait
                    produk maupun layanan yang ada dari BTN Properti.
                  </li>
                  <li>
                    Hubungi Kami merupakan layanan pesan secara online untuk
                    menanyakan suatu kendala secara detail dan lebih rini
                    mengenai kendala teknis, Pengajuan Kredit, Produk Kredit,
                    Masukan/Usulan, Pasang Banner Promo, Pengembang, dan
                    lain-lain.
                  </li>
                  <li>
                    Konsultasi merupakan layanan untuk mengajukan pertanyaan
                    seputar Konsultasi Desain, KPR, Kredit komersial,
                    Perencanaan Keuangan, dan lainnya yang akan dimunculkan di
                    halaman konsultasi sehingga bisa dilihat oleh pengguna
                    lainnya.
                  </li>
                  <li>
                    Frequently Asked Question merupakan layanan yang akan
                    menginformasikan pertanyaan yang sering ditanyakan oleh
                    pengguna. Layanan ini juga akan bertindak sebagai sarana
                    bagi pengguna untuk menginformasikan aspirasi, keluhan
                    ataupun pertanyaan seputar BTN Properti.
                  </li>
                </ol> */}

                <h4 id="pelepasan">L. Pelepasan</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.pelepasan}`}}></p>
                {/* <p>
                  Jika Anda memiliki perselisihan dengan satu atau lebih
                  pengguna, Anda melepaskan Bank BTN (termasuk Induk Perusahaan,
                  Direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan
                  dan kerugian (aktual dan tersirat) dari setiap jenis dan
                  sifatnya, yang dikenal dan tidak dikenal, yang timbul dari
                  atau dengan cara apapun berhubungan dengan sengketa tersebut.
                  Dengan demikian maka Pengguna dengan sengaja melepaskan segala
                  perlindungan hukum (yang terdapat dalam undang-undang atau
                  peraturan hukum yang lain) yang akan membatasi cakupan
                  ketentuan pelepasan ini.
                </p> */}

                <h4 id="ganti_rugi">M. Ganti Rugi</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.ganti_rugi}`}}></p>
                {/* <p>
                  Pengguna akan melepaskan Bank BTN dari tuntutan ganti rugi dan
                  menjaga Bank BTN (termasuk Induk Perusahaan, direktur, dan
                  karyawan) dari setiap klaim atau tuntutan, termasuk biaya
                  hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul
                  dalam hal Anda melanggar Perjanjian ini, penggunaan Layanan
                  BTN Properti yang tidak semestinya dan/ atau pelanggaran Anda
                  terhadap hukum atau hak-hak pihak ketiga.
                </p> */}
                <h4 id="pilihan_hukum">N. Pilihan Hukum</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.pilihan_hukum}`}}></p>
                {/* <p>
                  Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan
                  hukum Republik Indonesia, tanpa memperhatikan pertentangan
                  aturan hukum. Anda setuju bahwa tindakan hukum apapun atau
                  sengketa yang mungkin timbul dari, berhubungan dengan, atau
                  berada dalam cara apapun berhubungan dengan situs dan/atau
                  Perjanjian ini akan diselesaikan secara eksklusif dalam
                  yurisdiksi pengadilan Republik Indonesia.
                </p> */}
                <h4 id="pembaharuan">O. Pembaharuan</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.pembaharuan}`}}></p>
                {/* <p>
                  Syarat &ketentuan mungkin di ubah dan/atau diperbaharui dari
                  waktu ke waktu tanpa pemberitahuan sebelumnya. Bank BTN
                  menyarankan agar anda membaca secara seksama dan memeriksa
                  halaman Syarat &ketentuan ini dari waktu ke waktu untuk
                  mengetahui perubahan apapun. Dengan tetap mengakses dan
                  menggunakan layanan BTN Properti, maka pengguna dianggap
                  menyetujui perubahan-perubahan dalam Syarat &ketentuan.
                </p> */}

                <h4 id="ketentuan_lain">P. Ketentuan Lain</h4>
                <p dangerouslySetInnerHTML={{__html: `${syarat?.ketentuan_lain}`}}></p>
                {/* <p>
                  Segala hal yang belum dan/atau tidak diatur dalam syarat dan
                  ketentuan khusus dalam fitur tersebut maka akan sepenuhnya
                  merujuk pada syarat dan ketentuan Bank BTN dan BTN Properti
                  secara umum.
                </p> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
