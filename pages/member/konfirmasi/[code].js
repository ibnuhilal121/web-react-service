import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react'
import AktivasiMember from '../../../components/auth/AktivasiMember';
import Layout from '../../../components/Layout'
import BreadcrumbSecondary from "../../../components/section/BreadcrumbSecondary";

function index() {
    const { code } = useRouter().query

  return (
    <Layout title="aktivasi member">
       <BreadcrumbSecondary active="Aktivasi Member"  />
        <AktivasiMember code={code} />    
    </Layout>
  )
}

export default index