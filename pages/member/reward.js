import React, { useEffect, useState } from 'react';
import Link from 'next/link'
import Layout from '../../components/Layout';
import MenuMember from '../../components/MenuMember';
import Breadcrumb from '../../components/section/Breadcrumb';
import ItemReward from '../../components/data/ItemReward';
import ItemRewardPenukaran from '../../components/data/ItemRewardPenukaran';
import { useAppContext } from '../../context';
import PaginationNew from '../../components/data/PaginationNew';
import withAuth from '../../helpers/withAuth';
import { useMediaQuery } from 'react-responsive';
import { defaultHeaders } from '../../utils/defaultHeaders';
const Pencarian = () => {
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1024px)' })

    /* ------------------------------ FETCH POINT ------------------------------ */
    const { userKey } = useAppContext();
    const [loadingPoint, setLoadingPoint] = useState(true);
    const [pointData, setPointData] = useState(null);

    useEffect(() => {
        if (!userKey) return;

        fetchpointData();
    }, [userKey])

    const fetchpointData = async () => {
        try {
            setLoadingPoint(true);
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/point/total`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/x-www-form-urlencoded",
                    AccessKey_Member: userKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams({}),
            });
            const resData = await res.json();
            if (!resData.IsError) {
                setPointData(resData.Data[0]);
            } else {
                throw {
                    message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoadingPoint(false);
        }
    };


    /* ------------------------------ FETCH REWARD ------------------------------ */
    const [loadingReward, setLoadingReward] = useState(true);
    const [rewardList, setRewardList] = useState([]);


    useEffect(() => {
        if (!userKey) return;

        fetchRewardList();
    }, [userKey])

    const fetchRewardList = async () => {
        try {
            setLoadingReward(true);
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/quest/show`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/x-www-form-urlencoded",
                    AccessKey_Member: userKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams({
                    Page: 1,
                    Limit: 999,
                }),
            });
            const resData = await res.json();
            if (!resData.IsError) {
                console.log("Get reward data", resData.Data)
                setRewardList(resData.Data);

            } else {
                throw {
                    message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoadingReward(false);
        }
    };


    /* -------------------------- FETCH REWARD HISTORY -------------------------- */
    const [loadingRewardHistory, setLoadingRewardHistory] = useState(true);
    const [rewardListHistory, setRewardListHistory] = useState([]);
    const [paginationData, setPaginationData] = useState({});
    const [reqBodyHistory, setReqBodyHistory] = useState({
        Page: 1,
        Limit: 10,
        Sort: "tgl_aktivitas DESC"
    });

    useEffect(() => {
        if (userKey) {
            fetchRewardHistory();
        }
    }, [userKey, reqBodyHistory])

    const fetchRewardHistory = async () => {
        try {
            setLoadingRewardHistory(true);
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/quest/history/show`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/x-www-form-urlencoded",
                    AccessKey_Member: userKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams(reqBodyHistory),
            });
            const resData = await res.json();
            if (!resData.IsError) {
                console.log("Get reward history", resData.Data)
                setRewardListHistory(resData.Data);
                setPaginationData(resData.Paging)
            } else {
                throw {
                    message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoadingRewardHistory(false);
        }
    };

    return (
        <Layout title="Reward - Dashboard Member | BTN Properti" isLoaderOpen={loadingReward || loadingRewardHistory || loadingPoint}>
            <Breadcrumb active="Akun" />
            <div className="dashboard-content mb-5">
                <div className="container">
                    <div className="row">
                        <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
                            <MenuMember active="reward" />
                        </div>
                        <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                            <h5 style={{ fontFamily: "FuturaBT", fontSize: "32px", fontWeight: "700" }} className="title_akun">Reward</h5>
                            {isTabletOrMobile
                                ? <MobileReward pointData={pointData} />
                                : (
                                    <div className="card card_statistik_reward">
                                        <div className="card-body">
                                            <div className="row  align-items-center">
                                                <div className="col poin ps-3" style={{ height: "98px" }}>
                                                    <h5 style={{ width: "237px", fontFamily: "FuturaBT", fontSize: "20px", fontWeight: "700" }}>Poin Saya</h5>
                                                </div>
                                                <div className="col ps-5">
                                                    <small style={{ lineHeight: "160%", fontStyle: "normal", fontFamily: "Helvetica", fontSize: "12px", fontWeight: "700" }} >Poin Saat Ini</small>
                                                    <h6 style={{ fontStyle: "normal", fontFamily: "Helvetica", fontSize: "24px", fontWeight: "700" }}>{pointData ? pointData.jml_point : "-"}</h6>
                                                </div>
                                                <div className="col ps-3">
                                                    <small style={{ lineHeight: "160%", fontStyle: "normal", fontFamily: "Helvetica", fontSize: "12px", fontWeight: "700" }}>Total Redeem</small>
                                                    <h6 style={{ fontStyle: "normal", fontFamily: "Helvetica", fontSize: "24px", fontWeight: "700" }}>{pointData ? pointData.jml_point_reedem : "-"}</h6>
                                                </div>
                                                <div className="col ps-3">
                                                    <small style={{ lineHeight: "160%", fontStyle: "normal", fontFamily: "Helvetica", fontSize: "12px", fontWeight: "700" }}>Total Poin</small>
                                                    <h6 style={{ fontStyle: "normal", fontFamily: "Helvetica", fontSize: "24px", fontWeight: "700" }}>{pointData ? pointData.jml_point + pointData.jml_point_reedem : "-"}</h6>
                                                </div>
                                                <div className="col">
                                                    <Link href="/member/redeem">
                                                        <a style={{ padding: "13px 0px", fontWeight: "700", fontFamily: "Helvetica", width: "95px", height: "48px" }} className="btn btn-main">Redeem</a>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                            <div className="tab-menu d-md-flex justify-content-between align-items-start">
                                <ul className="nav nav-tabs nav-tabs-custom">
                                    <li className="nav-item">
                                        <a href="#" className="nav-link active" data-bs-toggle="tab" data-bs-target="#tab-quest"
                                            role="tab"><span>Poin Quest</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="#" className="nav-link" data-bs-toggle="tab" data-bs-target="#tab-riwayat"
                                            role="tab"><span>Riwayat Penukaran</span></a>
                                    </li>
                                </ul>

                            </div>
                            <div className="tab-content revamp-ajukan-kprtipe">
                                <div className="tab-pane fade show active revamp-ajukan-kprtipe" id="tab-quest" role="tabpanel" style={{padding: "10px"}}>
                                    <div className="row gx-5 gy-2">
                                        {rewardList.map((data, index) =>
                                            <div key={index} className="col-md-4 px-3" style={isTabletOrMobile ? {width:"100%"} : {minWidth:"308px"}}>
                                                <ItemReward data={data} />
                                            </div>
                                        )}
                                    </div>


                                </div>
                                <div className="tab-pane fade revamp-ajukan-kprtipe" id="tab-riwayat" role="tabpanel">
                                    <div className="row">
                                        {rewardListHistory.map((data, index) =>
                                            <div key={index} className="col-12">
                                                <ItemRewardPenukaran data={data} />
                                            </div>
                                        )}

                                    </div>
                                    <div className="row">
                                        <div className="col-12 mt-5">
                                            <PaginationNew
                                                length={paginationData.JmlHalTotal}
                                                current={Number(paginationData.HalKe)}
                                                onChangePage={(e) => {
                                                    setReqBodyHistory(prevData => ({ ...prevData, Page: e }));
                                                    window.scrollTo(0, 0);
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

const MobileReward = ({ pointData }) => {
    const isGalaxyFold = useMediaQuery({ query: '(max-width: 280px)' })
    return (
        <div className="card card-reward" style={{ width: "100%", minHeight: "148px", boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.1)", border: "1px solid #EEEEEE" }}>
            <div className="card-body" style={isGalaxyFold ? { width: "100%" } : { minWidth: "308px" }}>
                <div style={{ minWidth:isGalaxyFold ? "" : "306px", marginLeft: "-16px", marginTop: "-16px", marginRight: "-16px", background: "#F7FAFF", borderRadius: "0 0 8px 8px" }}>
                    <div className="d-flex justify-content-between align-items-center" style={{ padding: "15px" }}>
                        <h5 style={{ width: "237px", fontFamily: "FuturaBT", fontSize: "20px", fontWeight: "700" }}>Poin Saya</h5>
                        <Link href="/member/redeem">
                            <a style={{ padding: "13px 0px", fontWeight: "700", fontFamily: "Helvetica", width: isGalaxyFold ? "120px" : "103px", height: "48px" }} href="#" className="btn btn-main">Redeem</a>
                        </Link>
                    </div>
                </div>
                <div className="d-flex justify-content-between" style={{ margin: isGalaxyFold ? "" : "16px 16px 0" }}>
                    <div>
                        <small style={{ lineHeight: "160%", fontStyle: "normal", fontFamily: "Helvetica", fontSize: "12px", fontWeight: "700", color: "#666666" }} >Poin Saat Ini</small>
                        <h6 style={{ fontStyle: "normal", fontFamily: "Helvetica", fontSize: "24px", fontWeight: "700", marginBottom: 0 }}>{pointData ? pointData.jml_point : "-"}</h6>
                    </div>
                    <div>
                        <small style={{ lineHeight: "160%", fontStyle: "normal", fontFamily: "Helvetica", fontSize: "12px", fontWeight: "700", color: "#666666" }}>Total Redeem</small>
                        <h6 style={{ fontStyle: "normal", fontFamily: "Helvetica", fontSize: "24px", fontWeight: "700", marginBottom: 0 }}>{pointData ? pointData.jml_point_reedem : "-"}</h6>
                    </div>
                    <div>
                        <small style={{ lineHeight: "160%", fontStyle: "normal", fontFamily: "Helvetica", fontSize: "12px", fontWeight: "700", color: "#666666" }}>Total Poin</small>
                        <h6 style={{ fontStyle: "normal", fontFamily: "Helvetica", fontSize: "24px", fontWeight: "700", marginBottom: 0 }}>{pointData ? pointData.jml_point + pointData.jml_point_reedem : "-"}</h6>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default withAuth(Pencarian);
