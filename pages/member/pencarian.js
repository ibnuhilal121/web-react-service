import React, { useEffect, useState } from 'react';
import Layout from '../../components/Layout';
import MenuMember from '../../components/MenuMember';
import Breadcrumb from '../../components/section/Breadcrumb';
import data_pencarian from '../../sample_data/data_pencarian';
import ItemPencarian from '../../components/data/ItemPencarian';
import { useAppContext } from '../../context';
import NotAvail from "../../components/element/NotAvail";
import withAuth from '../../helpers/withAuth';
import { defaultHeaders } from '../../utils/defaultHeaders';

const Pencarian = () => {


    /* ------------------------------ FETCH PENCARIAN ------------------------------ */
    const { userKey } = useAppContext();
    const [loadingData, setLoadingData] = useState(true);
    const [pencarianData, setPencarianData] = useState(null);


    useEffect(() => {
        if (!userKey) return;
        fetchPencarianData();
    }, [userKey])

    const fetchPencarianData = async () => {
        try {
            setLoadingData(true);
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pencarian/show`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/x-www-form-urlencoded",
                    AccessKey_Member: userKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams({
                    Limit: 9999
                }),
            });
            const resData = await res.json();
            if (!resData.IsError) {
                setPencarianData(resData.Data);
            } else {
                throw {
                message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoadingData(false);
        }
    };

    /* ---------------------------- DELETE PENCARIAN ---------------------------- */
    const [idToDelete, setIdToDelete] = useState(null);

    useEffect(() => {
        if (idToDelete) {
            deletePencarianData();
        }
    }, [idToDelete])

    const deletePencarianData = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pencarian/delete`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/x-www-form-urlencoded",
                    AccessKey_Member: userKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams({
                    i_pncrian: idToDelete
                }),
            });
            const resData = await res.json();
            if (!resData.IsError) {
                if (typeof window !== "undefined"){
                    window.location.reload();
                }
            } else {
                throw {
                message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            // setLoadingData(false);
        }
    };

    return (
        <Layout title="Hasil Pencarian - Dashboard Member | BTN Properti">
            <Breadcrumb active="Akun"/>
            <div className="dashboard-content mb-5">
                <div className="container">
                    <div className="row">
                        <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
                            <MenuMember active="pencarian"/>
                        </div>
                        <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                            <h5 className="title_akun">Hasil Pencarian</h5>
                            { pencarianData && pencarianData.map((data, index) =>
                                <div key={index}  className="">
                                    <ItemPencarian
                                        data={data}
                                        onDelete={(id) => setIdToDelete(id)}
                                    />
                                </div>
                            )}
                            {!pencarianData && (
                                <NotAvail message={loadingData ? "Memuat data..." : "Kamu belum memiliki list hasil pencarian"} />
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default withAuth(Pencarian);
