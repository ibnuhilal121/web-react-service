import { useRouter } from "next/router";
import { useAppContext } from "../../../context";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import React, { useEffect } from "react";
import MenuMember from "../../../components/MenuMember";
import data_portofolio from "../../../sample_data/data_portofolio";
import ReactImageFallback from "react-image-fallback";
import styles from "../../../components/data/ItemPromo.module.scss";
import { useState } from "react";
import { defaultHeaders } from "../../../utils/defaultHeaders";

export default function index() {
  const router = useRouter();
  const { id } = router.query;
  const { userKey, userProfile } = useAppContext();
  let image;
  const [isLoading, setIsLoading] = useState(true);
  const [portofolio, setPortoffolio] = useState(null);
  const [listPortofolio, setListPortofolio] = useState([]);
  const [profileMember, setProfileMember] = useState({
    bya_js: [{ jasa: "", hrg_rndh: "", hrg_tggi: "" }],
  });

  console.log("ini data portofolio", listPortofolio);

  const getListData = async () => {
    try {
      setIsLoading(true);
      let accessKey = userKey;
      if (!userKey) {
        accessKey = JSON.parse(sessionStorage.getItem("keyMember")) || JSON.parse(Cookies.get("keyMember"));
      }
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/profil/detail/bymember`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        console.log("Success get data profesional", response.Data);
        if (response.Data.length) {
          const getData = response.Data[0];
          console.log("ini datanya guys", getData);
          const payload = {
            id: getData.id,
          };
          console.log("ini data payload", payload);
          setProfileMember(payload);
          getListDataPortofolio(getData.id);
        }
        setIsLoading(false);
      } else {
        console.log(response.ErrToUser);
        setIsLoading(false);
        // throw {
        //     message: response.ErrToUser,
        // };
      }
    } catch (error) {
      console.log(error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const getListDataPortofolio = async (idMember) => {
    try {
      setIsLoading(true);
      let accessKey = userKey;
      if (!userKey) {
        accessKey = JSON.parse(sessionStorage.getItem("keyMember")) || JSON.parse(Cookies.get("keyMember"));
      }
      const payload = {
        i_prfl: idMember,
        Sort: "n ASC",
        id
      };
      console.log("payload portofolio", payload);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/proyek/show/bymember`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const response = await loadData.json();
      console.log("data Portofolio", response);
      if (!response.IsError) {
        console.log("Success get data Portofolio", response.Data);
        if (response.Data.length) {
          const listData = [];
          const getData = response.Data;
          getData.map((data, index) => {
            let dataImage;
            if (data.lst_fto !== null) {
              try {
                const myArray = JSON.parse(data.lst_fto);
                dataImage = Object.keys(myArray).map((key) => myArray[key]);
                console.log("get Data Gambarr", dataImage);
              } catch (e) {
                console.log("error mapping image!! ", e); // error in the above string (in this case, yes)!
              }
            }
            const payload = {
              lst_fto: dataImage != null ? dataImage : null,
              i_prfl: data.i_prfl != null ? data.i_prfl : "",
              n: data.n != null ? data.n : "",
              dsk: data.dsk != null ? data.dsk : "",
            };
            listData[index] = payload;
            data["lst_fto"] = dataImage;
          });
          setListPortofolio(response.Data);
        }
      }
      setIsLoading(false);
    } catch (error) {
      console.log("error profesional portoffolio: ", error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const deletePencarianData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/proyek/delete`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          id,
        }),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        if (typeof window !== "undefined") {
          window.location.replace("/member/profesional/");
        }
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoadingData(false);
    }
  };

  useEffect(() => {
    getListData();
    image = new Image();
    image.src = "/images/thumb-placeholder.png"
  }, []);

  console.log(
    "ini hasil filter",
    listPortofolio.filter((item) => item.n === id)
  );

  if (!id) return <div></div>

  return (
    <Layout title={id + " | BTN properti"}>
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="profesional" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 mt-detail-portofolio">
              <div className="d-flex justify-content-between align-items-start">
                <h5 className="title_akun judul-detail-portofolio">Detail Portofolio</h5>
                <button
                  onClick={deletePencarianData}
                  type="button"
                  className="d-flex align-items-center justify-content-center btn btn-danger tombol-detail-portofolio"
                  // data-bs-toggle="modal"
                  // data-bs-target="#modalKonsultasi"
                  style={{
                    borderRadius: "200px",
                    width: "187px",
                    height: "48px",
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                  }}
                >
                  <img className="me-2" src="/icons/icons/x-icon.svg"></img> Hapus Portofolio
                </button>
              </div>
              <hr style={{ marginTop: "5px" }} className="garis-detail-portofolio" />
              <div>
                {listPortofolio.length != 0 && listPortofolio
                  .map((item, idx) => (
                    <>
                      <h4
                        style={{
                          margin: "22px 0px",
                          fontSize: "20px",
                          fontFamily: "Helvetica",
                          fontWeight: "700",
                          color: "#00193E",
                        }}
                      >
                        {item.n}
                      </h4>

                      <p
                        style={{
                          fontSize: "12px",
                          fontWeight: "700",
                          fontFamily: "FuturaBT",
                          marginBottom: "20px",
                          opacity: "60%",
                          color: "#00193E",
                        }}
                      >
                        {item.almt}
                      </p>
                      <p
                        style={{
                          color: "#666666",
                          fontWeight: "400",
                          fontFamily: "Helvetica",
                        }}
                      >
                        {item.dsk}
                      </p>
                      <div className="row list_property">
                        {item.lst_fto?.map((data, i) => (
                          <div className="col-6 col-md-4 mb-3">
                            <div className="card_img">
                              <div className="ratio ratio-1x1">
                                {/* <img src={data} className="img-fluid" /> */}
                                <ReactImageFallback
                                  src={data ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.replace("1|", "") : "/images/thumb-placeholder.png"}
                                  fallbackImage="/images/thumb-placeholder.png"
                                  className={`img-fluid ${image?.width >= image?.height ? "w-100" : "h-100"} ${styles.blurImageChild} `}
                                  //title={data.n ? item.n : "-"}
                                />
                                <ReactImageFallback
                                  src={data ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.replace("1|", "") : "/images/thumb-placeholder.png"}
                                  fallbackImage="/images/thumb-placeholder.png"
                                  className={`img-fluid ${image?.width >= image?.height ? "w-100" : "h-100"} ${styles.blurImageChild} `}
                                  //title={data.n ? item.n : "-"}
                                />
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
