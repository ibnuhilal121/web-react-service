import React, { useEffect } from "react";
import Layout from "../../components/Layout";
import MenuMember from "../../components/MenuMember";
import Breadcrumb from "../../components/section/Breadcrumb";

import { Col, Form, InputGroup, Row } from "react-bootstrap";
import { useState } from "react";
import NumberFormat from "react-number-format";
import { useAppContext } from "../../context";
import Cookies from "js-cookie";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import { getEkycData } from "../../services/auth";
import md5 from "md5";
import { sha256 } from "js-sha256";
import Moment from "moment";
import { useSession } from "next-auth/client";
import withAuth from "../../helpers/withAuth";
import { isInvalid } from "../../utils/validation.register";
import { defaultHeaders } from "../../utils/defaultHeaders";

const ProfilNoSsr = () => {
  const [session, loadingSession] = useSession();
  const { userKey,userProfile } = useAppContext();
  const [validated, setValidated] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [profileMember, setProfileMember] = useState(null);
  const [validateData, setValidateData] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [ekycData,setEkycData] = useState({})

  /* ----------------------------- FETCH ADDRESS ----------------------------- */
  const [allProvince, setAllProvince] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState("");
  const [allCity, setAllCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState("");
  const [allKecamatan, setAllKecamatan] = useState([]);
  const [selectedKecamatan, setSelectedKecamatan] = useState("");
  const [allKelurahan, setAllKelurahan] = useState([]);
  const [selectedKelurahan, setSelectedKelurahan] = useState("");
  const [userVerified, setUserVerified] = useState(false)

  const [count, setCount] = useState(0);
  const [passwordOld, setPasswordOld] = useState(null);
  const [passwordNew, setPasswordNew] = useState(null);
  const [passwordNewVerify, setPasswordNewVerify] = useState(null);

  const [ktpError, setKtpError] = useState("");
  const [passwordOldError, setPasswordOldError] = useState("");
  const [isPassError, setIsPassError] = useState(false)
  const [passwordNewError, setPasswordNewError] = useState("");
  const [passwordNewConfError, setPasswordNewConfError] = useState("");

  const [isSavingPassword, setIsSavingPassword] = useState(false);
  const [passwordType1, setPasswordType1] = useState("password");
  const [passwordType2, setPasswordType2] = useState("password");
  const [passwordType3, setPasswordType3] = useState("password");
  const submitChangePassword = (e) => {
    e.preventDefault();

    if (passwordNew == passwordNewVerify) {
      if (!passwordOld) {
        setPasswordOldError("Isian tidak boleh kosong");
      }
      if (!passwordNew) {
        setPasswordNewError("Isian tidak boleh kosong");
      }
      if (!passwordNewVerify) {
        setPasswordNewConfError("Isian tidak boleh kosong");
        return;
      }
      if (passwordOldError || passwordNewError || passwordNewConfError) {
        return;
      }
      setNewPassword();
    } else {
      setPasswordNewConfError("Isi data yang sama dengan sandi baru");
    }
  };

  const handleChangeOldPassword = (e) => {
    setPasswordOld(handleBlurPassword(e.target.value))
  }
  const handleChangeNewPassword = (e) => {
    setPasswordNew(handleBlurNewPassword(e.target.value))
  }

  const checkValue = (value) =>{
    if(value === "null" || value === "undef") return ""
    return value
    
  }
  String.prototype.toLocaleString = function (locale) {
    if (locale == "de-de") return this.replace(/(\d+)-(\d+)-(\d+)/, '$3-$2-$1');
}

  const handleBlurPassword = (value) => {
    if (value) {
      if (value.trim() === '') {
        return setPasswordOldError('Isian tidak boleh kosong')
      } 
      // else if (isInvalid('password', value)) {
      //   return setPasswordOldError('Password minimal 8 karakter berupa alphanumerik, mengandung huruf besar dan huruf kecil, serta harus menggunakan setidaknya satu karakter khusus(.@$!%*?&)')
      // }
    } else {
      return setPasswordOldError('Isian tidak boleh kosong')
    }
    setPasswordOldError("");
    return value
  };

  const handleBlurNewPassword = (value) => {
    if (value) {
      if (value.trim() === '') {
        return setPasswordNewError('Isian tidak boleh kosong')
      } else if (isInvalid('password', value)) {
        return setPasswordNewError('Password minimal 8 karakter, terdiri dari huruf besar, huruf kecil, angka dan karakter khusus (.@$!%*?&)')
      }
    } else {
      return setPasswordNewError('Isian tidak boleh kosong')
    }
    setPasswordNewError("");
    return value
  };

  // useEffect(() => {
  //   if (count > 0) {
  //     if (!passwordOld) {
  //       setPasswordOldError("Isian tidak boleh kosong");
  //     }
  //     if (!passwordNew) {
  //       setPasswordNewError("Isian tidak boleh kosong");
  //     }
  //     if (!passwordNewVerify) {
  //       setPasswordNewConfError("Isian tidak boleh kosong");
  //     }
  //   }
  //   setCount(count + 1);
  // }, [passwordOld, passwordNew, passwordNewVerify]);

  // const submitChangePassword = () => {
  //   if (passwordNew == passwordNewVerify) {
  //     setNewPassword();
  //   } else {
  //     console.log("Periksa kata sandi anda");
  //   }
  // };

  const setNewPassword = async () => {
    try {
      const encrmd5_pass_old = md5(passwordOld);
      const hashedPassword_old = sha256.hmac(process.env.NEXT_PUBLIC_KEY_SECRET, encrmd5_pass_old);

      const encrmd5_pass_new = md5(passwordNew);
      const hashedPassword_new = sha256.hmac(process.env.NEXT_PUBLIC_KEY_SECRET, encrmd5_pass_new);

      const accessKey = JSON.parse(Cookies.get("keyMember"));
      const payload = {
        Pwd_Old: hashedPassword_old,
        Pwd_New: hashedPassword_new,
      };
      setIsSavingPassword(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/updatepassword`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          AccessKey_Member: accessKey,
          defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        console.log("Success", response.Data);
        window.location.reload();
      } else {
        throw {
          message: response.ErrToUser,
        };
      }
    } catch (error) {
      // console.log(error.message);
      setPasswordOldError(error.message);
    } finally {
      // setLoaded(true);
      setIsSavingPassword(false);
    }
  };

  /* --------------------- GET DETAIL DATA --------------------- */

  useEffect(() => {
    console.log("datanya", session);
  }, [session]);

  const getDetailData = async () => {
    const accessKey = JSON.parse(Cookies.get("keyMember"));
    try {
      setLoading(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          AccessKey_Member: accessKey,
          defaultHeaders
        },
      });
      const response = await loadData.json();
      console.log(response);
      setLoading(false);
      if (!response.IsError) {
        console.log("Success", response);
        if (response.Status === 200) {
          console.log(response.Data.length);
          if (response.Data.length) {
            const getData = response.Data[0];
            const payload = {
              n: getData.n, // Nama ----
              e: getData.e, // Email ----
              almt: getData.almt, // Alamat -----
              i_prop: getData.i_prop, // ID profinsi ----
              i_kot: getData.i_kot, // ID Kota/Kab
              i_kec: getData.i_kec, // ID Kecamatan
              i_kel: getData.i_kel, // ID Kelurahan
              pos: getData.pos, // Kode Pos ----
              no_hp: getData.no_hp, // Nomor HP ----
              tpt_lhr: getData.tpt_lhr, // Tempat Lahir ----
              tgl_lhr:
                getData.tgl_lhr != null
                  ? Moment(getData.tgl_lhr).format("YYYY-MM-DD")
                  : null, // Tgl Lahir ----
              no_ktp: getData.no_ktp, // No KTP ----
              almt2:
                getData.almt2 != null ? getData.almt2 : (getData.almt2 = ""), // Alamat 2 ----
              //e_prshn: getData.e_prshn, // Email Perusahaan
              u_fb: getData.u_fb, // URL FB ----
              u_twt: getData.u_twt, // URL Twitter ----
              u_gpls: getData.u_gpls, // URL Google Plus ----
              u_lnkdin: getData.u_lnkdin, // URL Linkdl ----
              u_web: getData.u_web, // URL Website ----
              jk: getData.jk != null ? getData.jk.toString() : null, // Jenis Kelamin ----
              no_tlp: getData.no_tlp, // No telp ---
              st_kwn: getData.st_kwn != null ? getData.st_kwn.toString() : null, // Status Kawin ----
              jns_pkrjn: getData.jns_pkrjn, // Jenis Pekerjaan ----
              pghsln: getData.pghsln, // Penghasilan ----
              bya_rmh_tng: getData.bya_rmh_tng, // Biaya Rumah Tangga ----
              pglrn_ln: getData.pglrn_ln, // Pengeluaran Lainnya ----
            };
            setProfileMember(payload);

            // Check KTP
            validateKtp(payload?.no_ktp)
          }
        } else {
          throw {
            message: response.ErrToUser,
          };
        }
        //window.location.reload();
      } else {
        throw {
          message: response.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
      setLoading(false);
    } finally {
      // setLoaded(true);
    }
  };

  /* --------------------- SUBMIT UPDATE DATA PROFILE --------------------- */

  const validateSubmit = () => {
    if (
      !profileMember?.no_ktp ||
      !profileMember?.n ||
      !profileMember?.tpt_lhr ||
      !profileMember?.tgl_lhr ||
      !profileMember?.e ||
      !profileMember?.jk ||
      !profileMember?.st_kwn ||
      ktpError
    ) {
      setValidateData(true);
      window.scrollTo(0, 200);
    } else if (
      !profileMember?.almt ||
      !profileMember?.i_prop ||
      !profileMember?.i_kot ||
      !profileMember?.i_kec ||
      !profileMember?.i_kel ||
      !profileMember?.pos ||
      !profileMember?.no_hp ||
      !profileMember?.no_tlp
    ) {
      setValidateData(true);
      window.scrollTo(0, 700);
    } else if (
      !profileMember?.jns_pkrjn ||
      !profileMember?.pghsln ||
      !profileMember?.bya_rmh_tng ||
      !profileMember?.pglrn_ln
    ) {
      setValidateData(true);
    } else {
      submitChangeProfile();
    }
  };

  const submitChangeProfile = async () => {
    const accessKey = JSON.parse(Cookies.get("keyMember"));;

    const validEmail =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!validEmail.test(profileMember.e)) {
      console.log("isi dengan alamat email yang benar");
      return;
    }

    try {
      //e.preventDefault();
      setLoading(true);
      console.log("payload", profileMember);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/update`;
      const submitData = await fetch(endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          AccessKey_Member: accessKey,
          defaultHeaders
        },
        body: new URLSearchParams(profileMember),
      });
      const response = await submitData.json();
      console.log(response);
      setLoading(false);
      if (!response.IsError) {
        console.log("Success", response.Data);
        window.location.reload();
      } else {
        throw {
          message: "Error (" + response.Status + ") " + response.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
      setLoading(false);
    } finally {
      // setLoaded(true);
    }
  };

  /* --------------------- GET ALL PROVINCE ON FIRST LOAD --------------------- */
  useEffect(() => {
    //getAllProvinceData();
    getDetailData();
    if(userKey){
      if(isLoading){ 
        getEkycData(userProfile?.id).then((result)=>{
          setLoading(true)
          console.log("log Adhin",result)
          if(result?.data?.isVerified == true){
            setUserVerified(true)
            setEkycData(result.data)
          }
        }
      ).finally(()=>{
        setLoading(false)
      })
    }
    }
  }, []);

  useEffect(() => {
    getAllProvinceData();
  }, [profileMember]);

  const getAllProvinceData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/propinsi/show`;
      const propinsi = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          Sort: "n ASC",
        }),
      });
      const dataPropinsi = await propinsi.json();
      if (!dataPropinsi.IsError) {
        console.log("GET PROPINSI", dataPropinsi.Data);
        setAllProvince(dataPropinsi.Data);
        if (profileMember?.i_prop != null) {
          setSelectedProvince(profileMember.i_prop);
        }
      } else {
        throw {
          message: dataPropinsi.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoaded(true);
    }
  };

  /* ------------------ GET CITIES WHEN PROVINCE IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedProvince) {
      getCityData();
    } else {
      setAllCity([]);
      setAllKecamatan([]);
      setAllKelurahan([]);
    }

    setSelectedCity("");
    setSelectedKecamatan("");
    setSelectedKelurahan("");
  }, [selectedProvince]);

  const getCityData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kota/show`;
      const cities = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_prop: selectedProvince,
          Sort: "n ASC",
        }),
      });
      const dataCities = await cities.json();
      if (!dataCities.IsError) {
        console.log("GET CITY", dataCities.Data);
        setAllCity(dataCities.Data);
        if (profileMember?.i_kot != null) {
          setSelectedCity(profileMember.i_kot);
        }
      } else {
        throw {
          message: dataCities.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  /* ------------------- GET KECAMATAN WHEN CITY IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedCity) {
      getKecamatanData();
    } else {
      setAllKecamatan([]);
      setAllKelurahan([]);
    }

    setSelectedKecamatan("");
    setSelectedKelurahan("");
  }, [selectedCity]);

  const getKecamatanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kecamatan/show`;
      const kecamatan = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_kot: selectedCity,
          Sort: "n ASC",
        }),
      });
      const dataKecamatan = await kecamatan.json();
      if (!dataKecamatan.IsError) {
        console.log("GET KECAMATAN", dataKecamatan.Data);
        setAllKecamatan(dataKecamatan.Data);
        if (profileMember?.i_kec != null) {
          setSelectedKecamatan(profileMember.i_kec);
        }
      } else {
        throw {
          message: dataKecamatan.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  /* ------------------- GET KELURAHAN WHEN KECAMATAN IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedKecamatan) {
      getKelurahanData();
    } else {
      setAllKelurahan([]);
    }

    setSelectedKelurahan("");
  }, [selectedKecamatan]);

  const getKelurahanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kelurahan/show`;
      const kelurahan = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_kec: selectedKecamatan,
          Sort: "n ASC",
        }),
      });
      const dataKelurahan = await kelurahan.json();
      if (!dataKelurahan.IsError) {
        console.log("GET KELURAHAN", dataKelurahan.Data);
        setAllKelurahan(dataKelurahan.Data);
        if (profileMember?.i_kel != null) {
          setSelectedKelurahan(profileMember.i_kel);
        }
      } else {
        throw {
          message: dataKelurahan.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  const setPosCode = (id) => {
    // if(id != null){
    allKelurahan
      .filter((item) => item.id == id)
      .map((data, idx) => changeProfil("pos", data.pos));
    // } else {
    //   changeProfil("pos", "")
    // }
  };

  // const form = event.currentTarget;
  // if (form.checkValidity() === false) {
  // 	event.preventDefault();
  // 	event.stopPropagation();
  // }

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  // useEffect(() => {
  //   getDataProfil();
  // }, []);

  // const getDataProfil = async () => {
  //   try {
  //     const accessKey = JSON.parse(
  //       sessionStorage.getItem("keyMember") || Cookies.get("keyMember")
  //     );

  //     const profil = await fetch(
  //       `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`,
  //       {
  //         method: "POST",
  //         headers: {
  //           Accept: "application/json",
  //           "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  //           AccessKey_Member: accessKey,
  //         },
  //       }
  //     );
  //     const dataProfil = await profil.json();
  //     if (!dataProfil.IsError) {
  //       console.log()
  //       setProfileMember(dataProfil.Data[0]);
  //     } else {
  //       throw {
  //         message: dataProfil.ErrToUser,
  //       };
  //     }
  //   } catch (error) {
  //     console.log("error get profil : ", error);
  //   } finally {
  //     setLoaded(true);
  //   }
  // };

  const changeProfil = (key, value) => {
    // let temp = { ...profileMember };
    // temp[key] = value;
    //setProfileMember(temp);
    setProfileMember((previousInputs) => ({ ...previousInputs, [key]: value }));
  };

  const validateKtp = (value) => {
    const numOnly = new RegExp("^[0-9]+$");
    if (numOnly.test(value) || value === "") {
      changeProfil("no_ktp", value)
      if (value.length != 16) {
        setKtpError("Nomor KTP harus 16 karakter");
      } else {
        setKtpError(false);
      }
      if (!value) {
        setKtpError("Isian tidak boleh kosong");
      }
    }
  }

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  //   return (
  //       <Layout title="Akun Profile">
  //           <Breadcrumb active="Akun"/>
  //           <div className="dashboard-content mb-5">
  //               <div className="container">
  //                   <div className="row">
  //                       <div className="col-lg-3 col-xl-3 col-md-4 col-sm-4">
  //                           <MenuMember active="profil"/>
  //                       </div>
  //                       <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">

  //   setValidated(true);
  // };
  const togglePassword1 = () => {
    if (passwordType1 === "password") {
      setPasswordType1("text")
      return;
    }
    setPasswordType1("password")
  }
  const togglePassword2 = () => {
    if (passwordType2 === "password") {
      setPasswordType2("text")
      return;
    }
    setPasswordType2("password")
  }
  const togglePassword3 = () => {
    if (passwordType3 === "password") {
      setPasswordType3("text")
      return;
    }
    setPasswordType3("password")
  }
  return (
    <Layout title="Akun Profile" isLoaderOpen={isLoading}>
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="profil" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 revamp-card-developer">
              <h5
                className="title_akun"
                style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
              >
                Profil
              </h5>

              <div className="dashboard-profil revamp-card-developer-child">
                <div className="tab-menu revamp-card-imgdev">
                  <ul className="nav nav-tabs">
                    <li className="nav-item">
                      <a
                        href="#"
                        className="nav-link active"
                        data-bs-toggle="tab"
                        data-bs-target="#tab-data-profile"
                        role="tab"
                      >
                        <span> Informasi Data Diri</span>
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        href="#"
                        className="nav-link"
                        data-bs-toggle="tab"
                        data-bs-target="#tab-password"
                        role="tab"
                      >
                        <span>Kata Sandi</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="tab-content revamp-ajukan-kprtipe">
                  <div
                    className="tab-pane fade show active revamp-ajukan-kprtipe"
                    id="tab-data-profile"
                    role="tabpanel"
                  >
                    <div className="row">
                      <div className="col-md-8 col-lg-8">
                        <form
                          className="form-validation"
                          noValidate
                          validated
                          id="form-profil"
                        >
                          <h5
                            style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
                          >
                            Informasi Data Diri
                          </h5>
                          <div className="floating-label-wrap mt-5">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="profil[no_ktp]"
                              maxLength="16"
                              minLength="16"
                              placeholder="Nomor KTP"
                              value={userVerified ? ekycData.identityCardNumber : checkValue(profileMember?.no_ktp)}
                              onChange={(e) => {
                                validateKtp(e.target.value)
                              }}
                              disabled={userVerified ? true : false}
                            />
                            <label className="floating-label">Nomor KTP </label>
                            {ktpError && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {ktpError}
                              </p>
                            )}
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[n]"
                              placeholder="Nama Lengkap"
                              value={userVerified ? ekycData.fullName : checkValue(profileMember?.n)}
                              onChange={(e) =>
                                changeProfil("n", e.target.value)
                              }
                              disabled={userVerified ? true : false }
                            />
                            <label className="floating-label">
                              Nama Lengkap{" "}
                            </label>
                            {validateData && !profileMember?.n && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>
                          <div className="row">
                            <div className="col-md-6 col-6 pe-1">
                              <div className="floating-label-wrap ">
                                <input
                                  type="text"
                                  className="floating-label-field"
                                  name="profil[tpt_lhr]"
                                  placeholder="Kota Kelahiran"
                                  value={userVerified ? ekycData.placeOfBirth : checkValue(profileMember?.tpt_lhr)}
                                  onChange={(e) =>
                                    changeProfil("tpt_lhr", e.target.value)
                                  }
                                  disabled={userVerified ? true : false}
                                />
                                <label className="floating-label">
                                  Kota Kelahiran{" "}
                                </label>
                                {validateData && !profileMember?.tpt_lhr && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-md-6 col-6 ps-1">
                              <div className="floating-label-wrap ">
                                <input
                                  required
                                  type="date"
                                  className="floating-label-field custom-calender"
                                  id="tgl_lhr"
                                  name="profil[tgl_lhr]"
                                  placeholder="Tangal lahir"
                                  value={userVerified ? (ekycData?.dateOfBirth?.toLocaleString("de-de")) : checkValue(profileMember?.tgl_lhr)}
                                  onChange={(e) =>
                                    changeProfil("tgl_lhr", e.target.value)
                                  }
                                  disabled={userVerified ? true : false}
                                />
                                <label className="floating-label">
                                  Tanggal Lahir{" "}
                                </label>
                                {validateData && !profileMember?.tgl_lhr && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-12">
                              <div className="floating-label-wrap ">
                                <input
                                  type="email"
                                  className="floating-label-field"
                                  name="profil[e]"
                                  placeholder="Email"
                                  value={checkValue(profileMember?.e)}
                                  onChange={(e) =>
                                    changeProfil("e", e.target.value)
                                  }
                                />
                                <label className="floating-label">Email </label>
                                {validateData && !profileMember?.e && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-md-6 col-6 pe-1">
                              <div className="floating-label-wrap ">
                                <select
                                  className="floating-label-select custom-select-profile form-select"
                                  name="profil[jk]"
                                  placeholder="Jenis Kelamin"
                                  value={userVerified ? ekycData?.gender == "PEREMPUAN" ? 0 : 1 
                                  :   profileMember?.jk}
                                  onChange={(e) =>
                                    changeProfil("jk", e.target.value)
                                  }
                                  required
                                  disabled={userVerified ? true : false}
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                  >
                                  <option value="">Jenis Kelamin</option>
                                  <option value="1">Laki - Laki</option>
                                  <option value="0">Perempuan</option>
                                </select>
                                <label className="floating-label">
                                  Jenis Kelamin{" "}
                                </label>
                                {validateData && !profileMember?.jk && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-md-6 col-6 ps-1">
                              <div className="floating-label-wrap">
                                <select
                                  className="floating-label-select custom-select-profile form-select"
                                  name="profil[st_kwn]"
                                  onChange={(e) =>
                                    changeProfil("st_kwn", e.target.value)
                                  }
                                  value={userVerified ? ekycData.maritalStatus == "BELUM KAWIN" ? 0 : 1 
                                  : profileMember?.st_kwn}
                                  required
                                  disabled={userVerified ? true : false}
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                >
                                  <option value="">Status Menikah </option>
                                  <option value="1">Sudah Menikah</option>
                                  <option value="0">Belum Menikah</option>
                                </select>
                                <label className="floating-label">
                                  Status Menikah{" "}
                                </label>
                                {validateData && !profileMember?.st_kwn && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                          </div>

                          <hr
                            style={{
                              background: "#EEEEEE",
                              height: 1,
                              opacity: 1,
                              marginTop: 0,
                              marginBottom: 32,
                            }}
                          />
                          <h5
                            style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
                          >
                            Informasi Kontak
                          </h5>
                          <div className="floating-label-wrap mt-5">
                            <textarea
                              style={{ height: "128px" }}
                              className="floating-label-field floating-label-textarea"
                              rows="4"
                              name="profil[almt]"
                              placeholder="Alamat lengkap"
                              value={checkValue(profileMember?.almt)}
                              onChange={(e) =>
                                changeProfil("almt", e.target.value)
                              }
                            ></textarea>
                            <label className="floating-label">
                              Alamat KTP{" "}
                            </label>
                            {validateData && !profileMember?.almt && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>
                          {/* <div className="row">
														<div className="col-md-6">
															<div className="floating-label-wrap ">
																<input
																	type="text"
																	className="floating-label-field"
																	name="profil[rt]"
																	placeholder="RT"
																/>
																<label className="floating-label">RT </label>
															</div>
														</div>
														<div className="col-md-6">
															<div className="floating-label-wrap ">
																<input
																	type="text"
																	className="floating-label-field"
																	name="profil[rw]"
																	placeholder="RW"
																/>
																<label className="floating-label">RW</label>
															</div>
														</div>
													</div> */}
                          <div className="row informasi-kontak-address">
                            <div className="col-6 pe-1">
                              <div className="floating-label-wrap">
                                <select
                                  className="floating-label-select custom-select-profile form-select"
                                  name="profil[i_prop]"
                                  id="provinsi"
                                  required
                                  value={profileMember?.i_prop}
                                  onChange={(e) => {
                                    changeProfil("i_prop", e.target.value);
                                    changeProfil("i_kot", "");
                                    changeProfil("i_kec", "");
                                    changeProfil("i_kel", "");
                                    changeProfil("pos", "");
                                    setSelectedProvince(e.target.value);
                                  }}
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                >
                                  <option value="">Pilih Provinsi</option>
                                  {allProvince.length &&
                                    allProvince.map((province) => (
                                      <option value={province.id}>
                                        {province.n}
                                      </option>
                                    ))}
                                </select>
                                <label className="floating-label">
                                  Provinsi{" "}
                                </label>
                                {validateData && !profileMember?.i_prop && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-6 ps-1">
                              <div className="floating-label-wrap">
                                <select
                                  className="floating-label-select custom-select-profile form-select"
                                  name="profil[i_kot]"
                                  id="kota"
                                  required
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                  value={profileMember?.i_kot}
                                  onChange={(e) => {
                                    changeProfil("i_kot", e.target.value);
                                    setSelectedCity(e.target.value);
                                    changeProfil("i_kec", "");
                                    changeProfil("i_kel", "");
                                    changeProfil("pos", "");
                                  }}
                                >
                                  <option value="">Pilih Kota/Kabupaten</option>
                                  {allCity.length &&
                                    allCity.map((city) => (
                                      <option value={city.id}>{city.n}</option>
                                    ))}
                                </select>
                                <label className="floating-label">
                                  Kota/Kabupaten{" "}
                                </label>
                                {validateData && !profileMember?.i_kot && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-6 pe-1">
                              <div className="floating-label-wrap">
                                <select
                                  className="floating-label-select custom-select-profile form-select"
                                  name="profil[i_kec]"
                                  id="kecamatan"
                                  required
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                  value={profileMember?.i_kec}
                                  onChange={(e) => {
                                    changeProfil("i_kec", e.target.value);
                                    setSelectedKecamatan(e.target.value);
                                    changeProfil("i_kel", "");
                                    changeProfil("pos", "");
                                  }}
                                >
                                  <option value="">Pilih Kecamatan</option>
                                  {allKecamatan.length &&
                                    allKecamatan.map((kecamatan) => (
                                      <option value={kecamatan.id}>
                                        {kecamatan.n}
                                      </option>
                                    ))}
                                </select>
                                <label className="floating-label">
                                  Kecamatan{" "}
                                </label>
                                {validateData && !profileMember?.i_kec && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-6 ps-1">
                              <div className="floating-label-wrap">
                                <select
                                  className="floating-label-select custom-select-profile form-select"
                                  name="profil[i_kel]"
                                  id="kelurahan"
                                  required
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                  value={profileMember?.i_kel}
                                  onChange={(e) => {
                                    setPosCode(e.target.value);
                                    changeProfil("i_kel", e.target.value);
                                    setSelectedKelurahan(e.target.value);
                                  }}
                                >
                                  <option value="">Pilih Kelurahan</option>
                                  {allKelurahan.length &&
                                    allKelurahan.map((kelurahan) => (
                                      <option value={kelurahan.id}>
                                        {kelurahan.n?.replace("Desa ", "").replace("Kelurahan ", "")}
                                      </option>
                                    ))}
                                </select>
                                <label className="floating-label">
                                  Kelurahan{" "}
                                </label>
                                {validateData && !profileMember?.i_kel && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[pos]"
                              id="kodepos"
                              placeholder="Kodepos"
                              value={checkValue(profileMember?.pos)}
                              onChange={(e) =>
                                changeProfil("pos", e.target.value)
                              }
                            />
                            <label className="floating-label">KodePos </label>
                            {validateData && !profileMember?.pos && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>
                          <div className="floating-label-wrap ">
                            <textarea
                              style={{ height: "128px" }}
                              className="floating-label-field floating-label-textarea"
                              rows="4"
                              name="profil[almt2]"
                              placeholder="Alamat Tambahan (Optional)"
                              value={
                                checkValue(profileMember?.almt2 ? profileMember?.almt2 : "")
                              }
                              onChange={(e) =>
                                changeProfil("almt2", e.target.value)
                              }
                            />
                            <label className="floating-label">
                              Alamat Tambahan (Optional)
                            </label>
                          </div>
                          <div className="row">
                            <div className="col-md-6 col-6 pe-1">
                              <div className="floating-label-wrap ">
                                <input
                                  type="text"
                                  className="floating-label-field"
                                  name="profil[no_hp]"
                                  placeholder="Nomor Handphone "
                                  value={checkValue(profileMember?.no_hp)}
                                  onChange={(e) =>
                                    changeProfil("no_hp", e.target.value)
                                    
                                  }
                                  maxLength={13}
                                />
                                <label className="floating-label">
                                  Nomor Handphone{" "}
                                </label>
                                {validateData && !profileMember?.no_hp && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                      
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                            <div className="col-md-6 col-6 ps-1">
                              <div className="floating-label-wrap ">
                                <input
                                  type="text"
                                  className="floating-label-field"
                                  name="profil[no_tlp]"
                                  placeholder="Nomor Telepon"
                                  value={checkValue(profileMember?.no_tlp)}
                                  onChange={(e) =>
                                    changeProfil("no_tlp", e.target.value)
                                  }
                                  maxLength={13}
                                />
                                <label className="floating-label">
                                  Nomor Telepon
                                </label>
                                {validateData && !profileMember?.no_tlp && (
                                  <p
                                    style={{
                                      color: "#dc3545",
                                      fontFamily: "Helvetica",
                                      fontSize: 12,
                                      transform: "translateY(5px)",
                                    }}
                                  >
                                    {/* {passwordNewError} */}
                                    Isian tidak boleh kosong
                                  </p>
                                )}
                              </div>
                            </div>
                          </div>

                          {/* <hr
																												style={{
																														background: "#EEEEEE",
																														height: 1,
																														opacity: 1,
																														marginTop: 0,
																														marginBottom: 32,
																												}}
																										/> 
																										 <h5
																												style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
																										>
																												Informasi Kontak
																										</h5>
																										<div className="floating-label-wrap mt-5">
																												<textarea
																														className="floating-label-field"
																														rows="4"
																														name="profil[almt]"
																														placeholder="Alamat lengkap"
																												></textarea>
																												<label className="floating-label">
																														Alamat KTP{" "}
																												</label>
																										</div>
																										<div className="row">
																												<div className="col-md-6">
																														<div className="floating-label-wrap ">
																																<input
																																		type="text"
																																		className="floating-label-field"
																																		name="profil[rt]"
																																		placeholder="RT"
																																/>
																																<label className="floating-label">RT </label>
																														</div>
																												</div>
																												<div className="col-md-6">
																														<div className="floating-label-wrap ">
																																<input
																																		type="text"
																																		className="floating-label-field"
																																		name="profil[rw]"
																																		placeholder="RW"
																																/>
																																<label className="floating-label">RW</label>
																														</div>
																												</div>
																										</div>
																										<div className="row">
																												<div className="col-6">
																														<div className="floating-label-wrap">
																																<select
																																		className="floating-label-select custom-select form-select"
																																		name="profil[i_prop]"
																																		id="provinsi"
																																		required
																																		style={{
																																				fontFamily: "Helvetica",
																																				fontWeight: 400,
																																		}}
																																>
																																		<option value="">Pilih Provinsi</option>
																																		<option value="11">Aceh</option>
																																		<option value="51">Bali</option>
																																		<option value="36">Banten</option>
																																		<option value="17">Bengkulu</option>
																																		<option value="34">DI Yogyakarta</option>
																																		<option value="31">DKI Jakarta</option>
																																		<option value="75">Gorontalo</option>
																																		<option value="15">Jambi</option>
																																		<option value="32">Jawa Barat</option>
																																		<option value="33">Jawa Tengah</option>
																																		<option value="35">Jawa Timur</option>
																																		<option value="61">Kalimantan Barat</option>
																																		<option value="63">Kalimantan Selatan</option>
																																		<option value="62">Kalimantan Tengah</option>
																																		<option value="64">Kalimantan Timur</option>
																																		<option value="65">Kalimantan Utara</option>
																																		<option value="19">
																																				Kep. Bangka Belitung
																																		</option>
																																		<option value="21">Kep. Riau</option>
																																		<option value="18">Lampung</option>
																																		<option value="81">Maluku</option>
																																		<option value="82">Maluku Utara</option>
																																		<option value="52">
																																				Nusa Tenggara Barat
																																		</option>
																																		<option value="53">
																																				Nusa Tenggara Timur
																																		</option>
																																		<option value="91">Papua</option>
																																		<option value="92">Papua Barat</option>
																																		<option value="14">Riau</option>
																																		<option value="76">Sulawesi Barat</option>
																																		<option value="73">Sulawesi Selatan</option>
																																		<option value="72">Sulawesi Tengah</option>
																																		<option value="74">Sulawesi Tenggara</option>
																																		<option value="71">Sulawesi Utara</option>
																																		<option value="13">Sumatra Barat</option>
																																		<option value="16">Sumatra Selatan</option>
																																		<option value="12">Sumatra Utara</option>
																																</select>
																																<label className="floating-label">
																																		Provinsi{" "}
																																</label>
																														</div>
																												</div>
																												<div className="col-6">
																														<div className="floating-label-wrap">
																																<select
																																		className="floating-label-select custom-select form-select"
																																		name="profil[i_kot]"
																																		id="kota"
																																		required
																																		style={{
																																				fontFamily: "Helvetica",
																																				fontWeight: 400,
																																		}}
																																>
																																		<option value="">Pilih Kota/Kabupaten</option>
																																</select>
																																<label className="floating-label">
																																		Kota/Kabupaten{" "}
																																</label>
																														</div>
																												</div>
																												<div className="col-6">
																														<div className="floating-label-wrap">
																																<select
																																		className="floating-label-select custom-select form-select"
																																		name="profil[i_kec]"
																																		id="kecamatan"
																																		required
																																		style={{
																																				fontFamily: "Helvetica",
																																				fontWeight: 400,
																																		}}
																																>
																																		<option value="">Pilih Kecamatan</option>
																																</select>
																																<label className="floating-label">
																																		Kecamatan{" "}
																																</label>
																														</div>
																												</div>
																												<div className="col-6">
																														<div className="floating-label-wrap">
																																<select
																																		className="floating-label-select custom-select form-select"
																																		name="profil[i_kel]"
																																		id="kelurahan"
																																		required
																																		style={{
																																				fontFamily: "Helvetica",
																																				fontWeight: 400,
																																		}}
																																>
																																		<option value="">Pilih Kelurahan</option>
																																</select>
																																<label className="floating-label">
																																		Kelurahan{" "}
																																</label>
																														</div>
																												</div>
																										</div>
																										<div className="floating-label-wrap ">
																												<input
																														type="text"
																														className="floating-label-field"
																														name="profil[pos]"
																														id="kodepos"
																														placeholder="Kodepos"
																												/>
																												<label className="floating-label">KodePos </label>
																										</div>
																										<div className="floating-label-wrap ">
																												<textarea
																														className="floating-label-field"
																														rows="4"
																														name="profil[almt2]"
																														placeholder="Alamat Tambahan (Optional)"
																												></textarea>
																												<label className="floating-label">
																														Alamat Tambahan (Optional)
																												</label>
																										</div>
																										<div className="row">
																												<div className="col-md-6">
																														<div className="floating-label-wrap ">
																																<input
																																		type="text"
																																		className="floating-label-field"
																																		name="profil[no_hp]"
																																		placeholder="Nomor Handphone "
																																/>
																																<label className="floating-label">
																																		Nomor Handphone{" "}
																																</label>
																														</div>
																												</div>
																												<div className="col-md-6">
																														<div className="floating-label-wrap ">
																																<input
																																		type="text"
																																		className="floating-label-field"
																																		name="profil[no_tlp]"
																																		placeholder="Nomor Telepon"
																																/>
																																<label className="floating-label">
																																		Nomor Telepon
																																</label>
																														</div>
																												</div>
																										</div> */}

                          {/* <hr
                            style={{
                              background: "#EEEEEE",
                              height: 1,
                              opacity: 1,
                              marginTop: 0,
                              marginBottom: 32,
                            }}
                          /> */}
                          <hr
                            style={{
                              background: "#EEEEEE",
                              height: 1,
                              opacity: 1,
                              marginTop: 0,
                              marginBottom: 32,
                            }}
                          />
                          <h5
                            style={{
                              paddingBottom: "48px",
                              fontFamily: "FuturaBT",
                              fontWeight: 700,
                            }}
                          >
                            Sosial Media
                          </h5>
                          <div className="floating-label-wrap mt-6">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[u_fb]"
                              placeholder="Facebook"
                              value={checkValue(profileMember?.u_fb)}
                              onChange={(e) =>
                                changeProfil("u_fb", e.target.value)
                              }
                            // style={{
                            //   fontFamily: "FuturaBT",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label className="floating-label">Facebook</label>
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[u_twt]"
                              placeholder="Twitter"
                              value={checkValue(profileMember?.u_twt)}
                              onChange={(e) =>
                                changeProfil("u_twt", e.target.value)
                              }
                            // style={{
                            //   fontFamily: "FuturaBT",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label className="floating-label">Twitter</label>
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[u_gpls]"
                              placeholder="Google+"
                              value={checkValue(profileMember?.u_gpls)}
                              onChange={(e) =>
                                changeProfil("u_gpls", e.target.value)
                              }
                            // style={{
                            //   fontFamily: "FuturaBT",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label className="floating-label">Google+</label>
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[u_lnkdin]"
                              placeholder="LinkedIn"
                              value={checkValue(profileMember?.u_lnkdin)}
                              onChange={(e) =>
                                changeProfil("u_lnkdin", e.target.value)
                              }
                            // style={{
                            //   fontFamily: "FuturaBT",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label className="floating-label">LinkedIn</label>
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type="text"
                              className="floating-label-field"
                              name="profil[u_web]"
                              placeholder="Website atau Blog"
                              value={checkValue(profileMember?.u_web)}
                              onChange={(e) =>
                                changeProfil("u_web", e.target.value)
                              }
                            // style={{
                            //   fontFamily: "FuturaBT",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label className="floating-label">
                              Website atau Blog
                            </label>
                          </div>
                          <hr
                            style={{
                              background: "#EEEEEE",
                              height: 1,
                              opacity: 1,
                              marginTop: 0,
                              marginBottom: 32,
                            }}
                          />
                          <h5
                          // style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
                          >
                            Informasi Pekerjaan
                          </h5>

                          <div className="floating-label-wrap mt-5 ">
                            <select
                              className="floating-label-select custom-select-profile form-select"
                              id="job"
                              name="profil[jns_pkrjn]"
                              value={checkValue(profileMember?.jns_pkrjn)}
                              onChange={(e) => {
                                changeProfil("jns_pkrjn", e.target.value);
                              }}
                              required
                            >
                              <option value="">Jenis Pekerjaan</option>
                              <option value="10">BUMN</option>
                              <option value="12">BUMD</option>
                              <option value="20">Perusahaan Swasta</option>
                              <option value="25">Perusahaan Asing</option>
                              <option value="30">PNS</option>
                              <option value="40">Profesional</option>
                              <option value="50">Wirausaha</option>
                              <option value="62">TNI POLRI</option>
                              <option value="99">PRT</option>{" "}
                            </select>
                            <label className="floating-label" htmlFor="job">
                              Jenis Pekerjaan{" "}
                            </label>
                            {validateData && !profileMember?.jns_pkrjn && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>

                          <div className="floating-label-wrap ">
                            <NumberFormat
                              thousandSeparator="."
                              decimalSeparator=","
                              allowNegative={false}
                              className="floating-label-field"
                              placeholder="Penghasilan Per Bulan"
                              value={checkValue(profileMember?.pghsln)}
                              onValueChange={(e) =>
                                changeProfil("pghsln", e.value)
                              }
                              prefix="Rp. "
                            // style={{
                            //   fontFamily: "Futura",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label
                              className="floating-label"
                              htmlFor="incomepermonth"
                            >
                              Penghasilan Per Bulan{" "}
                            </label>
                            {validateData && !profileMember?.pghsln && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>

                          <div className="floating-label-wrap ">
                            <NumberFormat
                              thousandSeparator="."
                              decimalSeparator=","
                              allowNegative={false}
                              className="floating-label-field"
                              placeholder="Biaya Hidup"
                              value={checkValue(profileMember?.bya_rmh_tng)}
                              onValueChange={(e) =>
                                changeProfil("bya_rmh_tng", e.value)
                              }
                              prefix="Rp. "
                            // style={{
                            //   fontFamily: "Futura",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label
                              className="floating-label"
                              htmlFor="lifecost"
                            >
                              Biaya Hidup{" "}
                            </label>
                            {validateData && !profileMember?.bya_rmh_tng && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>
                          <div className="floating-label-wrap ">
                            <NumberFormat
                              thousandSeparator="."
                              decimalSeparator=","
                              allowNegative={false}
                              className="floating-label-field"
                              placeholder="Pengeluaran Lain"
                              value={checkValue(profileMember?.pglrn_ln)}
                              onValueChange={(e) =>
                                changeProfil("pglrn_ln", e.value)
                              }
                              prefix="Rp. "
                            // style={{
                            //   fontFamily: "Futura",
                            //   fontWeight: 700,
                            // }}
                            />
                            <label
                              className="floating-label"
                              htmlFor="otherinstallment"
                            >
                              Pengeluaran Lain
                            </label>
                            {validateData && !profileMember?.pglrn_ln && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {/* {passwordNewError} */}
                                Isian tidak boleh kosong
                              </p>
                            )}
                          </div>

                          <div
                            className="form-group text-right"
                            style={{
                              fontSize: "14px",
                              fontFamily: "Helvetica",
                              fontStyle: "normal",
                              fontWeight: "bold",
                              lineHeight: "40px",
                            }}
                          >
                            <div
                              type="submit"
                              className="btn btn-main btn-big btn-profil revamp-btn-hitung"
                              onClick={() => validateSubmit()}
                              style={{
                                fontSize: 14,
                                fontFamily: "Helvetica",
                                fontWeight: 700,
                                fontWeight: "bold",
                                width: 167,
                                height: 48,
                                padding: 0,
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                              }}
                            >
                              Simpan Perubahan
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade revamp-ajukan-kprtipe"
                    id="tab-password"
                    role="tabpanel"
                  >
                    <div className="row">
                      <div className="col-md-6">
                        <form className="form-validation" id="form-password">
                          {/* <h5>Ubah Kata Sandi</h5> */}
                          <div className="floating-label-wrap mt-4">
                            <input
                              type={passwordType1}
                              className="floating-label-field required txtpassword password"
                              id="password_lama"
                              name="profil[password_lama]"
                              placeholder="Kata Sandi Lama"
                              onChange={handleChangeOldPassword}
                              autoComplete={"off"}
                              onFocus={(e) => e.target.placeholder = ""}
                              onBlur={(e) => e.target.placeholder = "Kata Sandi Lama"}
                            />
                            <label className="floating-label">
                              Kata Sandi Lama{" "}
                            </label>
                            <div className="eye-password" onClick={togglePassword1}>
                              {passwordType1 === "password" ?
                                <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                                <img src="/images/icons/eye.svg" width="20" height="20" />
                              }
                            </div>
                            {passwordOldError.length > 0 && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {passwordOldError}
                              </p>
                            )}
                            <input
                              type="hidden"
                              name="pass[Pwd_Old]"
                              required
                            />
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type={passwordType2}
                              className="floating-label-field required txtpassword password"
                              data-rule-passcheck="true"
                              id="password_baru"
                              placeholder="Kata Sandi Baru "
                              onChange={handleChangeNewPassword}
                              onFocus={(e) => e.target.placeholder = ""}
                              onBlur={(e) => e.target.placeholder = "Kata Sandi Baru"}
                            />
                            <label className="floating-label">
                              Kata Sandi Baru{" "}
                            </label>
                            <div className="eye-password" onClick={togglePassword2}>
                              {passwordType2 === "password" ?
                                <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                                <img src="/images/icons/eye.svg" width="20" height="20" />
                              }
                            </div>
                            {passwordNewError.length > 0 && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {passwordNewError}
                              </p>
                            )}
                            <input
                              type="hidden"
                              name="pass[Pwd_New]"
                              required
                            />
                          </div>
                          <div className="floating-label-wrap ">
                            <input
                              type={passwordType3}
                              name="pass[Pwd_ReNew]"
                              className="floating-label-field password"
                              equalTo="#password_baru"
                              placeholder="Konfirmasi Kata Sandi Baru"
                              onChange={(e) => {
                                const value = e.target.value;

                                if (value !== passwordNew) {
                                  setPasswordNewConfError(
                                    "Isi data yang sama dengan sandi baru"
                                  );
                                } else {
                                  setPasswordNewConfError("");
                                }

                                setPasswordNewVerify(value);
                              }}
                              onFocus={(e) => e.target.placeholder = ""}
                              onBlur={(e) => e.target.placeholder = "Konfirmasi Kata Sandi Baru"}
                            />
                            <label className="floating-label">
                              Konfirmasi Kata Sandi Baru{" "}
                            </label>
                            <div className="eye-password" onClick={togglePassword3}>
                              {passwordType3 === "password" ?
                                <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                                <img src="/images/icons/eye.svg" width="20" height="20" />
                              }
                            </div>
                            {passwordNewConfError && (
                              <p
                                style={{
                                  color: "#dc3545",
                                  fontFamily: "Helvetica",
                                  fontSize: 12,
                                  transform: "translateY(5px)",
                                }}
                              >
                                {passwordNewConfError}
                              </p>
                            )}
                          </div>
                          <div
                            className="form-group text-right"
                            style={{
                              borderTop: "1px solid #EEEEEE",
                              paddingTop: 32,
                            }}
                          // style={{ fontSize: '14px', fontFamily: 'Helvetica', fontStyle: "normal", fontWeight: 'bold', lineHeight: '40px' }}
                          >
                            <button
                              type="submit"
                              className="btn btn-main btn-big btn-profil revamp-btn-hitung"
                              onClick={submitChangePassword}
                              style={{
                                fontSize: 14,
                                fontFamily: "Helvetica",
                                fontWeight: "bold",
                                width: 167,
                                height: 48,
                                padding: 0,
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                // pointerEvents: passwordOldError || passwordOldError || passwordNewConfError || !passwordNew ? "none" : "initial"
                              }}
                            //disabled={passwordOldError || passwordOldError || passwordNewConfError || !passwordNew || !passwordOld}
                            >
                              {isSavingPassword
                                ? "Menyimpan data"
                                : "Simpan Perubahan"}
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withAuth(ProfilNoSsr);
