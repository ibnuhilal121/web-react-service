import React, { useEffect, useState } from "react";
import Layout from "../../../components/Layout";
import MenuMember from "../../../components/MenuMember";
import Breadcrumb from "../../../components/section/Breadcrumb";
import ItemRedeem from "../../../components/data/ItemRedeem";
import NotFound from "../../../components/element/NotFound";
import { useAppContext } from "../../../context";
import PaginationNew from "../../../components/data/PaginationNew";
import withAuth from "../../../helpers/withAuth";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../../../utils/defaultHeaders";

const Pencarian = () => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1024px)" });

  const { userKey } = useAppContext();

  /* ------------------------------ FETCH REWARD ------------------------------ */
  const [loadingReward, setLoadingReward] = useState(true);
  const [rewardList, setRewardList] = useState([]);
  const [categoryList, setCategoryList] = useState([]);
  const [paginationListData, setPaginationListData] = useState({});
  const [reqBodyList, setReqBodyList] = useState({
    Page: 1,
    Limit: 9,
    selectedCategory: "",
  });

  useEffect(() => {
    if (!userKey) return;

    fetchRewardList();
    fetchCategoryList();
  }, [userKey, reqBodyList]);

  const fetchRewardList = async () => {
    try {
      setLoadingReward(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/rewards/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
        body: new URLSearchParams(reqBodyList),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        console.log("Get reward data", resData.Data);
        setRewardList(resData.Data);
        setPaginationListData(resData.Paging);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoadingReward(false);
    }
  };

  const fetchCategoryList = async () => {
    try {
      // setLoadingReward(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/rewards/kategori/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({}),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        console.log("Get reward data", resData.Data);
        setCategoryList(resData.Data);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoadingReward(false);
    }
  };

  return (
    <Layout
      title="Redeem - Dashboard Member | BTN Properti"
      isLoaderOpen={loadingReward}
    >
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="reward" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 nambah-p ">
              <div className="d-flex justify-content-between custum-flex">
                <h5
                  // style={{ fontFamily: "FuturaBT", fontSize: "32px", fontWeight: "700" }}
                  className="title_akun"
                >
                  Penukaran Poin
                </h5>
                <select
                  className="floating-label-select custom-select form-select custum-select2"
                  name="kategori"
                  aria-invalid="false"
                  // style={{
                  //   color: "#AAAAAA",
                  //   height: "43px",
                  //   fontWeight: 700,
                  //   width: "309px",
                  //   padding: "10px 18px",
                  // }}
                  onChange={(e) => {
                    setReqBodyList((prevData) => ({
                      ...prevData,
                      selectedCategory: e.target.value,
                    }));
                  }}
                >
                  <option
                    value=""
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                  >
                    Semua Kategori
                  </option>
                  {categoryList &&
                    categoryList?.map((cat) => (
                      <option
                        value={cat.id}
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: 400,
                        }}
                      >
                        {cat.n}
                      </option>
                    ))}
                </select>
              </div>

              <div className="tab-content revamp-ajukan-kprtipe">
                <div
                  className="tab-pane fade show active revamp-ajukan-kprtipe"
                  id="tab-quest"
                  role="tabpanel"
                >
                  <div className="row gx-5 gy-2 revamp-flex">
                    {rewardList.map((data, index) => (
                      <div key={index} className="col-md-4 shadow-redem ">
                        <ItemRedeem data={data} />
                      </div>
                    ))}
                    {(rewardList.length == 0) && (
                      <NotFound message="Item redeem tidak ditemukan" />
                    )}
                    {/* <div key={index} className="col-md-4 px-3 revamp-flex">
                      <ItemRedeem />
                    </div> */}
                  </div>
                  <div className="row">
                    <div className="col-12 mt-5">
                      {(rewardList.length != 0) && (
                        <PaginationNew
                          className="custom-pagination"
                          length={paginationListData.JmlHalTotal}
                          current={Number(paginationListData.HalKe)}
                          onChangePage={(e) => {
                            setReqBodyList((prevData) => ({
                              ...prevData,
                              Page: e,
                            }));
                            window.scrollTo(0, 0);
                          }}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withAuth(Pencarian);
