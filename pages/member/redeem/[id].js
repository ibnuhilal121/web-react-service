import React, { useEffect, useState } from 'react';
import Layout from '../../../components/Layout';
import MenuMember from '../../../components/MenuMember';
import Breadcrumb from '../../../components/section/Breadcrumb';
import { useAppContext } from '../../../context';
import withAuth from '../../../helpers/withAuth';
import { useMediaQuery } from 'react-responsive';
import { useRouter } from "next/router";
import getHeaderWithAccessKey from '../../../utils/getHeaderWithAccessKey';
import { defaultHeaders } from '../../../utils/defaultHeaders';
import Cookies from "js-cookie";

const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const numOnly = new RegExp("^[0-9]+$");

const Pencarian = () => {
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1024px)' })
    const [validateData, setValidateData] = useState(false);
    const router = useRouter();

    const { userKey } = useAppContext();

    /* ------------------------------ FETCH REWARD ------------------------------ */
    const [loadingReward, setLoadingReward] = useState(true);
    const [rewardDetails, setRewardDetails] = useState([]);
    const [reqBodyList, setReqBodyList] = useState({
        id: router.query.id
    });

    useEffect(() => {
        if (!userKey) return;

        fetchRewardList();
    }, [userKey, reqBodyList])

    const fetchRewardList = async () => {
        try {
            setLoadingReward(true);
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/rewards/show`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/x-www-form-urlencoded",
                    AccessKey_Member: userKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams(reqBodyList),
            });
            const resData = await res.json();
            if (!resData.IsError) {
                console.log("Get reward data", resData.Data)
                setRewardDetails(resData.Data[0]);
            } else {
                throw {
                    message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoadingReward(false);
        }
    };

    /* --------------------- GET DETAIL DATA --------------------- */

    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {
        getDetailData();
    }, []);

    const getDetailData = async () => {
        let accessKey = JSON.parse(Cookies.get("keyMember"));
        try {
            // setLoading(true);
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
            const loadData = await fetch(endpoint, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                    AccessKey_Member: accessKey,
                    ...defaultHeaders
                },
            });
            const response = await loadData.json();
            if (!response.IsError) {
                console.log("Success", response);
                if (response.Status === 200) {
                    console.log(response.Data);
                    setUserDetails(response.Data[0])
                } else {
                    throw {
                        message: response.ErrToUser,
                    };
                }
            } else {
                throw {
                    message: response.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            // setLoaded(true);
        }
    };

    useEffect(() => {
        if (userDetails) {
            getAllProvinceData();
        }
    }, [userDetails])

    /* --------------------- GET ALL PROVINCE ON FIRST LOAD --------------------- */
    const [allProvince, setAllProvince] = useState([]);
    const [selectedProvince, setSelectedProvince] = useState("");
    const [allCity, setAllCity] = useState([]);
    const [selectedCity, setSelectedCity] = useState("");
    const [allKecamatan, setAllKecamatan] = useState([]);
    const [selectedKecamatan, setSelectedKecamatan] = useState("");
    const [allKelurahan, setAllKelurahan] = useState([]);
    const [selectedKelurahan, setSelectedKelurahan] = useState("");
    const [posCode, setPosCode] = useState("");
    const [rt, setRt] = useState("");
    const [rw, setRw] = useState("");

    const getAllProvinceData = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/propinsi/show`;
            const propinsi = await fetch(endpoint, {
                method: "POST",
                headers: getHeaderWithAccessKey(),
                body: new URLSearchParams({
                    Sort: "n ASC",
                }),
            });
            const dataPropinsi = await propinsi.json();
            if (!dataPropinsi.IsError) {
                console.log("GET PROPINSI", dataPropinsi.Data);
                setAllProvince(dataPropinsi.Data);
                if (userDetails) {
                    console.log(userDetails.i_prop)
                    setSelectedProvince(userDetails.i_prop);
                }
            } else {
                throw {
                    message: dataPropinsi.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            // setLoaded(true);
        }
    };

    /* ------------------ GET CITIES WHEN PROVINCE IS SELECTED ------------------ */
    useEffect(() => {
        if (selectedProvince) {
            getCityData();
        } else {
            setAllCity([]);
            setAllKecamatan([]);
            setAllKelurahan([]);
        }

        setSelectedCity("");
        setSelectedKecamatan("");
        setSelectedKelurahan("");
    }, [selectedProvince]);

    const getCityData = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kota/show`;
            const cities = await fetch(endpoint, {
                method: "POST",
                headers: getHeaderWithAccessKey(),
                body: new URLSearchParams({
                    i_prop: selectedProvince,
                    Sort: "n ASC",
                }),
            });
            const dataCities = await cities.json();
            if (!dataCities.IsError) {
                console.log("GET CITY", dataCities.Data);
                setAllCity(dataCities.Data);
                if (userDetails?.i_kot != null) {
                    setSelectedCity(userDetails.i_kot);
                }
            } else {
                throw {
                    message: dataCities.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            // setLoaded(true);
        }
    };

    /* ------------------- GET KECAMATAN WHEN CITY IS SELECTED ------------------ */
    useEffect(() => {
        if (selectedCity) {
            getKecamatanData();
        } else {
            setAllKecamatan([]);
            setAllKelurahan([]);
        }

        setSelectedKecamatan("");
        setSelectedKelurahan("");
    }, [selectedCity]);

    const getKecamatanData = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kecamatan/show`;
            const kecamatan = await fetch(endpoint, {
                method: "POST",
                headers: getHeaderWithAccessKey(),
                body: new URLSearchParams({
                    i_kot: selectedCity,
                    Sort: "n ASC",
                }),
            });
            const dataKecamatan = await kecamatan.json();
            if (!dataKecamatan.IsError) {
                console.log("GET KECAMATAN", dataKecamatan.Data);
                setAllKecamatan(dataKecamatan.Data);
                if (userDetails?.i_kec != null) {
                    setSelectedKecamatan(userDetails.i_kec);
                }
            } else {
                throw {
                    message: dataKecamatan.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            // setLoaded(true);
        }
    };

    /* ------------------- GET KELURAHAN WHEN KECAMATAN IS SELECTED ------------------ */
    useEffect(() => {
        if (selectedKecamatan) {
            getKelurahanData();
        } else {
            setAllKelurahan([]);
        }

        setSelectedKelurahan("");
    }, [selectedKecamatan]);

    const getKelurahanData = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kelurahan/show`;
            const kelurahan = await fetch(endpoint, {
                method: "POST",
                headers: getHeaderWithAccessKey(),
                body: new URLSearchParams({
                    i_kec: selectedKecamatan,
                    Sort: "n ASC",
                }),
            });
            const dataKelurahan = await kelurahan.json();
            if (!dataKelurahan.IsError) {
                console.log("GET KELURAHAN", dataKelurahan.Data);
                setAllKelurahan(dataKelurahan.Data);
                if (userDetails?.i_kel != null) {
                    setSelectedKelurahan(userDetails.i_kel);
                }
            } else {
                throw {
                    message: dataKelurahan.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            // setLoaded(true);
        }
    };

    useEffect(() => {
        if (selectedKelurahan) {
            setPosCode(getPosCode(selectedKelurahan))
        }
    }, [selectedKelurahan])


    const getPosCode = (id) => {
        console.log("POSTCODE", allKelurahan)
        return allKelurahan.filter((item) => item.id == id).map((data, idx) => data.pos);
    };


    /* ----------------------------- REQUEST REDEEM ----------------------------- */
    const [loadingRequest, setLoadingRequest] = useState(false);

    const requestRedeem = async () => {
        if (!userDetails.n || !userDetails.no_hp || !userDetails.e
            || !userDetails.almt || !rt || !rw || !selectedProvince
            || !selectedCity || !selectedKecamatan || !selectedKelurahan
            || !posCode || !validEmail.test(userDetails.e)) return;
        let accessKey = userKey;
        if (!userKey) {
            accessKey = sessionStorage.getItem("keyMember") || Cookies.get("keyMember");
            if (accessKey) {
                accessKey = JSON.parse(accessKey);
            }
        }
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/rewards/request`;
            const res = await fetch(endpoint, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                    AccessKey_Member: accessKey,
                    ...defaultHeaders
                },
                body: new URLSearchParams({
                    id: rewardDetails.id,
                    n: userDetails.n,
                    no_hp: userDetails.no_hp,
                    eml: userDetails.e,
                    almt: userDetails.almt,
                    i_prop: userDetails.i_prop,
                    i_kot: userDetails.i_kot,
                    i_kec: userDetails.i_kec,
                    i_kel: userDetails.i_kel,
                    pos: posCode,
                    ket: rewardDetails.ket,
                    rt: rt,
                    rw: rw
                }),
            });
            setLoadingRequest(true);
            const resData = await res.json();
            if (!resData.IsError) {
                console.log(resData.Output);
                router.push("/member/reward")
            } else {
                throw {
                    message: resData.ErrToUser,
                };
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoadingRequest(false);
        }
    };

    return (
        <Layout title="Redeem - Dashboard Member | BTN Properti" isLoaderOpen={loadingReward || loadingRequest}>
            <Breadcrumb active="Akun" />
            <div className="dashboard-content mb-5">
                <div className="container">
                    <div className="row">
                        <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
                            <MenuMember active="reward" />
                        </div>
                        <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                            <div className="d-flex justify-content-between">
                                <h5 style={{ fontFamily: "FuturaBT", fontSize: "32px", fontWeight: "700" }} className="title_akun">Penukaran Poin</h5>
                            </div>


                            <div className="tab-content revamp-ajukan-kprtipe" style={{ marginBottom: 23, maxWidth: "959px" }}>
                                <div className="tab-pane fade show active revamp-ajukan-kprtipe" id="tab-quest" role="tabpanel">
                                    <div className="card card-reward w-100" style={{ height: "148px", flexDirection: "row", overflow: "hidden" }}>
                                        <img
                                            src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${rewardDetails?.gbr?.replace("1|", "")}`}
                                            alt=""
                                            style={{
                                                maxWidth: 164,
                                                objectFit: "cover"
                                            }}
                                        />
                                        <div className="card-body w-100" style={{ minWidth: "308px" }}>
                                            <div className="title" style={{ fontFamily: "Helvetica", fontWeight: "700", lineHeight: "150%", fontSize: "20px" }}>
                                                {rewardDetails.n}
                                            </div>
                                            <div className="d-flex justify-content-between">
                                                <div className="item" style={{ fontFamily: "Helvetica", fontWeight: "400", lineHeight: "160%", fontSize: "14px" }}>
                                                    {rewardDetails.ket}
                                                </div>
                                            </div>
                                            <div className="d-flex justify-content-between">
                                                <div className="item" style={{ fontFamily: "Helvetica", fontWeight: "400", lineHeight: "160%", fontSize: "14px", paddingTop: 0 }}>
                                                    Tukar untuk <span style={{ fontWeight: "700" }}>{rewardDetails.jml_point} Poin</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style={{
                                backgroundColor: "#F6FAFF",
                                padding: "15px",
                                textAlign: "center",
                                border: "1px solid #EEEEEE",
                                borderTopLeftRadius: 8,
                                borderTopRightRadius: 8,
                                marginBottom: 23,
                                maxWidth: "956px",
                                minHeight: "61px"

                            }}>
                                <p style={{
                                    margin: 0,
                                    fontFamily: "FuturaBT",
                                    fontSize: 18,
                                    fontWeight: 700,
                                    color: "#00193E",
                                    opacity: 0.6,
                                    padding: 0,
                                }}
                                >
                                    Mohon isi data dengan benar untuk melakukan proses redeem poin</p>
                            </div>

                            <div
                                className="tab-pane fade show active revamp-ajukan-kprtipe"
                                id="tab-data"
                                role="tabpanel"
                            >
                                <div className="row">
                                    <div className="col-lg-6">
                                        <form className="form-validation" noValidate validated id="form-profil"
                                            style={{ maxWidth: "480px" }}
                                        >
                                            <h5 style={{ fontFamily: "FuturaBT", fontWeight: 700, marginBottom: 48, fontSize: 20 }}>
                                                Informasi Data Diri
                                            </h5>

                                            <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                <input
                                                    type="text"
                                                    className="floating-label-field"
                                                    name="profil[n]"
                                                    placeholder="Nama Lengkap"
                                                    value={userDetails.n}
                                                    onChange={(e) => setUserDetails(prevData => ({...prevData, n: e.target.value}))}
                                                />
                                                <label className="floating-label">Nama Lengkap <span style={{ color: "#D92327" }}>*</span></label>
                                                {!userDetails.n && (
                                                    <p
                                                        style={{
                                                            color: "#dc3545",
                                                            fontFamily: "Helvetica",
                                                            fontSize: 12,
                                                            transform: "translateY(5px)",
                                                        }}
                                                    >
                                                        {/* {passwordNewError} */}
                                                        Isian tidak boleh kosong
                                                    </p>
                                                )}
                                            </div>
                                            <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                <input
                                                    type="text"
                                                    className="floating-label-field"
                                                    name="profil[n]"
                                                    placeholder="Nomor HP"
                                                    maxLength="13"
                                                    value={userDetails.no_hp ? userDetails.no_hp : ""}
                                                    onChange={(e) => {
                                                        const value = e.target.value;
                                                        if (numOnly.test(value) || value == "") {
                                                            setUserDetails(prevData => ({ ...prevData, no_hp: value }))
                                                        }
                                                    }}
                                                />
                                                <label className="floating-label">Nomor HP <span style={{ color: "#D92327" }}>*</span></label>
                                                {!userDetails.no_hp && (
                                                    <p
                                                        style={{
                                                            color: "#dc3545",
                                                            fontFamily: "Helvetica",
                                                            fontSize: 12,
                                                            transform: "translateY(5px)",
                                                        }}
                                                    >
                                                        {/* {passwordNewError} */}
                                                        Isian tidak boleh kosong
                                                    </p>
                                                )}
                                            </div>
                                            <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                <input
                                                    type="text"
                                                    className="floating-label-field"
                                                    name="profil[n]"
                                                    placeholder="Email"
                                                    value={userDetails.e}
                                                    onChange={(evt) => setUserDetails(prevData => ({...prevData, e: evt.target.value}))}
                                                />
                                                <label className="floating-label">Email</label>
                                                {(!userDetails.e || !validEmail.test(userDetails.e)) && (
                                                    <p
                                                        style={{
                                                            color: "#dc3545",
                                                            fontFamily: "Helvetica",
                                                            fontSize: 12,
                                                            transform: "translateY(5px)",
                                                        }}
                                                    >
                                                        {/* {passwordNewError} */}
                                                        {!userDetails.e ? "Isian tidak boleh kosong" : "Isi dengan alamat email"}
                                                    </p>
                                                )}
                                            </div>
                                            <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                <textarea
                                                    style={{ height: "128px" }}
                                                    className="floating-label-field"
                                                    rows="4"
                                                    name="profil[almt]"
                                                    placeholder="Alamat"
                                                    value={userDetails.almt}
                                                    onChange={(evt) => setUserDetails(prevData => ({...prevData, almt: evt.target.value}))}
                                                ></textarea>
                                                <label className="floating-label">Alamat</label>
                                                {!userDetails.almt && (
                                                    <p
                                                        style={{
                                                            color: "#dc3545",
                                                            fontFamily: "Helvetica",
                                                            fontSize: 12,
                                                            transform: "translateY(5px)",
                                                        }}
                                                    >
                                                        {/* {passwordNewError} */}
                                                        Isian tidak boleh kosong
                                                    </p>
                                                )}
                                            </div>

                                            <div className="row">
                                                <div className="col-md-6 col-6"
                                                    style={{
                                                        paddingRight: "12px"
                                                    }}
                                                >
                                                    <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                        <input
                                                            type="text"
                                                            className="floating-label-field"
                                                            name="profil[rt]"
                                                            placeholder="RT"
                                                            maxLength="3"
                                                            value={rt}
                                                            onChange={(evt) => {
                                                                const value = evt.target.value;
                                                                if (value.charAt(0) == "0") return;
                                                                if (numOnly.test(value) || value == "") {
                                                                    setRt(value)
                                                                }
                                                            }}
                                                        />
                                                        <label className="floating-label">RT</label>
                                                        {!rt && (
                                                            <p
                                                                style={{
                                                                    color: "#dc3545",
                                                                    fontFamily: "Helvetica",
                                                                    fontSize: 12,
                                                                    transform: "translateY(5px)",
                                                                }}
                                                            >
                                                                {/* {passwordNewError} */}
                                                                Isian tidak boleh kosong
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-6"
                                                    style={{
                                                        paddingLeft: "12px"
                                                    }}
                                                >
                                                    <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                        <input
                                                            type="text"
                                                            className="floating-label-field"
                                                            name="profil[no_tlp]"
                                                            placeholder="RW"
                                                            maxLength="3"
                                                            value={rw}
                                                            onChange={(evt) => {
                                                                const value = evt.target.value;
                                                                if (value.charAt(0) == "0") return;
                                                                if (numOnly.test(value) || value == "") {
                                                                    setRw(value)
                                                                }
                                                            }}
                                                        />
                                                        <label className="floating-label">RW</label>
                                                        {!rw && (
                                                            <p
                                                                style={{
                                                                    color: "#dc3545",
                                                                    fontFamily: "Helvetica",
                                                                    fontSize: 12,
                                                                    transform: "translateY(5px)",
                                                                }}
                                                            >
                                                                {/* {passwordNewError} */}
                                                                Isian tidak boleh kosong
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-6"
                                                    style={{
                                                        paddingRight: "12px"
                                                    }}
                                                >
                                                    <div className="floating-label-wrap" style={{ paddingBottom: 0 }}>
                                                        <select
                                                            className="floating-label-select custom-select-profile form-select"
                                                            name="profil[i_prop]"
                                                            id="provinsi"
                                                            required
                                                            style={{
                                                                fontFamily: "Helvetica",
                                                                fontWeight: 400,
                                                                whiteSpace: "nowrap",
                                                                overflow: "hidden",
                                                                textOverflow: "ellipsis",
                                                            }}
                                                            value={selectedProvince}
                                                            onChange={(e) => {
                                                                setPosCode("");
                                                                setUserDetails(prevData => ({ ...prevData, i_prop: e.target.value, i_kot: "",i_kec: "", i_kel: ""}))
                                                                setSelectedProvince(e.target.value);
                                                            }}
                                                        >
                                                            <option value="">Pilih Provinsi</option>
                                                            {allProvince.length &&
                                                                allProvince.map((province) => (
                                                                    <option value={province.id}>{province.n}</option>
                                                                ))}
                                                        </select>
                                                        <label className="floating-label">Provinsi </label>
                                                        {!selectedProvince && (
                                                            <p
                                                                style={{
                                                                    color: "#dc3545",
                                                                    fontFamily: "Helvetica",
                                                                    fontSize: 12,
                                                                    transform: "translateY(5px)",
                                                                }}
                                                            >
                                                                {/* {passwordNewError} */}
                                                                Isian tidak boleh kosong
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="col-6"
                                                    style={{
                                                        paddingLeft: "12px"
                                                    }}
                                                >
                                                    <div className="floating-label-wrap" style={{ paddingBottom: 0 }}>
                                                        <select
                                                            className="floating-label-select custom-select-profile form-select"
                                                            name="profil[i_kot]"
                                                            id="kota"
                                                            required
                                                            style={{
                                                                fontFamily: "Helvetica",
                                                                fontWeight: 400,
                                                                whiteSpace: "nowrap",
                                                                overflow: "hidden",
                                                                textOverflow: "ellipsis",
                                                            }}
                                                            value={selectedCity}
                                                            onChange={(e) => {
                                                                setPosCode("");
                                                                setUserDetails(prevData => ({ ...prevData, i_kot: e.target.value,i_kec: "", i_kel: "" }))
                                                                setSelectedCity(e.target.value);
                                                            }}
                                                        >
                                                            <option value="">Pilih Kota/Kabupaten</option>
                                                            {allCity.length &&
                                                                allCity.map((city) => (
                                                                    <option value={city.id}>{city.n}</option>
                                                                ))}
                                                        </select>
                                                        <label className="floating-label">Kota/Kabupaten </label>
                                                        {!selectedCity && (
                                                            <p
                                                                style={{
                                                                    color: "#dc3545",
                                                                    fontFamily: "Helvetica",
                                                                    fontSize: 12,
                                                                    transform: "translateY(5px)",
                                                                }}
                                                            >
                                                                {/* {passwordNewError} */}
                                                                Isian tidak boleh kosong
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="col-6"
                                                    style={{
                                                        paddingRight: "12px"
                                                    }}
                                                >
                                                    <div className="floating-label-wrap" style={{ paddingBottom: 0 }}>
                                                        <select
                                                            className="floating-label-select custom-select-profile form-select"
                                                            name="profil[i_kec]"
                                                            id="kecamatan"
                                                            required
                                                            style={{
                                                                fontFamily: "Helvetica",
                                                                fontWeight: 400,
                                                                whiteSpace: "nowrap",
                                                                overflow: "hidden",
                                                                textOverflow: "ellipsis",
                                                            }}
                                                            value={selectedKecamatan}
                                                            onChange={(e) => {
                                                                setPosCode("");
                                                                setUserDetails(prevData => ({ ...prevData, i_kec: e.target.value, i_kel: ""}))
                                                                setSelectedKecamatan(e.target.value);
                                                            }}
                                                        >
                                                            <option value="">Pilih Kecamatan</option>
                                                            {allKecamatan.length &&
                                                                allKecamatan.map((kec) => (
                                                                    <option value={kec.id}>{kec.n}</option>
                                                                ))}
                                                        </select>
                                                        <label className="floating-label">Kecamatan </label>
                                                        {!selectedKecamatan && (
                                                            <p
                                                                style={{
                                                                    color: "#dc3545",
                                                                    fontFamily: "Helvetica",
                                                                    fontSize: 12,
                                                                    transform: "translateY(5px)",
                                                                }}
                                                            >
                                                                {/* {passwordNewError} */}
                                                                Isian tidak boleh kosong
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="col-6"
                                                    style={{
                                                        paddingLeft: "12px"
                                                    }}
                                                >
                                                    <div className="floating-label-wrap" style={{ paddingBottom: 0 }}>
                                                        <select
                                                            className="floating-label-select custom-select-profile form-select"
                                                            name="profil[i_kel]"
                                                            id="kelurahan"
                                                            required
                                                            style={{
                                                                fontFamily: "Helvetica",
                                                                fontWeight: 400,
                                                                whiteSpace: "nowrap",
                                                                overflow: "hidden",
                                                                textOverflow: "ellipsis",
                                                            }}
                                                            value={userDetails?.i_kel}
                                                            onChange={(e) => {
                                                                setPosCode(getPosCode(e.target.value));
                                                                setUserDetails(prevData => ({ ...prevData, i_kel: e.target.value}))
                                                                setSelectedKelurahan(e.target.value);
                                                            }}
                                                        >
                                                            <option value="">Pilih Kelurahan</option>
                                                            {allKelurahan.length &&
                                                                allKelurahan.map((kelurahan) => (
                                                                    <option value={kelurahan.id}>{kelurahan.n}</option>
                                                                ))}
                                                        </select>
                                                        <label className="floating-label">Kelurahan </label>
                                                        {!selectedKelurahan && (
                                                            <p
                                                                style={{
                                                                    color: "#dc3545",
                                                                    fontFamily: "Helvetica",
                                                                    fontSize: 12,
                                                                    transform: "translateY(5px)",
                                                                }}
                                                            >
                                                                {/* {passwordNewError} */}
                                                                Isian tidak boleh kosong
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="floating-label-wrap " style={{ paddingBottom: 0 }}>
                                                <input
                                                    type="text"
                                                    className="floating-label-field"
                                                    name="profil[pos]"
                                                    id="kodepos"
                                                    maxLength="5"
                                                    placeholder="Kodepos"
                                                    value={posCode}
                                                    onChange={(e) => {
                                                        const value = e.target.value;
                                                        if (numOnly.test(value) || value === "") {
                                                            setPosCode(value);
                                                        }
                                                    }}
                                                />
                                                <label className="floating-label">KodePos </label>
                                                {!posCode && (
                                                    <p
                                                        style={{
                                                            color: "#dc3545",
                                                            fontFamily: "Helvetica",
                                                            fontSize: 12,
                                                            transform: "translateY(5px)",
                                                        }}
                                                    >
                                                        {/* {passwordNewError} */}
                                                        Isian tidak boleh kosong
                                                    </p>
                                                )}
                                            </div>

                                            <div
                                                className="form-group text-right"
                                                style={{
                                                    fontSize: "14px",
                                                    fontFamily: "Helvetica",
                                                    fontStyle: "normal",
                                                    fontWeight: "bold",
                                                    lineHeight: "40px",
                                                }}
                                            >
                                                <div
                                                    type="submit"
                                                    className="btn btn-main btn-big btn-profil revamp-btn-hitung"
                                                    style={{
                                                        fontSize: 14,
                                                        fontFamily: "Helvetica",
                                                        fontWeight: 700,
                                                        fontWeight: "bold",
                                                        width: 163,
                                                        height: 48,
                                                        padding: 0,
                                                        display: "flex",
                                                        justifyContent: "center",
                                                        alignItems: "center",
                                                    }}
                                                    onClick={requestRedeem}
                                                >
                                                    Tukar Poin
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default withAuth(Pencarian);
