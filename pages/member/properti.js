import Link from "next/link";
import ItemUnit from "../../components/data/ItemUnit";
import Layout from "../../components/Layout";
import MenuMember from "../../components/MenuMember";
import Breadcrumb from "../../components/section/Breadcrumb";
import { useEffect, useState } from "react";
import { useAppContext } from "../../context";
import SearchIcon from "../../components/element/icons/SearchIcon";
import { useForm } from "react-hook-form";
import NumberFormat from "react-number-format";
import data_all from "../../sample_data/data_property_all";
import withAuth from "../../helpers/withAuth";
import verifiedMember from "../../helpers/verifiedMember";
import { useRouter } from "next/router";
import { defaultHeaders } from "../../utils/defaultHeaders";

const Properti = () => {
  const [properties, setProperties] = useState([]);
  const { userKey, userProfile } = useAppContext();
  const [page, setPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  const [show, setShow] = useState(false);
  const router = useRouter();
  
  useEffect(() => {
    if (userProfile?.agen == 0) {
      router.push('/member/agen/');
    };
  }, [userProfile]);

  useEffect(() => {
    if (userKey) {
      getProperti();
    }
  }, []);

  const getProperti = async () => {
    setLoaded(false);

    const payload = {
      Page: page,
      Limit: 6,
    };

    try {
      const dataProperti = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/member/iklan/show`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_Member: userKey,
            ...defaultHeaders
          },
          body: new URLSearchParams(payload),
        }
      );
      const responseProperti = await dataProperti.json();
      if (responseProperti.IsError) {
        throw responseProperti.ErrToUser;
      } else {
        console.log("ini data properties", properties)
        setProperties(responseProperti.Data);
      }
    } catch (error) {
      console.log(" ~ Error Get Profil Properti : ", error);
      // router.push('/member/agen/');
    } finally {
      setLoaded(true);
    }
  };

  const NotFound = () => {
    return (
      <div
        style={{
          paddingTop: 100,
          textAlign: "center",
        }}
      >
        <SearchIcon />
        <div
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "16px",
            lineHeight: "150%",
            marginTop: 40,
          }}
        >
          Iklan tidak ditemukan
        </div>
      </div>
    );
  };

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout title="Hasil Properti - Dashboard Member | BTN Properti">
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="properti" />
            </div>
            {show ? (
              <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                <div className="d-flex justify-content-between align-items-center mb-3">
                  <h5 className="title_akun mb-2">Tambah Properti Agen</h5>
                </div>
                <div className="row ms-2">
                  <div className="col-md-7" style={{ width: "504px" }}>
                    <form
                      className="form-validation"
                      noValidate
                      validated
                      id="form-properti"
                    >
                      <div className="floating-label-wrap">
                        <input
                          type="text"
                          className="floating-label-field required number"
                          name="profil[no_ktp]"
                          maxLength="16"
                          minLength="16"
                          placeholder="Judul Properti"
                          // value={profileMember?.no_ktp}
                          // onChange={(e) =>
                          //   changeProfil("no_ktp", e.target.value)
                          // }
                        />
                        <label className="floating-label">
                          Judul Properti{" "}
                          <span
                            style={{ color: "red", fontFamily: "Helvetica" }}
                          >
                            *
                          </span>
                        </label>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="tipe"
                              maxLength="16"
                              minLength="16"
                              placeholder="Tipe"
                              // value={profileMember?.no_ktp}
                              // onChange={(e) =>
                              //   changeProfil("no_ktp", e.target.value)
                              // }
                            />
                            <label className="floating-label">
                              Tipe{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div
                            className="floating-label-wrap"
                            // style={{ position: "relative" }}
                          >
                            <NumberFormat
                              allowNegative={false}
                              prefix="Rp. "
                              thousandSeparator="."
                              decimalSeparator=","
                              className={`floating-label-field`}
                              placeholder="Harga Properti"
                              // value={dataPengajuan.harga}
                              // onChange={(e) => changeData("harga", e.target.value)}
                              // disabled={!tipeKPR ? true : false}
                              // onBlur={(e) => handleBlur('harga', e.target.value)}
                              // onKeyPress={handlePressEnter}
                            />

                            <label
                              htmlFor="harga_property"
                              className="floating-label"
                            >
                              Harga Properti{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {dataPengajuan.harga && <p style={{
            position: 'absolute', top: '18px', marginLeft: '15px', fontFamily: 'Helvetica', fontWeight:700, color: '#666666' }}>Rp</p>
          }
          {showErr.harga && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isian Tidak Boleh Kosong
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Jenis</option>
                              <option value="10">Rumah Baru</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Jenis{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="video"
                              maxLength="16"
                              minLength="16"
                              placeholder="Link Video"
                              // value={profileMember?.no_ktp}
                              // onChange={(e) =>
                              //   changeProfil("no_ktp", e.target.value)
                              // }
                            />
                            <label className="floating-label">Video </label>
                          </div>
                        </div>
                      </div>
                      <div className="floating-label-wrap">
                        <textarea
                          style={{ height: "128px" }}
                          className="floating-label-field"
                          rows="4"
                          name="profil[almt]"
                          placeholder="Deskripsi"
                          // value={profileMember?.almt}
                          // onChange={(e) =>
                          //   changeProfil("almt", e.target.value)
                          // }
                        ></textarea>
                        <label className="floating-label">
                          Deskripsi{" "}
                          <span
                            style={{ color: "red", fontFamily: "Helvetica" }}
                          >
                            *
                          </span>
                        </label>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Status Subsudi</option>
                              <option value="10">Subsidi</option>
                              <option value="11">Non-Subsidi</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Status Subsudi{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div
                            className="floating-label-wrap"
                            // style={{ position: "relative" }}
                          >
                            <NumberFormat
                              allowNegative={false}
                              prefix=""
                              // prefix="Rp. "
                              thousandSeparator="."
                              decimalSeparator=","
                              className={`floating-label-field`}
                              placeholder="Luas Tanah"
                              // value={dataPengajuan.harga}
                              // onChange={(e) => changeData("harga", e.target.value)}
                              // disabled={!tipeKPR ? true : false}
                              // onBlur={(e) => handleBlur('harga', e.target.value)}
                              // onKeyPress={handlePressEnter}
                            />

                            <label
                              htmlFor="harga_property"
                              className="floating-label"
                            >
                              Luas Tanah{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {dataPengajuan.harga && <p style={{
            position: 'absolute', top: '18px', marginLeft: '15px', fontFamily: 'Helvetica', fontWeight:700, color: '#666666' }}>Rp</p>
          }
          {showErr.harga && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isian Tidak Boleh Kosong
            </div>
          )} */}
                            <div
                              style={{
                                fontWeight: "400",
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                fontFamily: "Helvetica",
                                color: "#8D8C8C",
                                border: "1px solid #aaaaaa",
                                right: "-1px",
                                top: "-0.5px",
                                position: "absolute",
                                zIndex: "2",
                                width: "48px",
                                height: "48px",
                                backgroundColor: "#EEEEEE",
                                borderTopRightRadius: "8px",
                                borderBottomRightRadius: "8px",
                              }}
                            >
                              m<sup>2</sup>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div
                            className="floating-label-wrap"
                            // style={{ position: "relative" }}
                          >
                            <NumberFormat
                              allowNegative={false}
                              prefix=""
                              // prefix="Rp. "
                              thousandSeparator="."
                              decimalSeparator=","
                              className={`floating-label-field`}
                              placeholder="Luas Bangunan"
                              // value={dataPengajuan.harga}
                              // onChange={(e) => changeData("harga", e.target.value)}
                              // disabled={!tipeKPR ? true : false}
                              // onBlur={(e) => handleBlur('harga', e.target.value)}
                              // onKeyPress={handlePressEnter}
                            />

                            <label
                              htmlFor="harga_property"
                              className="floating-label"
                            >
                              Luas Bangunan{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {dataPengajuan.harga && <p style={{
            position: 'absolute', top: '18px', marginLeft: '15px', fontFamily: 'Helvetica', fontWeight:700, color: '#666666' }}>Rp</p>
          }
          {showErr.harga && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isian Tidak Boleh Kosong
            </div>
          )} */}
                            <div
                              style={{
                                fontWeight: "400",
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                fontFamily: "Helvetica",
                                color: "#8D8C8C",
                                border: "1px solid #aaaaaa",
                                right: "-1px",
                                top: "-0.5px",
                                position: "absolute",
                                zIndex: "2",
                                width: "48px",
                                height: "48px",
                                backgroundColor: "#EEEEEE",
                                borderTopRightRadius: "8px",
                                borderBottomRightRadius: "8px",
                              }}
                            >
                              m<sup>2</sup>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Tahun Bangunan</option>
                              <option value="">2011</option>
                              <option value="">2012</option>
                              <option value="">2013</option>
                              <option value="">2014</option>
                              <option value="">2015</option>
                              <option value="">2016</option>
                              <option value="">2017</option>
                              <option value="">2018</option>
                              <option value="">2019</option>
                              <option value="">2020</option>
                              <option value="">2021</option>
                              <option value="">2022</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Tahun Bangunan{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Kamar Tidur</option>
                              <option value="">1</option>
                              <option value="">2</option>
                              <option value="">3</option>
                              <option value="">4</option>
                              <option value="">5</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Kamar Tidur
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Kamar Mandi</option>
                              <option value="">1</option>
                              <option value="">2</option>
                              <option value="">3</option>
                              <option value="">4</option>
                              <option value="">5</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Kamar Mandi
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Jumlah Lantai</option>
                              <option value="">1</option>
                              <option value="">2</option>
                              <option value="">3</option>
                              <option value="">4</option>
                              <option value="">5</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Jumlah Lantai{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Daya Listrik</option>
                              <option value="">450VA</option>
                              <option value="">500VA</option>
                              <option value="">550VA</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Daya Listrik
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Sertifikat</option>
                              <option value="">Sertifikat Hak Milik</option>
                              <option value="">Sertifikat</option>
                              <option value="">Sertifikat</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Sertifikat{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Status Properti</option>
                              <option value="">Tersedia</option>
                              <option value="">Tidak Tersedia</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Status Properti{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="tipe"
                              maxLength="16"
                              minLength="16"
                              placeholder="Cluster"
                              // value={profileMember?.no_ktp}
                              // onChange={(e) =>
                              //   changeProfil("no_ktp", e.target.value)
                              // }
                            />
                            <label className="floating-label">Cluster</label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="tipe"
                              maxLength="16"
                              minLength="16"
                              placeholder="Blok"
                              // value={profileMember?.no_ktp}
                              // onChange={(e) =>
                              //   changeProfil("no_ktp", e.target.value)
                              // }
                            />
                            <label className="floating-label">Blok</label>
                          </div>
                        </div>
                      </div>
                      <div className="floating-label-wrap">
                        <input
                          type="text"
                          className="floating-label-field required number"
                          name="tipe"
                          maxLength="16"
                          minLength="16"
                          placeholder="No. Rumah"
                          // value={profileMember?.no_ktp}
                          // onChange={(e) =>
                          //   changeProfil("no_ktp", e.target.value)
                          // }
                        />
                        <label className="floating-label">No. Rumah</label>
                      </div>
                      <div className="floating-label-wrap">
                        <textarea
                          style={{ height: "128px" }}
                          className="floating-label-field"
                          rows="4"
                          name="profil[almt]"
                          placeholder="Alamat"
                          // value={profileMember?.almt}
                          // onChange={(e) =>
                          //   changeProfil("almt", e.target.value)
                          // }
                        ></textarea>
                        <label className="floating-label">
                          Alamat{" "}
                          <span
                            style={{ color: "red", fontFamily: "Helvetica" }}
                          >
                            *
                          </span>
                        </label>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Provinsi</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Provinsi{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Kota/Kabupaten</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Kota/Kabupaten{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Kecamatan</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Kecamatan{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <select
                              id="input2"
                              className="floating-label-select custom-select-profile form-select"
                              // value={dataDiri.statMenikah}
                              // onChange={(e) => changeData("statMenikah", e.target.value)}
                              // onBlur={validateStatus}
                              autoComplete="off"
                              // onKeyPress={handlePressEnter}
                              required
                            >
                              <option value="">Kelurahan</option>
                            </select>
                            <label htmlFor="input2" className="floating-label">
                              Kelurahan{" "}
                              <span
                                style={{
                                  color: "red",
                                  fontFamily: "Helvetica",
                                }}
                              >
                                *
                              </span>
                            </label>
                            {/* {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )} */}
                          </div>
                        </div>
                      </div>
                      <div
                        className="floating-label-wrap"
                        style={{ marginBottom: "12px" }}
                      >
                        <input
                          type="text"
                          className="floating-label-field required number"
                          name="tipe"
                          maxLength="16"
                          minLength="16"
                          placeholder="Kodepos"
                          // value={profileMember?.no_ktp}
                          // onChange={(e) =>
                          //   changeProfil("no_ktp", e.target.value)
                          // }
                        />
                        <label className="floating-label">
                          Kodepos{" "}
                          <span
                            style={{ color: "red", fontFamily: "Helvetica" }}
                          >
                            *
                          </span>
                        </label>
                      </div>
                      {/* ------------------------------------------- */}
                      <div className="row">
                        <h5
                          style={{
                            fontSize: "14px",
                            marginBottom: "10px",
                            fontFamily: "FuturaBT",
                            fontWeight: 700,
                          }}
                        >
                          Kelengkapan Rumah
                        </h5>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Dapur
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Jalur PDAM
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Ruang Kerja
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Wifi
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check" style={{display:'flex'}}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Ruang Keluarga
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Jalur Listrik
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check" style={{display:'flex'}}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Jalur Telpon
                            </label>
                          </div>
                        </div>
                      </div>
                      {/* ------------------------------------------- */}
                      <div className="row">
                        <h5
                          style={{
                            fontSize: "14px",
                            marginBottom: "10px",
                            marginTop: "12px",
                            fontFamily: "FuturaBT",
                            fontWeight: 700,
                          }}
                        >
                          Akses
                        </h5>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Rumah Sakit
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Restoran
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Farmasi
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Jalan Tol
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Bioskop
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check" style={{display:'flex'}}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Rumah Ibadah
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Sekolah
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Bar
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Restoran
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Mall
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Halte
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Gerbang Tol
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Bank/ATM
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Stasiun
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              SPBU
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Taman
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Bandara
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Gymnasium
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Pasar
                            </label>
                          </div>
                        </div>
                      </div>
                      {/* ------------------------------------------- */}
                      <div className="row">
                        <h5
                          style={{
                            fontSize: "14px",
                            marginBottom: "10px",
                            marginTop: "12px",
                            fontFamily: "FuturaBT",
                            fontWeight: 700,
                          }}
                        >
                          Fasilitas
                        </h5>
                        <div className="col-md-4 mb-3">
                          <div className="form-check" style={{display:'flex'}}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Kolam Renang
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Elevator
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              CCTV
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check" style={{display:'flex'}}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Tempat Parkir
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Gym
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Lift
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4 mb-3">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Keamanan
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Garasi
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Club House
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Penghijauan
                            </label>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-check" style={{display:'flex'}}>
                            <input
                              className="form-check-input"
                              type="checkbox"
                              value=""
                              id="filter_apartemen"
                              // checked={apartemen}
                              // onChange={(e) => setApartemen(e.target.checked)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="filter_apartemen"
                            >
                              Row Jalan 12
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <h5
                          style={{
                            fontSize: "14px",
                            marginBottom: "10px",
                            marginTop: "12px",
                            fontFamily: "FuturaBT",
                            fontWeight: 700,
                          }}
                        >
                          Lokasi Map
                        </h5>
                        <div className="col-md-12">
                          <div
                            style={{
                              height: "181.13px",
                              border: "1px solid #666666",
                              borderRadius: "8px",
                              marginBottom: "40.87px",
                            }}
                          ></div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="tipe"
                              maxLength="16"
                              minLength="16"
                              placeholder="Latitude"
                              // value={profileMember?.no_ktp}
                              // onChange={(e) =>
                              //   changeProfil("no_ktp", e.target.value)
                              // }
                            />
                            <label className="floating-label">Latitude</label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="floating-label-wrap">
                            <input
                              type="text"
                              className="floating-label-field required number"
                              name="tipe"
                              maxLength="16"
                              minLength="16"
                              placeholder="Longitude"
                              // value={profileMember?.no_ktp}
                              // onChange={(e) =>
                              //   changeProfil("no_ktp", e.target.value)
                              // }
                            />
                            <label className="floating-label">Longitude </label>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <h5
                          style={{
                            fontSize: "14px",
                            marginBottom: "10px",

                            fontFamily: "FuturaBT",
                            fontWeight: 700,
                          }}
                        >
                          Area Sekitar
                        </h5>
                        <div className="col-md-12 d-flex">
                          <input
                            style={{
                              width: "113.5px",
                              borderTopRightRadius: "0px",
                              borderBottomRightRadius: "0px",
                            }}
                            type="text"
                            className="floating-label-field custom-plc"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Nama Tempat"
                            // value={profileMember?.no_ktp}
                            // onChange={(e) =>
                            //   changeProfil("no_ktp", e.target.value)
                            // }
                          />
                          <select
                            style={{
                              width: "85.21px",
                              borderRadius: "0px",
                              fontWeight: "400",
                            }}
                            id="input2"
                            className="floating-label-select custom-select-small form-select"
                            // value={dataDiri.statMenikah}
                            // onChange={(e) => changeData("statMenikah", e.target.value)}
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Pasar</option>
                          </select>
                          <input
                            style={{
                              width: "131.16px",
                              borderRadius: "0px",
                            }}
                            type="text"
                            className="floating-label-field custom-plc"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Koordinat Lokasi"
                            // value={profileMember?.no_ktp}
                            // onChange={(e) =>
                            //   changeProfil("no_ktp", e.target.value)
                            // }
                          />
                          <input
                            style={{
                              width: "67.1px",
                              borderRadius: "0px",
                            }}
                            type="text"
                            className="floating-label-field custom-plc"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Jarak"
                            // value={profileMember?.no_ktp}
                            // onChange={(e) =>
                            //   changeProfil("no_ktp", e.target.value)
                            // }
                          />
                          <div
                            style={{
                              fontWeight: "400",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              fontFamily: "Helvetica",
                              color: "#8D8C8C",
                              border: "1px solid #aaaaaa",

                              width: "48px",
                              height: "48px",
                              backgroundColor: "#EEEEEE",
                              borderRadius: "0px",
                            }}
                          >
                            m
                          </div>
                          <div
                            style={{
                              fontWeight: "400",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              fontFamily: "Helvetica",

                              border: "1px solid #aaaaaa",

                              width: "43.8px",
                              height: "48px",
                              backgroundColor: "#15CF74",
                              borderRadius: "0px",
                            }}
                          >
                            <img src="/icons/icons/icon_plus.svg"></img>
                          </div>
                          <div
                            style={{
                              fontWeight: "400",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              fontFamily: "Helvetica",

                              border: "1px solid #aaaaaa",

                              width: "43.8px",
                              height: "48px",
                              backgroundColor: "#F9C014",
                              borderRadius: "0px",
                              borderBottomRightRadius: "8px",
                              borderTopRightRadius: "8px",
                            }}
                          >
                            <img src="/icons/icons/icon_location.svg"></img>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <h5
                          style={{
                            fontSize: "14px",
                            marginBottom: "4px",
                            marginTop: "18px",
                            fontFamily: "FuturaBT",
                            fontWeight: 700,
                          }}
                        >
                          Foto
                        </h5>
                        <div className="col-md-12 d-flex">
                          <p
                            style={{
                              fontSize: "10px",
                              fontWeight: "400",
                              fontFamily: "Helvetica",
                              color: "#000000",
                            }}
                          >
                            Dengan mengunggah foto, Anda menyetujui bahwa Anda
                            memiliki hak atas gambar tersebut atau bahwa Anda
                            memiliki izin atau lisensi dan setuju untuk memenuhi
                            Syarat & Ketentuan BTN Properti.
                          </p>
                        </div>
                        <div className="d-flex justify-content-end mb-4">
                          <button
                            type="button"
                            className="d-flex align-items-center justify-content-center btn btn-outline-main"
                            // data-bs-toggle="modal"
                            // data-bs-target="#modalKonsultasi"
                            style={{
                              width: "156px",
                              height: "48px",
                              fontFamily: "Helvetica",
                              fontWeight: 700,
                            }}
                          >
                            <img
                              className="me-2"
                              src="/icons/icons/icon_plus_blue.svg"
                            ></img>{" "}
                            Tambah Foto
                          </button>
                        </div>
                        <div className="col-md-3 d-flex">
                          <img
                            style={{ width: "111px", borderRadius: "8px" }}
                            src="/images/properti/6.png"
                          ></img>
                        </div>
                        <div className="col-md-3 d-flex">
                          <img
                            style={{ width: "111px", borderRadius: "8px" }}
                            src="/images/properti/6.png"
                          ></img>
                        </div>
                        <div className="col-md-3 d-flex">
                          <img
                            style={{ width: "111px", borderRadius: "8px" }}
                            src="/images/properti/6.png"
                          ></img>
                        </div>
                        <div className="col-md-3 d-flex">
                          <img
                            style={{ width: "111px", borderRadius: "8px" }}
                            src="/images/properti/6.png"
                          ></img>
                        </div>
                        <div
                          style={{ marginTop: "80px" }}
                          className="d-flex justify-content-center"
                        >
                          <button
                            type="button"
                            className="d-flex align-items-center justify-content-center btn btn-main"
                            // data-bs-toggle="modal"
                            // data-bs-target="#modalKonsultasi"
                            style={{
                              width: "302px",
                              height: "48px",
                              fontFamily: "Helvetica",
                              fontWeight: 700,
                            }}
                          >
                            Simpan
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            ) : (
              <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                <div className="d-flex justify-content-between align-items-center mb-3">
                  <h5 className="title_akun mb-0">Properti</h5>

                  <a
                    onClick={() => router.push('/member/update-properti')}
                    className="btn btn-main"
                    style={{
                      display: "flex",
                      alignItems: "center",
                      width: 175,
                      height: 48,
                      fontFamily: "Helvetica",
                      fontWeight: "Bold",
                      marginLeft: "10px",
                    }}
                  >
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      style={{
                        marginLeft: 2,
                        marginRight: 5,
                      }}
                    >
                      <path
                        d="M19 13H13V19H11V13H5V11H11V5H13V11H19V13Z"
                        fill="white"
                      />
                    </svg>
                    Tambah Properti
                  </a>
                </div>

                {properties.length > 0 ? (
                  <div className="row">
                    {properties.map((data, index) => (
                      <div key={index} className="col-6 col-md-4 mb-4">
                        <ItemUnit
                          data={data}
                          action="true"
                          detail="true"
                          getProperti={getProperti}
                          disableDev="true"
                        />
                      </div>
                    ))}
                    {/* {data_all.filter((data) => data.me == 1).map((data, index) =>
                                        <div key={index} className="col-md-4 mb-4">
                                            <ItemUnit data={data} action="true" detail="true" />
                                        </div>
                                    )} */}
                  </div>
                ) : (
                  NotFound()
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default (withAuth(Properti));
// export default withAuth(Properti);
