import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useSession } from "next-auth/client";
import Cookies from "js-cookie";
import MenuMember from "../../components/MenuMember";
import Layout from "../../components/Layout";
import Navbar from "../../components/Navbar";
import { useAppContext } from "../../context";
import { useRouter } from "next/router";
import withAuth from "../../helpers/withAuth";

function Dashboard() {
  const [isDashboard, setDashboard] = useState(false);
  const [active, setActive] = useState("");
  useEffect(() => {
    if (
      Cookies.get("user") ||
      Cookies.get("keyMember") ||
      sessionStorage.getItem("user") ||
      sessionStorage.getItem("keyMember") ||
      localStorage.getItem("user") ||
      localStorage.getItem("keyMember")
    ) {
      setDashboard(true);
    } else {
      setDashboard(false);
    }
  });

  useEffect(() => {
    const lastRoute = sessionStorage.getItem('log-rts-mmbr');
    if (lastRoute) {
      setActive(lastRoute);
    } else {
      setActive("profil");
    };
  }, []);

  return (
    <Layout title="Dashboard Profil | BTN Properti">
      <div
        id="dashboard_profil"
        style={{
          padding: "5px 20px",
          width: "100%",
          height: "100%",
          backgroundColor: "#fff",
          display: "block",
          marginTop: "72px",
        }}
      >
        {/* <Navbar /> */}
        <MenuMember active={active} isDashboard={isDashboard} />
      </div>
    </Layout>
  );
}

export default withAuth(Dashboard)