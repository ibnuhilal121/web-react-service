import { useEffect, useState } from "react";
import axios from "axios";
import qs from "qs";
import Layout from "../../components/Layout";
import MenuMember from "../../components/MenuMember";
import Breadcrumb from "../../components/section/Breadcrumb";
import { useAppContext } from "../../context";
import withAuth from "../../helpers/withAuth";
import verifiedMember from "../../helpers/verifiedMember";
import { getAgent } from "../../services/member";
import { useRouter } from "next/router";
import { defaultHeaders } from "../../utils/defaultHeaders";

const options = [
  {
    name: "(Y) Pro",
    value: 0,
  },
  {
    name: "[X]PRO Reality",
    value: 1,
  },
  {
    name: "*Bandung Independent Agent",
    value: 2,
  },
  {
    name: "*BestPro Indonesia",
    value: 3,
  },
  {
    name: "*Agen Independen",
    value: 4,
  },
  {
    name: "1 PROPERTY",
    value: 5,
  },
  {
    name: "1001 Property",
    value: 6,
  },
  {
    name: "12 Property",
    value: 7,
  },
  {
    name: "129PROPERTY",
    value: 8,
  },
  {
    name: "18 Jaya Properti",
    value: 9,
  },
];

const Agen = () => {
  const { userKey, userProfile, setUserProfile } = useAppContext();
  const router = useRouter();
  const [Agen, setAgen] = useState("");
  const [idAgen, setIdAgen] = useState(null);
  const [data, setData] = useState(0);
  const [search, setSearch] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [isToggleSubmit, setToggleSubmit] = useState(false);
  const [isNoResult, setNoResult] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(null);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (isToggleSubmit) {
      console.log('~ is agen : ', data)
      let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/updatestatusagen`;
      let body = data;
      let header = {
        headers: {
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
      };
  
      axios
        .post(endpoint, qs.stringify(body), header)
        .then((res) => {
          if (res.data.IsError) {
            console.log(res.data.ErrToDev);
            return console.log(res.data.ErrToUser);
          }
          let temp = { ...userProfile };
          temp["agen"] = Number(Agen);
          if (idAgen) {
            temp["idAgen"] = Number(idAgen.id);
            temp["namaAgensi"] = idAgen.n;
          } else {
            temp["idAgen"] = null;
            temp["namaAgensi"] = '';
          }
          setUserProfile(temp);
          
          console.log(res.data.Output);
          console.log('~ agen : ', Agen, typeof Agen)
          if (Agen === '1' || Agen === '2') {
            router.push('/member/properti/');
          };
        })
        .catch((err) => {
          console.log(err.message)
        })
        .finally(() => setToggleSubmit(false))
    }
  }, [isToggleSubmit]);

  const hitAgensiName = async (inputPage) => {
    try {
      let body = {
        n: searchQuery, // STR optional
        Page: inputPage || page, // INT optional
        Limit: 10, // INT optional
      };

      getAgent(body)
        .then((res) => {
          if (res.IsError) {
            return console.log(res.ErrToUser);
          }
          setSearchResults(res.Data);
          setTotalPage(res.Paging.JmlHalTotal);
        })
        .catch((err) => console.log(err.message));

    } catch (error) {

    }
  };

  const handleChangePage = (inputPage) => {
    hitAgensiName(inputPage);
    setPage(inputPage);
  };

  const handleChange = (event) => {
    const id = event.target.id;
    const value = event.target.value;

    if (id === "is_agen") setAgen(value);
    if (id === "n_agensi_cari") setSearch(value);
  };

  const handleSearch = () => {
    setSearchQuery(search);
    setPage(1);
  };

  useEffect(() => {
    hitAgensiName(1);
  }, [searchQuery]);

  const handleKeyPress = (e) => {
    if (e.key === "Enter") handleSearch();
  };

  const handleClick = (e, id_agn) => {
    const id = e.target.id;
    setSearch("");
    setSearchQuery("");
    if (id === "id_agn") setIdAgen(id_agn);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (Agen)
      setData({
        is_agn: parseInt(e.target[0].value),
        i_agn: Agen === "2" ? idAgen.id : null,
      });
    setToggleSubmit(true);
  };

  useEffect(() => {
    if ([0, 1, 2].includes(userProfile?.agen)) {
      setAgen(userProfile.agen.toString());
      if (userProfile.agen == 2) {
        setIdAgen({
          id: Number(userProfile.idAgen),
          n: userProfile.namaAgensi
        });
      };
    };
    setLoaded(true);
  }, [userProfile]);

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
          minWidth: "100%"
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout title="Hasil Agen - Dashboard Member | BTN Properti">
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-3">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="agen" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8">
              <h5 className="title_akun " style={{ fontFamily: "FuturaBT", fontWeight: 700 }}>
                Agen
              </h5>
              <div className="dashboard-agen">
                <div className="row">
                  <div className="col">
                    <form
                      className="form-validation"
                      onSubmit={handleSubmit}
                      id="form-agen"
                      noValidate="novalidate"
                    >
                      <div id="row_agen" className="row w-100">
                        <div className="col-12">
                          <div className="form-group">
                            {Agen !== "" && (
                              <label
                                style={{
                                  fontFamily: "FuturaBT",
                                  fontWeight: 700,
                                }}
                              >
                                Apakah anda seorang Agen Properti ?
                              </label>
                            )}
                            <div>
                              <select
                                className={
                                  Agen === ""
                                    ? "floating-label-select custom-select form-select select-agen"
                                    : "floating-label-select custom-select form-select selected-agen"
                                }
                                name="agen[is_agen]"
                                id="is_agen"
                                aria-invalid="false"
                                onChange={handleChange}
                                value={Agen}
                                style={{
                                  color: "#00193E",
                                  fontSize: "14px",
                                  height: 48,
                                  padding: "10px 15px",
                                  width:"100%",
                                  maxWidth:"480px"
                                }}
                              >
                                {Agen === "" && (
                                  <option
                                    value=""
                                    style={{
                                      fontFamily: "Helvetica",
                                      fontWeight: 400,
                                    }}
                                  >
                                    Apakah anda seorang Agen Properti ?
                                  </option>
                                )}
                                <option
                                  value="0"
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                >
                                  Bukan, saya bukan agen properti.
                                </option>
                                <option
                                  value="1"
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                >
                                  Ya, saya agen properti perorangan.
                                </option>
                                <option
                                  value="2"
                                  style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                  }}
                                >
                                  Ya, saya agen properti dalam agensi.
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>
                        {Agen === "2" && (
                          <div className="col-12">
                            <div className="form-group input-group-append">
                              <div
                                className="floating-label-select "
                                style={{
                                  maxWidth: 480,
                                  height: 48,
                                  fontFamily: "Helvetica",
                                  fontWeight: 400,
                                }}
                              >
                                <p
                                  style={{
                                    fontFamily: "FuturaBT",
                                    fontWeight: 700,
                                    fontSize: "14px",
                                    color: "#00193e",
                                  }}
                                >
                                  {idAgen ? idAgen.n ? idAgen.n : "Nama Agensi" : "Nama Agensi"}
                                </p>
                                <div
                                  style={{
                                    transform: "translateX(94%)",
                                    fontFamily: "Helvetica",
                                    fontWeight: 400,
                                    color: "#0061A7",
                                  }}
                                >
                                  <div
                                    className="input-group-append"
                                    style={{
                                      transform: "translateY(-45px)",
                                      width: "17.49px",
                                      height: "17.49px",
                                    }}
                                  >
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      data-bs-toggle="modal"
                                      data-bs-target="#modal-agensi"
                                      style={{
                                        transform: "translateY(8px)",
                                        marginLeft: 10,
                                      }}
                                      onClick={hitAgensiName}
                                    >
                                      <path
                                        d="M15.5 14H14.71L14.43 13.73C15.41 12.59 16 11.11 16 9.5C16 5.91 13.09 3 9.5 3C5.91 3 3 5.91 3 9.5C3 13.09 5.91 16 9.5 16C11.11 16 12.59 15.41 13.73 14.43L14 14.71V15.5L19 20.49L20.49 19L15.5 14ZM9.5 14C7.01 14 5 11.99 5 9.5C5 7.01 7.01 5 9.5 5C11.99 5 14 7.01 14 9.5C14 11.99 11.99 14 9.5 14Z"
                                        fill="#0061A7"
                                      />
                                    </svg>
                                  </div>
                                  ,
                                </div>
                              </div>
                            </div>
                          </div>
                        )}

                        <div className="col-12">
                          <div className="form-group button-submit-area text-right">
                            {Agen !== "" && (
                              <button
                                type="submit"
                                className="btn btn-main"
                                style={{
                                  fontFamily: "Helvetica",
                                  fontWeight: 700,
                                  width: 83,
                                  height: 46,
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                }}
                              >
                                Simpan
                              </button>
                            )}
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal fade"
        id="modal-agensi"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myLargeModalLabel"
        aria-hidden="true"
        style={{ fontFamily: "Helvetica",zIndex:"9999" }}
      >
        <div className="px-2 modal-dialog modal-dialog-centered mx-auto" style={{ maxWidth: "480px" }}>
          <div className="modal-content modal_agen">
            <div className="close-modal" data-bs-dismiss="modal">
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <div className="">
              <h5 className="modal-title">Pilih Agensi</h5>
              {/* <button style={{color:"#0061A7"}} type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> */}
            </div>
            <div className="modal-body p-0">
              <div className="row g-md-0">
                <div className="col-9 mx-0">
                  <div className="floating-label-wrap">
                    <div className="input-group mb-0">
                      <input
                        style={{ height: "46px", maxWidth: "250px",width:"75%" }}
                        type="text"
                        id="n_agensi_cari"
                        value={search}
                        onKeyPress={handleKeyPress}
                        onChange={(e) => setSearch(e.target.value)}
                        className="floating-label-field custom-plc-profesional"
                        placeholder="Cari Agensi"
                        aria-label="Cari Agensi"
                      />
                      <label htmlFor="n_agensi_cari" className="floating-label">
                        Cari Agensi
                      </label>
                      <div className="input-group-append" style={{width:"25%"}}>
                        <button
                          onClick={handleSearch}
                          className="btn btn-primary btn-lg active w-100"
                          type="button"
                          style={{
                            maxWidth: "70px",
                            height: "46px",
                            backgroundColor: "#0061A7",
                            fontFamily: "Helvetica",
                            fontWeight: "700",
                            fontSize: "14px",
                            padding: "0px",
                            borderRadius: "0px 8px 8px 0px",
                          }}
                        >
                          <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.7799 16.7203L13.3031 12.2436C14.5231 10.7515 15.1229 8.84764 14.9785 6.92574C14.8341 5.00384 13.9565 3.21097 12.5273 1.91796C11.0981 0.624952 9.22654 -0.0692619 7.29983 -0.0210881C5.37311 0.0270858 3.53862 0.813962 2.1758 2.17678C0.812985 3.5396 0.0261093 5.37409 -0.0220646 7.3008C-0.0702385 9.22752 0.623976 11.099 1.91698 12.5283C3.20999 13.9575 5.00287 14.8351 6.92477 14.9795C8.84667 15.1239 10.7505 14.524 12.2426 13.3041L16.7194 17.7808C16.8608 17.9174 17.0503 17.993 17.2469 17.9913C17.4436 17.9896 17.6317 17.9107 17.7707 17.7717C17.9098 17.6326 17.9886 17.4445 17.9904 17.2479C17.9921 17.0512 17.9165 16.8618 17.7799 16.7203ZM7.4996 13.5006C6.31291 13.5006 5.15287 13.1487 4.16617 12.4894C3.17948 11.8301 2.41044 10.893 1.95632 9.79667C1.50219 8.70032 1.38337 7.49392 1.61488 6.33003C1.84639 5.16614 2.41784 4.09704 3.25695 3.25793C4.09607 2.41881 5.16517 1.84737 6.32905 1.61586C7.49294 1.38435 8.69934 1.50317 9.7957 1.95729C10.8921 2.41142 11.8291 3.18045 12.4884 4.16715C13.1477 5.15384 13.4996 6.31388 13.4996 7.50057C13.4978 9.09132 12.8651 10.6164 11.7403 11.7412C10.6154 12.8661 9.09035 13.4988 7.4996 13.5006Z" fill="white" />
                          </svg>{" "}
                          Cari
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 px-0">
                  <div className="row justify-content-bewtween px-0 mx-0">
                    <div className="col px-0 mx-0 text-center me-1 me-md-0">
                      <div
                        className="btn-group w-100"
                        role="group"
                        style={{ maxWidth: "45.59px", height: "45.59px", marginTop: "5px" }}
                      >
                        <button
                          type="button"
                          style={{
                            backgroundColor: "#0061A7",
                            borderRadius: "8px",
                            padding:"0"
                          }}
                          className="btn btn-secondary btn-paging"
                          onClick={() => handleChangePage(page - 1)}
                          disabled={page === 1}
                        >
                          <svg
                            width="14"
                            height="21"
                            viewBox="0 0 14 21"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M13.1299 18.5859L5.33585 10.7944L13.1299 3.00285L10.7304 0.609375L0.519883 10.7944L10.7304 20.9794L13.1299 18.5859Z"
                              fill="white"
                            />
                          </svg>
                        </button>
                      </div>
                    </div>
                    <div className="col px-0 text-center">
                      <div
                        className="btn-group w-100"
                        role="group"
                        style={{ maxWidth: "45.59px", height: "45.59px", marginTop: "5px" }}
                      >
                        <button
                          type="button"
                          style={{
                            backgroundColor: "#0061A7",
                            borderRadius: "8px",
                            padding:"0"
                          }}
                          className="btn btn-secondary btn-paging"
                          onClick={() => handleChangePage(page + 1)}
                          disabled={page === totalPage}
                        >
                          <svg
                            width="14"
                            height="21"
                            viewBox="0 0 14 21"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M0.870119 3.00394L8.66415 10.7955L0.870117 18.587L3.26959 20.9805L13.4801 10.7955L3.26959 0.610468L0.870119 3.00394Z"
                              fill="white"
                            />
                          </svg>
                        </button>
                      </div>
                    </div>


                  </div>
                </div>

                <div className="col-12" style={{ padding: "0 5px 0 5px" }}>
                  <div className="list-agensi list-group agency-result">
                    <ul className="">
                      {isNoResult
                        ? "No Result"
                        : searchResults?.map((result, index) => (
                          <li key={index}>
                            <button
                              id="id_agn"
                              data-bs-dismiss="modal"
                              onClick={(e) => handleClick(e, result)}
                              style={{}}
                            >
                              {result.n}
                            </button>
                          </li>
                        ))}
                    </ul>
                    <div className="text-center">
                      <i className="bi bi-circle-o-notch fa-spin fa-3x fa-fw"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              {/* <button type="button" style={{backgroundColor : "#0061A7"}} className="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button> */}
              <div className="form-group button-submit-area text-right">
                <button
                  type="submit"
                  className="btn btn-main"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    width: 102,
                    height: 42.16,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  Batal
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default (withAuth(Agen));
// export default withAuth(Agen);

