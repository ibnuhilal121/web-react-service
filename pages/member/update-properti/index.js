import Link from "next/link";
import ItemUnit from "../../../components/data/ItemUnit";
import Layout from "../../../components/Layout";
import MenuMember from "../../../components/MenuMember";
import Breadcrumb from "../../../components/section/Breadcrumb";
import Lottie from "react-lottie";
import * as animationData from "../../../public/x_animation.json";
import succes from "../../../public/succes_animation.json";
import { useEffect, useState, useRef, Fragment } from "react";
import { useAppContext } from "../../../context";
import NumberFormat from "react-number-format";
import withAuth from "../../../helpers/withAuth";
import verifiedMember from "../../../helpers/verifiedMember";
import {
  getFasilitas,
  getProvince,
  getKota,
  getKecamatan,
  getKelurahan,
  getAreaSekitar,
} from "../../../services/master";
import {
  addIklan,
  updateAreaSekitar,
  getDetailIklan,
} from "../../../services/member";
import { useRouter } from "next/router";
import { uploadImageIklan } from "../../../services/upload";
import GoogleMapsDynamic from "../../../components/GoogleMapsDynamic";
import GoogleMapsPopUp from "../../../components/GoogleMapsPopUp";

const generateListTahun = () => {
  let yearNow = new Date().getFullYear();
  let tempList = [];
  for (let i = yearNow - 11; i <= yearNow; i++) {
    tempList.push(i);
  }
  return tempList;
};

const listTahun = generateListTahun();

const Properti = () => {
  const { userKey } = useAppContext();
  const router = useRouter();
  const { id } = router.query;
  const [addMargin, setAddMargin] = useState(100);
  const [loaded, setLoaded] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [kelengkapan, setKelengkapan] = useState([]);
  const [akses, setAkses] = useState([]);
  const [fasilitas, setFasilitas] = useState([]);
  const [alertText, setAlertText] = useState("");
  const [alert,setalert] = useState(false);
  const [succesModal ,setSuccesModal] = useState(false);
  const [sekitar, setSekitar] = useState({
    n: "",
    i_kat: "",
    koordinat: "",
    jrk: "",
  });
  const [data, setData] = useState({
    jdl: "",
    typ: "",
    hrg: "",
    jns: "",
    vdo: "",
    dsk: "",
    st_ssd: "",
    ls_tnh: "",
    ls_bgn: "",
    thn_bgn: "",
    km_tdr: "",
    km_mnd: "",
    lt: "",
    dy_lstr: "",
    srtfkt: "",
    st_prpt: "",
    clstr: "",
    blk: "",
    no: "",
    almt: "",
    fslts: "",
    lat: "",
    lon: "",
    gbr1: "",
    gbr2: "",
    gbr3: "",
    gbr4: "",
    gbr5: "",
  });
  const [allProvinsi, setAllProvinsi] = useState([]);
  const [provinsi, setProvinsi] = useState("");
  const [allKota, setAllKota] = useState([]);
  const [kota, setKota] = useState("");
  const [allKecamatan, setAllKecamatan] = useState([]);
  const [kecamatan, setKecamatan] = useState("");
  const [allKelurahan, setAllKelurahan] = useState([]);
  const [kelurahan, setKelurahan] = useState("");
  const [kodepos, setKodepos] = useState("");
  const [images, setImages] = useState([]);
  const inputRef = useRef();
  const [areaSekitar, setAreaSekitar] = useState([]);
  const [gabunganFasilitas, setGabunganFasilitas] = useState(null);
  const [gabunganLoaded, setGabunganLoaded] = useState(false);
  const modalMapsRef = useRef();
  const [indexMaps, setIndexMaps] = useState("");
  const [current, setCurrent] = useState(null);
  const [texterr,setTextErr] = useState("");

  const deleteSpace = () => {
    setAddMargin(addMargin - 60);
  };

  useEffect(() => {
    setLoaded(false);
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        setCurrent({ lat: pos.coords.latitude, lon: pos.coords.longitude });
      });
    }

    const arrPromises = [getFasilitas(), getProvince()];
    Promise.all(arrPromises)
      .then((res) => {
        const [resFasilitas, resProvince] = res;
        if (resFasilitas.IsError || resProvince.IsError) {
          throw res;
        } else {
          const arrData = resFasilitas.Data;
          let tempKelengkapan = [];
          let tempAkses = [];
          let tempFasilitas = [];
          for (let i = 0; i < arrData.length; i++) {
            switch (arrData[i].i_par) {
              case 1:
                tempKelengkapan.push({ ...arrData[i], checked: false });
                break;
              case 9:
                tempAkses.push({ ...arrData[i], checked: false });
                break;
              case 28:
                tempFasilitas.push({ ...arrData[i], checked: false });
                break;
              default:
                break;
            }
          }
          setKelengkapan(tempKelengkapan);
          setAkses(tempAkses);
          setFasilitas(tempFasilitas);
          setAllProvinsi(resProvince.Data);
        }
      })
      .catch((error) => { })
      .finally(() => {
        getExistingData();
      });
  }, []);

  const getExistingData = () => {
    if (id) {
      let arrPromises = [getDetailIklan(userKey, id), getAreaSekitar(id)];
      Promise.all(arrPromises)
        .then((res) => {
          const [resDetailIklan, resAreaSekitar] = res;
          const dataDetail = resDetailIklan.Data[0];
          let dataArea = resAreaSekitar.data;

          let tempData = { ...data };
          let tempFasilitas;
          let margin = 0;

          let listGambar = JSON.parse(dataDetail["lst_gmbr"]);
          listGambar = listGambar.filter((gambar) => {
            return (gambar && gambar !== 'null');
          });

          for (let value in tempData) {
            if (value === "fslts" && dataDetail[value]) {
              tempFasilitas = JSON.parse(dataDetail[value]);
            } else {
              tempData[value] = dataDetail[value];
            }
          }

          for (let i = 0; i < dataArea.length; i++) {
            dataArea[i]["koordinat"] =
              dataArea[i]["lat"] + ", " + dataArea[i]["lon"];
            dataArea[i]["i_kat"] = dataArea[i]["i_sub_kat"];
            margin += 60;
          }
          
          setAddMargin(addMargin + margin);
          setAreaSekitar(dataArea);
          setProvinsi(dataDetail["i_prop"]);
          setKota(dataDetail["i_kot"]);
          setKecamatan(dataDetail["i_kec"]);
          setKelurahan(dataDetail["i_kel"]);
          setKodepos(dataDetail["pos"]);
          setData(tempData);
          setImages(listGambar);
          setGabunganFasilitas(tempFasilitas);
        })
        .catch((error) => { })
        .finally(() => setGabunganLoaded(true));
    } else {
      setGabunganLoaded(true);
    }
  };

  useEffect(() => {
    if (gabunganLoaded) {
      if (id) {
        // ========== Break and categorize "Fasilitas" ==========
        for (let key in gabunganFasilitas) {
          for (let innerKey in gabunganFasilitas[key]) {
            if (key == "Akses") {
              let tempObjFasilitas = [...akses];
              for (let i = 0; i < tempObjFasilitas.length; i++) {
                if (tempObjFasilitas[i].id == innerKey) {
                  tempObjFasilitas[i].checked = true;
                }
              }
              setAkses(tempObjFasilitas);
            } else if (key == "Fasilitas") {
              let tempObjFasilitas = [...fasilitas];
              for (let i = 0; i < tempObjFasilitas.length; i++) {
                if (tempObjFasilitas[i].id == innerKey) {
                  tempObjFasilitas[i].checked = true;
                }
              }
              setFasilitas(tempObjFasilitas);
            } else if (key == "Kelengkapan Rumah") {
              let tempObjFasilitas = [...kelengkapan];
              console.log("from kelengkapan : ", tempObjFasilitas);
              for (let i = 0; i < tempObjFasilitas.length; i++) {
                if (tempObjFasilitas[i].id == innerKey) {
                  tempObjFasilitas[i].checked = true;
                }
              }
              setKelengkapan(tempObjFasilitas);
            }
          }
        }
        // ========== Break and categorize "Fasilitas" ==========
      }
      setLoaded(true);
    }
  }, [gabunganLoaded]);

  const handleChange = (type, value) => {
    let tempData = { ...data };
    let tempValue = value;
    if (type === "hrg" || type === "ls_tnh" || type === "ls_bgn") {
      tempValue = tempValue.replaceAll(" ", "");
    }
    if (type === "hrg" || type === "ls_tnh" || type === "ls_bgn") {
      tempValue = tempValue.replaceAll(".", "");
    }
    if (type === "latlon") {
      tempData["lat"] = tempValue[0];
      tempData["lon"] = tempValue[1];
    } else {
      tempData[type] = tempValue;
    }
    setData(tempData);
  };

  const addImage = (e,maxSize) => {
    if (
      (e.target.files[0]?.type.includes("jpeg") ||
      e.target.files[0]?.type.includes("jpg") ||
      e.target.files[0]?.type.includes("png") ||
      e.target.files[0]?.type.includes("pdf")) &&
    // (e.target.files[0].size / 1048576).toFixed(2) < maxSize
    +(e.target.files[0]?.size) <= (maxSize * 1000000)
    ) {
      // setErrImage('');
      if (e.target.files && e.target.files.length > 0) {
        setTextErr("");
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = function () {
          const payload = {
            file: reader.result,
            extension: e.target.files[0].name.split(".").pop(),
          };
          let tempImages = [...images];
          tempImages.push(payload);
          setImages(tempImages);
        };

        reader.onerror = function (error) {
          // console.log('error attach image : ', error);
        };
      }
    } else {
      if (!e.target.files[0]?.type.includes("jpeg") &&
      !e.target.files[0]?.type.includes("jpg") &&
      !e.target.files[0]?.type.includes("png") &&
      !e.target.files[0]?.type.includes("pdf")) {
        setTextErr("File yang dimasukan tidak sesuai format");
      } else if (+(e.target.files[0]?.size) > (maxSize * 1000000)) {
        setTextErr("Image lebih dari 3MB");
      };
    }
  };

  const changeSekitar = (type, value) => {
    let tempData = { ...sekitar };
    tempData[type] = value;
    setSekitar(tempData);
  };

  const changeAreaSekitar = (type, index, value) => {
    let tempData = [...areaSekitar];
    tempData[index][type] = value;
    setAreaSekitar(tempData);
  };

  const changeFasilitas = (type, id) => {
    let tempData = [];
    switch (type) {
      case "kelengkapan":
        tempData = [...kelengkapan];
        break;
      case "akses":
        tempData = [...akses];
        break;
      case "fasilitas":
        tempData = [...fasilitas];
        break;
      default:
        break;
    }
    for (let i = 0; i < tempData.length; i++) {
      if (tempData[i].id == id) {
        tempData[i].checked = !tempData[i].checked;
      }
    }
    switch (type) {
      case "kelengkapan":
        setKelengkapan(tempData);
        break;
      case "akses":
        setAkses(tempData);
        break;
      case "fasilitas":
        setFasilitas(tempData);
        break;
      default:
        break;
    }
  };

  const addLineSekitar = () => {
    let arrTemp = [...areaSekitar];
    arrTemp.push(sekitar);
    setAreaSekitar(arrTemp);
    setSekitar({
      n: "",
      i_kat: "",
      koordinat: "",
      jrk: "",
    });
  };

  const removeLineSekitar = (index) => {
    let arrTemp = [...areaSekitar];
    arrTemp.splice(index, 1);
    setAreaSekitar(arrTemp);
  };

  const submitIklan = (e) => {
    e.preventDefault();
    setLoading(true);
    let arrUpload = [];
    for (let i = 0; i < images.length; i++) {
      if (images[i].file) {
        arrUpload.push(
          uploadImageIklan("member-properti", images[i].extension, images[i].file)
        );
      }
    }
    Promise.all(arrUpload)
      .then((res) => {
        let tempData = { ...data };
        tempData["i_prop"] = provinsi;
        tempData["i_kot"] = kota;
        tempData["i_kec"] = kecamatan;
        tempData["i_kel"] = kelurahan;
        tempData["pos"] = kodepos;
        let tempImages = [];
        if (images.length > 0) {
          for (let i = 0; i < images.length; i++) {
            if (!images[i].file) {
              tempImages.push(images[i]);
            }
          }
        }
        for (let i = 0; i < res.length; i++) {
          if (!res[i].IsError) {
            tempImages.push(res[i].Output);
          }
        }
        for (let i = 0; i < tempImages.length; i++) {
          tempData[`gbr${i + 1}`] = tempImages[i];
        }
        // for(let i = 0; i < res.length; i++) {
        //   if (!res[i].IsError) {
        //     tempData[`gbr${i+1}`] = res[i].Output;
        //   };
        // };
        let tempFslts = {
          "Kelengkapan Rumah": {},
          Akses: {},
          Fasilitas: {},
        };
        for (let i = 0; i < kelengkapan.length; i++) {
          if (kelengkapan[i].checked) {
            tempFslts["Kelengkapan Rumah"][kelengkapan[i].id] =
              kelengkapan[i].n;
          }
        }
        for (let i = 0; i < akses.length; i++) {
          if (akses[i].checked) {
            tempFslts["Akses"][akses[i].id] = akses[i].n;
          }
        }
        for (let i = 0; i < fasilitas.length; i++) {
          if (fasilitas[i].checked) {
            tempFslts["Fasilitas"][fasilitas[i].id] = fasilitas[i].n;
          }
        }
        tempData.fslts = JSON.stringify(tempFslts);
        tempData["id"] = id;
        const tipeAPI = id ? "update" : "new";
        return addIklan(userKey, tempData, tipeAPI);
      })
      .then((res) => {
        if (res.IsError) {
          throw res;
        } else {
          if (areaSekitar.length > 0) {
            let tempAreaSekitar = [...areaSekitar];
            if (
              sekitar.n ||
              sekitar.i_kat ||
              sekitar.koordinat ||
              sekitar.jrk
            ) {
              tempAreaSekitar.push(sekitar);
            }
            for (let i = 0; i < tempAreaSekitar.length; i++) {
              tempAreaSekitar[i]["lat"] = tempAreaSekitar[i]["koordinat"]
                ?.replaceAll(" ", "")
                .split(",")[0];
              tempAreaSekitar[i]["lon"] = tempAreaSekitar[i]["koordinat"]
                ?.replaceAll(" ", "")
                .split(",")[1];
            }
            const idIklan = id || res.Output;

            console.log("~ data area sekitar : ", tempAreaSekitar);

            return updateAreaSekitar(
              userKey,
              idIklan,
              JSON.stringify(tempAreaSekitar)
            );
          } else {
            setAlertText(res.Output)
            setSuccesModal(true);
            // router.push("/member/properti");
          }
        }
        
      })
      .then((res) => {
        if (res.IsError) {
          throw res;
        } else {
          router.push("/member/properti");
        }
      })
      .catch((error) => {
        console.log("error : ", error);
        if (error?.ErrToUser?.includes("iklan yang sedang diproses")) {
          setAlertText(error.ErrToUser)
          setalert(true);
        }
      })
      .finally(() => setLoading(false));
  };
  // Outputnya ID di key : "Output"

  // Get All Cities when province has been selected
  useEffect(() => {
    if (provinsi) {
      console.log("provinsi selected : ", provinsi);
      getKota(provinsi)
        .then((res) => {
          setAllKota(res.Data);
        })
        .catch((error) => { });
    } else {
      setAllKota([]);
      setAllKecamatan([]);
      setAllKelurahan([]);
    }
  }, [provinsi]);

  // Get All Regions when city has been selected
  useEffect(() => {
    if (kota) {
      getKecamatan(kota)
        .then((res) => {
          setAllKecamatan(res.Data);
        })
        .catch((error) => { });
    } else {
      setAllKecamatan([]);
      setAllKelurahan([]);
    }
  }, [kota]);

  // Get All Villages when region has been selected
  useEffect(() => {
    if (kecamatan) {
      getKelurahan(kecamatan)
        .then((res) => {
          setAllKelurahan(res.Data);
        })
        .catch((error) => { });
    } else {
      setAllKelurahan([]);
    }
  }, [kecamatan]);

  // Get Postcode when village has been selected
  useEffect(() => {
    if (kelurahan && allKelurahan.length > 0) {
      const villageSelected = allKelurahan.filter((value) => {
        return value.id == kelurahan;
      });
      setKodepos(villageSelected[0]?.pos);
    } else {
      setKodepos("");
    }
  }, [kelurahan, allKelurahan]);

  // const SekitarComponent = ({
  //   areaSekitar,
  //   akses,
  //   removeLineSekitar,
  //   changeAreaSekitar,
  // }) => {
  //   return areaSekitar.map((value, index) => {
  //     return (
  //       <div key={index} className="col-md-12 d-flex mt-3">
  //         <input
  //           style={{
  //             width: "108.41px",
  //             borderTopRightRadius: "0px",
  //             borderBottomRightRadius: "0px",
  //             padding: "15px 10px",
  //           }}
  //           type="text"
  //           className="floating-label-field custom-plc"
  //           name="tipe"
  //           maxLength="16"
  //           minLength="16"
  //           placeholder="Nama Tempat"
  //           value={value.n}
  //           onChange={(e) => changeAreaSekitar("n", index, e.target.value)}
  //         />
  //         <select
  //           style={{
  //             whiteSpace: "nowrap",
  //             overflow: "hidden",
  //             textOverflow: "ellipsis",
  //             padding: "15px 10px",

  //             width: "87.5px",
  //             borderRadius: "0px",
  //             fontWeight: "400",
  //           }}
  //           id="input2"
  //           className="floating-label-select custom-select-small form-select"
  //           value={value.i_kat}
  //           onChange={(e) => changeAreaSekitar("i_kat", index, e.target.value)}
  //           autoComplete="off"
  //           required
  //         >
  //           {akses.map((value, index) => {
  //             return (
  //               <option key={index} value={value.id}>
  //                 {value.n}
  //               </option>
  //             );
  //           })}
  //         </select>
  //         <input
  //           style={{
  //             width: "108.33px",
  //             borderRadius: "0px",
  //             padding: "15px 8px",
  //           }}
  //           type="text"
  //           className="floating-label-field custom-plc"
  //           name="tipe"
  //           maxLength="16"
  //           minLength="16"
  //           placeholder="Koordinat Lokasi"
  //           value={value.koordinat}
  //           onChange={(e) =>
  //             changeAreaSekitar("koordinat", index, e.target.value)
  //           }
  //         />
  //         <input
  //           style={{
  //             width: "58.36px",
  //             borderRadius: "0px",
  //             padding: "15px 10px",
  //           }}
  //           type="text"
  //           className="floating-label-field custom-plc"
  //           name="tipe"
  //           maxLength="16"
  //           minLength="16"
  //           placeholder="Jarak"
  //           value={value.jrk}
  //           onChange={(e) => changeAreaSekitar("jrk", index, e.target.value)}
  //         />
  //         <div
  //           style={{
  //             fontWeight: "400",
  //             display: "flex",
  //             alignItems: "center",
  //             justifyContent: "center",
  //             fontFamily: "Helvetica",
  //             color: "#8D8C8C",
  //             border: "1px solid #aaaaaa",

  //             width: "8.73%",
  //             height: "48px",
  //             backgroundColor: "#EEEEEE",
  //             borderRadius: "0px",
  //           }}
  //         >
  //           m
  //         </div>
  //         <div
  //           style={{
  //             fontWeight: "400",
  //             display: "flex",
  //             alignItems: "center",
  //             justifyContent: "center",
  //             fontFamily: "Helvetica",

  //             border: "1px solid #aaaaaa",

  //             width: "8.73%",
  //             height: "48px",
  //             backgroundColor: "#ad050a",
  //             borderRadius: "0px",
  //             cursor: "pointer",
  //           }}
  //           onClick={() => removeLineSekitar(index)}
  //         >
  //           <svg
  //             xmlns="http://www.w3.org/2000/svg"
  //             width="24"
  //             height="24"
  //             viewBox="0 0 24 24"
  //             fill="none"
  //             stroke="#ffffff"
  //             stroke-width="2"
  //             stroke-linecap="round"
  //             stroke-linejoin="round"
  //           >
  //             <line x1="18" y1="6" x2="6" y2="18"></line>
  //             <line x1="6" y1="6" x2="18" y2="18"></line>
  //           </svg>
  //         </div>
  //         <div
  //           style={{
  //             fontWeight: "400",
  //             display: "flex",
  //             alignItems: "center",
  //             justifyContent: "center",
  //             fontFamily: "Helvetica",

  //             border: "1px solid #aaaaaa",

  //             width: "8.73%",
  //             height: "48px",
  //             backgroundColor: "#F9C014",
  //             borderRadius: "0px",
  //             borderBottomRightRadius: "8px",
  //             borderTopRightRadius: "8px",
  //             cursor: "pointer",
  //           }}
  //           // onClick={
  //           //   typeof window !== "undefined"
  //           //     ? () => window.open("https://www.google.com/maps", "_blank")
  //           //     : null
  //           // }
  //           onClick={() => modalMapsRef.current?.click()}
  //         >
  //           <img src="/icons/icons/icon_location.svg"></img>
  //         </div>
  //       </div>
  //     );
  //   });
  // };

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  const defaultSucces = {
    loop: true,
    autoplay: true,
    animationData:succes,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <Layout
      title="Hasil Properti - Dashboard Member | BTN Properti"
      isLoaderOpen={isLoading}
    >
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4 mb-5">
              <MenuMember active="properti" />
            </div>
            <div
              className="col-xl-9 col-lg-9 col-md-8 col-sm-8 px-2"
              style={{
                paddingRight: "16px",
                paddingLeft: "16px",
              }}
            >
              <div className="d-flex justify-content-start align-items-center mb-3">
                <h5 className="title_akun mb-3">
                  {id ? "Edit" : "Tambah"} Properti Agen
                </h5>
              </div>
              <div className="row">
                <div id="member_update_properti" className="col-md-7">
                  <form
                    className="form-validation"
                    noValidate
                    validated
                    id="form-properti"
                    style={{maxWidth:"480px"}}
                  >
                    <div id="form_properti_field">
                    <div
                      className="floating-label-wrap"
                      id="input_update_properti"
                    >
                      <input
                        type="text"
                        className="floating-label-field required number"
                        name="profil[no_ktp]"
                        maxLength="16"
                        minLength="16"
                        placeholder="Judul Properti"
                        value={data.jdl}
                        onChange={(e) => handleChange("jdl", e.target.value)}
                      />
                      <label className="floating-label">
                        Judul Properti{" "}
                        <span
                          style={{
                            color: "red",
                            fontFamily: "Helvetica",
                          }}
                        >
                          *
                        </span>
                      </label>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <input
                            type="text"
                            className="floating-label-field required number"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Tipe"
                            value={data.typ}
                            onChange={(e) =>
                              handleChange("typ", e.target.value)
                            }
                          />
                          <label className="floating-label">
                            Tipe{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        // style={{ position: "relative" }}
                        >
                          <NumberFormat
                            allowNegative={false}
                            prefix="      "
                            // prefix="Rp. "
                            thousandSeparator="."
                            decimalSeparator=","
                            className={`floating-label-field`}
                            placeholder="Harga"
                            value={data.hrg}
                            onChange={(e) =>
                              handleChange("hrg", e.target.value)
                            }
                          // disabled={!tipeKPR ? true : false}
                          // onBlur={(e) => handleBlur('harga', e.target.value)}
                          // onKeyPress={handlePressEnter}
                          />

                          <label
                            htmlFor="harga_property"
                            className="floating-label"
                          >
                            Harga{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {data.hrg && (
                            <p
                              style={{
                                height: "0px",
                                position: "relative",
                                bottom: "34px",
                                right: "0px",
                                marginLeft: "16px",
                                fontFamily: "Helvetica",
                                fontWeight: 700,
                                color: "#00193e",
                              }}
                            >
                              Rp
                            </p>
                          )}
                          {/* {dataPengajuan.harga && <p style={{
                              position: 'absolute', top: '18px', marginLeft: '15px', fontFamily: 'Helvetica', fontWeight:700, color: '#666666' }}>Rp</p>
                            }
                            {showErr.harga && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isian Tidak Boleh Kosong
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.jns}
                            onChange={(e) =>
                              handleChange("jns", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Jenis</option>
                            <option value="1">Rumah Baru</option>
                            <option value="2">Rumah Bekas</option>
                            <option value="3">Apartemen</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Jenis{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <input
                            type="text"
                            className="floating-label-field required number"
                            name="video"
                            maxLength="16"
                            minLength="16"
                            placeholder="Link Video"
                            value={data.vdo == 'null' ? '' : data.vdo }
                            onChange={(e) =>
                              handleChange("vdo", e.target.value)
                            }
                          />
                          <label className="floating-label">Video </label>
                        </div>
                      </div>
                    </div>
                    <div
                      className="floating-label-wrap"
                      id="input_update_properti"
                    >
                      <textarea
                        style={{ height: "137px" }}
                        className="floating-label-field floating-label-textarea"
                        rows="4"
                        name="profil[almt]"
                        placeholder="Deskripsi"
                        value={data.dsk}
                        onChange={(e) => handleChange("dsk", e.target.value)}
                      ></textarea>
                      <label className="floating-label">
                        Deskripsi{" "}
                        <span
                          style={{
                            color: "red",
                            fontFamily: "Helvetica",
                          }}
                        >
                          *
                        </span>
                      </label>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.st_ssd}
                            onChange={(e) =>
                              handleChange("st_ssd", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Status Subsidi</option>
                            <option value="0">Non Subsidi</option>
                            <option value="1">Subsidi</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Status Subsidi{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        // style={{ position: "relative" }}
                        >
                          <NumberFormat
                            allowNegative={false}
                            // prefix="Rp. "
                            thousandSeparator="."
                            decimalSeparator=","
                            className={`floating-label-field`}
                            placeholder="Luas Tanah"
                            value={data.ls_tnh}
                            onChange={(e) =>
                              handleChange("ls_tnh", e.target.value)
                            }
                          // disabled={!tipeKPR ? true : false}
                          // onBlur={(e) => handleBlur('harga', e.target.value)}
                          // onKeyPress={handlePressEnter}
                          />

                          <label
                            htmlFor="harga_property"
                            className="floating-label"
                          >
                            Luas Tanah{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {dataPengajuan.harga && <p style={{
                              position: 'absolute', top: '18px', marginLeft: '15px', fontFamily: 'Helvetica', fontWeight:700, color: '#666666' }}>Rp</p>
                            }
                            {showErr.harga && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isian Tidak Boleh Kosong
                              </div>
                            )} */}
                          <div
                            style={{
                              fontWeight: "400",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              fontFamily: "Helvetica",
                              color: "#8D8C8C",
                              border: "1px solid #aaaaaa",
                              right: "-1px",
                              top: "4.5px",
                              position: "absolute",
                              zIndex: "2",
                              width: "48px",
                              height: "48px",
                              backgroundColor: "#EEEEEE",
                              borderTopRightRadius: "8px",
                              borderBottomRightRadius: "8px",
                            }}
                          >
                            m<sup>2</sup>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        // style={{ position: "relative" }}
                        >
                          <NumberFormat
                            allowNegative={false}
                            // prefix="Rp. "
                            thousandSeparator="."
                            decimalSeparator=","
                            className={`floating-label-field`}
                            placeholder="Luas Bangunan"
                            value={data.ls_bgn}
                            onChange={(e) =>
                              handleChange("ls_bgn", e.target.value)
                            }
                          // disabled={!tipeKPR ? true : false}
                          // onBlur={(e) => handleBlur('harga', e.target.value)}
                          // onKeyPress={handlePressEnter}
                          />

                          <label
                            htmlFor="harga_property"
                            className="floating-label"
                          >
                            Luas Bangunan{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {dataPengajuan.harga && <p style={{
                              position: 'absolute', top: '18px', marginLeft: '15px', fontFamily: 'Helvetica', fontWeight:700, color: '#666666' }}>Rp</p>
                            }
                            {showErr.harga && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isian Tidak Boleh Kosong
                              </div>
                            )} */}
                          <div
                            style={{
                              fontWeight: "400",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              fontFamily: "Helvetica",
                              color: "#8D8C8C",
                              border: "1px solid #aaaaaa",
                              right: "-1px",
                              top: "4.5px",
                              position: "absolute",
                              zIndex: "2",
                              width: "48px",
                              height: "48px",
                              backgroundColor: "#EEEEEE",
                              borderTopRightRadius: "8px",
                              borderBottomRightRadius: "8px",
                            }}
                          >
                            m<sup>2</sup>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.thn_bgn}
                            onChange={(e) =>
                              handleChange("thn_bgn", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Tahun Bangunan</option>
                            {listTahun.map((value, index) => {
                              return <option value={value}>{value}</option>;
                            })}
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Tahun Bangunan{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.km_tdr}
                            onChange={(e) =>
                              handleChange("km_tdr", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Kamar Tidur</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Kamar Tidur
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.km_mnd}
                            onChange={(e) =>
                              handleChange("km_mnd", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Kamar Mandi</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Kamar Mandi
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.lt}
                            onChange={(e) => handleChange("lt", e.target.value)}
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Jumlah Lantai</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Jumlah Lantai
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.dy_lstr}
                            onChange={(e) =>
                              handleChange("dy_lstr", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Daya Listrik</option>
                            <option value="450">450VA</option>
                            <option value="900">900VA</option>
                            <option value="1300">1300VA</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Daya Listrik
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.srtfkt}
                            onChange={(e) =>
                              handleChange("srtfkt", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Sertifikat</option>
                            <option value="SHM">Sertifikat Hak Milik</option>
                            <option value="SHG">Sertifikat Hak Guna</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Sertifikat{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={data.st_prpt}
                            onChange={(e) =>
                              handleChange("st_prpt", e.target.value)
                            }
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Status Properti</option>
                            <option value="0">Not Available</option>
                            <option value="1">Indent</option>
                            <option value="2">Available</option>
                            <option value="5">Sold Out</option>
                            <option value="6">Cancelled</option>
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Status Properti{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <input
                            type="text"
                            className="floating-label-field required number"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Cluster"
                            value={data.clstr == 'null' ? '' : data.clstr}
                            onChange={(e) =>
                              handleChange("clstr", e.target.value)
                            }
                          />
                          <label className="floating-label">Cluster</label>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <input
                            type="text"
                            className="floating-label-field required number"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Blok"
                            value={data.blk == 'null' ? '' : data.blk}
                            onChange={(e) =>
                              handleChange("blk", e.target.value)
                            }
                          />
                          <label className="floating-label">Blok</label>
                        </div>
                      </div>
                    </div>
                    <div
                      className="floating-label-wrap"
                      id="input_update_properti"
                    >
                      <input
                        type="text"
                        className="floating-label-field required number"
                        name="tipe"
                        maxLength="16"
                        minLength="16"
                        placeholder="No. Rumah"
                        value={data.no  == 'null' ? '' : data.no}
                        onChange={(e) => handleChange("no", e.target.value)}
                      />
                      <label className="floating-label">No. Rumah</label>
                    </div>
                    <div
                      className="floating-label-wrap"
                      id="input_update_properti"
                    >
                      <textarea
                        style={{ height: "137px", marginBottom: "10px" }}
                        className="floating-label-field floating-label-textarea"
                        rows="4"
                        name="profil[almt]"
                        placeholder="Alamat"
                        value={data.almt}
                        onChange={(e) => handleChange("almt", e.target.value)}
                      ></textarea>
                      <label className="floating-label">
                        Alamat{" "}
                        <span
                          style={{
                            color: "red",
                            fontFamily: "Helvetica",
                          }}
                        >
                          *
                        </span>
                      </label>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={provinsi}
                            onChange={(e) => {
                              setProvinsi(e.target.value);
                              setKota("");
                              setKecamatan("");
                              setKelurahan("");
                            }}
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                            style={{
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              paddingRight:"24px"
                            }}
                          >
                            <option value="">Provinsi</option>
                            {allProvinsi.map((value, index) => {
                              return (
                                <option key={index} value={value.id}>
                                  {value.n}
                                </option>
                              );
                            })}
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Provinsi{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={kota}
                            onChange={(e) => {
                              setKota(e.target.value);
                              setKecamatan("");
                              setKelurahan("");
                            }}
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            style={{
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              paddingRight:"24px"
                            }}
                            required
                          >
                            <option value="">Kota/Kabupaten</option>
                            {allKota.map((value, index) => {
                              return (
                                <option key={index} value={value.id}>
                                  {value.n}
                                </option>
                              );
                            })}
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Kota/Kabupaten{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={kecamatan}
                            onChange={(e) => {
                              setKecamatan(e.target.value);
                              setKelurahan("");
                            }}
                            // onBlur={validateStatus}
                            autoComplete="off"
                            style={{
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              paddingRight:"24px"
                            }}
                            // onKeyPress={handlePressEnter}
                            required
                          >
                            <option value="">Kecamatan</option>
                            {allKecamatan.map((value, index) => {
                              return (
                                <option key={index} value={value.id}>
                                  {value.n}
                                </option>
                              );
                            })}
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Kecamatan{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="floating-label-wrap"
                          id="input_update_properti"
                        >
                          <select
                            id="input2"
                            className="floating-label-select custom-select-profile form-select"
                            value={kelurahan}
                            onChange={(e) => {
                              setKelurahan(e.target.value);
                            }}
                            // onBlur={validateStatus}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            style={{
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              paddingRight:"24px"
                            }}
                            required
                          >
                            <option value="">Kelurahan</option>
                            {allKelurahan.map((value, index) => {
                              return (
                                <option key={index} value={value.id}>
                                  {value.n}
                                </option>
                              );
                            })}
                          </select>
                          <label htmlFor="input2" className="floating-label">
                            Kelurahan{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                          {/* {statusInvalid && (
                              <div className="error-input-kpr">
                                <i
                                  class="bi bi-exclamation-circle-fill"
                                  style={{ marginRight: "5px" }}
                                ></i>
                                Isi status pernikahan
                              </div>
                            )} */}
                        </div>
                      </div>
                    </div>
                    <div
                      className="floating-label-wrap"
                      style={{ marginBottom: "12px" }}
                    >
                      <input
                        type="text"
                        className="floating-label-field required number"
                        name="tipe"
                        maxLength="16"
                        minLength="16"
                        placeholder="Kodepos"
                        value={kodepos}
                        // onChange={(e) =>
                        //   changeProfil("no_ktp", e.target.value)
                        // }
                        disabled
                      />
                      <label className="floating-label">
                        Kodepos{" "}
                        <span
                          style={{
                            color: "red",
                            fontFamily: "Helvetica",
                          }}
                        >
                          *
                        </span>
                      </label>
                    </div>
                    {/* ------------------------------------------- */}
                    <div
                      className="row"
                      style={{
                        paddingLeft: "16px",
                        paddingRight: "8px",
                      }}
                    >
                      <h5
                        style={{
                          fontSize: "14px",
                          marginBottom: "17px",
                          fontFamily: "FuturaBT",
                          fontWeight: 700,
                          color: "#0193E",
                          padding: 0,
                        }}
                      >
                        Kelengkapan Rumah
                      </h5>
                      {console.log(
                        "~ kelengkapan rumah in component : ",
                        kelengkapan
                      )}
                      {kelengkapan.map((value, index) => {
                        return (
                          <div key={index} className="col-md-4 col-4 mb-3 px-0">
                            <div className="form-check align-middle">
                              <input
                                className="form-check-input me-0"
                                type="checkbox"
                                value={value.id}
                                id={`kelengkapan-${index}`}
                                checked={value.checked}
                                onChange={(e) =>
                                  changeFasilitas("kelengkapan", e.target.value)
                                }
                                style={{}}
                              />
                              <label
                                className="form-check-label pt-1 ms-1"
                                htmlFor={`kelengkapan-${index}`}
                                style={{
                                  color: "#000000",
                                  fontWeight: "400",
                                  display: "block",
                                }}
                              >
                                {value.n}
                              </label>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    {/* ------------------------------------------- */}
                    <div className="row">
                      <h5
                        style={{
                          fontSize: "14px",
                          marginBottom: "17px",
                          marginTop: "12px",
                          fontFamily: "FuturaBT",
                          fontWeight: 700,
                          color: "#0193E",
                        }}
                      >
                        Akses
                      </h5>
                      {akses.map((value, index) => {
                        return (
                          <div key={index} className="col-md-4 col-4 mb-3">
                            <div className="form-check">
                              <input
                                className="form-check-input me-0"
                                type="checkbox"
                                value={value.id}
                                id={`akses-${index}`}
                                checked={value.checked}
                                onChange={(e) =>
                                  changeFasilitas("akses", e.target.value)
                                }
                              />
                              <label
                                className="form-check-label pt-1 ms-1"
                                htmlFor={`akses-${index}`}
                                style={{
                                  color: "#000000",
                                  fontWeight: "400",
                                  display: "block",
                                }}
                              >
                                {value.n}
                              </label>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    {/* ------------------------------------------- */}
                    <div className="row">
                      <h5
                        style={{
                          fontSize: "14px",
                          marginBottom: "17px",
                          marginTop: "12px",
                          fontFamily: "FuturaBT",
                          fontWeight: 700,
                          color: "#0193E",
                        }}
                      >
                        Fasilitas
                      </h5>
                      {fasilitas.map((value, index) => {
                        return (
                          <div key={index} className="col-md-4 col-4 mb-3">
                            <div className="form-check">
                              <input
                                className="form-check-input me-0"
                                type="checkbox"
                                value={value.id}
                                id={`fasilitas-${index}`}
                                checked={value.checked}
                                onChange={(e) =>
                                  changeFasilitas("fasilitas", e.target.value)
                                }
                              />
                              <label
                                className="form-check-label ms-1 pt-1 "
                                htmlFor={`fasilitas-${index}`}
                                style={{
                                  color: "#000000",
                                  fontWeight: "400",
                                  display: "block",
                                }}
                              >
                                {value.n}
                              </label>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    <div className="row">
                      <h5
                        style={{
                          fontSize: "14px",
                          marginBottom: "17px",
                          marginTop: "12px",
                          fontFamily: "FuturaBT",
                          fontWeight: 700,
                          color: "#0193E",
                        }}
                      >
                        Lokasi Map
                      </h5>
                      <div className="col-md-12">
                        <div
                          className="row"
                          style={{
                            width: "100%",
                            height: "181.13px",
                            marginBottom: "40.87px",
                            borderRadius: "8px",
                          }}
                        >
                          <GoogleMapsDynamic
                            dinamicPosition={true}
                            position={
                              data.lat && data.lon
                                ? {
                                  lat: data.lat * 1,
                                  lng: data.lon * 1,
                                }
                                : {
                                  lat: current?.lat,
                                  lng: current?.lon,
                                }
                            }
                            handleChange={handleChange}
                          />
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div className="floating-label-wrap">
                          <input
                            type="text"
                            className="floating-label-field required number"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Latitude"
                            value={data.lat}
                            onChange={(e) =>
                              handleChange("lat", e.target.value)
                            }
                          />
                          <label className="floating-label">
                            Latitude{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div className="floating-label-wrap">
                          <input
                            type="text"
                            className="floating-label-field required number"
                            name="tipe"
                            maxLength="16"
                            minLength="16"
                            placeholder="Longitude"
                            value={data.lon}
                            onChange={(e) =>
                              handleChange("lon", e.target.value)
                            }
                          />
                          <label className="floating-label">
                            Longitude{" "}
                            <span
                              style={{
                                color: "red",
                                fontFamily: "Helvetica",
                              }}
                            >
                              *
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                    </div>
                    <div   id="area_update_properti" style={{ position: "absolute" }} className="row">
                      <h5
                        style={{
                          fontSize: "14px",

                          fontFamily: "FuturaBT",
                          fontWeight: 700,
                          color: "#0193E",
                        }}
                      >
                        Area Sekitar
                      </h5>
                      <SekitarComponent
                        deleteSpace={deleteSpace}
                        areaSekitar={areaSekitar}
                        akses={akses}
                        removeLineSekitar={removeLineSekitar}
                        changeAreaSekitar={changeAreaSekitar}
                        modalMapsRef={modalMapsRef}
                        setIndexMaps={setIndexMaps}
                      />
                      <div className="col-md-12 d-flex mt-3 area-sekitar-field">
                        <input
                          type="text"
                          className="floating-label-field custom-plc res-font-nm-tmpt field1"
                          name="tipe"
                          maxLength="16"
                          minLength="16"
                          placeholder="Nama Tempat"
                          value={sekitar.n}
                          onChange={(e) => changeSekitar("n", e.target.value)}
                        />
                        <select
                          style={{
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            padding: "15px 10px",
                        
                            borderRadius: "0px",
                            fontWeight: "400",
                          }}
                          id="input2"
                          className="floating-label-select custom-select-small form-select res-font-loc field2"
                          value={sekitar.i_kat}
                          onChange={(e) =>
                            changeSekitar("i_kat", e.target.value)
                          }
                          // onBlur={validateStatus}
                          autoComplete="off"
                          // onKeyPress={handlePressEnter}
                          required
                        >
                          {akses.map((value, index) => {
                            return (
                              <option key={index} value={value.id}>
                                {value.n}
                              </option>
                            );
                          })}
                        </select>
                        <input
                          style={{ borderRadius: "0px" }}
                          type="text"
                          className="floating-label-field custom-plc res-font-nm-koord field3"
                          name="tipe"
                          maxLength="16"
                          minLength="16"
                          placeholder="Koordinat Lokasi"
                          value={sekitar.koordinat}
                          onChange={(e) =>
                            changeSekitar("koordinat", e.target.value)
                          }
                        />
                        <input
                          type="text"
                          className="floating-label-field custom-plc res-font-jarak field4"
                          name="tipe"
                          maxLength="16"
                          minLength="16"
                          placeholder="Jarak"
                          value={sekitar.jrk}
                          onChange={(e) => changeSekitar("jrk", e.target.value)}
                        />
                        <div
                          className="field5"
                          style={{
                            fontWeight: "400",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            fontFamily: "Helvetica",
                            color: "#8D8C8C",
                            border: "1px solid #aaaaaa",
                            height: "48px",
                           
                            backgroundColor: "#EEEEEE",
                            borderRadius: "0px",
                          }}
                        >
                          m
                        </div>
                        <div
                          className="field5"
                          style={{
                            fontWeight: "400",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            fontFamily: "Helvetica",
                            border: "1px solid #aaaaaa",
                            height: "48px",
                           
                            backgroundColor: "#15CF74",
                            borderRadius: "0px",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            addLineSekitar();
                            setAddMargin(addMargin + 60);
                          }}
                        >
                          <img src="/icons/icons/icon_plus.svg"></img>
                        </div>
                        <div
                          className="field5"
                          style={{
                            fontWeight: "400",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            fontFamily: "Helvetica",
                            border: "1px solid #aaaaaa",
                            height: "48px",
                         
                            backgroundColor: "#F9C014",
                            borderRadius: "0px",
                            borderBottomRightRadius: "8px",
                            borderTopRightRadius: "8px",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            setIndexMaps("");
                            setTimeout(() => {
                              modalMapsRef.current?.click();
                            }, 300);
                          }}
                        >
                          {console.log("ini margin", addMargin)}

                          <img src="/icons/icons/icon_location.svg"></img>
                        </div>
                      </div>
                    </div>
                    <div
                    id="foto_update_properti"
                      style={{ marginTop: `${addMargin}px` }}
                      className="row"
                    >
                      <h5
                        style={{
                          fontSize: "14px",
                          marginBottom: "4px",
                          marginTop: "18px",
                          fontFamily: "FuturaBT",
                          fontWeight: 700,
                          color: "#0193E",
                        }}
                      >
                        Foto
                        <span
                          style={{
                            color: "red",
                            fontFamily: "Helvetica",
                          }}
                        >
                          *
                        </span>
                      </h5>
                      <div className="col-md-12 d-flex">
                        <p
                          style={{
                            fontSize: "10px",
                            fontWeight: "400",
                            fontFamily: "Helvetica",
                            color: "#000000",
                          }}
                        >
                          Dengan mengunggah foto, Anda menyetujui bahwa Anda
                          memiliki hak atas gambar tersebut atau bahwa Anda
                          memiliki izin atau lisensi dan setuju untuk memenuhi
                          Syarat & Ketentuan BTN Properti.
                        </p>
                      </div>
                      <div className="d-flex justify-content-end mb-4">
                        <input
                          type={"file"}
                          ref={inputRef}
                          onChange={(e) => addImage(e,3)}
                          hidden
                        />
                        <button
                          type="button"
                          className="d-flex align-items-center justify-content-center btn btn-outline-main"
                          // data-bs-toggle="modal"
                          // data-bs-target="#modalKonsultasi"
                          style={{
                            width: "156px",
                            height: "46px",
                            fontFamily: "Helvetica",
                            fontWeight: 700,
                            pointerEvents: images.length === 5 && "none",
                          }}
                          onClick={() => inputRef.current.click()}
                        >
                          <img
                            className="me-2"
                            src="/icons/icons/icon_plus_blue.svg"
                          ></img>{" "}
                            Tambah Foto
                        </button>
                        
                      </div>
                      <div className="row g-0 justify-content-end">
                      <div className="col-12">
                          {texterr.length !=0 &&
                           <div
                           className="error-input"
                           style={{ marginBottom: "10px", marginTop: "-10px",justifyContent:"end" }}
                         >
                           <i
                             class="bi bi-exclamation-circle-fill"
                             style={{ marginRight: "5px" }}
                           ></i>
                           {texterr}
                         </div>
                          }
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-3 col-3 d-flex">
                          <embed
                            style={{ width: "88px", borderRadius: "8px",height:"88px" }}
                            src={
                              images[0]?.file ||
                              generateImageUrl(images[0]?.split("|")[1])
                            }
                          />
                        </div>
                        <div className="col-md-3 col-3 d-flex">
                          <embed
                            style={{ width: "88px", borderRadius: "8px",height:"88px" }}
                            src={
                              images[1]?.file ||
                              generateImageUrl(images[1]?.split("|")[1])
                            }
                          />
                        </div>
                        <div className="col-md-3 col-3 d-flex">
                          <embed
                            style={{ width: "88px", borderRadius: "8px",height:"88px" }}
                            src={
                              images[2]?.file ||
                              generateImageUrl(images[2]?.split("|")[1])
                            }
                          />
                        </div>
                        <div className="col-md-3 col-3 d-flex">
                          <embed
                            style={{ width: "88px", borderRadius: "8px",height:"88px" }}
                            src={
                              images[3]?.file ||
                              generateImageUrl(images[3]?.split("|")[1])
                            }
                          />
                        </div>                 
                      </div>
                      <div
                        style={{ marginTop: "80px" }}
                        className="d-flex justify-content-center"
                      >
                        <button
                          type="button"
                          className="d-flex align-items-center justify-content-center btn btn-main"
                          // data-bs-toggle="modal"
                          // data-bs-target="#modalKonsultasi"
                          style={{
                            width: "302px",
                            height: "46px",
                            fontFamily: "Helvetica",
                            fontWeight: 700,
                          }}
                          onClick={submitIklan}
                        >
                          Simpan
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        data-bs-toggle="modal"
        data-bs-target="#modal-maps"
        ref={modalMapsRef}
      ></div>

      {/* Modal Google Maps */}
      <div className="modal fade" id="modal-maps" tabIndex="-1" role="dialog">
        <div className="modal-dialog modal-dialog-centered" style={{width:"97.5%"}}>
          <div
            className="modal-content"
            style={{ backgroundColor: "transparent", border: 0 }}
          >
            <div className="d-flex">
              <div
                style={{
                  width: "100%",
                  height: "250px",
                  borderRadius: "10px",
                }}
              >
                <GoogleMapsPopUp
                  dinamicPosition={true}
                  index={indexMaps}
                  changeAreaSekitar={changeAreaSekitar}
                  changeSekitar={changeSekitar}
                  position={
                    indexMaps === ""
                      ? sekitar.koordinat
                        ? {
                          lat: +sekitar.koordinat.split(",")[0],
                          lng: +sekitar.koordinat.split(",")[1],
                        }
                        : {
                          lat: current?.lat,
                          lng: current?.lon,
                        }
                      : areaSekitar[indexMaps]["koordinat"]
                        ? {
                          lat: +areaSekitar[indexMaps]["koordinat"].split(
                            ","
                          )[0],
                          lng: +areaSekitar[indexMaps]["koordinat"].split(
                            ","
                          )[1],
                        }
                        : {
                          lat: current?.lat,
                          lng: current?.lon,
                        }
                  }
                />
              </div>
              <div className="ms-md-2 btn-close-mdl" style={{
                background: "rgba(87, 87, 87, 0.8)",
                display: "flex",
                transform: "translateY(-30px)",
                height: "fitContent",
                borderRadius: "50%",
                padding: "3px",
                height:"fit-content"
              }}>
                <button
                  type="button"
                  className="btn-close btn-close-white "
                  data-bs-dismiss="modal"
                  aria-label="Close"
                  style={{
                    opacity: 1,
                    fontSize: "17px",
                    width: "13px",
                    height: "15px",
                    filter: "invert(1) grayscale(90%) brightness(70%)",
                  }}
                ></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      

      {/* //////----modal confirmation----/// */}
      
      <div className={`modal fade d-${alert? "block show" : "none"}`} id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
      style={{zIndex:"9999",background:"#00193e69"}}
      >
        <div className="modal-dialog modal-dialog-centered px-3 mx-auto">
          <div className="modal-content" style={{padding:"20px 60px",borderRadius:"8px"}}>
            <div className="modal-body">
              <div className="row justify-content-end">
                <button type="button" className="btn-close" aria-label="Close"
              onClick={()=>{setalert(false)}}
              style={{marginRight:"-30px"}}
                ></button>
              </div>
              <h2 className="text-center">Maaf</h2>
              <Lottie
                      options={defaultOptions}
                      height={"80%"}
                      width={"100%"}
                      isStopped={false}
                      isPaused={false}
                      max-width={350}
                    />
              <div className="row g-0">
              <div className="row g-0">
              <p style={{
                fontFamily:"Helvetica",
                fontSize:"14px",
                textAlign:"center"
              }}>
              {alertText}
              </p>
              </div>
                <button type="button" className="btn btn-primary" data-bs-dismiss="modal"
                style={{
                  background:"#0061a7",
                  borderRadius:"200px",
                  lineHeight: "31px",
                  width: "100%",
                  height: "40px",
                }}
              onClick={()=>{setalert(false); router.push("/member/properti");}}
              >Kembali</button>
              
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* //////----modal confirmation succes----/// */}
      
      <div className={`modal fade d-${succesModal? "block show" : "none"}`} id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
      style={{zIndex:"9999",background:"#00193e69"}}
      >
        <div className="modal-dialog modal-dialog-centered px-3 mx-auto">
          <div className="modal-content" style={{padding:"20px 60px",borderRadius:"8px"}}>
            <div className="modal-body">
              <div className="row justify-content-end">
                <button type="button" className="btn-close" aria-label="Close"
              onClick={()=>{
                setSuccesModal(false)
                router.push("/member/properti")
              }}
              style={{marginRight:"-30px"}}
                ></button>
              </div>
              <h2 className="text-center">Sukses</h2>
              <Lottie
                      options={defaultSucces}
                      height={"80%"}
                      width={"100%"}
                      isStopped={false}
                      isPaused={false}
                      max-width={350}
                    />
              <div className="row g-0">
              <div className="row g-0">
              {/* <p style={{
                fontFamily:"Helvetica",
                fontSize:"14px"
              }}>
              {alertText}
              </p> */}
              </div>
                <button type="button" className="btn btn-primary" data-bs-dismiss="modal"
                style={{
                  background:"#0061a7",
                  borderRadius:"200px",
                  lineHeight: "31px",
                  width: "100%",
                  height: "40px",
                }}
              onClick={()=>{
                setSuccesModal(false)
                router.push("/"); 
              }}
              >Kembali Ke Beranda</button>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export const SekitarComponent = ({
  areaSekitar,
  akses,
  removeLineSekitar,
  changeAreaSekitar,
  modalMapsRef,
  setIndexMaps,
  deleteSpace,
}) => {
  return areaSekitar.map((value, index) => {
    return (
      <div key={index} className="col-md-12 d-flex mt-3 area-sekitar-field">
        <input
          style={{
           
            borderTopRightRadius: "0px",
            borderBottomRightRadius: "0px",
            padding: "15px 10px",
          }}
          type="text"
          className="floating-label-field custom-plc field1"
          name="tipe"
          maxLength="16"
          minLength="16"
          placeholder="Nama Tempat"
          value={value.n}
          onChange={(e) => changeAreaSekitar("n", index, e.target.value)}
        />
        <select
          style={{
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            padding: "15px 10px",

       
            borderRadius: "0px",
            fontWeight: "400",
          }}
          id="input2"
          className="floating-label-select custom-select-small form-select field2"
          value={value.i_kat}
          onChange={(e) => changeAreaSekitar("i_kat", index, e.target.value)}
          autoComplete="off"
          required
        >
          {akses.map((value, index) => {
            return (
              <option key={index} value={value.id}>
                {value.n}
              </option>
            );
          })}
        </select>
        <input
          style={{
           
            borderRadius: "0px",
            padding: "15px 8px",
            borderRadius: "0px",
          }}
          type="text"
          className="floating-label-field custom-plc field3"
          name="tipe"
          maxLength="16"
          minLength="16"
          placeholder="Koordinat Lokasi"
          value={value.koordinat}
          onChange={(e) =>
            changeAreaSekitar("koordinat", index, e.target.value)
          }
        />
        <input
          style={{
      
            borderRadius: "0px",
            padding: "15px 10px",
          }}
          type="text"
          className="floating-label-field custom-plc field4"
          name="tipe"
          maxLength="16"
          minLength="16"
          placeholder="Jarak"
          value={value.jrk}
          onChange={(e) => changeAreaSekitar("jrk", index, e.target.value)}
        />
        <div
        className="field5"
          style={{
            fontWeight: "400",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontFamily: "Helvetica",
            color: "#8D8C8C",
            border: "1px solid #aaaaaa",

          
            height: "48px",
            backgroundColor: "#EEEEEE",
            borderRadius: "0px",
          }}
        >
          m
        </div>
        <div
        className="field5"
          style={{
            fontWeight: "400",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontFamily: "Helvetica",

            border: "1px solid #aaaaaa",

        
            height: "48px",
            backgroundColor: "#ad050a",
            borderRadius: "0px",
            cursor: "pointer",
          }}
          onClick={() => {
            setIndexMaps("");
            removeLineSekitar(index);
            deleteSpace();
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="#ffffff"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          >
            <line x1="18" y1="6" x2="6" y2="18"></line>
            <line x1="6" y1="6" x2="18" y2="18"></line>
          </svg>
        </div>
        <div
        className="field5"
          style={{
            fontWeight: "400",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontFamily: "Helvetica",

            border: "1px solid #aaaaaa",

           
            height: "48px",
            backgroundColor: "#F9C014",
            borderRadius: "0px",
            borderBottomRightRadius: "8px",
            borderTopRightRadius: "8px",
            cursor: "pointer",
          }}
          onClick={() => {
            setIndexMaps(index);
            setTimeout(() => {
              modalMapsRef.current?.click();
            }, 300);
          }}
        >
          <img src="/icons/icons/icon_location.svg"></img>
        </div>
      </div>
    );
  });
};

const generateImageUrl = (url) => {
  if (!url) return "/images/icons/thumb.png";

  let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  fullUrl = fullUrl.replaceAll(" ", "%20");

  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/icons/thumb.png";
  }
};


export default (withAuth(Properti))
