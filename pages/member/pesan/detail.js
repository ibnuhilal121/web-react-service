import { useRouter } from "next/router";
import Link from "next/link";
import MenuMember from "../../../components/MenuMember";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import { useEffect, useState } from "react";
import { useAppContext } from "../../../context";
import axios from "axios";
import withAuth from "../../../helpers/withAuth";
import parse from "html-react-parser";
import { defaultHeaders } from "../../../utils/defaultHeaders";
import { deleteNotification } from "../../../services/member";

const index = () => {
  const { userKey, userProfile } = useAppContext();

  const [datas, setDatas] = useState([]);
  const [psn, setPsn] = useState("");
  const [isUserReply, setUserReply] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmit, setSubmit] = useState(false);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  const { id } = router.query;
  const [title, setTitle] = useState(null);

  useEffect(() => {
    if(router.isReady) {
      deleteNotification(parseInt(id), userProfile?.id)
    }
    let tempData = sessionStorage.getItem("detailPesan");
    if (tempData) {
      tempData = JSON.parse(tempData);
    } else {
      router.push("/member/pesan?rld=pesan");
    }

    if (tempData["kategori"] === "pesan") {
      setTitle(tempData);
    } else {
      router.push("/member/pesan?rld=pesan");
    }
  }, []);

  const sendMessage = () => {
    if (userKey) {
      setIsLoading(true)
      let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/reply`;
      let body = {
        i_cht: parseInt(id),
        psn: psn,
      };
      let header = {
        headers: {
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
      };

      axios
        .post(endpoint, new URLSearchParams(body), header)
        .then((res) => {
          if (res.data.isError) {
            console.log(res.data.ErrorToDev);
            // return console.log(res.data.ErrToUser);
          }
        })
        .catch((err) => { })
        .finally(() => {
          getConversation();
          setIsLoading(false)
        });
    }
  }

  // useEffect(() => {
  //   console.log({userKey, isUserReply});
  //   if (userKey && isUserReply) {

  //     let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/reply`;
  //     let body = {
  //       i_cht: parseInt(id),
  //       psn: psn,
  //     };
  //     let header = {
  //       headers: {
  //         AccessKey_Member: userKey,
  //         ...defaultHeaders
  //       },
  //     };

  //     axios
  //       .post(endpoint, new URLSearchParams(body), header)
  //       .then((res) => {
  //         if (res.data.isError) {
  //           console.log(res.data.ErrorToDev);
  //           // return console.log(res.data.ErrToUser);
  //         }
  //       })
  //       .catch((err) => { })
  //       .finally(() => {
  //         getConversation();
  //       });
  //   }
  // }, [psn]);

  useEffect(() => {
    if (userKey) {
      getConversation();
    }
  }, [isSubmit]);

  const getConversation = async () => {
    let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/show/conversation`;
    let body = {
      i_cht: parseInt(id),
      // Limit: 10, // optional
      // Page: 1, // optional
    };
    let header = {
      headers: {
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
    };

    axios
      .post(endpoint, new URLSearchParams(body), header)
      .then((res) => {
        if (res.data.IsError) {
          console.log(res.data.ErrToDev);
          // return console.log(res.data.ErrToUser);
        } else {
          setDatas(res.data.Data.reverse());
        }
      })
      .catch((err) => {
        // console.log(err.message);
      })
      .finally(() => setUserReply(false));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    sendMessage()
    setPsn(e.target[0].value);
    setUserReply(true);
    setSubmit((prev) => !prev);
    e.target[0].value = "";
  };

  function createMarkup(content) {
    return { __html: content };
  }

  return (
    <Layout title="Pesan - Dashboard Member | BTN Properti" isLoaderOpen={isLoading}>
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="pesan" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 detail_pesan p-1">
              <h4 className="title px-xl-2 px-4">{title?.judul}</h4>
              <p
              className="px-xl-2 px-4"
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: 400,
                  color: "#000000",
                }}
              >
                {title?.isi ? parse(title.isi) : ""}
              </p>
              <div
                className="row p-xl-2 p-4 d-flex flex-column"
                style={{ marginBottom: "24px" }}
              >
                {datas?.map((data, index) => (
                  <div
                    key={index}
                    className={
                      "card card_jawaban col-12" +
                      `${data.n_pgrm === userProfile.n ? " align-self-end" : " align-self-start"}`
                    }
                    style={{
                      maxWidth: "860px",
                      width:"fit-content",
                      minHeight: "216px",
                      marginTop: "10px",
                      float: `${data.n_pgrm === userProfile.n ? "right" : "left"}`,
                    }}
                  >
                    {/* <div
                      className="jawaban"
                      style={{ fontFamily: "Helvetica", fontWeight: 700, color: "#666" }}
                    >
                      Jawaban:
                    </div> */}
                    <div className="card-body">
                      <div
                        className={`d-flex align-items-center user justify-content-${data.n_pgrm !== userProfile.n ? "start" : "end"
                          }`}
                      >
                        {data.n_pgrm !== userProfile.n ? (
                          <img
                            src={
                              data.n_pgrm === "System"
                                ? "/images/user/avatar.svg"
                                : userProfile?.gmbr
                                  ? userProfile?.gmbr
                                  : "/images/user/member-profile.jpg"
                            }
                            className="img-fluid"
                            alt="user"
                            style={{ width: 48, height: 48, borderRadius: 30 }}
                          />
                        ) : (
                          ""
                        )}

                        <div className={`${data.n_pgrm !== userProfile.n ? "ms-3" : "me-3"} `}>
                          <h5 className="nama">{data.n_pgrm}</h5>
                          <p
                            className={`date text-${data.n_pgrm !== userProfile.n ? "start" : "end"
                              }`}
                          >
                            {data.tgl
                              .split("T")
                              .join(", ")
                              .slice(0, data.tgl.length - 2)}
                          </p>
                        </div>
                        {data.n_pgrm === userProfile.n ? (
                          <img
                            src={
                              data.n_pgrm === "System"
                                ? "/images/user/avatar.svg"
                                : userProfile?.gmbr
                                  ? userProfile?.gmbr
                                  : "/images/user/member-profile.jpg"
                            }
                            className="img-fluid"
                            alt="user"
                            style={{ width: 48, height: 48, borderRadius: 30 }}
                          />
                        ) : (
                          ""
                        )}
                      </div>
                      <div
                        dangerouslySetInnerHTML={createMarkup(data.psn)}
                        className={`pt-3 text-start`}
                        style={{ fontFamily: "Helvetica", fontWeight: 400, color: "#000000" }}
                      />
                    </div>
                  </div>
                ))}
              </div>
              <div
                className="card card_jawaban p-0"
                style={{

                  height: "172px",
                }}
              >
                <div
                  id="card-jawab-pesan"
                  className="card-body"
                  style={{
                    padding: "25px",
                  }}
                >
                  <form id="replyPsn" onSubmit={handleSubmit}>
                    <textarea
                      className="form-control"
                      placeholder="Balasan Kamu..."
                      style={{
                        width: "100%",
                        height: "80px",
                        borderRadius: "8px",
                        padding: "16px",
                        background: "transparent",
                        fontFamily: "Helvetica",
                      }}
                      onChange={(e)=>{setPsn(e.target.value)}}
                    ></textarea>
                  </form>
                  <div className="text-end" 
                      style={{marginTop: 10}}>
                    <button
                      form="replyPsn"
                      type="submit"
                      className="btn btn-main"
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        width: "123px",
                        height: "48px",
                      }}
                      disabled={psn.length === 0 ? true : false}
                    >
                      Balas Pesan
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withAuth(index);
