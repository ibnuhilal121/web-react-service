import React, {useEffect, useState, useRef} from 'react';
import axios from 'axios';
import ItemNotification from '../../../components/data/ItemNotification';
import ItemPesan from '../../../components/data/ItemPesan';
import Layout from '../../../components/Layout';
import MenuMember from '../../../components/MenuMember';
import Breadcrumb from '../../../components/section/Breadcrumb';
import Pagination from '../../../components/data/Pagination';

import data_notification from '../../../sample_data/data_notification';
import data_pesan from '../../../sample_data/data_pesan';
import DataEmpty from '../../../components/data/DataEmpty';
import { useAppContext } from '../../../context';
import withAuth from '../../../helpers/withAuth';
import SearchIcon from '../../../components/element/icons/SearchIcon';
import { getPesan, getNotification } from '../../../services/member';
import PaginationNew from '../../../components/data/PaginationNew';
import { useRouter } from 'next/router';
import { deleteAllNotif } from '../../../services/member';

const limit = 10;

const Pesan = () => {
    const router = useRouter();
    const { rld } = router.query;
    const { userKey, userProfile } = useAppContext();
    const [dataPesans, setDataPesans] = useState([]);
    const [dataNotifications, setDataNotifications] = useState([]);
    const [isForceRender, setForceRender] = useState(false);
    const [isLoading, setLoading] = useState(true);
    const [pagePesan, setPagePesan] = useState(1);
    const [keywordPesan, setKeywordPesan] = useState('');
    const [pageNotif, setPageNotif] = useState(1);
    const [pagingNotif, setPagingNotif] = useState(null);
    const [pagingPesan, setPagingPesan] = useState(null);
    const [softLoad, setSoftLoad] = useState(false);
    const reload = rld;

    useEffect(() => {
        if (userKey) {
            getAllData();
        };
    }, [pageNotif, pagePesan])

    const getAllData = () => {
        setSoftLoad(true);

        const body = {
            kywd: keywordPesan,
            Limit: limit,
            Page: pagePesan,
        };
        const arrPromises = [
            getPesan(userKey, body), 
            getNotification(userProfile.id, pageNotif, limit, userKey)
        ];

        Promise.allSettled(arrPromises)
        .then(res => {
            const [resPesan, resNotifikasi] = res;
            if (resPesan.status === "fulfilled") {
                if (!resPesan.value.IsError) {
                    setDataPesans(resPesan.value.Data);
                    setPagingPesan(resPesan.value.Paging);
                }
            };
            if (resNotifikasi.status === "fulfilled") {
                if (resNotifikasi.value.status) {
                    setDataNotifications(resNotifikasi.value.data);
                    setPagingNotif(resNotifikasi.value.pagging);
                };
            }
        })
        .catch(error => {})
        .finally(() => {
            setSoftLoad(false);
            setLoading(false);
        })
    };

    const semuaDibaca = () => {
        // setLoading(true);
        deleteAllNotif(userProfile.id)
        .then(res => {})
        .catch(err => {})
        .finally(() => getAllData())
    };

    const forceRender = () => setForceRender(prev => !prev)

    const NotFound = (kategori) => {
        return (
          <div
            style={{
              padding: '30px 0',
              textAlign: "center",
            }}
          >
            <SearchIcon />
            <div
              style={{
                fontFamily: "Helvetica",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "16px",
                lineHeight: "150%",
                marginTop: 40,
              }}
            >
              {`Tidak ada ${kategori}`}
            </div>
          </div>
        );
    };

    if (isLoading) {
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
              minWidth: "100%"
            }}
          >
            <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
          </div>
        );
    };

    return (
        <Layout title="Pesan - Dashboard Member | BTN Properti" isLoaderOpen={softLoad}>
            <Breadcrumb active="Akun" />
            <div className="dashboard-content mb-5">
                <div className="container">
                    <div className="row">
                        <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
                            <MenuMember active="pesan" />
                        </div>
                        <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                            <h5 className="title_akun" style={{ fontFamily: 'FuturaBT', fontWeight: 700 }}>Pesan</h5>

                            <div className="tab-menu d-md-flex justify-content-between align-items-start">
                                <ul className="nav nav-tabs">
                                    <li className="nav-item">
                                        <a href="#" className={`nav-link ${reload !== 'pesan' ? 'active' : null}`} data-bs-toggle="tab" data-bs-target="#tab-notif"
                                            role="tab"><span>Notifikasi</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="#" className={`nav-link ${reload === 'pesan' ? 'active' : null}`} data-bs-toggle="tab" data-bs-target="#tab-pesan"
                                            role="tab"><span>Pesan</span></a>
                                    </li>
                                </ul>
                                <div style={{ display: 'flex' }}>
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.0002 3.75C5.8335 3.75 2.27516 6.34167 0.833496 10C2.27516 13.6583 5.8335 16.25 10.0002 16.25C14.1668 16.25 17.7252 13.6583 19.1668 10C17.7252 6.34167 14.1668 3.75 10.0002 3.75ZM10.0002 14.1667C7.70016 14.1667 5.8335 12.3 5.8335 10C5.8335 7.7 7.70016 5.83333 10.0002 5.83333C12.3002 5.83333 14.1668 7.7 14.1668 10C14.1668 12.3 12.3002 14.1667 10.0002 14.1667ZM10.0002 7.5C8.61683 7.5 7.50016 8.61667 7.50016 10C7.50016 11.3833 8.61683 12.5 10.0002 12.5C11.3835 12.5 12.5002 11.3833 12.5002 10C12.5002 8.61667 11.3835 7.5 10.0002 7.5Z" fill="#0061A7" />
                                    </svg>
                                    <span className="tandai_dilihat" style={{cursor: 'pointer'}} onClick={semuaDibaca} >
                                        Tandai Semua Dilihat
                                    </span>
                                </div>
                            </div>
                            <div className="tab-content revamp-ajukan-kprtipe">
                                <div className={`tab-pane fade ${reload !== 'pesan' ? 'show active' : null} revamp-ajukan-kprtipe`} id="tab-notif" role="tabpanel">
                                    {dataNotifications?.length > 0 ?
                                    (
                                        dataNotifications?.map((data, index) =>
                                            // <div key={index} className="">
                                                <ItemNotification key={index} data={data} />
                                            // </div>
                                        )
                                    ) : NotFound('notifikasi')}
                                    <div className="row">
                                        <div className="col-12 mt-4">
                                            {/* <Pagination /> */}
                                            <PaginationNew 
                                                length={pagingNotif ? pagingNotif.jumlahHalTotal : 1}
                                                current={Number(pagingNotif ? pagingNotif.halKe : 1)}
                                                onChangePage={(e) => {
                                                  setPageNotif(e);
                                                  window.scrollTo(0, 0);
                                                }}
                                            />
                                        </div>
                                    </div>
                                    {/* jika tidak ada data maka ambil tampilan berikut */}
                                    {/* <DataEmpty title="Kamu belum memiliki Notifikasi"/> */}
                                </div>
                                <div className={`tab-pane fade ${reload === 'pesan' ? 'show active' : null} revamp-ajukan-kprtipe`} id="tab-pesan" role="tabpanel">
                                    {dataPesans?.length > 0 ?
                                    (
                                        dataPesans?.map((data, index) =>
                                            // <div key={index} className="">
                                                <ItemPesan 
                                                    key={index} 
                                                    data={data} 
                                                    forceRender={forceRender}
                                                    setLoading={setLoading}
                                                />
                                            // </div>
                                        )
                                    ) : NotFound('pesan')}
                                    <div className="row">
                                        <div className="col-12 mt-4">
                                            {/* <Pagination /> */}
                                            <PaginationNew 
                                                length={pagingPesan ? pagingPesan.JmlHalTotal : 1}
                                                current={Number(pagingPesan ? pagingPesan.HalKe : 1)}
                                                onChangePage={(e) => {
                                                  setPagePesan(e);
                                                  window.scrollTo(0, 0);
                                                }}
                                            />
                                        </div>
                                    </div>
                                    {/* jika tidak ada data maka ambil tampilan berikut */}
                                    {/* <DataEmpty title="Kamu belum memiliki Pesan"/> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default withAuth(Pesan);
