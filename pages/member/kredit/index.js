import Link from "next/link";
import { useEffect, useState } from "react";
import ItemPengajuanLuar from "../../../components/data/ItemPengajuanLuar";
import NotAvail from "../../../components/element/NotAvail";
import Layout from "../../../components/Layout";
import MenuMember from "../../../components/MenuMember";
import Breadcrumb from "../../../components/section/Breadcrumb";
import PaginationNew from "../../../components/data/PaginationNew";
import { useAppContext } from "../../../context";
import withAuth from "../../../helpers/withAuth";
import verifiedMember from "../../../helpers/verifiedMember";
import { useRouter } from "next/router";
import { defaultHeaders } from "../../../utils/defaultHeaders";
import Cookies from "js-cookie";

const index = () => {
  /* ------------------------------ FETCH KPR ------------------------------ */
  const router = useRouter()
  const {query, asPath} = router
  const { userKey, setKprList } = useAppContext();
  const [loadingKpr, setLoadingKpr] = useState(true);
  const [kprData, setKprData] = useState(null);
  const [jenisKreditKonvensional, setJenisKreditKonvensional] = useState(null);
  const [jenisKreditSyariah, setJenisKreditSyariah] = useState(null);
  const [page, setPage] = useState(query.page || 1)
  const [paging, setPaging] = useState({
    JmlHalTotal: 1
  })
  
  function onChangePage(n) {
    setPage(n);
    router.push(asPath.split('?')[0] + `?page=${n}`)
  }

  useEffect(() => {
    sessionStorage.removeItem('back_log');
  }, [])

  useEffect(() => {
    if (!userKey) return;

    fetchpointData();
    fetchJenisKredit();
    fetchJenisKreditSyariah();
  }, [userKey, page]);

  const fetchpointData = async () => {
    try {
      setLoadingKpr(true);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/nonstok/show/bymember`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: JSON.parse(Cookies.get("keyMember")),
          ...defaultHeaders
        },
        body: new URLSearchParams({
          Limit: 9999,
          Sort: "t_ins DESC",
          page: page
        }),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        setKprData(resData.Data);
        setPaging(resData.Paging)
        setKprList(resData.Data.map((data) => data.id))
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoadingKpr(false);
    }
  };

  const fetchJenisKredit = async () => {
    try {
      const dataTipe = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/jeniskredit?tipe=1`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
          },
        }
      );
      const responseTipe = await dataTipe.json();
      setJenisKreditKonvensional(responseTipe.data);
    } catch (error) {
      console.log("error get data jenis KPR : ", error);
    } finally {
      // setLoadingJenis(false);
    }
  };

  const fetchJenisKreditSyariah = async () => {
    try {
      const dataTipe = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/jeniskredit?tipe=2`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
          },
        }
      );
      const responseTipe = await dataTipe.json();
      setJenisKreditSyariah(responseTipe.data);
    } catch (error) {
      console.log("error get data jenis KPR : ", error);
    } finally {
      // setLoadingJenis(false);
    }
  };

  return (
    <Layout
      title="Pengajuan Kredit Non Listing - Dashboard Member | BTN Properti"
      isLoaderOpen={loadingKpr}
    >
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="kredit" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
              <h5 className="title_akun">Pengajuan KPR</h5>
              <div className="dashboard-profil revamp-card-developer ">
                <div className="tab-menu revamp-card-imgdev ">
                  <ul
                    className="nav nav-tabs navbar_kategori"
                    // style={{ maxWidth: "350px" }}
                  >
                    <div className="d-flex p-0">
                      <li className="nav-item">
                        <Link href="#">
                          <a className="nav-link active p-0">
                            <span>KPR di Luar Listing BTN</span>
                          </a>
                        </Link>
                      </li>
                    </div>

                    <div className="d-flex p-0">
                      <li className="nav-item">
                        <Link href="/member/kredit/listing">
                          <a className="nav-link p-0">
                            <span>KPR dalam Listing BTN</span>
                          </a>
                        </Link>
                      </li>
                    </div>

                    <div className="d-flex p-0">
                      <li className="nav-item">
                        <Link href="/member/kredit/other">
                          <a className="nav-link p-0">
                            <span style={{ fontSize: "14px" }}>Pengajuan Lainnya</span>
                          </a>
                        </Link>
                      </li>
                    </div>
                  </ul>
                </div>

                {kprData &&
                  kprData.map((data, index) => (
                    <div key={index} className="">
                      <ItemPengajuanLuar
                        data={{ ...data, jenisKreditKonvensional, jenisKreditSyariah }}
                      />
                    </div>
                  ))}
                <PaginationNew
                  current={Number(paging.HalKe)}
                  length={Number(paging.JmlHalTotal)}
                  onChangePage={onChangePage}
                />
                {!kprData && (
                  <NotAvail
                    message={
                      loadingKpr ? "Memuat data..." : "Kamu belum memiliki list pengajuan KPR"
                    }
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default (withAuth(index));
