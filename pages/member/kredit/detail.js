import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import { PDFDocument } from 'pdf-lib';
import axios from "axios";
import download from 'downloadjs';
import { useAppContext } from "../../../context";
import Lottie from "react-lottie";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import MenuMember from "../../../components/MenuMember";
import SuccessGenerate from "../../../components/popup/SuccesGenerate"
import Link from "next/link";
import withAuth from "../../../helpers/withAuth";
import rupiahFormater from "../../../helpers/rupiahFormater";
import getHeaderWithAccessKey from "../../../utils/getHeaderWithAccessKey";
import styles from "../../../components/element/modal_confirmation//modal_confirmation.module.scss";
import numberToTupiah from "../../../utils/numberToRupiah";
import * as animationData from "../../../public/animate_home.json";
import { newBase64 } from "../../../public/resume/newBase64";
import { compressedBase64 } from "../../../public/resume/compressedBase64";
import { string } from "yup";
import moment from 'moment';
import QRCode from "qrcode";
const urlEkyc = process.env.NEXT_PUBLIC_API_HOST_EKYC;
import { QRCode as QR } from "react-qrcode-logo";
import DetailOther from "./detail-other";
            
import { base64File } from "../../../public/resume/base64";
import { defaultHeaders } from "../../../utils/defaultHeaders";
import Cookies from "js-cookie";
import { generatePDF } from "../../../helpers/generatePdf";
import generateFormPdf from "../../../helpers/generateFormPdf";
import { async } from "@firebase/util";

const index = () => {
    const router = useRouter();
    const { id } = router.query;
    const idReq = id.substring(0, 5);
    const { userKey, userProfile } = useAppContext();
    const [isLoading, setIsLoading] = useState(false);
    const [memberDetail, setMemberDetail] = useState({})
    const [alert, setAlert] = useState(false)
    const [pdfFile, setPdfFile] = useState(null)
    const [statusDoc, setStatusDoc] = useState({})
    const [isSuccess, setIsSuccess] = useState(1)
    const [ekycData, setEkycData] = useState({});
    const [isDisableGenerate, setIsDisableGenerate] = useState(false)
    const url = process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE
    const [urlTekenAja, setUrlTekenAja] = useState("")
    const [urlQr, setUrlQr] = useState(`https://property.mylabzolution.com/verifikasi_document/`)
    const modalRef = useRef();
    const modalUpdate = useRef();
    const modalUpload = useRef();
    const modalDelete = useRef();
    const modalAppear = useRef(null);
    const modalClose = useRef(null);

    const openModal = () => {
        modalAppear.current.click()
    }
    const closeModal = () => {
        console.log("aaaaaaaaa");
        modalClose.current.click()
    }
    const checkDocument = async () => {
        try {
            const body = {
                userId: userProfile.id,
                kprId: id
              }
            const {data} = await axios.post(url + "/documentCheck", body)
            if(data.status == "OK") {
                return "DOCUMENT_EXISTS"

            } else {
                return data?.code
            }
        } catch (error) {
            console.log(error);
            return error.response.data.code
            // if(error.response.data.code === "DOCUMENT_NOT_FOUND") {
            //     router.push("/upload_doc/?kprId="+id)

            // }
            // setIsSuccess(-1)
        }
    }
    const checkDownload = async () => {
        try {
            const body = {
                userId: userProfile.id,
                kprId: id
              }
            const data = await axios.get(url + "/downloadCheck/?"+ new URLSearchParams(body), )
            console.log({data})
            return data
        } catch (error) {
            
            if(error.response.data.code == "RE_SIGNING_DOCUMENT") {
                setUrlTekenAja(error.response.data.data.url)
            }
            return error.response.data.code
            // if(error.response.data.code === "DOCUMENT_NOT_FOUND") {
            //     router.push("/upload_doc/?kprId="+id)

            // }
            // setIsSuccess(-1)
        }
    }

    const getDetailMember = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
            const loadData = await fetch(endpoint, {
                method: "POST",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                  AccessKey_Member: JSON.parse(sessionStorage.getItem("keyMember")),
                  ...defaultHeaders
                    },
            });
            const response = await loadData.json()
            console.log("aaaaaaaa", response)
                
            setMemberDetail(response.Data[0])

        } catch(error) {
            console.log({error})
        }
    }

    const uploadDoc = async (endpoint, formData) => {
        axios
        .post(endpoint, formData)
        .then((res) => {
            if(res.data.status == "OK") {
                setUrlTekenAja(res.data.data.url)
                setIsLoading(false)
                setIsSuccess(1)
                openModal()
                checkDocument()
            } else {
                setIsLoading(false)
                setIsSuccess(1)
                openModal()
                checkDocument()
            }
            
        })
        .catch((err) => {
            console.log('upload digital sign ', err)
            setIsLoading(false)
            setIsSuccess(-1)
            openModal()
        })
    }
    const getEkycData = async () => {
		try {
			const response = await axios.get(urlEkyc + "/getEKYCData/?userId=" + userProfile.id, {
				method: "GET",
				headers: {
				  'Accept': 'application/json',
				}
			  })
			setEkycData(response.data.data)
		} catch (error) {
			console.log(error);
			alert("ekyc data not found")
		}
	}
    const registerUser = async (uploadDoc) => {
        try {
            const body = {
                email: memberDetail?.e,
                mobileNumber: memberDetail?.no_hp,
                provinceId: memberDetail?.i_prop.toString(),
                districtId: memberDetail?.i_kot.toString().split("").splice(2,4).join(""),
                subDistrictId: memberDetail?.i_kec.toString().split("").splice(4,6).join(""),
                address: memberDetail?.almt,
                postalCode: memberDetail?.pos,
                userId: memberDetail?.id
            }
            const response = await axios.post(url+ '/registerUser',body)

            
            return response
        } catch (error) {
            setIsSuccess(-1)
            console.log("SALAH WOI", error)
        }
    }
    const hitSign = async (docId) => {
        try {
            const {data} = await axios.post(url+"/sign", {
                documentId: docId,
                email
            })
            console.log({data});
            setIsSuccess(1)
            modalAppear.current.click()
            return data
        } catch (error) {
            setIsSuccess(-1)
        }
    }

    const checkRegister = async () => {
        try {
            const res = await axios.post(url+'/registerCheck', {
                nik: memberDetail?.no_ktp,
                action: "check_nik"
            })
            return res
        } catch (error) {
            setIsSuccess(-1)
        }
    }
    const downloadDoc = async () => {
        try {
            setIsLoading(true)
            const {data} = await axios( url + "/downloadDocument/" + id, {method: "GET", responseType: "blob"})
            // setIsLoading(false)
            console.log({data});
            return data
        } catch (error) {
            // setIsLoading(false)
            console.log({error});
        }
    }
    const fetchDowload = async () => {
            setIsLoading(true)
            try {
                const code = await axios.get( url + `/download/?userId=${userProfile.id}&kprId=${id}`, {
                    responseType: 'blob'
                }) 
                return code
            } catch (error) {
                console.log(error.response);
                const json = JSON.parse(await error.response.data.text());
                console.log({json});
                if(json.code == "RE_SIGNING_DOCUMENT") {
                    setUrlTekenAja(json.data)
                }
                return json.code
            }

    }
    const postVerificationData = async () => {
		const body = {
			userId: userProfile?.id,
			kprId: id,
			documentOwnerName: detailKPR?.nm_lgkp,
			// email: memberDetail?.e,
			// applicantName: detailKPR?.nm_lgkp,
			// documentName: "resume_kpr("+kprId+").pdf",
			// documentDate: date,
			// signEmail: memberDetail?.e,
			// signDate: date
		}
		console.log({body});
		try {
			await axios.post( process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE + '/documentVerification', body);

		} catch (error) {
			console.log(error);
			
		}
	}
    const downloadManual = async () => {
        const pdfBytes = await generateFormPdf(setIsLoading, detailKPR, memberDetail, ekycData, dokPersyaratan)
        download(pdfBytes, "Dokumen Permohonan Kredit("+id+").pdf", "application/pdf")
        setIsLoading(false)
    }

    const hitDownload = async() => {
        try {
            const isReadyToDownload = await checkDownload()
            console.log({isReadyToDownload});

            if(isReadyToDownload == "DOCUMENT_IS_NOT_AVAILABLE_YET") {
                setIsLoading(false)
                setIsSuccess(2)
                openModal()
            } else if(isReadyToDownload == "RE_SIGNING_DOCUMENT") {
                setIsLoading(false)
                setIsSuccess(-3)
                openModal()
            } else if(isReadyToDownload == "DOCUMENT_NOT_FOUND") {
                router.push("/upload_doc/?kprId="+id)

            } else if(isReadyToDownload.status == 200) {
                const res = await fetchDowload()
                console.log({res})
                var file = new File([res.data], "file_name", {lastModified: 1534584790000});
                download(file, "Dokumen Permohonan Kredit("+id+").pdf", "application/pdf")
                setIsSuccess(5)
                openModal()
            }
            setIsLoading(false)

            
        } catch (error) {
            console.log({error});
            setIsLoading(false)
            setIsSuccess(-6)
            openModal()
        } 
    }

    const downloadPdf = async () => {
        setIsLoading(true)
        if(pdfFile !== null) {
            download(pdfFile, "resume_kpr.pdf", "application/pdf")
            setIsLoading(false)

        } else {
            setTimeout(() => {setIsLoading(false);setIsSuccess(-3);openModal()}, 1000)
            
        }
    }

    const [detailKPR, setDetailKPR] = useState();
    const d = new Date(detailKPR?.tgl_eln?.toString()).valueOf()
    console.log({d});

    const [jenisKategori, setJenisKategori] = useState();
    const [listDokumen, setListDokumen] = useState();
    const [trackPengajuan, setTrackPengajuan] = useState();
    const [dokSelect, setDokSelect] = useState();
    const [dokPersyaratan, setDokPersyaratan] = useState();

    const [jenisKreditKonvensional, setJenisKreditKonvensional] = useState(null);
    const [jenisKreditSyariah, setJenisKreditSyariah] = useState(null);
    
    const [validateImage, setValidateImage] = useState(false);
    const [validateMessage, setValidateMessage] = useState("")
    const [selectedImage, setSelectedImage] = useState();
    const [selectedImageUrl, setSelectedImageUrl] = useState("/images/icons/thumb.png");
    const [imageConverted, setImageConverted] = useState("");
    const [jnsDokumen, setJnsDokumen] = useState("");
    const [notMatch,setNotMatch] = useState(false);
    const [claimed,setClaimed] = useState(false)

    const [modalSucces, setModalSucces] = useState(false);
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice",
        },
    };

    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    const date = new Date(detailKPR?.t_ins)
    const formattedDate = date.toLocaleDateString("id", options)
    const datelahir = new Date(detailKPR?.tgl_lhr)
    const tgl_lahir = datelahir.toLocaleDateString("id", options)
    const dateterbit = new Date(detailKPR?.kavling?.t_ins)
    const tgl_terbit = dateterbit.toLocaleDateString("id", options)

    const listJenisKredit = detailKPR?.typ_pgjn == 1 ? jenisKreditKonvensional : jenisKreditSyariah
    const nameJenisKredit = listJenisKredit?.find(item => item.kd == (detailKPR?.jns_krdt || detailKPR?.jns_pmbyn || detailKPR?.sft_krdt || detailKPR?.SIFAT_KREDIT))
    const email = detailKPR?.eml ? detailKPR?.eml : ""
    const nik = detailKPR?.no_ktp ? detailKPR?.no_ktp : ""
    const truncate = (str) => {
        const arr = str.split(" ")
        let cuttingWord = undefined;
        let maxLength = 0

        arr.every((word, i) => {
            // maxLength > 35 ? cuttingWord = i : null
            maxLength += word.length

            console.log({maxLength});
            if(maxLength > 35) {
                cuttingWord = i
                return false
            }
        })
        console.log('lalalal', {firstLine: arr.slice(0,maxLength+1).join(" "),
            secLine: arr.slice(maxLength + 1, arr.length).join(" ")})
        return {
            firstLine: arr.slice(0,maxLength+1).join(" "),
            secLine: arr.slice(maxLength + 1, arr.length).join(" ")
        }
    }

    console.log({detailKPR});
    console.log({trackPengajuan});
    console.log({listDokumen});
    console.log({dokPersyaratan})
    console.log({statusDoc})

    const generateImageUrl = (url) => {
        if (!url) return "/images/icons/thumb.png";
      
        let fullUrl = `https://www.btnproperti.co.id/cdn1/files/${url.split('"')[0]}`;
        fullUrl = fullUrl.replaceAll(" ", "%20");
        console.log('fullurl', fullUrl)
        var img = new Image();
        const getImage = async ()=>{
        img.src = fullUrl;
        await img.decode();
        return img.height
         };
        const isImageExist = getImage() != 0;
        // img.src = fullUrl;
        // const isImageExist = img.height != 0;
      
        if (isImageExist) {
          setSelectedImageUrl(fullUrl)
        } else {
          setSelectedImageUrl("/images/icons/thumb.png")
        }
    };



    /* ------------------------------ FETCH KPR ------------------------------ */
    useEffect(() => {
        fetchJenisKredit();
        fetchJenisKreditSyariah();
        getDetailDokumen();
        if(idReq == "KPRCB"){
            getDetailOther();
        }else{
            getDetailKPR();
        }
        getDetailJenisDokumen();
        
        getPersyaratanDoc();

        checkDocument();  // comment this line to hide-digital-signature

        getEkycData() // comment this line to hide-digital-signature

        getDetailMember()
    }, []);

    useEffect(() => {
        console.log("selectnya", dokSelect)
    }, [dokSelect]);

    useEffect(() => {
        console.log("jenis dokumen",jnsDokumen)
    }, [jnsDokumen]);

    useEffect(()=>{
        if(notMatch){
            window.location.replace("/member/kredit/other")
        }
    },[notMatch])

    const checkKPROwnership = (data) =>{
        return new Promise(function (resolve, reject) {

            if(data.EMAIL.length == 0 || data.EMAIL == null) {
                ////belum diklaim////
                resolve(1);
              } else if(data.EMAIL.toLowerCase() != userProfile?.email.toLowerCase()){
                ////idkpr user lain////
                resolve(2);
              }else{
                ////idkpr cocok////
                resolve(3);
              }
          });
    }


    const formatStatus = (value) => {
        const date = new Date(value)
        const formattedDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " - " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
        return formattedDate
    }

    const downloadPDF = async () => {
        try {
            setIsLoading(true)
            
            const status = await checkDocument()
            console.log({status});
            if(status === "DOCUMENT_EXISTS") {
                hitDownload()
                
            } else if(status === "DOCUMENT_NOT_FOUND") {
                setIsLoading(false)
                router.push("/upload_doc/?kprId="+id)
            }
            
        } catch (error) {
            console.log(error);
        }
    }


    const getPersyaratanDoc = async () => {
        try {
            const params = `i_kpr=${id}&i_member=${userProfile.id}}`
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/ceklist?${params}`;
            const loadData = await fetch(endpoint, {
              method: "GET",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
            },
            });
            const response = await loadData.json();
            if (!response.IsError) {
              console.log("Success get data persyaratan", response);
              if(response.data){
                  setDokPersyaratan(response.data)
              }
            }
            //setIsLoading(false);
          } catch (error) {
            console.log("error get data persyaratan: ", error.message);
            //setIsLoading(false);
          } finally {
        }
    }

    const getDetailPengajuan = async () => {
        try {
          const payload = {
            kpr_id: id,
          };
          const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan-stok-v1/track-pengajuan`;
          const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
              },
            body: new URLSearchParams(payload),
          });
          const response = await loadData.json();
          if (!response.IsError) {
            console.log("Success get data track pengajuan", response);
            if(response.data){
                setTrackPengajuan(response.data.data)
            } else {
                setTrackPengajuan(null)
            }
          }
          //setIsLoading(false);
        } catch (error) {
            console.log("error get detail KPR: ", error.message);
            
          //setIsLoading(false);
        } finally {
        }
      };

      const getDetailPengajuanOther = async () => {
        try {
          const payload = {
            kpr_id: id,
          };
          const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/other/tracking`;
          
          const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: 'TXugSwyTQkBQNLWMcPgc',
                secret_key: 'rwBea8tTftJHZ87C9nm4XP7MD8FpLzND',
                /////app_key & secret_key for dev only /////
              },
            body: new URLSearchParams(payload),
          });
          const response = await loadData.json();
          if (!response.IsError) {
            console.log("Success get data track pengajuan", response);
            if(response.data){
                setTrackPengajuan(response.data.data)
            } else {
                setTrackPengajuan(null)
            }
          }
          //setIsLoading(false);
        } catch (error) {
            console.log("error get detail KPR: ", error.message);
            
          //setIsLoading(false);
        } finally {
        }
      };


    const getDetailJenisDokumen = async () => {
        try {
          const accessKey = JSON.parse(Cookies.get("keyMember"));
          const payload = {
            i_kat: 4,
          };
          const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/umum/show`
          const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
                AccessKey_App: accessKey,
                ...defaultHeaders
            },
            body: new URLSearchParams(payload),
          });
          const response = await loadData.json();
          if (!response.IsError) {
            console.log("Success get data jenis kategori", response);
            setJenisKategori(response.Data)
          }
          //setIsLoading(false);
        } catch (error) {
          console.log("error get detail KPR: ", error.message);
          //setIsLoading(false);
        } finally {
        }
    };

    const getDetailKPR = async () => {
        try {
          let apiHit;
          if(idReq == "KPRST"){
            apiHit = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/stok/detail`;
          } else {
            apiHit = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/nonstok/show/bymember`;
          }

          setIsLoading(true);
          const accessKey = JSON.parse(Cookies.get("keyMember"));
          const payload = {
            id: id
          };
          const endpoint = apiHit;
          const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
              AccessKey_Member: accessKey,
              ...defaultHeaders
            },
            body: new URLSearchParams(payload),
          });
          const response = await loadData.json();
          if (!response.IsError) {
            console.log("Success get data detail KPR", response);
            if (response.Data.length) {
                setDetailKPR(response.Data[0])
                setIsDisableGenerate(false);
                setIsLoading(false);
                getDetailPengajuan();
            }
          } else {
            setIsDisableGenerate(true);
          }
          //setIsLoading(false);
        } catch (error) {
          console.log("error get detail KPR: ", error.message);
          setIsLoading(false);
        } finally {
        }
      };

      const getDetailDokumen = async () => {
        try {
          const params = `i_kpr=${id}&i_member=${userProfile.id}}`
          const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/show?${params}`;
          const loadData = await fetch(endpoint, {
            method: "GET",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
            }
          });
          const response = await loadData.json();

          console.log("Success get data detail Dokumen", response.message);

          if(!response.status){
            setNotMatch(false)
          }

          if (response.status) {
            if (response?.data?.length) {
                setListDokumen(response.data);
            }
          }
          //setIsLoading(false);
        } catch (error) {
          console.log("error get detail Dokumen: ", error);
          
          
          setIsLoading(false);
        } finally {
            setIsLoading(false);
        }
      };

      const getDetailOther = async (e) => {
        try {
          const payload = {
            ID_KPR: id,
          };
          const dataTipe = await fetch(
            `${process.env.NEXT_PUBLIC_API_HOST_OTHER_PENGAJUAN}/other-pengajuan/detail`,
            {
              method: "POST",
              headers: {
                Accept: "application/json",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
              },
              body: new URLSearchParams(payload),
            }
          );
          const responseTipe = await dataTipe.json();
    
          console.log("data other", responseTipe);
          
          checkKPROwnership(responseTipe.data).then((value)=>{

            if(value == 3){
                setDetailKPR(responseTipe.data);
                getDetailPengajuanOther();
            }else if(value == 1){
                window.location.replace("/member/kredit/other/");
            }else{
                setClaimed(true);
                
            }
          })
    
          
        } catch (error) {
          console.log("error get data jenis KPR : ", error);
        } finally {
        }
      }

      const fetchJenisKredit = async () => {
        try {
          const dataTipe = await fetch(
            `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/jeniskredit?tipe=1`,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
              },
            }
          );
          const responseTipe = await dataTipe.json();
          setJenisKreditKonvensional(responseTipe.data);
        } catch (error) {
          console.log("error get data jenis KPR : ", error);
        } finally {
          // setLoadingJenis(false);
        }
      };

      const fetchJenisKreditSyariah = async () => {
        try {
          const dataTipe = await fetch(
            `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/jeniskredit?tipe=2`,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
              },
            }
          );
          const responseTipe = await dataTipe.json();
          setJenisKreditSyariah(responseTipe.data);
        } catch (error) {
          console.log("error get data jenis KPR : ", error);
        } finally {
          // setLoadingJenis(false);
        }
      };

    const imageChange = (e) => {
        if (e.target.files.length > 0) {
            if ((e.target.files[0].type.includes("jpeg") ||
                e.target.files[0].type.includes("jpg") ||
                e.target.files[0].type.includes("png")) && (e.target.files[0].size / 1048576).toFixed(2) < 3){
                    if (e.target.files && e.target.files.length > 0) {
                        setValidateImage(false);
                        setValidateMessage("")
                        setSelectedImage(e.target.files[0]);
                        var reader = new FileReader();
                        reader.readAsDataURL(e.target.files[0]);

                        reader.onload = function () {
                            setImageConverted(reader.result);
                        };

                        reader.onerror = function (error) {
                            console.log("error attach image : ", error);
                        };
                }
            } else {
                setValidateImage(true);
                setValidateMessage("Ukuran file tidak melebihi dari 3MB")
            }
        }
    };

    const uploadImage = async () => {
        let resumeDoc;
        if (jnsDokumen){
            if (imageConverted) {
                const picture1 = await makeRequest(imageConverted, selectedImage, "1");
                if (picture1) {
                    resumeDoc = picture1;
                }
                submitDokumen(resumeDoc);
                modalRef?.current?.click();
            } else {
                setValidateImage(true);
                setValidateMessage("Silakan pilih file dokumen")
            }
        } else {
            setValidateImage(true);
            setValidateMessage("Silakan pilih jenis dokumen")
        }
    }

    const updateImage = async () => {
        let resumeDoc;
        let id;
        var promises = jenisKategori?.map((data, index) => {
            if(dokSelect.jenis === data.nl){
                id = data.id
            }
        });
        await Promise.all(promises).then(async() => {
            if (imageConverted) {
                const picture1 = await makeRequest(imageConverted, selectedImage, "1");
                if (picture1) {
                    resumeDoc = picture1;
                }
                //console.log("jenissnya",id)
                submitUpdateDokumen(resumeDoc,id);
                modalUpload?.current?.click();
            } else {
                setValidateImage(true);
                setValidateMessage("Silakan pilih file dokumen")
            }
        });
    }

    const makeRequest = (imageConverted, selectedImage, index) => {
        return new Promise(async (result, reject) => {
          try {
            setIsLoading(true);
            const uploadPayload = {
              FolderName: "dokumenidentitas",
              //FileExtension: selectedImage.name.split(".").pop(),
              FileBase64: imageConverted.split("base64,")[1],
              TipeUpload: "1"
            };
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/image/upload`;
            const loadData = await fetch(endpoint, {
              method: "POST",
              headers: getHeaderWithAccessKey(),
              body: new URLSearchParams(uploadPayload),
            });
            const response = await loadData.json();
            if (!response.IsError) {
              console.log("image ", index, " berhasil di upload ", response.Output);
              result(response.Output);
            } else {
              console.log("error upload file ", index, " ", response.ErrToUser);
              setIsLoading(false);
              // throw {
              //     message: response.ErrToUser,
              // };
            }
          } catch (error) {
            console.log("Kena catch upload files! ", error.message);
            setIsLoading(false);
          } finally {
          }
        });
    };

    const submitDokumen = async (resumeDoc) => {
        try {
          const payload = {
            i_kpr: id,
            fl: resumeDoc,
            jns: jnsDokumen,
            i_member: userProfile.id
          };
          const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/tambah`;
          const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
            },
            body: new URLSearchParams(payload),
          });
          const response = await loadData.json();
          if (!response.IsError) {
            console.log("Success submit Dokumen", response);
            setModalSucces(true);
          }
          setIsLoading(false);
        } catch (error) {
          console.log("error submit Dokumen: ", error.message);
          setIsLoading(false);
        } finally {
        }
      };

      const submitUpdateDokumen = async (resumeDoc, idDok) => {
        try {
            const payload = {
                id: dokSelect.id,
                i_kpr: id,
                fl: resumeDoc,
                jns: idDok,
                i_member: userProfile.id,
            };
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/update`;
            const loadData = await fetch(endpoint, {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    app_key: process.env.NEXT_PUBLIC_APP_KEY,
                    secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                    ...defaultHeaders
                },
                body: new URLSearchParams(payload),
            });
            const response = await loadData.json();
            if (!response.IsError) {
                console.log("Success submit Dokumen", response);
                modalRef?.current?.click();
                setModalSucces(true);
            }
            setIsLoading(false);
            } catch (error) {
            console.log("error submit Dokumen: ", error.message);
            setIsLoading(false);
            } finally {
        }
      };

      const deleteImage = async () => {
        modalDelete?.current?.click();
        try {
            setIsLoading(true);
            const payload = {
              id: dokSelect.id,
              i_kpr: id,
              i_member: userProfile.id,
            };
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/hapus`;
            const loadData = await fetch(endpoint, {
              method: "POST",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                ...defaultHeaders
            },
              body: new URLSearchParams(payload),
            });
            const response = await loadData.json();
            if (!response.IsError) {
              console.log("Success delete data dokumen", response);
              setModalSucces(true);
              setIsLoading(false);
            } else {
              setIsLoading(false);
            }
          } catch (error) {
            console.log("error delete data dokumen: ", error.message);
            setIsLoading(false);
          } finally {
          }
      }

    const Ceklist = () => {
        return (
        <svg width="17" height="12" viewBox="0 0 17 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.7328 0.263488C16.9039 0.432292 17 0.661236 17 0.899957C17 1.13868 16.9039 1.36762 16.7328 1.53643L6.39287 11.7364C6.22175 11.9052 5.98967 12 5.74767 12C5.50567 12 5.27359 11.9052 5.10247 11.7364L0.263351 6.96269C0.0939465 6.79354 -0.000727482 6.56513 4.20982e-06 6.32735C0.000735902 6.08958 0.0968136 5.86174 0.267256 5.69361C0.437699 5.52547 0.668659 5.43069 0.909701 5.42997C1.15074 5.42925 1.38228 5.52264 1.55376 5.68975L5.74764 9.82693L15.4426 0.263488C15.6137 0.0947753 15.8458 0 16.0877 0C16.3296 0 16.5616 0.0947753 16.7328 0.263488Z" fill="black"/>
        </svg>
    )}

    return (
        <>
        {idReq == "KPRCB" ? <DetailOther detailKprData={detailKPR} dokPersyaratan={dokPersyaratan} trackPengajuan={trackPengajuan} claimed={claimed}/>
        :(


        <Layout
            title="Pengajuan Kredit Non Listing - Dashboard Member | BTN Properti"
            isLoaderOpen={isLoading}>
            <Breadcrumb active="Akun" />
            <button
            type="button"
            data-bs-target="#modalGeneratePdf"
            data-bs-dismiss="modal"
            style={{display: 'none'}}
            ref={modalClose}
            >
            </button>
            {/* modal digital signature */}
            <SuccessGenerate isSuccess={isSuccess} downloadPDF={downloadPdf} url={urlTekenAja} /> 
            {/* <div style={{display: "none"}}>
                <QR
                value={
                    urlQr
                } // here you should keep the link/value(string) for which you are generation promocode
                size={250} // the dimension of the QR code (number)
                logoImage="/images/qr_logo.jpg" // URL of the logo you want to use, make sure it is a dynamic url
                logoHeight={65}
                logoWidth={65}
                logoOpacity={1}
                enableCORS={true} // enabling CORS, this is the thing that will bypass that DOM check
                qrStyle="squares" // type of qr code, wether you want dotted ones or the square ones
                eyeRadius={10} // radius of the promocode eye
                id={"QR"}   
                />
            </div> */}
            <button
                type="button"
                data-bs-target="#modalGeneratePdf"
                data-bs-toggle="modal"
                style={{display: "none"}}
                ref={modalAppear}
            ></button>
            <div className="dashboard-content mb-5">
                <div className="container">
                    <div id="detail_ajukan_kpr">
                    <div className="row">
                        <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
                            <MenuMember active="kredit" />
                        </div>
                        <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                            <h5 className="title_akun">Pengajuan KPR</h5>
                            <div className="dashboard-profil revamp-card-developer ">
                                <div className="tab-menu revamp-card-imgdev ">
                                    <div id="card_detail_pengajuan">
                                        <div className="card card_pengajuan">
                                            <div className="card-header ">
                                                <div className="kode" style={{ fontFamily: "FuturaBT", fontWeight: "700" }}>
                                                    {
                                                    detailKPR?.id ? detailKPR.id : "-"} &middot; <span>
                                                        <div className="break-line"><br/></div>
                                                        {formattedDate ? formattedDate : "-"}
                                                        </span>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="row g-md-0" style={{paddingTop: "10px"}}>
                                                    <div className="col-6 col-md-5 col-lg-3 ps-0 pe-1 item" style={{maxWidth:"186px"}}>
                                                        <a 
                                                        onClick={() => d > process.env.NEXT_PUBLIC_UNIX_TIMESTAMP ? downloadPDF() : downloadManual()} // comment this line to hide-digital-signature
                                                        // onClick={downloadManual}
                                                        className="btn btn-main w-100 d-flex justify-content-center align-items-center" style={{fontSize: "14px" ,padding: "13px 20px" ,fontWeight: 700,  maxWidth: 180, height: 48, pointerEvents: isDisableGenerate ? "none" : "auto", opacity: isDisableGenerate ? 0.5 : 1, fontFamily: "Helvetica",}} download>
                                                            Generate Form KPR
                                                        </a>
                                                    </div>
                                                    <div className="col-6 col-md-7 col-lg-5 ps-1 pe-0 item">
                                                        <div className="btn btn-outline-main btn_rounded w-100 d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#modal-persyaratan" style={{maxWidth: 257,minHeight: 48, alignItems: "center", justifyContent: "center",fontFamily: "Helvetica",fontWeight: 700}}>
                                                            Dokumen Persyaratan KPR
                                                        </div>
                                                    </div>
                                                    <div className="col item status-detail-pengajuan">
                                                        <label id="status_web" style={{fontSize: "14px" ,textAlign: "right"}}>Status Pengajuan</label>
                                                        <div className="d-flex justify-content-between">
                                                            <div className="col-6 text-start mt-auto">
                                                            <label id="status_mobile" style={{textAlign:"start",fontSize: "14px"}}>Status Pengajuan</label>
                                                            </div>
                                                            <div className="col-6 text-end">
                                                            
                                                            <label style={{ fontSize: "24px" ,textAlign: "right", fontWeight: 700, color: "#00193E"}}>Proses Pengajuan Kredit</label>
                                                            </div>
                                                        </div>
                                                        <label className={trackPengajuan?.length > 0 ? 'label_hover_kpr' : 'label_hover_kpr_disabled'} style={{width: "81px", fontSize: "12px" , float: "right", fontWeight: 700, color: "#00193E"}}
                                                            data-bs-toggle={trackPengajuan?.length > 0 ? 'modal' : ''} data-bs-target="#modal-detail">Lihat Detail
                                                            <svg style={{verticalAlign:"-1px"}} width="15" height="10" viewBox="0 0 1 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M0 10L5 5L-4.37114e-07 0L0 10Z" fill="#00193E"/>
                                                            </svg>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="row g-md-0" style={{paddingBottom: "10px"}}>
                                                    <label style={{fontSize: "24px" , fontWeight: 700, color: "#8F8F8F"}}>Data Kredit</label>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row" style={{paddingTop: "10px"}}>
                                                    <div className="col-6 item">
                                                        <label>Jenis kredit</label>
                                                        <strong>{nameJenisKredit ? nameJenisKredit.nl : "-"}</strong>
                                                    </div>
                                                    {detailKPR?.n_cbg &&
                                                    <div className="col-6 item">
                                                        <label>Kantor Cabang</label>
                                                        <strong>{detailKPR?.n_cbg}</strong>
                                                    </div>
                                                    }
                                                    {detailKPR?.bga &&
                                                    <div className="col-6 item">
                                                        <label>Suku Bunga Promo per Tahun</label>
                                                        <strong>{detailKPR?.bga+"%" }</strong>
                                                    </div>
                                                    }
                                                    {detailKPR?.sys_bga &&
                                                    <div className="col-6 item">
                                                        <label>Suku Bunga Floating per Tahun</label>
                                                        <strong>{detailKPR?.sys_bga+"%"}</strong>
                                                    </div>
                                                    }
                                                    {detailKPR?.jng_wkt || detailKPR?.jangkaWaktu ? 
                                                    <div className="col-6 item">
                                                        <label>Masa Kredit Fix</label>
                                                        <strong>{(detailKPR?.jng_wkt || detailKPR?.jangkaWaktu)+" Bulan" }</strong>
                                                    </div>
                                                    : <></>
                                                    }
                                                </div>
                                                <div className="row g-md-0" style={{padding: "20px 0 10px 0"}}>
                                                    <label style={{fontSize: "24px" , fontWeight: 700, color: "#8F8F8F"}}>Data Diri</label>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row" style={{paddingTop: "10px"}}>
                                                    {detailKPR?.nm_lgkp &&
                                                    <div className="col-6 item">
                                                        <label>Nama Lengkap</label>
                                                        <strong>{detailKPR?.nm_lgkp}</strong>
                                                    </div>
                                                    }
                                                    {detailKPR?.no_ktp &&
                                                    <div className="col-6 item">
                                                        <label>No. KTP</label>
                                                        <strong>{detailKPR?.no_ktp}</strong>
                                                    </div>
                                                    }
                                                    {detailKPR?.tpt_lhr &&
                                                    <div className="col-6 item">
                                                        <label>Kota Lahir</label>
                                                        <strong>{detailKPR?.tpt_lhr}</strong>
                                                    </div>
                                                    }
                                                    {tgl_lahir &&
                                                    <div className="col-6 item">
                                                        <label>Tanggal Lahir</label>
                                                        <strong>{tgl_lahir ?  tgl_lahir == 'Invalid Date' ? "-" : tgl_lahir 
                                                          : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.almt &&
                                                    <div className="col-6 item">
                                                        <label>Alamat KTP</label>
                                                        <strong>{detailKPR?.almt}</strong>
                                                    </div>}
                                                    {detailKPR?.n_prop &&
                                                    <div className="col-6 item">
                                                        <label>Provinsi</label>
                                                        <strong>{detailKPR?.n_prop}</strong>
                                                    </div>}
                                                    {detailKPR?.n_kot &&
                                                    <div className="col-6 item">
                                                        <label>Kota</label>
                                                        <strong>{detailKPR?.n_kot ? detailKPR?.n_kot : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.pos &&
                                                    <div className="col-6 item">
                                                        <label>Kode Pos</label>
                                                        <strong>{detailKPR?.pos}</strong>
                                                    </div>}
                                                    {detailKPR?.n_kec &&
                                                    <div className="col-6 item">
                                                        <label>Kecamatan</label>
                                                        <strong>{detailKPR?.n_kec}</strong>
                                                    </div>}
                                                    {detailKPR?.n_kel &&
                                                    <div className="col-6 item">
                                                        <label>Kelurahan</label>
                                                        <strong>{detailKPR?.n_kel }</strong>
                                                    </div>}
                                                    {detailKPR?.rt && 
                                                    <div className="col-6 item">
                                                        <label>RT / RW</label>
                                                        <strong>{detailKPR?.rt ? detailKPR?.rt : "-"} / {detailKPR?.rw ? detailKPR?.rw : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.no_hp1 &&
                                                    <div className="col-6 item">
                                                        <label>No. Handphone</label>
                                                        <strong>{detailKPR?.no_hp1}</strong>
                                                    </div>}
                                                    {detailKPR?.statusRumah &&
                                                    <div className="col-6 item">
                                                        <label>Status Rumah</label>
                                                        <strong>{detailKPR?.statusRumah ? detailKPR?.statusRumah : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.lamaDitempati &&
                                                    <div className="col-6 item">
                                                        <label>Lama Ditempati</label>
                                                        <strong>{detailKPR?.lamaDitempati}</strong>
                                                    </div>}
                                                    {detailKPR?.dijaminkan &&
                                                    <div className="col-6 item">
                                                        <label>Sedang DIjaminkan/tidak</label>
                                                        <strong>{detailKPR?.dijaminkan}</strong>
                                                    </div>}
                                                    {detailKPR?.nonpwp &&
                                                    <div className="col-6 item">
                                                        <label>No. NPWP</label>
                                                        <strong>{detailKPR?.nonpwp}</strong>
                                                    </div>}
                                                    {detailKPR?.agama &&
                                                    <div className="col-6 item">
                                                        <label>Agama</label>
                                                        <strong>{detailKPR?.agama ? detailKPR?.agama : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.pendidikanTerakhir &&
                                                    <div className="col-6 item">
                                                        <label>Pendidikan terakhir</label>
                                                        <strong>{detailKPR?.pendidikanTerakhir}</strong>
                                                    </div>}
                                                    {detailKPR?.jenisKelamin &&
                                                    <div className="col-6 item">
                                                        <label>Jenis Kelamin</label>
                                                        <strong>{detailKPR?.jenisKelamin}</strong>
                                                    </div>}
                                                    {detailKPR?.jumlahTanggungan &&
                                                    <div className="col-6 item">
                                                        <label>Jumlah Tanggungan</label>
                                                        <strong>{detailKPR?.jumlahTanggungan}</strong>
                                                    </div>}
                                                </div>
                                                    {
                                                    detailKPR?.badanUsaha || 
                                                    detailKPR?.jns_pkrjn ||
                                                    detailKPR?.badanUsaha ||
                                                    detailKPR?.ALAMAT_PEKERJAAN ||
                                                    detailKPR?.ID_PROPINSI_PEKERJAAN ||
                                                    detailKPR?.ID_KAB_KOTA_PEKERJAAN ||
                                                    detailKPR?.ID_KECAMATAN_PEKERJAAN ||
                                                    detailKPR?.ID_DESA_KELURAHAN_PEKERJAAN ||
                                                    detailKPR?.KODEPOS_PEKERJAAN ||
                                                    detailKPR?.RT_PEKERJAAN ||
                                                    detailKPR?.NO_TELP_PEKERJAAN ||
                                                    detailKPR?.statusPekerjaan ||
                                                    detailKPR?.lamaTempatBekerja ||
                                                    detailKPR?.departemen ||
                                                    detailKPR?.jabatan ||
                                                    detailKPR?.masaKerja ||
                                                    detailKPR?.nipinrp ||
                                                    detailKPR?.namaAtasan ||
                                                    detailKPR?.noTelpAtasan ||
                                                    detailKPR?.pengalamanKerja ||
                                                    memberDetail?.pghsln ||
                                                    memberDetail?.bya_rmh_tng ||
                                                    memberDetail?.pglrn_ln
                                                ?
                                                <div>
                                                <div className="row g-md-0" style={{padding: "20px 0 10px 0"}}>
                                                    <label style={{fontSize: "24px" , fontWeight: 700, color: "#8F8F8F"}}>Data Pekerjaan</label>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row" style={{paddingTop: "10px"}}>
                                                    {detailKPR?.badanUsaha &&
                                                    <div className="col-6 item">
                                                        <label>Instansi</label>
                                                        <strong>{detailKPR?.badanUsaha}</strong>
                                                    </div>}
                                                    {detailKPR?.jns_pkrjn &&
                                                    <div className="col-6 item">
                                                        <label>Jenis Pekerjaan</label>
                                                        <strong>{detailKPR?.jns_pkrjn}</strong>
                                                    </div>}
                                                    {detailKPR?.badanUsaha &&
                                                    <div className="col-6 item">
                                                        <label>Bentuk Badan Usaha</label>
                                                        <strong>{ detailKPR?.badanUsaha}</strong>
                                                    </div>}
                                                    {detailKPR?.ALAMAT_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>Alamat Perusahaan</label>
                                                        <strong>{detailKPR?.ALAMAT_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.ID_PROPINSI_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>Provinsi Perusahaan</label>
                                                        <strong>{detailKPR?.ID_PROPINSI_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.ID_KAB_KOTA_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>Kota Perusahaan</label>
                                                        <strong>{ detailKPR?.ID_KAB_KOTA_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.ID_KECAMATAN_PEKERJAAN && 
                                                    <div className="col-6 item">
                                                        <label>Kecamatan Perusahaan</label>
                                                        <strong>{detailKPR?.ID_KECAMATAN_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.ID_DESA_KELURAHAN_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>Kelurahan Perusahaan</label>
                                                        <strong>{detailKPR?.ID_DESA_KELURAHAN_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.KODEPOS_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>Kodepos Perusahaan</label>
                                                        <strong>{detailKPR?.KODEPOS_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.RT_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>RT / RW Perusahaan</label>
                                                        <strong>{detailKPR?.RT_PEKERJAAN} / {detailKPR?.RW_PEKERJAAN ? detailKPR?.RW_PEKERJAAN : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.NO_TELP_PEKERJAAN &&
                                                    <div className="col-6 item">
                                                        <label>No. Telepon Perusahaan</label>
                                                        <strong>{detailKPR?.NO_TELP_PEKERJAAN}</strong>
                                                    </div>}
                                                    {detailKPR?.statusPekerjaan && 
                                                    <div className="col-6 item">
                                                        <label>Status Pekerjaan</label>
                                                        <strong>{detailKPR?.statusPekerjaan}</strong>
                                                    </div>}
                                                    {detailKPR?.lamaTempatBekerja &&
                                                    <div className="col-6 item">
                                                        <label>Lama Tempat Bekerja</label>
                                                        <strong>{detailKPR?.lamaTempatBekerja+" Tahun"}</strong>
                                                    </div>}
                                                    {detailKPR?.departemen &&
                                                    <div className="col-6 item">
                                                        <label>Departemen</label>
                                                        <strong>{detailKPR?.departemen}</strong>
                                                    </div>}
                                                    {detailKPR?.jabatan &&
                                                    <div className="col-6 item">
                                                        <label>Jabatan</label>
                                                        <strong>{detailKPR?.jabatan}</strong>
                                                    </div>}
                                                    {detailKPR?.masaKerja &&
                                                    <div className="col-6 item">
                                                        <label>Masa Kerja</label>
                                                        <strong>{detailKPR?.masaKerja}</strong>
                                                    </div>}
                                                    {detailKPR?.nipinrp &&
                                                    <div className="col-6 item">
                                                        <label>NIP / NRP</label>
                                                        <strong>{detailKPR?.nipinrp}</strong>
                                                    </div>}
                                                    {detailKPR?.namaAtasan &&
                                                    <div className="col-6 item">
                                                        <label>Nama Atasan</label>
                                                        <strong>{detailKPR?.namaAtasan}</strong>
                                                    </div>}
                                                    {detailKPR?.noTelpAtasan &&
                                                    <div className="col-6 item">
                                                        <label>Nomor Telepon Atasan</label>
                                                        <strong>{detailKPR?.noTelpAtasan}</strong>
                                                    </div>}
                                                    {detailKPR?.pengalamanKerja &&
                                                    <div className="col-6 item">
                                                        <label>Pengalaman Kerja  Di Tempat</label>
                                                        <strong>{detailKPR?.pengalamanKerja}</strong>
                                                    </div>}
                                                    {memberDetail?.pghsln &&
                                                    <div className="col-6 item">
                                                        <label>Penghasilan</label>
                                                        <strong>{numberToTupiah(memberDetail?.pghsln)}</strong>
                                                    </div>}
                                                    {memberDetail?.bya_rmh_tng &&
                                                    <div className="col-6 item">
                                                        <label>Biaya Hidup</label>
                                                        <strong>{numberToTupiah(memberDetail?.bya_rmh_tng)}</strong>
                                                    </div>}
                                                    <div className="col-6 item">
                                                        <label>Angsuran Lain</label>
                                                        <strong>{numberToTupiah(memberDetail?.pglrn_ln)}</strong>
                                                    </div>
                                                </div>
                                                </div>
                                                : <></>}
                                                {
                                                detailKPR?.kavling?.almt ||
                                                detailKPR?.kavling?.blk ||
                                                detailKPR?.kavling?.no ||
                                                detailKPR?.kavling?.n_prop ||
                                                detailKPR?.kavling?.n_kot ||
                                                detailKPR?.kavling?.pos ||
                                                detailKPR?.kavling?.n_kec ||
                                                detailKPR?.kavling?.n_kel 
                                                ?
                                                <div>
                                                <div className="row g-md-0" style={{padding: "20px 0 10px 0"}}>
                                                    <label style={{fontSize: "24px" , fontWeight: 700, color: "#8F8F8F"}}>Data Properti / Agunan</label>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row" style={{paddingTop: "10px"}}>
                                                    {detailKPR?.kavling?.almt &&
                                                    <div className="col-6 item">
                                                        <label>Alamat Agunan</label>
                                                        <strong>{detailKPR?.kavling?.almt ? detailKPR?.kavling.almt : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.blk &&
                                                    <div className="col-6 item">
                                                        <label>Blok</label>
                                                        <strong>{detailKPR?.kavling?.blk}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.no &&
                                                    <div className="col-6 item">
                                                        <label>Nomor</label>
                                                        <strong>{detailKPR?.kavling?.no }</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.n_prop &&
                                                    <div className="col-6 item">
                                                        <label>Provinsi</label>
                                                        <strong>{detailKPR?.kavling?.n_prop}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.n_kot &&
                                                    <div className="col-6 item">
                                                        <label>Kota</label>
                                                        <strong>{detailKPR?.kavling?.n_kot}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.pos &&
                                                    <div className="col-6 item">
                                                        <label>Kodepos</label>
                                                        <strong>{detailKPR?.kavling?.pos}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.n_kec &&
                                                    <div className="col-6 item">
                                                        <label>Kecamatan</label>
                                                        <strong>{detailKPR?.kavling?.n_kec}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.n_kel &&
                                                    <div className="col-6 item">
                                                        <label>Kelurahan</label>
                                                        <strong>{detailKPR?.kavling?.n_kel}</strong>
                                                    </div>}
                                                    {/* <div className="col-6 item">
                                                        <label>Dati II</label>
                                                        <strong>-</strong>
                                                    </div> */}
                                                    {detailKPR?.kavling?.srtfkt &&
                                                    
                                                    <div className="col-6 item">
                                                        <label>Status Bukti Kepemilikan</label>
                                                        <strong>{detailKPR?.kavling?.srtfkt ? detailKPR?.kavling.srtfkt : "-"}</strong>
                                                    </div>}

                                                    {/* <div className="col-6 item">
                                                        <label>No. Sertifikat</label>
                                                        <strong>-</strong>
                                                    </div> */}

                                                    {tgl_terbit &&
                                                    <div className="col-6 item">
                                                        <label>Tanggal Terbit</label>
                                                        <strong>{(tgl_terbit && tgl_terbit !== 'Invalid Date') ? tgl_terbit : "-"}</strong>
                                                    </div>}
                                                    
                                                    {/* <div className="col-6 item">
                                                        <label>Tanggal Jatuh Tempo</label>
                                                        <strong>-</strong>
                                                    </div> */}
                                                    {detailKPR?.kavling?.ls_tnh &&
                                                    <div className="col-6 item">
                                                        <label>Luas Tanah</label>
                                                        <strong style={{textTransform:'lowercase'}}>{detailKPR?.kavling?.ls_tnh ? detailKPR?.kavling.ls_tnh+" m²" : "-"}</strong>
                                                    </div>}
                                                    {detailKPR?.kavling?.ls_bgn &&
                                                    <div className="col-6 item">
                                                        <label>Luas Bangunan</label>
                                                        <strong style={{textTransform:'lowercase'}}> {detailKPR?.kavling.ls_bgn+" m²"}</strong>
                                                    </div>}
                                                    
                                                    {/* <div className="col-6 item">
                                                        <label>Atas Nama</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Nomor IMB</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Tanggal Terbit IMB</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Jarak Agunan Ke Tempat Kerja</label>
                                                        <strong>-</strong>
                                                    </div> */}
                                                </div>
                                                </div>
                                                : <></>}
                                                {/* <div className="row g-md-0" style={{padding: "20px 0 10px 0"}}>
                                                    <label style={{fontSize: "24px" , fontWeight: 700, color: "#8F8F8F"}}>Data Kontak Darurat</label>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row" style={{paddingTop: "10px"}}>
                                                    <div className="col-6 item">
                                                        <label>Nama Lengkap</label>
                                                        <strong>{detailKPR?.namaLengkapDarurat ? detailKPR?.namaLengkapDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Hubungan Keluarga</label>
                                                        <strong>{detailKPR?.hubunganKeluarga ? detailKPR?.hubunganKeluarga : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Alamat</label>
                                                        <strong>{detailKPR?.alamatDarurat ? detailKPR?.alamatDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Provinsi</label>
                                                        <strong>{detailKPR?.n_propDarurat ? detailKPR?.n_propDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kota</label>
                                                        <strong>{detailKPR?.n_kotDarurat ? detailKPR?.n_kotDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kode Pos</label>
                                                        <strong>{detailKPR?.posDarurat ? detailKPR?.posDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kecamatan</label>
                                                        <strong>{detailKPR?.n_kecDarurat ? detailKPR?.n_kecDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kelurahan</label>
                                                        <strong>{detailKPR?.n_kelDarurat ? detailKPR?.n_kelDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>RT / RW</label>
                                                        <strong>{detailKPR?.rtDarurat ? detailKPR?.rtDarurat : "-"} / {detailKPR?.rwDarurat ? detailKPR?.rwDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Dati II</label>
                                                        <strong>{detailKPR?.datiDarurat ? detailKPR?.datiDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>No. Telepon Rumah</label>
                                                        <strong>{detailKPR?.noTelpDarurat ? detailKPR?.noTelpDarurat : "-"}</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>No. Handphone</label>
                                                        <strong>{detailKPR?.noHp1Darurat ? detailKPR?.noHp1Darurat : "-"}</strong>
                                                    </div>
                                                </div> */}
                                                <div className="row g-md-0" style={{padding: "20px 0 10px 0"}}>
                                                    <div className="col-md-6 col-5">
                                                        <label id="label-dokumen">File Dokumen</label>
                                                    </div>
                                                    <div className="col-md-6 col-7" style={{textAlign: "right"}}>
                                                        <a data-bs-toggle="modal" data-bs-target="#modal-upload-doc" 
                                                        className="btn btn-main d-flex p-1 justify-content-center ms-auto" 
                                                        style={{ fontSize: "14px" ,padding: "13px 20px" ,fontWeight: 700,  maxWidth: 219, height: 48,alignItems:"center",fontFamily: "Helvetica" }}>
                                                            <div>
                                                            Tambah File Dokumen
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row g-md-0" style={{padding: "20px 0 50px 0"}}>
                                                    <div className="col-3 col-md-1">
                                                        <label>No.</label>
                                                        {listDokumen?.map((data, index) => (
                                                            <label style={{fontWeight: 700, color: "#00193E",whiteSpace:"nowrap",overflow:"hidden"}}>{index+1}</label>
                                                        ))}
                                                    </div>
                                                    <div className="col-5 col-md-3 col-lg-3">
                                                        <label>Jenis Dokumen</label>
                                                        {listDokumen?.map((data, index) => (
                                                            <label 
                                                            onClick={()=> {
                                                                setDokSelect(data)
                                                                generateImageUrl(data?.file?.split("|")[1])
                                                            }}
                                                            data-bs-toggle="modal" data-bs-target="#modal-update" 
                                                            style={{fontWeight: 700, color: "#00193E",whiteSpace:"nowrap",overflow:"hidden",textOverflow:"ellipsis"}}>{data.jenis}</label>
                                                        ))}
                                                    </div>
                                                    <div className="pe-0 col-4 col-md-2">
                                                        <label>File</label>
                                                        {listDokumen?.map((data, index) => (
                                                            <label 
                                                            onClick={()=> {
                                                                setDokSelect(data)
                                                                generateImageUrl(data?.file?.split("|")[1])
                                                            }}
                                                            data-bs-toggle="modal" data-bs-target="#modal-update" 
                                                            style={{cursor: "pointer", fontWeight: 700, color: "#00193E", textDecoration: "underline",whiteSpace:"nowrap",overflow:"hidden",textOverflow:"ellipsis"}}>{data.file?.split("/")[2] ? data.file?.split("/")[2] : "-"}</label>
                                                        ))}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            {/* ------------------------------ Modal Detail ------------------------------ */}
            <div className="modal fade" id="modal-detail" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica" }}>
                <div id="lihat_detail_kpr" className="modal-dialog modal-dialog-centered" style={{maxWidth: "480px"}}>
                    <div className="modal-content modal_detail_kpr modal-ml" style={{padding: "20px"}}>
                        <div className="close-modal" data-bs-dismiss="modal">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                />
                            </svg>
                        </div>
                        <h5 className="modal-title">Progress Status Pengajuan</h5>
                        <div class="container">
                            <div style={{display:"flex", justifyContent: "center", alignItems: "center", padding: "10px"}}>
                                {trackPengajuan?.length > 0 ? (
                                    <div className="step-progress"> 
                                        {[...(trackPengajuan.filter((data, i) => data.ID_GROUPTRACK !== trackPengajuan[i-1]?.ID_GROUPTRACK))].reverse().map((data,index) => {
                                            if(data.CODE_TRACKING === "BP1.0.0"){
                                                return(
                                                    <li className="step-item">
                                                        <div className="step-line">
                                                            <div className="step-icon">
                                                                <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <g clip-path="url(#clip0_3660_577)">
                                                                        <path d="M9.50093 1.58355C10.5511 1.587 11.5901 1.79868 12.5579 2.20634C13.5257 2.61401 14.4031 3.20957 15.1392 3.95855H12.6676C12.4576 3.95855 12.2563 4.04196 12.1078 4.19042C11.9593 4.33889 11.8759 4.54025 11.8759 4.75022C11.8759 4.96018 11.9593 5.16154 12.1078 5.31001C12.2563 5.45848 12.4576 5.54188 12.6676 5.54188H15.9475C16.3373 5.54167 16.7111 5.38672 16.9868 5.11106C17.2624 4.83541 17.4174 4.4616 17.4176 4.07176V0.791885C17.4176 0.581922 17.3342 0.380558 17.1857 0.232092C17.0373 0.083626 16.8359 0.000218472 16.6259 0.000218472C16.416 0.000218472 16.2146 0.083626 16.0661 0.232092C15.9177 0.380558 15.8343 0.581922 15.8343 0.791885V2.43697C14.5258 1.25853 12.9151 0.467626 11.1825 0.152811C9.44995 -0.162003 7.66397 0.0117071 6.02455 0.654489C4.38514 1.29727 2.95703 2.38374 1.90015 3.79224C0.843272 5.20074 0.199358 6.87564 0.0405168 8.62938C0.0302911 8.73963 0.0431417 8.85079 0.0782499 8.9558C0.113358 9.0608 0.169954 9.15734 0.244431 9.23927C0.318909 9.3212 0.409635 9.38671 0.510828 9.43163C0.612022 9.47656 0.721465 9.49992 0.832183 9.50021C1.02582 9.50268 1.2134 9.4328 1.35821 9.30423C1.50303 9.17567 1.59465 8.99769 1.61514 8.80513C1.79138 6.83448 2.69812 5.00094 4.15725 3.66471C5.61637 2.32849 7.52241 1.58616 9.50093 1.58355Z" fill="#424242"/>
                                                                        <path d="M18.1702 9.50006C17.9766 9.4976 17.789 9.56748 17.6442 9.69604C17.4993 9.82461 17.4077 10.0026 17.3872 10.1951C17.2564 11.7019 16.696 13.1394 15.7728 14.3373C14.8495 15.5352 13.6019 16.4431 12.1782 16.9533C10.7544 17.4635 9.21422 17.5546 7.74026 17.2157C6.2663 16.8769 4.92046 16.1224 3.8624 15.0417H6.33399C6.54395 15.0417 6.74531 14.9583 6.89378 14.8099C7.04224 14.6614 7.12565 14.46 7.12565 14.2501C7.12565 14.0401 7.04224 13.8387 6.89378 13.6903C6.74531 13.5418 6.54395 13.4584 6.33399 13.4584H3.05411C2.86102 13.4583 2.6698 13.4963 2.49139 13.5701C2.31298 13.6439 2.15088 13.7522 2.01434 13.8888C1.87781 14.0253 1.76952 14.1874 1.69568 14.3658C1.62184 14.5442 1.58388 14.7354 1.58398 14.9285V18.2084C1.58398 18.4184 1.66739 18.6197 1.81586 18.7682C1.96432 18.9167 2.16569 19.0001 2.37565 19.0001C2.58561 19.0001 2.78698 18.9167 2.93544 18.7682C3.08391 18.6197 3.16732 18.4184 3.16732 18.2084V16.5633C4.47581 17.7418 6.08652 18.5327 7.81907 18.8475C9.55163 19.1623 11.3376 18.9886 12.977 18.3458C14.6164 17.703 16.0446 16.6165 17.1014 15.208C18.1583 13.7995 18.8022 12.1246 18.9611 10.3709C18.9713 10.2607 18.9584 10.1495 18.9233 10.0445C18.8882 9.93947 18.8316 9.84293 18.7572 9.76101C18.6827 9.67908 18.592 9.61357 18.4908 9.56864C18.3896 9.52372 18.2801 9.50036 18.1694 9.50006H18.1702Z" fill="#424242"/>
                                                                    </g>
                                                                    <defs>
                                                                        <clipPath id="clip0_3660_577">
                                                                            <rect width="19" height="19" fill="white"/>
                                                                        </clipPath>
                                                                    </defs>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div className="step-content">
                                                            <span className="step-title">Proses Pengajuan Kredit</span>
                                                            <span className="step-description">Pengajuan kredit kamu sedang diproses oleh Bank BTN</span>
                                                            <span className="step-date">{data.AP_LASTTRDATE ? data.AP_LASTTRDATE : "-"}</span>
                                                        </div>
                                                    </li>
                                                )
                                            } else if(data.CODE_TRACKING === "BP2.0.0"){
                                                return(
                                                    <li className="step-item">
                                                        <div className="step-line">
                                                            <div className="step-icon">
                                                                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <g clip-path="url(#clip0_3660_584)">
                                                                        <path d="M17.0931 2.73177L15.6441 1.28102C15.2387 0.873541 14.7565 0.550514 14.2254 0.330631C13.6943 0.110748 13.1249 -0.00162399 12.5501 1.77323e-05H7C5.8401 0.00140711 4.72811 0.46279 3.90794 1.28296C3.08777 2.10313 2.62639 3.21512 2.625 4.37502V16.625C2.62639 17.7849 3.08777 18.8969 3.90794 19.7171C4.72811 20.5373 5.8401 20.9986 7 21H14C15.1599 20.9986 16.2719 20.5373 17.0921 19.7171C17.9122 18.8969 18.3736 17.7849 18.375 16.625V5.82489C18.3764 5.25018 18.2638 4.68089 18.0438 4.14996C17.8238 3.61903 17.5007 3.13701 17.0931 2.73177ZM15.8559 3.96902C15.98 4.09267 16.0911 4.22872 16.1875 4.37502H14V2.18752C14.1461 2.28492 14.2823 2.39628 14.4069 2.52002L15.8559 3.96902ZM16.625 16.625C16.625 17.3212 16.3484 17.9889 15.8562 18.4812C15.3639 18.9735 14.6962 19.25 14 19.25H7C6.30381 19.25 5.63613 18.9735 5.14384 18.4812C4.65156 17.9889 4.375 17.3212 4.375 16.625V4.37502C4.375 3.67883 4.65156 3.01115 5.14384 2.51886C5.63613 2.02658 6.30381 1.75002 7 1.75002H12.25V4.37502C12.25 4.83915 12.4344 5.28427 12.7626 5.61246C13.0907 5.94064 13.5359 6.12502 14 6.12502H16.625V16.625ZM14 7.87502C14.2321 7.87502 14.4546 7.96721 14.6187 8.1313C14.7828 8.2954 14.875 8.51796 14.875 8.75002C14.875 8.98208 14.7828 9.20464 14.6187 9.36874C14.4546 9.53283 14.2321 9.62502 14 9.62502H7C6.76793 9.62502 6.54538 9.53283 6.38128 9.36874C6.21719 9.20464 6.125 8.98208 6.125 8.75002C6.125 8.51796 6.21719 8.2954 6.38128 8.1313C6.54538 7.96721 6.76793 7.87502 7 7.87502H14ZM14.875 12.25C14.875 12.4821 14.7828 12.7046 14.6187 12.8687C14.4546 13.0328 14.2321 13.125 14 13.125H7C6.76793 13.125 6.54538 13.0328 6.38128 12.8687C6.21719 12.7046 6.125 12.4821 6.125 12.25C6.125 12.018 6.21719 11.7954 6.38128 11.6313C6.54538 11.4672 6.76793 11.375 7 11.375H14C14.2321 11.375 14.4546 11.4672 14.6187 11.6313C14.7828 11.7954 14.875 12.018 14.875 12.25ZM14.707 15.2364C14.843 15.4235 14.8994 15.6569 14.8638 15.8855C14.8282 16.114 14.7036 16.3192 14.5171 16.4561C13.6306 17.0879 12.5806 17.4503 11.4931 17.5C10.8578 17.497 10.2417 17.2813 9.74312 16.8875C9.45612 16.6906 9.34675 16.625 9.13062 16.625C8.54562 16.7155 7.99366 16.9548 7.52762 17.3198C7.34278 17.4515 7.11396 17.5063 6.88948 17.4725C6.665 17.4387 6.46244 17.319 6.32457 17.1387C6.1867 16.9584 6.12432 16.7315 6.15059 16.506C6.17686 16.2805 6.28974 16.0741 6.46537 15.9303C7.23641 15.3317 8.16224 14.9656 9.13412 14.875C9.71699 14.8843 10.2805 15.0858 10.7371 15.4481C10.9453 15.6354 11.2133 15.7424 11.4931 15.75C12.2086 15.6964 12.8966 15.452 13.4855 15.0421C13.6733 14.906 13.9074 14.85 14.1364 14.8865C14.3655 14.9229 14.5707 15.0487 14.707 15.2364Z" fill="#636363"/>
                                                                    </g>
                                                                    <defs>
                                                                        <clipPath id="clip0_3660_584">
                                                                            <rect width="21" height="21" fill="white"/>
                                                                        </clipPath>
                                                                    </defs>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div className="step-content">
                                                            <span className="step-title">Proses Verifikasi Data</span>
                                                            <span className="step-description">Pengajuan kredit kamu sedang dalam proses verifikasi data</span>
                                                            <span className="step-date">{data.AP_LASTTRDATE ? data.AP_LASTTRDATE : "-"}</span>
                                                        </div>
                                                    </li>
                                                )
                                            } else if(data.CODE_TRACKING === "BP3.0.0" || data.CODE_TRACKING === "BP9.0.0"){
                                                return(
                                                    <li className="step-item green">
                                                        <div className="step-line">
                                                            <div className="step-icon">
                                                                <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <g clip-path="url(#clip0_3660_590)">
                                                                        <path d="M21.3898 4.24638L8.14662 17.4886C8.05758 17.578 7.95177 17.649 7.83525 17.6973C7.71873 17.7457 7.59381 17.7707 7.46764 17.7707C7.34147 17.7707 7.21655 17.7457 7.10003 17.6973C6.98351 17.649 6.8777 17.578 6.78866 17.4886L1.66733 12.3625C1.57829 12.2731 1.47248 12.2022 1.35596 12.1538C1.23944 12.1054 1.11452 12.0805 0.98835 12.0805C0.862182 12.0805 0.737255 12.1054 0.620739 12.1538C0.504222 12.2022 0.398409 12.2731 0.309371 12.3625C0.219981 12.4515 0.149052 12.5574 0.100655 12.6739C0.0522572 12.7904 0.0273437 12.9153 0.0273438 13.0415C0.0273437 13.1677 0.0522572 13.2926 0.100655 13.4091C0.149052 13.5256 0.219981 13.6314 0.309371 13.7205L5.43262 18.8428C5.97307 19.3822 6.70548 19.6852 7.46908 19.6852C8.23268 19.6852 8.96509 19.3822 9.50554 18.8428L22.7478 5.60338C22.837 5.51436 22.9078 5.40861 22.9561 5.29218C23.0045 5.17575 23.0293 5.05094 23.0293 4.92488C23.0293 4.79883 23.0045 4.67401 22.9561 4.55758C22.9078 4.44116 22.837 4.3354 22.7478 4.24638C22.6587 4.15699 22.5529 4.08606 22.4364 4.03767C22.3199 3.98927 22.195 3.96436 22.0688 3.96436C21.9426 3.96436 21.8177 3.98927 21.7012 4.03767C21.5847 4.08606 21.4789 4.15699 21.3898 4.24638Z" fill="white"/>
                                                                    </g>
                                                                    <defs>
                                                                        <clipPath id="clip0_3660_590">
                                                                            <rect width="23" height="23" fill="white"/>
                                                                        </clipPath>
                                                                    </defs>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div className="step-content">
                                                            <span className="step-title">{data.STATUS_TRACKING}</span>
                                                            <span className="step-description">Selamat, pengajuan kredit kamu telah disetujui</span>
                                                            <span className="step-date">{data.AP_LASTTRDATE ? data.AP_LASTTRDATE : "-"}</span>
                                                        </div>
                                                    </li>
                                                )
                                            } else if(data.CODE_TRACKING === "BP4.0.0"){
                                                return(
                                                    <li className="step-item blue">
                                                        <div className="step-line">
                                                            <div className="step-icon">
                                                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <g clip-path="url(#clip0_3660_592)">
                                                                        <path d="M19.2675 7.55727L12.9467 1.2356C12.1644 0.455591 11.1047 0.0175781 10 0.0175781C8.89529 0.0175781 7.83564 0.455591 7.05334 1.2356L0.73251 7.55727C0.499531 7.78875 0.314818 8.06418 0.189079 8.36759C0.0633394 8.67099 -0.000925334 8.99634 1.00662e-05 9.32477V17.5056C1.00662e-05 18.1686 0.263402 18.8045 0.732243 19.2734C1.20108 19.7422 1.83697 20.0056 2.50001 20.0056H17.5C18.163 20.0056 18.7989 19.7422 19.2678 19.2734C19.7366 18.8045 20 18.1686 20 17.5056V9.32477C20.0009 8.99634 19.9367 8.67099 19.8109 8.36759C19.6852 8.06418 19.5005 7.78875 19.2675 7.55727ZM12.5 18.3389H7.50001V15.0606C7.50001 14.3976 7.7634 13.7617 8.23224 13.2928C8.70108 12.824 9.33697 12.5606 10 12.5606C10.6631 12.5606 11.2989 12.824 11.7678 13.2928C12.2366 13.7617 12.5 14.3976 12.5 15.0606V18.3389ZM18.3333 17.5056C18.3333 17.7266 18.2455 17.9386 18.0893 18.0949C17.933 18.2511 17.721 18.3389 17.5 18.3389H14.1667V15.0606C14.1667 13.9555 13.7277 12.8957 12.9463 12.1143C12.1649 11.3329 11.1051 10.8939 10 10.8939C8.89494 10.8939 7.83513 11.3329 7.05373 12.1143C6.27233 12.8957 5.83334 13.9555 5.83334 15.0606V18.3389H2.50001C2.279 18.3389 2.06703 18.2511 1.91075 18.0949C1.75447 17.9386 1.66668 17.7266 1.66668 17.5056V9.32477C1.66745 9.10391 1.75517 8.89225 1.91084 8.7356L8.23168 2.41643C8.70143 1.94887 9.33723 1.68639 10 1.68639C10.6628 1.68639 11.2986 1.94887 11.7683 2.41643L18.0892 8.7381C18.2442 8.89414 18.3319 9.10478 18.3333 9.32477V17.5056Z" fill="white"/>
                                                                    </g>
                                                                    <defs>
                                                                        <clipPath id="clip0_3660_592">
                                                                            <rect width="20" height="20" fill="white"/>
                                                                        </clipPath>
                                                                    </defs>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div className="step-content">
                                                            <span className="step-title">Realisasi Kredit</span>
                                                            <span className="step-description">Selamat, sekarang kamu sudah memiliki hunian idaman kamu</span>
                                                            <span className="step-date">{data.AP_LASTTRDATE ? data.AP_LASTTRDATE : "-"}</span>
                                                        </div>
                                                    </li>
                                                )
                                            } else if(data.CODE_TRACKING === "BP9.1.0"){
                                                return(
                                                    <li className="step-item yellow">
                                                        <div className="step-line">
                                                            <div className="step-icon yellow">
                                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <g clip-path="url(#clip0_3660_1746)">
                                                                        <path d="M12 0C9.62663 0 7.30655 0.703788 5.33316 2.02236C3.35977 3.34094 1.8217 5.21508 0.913451 7.4078C0.00519943 9.60051 -0.232441 12.0133 0.230582 14.3411C0.693605 16.6689 1.83649 18.807 3.51472 20.4853C5.19295 22.1635 7.33115 23.3064 9.65892 23.7694C11.9867 24.2324 14.3995 23.9948 16.5922 23.0865C18.7849 22.1783 20.6591 20.6402 21.9776 18.6668C23.2962 16.6935 24 14.3734 24 12C23.9966 8.81846 22.7312 5.76821 20.4815 3.51852C18.2318 1.26883 15.1815 0.00344108 12 0V0ZM12 2C14.3065 1.99816 16.5419 2.79775 18.324 4.262L4.26201 18.324C3.06257 16.8598 2.30366 15.0851 2.07362 13.2064C1.84358 11.3277 2.15186 9.42234 2.96259 7.71204C3.77332 6.00173 5.05314 4.55688 6.65308 3.54564C8.25303 2.53441 10.1073 1.9984 12 2V2ZM12 22C9.69353 22.0018 7.45808 21.2022 5.67601 19.738L19.738 5.676C20.9374 7.14016 21.6963 8.91488 21.9264 10.7936C22.1564 12.6723 21.8481 14.5777 21.0374 16.288C20.2267 17.9983 18.9469 19.4431 17.3469 20.4544C15.747 21.4656 13.8927 22.0016 12 22Z" fill="white"/>
                                                                    </g>
                                                                    <defs>
                                                                        <clipPath id="clip0_3660_1746">
                                                                        <rect width="24" height="24" fill="white"/>
                                                                        </clipPath>
                                                                    </defs>
                                                                </svg>

                                                            </div>
                                                        </div>
                                                        <div className="step-content">
                                                            <span className="step-title">Pembatalan Proses</span>
                                                            <span className="step-description">Pengajuan kredit kamu dibatalkan prosesnya</span>
                                                            <span className="step-date">{data.AP_LASTTRDATE ? data.AP_LASTTRDATE : "-"}</span>
                                                        </div>
                                                    </li>
                                                )
                                            } else { // data.STATUS === 5
                                                return(
                                                    <li className="step-item red">
                                                        <div className="step-line">
                                                            <div className="step-icon">
                                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M0.268776 0.268388C0.440678 0.0965391 0.673794 0 0.916861 0C1.15993 0 1.39304 0.0965391 1.56495 0.268388L11.0002 9.70364L20.4354 0.268388C20.6073 0.0965391 20.8405 0 21.0835 0C21.3266 0 21.5597 0.0965391 21.7316 0.268388C21.9035 0.440288 22 0.673404 22 0.916471C22 1.15954 21.9035 1.39265 21.7316 1.56455L12.2964 10.9998L21.7316 20.4351C21.9035 20.607 22 20.8401 22 21.0831C22 21.3262 21.9035 21.5593 21.7316 21.7312C21.5597 21.9031 21.3266 21.9996 21.0835 21.9996C20.8405 21.9996 20.6073 21.9031 20.4354 21.7312L11.0002 12.296L1.56495 21.7312C1.39304 21.9031 1.15993 21.9996 0.916861 21.9996C0.673794 21.9996 0.440678 21.9031 0.268776 21.7312C0.0969276 21.5593 0.000389099 21.3262 0.000389099 21.0831C0.000389099 20.8401 0.0969276 20.607 0.268776 20.4351L9.70403 10.9998L0.268776 1.56455C0.0969276 1.39265 0.000389099 1.15954 0.000389099 0.916471C0.000389099 0.673404 0.0969276 0.440288 0.268776 0.268388Z" fill="white"/>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div className="step-content">
                                                            <span className="step-title">Penolakan Kredit</span>
                                                            <span className="step-description">Mohon maaf, pengajuan kredit kamu belum disetujui</span>
                                                            <span className="step-date">{data.AP_LASTTRDATE ? data.AP_LASTTRDATE : "-"}</span>
                                                        </div>
                                                    </li>
                                                )
                                            }
                                        })}
                                    </div> 
                                ) : (
                                    <div>
                                        <div className="text-center" style={{marginTop:"100px"}}>
                                            <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M53.3337 3.33398H13.3337C9.66699 3.33398 6.66699 6.33398 6.66699 10.0007V56.6673H13.3337V10.0007H53.3337V3.33398ZM50.0003 16.6673L70.0003 36.6673V70.0006C70.0003 73.6673 67.0003 76.6673 63.3337 76.6673H26.6337C22.967 76.6673 20.0003 73.6673 20.0003 70.0006L20.0337 23.334C20.0337 19.6673 23.0003 16.6673 26.667 16.6673H50.0003ZM46.667 40.0006H65.0003L46.667 21.6673V40.0006Z" fill="#0061A7" fill-opacity="0.75"/>
                                            </svg>
                                            <h5 style={{fontFamily: "Helvetica",
                                                fontStyle: "normal",
                                                fontWeight: "bold",
                                                fontSize: "16px",
                                                lineHeight: "150%",
                                                marginTop:"40px",
                                                color: "#000000"}}>
                                                    Kamu belum memiliki status pengajuan
                                                </h5>
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div 
                                // style={{ marginLeft: "30px", paddingTop: "20px"}}
                                className="botton-modal-kpr">
                                <a className="btn btn-main" data-bs-dismiss="modal" style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  width: 386, height: 42.16 }}>
                                    OK
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* ------------------------------ Modal Persyaratan ------------------------------ */}
            <div className="modal fade" id="modal-persyaratan" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica",zIndex:"9999" }}>
                <div className="modal-dialog modal-dialog-centered m-auto px-2" style={{maxWidth: "480px"}}>
                    <div className="modal-content modal_detail_kpr mb-4" style={{padding: "20px", minHeight: "540px", marginTop:"85px"}}>
                        <div className="close-modal" data-bs-dismiss="modal">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                />
                            </svg>
                        </div>
                        <h5 className="modal-title">Dokumen Persyaratan KPR</h5>
                        <p>Syarat - syarat KPR adalah kelengkapan administratif yang harus dipenuhi saat mengajukan KPR:</p>
                        <div class="container">
                            <div className="">
                                <div className="list-group-kpr">
                                    <ul className="list-group menu_member mb-0">
                                        <li className="list-group-item list-blue" style={{textAlign: "center", height: "37px"}}>
                                            <label>Checklist Dokumen</label>
                                        </li>
                                        <li className="list-group-item d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        KTP Pemohon 
                                        </div>
                                        <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.ktp_pemohon && <Ceklist/>} </div>
                                        </li>
                                        <li className="list-group-item list-grey d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        KTP Pasangan
                                        </div>
                                         <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.ktp_pasangan && <Ceklist/>}</div>
                                        </li>
                                        <li className="list-group-item d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Kartu Keluarga
                                        </div>
                                         <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.kartu_keluarga && <Ceklist/>}</div>
                                        </li>
                                        <li className="list-group-item list-grey d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Surat Nikah / Cerai 
                                        </div>
                                        <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.surat_nikah && <Ceklist/>}</div>
                                        </li>
                                        <li className="list-group-item d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        NPWP/SPT Pph 21	
                                        </div>
                                        <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.npwp && <Ceklist/>}</div>
                                        </li>
                                        <li className="list-group-item list-grey d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Rekening Tabungan/Tabungan 3 Bulan Terakhir 
                                        </div>
                                        <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.rekening && <Ceklist/>}</div>
                                        </li>
                                        <li className="list-group-item d-flex">
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Surat Keterangan Pengangkatan Pegawai Tetap
                                        </div>
                                        <div className="col-4 justify-content-end d-flex" style={{alignItems:"center"}}><ul className="text-end">Khusus Karyawan</ul></div>
                                        </li>
                                        <li className="list-group-item list-grey d-flex" >
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Surat Izin Praktek
                                        </div>
                                         <div className="col-4 justify-content-end d-flex" style={{alignItems:"center"}}><ul className="text-end">Khusus Profesional</ul></div>
                                        </li>
                                        <li className="list-group-item d-flex" >
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Surat Izin Usaha (Akta Pendirian Perusahaan, SIUP, TDP)
                                        </div>
                                         <div className="col-4 justify-content-end d-flex" style={{alignItems:"center"}}><ul className="text-end">Khusus Pengusaha</ul></div>
                                        </li>
                                        <li className="list-group-item list-grey d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Dokumen Penghasilan Pemohon 
                                        </div>
                                        <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.dok_penghasilan && <Ceklist/>}</div>
                                        </li>
                                        <li className="list-group-item d-flex" style={{minHeight: "25.24px"}}>
                                        <div className="col-8" style={{lineHeight:"normal"}}>
                                        Dokumen Penghasilan Pasangan
                                        </div>
                                         <div className="col-4 d-flex justify-content-end" style={{alignItems:"center"}}>{dokPersyaratan?.dok_penghasilan_pas && <Ceklist/>}</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                           <div classname="row mt-3 w-100" style={{textAlign:"center"}}>
                                <button className="btn btn-main w-100" data-bs-dismiss="modal" style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  maxWidth: 386, height: 42.16 }}>
                                    OK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            {/* ------------------------------ Modal Upload File ------------------------------ */}
            <div className="modal fade" id="modal-upload-doc" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica",zIndex:"9999" }}>
                <div className="modal-dialog modal-dialog-centered mx-auto" style={{maxWidth: "480px", width: "95%",}}>
                    <div className="modal-content modal_detail_kpr" style={{padding: "20px 45px 45px 45px", height: "417px"}}>
                        <div className="close-modal" data-bs-dismiss="modal">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                />
                            </svg>
                        </div>
                        <h5 className="modal-title">Upload Kelengkapan Dokumen</h5>
                        <div class="container">
                        <label style={{paddingBottom:"10px", fontFamily:"FuturaBT", fontWeight: 700, marginTop: "20px", color: "#00193E", fontSize: "14px"}}>
                            Pilih Jenis Dokumen
                        </label>
                        <select
                            className="floating-label-select custom-select form-select"
                            name="profil[i_prop]"
                            id="provinsi"
                            //required
                            //value={data?.i_prop}
                            onChange={(e) => {setJnsDokumen(e.target.value); setValidateImage(false)}}
                            style={{
                                fontFamily: "Helvetica",
                                fontWeight: 400,
                                paddingLeft: "20px",
                                width: "100%",
                            }}
                        >
                            <option value="">Pilih Jenis Dokumen</option>
                            {jenisKategori?.map((data,index) => (
                                <option value={data.id}>{data.nl}</option>
                            ))}
                        </select>
                        <label style={{ fontFamily:"FuturaBT", fontWeight: 700, marginTop: "10px", color: "#00193E", fontSize: "14px"}}>
                            Pilih Dokumen
                        </label>
                            <div style={{paddingTop: "10px"}}>
                                <div className="row">
                                    <div className="col-sm-4">
                                        <div id="btn-tambah-foto">
                                            <label htmlFor="upload">
                                                <div className="btn btn-outline-main" style={{width: 107.55, height: 33.02, padding: "5px 20px", fontFamily: "Helvetica",fontWeight: 700}}>
                                                    Pilih File
                                                </div>
                                            </label>
                                            <input className="form-control" type="file" required="" accept="image/jpeg,image/jpg,image/png" multiple hidden id="upload" onChange={imageChange}/>
                                        </div>
                                    </div>
                                    <div className="col-sm-7">
                                        {selectedImage && (
                                            <div style={{paddingTop:"6px"}}>
                                                <label>{selectedImage.name}</label>
                                            </div>
                                        )}
                                        {validateImage && (
                                            <div className="error-input">
                                                <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
                                                {/* File tidak lebih dari 3MB */}
                                                {validateMessage}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div data-bs-dismiss="modal" ref={modalRef} style={{ display: 'none' }} />
                            <div style={{ marginTop:"55px"}}>
                                <a className="btn btn-main" onClick={()=> uploadImage()} style={{ width:"100%", fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  maxWidth: 386, height: 42.16 }}>
                                    Submit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {modalSucces && (
              <div className={styles.boxModal} id="modal-upload-doc" style={{zIndex:"9999"}}>
                <div className={styles.boxModal__Modal} style={{top:"50%"}}>
                  <div className="text-center">
                    <button
                      type="button"
                      style={{ background: "#ffffff", border: "none",width:"100%",textAlign:"end" }}
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      data-bs-target="#modalSuccessRegister"
                      onClick={() => {
                        setModalSucces(false);
                        router.reload();
                      }}
                    >
                      <svg style={{ transform:"translateX(32px)" }} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                          fill="#0061A7"
                        />
                      </svg>
                    </button>
                    <h4 className="modal-title mb-3">Terima Kasih</h4>

                    {/* {loading && <div className="spinner-border text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />} */}
                    <Lottie options={defaultOptions} height={250} max-width={350} isStopped={false} isPaused={false} />
                    <div className="desc text-center mb-5" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                      Data berhasil disimpan.
                    </div>
                    <div className={`button-kembali ${styles.boxModal__Modal__Footer}`}>
                      <Link href="/">
                        <button
                          type="buttom"
                          className="btn btn-main form-control btn-back px-5"
                          style={{
                            maxWidth: "490px",
                            height: "48px",
                            fontSize: "14px",
                            fontFamily: "Helvetica",
                            fontWeight: 700,
                            backgroundColor: "#0061A7",
                          }}
                        >
                          Kembali ke Beranda
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {/* ------------------------------ Modal update delete ------------------------------ */}
            <div className="modal fade" id="modal-update" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica" }}>
                <div id="dialog-update-modal" className="modal-dialog modal-dialog-centered" style={{maxWidth: "480px"}}>
                    <div className="modal-content modal_detail_kpr" style={{padding: "45px", minHeight: "300px"}}>
                        <div className="close-modal" data-bs-dismiss="modal">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                />
                            </svg>
                        </div>
                        <h5 className="modal-title">Perbarui dan hapus Dokumen</h5>
                        <div class="container">
                            <div  style={{textAlign: "center"}}>
                                <label>
                                    {dokSelect?.file?.split("/")[2] ? dokSelect.file?.split("/")[2] : "-"}
                                </label>
                            </div>
                            <div 
                                className="d-flex justify-content-center align-items-center" 
                                style={{ marginTop: "20px", width: "100%" }}
                            >
                                <div style={{ width: "100px", height: "100px" }}>
                                    <div className={"ratio ratio-1x1 photo_input"}>
                                        <img 
                                            data-bs-toggle="modal" data-bs-target="#modal-image-preview" 
                                            src={selectedImageUrl}
                                            alt="berkas KPR"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div data-bs-dismiss="modal" ref={modalUpdate} style={{ display: 'none' }} />
                            <div style={{paddingTop: "30px", marginLeft: "0px"}}>
                                <a data-bs-toggle="modal" data-bs-target="#modal-update-doc" className="btn btn-main" onClick={()=> {modalUpdate?.current?.click()}} style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  width: 386, height: 42.16 }}>
                                    Perbarui
                                </a>
                            </div>
                            <div style={{paddingTop: "30px", marginLeft: "0px"}}>
                                <a data-bs-toggle="modal" data-bs-target="#modal-delete-doc" className="btn btn-main" onClick={()=> {modalUpdate?.current?.click()}} style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  width: 386, height: 42.16 }}>
                                    Hapus
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* ------------------------------ Modal delete confirmation ------------------------------ */}
            <div className="modal fade" id="modal-delete-doc" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica" }}>
                <div className="modal-dialog modal-dialog-centered" style={{maxWidth: "480px"}}>
                    <div className="modal-content modal_detail_kpr" style={{padding: "45px", height: "300px"}}>
                        <div className="close-modal" data-bs-dismiss="modal">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                />
                            </svg>
                        </div>
                        <h5 className="modal-title">Apakah anda yakin?</h5>
                        <div class="container">
                        
                            <div data-bs-dismiss="modal" ref={modalDelete} style={{ display: 'none' }} />
                            <div className="row" style={{textAlign: "center"}}>
                                <div className="col">
                                    <div style={{paddingTop: "30px", marginLeft: "0px"}}>
                                        <a className="btn btn-main" onClick={()=> deleteImage()} style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  width: 150, height: 42.16 }}>
                                            Ya
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div style={{paddingTop: "30px", marginLeft: "0px"}}>
                                        <a data-bs-dismiss="modal" className="btn btn-main" style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  width: 150, height: 42.16 }}>
                                            Tidak
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* ------------------------------ Modal update File ------------------------------ */}
            <div className="modal fade" id="modal-update-doc" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica" }}>
                <div className="modal-dialog modal-dialog-centered" style={{maxWidth: "480px"}}>
                    <div className="modal-content modal_detail_kpr" style={{padding: "20px 45px 45px 45px", height: "417px"}}>
                        <div className="close-modal" data-bs-dismiss="modal">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                />
                            </svg>
                        </div>
                        <h5 className="modal-title">Perbarui Dokumen</h5>
                        <div class="container">
                        <label style={{paddingBottom:"10px", fontFamily:"FuturaBT", fontWeight: 700, marginTop: "20px", color: "#00193E", fontSize: "14px"}}>
                            Jenis Dokumen
                        </label>
                        <input
                            type="text"
                            style={{ height: "43.4px", padding: "15px" }}
                            className="floating-label-field"
                            name="portoffolio[n]"
                            placeholder="Pilih Jenis Dokumen"
                            value={dokSelect?.jenis}
                            disabled
                        />
                        <label style={{ fontFamily:"FuturaBT", fontWeight: 700, marginTop: "10px", color: "#00193E", fontSize: "14px"}}>
                            Pilih Dokumen
                        </label>
                            <div style={{paddingTop: "10px"}}>
                                <div className="row">
                                    <div className="col-sm-4">
                                        <div id="btn-tambah-foto">
                                            <label htmlFor="upload">
                                                <div className="btn btn-outline-main" style={{width: 107.55, height: 33.02, padding: "5px 20px", fontFamily: "Helvetica",fontWeight: 700}}>
                                                    Pilih File
                                                </div>
                                            </label>
                                            <input className="form-control" type="file" required="" accept="image/jpeg,image/jpg,image/png" multiple hidden id="upload" onChange={imageChange}/>
                                        </div>
                                    </div>
                                    <div className="col-sm-7">
                                        {selectedImage && (
                                            <div style={{paddingTop:"6px"}}>
                                                <label>{selectedImage.name}</label>
                                            </div>
                                        )}
                                        {validateImage && (
                                            <div className="error-input">
                                                <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
                                                {/* File tidak lebih dari 3MB */}
                                                {validateMessage}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div data-bs-dismiss="modal" ref={modalUpload} style={{ display: 'none' }} />
                            <div style={{position:"absolute", marginLeft: "0px", bottom: "68.84px"}}>
                                <a className="btn btn-main" onClick={()=> updateImage(selectedImage.jenis)} style={{ fontSize: "14px" ,padding: "10px 20px" ,fontWeight: 700,  width: 386, height: 42.16 }}>
                                    Submit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* -------------------------------------------------- Modal Preview Image ------------------------------------------------------------------- */}

            <div className="modal fade" id="modal-image-preview" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style={{ fontFamily: "Helvetica" }}>
                <div className="modal-dialog modal-dialog-centered modal-preview-img" >
                    <div className="modal-content" >
                        <div class="modal-body modal-xl">
                            <img src={selectedImageUrl} className="img-fluid" />
                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    
    )} </>);
};



export default withAuth(index);