import Layout from "../../../components/Layout";
import verifiedMember from "../../../helpers/verifiedMember";
import withAuth from "../../../helpers/withAuth";
import Breadcrumb from "../../../components/section/Breadcrumb";
import { useEffect, useState } from "react";
import MenuMember from "../../../components/MenuMember";
import { useRouter } from "next/router";
import { useAppContext } from "../../../context";
import { newBase64 } from "../../../public/resume/newBase64";
import download from "downloadjs";
import { PDFDocument } from "pdf-lib";
import rupiahFormater from "../../../helpers/rupiahFormater";
import Lottie from "react-lottie";
import * as animationData from "../../../public/x_animation.json";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const DetailOther = (props) => {
  const {detailKprData,dokPersyaratan,trackPengajuan,claimed} = props;
  const router = useRouter();
  const { id } = router.query;
  const { userProfile } = useAppContext();
  const { email } = userProfile || {};

  const formattedDate = (value) =>{
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const dateFormat = new Date(value || "-");
    const birthDate = dateFormat.toLocaleDateString("id", options);
    if(birthDate == 'Invalid Date') return '-'
    return birthDate
  }
  


  return (
    <Layout>
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        {claimed ? <PopUpError/>:<></>}
        <div className="container">
          <div id="detail_ajukan_kpr">
            <div className="row">
              <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
                <MenuMember active="kredit" />
              </div>
              <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                {detailKprData ? (
                  /////////////////--------detail goes here-----------/////////////

                  <>
                    <h5 className="title_akun">Pengajuan KPR</h5>
                    <div className="dashboard-profil revamp-card-developer ">
                      <div className="tab-menu revamp-card-imgdev ">
                        <div id="card_detail_pengajuan">
                          <div className="card card_pengajuan">
                            <div className="card-header ">
                              <div
                                className="kode"
                                style={{
                                  fontFamily: "FuturaBT",
                                  fontWeight: "700",
                                }}
                              >
                                {detailKprData?.ID ? detailKprData.ID : "-"}{" "}
                                &middot;{" "}
                                <span>
                                  <div className="break-line">
                                    <br />
                                  </div>
                                  {detailKprData?.TGL_DIAJUKAN
                                    ? detailKprData.TGL_DIAJUKAN
                                    : "-"}
                                </span>
                              </div>
                            </div>
                            <div className="card-body">
                              {/* <div className="row g-md-0 custom-gap" style={{paddingTop: "10px"}}>
                                                    <div className="col-md-5 col-sm-3 item" style={{maxWidth: 200}}>
                                                        <a href="/resume/resume_kpr.pdf" className="btn btn-main" style={{fontSize: "14px" ,padding: "13px 20px" ,fontWeight: 700,  width: 179, height: 48 }} download>
                                                            Generate From KPR
                                                        </a>
                                                    </div>
                                                    <div className="col-md-4 col-sm-4 item">
                                                        <div className="btn btn-outline-main btn_rounded" data-bs-toggle="modal" data-bs-target="#modal-persyaratan" style={{width: 257,height: 48, alignItems: "center", justifyContent: "center",fontFamily: "Helvetica",fontWeight: 700}}> */}
                              <div
                                className="row g-md-0"
                                style={{ paddingTop: "10px" }}
                              >
                                <div
                                  className="col-6 col-md-5 col-lg-3 ps-0 pe-1 item"
                                  style={{ maxWidth: "186px" }}
                                >
                                  <button
                                    className="btn btn-main w-100 d-flex justify-content-center align-items-center"
                                    style={{
                                      fontSize: "14px",
                                      padding: "13px 20px",
                                      fontWeight: 700,
                                      maxWidth: 180,
                                      height: 48,
                                      fontFamily: "Helvetica",
                                      fontWeight: 700,
                                    }}
                                    // onClick={() => generateFormPdf()}
                                    disabled
                                  >
                                    Generate Form KPR
                                  </button>
                                </div>
                                <div className="col-6 col-md-7 col-lg-5 ps-1 pe-0 item">
                                  <button
                                    className="btn btn-outline-main btn_rounded w-100 d-flex align-items-center"
                                    data-bs-toggle="modal"
                                    data-bs-target="#modal-persyaratan"
                                    style={{
                                      maxWidth: 257,
                                      minHeight: 48,
                                      alignItems: "center",
                                      justifyContent: "center",
                                      fontFamily: "Helvetica",
                                      fontWeight: 700,
                                    }}
                                  >
                                    Dokumen Persyaratan KPR
                                  </button>
                                </div>
                                <div className="col item status-detail-pengajuan">
                                  <label
                                    id=""
                                    style={{
                                      fontSize: "14px",
                                      textAlign: "right",
                                    }}
                                  >
                                    Status Pengajuan
                                  </label>
                                  <div className="d-flex justify-content-between">
                                    <div className="col-6 text-start mt-auto">
                                    
                                    </div>
                                    <div className="col-6 text-end">
                                      <label
                                        style={{
                                          fontSize: "24px",
                                          textAlign: "right",
                                          fontWeight: 700,
                                          color: "#00193E",
                                        }}
                                      >
                                        {detailKprData.STATUS_PENGAJUAN || "-"}
                                      </label>
                                    </div>
                                  </div>
                                  <label
                                    className="label_hover_kpr"
                                    style={{
                                      width: "81px",
                                      fontSize: "12px",
                                      float: "right",
                                      fontWeight: 700,
                                      color: "#00193E",
                                    }}
                                    data-bs-toggle="modal"
                                    data-bs-target="#modal-detail"
                                  >
                                    Lihat Detail
                                    <svg
                                      style={{ verticalAlign: "-1px" }}
                                      width="15"
                                      height="10"
                                      viewBox="0 0 1 10"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                    >
                                      <path
                                        d="M0 10L5 5L-4.37114e-07 0L0 10Z"
                                        fill="#00193E"
                                      />
                                    </svg>
                                  </label>
                                </div>
                              </div>
                              <div
                                className="row g-md-0"
                                style={{ paddingBottom: "10px" }}
                              >
                                <label
                                  style={{
                                    fontSize: "24px",
                                    fontWeight: 700,
                                    color: "#8F8F8F",
                                  }}
                                >
                                  Data Kredit
                                </label>
                              </div>
                              <hr
                                style={{
                                  background: "#EEEEEE",
                                  height: 1,
                                  opacity: 1,
                                  marginTop: 0,
                                  marginBottom: 10,
                                }}
                              />
                              <div
                                className="row"
                                style={{ paddingTop: "10px" }}
                              >
                                <div className="col-6 item">
                                  <label>Jenis kredit</label>
                                  <strong>
                                    {detailKprData.PRODUK
                                      ? detailKprData.PRODUK
                                      : "-"}
                                  </strong>
                                </div>
                                <div className="col-6 item">
                                  <label>Kantor Cabang</label>
                                  <strong>
                                    {detailKprData?.CABANG
                                      ? detailKprData?.CABANG
                                      : "-"}
                                  </strong>
                                </div>
                                {/* <div className="col-6 item">
                                  <label>Channel</label>
                                  <strong>{detailKprData?.CHANNEL ? detailKprData?.CHANNEL : "-"}</strong>
                                </div> */}
                                <div className="col-6 item">
                                  <label>Developer</label>
                                  <strong>{detailKprData?.DEVELOPER ? detailKprData?.DEVELOPER : "-"}</strong>
                                </div>
                                <div className="col-6 item">
                                  <label>Nilai Diajukan</label>
                                  <strong>
                                  Rp {rupiahFormater(detailKprData?.NILAI_DIAJUKAN)}
                                   
                                  </strong>
                                </div>
                                <div className="col-6 item">
                                  <label>Nilai Disetujui</label>
                                  <strong>
                                  Rp {rupiahFormater(detailKprData?.NILAI_DISETUJUI)}
                                  </strong>
                                </div>
                                <div className="col-6 item">
                                  <label>Tanggal Pengajuan</label>
                                  <strong>
                                  {formattedDate(detailKprData?.TGL_DIAJUKAN)}
                                   
                                  </strong>
                                </div>
                              </div>
                              <div
                                className="row g-md-0"
                                style={{ padding: "20px 0 10px 0" }}
                              >
                                <label
                                  style={{
                                    fontSize: "24px",
                                    fontWeight: 700,
                                    color: "#8F8F8F",
                                  }}
                                >
                                  Data Diri
                                </label>
                              </div>
                              <hr
                                style={{
                                  background: "#EEEEEE",
                                  height: 1,
                                  opacity: 1,
                                  marginTop: 0,
                                  marginBottom: 10,
                                }}
                              />
                              <div
                                className="row"
                                style={{ paddingTop: "10px" }}
                              >
                                <div className="col-6 item">
                                  <label>Nama Lengkap</label>
                                  <strong>
                                    {detailKprData?.NAMA
                                      ? detailKprData?.NAMA
                                      : "-"}
                                  </strong>
                                </div>
                                <div className="col-6 item">
                                  <label>No. KTP</label>
                                  <strong>{detailKprData?.KTP
                                      ? detailKprData?.KTP
                                      : "-"}</strong>
                                </div>
                                <div className="col-6 item">
                                  <label>Tanggal Lahir</label>
                                  <strong>{formattedDate(detailKprData?.TGL_LAHIR)}
                                 </strong>
                                </div>
                                <div className="col-6 item">
                                  <label>No. Handphone</label>
                                  <strong>

                                    {detailKprData.NO_HP
                                      ? detailKprData.NO_HP
                                      : "-"}
                                  </strong>
                                </div>
                                </div>
                                <div className="row g-md-0" style={{padding: "20px 0 10px 0"}}>
                                                    <label style={{fontSize: "24px" , fontWeight: 700, color: "#8F8F8F"}}>Data Properti / Agunan</label>
                                                </div>
                                                <hr
                                                    style={{
                                                    background: "#EEEEEE",
                                                    height: 1,
                                                    opacity: 1,
                                                    marginTop: 0,
                                                    marginBottom: 10,
                                                    }}
                                                />
                                                <div className="row" style={{paddingTop: "10px"}}>
                                                <div className="col-6 item">
                                                  <label>Nama Properti</label>
                                                  <strong>{detailKprData?.PROPER ? detailKprData?.PROPER : "-"}</strong>
                                                </div>
                                                    <div className="col-6 item">
                                                        <label>Alamat Agunan</label>
                                                        <strong>
                                                          {detailKprData?.ALAMAT
                                                            ? detailKprData.ALAMAT
                                                            : "-"}
                                                        </strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Blok</label>
                                                        <strong>
                                                            {detailKprData?.BLOK
                                                              ? detailKprData.BLOK
                                                              : "-"}
                                                          </strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                      <label>Kavling</label>
                                                      <strong>
                                                        {detailKprData?.KAVLING
                                                          ? detailKprData.KAVLING
                                                          : "-"}
                                                      </strong>
                                                    </div>
                                                    {/* <div className="col-6 item">
                                                        <label>Nomor</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Provinsi</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kota</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kodepos</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kecamatan</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Kelurahan</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Dati II</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Status Bukti Kepemilikan</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>No. Sertifikat</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Tanggal Terbit</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Tanggal Jatuh Tempo</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Luas Tanah</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Luas Bangunan</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Atas Nama</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>NIP / NRP</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Nama Atasan</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Nomor IMB</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Tanggal Terbit IMB</label>
                                                        <strong>-</strong>
                                                    </div>
                                                    <div className="col-6 item">
                                                        <label>Jarak Agunan Ke Tempat Kerja</label>
                                                        <strong>-</strong>
                                                    </div> */}
                                                </div>

                              {/* <div
                                className="row g-md-0"
                                style={{ padding: "20px 0 10px 0" }}
                              >
                                <div className="col-md-6 col-5">
                                  <label id="label-dokumen">File Dokumen</label>
                                </div>
                                <div
                                  className="col-md-6 col-7"
                                  style={{ textAlign: "right" }}
                                >
                                  <button
                                    className="btn btn-main d-flex p-1 justify-content-center ms-auto w-100"
                                    style={{
                                      fontSize: "14px",
                                      padding: "13px 20px",
                                      fontWeight: 700,
                                      maxWidth: 219,
                                      height: 48,
                                      alignItems: "center",
                                      fontFamily: "Helvetica",
                                      fontWeight: 700,
                                    }}
                                    disabled
                                  >
                                    <div>Tambah File Dokumen</div>
                                  </button>
                                </div>
                              </div>
                              <hr
                                style={{
                                  background: "#EEEEEE",
                                  height: 1,
                                  opacity: 1,
                                  marginTop: 0,
                                  marginBottom: 10,
                                }}
                              />
                              <div
                                className="row g-md-0"
                                style={{ padding: "20px 0 50px 0" }}
                              >
                                <div className="col-3 col-md-1">
                                  <label>No.</label>
                                </div>
                                <div className="col-5 col-md-3 col-lg-3"></div>
                                <div className="pe-0 col-4 col-md-2">
                                  <label>File</label>-
                                </div>
                              </div> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal fade"
        id="modal-detail"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myLargeModalLabel"
        aria-hidden="true"
        style={{ fontFamily: "Helvetica" }}
      >
        <div
          id="lihat_detail_kpr"
          className="modal-dialog modal-dialog-centered"
          style={{ maxWidth: "480px" }}
        >
          <div
            className="modal-content modal_detail_kpr modal-ml"
            style={{ padding: "20px" }}
          >
            <div className="close-modal" data-bs-dismiss="modal">
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <h5 className="modal-title">Progress Status Pengajuan</h5>
            <div class="container">
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  padding: "10px",
                }}
              >
                {trackPengajuan?.length > 0 ? (
                  <div className="step-progress">
                    {[
                      ...trackPengajuan.filter(
                        (data, i) =>
                          data.ID_GROUPTRACK !==
                          trackPengajuan[i - 1]?.ID_GROUPTRACK
                      ),
                    ]
                      .reverse()
                      .map((data, index) => {
                        if (data.CODE_TRACKING === "BP1.0.0") {
                          return (
                            <li className="step-item">
                              <div className="step-line">
                                <div className="step-icon">
                                  <svg
                                    width="19"
                                    height="19"
                                    viewBox="0 0 19 19"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <g clip-path="url(#clip0_3660_577)">
                                      <path
                                        d="M9.50093 1.58355C10.5511 1.587 11.5901 1.79868 12.5579 2.20634C13.5257 2.61401 14.4031 3.20957 15.1392 3.95855H12.6676C12.4576 3.95855 12.2563 4.04196 12.1078 4.19042C11.9593 4.33889 11.8759 4.54025 11.8759 4.75022C11.8759 4.96018 11.9593 5.16154 12.1078 5.31001C12.2563 5.45848 12.4576 5.54188 12.6676 5.54188H15.9475C16.3373 5.54167 16.7111 5.38672 16.9868 5.11106C17.2624 4.83541 17.4174 4.4616 17.4176 4.07176V0.791885C17.4176 0.581922 17.3342 0.380558 17.1857 0.232092C17.0373 0.083626 16.8359 0.000218472 16.6259 0.000218472C16.416 0.000218472 16.2146 0.083626 16.0661 0.232092C15.9177 0.380558 15.8343 0.581922 15.8343 0.791885V2.43697C14.5258 1.25853 12.9151 0.467626 11.1825 0.152811C9.44995 -0.162003 7.66397 0.0117071 6.02455 0.654489C4.38514 1.29727 2.95703 2.38374 1.90015 3.79224C0.843272 5.20074 0.199358 6.87564 0.0405168 8.62938C0.0302911 8.73963 0.0431417 8.85079 0.0782499 8.9558C0.113358 9.0608 0.169954 9.15734 0.244431 9.23927C0.318909 9.3212 0.409635 9.38671 0.510828 9.43163C0.612022 9.47656 0.721465 9.49992 0.832183 9.50021C1.02582 9.50268 1.2134 9.4328 1.35821 9.30423C1.50303 9.17567 1.59465 8.99769 1.61514 8.80513C1.79138 6.83448 2.69812 5.00094 4.15725 3.66471C5.61637 2.32849 7.52241 1.58616 9.50093 1.58355Z"
                                        fill="#424242"
                                      />
                                      <path
                                        d="M18.1702 9.50006C17.9766 9.4976 17.789 9.56748 17.6442 9.69604C17.4993 9.82461 17.4077 10.0026 17.3872 10.1951C17.2564 11.7019 16.696 13.1394 15.7728 14.3373C14.8495 15.5352 13.6019 16.4431 12.1782 16.9533C10.7544 17.4635 9.21422 17.5546 7.74026 17.2157C6.2663 16.8769 4.92046 16.1224 3.8624 15.0417H6.33399C6.54395 15.0417 6.74531 14.9583 6.89378 14.8099C7.04224 14.6614 7.12565 14.46 7.12565 14.2501C7.12565 14.0401 7.04224 13.8387 6.89378 13.6903C6.74531 13.5418 6.54395 13.4584 6.33399 13.4584H3.05411C2.86102 13.4583 2.6698 13.4963 2.49139 13.5701C2.31298 13.6439 2.15088 13.7522 2.01434 13.8888C1.87781 14.0253 1.76952 14.1874 1.69568 14.3658C1.62184 14.5442 1.58388 14.7354 1.58398 14.9285V18.2084C1.58398 18.4184 1.66739 18.6197 1.81586 18.7682C1.96432 18.9167 2.16569 19.0001 2.37565 19.0001C2.58561 19.0001 2.78698 18.9167 2.93544 18.7682C3.08391 18.6197 3.16732 18.4184 3.16732 18.2084V16.5633C4.47581 17.7418 6.08652 18.5327 7.81907 18.8475C9.55163 19.1623 11.3376 18.9886 12.977 18.3458C14.6164 17.703 16.0446 16.6165 17.1014 15.208C18.1583 13.7995 18.8022 12.1246 18.9611 10.3709C18.9713 10.2607 18.9584 10.1495 18.9233 10.0445C18.8882 9.93947 18.8316 9.84293 18.7572 9.76101C18.6827 9.67908 18.592 9.61357 18.4908 9.56864C18.3896 9.52372 18.2801 9.50036 18.1694 9.50006H18.1702Z"
                                        fill="#424242"
                                      />
                                    </g>
                                    <defs>
                                      <clipPath id="clip0_3660_577">
                                        <rect
                                          width="19"
                                          height="19"
                                          fill="white"
                                        />
                                      </clipPath>
                                    </defs>
                                  </svg>
                                </div>
                              </div>
                              <div className="step-content">
                                <span className="step-title">
                                  Proses Pengajuan Kredit
                                </span>
                                <span className="step-description">
                                  Pengajuan kredit kamu sedang diproses oleh
                                  Bank BTN
                                </span>
                                <span className="step-date">
                                  {data.AP_LASTTRDATE
                                    ? data.AP_LASTTRDATE.slice(0, 16)
                                    : "-"}
                                </span>
                              </div>
                            </li>
                          );
                        } else if (data.CODE_TRACKING === "BP2.0.0") {
                          return (
                            <li className="step-item">
                              <div className="step-line">
                                <div className="step-icon">
                                  <svg
                                    width="21"
                                    height="21"
                                    viewBox="0 0 21 21"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <g clip-path="url(#clip0_3660_584)">
                                      <path
                                        d="M17.0931 2.73177L15.6441 1.28102C15.2387 0.873541 14.7565 0.550514 14.2254 0.330631C13.6943 0.110748 13.1249 -0.00162399 12.5501 1.77323e-05H7C5.8401 0.00140711 4.72811 0.46279 3.90794 1.28296C3.08777 2.10313 2.62639 3.21512 2.625 4.37502V16.625C2.62639 17.7849 3.08777 18.8969 3.90794 19.7171C4.72811 20.5373 5.8401 20.9986 7 21H14C15.1599 20.9986 16.2719 20.5373 17.0921 19.7171C17.9122 18.8969 18.3736 17.7849 18.375 16.625V5.82489C18.3764 5.25018 18.2638 4.68089 18.0438 4.14996C17.8238 3.61903 17.5007 3.13701 17.0931 2.73177ZM15.8559 3.96902C15.98 4.09267 16.0911 4.22872 16.1875 4.37502H14V2.18752C14.1461 2.28492 14.2823 2.39628 14.4069 2.52002L15.8559 3.96902ZM16.625 16.625C16.625 17.3212 16.3484 17.9889 15.8562 18.4812C15.3639 18.9735 14.6962 19.25 14 19.25H7C6.30381 19.25 5.63613 18.9735 5.14384 18.4812C4.65156 17.9889 4.375 17.3212 4.375 16.625V4.37502C4.375 3.67883 4.65156 3.01115 5.14384 2.51886C5.63613 2.02658 6.30381 1.75002 7 1.75002H12.25V4.37502C12.25 4.83915 12.4344 5.28427 12.7626 5.61246C13.0907 5.94064 13.5359 6.12502 14 6.12502H16.625V16.625ZM14 7.87502C14.2321 7.87502 14.4546 7.96721 14.6187 8.1313C14.7828 8.2954 14.875 8.51796 14.875 8.75002C14.875 8.98208 14.7828 9.20464 14.6187 9.36874C14.4546 9.53283 14.2321 9.62502 14 9.62502H7C6.76793 9.62502 6.54538 9.53283 6.38128 9.36874C6.21719 9.20464 6.125 8.98208 6.125 8.75002C6.125 8.51796 6.21719 8.2954 6.38128 8.1313C6.54538 7.96721 6.76793 7.87502 7 7.87502H14ZM14.875 12.25C14.875 12.4821 14.7828 12.7046 14.6187 12.8687C14.4546 13.0328 14.2321 13.125 14 13.125H7C6.76793 13.125 6.54538 13.0328 6.38128 12.8687C6.21719 12.7046 6.125 12.4821 6.125 12.25C6.125 12.018 6.21719 11.7954 6.38128 11.6313C6.54538 11.4672 6.76793 11.375 7 11.375H14C14.2321 11.375 14.4546 11.4672 14.6187 11.6313C14.7828 11.7954 14.875 12.018 14.875 12.25ZM14.707 15.2364C14.843 15.4235 14.8994 15.6569 14.8638 15.8855C14.8282 16.114 14.7036 16.3192 14.5171 16.4561C13.6306 17.0879 12.5806 17.4503 11.4931 17.5C10.8578 17.497 10.2417 17.2813 9.74312 16.8875C9.45612 16.6906 9.34675 16.625 9.13062 16.625C8.54562 16.7155 7.99366 16.9548 7.52762 17.3198C7.34278 17.4515 7.11396 17.5063 6.88948 17.4725C6.665 17.4387 6.46244 17.319 6.32457 17.1387C6.1867 16.9584 6.12432 16.7315 6.15059 16.506C6.17686 16.2805 6.28974 16.0741 6.46537 15.9303C7.23641 15.3317 8.16224 14.9656 9.13412 14.875C9.71699 14.8843 10.2805 15.0858 10.7371 15.4481C10.9453 15.6354 11.2133 15.7424 11.4931 15.75C12.2086 15.6964 12.8966 15.452 13.4855 15.0421C13.6733 14.906 13.9074 14.85 14.1364 14.8865C14.3655 14.9229 14.5707 15.0487 14.707 15.2364Z"
                                        fill="#636363"
                                      />
                                    </g>
                                    <defs>
                                      <clipPath id="clip0_3660_584">
                                        <rect
                                          width="21"
                                          height="21"
                                          fill="white"
                                        />
                                      </clipPath>
                                    </defs>
                                  </svg>
                                </div>
                              </div>
                              <div className="step-content">
                                <span className="step-title">
                                  Proses Verifikasi Data
                                </span>
                                <span className="step-description">
                                  Pengajuan kredit kamu sedang dalam proses
                                  verifikasi data
                                </span>
                                <span className="step-date">
                                  {data.AP_LASTTRDATE
                                    ? data.AP_LASTTRDATE
                                    : "-"}
                                </span>
                              </div>
                            </li>
                          );
                        } else if (
                          data.CODE_TRACKING === "BP3.0.0" ||
                          data.CODE_TRACKING === "BP9.0.0"
                        ) {
                          return (
                            <li className="step-item green">
                              <div className="step-line">
                                <div className="step-icon">
                                  <svg
                                    width="23"
                                    height="23"
                                    viewBox="0 0 23 23"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <g clip-path="url(#clip0_3660_590)">
                                      <path
                                        d="M21.3898 4.24638L8.14662 17.4886C8.05758 17.578 7.95177 17.649 7.83525 17.6973C7.71873 17.7457 7.59381 17.7707 7.46764 17.7707C7.34147 17.7707 7.21655 17.7457 7.10003 17.6973C6.98351 17.649 6.8777 17.578 6.78866 17.4886L1.66733 12.3625C1.57829 12.2731 1.47248 12.2022 1.35596 12.1538C1.23944 12.1054 1.11452 12.0805 0.98835 12.0805C0.862182 12.0805 0.737255 12.1054 0.620739 12.1538C0.504222 12.2022 0.398409 12.2731 0.309371 12.3625C0.219981 12.4515 0.149052 12.5574 0.100655 12.6739C0.0522572 12.7904 0.0273437 12.9153 0.0273438 13.0415C0.0273437 13.1677 0.0522572 13.2926 0.100655 13.4091C0.149052 13.5256 0.219981 13.6314 0.309371 13.7205L5.43262 18.8428C5.97307 19.3822 6.70548 19.6852 7.46908 19.6852C8.23268 19.6852 8.96509 19.3822 9.50554 18.8428L22.7478 5.60338C22.837 5.51436 22.9078 5.40861 22.9561 5.29218C23.0045 5.17575 23.0293 5.05094 23.0293 4.92488C23.0293 4.79883 23.0045 4.67401 22.9561 4.55758C22.9078 4.44116 22.837 4.3354 22.7478 4.24638C22.6587 4.15699 22.5529 4.08606 22.4364 4.03767C22.3199 3.98927 22.195 3.96436 22.0688 3.96436C21.9426 3.96436 21.8177 3.98927 21.7012 4.03767C21.5847 4.08606 21.4789 4.15699 21.3898 4.24638Z"
                                        fill="white"
                                      />
                                    </g>
                                    <defs>
                                      <clipPath id="clip0_3660_590">
                                        <rect
                                          width="23"
                                          height="23"
                                          fill="white"
                                        />
                                      </clipPath>
                                    </defs>
                                  </svg>
                                </div>
                              </div>
                              <div className="step-content">
                                <span className="step-title">
                                  {data.STATUS_TRACKING}
                                </span>
                                <span className="step-description">
                                  Selamat, pengajuan kredit kamu telah disetujui
                                </span>
                                <span className="step-date">
                                  {data.AP_LASTTRDATE
                                    ? data.AP_LASTTRDATE
                                    : "-"}
                                </span>
                              </div>
                            </li>
                          );
                        } else if (data.CODE_TRACKING === "BP4.0.0") {
                          return (
                            <li className="step-item blue">
                              <div className="step-line">
                                <div className="step-icon">
                                  <svg
                                    width="20"
                                    height="20"
                                    viewBox="0 0 20 20"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <g clip-path="url(#clip0_3660_592)">
                                      <path
                                        d="M19.2675 7.55727L12.9467 1.2356C12.1644 0.455591 11.1047 0.0175781 10 0.0175781C8.89529 0.0175781 7.83564 0.455591 7.05334 1.2356L0.73251 7.55727C0.499531 7.78875 0.314818 8.06418 0.189079 8.36759C0.0633394 8.67099 -0.000925334 8.99634 1.00662e-05 9.32477V17.5056C1.00662e-05 18.1686 0.263402 18.8045 0.732243 19.2734C1.20108 19.7422 1.83697 20.0056 2.50001 20.0056H17.5C18.163 20.0056 18.7989 19.7422 19.2678 19.2734C19.7366 18.8045 20 18.1686 20 17.5056V9.32477C20.0009 8.99634 19.9367 8.67099 19.8109 8.36759C19.6852 8.06418 19.5005 7.78875 19.2675 7.55727ZM12.5 18.3389H7.50001V15.0606C7.50001 14.3976 7.7634 13.7617 8.23224 13.2928C8.70108 12.824 9.33697 12.5606 10 12.5606C10.6631 12.5606 11.2989 12.824 11.7678 13.2928C12.2366 13.7617 12.5 14.3976 12.5 15.0606V18.3389ZM18.3333 17.5056C18.3333 17.7266 18.2455 17.9386 18.0893 18.0949C17.933 18.2511 17.721 18.3389 17.5 18.3389H14.1667V15.0606C14.1667 13.9555 13.7277 12.8957 12.9463 12.1143C12.1649 11.3329 11.1051 10.8939 10 10.8939C8.89494 10.8939 7.83513 11.3329 7.05373 12.1143C6.27233 12.8957 5.83334 13.9555 5.83334 15.0606V18.3389H2.50001C2.279 18.3389 2.06703 18.2511 1.91075 18.0949C1.75447 17.9386 1.66668 17.7266 1.66668 17.5056V9.32477C1.66745 9.10391 1.75517 8.89225 1.91084 8.7356L8.23168 2.41643C8.70143 1.94887 9.33723 1.68639 10 1.68639C10.6628 1.68639 11.2986 1.94887 11.7683 2.41643L18.0892 8.7381C18.2442 8.89414 18.3319 9.10478 18.3333 9.32477V17.5056Z"
                                        fill="white"
                                      />
                                    </g>
                                    <defs>
                                      <clipPath id="clip0_3660_592">
                                        <rect
                                          width="20"
                                          height="20"
                                          fill="white"
                                        />
                                      </clipPath>
                                    </defs>
                                  </svg>
                                </div>
                              </div>
                              <div className="step-content">
                                <span className="step-title">
                                  Realisasi Kredit
                                </span>
                                <span className="step-description">
                                  Selamat, sekarang kamu sudah memiliki hunian
                                  idaman kamu
                                </span>
                                <span className="step-date">
                                  {data.AP_LASTTRDATE
                                    ? data.AP_LASTTRDATE
                                    : "-"}
                                </span>
                              </div>
                            </li>
                          );
                        } else if (data.CODE_TRACKING === "BP9.1.0") {
                          return (
                            <li className="step-item yellow">
                              <div className="step-line">
                                <div className="step-icon yellow">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <g clip-path="url(#clip0_3660_1746)">
                                      <path
                                        d="M12 0C9.62663 0 7.30655 0.703788 5.33316 2.02236C3.35977 3.34094 1.8217 5.21508 0.913451 7.4078C0.00519943 9.60051 -0.232441 12.0133 0.230582 14.3411C0.693605 16.6689 1.83649 18.807 3.51472 20.4853C5.19295 22.1635 7.33115 23.3064 9.65892 23.7694C11.9867 24.2324 14.3995 23.9948 16.5922 23.0865C18.7849 22.1783 20.6591 20.6402 21.9776 18.6668C23.2962 16.6935 24 14.3734 24 12C23.9966 8.81846 22.7312 5.76821 20.4815 3.51852C18.2318 1.26883 15.1815 0.00344108 12 0V0ZM12 2C14.3065 1.99816 16.5419 2.79775 18.324 4.262L4.26201 18.324C3.06257 16.8598 2.30366 15.0851 2.07362 13.2064C1.84358 11.3277 2.15186 9.42234 2.96259 7.71204C3.77332 6.00173 5.05314 4.55688 6.65308 3.54564C8.25303 2.53441 10.1073 1.9984 12 2V2ZM12 22C9.69353 22.0018 7.45808 21.2022 5.67601 19.738L19.738 5.676C20.9374 7.14016 21.6963 8.91488 21.9264 10.7936C22.1564 12.6723 21.8481 14.5777 21.0374 16.288C20.2267 17.9983 18.9469 19.4431 17.3469 20.4544C15.747 21.4656 13.8927 22.0016 12 22Z"
                                        fill="white"
                                      />
                                    </g>
                                    <defs>
                                      <clipPath id="clip0_3660_1746">
                                        <rect
                                          width="24"
                                          height="24"
                                          fill="white"
                                        />
                                      </clipPath>
                                    </defs>
                                  </svg>
                                </div>
                              </div>
                              <div className="step-content">
                                <span className="step-title">
                                  Pembatalan Proses
                                </span>
                                <span className="step-description">
                                  Pengajuan kredit kamu dibatalkan prosesnya
                                </span>
                                <span className="step-date">
                                  {data.AP_LASTTRDATE
                                    ? data.AP_LASTTRDATE
                                    : "-"}
                                </span>
                              </div>
                            </li>
                          );
                        } else {
                          // data.STATUS === 5
                          return (
                            <li className="step-item red">
                              <div className="step-line">
                                <div className="step-icon">
                                  <svg
                                    width="22"
                                    height="22"
                                    viewBox="0 0 22 22"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M0.268776 0.268388C0.440678 0.0965391 0.673794 0 0.916861 0C1.15993 0 1.39304 0.0965391 1.56495 0.268388L11.0002 9.70364L20.4354 0.268388C20.6073 0.0965391 20.8405 0 21.0835 0C21.3266 0 21.5597 0.0965391 21.7316 0.268388C21.9035 0.440288 22 0.673404 22 0.916471C22 1.15954 21.9035 1.39265 21.7316 1.56455L12.2964 10.9998L21.7316 20.4351C21.9035 20.607 22 20.8401 22 21.0831C22 21.3262 21.9035 21.5593 21.7316 21.7312C21.5597 21.9031 21.3266 21.9996 21.0835 21.9996C20.8405 21.9996 20.6073 21.9031 20.4354 21.7312L11.0002 12.296L1.56495 21.7312C1.39304 21.9031 1.15993 21.9996 0.916861 21.9996C0.673794 21.9996 0.440678 21.9031 0.268776 21.7312C0.0969276 21.5593 0.000389099 21.3262 0.000389099 21.0831C0.000389099 20.8401 0.0969276 20.607 0.268776 20.4351L9.70403 10.9998L0.268776 1.56455C0.0969276 1.39265 0.000389099 1.15954 0.000389099 0.916471C0.000389099 0.673404 0.0969276 0.440288 0.268776 0.268388Z"
                                      fill="white"
                                    />
                                  </svg>
                                </div>
                              </div>
                              <div className="step-content">
                                <span className="step-title">
                                  Penolakan Kredit
                                </span>
                                <span className="step-description">
                                  Mohon maaf, pengajuan kredit kamu belum
                                  disetujui
                                </span>
                                <span className="step-date">
                                  {data.AP_LASTTRDATE
                                    ? data.AP_LASTTRDATE
                                    : "-"}
                                </span>
                              </div>
                            </li>
                          );
                        }
                      })}
                  </div>
                ) : (
                  <div>
                    <div className="text-center" style={{ marginTop: "100px" }}>
                      <svg
                        width="80"
                        height="80"
                        viewBox="0 0 80 80"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M53.3337 3.33398H13.3337C9.66699 3.33398 6.66699 6.33398 6.66699 10.0007V56.6673H13.3337V10.0007H53.3337V3.33398ZM50.0003 16.6673L70.0003 36.6673V70.0006C70.0003 73.6673 67.0003 76.6673 63.3337 76.6673H26.6337C22.967 76.6673 20.0003 73.6673 20.0003 70.0006L20.0337 23.334C20.0337 19.6673 23.0003 16.6673 26.667 16.6673H50.0003ZM46.667 40.0006H65.0003L46.667 21.6673V40.0006Z"
                          fill="#0061A7"
                          fill-opacity="0.75"
                        />
                      </svg>
                      <h5
                        style={{
                          fontFamily: "Helvetica",
                          fontStyle: "normal",
                          fontWeight: "bold",
                          fontSize: "16px",
                          lineHeight: "150%",
                          marginTop: "40px",
                          color: "#000000",
                        }}
                      >
                        Kamu belum memiliki status pengajuan
                      </h5>
                    </div>
                  </div>
                )}
              </div>
              <div
                // style={{ marginLeft: "30px", paddingTop: "20px"}}
                className="botton-modal-kpr"
              >
                <a
                  className="btn btn-main"
                  data-bs-dismiss="modal"
                  style={{
                    fontSize: "14px",
                    padding: "10px 20px",
                    fontWeight: 700,
                    width: 386,
                    height: 42.16,
                  }}
                >
                  OK
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal fade"
        id="modal-persyaratan"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myLargeModalLabel"
        aria-hidden="true"
        style={{ fontFamily: "Helvetica", zIndex: "9999" }}
      >
        <div
          className="modal-dialog modal-dialog-centered m-auto px-2"
          style={{ maxWidth: "480px" }}
        >
          <div
            className="modal-content modal_detail_kpr mb-4"
            style={{ padding: "20px", minHeight: "540px", marginTop: "85px" }}
          >
            <div className="close-modal" data-bs-dismiss="modal">
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <h5 className="modal-title">Dokumen Persyaratan KPR</h5>
            <p>
              Syarat - syarat KPR adalah kelengkapan administratif yang harus
              dipenuhi saat mengajukan KPR:
            </p>
            <div class="container">
              <div className="">
                <div className="list-group-kpr">
                  <ul className="list-group menu_member mb-0">
                    <li
                      className="list-group-item list-blue"
                      style={{ textAlign: "center", height: "37px" }}
                    >
                      <label>Checklist Dokumen</label>
                    </li>
                    <li
                      className="list-group-item d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        KTP Pemohon
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.ktp_pemohon && <Ceklist />}{" "}
                      </div>
                    </li>
                    <li
                      className="list-group-item list-grey d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        KTP Pasangan
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.ktp_pasangan && <Ceklist />}
                      </div>
                    </li>
                    <li
                      className="list-group-item d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Kartu Keluarga
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.kartu_keluarga && <Ceklist />}
                      </div>
                    </li>
                    <li
                      className="list-group-item list-grey d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Surat Nikah / Cerai
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.surat_nikah && <Ceklist />}
                      </div>
                    </li>
                    <li
                      className="list-group-item d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        NPWP/SPT Pph 21
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.npwp && <Ceklist />}
                      </div>
                    </li>
                    <li
                      className="list-group-item list-grey d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Rekening Tabungan/Tabungan 3 Bulan Terakhir
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.rekening && <Ceklist />}
                      </div>
                    </li>
                    <li className="list-group-item d-flex">
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Surat Keterangan Pengangkatan Pegawai Tetap
                      </div>
                      <div
                        className="col-4 justify-content-end d-flex"
                        style={{ alignItems: "center" }}
                      >
                        <ul className="text-end">Khusus Karyawan</ul>
                      </div>
                    </li>
                    <li className="list-group-item list-grey d-flex">
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Surat Izin Praktek
                      </div>
                      <div
                        className="col-4 justify-content-end d-flex"
                        style={{ alignItems: "center" }}
                      >
                        <ul className="text-end">Khusus Profesional</ul>
                      </div>
                    </li>
                    <li className="list-group-item d-flex">
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Surat Izin Usaha (Akta Pendirian Perusahaan, SIUP, TDP)
                      </div>
                      <div
                        className="col-4 justify-content-end d-flex"
                        style={{ alignItems: "center" }}
                      >
                        <ul className="text-end">Khusus Pengusaha</ul>
                      </div>
                    </li>
                    <li
                      className="list-group-item list-grey d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Dokumen Penghasilan Pemohon
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.dok_penghasilan && <Ceklist />}
                      </div>
                    </li>
                    <li
                      className="list-group-item d-flex"
                      style={{ minHeight: "25.24px" }}
                    >
                      <div className="col-8" style={{ lineHeight: "normal" }}>
                        Dokumen Penghasilan Pasangan
                      </div>
                      <div
                        className="col-4 d-flex justify-content-end"
                        style={{ alignItems: "center" }}
                      >
                        {dokPersyaratan?.dok_penghasilan_pas && <Ceklist />}
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div classname="row mt-3 w-100" style={{ textAlign: "center" }}>
                <button
                  className="btn btn-main w-100"
                  data-bs-dismiss="modal"
                  style={{
                    fontSize: "14px",
                    padding: "10px 20px",
                    fontWeight: 700,
                    maxWidth: 386,
                    height: 42.16,
                  }}
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

const PopUpError = () =>{
  return(
      <div
      className="modal show"
      id=""
      tabIndex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
      style={{display:"block",zIndex:9999}}
    >
      <div className="modal-dialog modal-dialog-centered mx-auto px-4">
        <div
          className="modal-content modal_delete"
          style={{ padding: "20px 29px",height:"60%" }}
        >
          <div className="modal-body text-center pt-3 px-0">
            <div className="text-center">
              <h4
                className="modal-title mb-3"
                style={{
                  fontFamily: "Futura",
                  fontStyle: "normal",
                  lineHeight: "130%",
                  fontWeight: "700",
                }}
              >
              Data KPR sudah diklaim
              </h4>
              
            
              <Lottie
                  options={defaultOptions}
                  isStopped={false}
                  isPaused={false}
                  style={{margin:"50px auto",maxHeight:"400px",maxWidth:"400px"}}
                />

              <button
                type="buttom"
                className="btn btn-main form-control btn-back px-5"
                style={{
                  width: "100%",
                  maxWidth: "540px",
                  height: "48px",
                  fontSize: "14px",
                  fontFamily: "Helvetica",
                  fontWeight: 700,
                  backgroundColor: "#0061A7",
                }}
                onClick={()=>{window.location.replace("/member/kredit/other")}}
              >
                Oke
              </button>
              {/* <Link href="/">
                          </Link> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default withAuth(DetailOther);

//  ------------------- fetchdetailkpr --------------------------------------//

//   const fetchDetailKpr = async () => {
//     const url = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/show?i_kpr=KPRCB01072214581374&i_member=115374`;
//     const payload = {};
//     try {
//       const dataTipe = await fetch(url, {
//         method: "GET",
//         headers: {
//           "Content-Type": "application/x-www-form-urlencoded",
//           app_key: process.env.NEXT_PUBLIC_APP_KEY,
//           secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
//         },
//       });
//       const responseTipe = await dataTipe.json();
//       console.log("detail other", responseTipe);
//     } catch (error) {
//       console.log("error get data jenis KPR : ", error);
//     } finally {
//     }
//   };

//  ------------------- fetchnewendpoint --------------------------------------//
// const fetchDetail = async() =>{
//     const url = `${process.env.NEXT_PUBLIC_API_HOST_OTHER_PENGAJUAN}/other-penngajuan`
//     const payload = {
//         "ID_KPR": "KPRCB01072214581374",
//         "CABANG":"BEKASI",
//         "NAMA": "Destri Wirani",
//         "EMAIL": "cobaajaa97@gmail.com",
//         "NO_HP":"083899733420",
//         "PRODUK":"KPR NON SUBSIDI",
//         "NILAI_DIAJUKAN":"550000000",
//         "TGL_DIAJUKAN": "0000-00-00 00:00:00",
//         "ID_DEV": "S555",
//         "DEVELOPER": "SRI PERTIWI SEJATI, PT",
//         "ID_PROPER": "1733006",
//         "PROPER": "KOTA SERANG BARU",
//         "ALAMAT": "",
//         "BLOK": "",
//         "KAVLING": "",
//         "STATUS_PENGAJUAN": "BP1.0.0",
//         "ID_MEMBER": null

//     }
//     try {
//         const dataTipe = await fetch(
//           url,
//           {
//             method: "POST",
//             headers: {
//             "Content-Type": "multipart/form-data",
//               app_key: process.env.NEXT_PUBLIC_APP_KEY,
//               secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
//             },
//             body: new URLSearchParams(payload),
//           }
//         );
//         const responseTipe = await dataTipe.json();
//         console.log("detail other", responseTipe)
//       } catch (error) {
//         console.log("error get data jenis KPR : ", error);

//       } finally {

//       }
// }
