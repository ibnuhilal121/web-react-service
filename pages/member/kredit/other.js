import Link from "next/link";
import { useEffect, useState } from "react";
import ItemPengajuanLuar from "../../../components/data/ItemPengajuanLuar";
import NotAvail from "../../../components/element/NotAvail";
import Layout from "../../../components/Layout";
import MenuMember from "../../../components/MenuMember";
import Breadcrumb from "../../../components/section/Breadcrumb";
import PaginationNew from "../../../components/data/PaginationNew";
import { useAppContext } from "../../../context";
import withAuth from "../../../helpers/withAuth";
import data_pengajuan_luar from "../../../sample_data/data_pengajuan_luar";
import { useRouter } from "next/router";
import ItemPengajuanOther from "../../../components/data/itemPengajuanOther";
import * as animationData from "../../../public/x_animation.json";
import Lottie from "react-lottie";
import verifiedMember from "../../../helpers/verifiedMember";

const index = () => {
  /* ------------------------------ FETCH KPR ------------------------------ */
  const router = useRouter();
  const { query, asPath } = router;
  const { userKey, userProfile } = useAppContext();
  const { email = "" } = userProfile || {};
  const [loadingKpr, setLoadingKpr] = useState(false);
  const [kprData, setKprData] = useState([]);
  const [jenisKreditKonvensional, setJenisKreditKonvensional] = useState(null);
  const [jenisKreditSyariah, setJenisKreditSyariah] = useState(null);
  const [page, setPage] = useState(query.page || 1);
  const [phoneNumber, setPhoneNumber] = useState("");
  const [phoneNumberError, setPhoneNumberError] = useState("");
  const [kprId, setKprId] = useState("");
  const [kprIdError, setKprIdErorr] = useState("");
  const [msgErr, setMsgErr] = useState("");
  const [searchError,setSearchError] = useState(false);
  const app_key = "TXugSwyTQkBQNLWMcPgc"
  const secret_key = "rwBea8tTftJHZ87C9nm4XP7MD8FpLzND"

  const [paging, setPaging] = useState({
    JmlHalTotal: 1,
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  function onChangePage(n) {
    setPage(n);
    router.push(asPath.split("?")[0] + `?page=${n}`);
  }

  useEffect(()=>{
    fetchPengajuanList(email);
  },[])

  const validatePhoneNumber = (value) => {
    const numOnly = new RegExp("^[0-9]+$");
    if (numOnly.test(value) || value === "") {
      setPhoneNumber(value);
      if(value.slice(0,2) != "08" ) {
        setPhoneNumberError("Nomor harus menggunakan format '08xxxxxxxx' ");
      } else if (value.length < 10) {
        setPhoneNumberError("Nomor harus berisi minimal 10 karakter");
      } else {
        setPhoneNumberError("");
      }
      if (!value) {
        setPhoneNumberError("Isian tidak boleh kosong");
      }
    }
  };
  const validateKprId = (value) => {
    setKprId(value);
    if (!value) {
      setKprIdErorr("Isian tidak boleh kosong");
    } else{
      setKprIdErorr("");
    }
  };

  const fetchOtherPengajuan = async (e) => {
    e.preventDefault();
    const payload = {
      ID_KPR: kprId,
      NO_HP: phoneNumber,
      EMAIL: email,
    };
    if (kprId.length != 0 && phoneNumber.length != 0) {
      try {
        const dataTipe = await fetch(
          `${process.env.NEXT_PUBLIC_API_HOST_OTHER_PENGAJUAN}/update-other-pengajuan`,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          },
            body: new URLSearchParams(payload),
          }
        );
        const responseTipe = await dataTipe.json();
        console.log("data other", responseTipe);
        if (responseTipe.status != false) {
          setMsgErr("");
          setKprId("");
          setPhoneNumber("");
          fetchPengajuanList(email);
        } else {
          setSearchError(true)
          setMsgErr(responseTipe.message);
        }
      } catch (error) {
        console.log("error get data jenis KPR : ", error);
      } finally {
        setLoadingKpr(false);
      }
    } else {
      setMsgErr("Semua kolom wajib diisi");
    }
  };

  const fetchPengajuanList = async (e) => {
    try {
        const payload = {
          "EMAIL": e,
        }
        const dataTipe = await fetch(
          `${process.env.NEXT_PUBLIC_API_HOST_OTHER_PENGAJUAN}/list-other-pengajuan`,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            },
            body: new URLSearchParams(payload)
          }
        );
        const responseTipe = await dataTipe.json();  
        console.log("data other", responseTipe);
        setKprData(responseTipe.data) 
      } catch (error) {
        console.log("error get data jenis KPR : ", error);
       
      } finally {
        setLoadingKpr(false)
      }
    
  }


  return (
    <Layout
      title="Pengajuan Kredit Lainnya - Dashboard Member | BTN Properti"
      isLoaderOpen={loadingKpr}
    >
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="kredit" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
              <h5 className="title_akun">Pengajuan KPR</h5>
              <div className="dashboard-profil revamp-card-developer ">
                <div className="tab-menu revamp-card-imgdev ">
                  <ul
                    className="nav nav-tabs navbar_kategori"
                    // style={{ maxWidth: "350px" }}
                  >
                    <div className="d-flex p-0">
                      <li className="nav-item">
                        <Link href="/member/kredit">
                          <a className="nav-link p-0">
                            <span>KPR di Luar Listing BTN</span>
                          </a>
                        </Link>
                      </li>
                    </div>

                    <div className="d-flex p-0">
                      <li className="nav-item">
                        <Link href="/member/kredit/listing">
                          <a className="nav-link p-0">
                            <span>KPR dalam Listing BTN</span>
                          </a>
                        </Link>
                      </li>
                    </div>

                    <div className="d-flex p-0">
                      <li className="nav-item">
                        <Link href="#">
                          <a className="nav-link active p-0">
                            <span style={{ fontSize: "14px" }}>
                              Pengajuan Lainnya
                            </span>
                          </a>
                        </Link>
                      </li>
                    </div>
                  </ul>
                </div>

                <form onSubmit={fetchOtherPengajuan} id="other-pengajuan-field">
                  <div className="row mb-4 g-0">
                    <div className="col-md-3 d-flex p-0 text-start justify-content-start">
                      <p
                        style={{
                          fontFamily: "Futura",
                          fontWeight: 700,
                          fontSize: "14px",
                          fontStyle: "normal",
                          lineHeight: "150%",
                          color: "#00193e",
                          margin: "auto 0",
                          textAlign: "start",
                        }}
                      >
                        Masukkan Nomor Handphone
                      </p>
                    </div>
                    <div className="col-md-4 p-0">
                      <div className="floating-label-wrap m-0 p-0">
                        <input
                          type="text"
                          className="floating-label-field required number"
                          maxLength="13"
                          placeholder="Nomor Handphone"
                          value={phoneNumber}
                          onChange={(e) => {
                            validatePhoneNumber(e.target.value);
                          }}
                          style={{ padding: "8px 16px", height: "35px" }}
                        />

                        {phoneNumberError && (
                          <p
                            style={{
                              color: "#dc3545",
                              fontFamily: "Helvetica",
                              fontSize: 12,
                              transform: "translateY(3px)",
                              position: "absolute",
                            }}
                          >
                            {phoneNumberError}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4 g-0">
                    <div className="col-md-3 d-flex p-0 text-start justify-content-start">
                      <p
                        style={{
                          fontFamily: "Futura",
                          fontWeight: 700,
                          fontSize: "14px",
                          fontStyle: "normal",
                          lineHeight: "150%",
                          color: "#00193e",
                          margin: "auto 0",
                          textAlign: "start",
                        }}
                      >
                        Masukkan ID KPR
                      </p>
                    </div>
                    <div className="col-md-4 p-0">
                      <div className="floating-label-wrap m-0 p-0">
                        <input
                          type="text"
                          className="floating-label-field required number"
                          maxLength="20"
                          minLength="0"
                          placeholder="ID KPR"
                          value={kprId}
                          onChange={(e) => {
                            validateKprId(e.target.value);
                          }}
                          style={{ padding: "8px 16px", height: "35px" }}
                        />

                        {kprIdError && (
                          <p
                            style={{
                              color: "#dc3545",
                              fontFamily: "Helvetica",
                              fontSize: 12,
                              transform: "translateY(3px)",
                              position: "absolute",
                            }}
                          >
                            {kprIdError}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row g-0 mb-4 align-items-center">
                    <div className="col-md-2 col-6">
                      <button
                        type="submit"
                        className="btn btn-main w-100 h-100"
                        style={{ borderRadius: "8px" }}
                        disabled={kprId.length != 0 && phoneNumber.length >= 10 && phoneNumber.slice(0,2) == "08" ? false : true}
                      >
                        Cari
                      </button>
                    </div>
                   
                  </div>
                </form>
                {kprData.length != 0 &&
                  kprData.map((data, index) => (
                    <div key={index} className="">
                      <ItemPengajuanOther data={data} />
                    </div>
                  ))}
                {kprData.length == 0 && (
                  <>
                    <NotAvail
                      message={
                        loadingKpr
                          ? "Memuat data..."
                          : "Kamu belum memiliki list pengajuan kredit"
                      }
                    />
                    <div className="mb-5"></div>
                  </>
                )}

                <PaginationNew
                  current={Number(paging.HalKe)}
                  length={Number(paging.JmlHalTotal)}
                  onChangePage={onChangePage}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {searchError ? 
      <div
        className="modal show"
        id=""
        tabIndex="-1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
        style={{display:"block"}}
      >
        <div className="modal-dialog modal-dialog-centered mx-auto px-2">
          <div
            className="modal-content modal_delete"
            style={{ padding: "20px 29px",height:"60%" }}
          >
            <div className="modal-body text-center pt-3 px-0">
              <div className="text-center">
                <h4
                  className="modal-title mb-3"
                  style={{
                    fontFamily: "Futura",
                    fontStyle: "normal",
                    lineHeight: "130%",
                    fontWeight: "700",
                  }}
                >
                {msgErr}
                </h4>
                
              
                <Lottie
                    options={defaultOptions}
                    isStopped={false}
                    isPaused={false}
                    style={{margin:"50px auto",maxHeight:"400px",maxWidth:"400px"}}
                  />

                <button
                  type="buttom"
                  className="btn btn-main form-control btn-back px-5"
                  style={{
                    width: "100%",
                    maxWidth: "540px",
                    height: "48px",
                    fontSize: "14px",
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    backgroundColor: "#0061A7",
                  }}
                  onClick={()=>{setSearchError(false)}}
                >
                  Oke
                </button>
                {/* <Link href="/">
                            </Link> */}
              </div>
            </div>
          </div>
        </div>
      </div> : <></>}
    
    </Layout>
  );
};

export default (withAuth(index));
