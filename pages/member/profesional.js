import React, { useEffect, useState, useRef } from "react";
import Link from "next/link";
import ProfesionalPortofolio from "../../components/data/ProfesionalPortofolio";
import Informasibisnis from "../../components/form/InformasiBisnis";
import Layout from "../../components/Layout";
import { useAppContext } from "../../context";
import MenuMember from "../../components/MenuMember";
import Breadcrumb from "../../components/section/Breadcrumb";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import PaginationNew from "../../components/data/PaginationNew";
import Select from "react-select";

import data_portofolio from "../../sample_data/data_portofolio";
import withAuth from "../../helpers/withAuth";
import verifiedMember from "../../helpers/verifiedMember";
import { useMediaQuery } from "react-responsive";
import Lottie from "react-lottie";
import styles from "../../components/element/modal_confirmation//modal_confirmation.module.scss";
import * as animationData from "../../public/animate_home.json";
import { useRouter } from "next/router";
import { defaultHeaders } from "../../utils/defaultHeaders";

const Profesional = () => {
  const router = useRouter();
  const isTabletOrDesktop = useMediaQuery({ query: `(max-width: 768px)` });
  const { userKey, userProfile } = useAppContext();
  const [isLoading, setIsLoading] = useState(true);
  const [profileMember, setProfileMember] = useState({
    bya_js: [{ jasa: "", hrg_rndh: "", hrg_tggi: "" }],
    st: 1,
  });
  const [haveImage, setHaveImage] = useState(0);

  const [selectedImage, setSelectedImage] = useState();
  const [imageConverted, setImageConverted] = useState("");
  const [selectedImage2, setSelectedImage2] = useState();
  const [imageConverted2, setImageConverted2] = useState("");
  const [selectedImage3, setSelectedImage3] = useState();
  const [imageConverted3, setImageConverted3] = useState("");
  const [selectedImage4, setSelectedImage4] = useState();
  const [imageConverted4, setImageConverted4] = useState("");
  const [selectedImage5, setSelectedImage5] = useState();
  const [imageConverted5, setImageConverted5] = useState("");
  const [modalSuc, setModalSuc] = useState(false);

  const [idCheckbox, setIdCheckbox] = useState([]);

  const [validateData, setValidateData] = useState(false);
  const [validatePortofolio, setValidatePortofolio] = useState(false);
  const [listPortofolio, setListPortofolio] = useState([]);
  const [portofolio, setPortoffolio] = useState(null);
  const [paging, setPaging] = useState(null);
  const [page, setPage] = useState(1);

  const [selectValue, setSelectValue] = useState();
  const [valueAlamat, setValueAlamat] = useState();

  const selectInputRef = useRef();
  const modalRef = useRef();
  const [blockPortofolio, setBlockPortofolio] = useState(false);
  const animStr = (i) => `fadeIn ${1500}ms ease-out ${0 * (i + 1)}ms forwards`;
  const [modalSucces, setModalSucces] = useState(false);

  //const [logoSize, setLogoSize] = useState({});
  const [loading, setLoading] = useState(false);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  useEffect(() => {
    if (!userKey) return;
    getListData();
  }, [userKey]);

  useEffect(() => {
    console.log("Data set value profesional", profileMember);
  }, [profileMember]);

  useEffect(() => {
    console.log("Data set value portofolio", portofolio);
  }, [portofolio]);

  useEffect(() => {
      getListDataPortofolio();
  }, [page]);

  const getListDataPortofolio = async () => {
    try {
      setIsLoading(true);
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      const payload = {
        i_prfl: profileMember.id,
        Sort: "t_ins DESC",
        Limit: 9,
        Page: page
      };
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/proyek/show/bymember`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const response = await loadData.json();
      console.log("data Portofolio", response);
      if (!response.IsError) {
        console.log("Success get data Portofolio", response.Data);
        if (response.Data.length) {
          const listData = [];
          const getData = response.Data;
          getData.map((data, index) => {
            let dataImage;
            if (data.lst_fto !== null) {
              try {
                const myArray = JSON.parse(data.lst_fto);
                dataImage = Object.keys(myArray).map((key) => myArray[key]);
                console.log("get Data Gambarr", dataImage);
              } catch (e) {
                console.log("error mapping image!! ", e); // error in the above string (in this case, yes)!
              }
            }
            const payload = {
              lst_fto: dataImage != null ? dataImage : null,
              i_prfl: data.i_prfl != null ? data.i_prfl : "",
              n: data.n != null ? data.n : "",
              id: data.id != null ? data.id : "",
              dsk: data.dsk != null ? data.dsk : "",
              almt: data.almt != null ? data.almt : "",
            };
            listData[index] = payload;
          });
          setPortoffolio({
            i_prfl: profileMember.id,
            i_prop: profileMember.i_prop,
            i_kot: profileMember.i_kot,
            i_kec: profileMember.i_kec,
            i_kel: profileMember.i_kel,
            pos: profileMember.pos,
            lst_fto: [],
          });
          setListPortofolio((previousInputs) => ({
            ...previousInputs,
            dataList: listData,
          }));
          setPaging(response.Paging);
        }
      }
      setIsLoading(false);
    } catch (error) {
      console.log("error profesional portoffolio: ", error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const submitDataPortofolio = () => {
    if (!portofolio?.n || !portofolio?.dsk || !portofolio.almt) {
      setValidatePortofolio(true);
    } else {
      modalRef?.current?.click();
      makeRequestSendFoto();
    }
  };

  const makeRequestSendFoto = async () => {
    portofolio.lst_fto = [];
    setIsLoading(true);
    if (imageConverted) {
      const picture1 = await makeRequest(imageConverted, selectedImage, "1");
      if (picture1) {
        portofolio.lst_fto.push(picture1);
      }
    }
    if (imageConverted2) {
      const picture2 = await makeRequest(imageConverted2, selectedImage2, "2");
      if (picture2) {
        portofolio.lst_fto.push(picture2);
      }
    }
    if (imageConverted3) {
      const picture3 = await makeRequest(imageConverted3, selectedImage3, "3");
      if (picture3) {
        portofolio.lst_fto.push(picture3);
      }
    }
    if (imageConverted4) {
      const picture4 = await makeRequest(imageConverted4, selectedImage4, "4");
      if (picture4) {
        portofolio.lst_fto.push(picture4);
      }
    }
    if (imageConverted5) {
      const picture5 = await makeRequest(imageConverted5, selectedImage5, "5");
      if (picture5) {
        portofolio.lst_fto.push(picture5);
      }
    }
    // after all image is upload then update data portofolio
    insertDataPortofolio();
  };

  const makeRequest = (imageConverted, selectedImage, index) => {
    return new Promise(async (result, reject) => {
      try {
        const uploadPayload = {
          FolderName: "profesional-portofolio",
          //FileExtension: selectedImage.name.split(".").pop(),
          FileBase64: imageConverted.split("base64,")[1],
          TipeUpload: "1",
        };
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/image/upload`;
        const loadData = await fetch(endpoint, {
          method: "POST",
          headers: getHeaderWithAccessKey(),
          body: new URLSearchParams(uploadPayload),
        });
        const response = await loadData.json();
        if (!response.IsError) {
          console.log("image ", index, " berhasil di upload ", response.Output);
          result(response.Output);
        } else {
          console.log("error upload file ", index, " ", response.ErrToUser);
          //setIsLoading(false);
          // throw {
          //     message: response.ErrToUser,
          // };
        }
      } catch (error) {
        console.log("Kena catch! ", error.message);
      } finally {
      }
    });
  };

  const insertDataPortofolio = async () => {
    try {
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      portofolio["lst_fto"] = JSON.stringify(portofolio.lst_fto);
      portofolio["i_prfl"] = profileMember.id;
      portofolio["i_prop"] = profileMember.i_prop;
      portofolio["i_kot"] = profileMember.i_kot;
      portofolio["i_kec"] = profileMember.i_kec;
      portofolio["i_kel"] = profileMember.i_kel;
      portofolio["pos"] = profileMember.pos;
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/proyek/insert`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams(portofolio),
      });
      const response = await loadData.json();
      if (!response.IsError) {
        console.log("Success update data", response);
        setIsLoading(false);
        //alert("Data berhasil disimpan");
        setModalSucces(true);
        getListDataPortofolio();
      } else {
        console.log("error update portofolio ", response.ErrToUser);
        setIsLoading(false);
        // throw {
        //     message: response.ErrToUser,
        // };
      }
    } catch (error) {
      console.log("Kena catch portofolio! ", error.message);
    } finally {
      setValidatePortofolio(false);
    }
  };

  const getListData = async () => {
    try {
      setIsLoading(true);
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/profil/detail/bymember`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        console.log("Success get data profesional", response.Data);
        if (response.Data.length) {
          const getData = response.Data[0];
          let byajs;
          let kategori;
          let listIdChecbox = [];
          if ((getData.bya_jsa !== null) && isValidJsonString(getData.bya_jsa)) {
            const myArray = JSON.parse(getData.bya_jsa);
            // const myArray = JSON.stringify(getData.bya_jsa)
            byajs = Object.keys(myArray).map((key) => myArray[key]);
            console.log("get Data Biaya Jasa", byajs);
          }
          if (getData.lst_kat !== null) {
            kategori = Object.keys(getData.lst_kat).map(
              (key) => getData.lst_kat[key]
            );
            console.log("get Data Biaya Jasa", kategori);
            kategori.map((data, index) => {
              listIdChecbox.push(data.i_kat);
            });
            setIdCheckbox(listIdChecbox);
          }
          const payload = {
            n: getData.n != null ? getData.n : "", // Nama PT----
            dsk: getData.dsk != null ? getData.dsk : "", // Deskripsi----
            no_tlp: getData.no_tlp != null ? getData.no_tlp : "", // No Telepon----
            no_fax: getData.no_fax != null ? getData.no_fax : "", // No Fax----
            eml: getData.eml != null ? getData.eml : "", // Email----
            web: getData.web != null ? getData.web : "", // Website----
            fb: getData.fb != null ? getData.fb : "", // Facebook----
            twt: getData.twt != null ? getData.twt : "", // Twitter----
            gpl: getData.gpl != null ? getData.gpl : "", // Google+ ----
            almt: getData.almt != null ? getData.almt : "", // Google+ ----
            dsk_byr: getData.dsk_byr != null ? getData.dsk_byr : "", // Deskripsi Bayar----
            bya_js:
              byajs != null
                ? byajs
                : [{ jasa: "", hrg_rndh: "", hrg_tggi: "" }], // Biaya Jasa Bayar----
            //lst_kat: kategori !=null ? kategori : [],
            /* list param Important! */
            id: getData.id,
            pos: getData.pos,
            i_prop: getData.i_prop,
            i_kot: getData.i_kot,
            i_kec: getData.i_kec,
            i_kel: getData.i_kel,
            st: getData.st,
          };
          setProfileMember(payload);
        }
        setIsLoading(false);
      } else {
        setBlockPortofolio(true);
        // console.log(response.ErrToUser);
        setIsLoading(false);
        // throw {
        //     message: response.ErrToUser,
        // };
      }
    } catch (error) {
      console.log(error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const submitData = async () => {
    let cekValueJasa = [];
    setIsLoading(true);
    const promise = profileMember.bya_js.map((data, i) => {
      if (data.jasa === "" || data.hrg_rndh === "" || data.hrg_tggi === "") {
        cekValueJasa = [...cekValueJasa, i + 1];
      }
    });
    await Promise.all(promise).then(() => {
      if (!profileMember?.n || !profileMember?.dsk) {
        setValidateData(true);
        window.scrollTo(0, 200);
        setIsLoading(false);
      } else if (
        !profileMember?.almt ||
        !profileMember?.i_kel ||
        !profileMember?.i_kec ||
        !profileMember?.i_kot ||
        !profileMember?.i_prop ||
        !profileMember?.pos
      ) {
        setValidateData(true);
        window.scrollTo(0, 1500);
        setIsLoading(false);
      } else if (cekValueJasa.length > 0) {
        console.log(cekValueJasa.length);
        setValidateData(true);
        window.scrollTo(0, 2150);
        setIsLoading(false);
      } else {
        console.log("Hit data", profileMember);
        updateData();
      }
      setModalSuc(true);
    });
  };

  const closeSuccessModal = () => {
    setModalSuc(false);
    window.location.reload();
  };

  const updateData = async () => {
    try {
      let temp = { ...profileMember };
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      temp["bya_js"] = JSON.stringify(temp.bya_js);
      temp["id"] = profileMember.id;
      temp["st"] = profileMember.st;
      console.log("~ payload update data : ", temp);
      console.log("~ profile member : ", profileMember);
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST
        }/profesional/profil/${blockPortofolio ? "insert" : "update"}`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams(temp),
      });
      const response = await loadData.json();
      //console.log("response update data",response);
      if (!response.IsError) {
        console.log("Success update data", response);
        // setIsLoading(false);
        // setModalSuc(true);
        // console.log("Data berhasil disimpan");
        updateLayanan();
      } else {
        // console.log(response.ErrToUser);
        setIsLoading(false);
        // throw {
        //     message: response.ErrToUser,
        // };
      }
    } catch (error) {
      console.log(error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const updateLayanan = async () => {
    try {
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/profillayanan/insert/bulk`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          i_prfl: profileMember.id,
          lst_kat: idCheckbox,
        }),
      });
      const response = await loadData.json();
      //console.log("response update Layanan ",response);
      if (!response.IsError) {
        console.log("Success update layanan", response);
        setIsLoading(false);
        setModalSuc(true);
        //alert("Data berhasil disimpan");
      } else {
        // console.log(response.ErrToUser);
        setIsLoading(false);
        // throw {
        //     message: response.ErrToUser,
        // };
      }
    } catch (error) {
      console.log(error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const resultListData = (key, value) => {
    setProfileMember((previousInputs) => ({ ...previousInputs, [key]: value }));
  };

  useEffect(() => {
    console.log("~ profile : ", profileMember);
  }, [profileMember]);

  const changePortofolio = (key, value) => {
    setPortoffolio((previousInputs) => ({ ...previousInputs, [key]: value }));
  };

  const handleInputChange = (value, index, key) => {
    const list = [...profileMember.bya_js];
    list[index][key] = value;
    setProfileMember((previousInputs) => ({ ...previousInputs, bya_js: list }));
  };

  const imageChange = (e) => {
    if (e.target.files.length > 0) {
      if (
        (e.target.files[0].type.includes("jpeg") ||
          e.target.files[0].type.includes("jpg") ||
          e.target.files[0].type.includes("png")) &&
        (e.target.files[0].size / 1048576).toFixed(2) < 3
      ) {
        if (e.target.files && e.target.files.length > 0) {
          setHaveImage(haveImage + 1);
          if (!imageConverted) {
            setSelectedImage(e.target.files[0]);
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function () {
              setImageConverted(reader.result);
            };
            reader.onerror = function (error) {
              console.log("error attach image 1 : ", error);
            };
          } else if (!imageConverted2) {
            setSelectedImage2(e.target.files[0]);
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function () {
              setImageConverted2(reader.result);
            };
            reader.onerror = function (error) {
              console.log("error attach image 2 : ", error);
            };
          } else if (!imageConverted3) {
            setSelectedImage3(e.target.files[0]);
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function () {
              setImageConverted3(reader.result);
            };
            reader.onerror = function (error) {
              console.log("error attach image 3 : ", error);
            };
          } else if (!imageConverted4) {
            setSelectedImage4(e.target.files[0]);
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function () {
              setImageConverted4(reader.result);
            };
            reader.onerror = function (error) {
              console.log("error attach image 4 : ", error);
            };
          } else if (!imageConverted5) {
            setSelectedImage5(e.target.files[0]);
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function () {
              setImageConverted5(reader.result);
            };
            reader.onerror = function (error) {
              console.log("error attach image 5 : ", error);
            };
          }
        }
      }
    }
  };

  const imageDelete = (index) => {
    if (index === 1) {
      setSelectedImage();
      setImageConverted("");
    } else if (index === 2) {
      setSelectedImage2();
      setImageConverted2("");
    } else if (index === 3) {
      setSelectedImage3();
      setImageConverted3("");
    } else if (index === 4) {
      setSelectedImage4();
      setImageConverted4("");
    } else if (index === 5) {
      setSelectedImage5();
      setImageConverted5("");
    }
    setHaveImage(haveImage - 1);
  };

  const clearText = () => {
    changePortofolio("n", "");
    changePortofolio("dsk", "");
    changePortofolio("almt", "");
    selectInputRef.current.clearValue();

    setSelectedImage();
    setImageConverted("");
    setSelectedImage2();
    setImageConverted2("");
    setSelectedImage3();
    setImageConverted3("");
    setSelectedImage4();
    setImageConverted4("");
    setSelectedImage5();
    setImageConverted5("");
    setHaveImage(0);
  };

  const checkedChange = (e, id) => {
    let array = [...idCheckbox];
    if (e.target.checked) {
      array.push(id);
    } else {
      const index = array.indexOf(id);
      if (index !== -1) {
        array.splice(index, 1);
      }
    }
    setIdCheckbox(array);
  };

  const customStyles = {
    control: (base) => ({
      ...base,
      height: 43.4,
      minHeight: 35,
      border: "1px solid #AAAAAA",
      borderRadius: "8px",
      cursor: "text",
      boxShadow: "none",
      fontFamily: "Helvetica",
      fontSize: 14,
      fontWeight: 400,
      paddingLeft: "5px",
      '[type="text"]': {
        color: "#00193E !important",
      },
      "&:hover": {
        border: "1px solid #AAAAAA",
        boxShadow: "none",
      },
      "&:focus": {
        border: "1px solid #AAAAAA",
        boxShadow: "none",
      },
    }),
    placeholder: (base) => ({
      ...base,
      fontFamily: valueAlamat ? "Helvetica" : "FuturaBT",
      fontWeight: valueAlamat ? 400 : 700,
      fontSize: 14,
      color: valueAlamat ? "#00193E" : "#aaaaaa",
    }),
    option: (base, state) => ({
      ...base,
      backgroundColor: state.isSelected ? "white" : "white",
      color: "#00193E",
      fontFamily: "Helvetica",
      fontSize: 14,
      fontWeight: 400,
    }),
    menuList: (styles) => ({
      ...styles,
      height: 144
    })
  };

  const locationSearch = (e) => {
    hitLocationSearch(e);
    setValueAlamat(e);
    changePortofolio("almt", e);
  };

  const hitLocationSearch = async (e) => {
    //console.log(e.length)
    // let obj = [];
    // obj = [...obj, {label: e , value: e}]
    if (e.length > 0) {
      try {
        let accessKey = userKey;
        if (!userKey) {
          accessKey =
            JSON.parse(sessionStorage.getItem("keyMember")) ||
            JSON.parse(Cookies.get("keyMember"));
        }
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/lokasi/search`;
        const loadData = await fetch(endpoint, {
          method: "POST",
          headers: {
            AccessKey_App: accessKey,
            ...defaultHeaders
          },
          body: new URLSearchParams({
            n: e,
            // Limit: 3,
          }),
        });
        const response = await loadData.json();
        if (!response.IsError) {
          let obj = [];
          response.Data[1].map((data, index) => {
            obj = [...obj, { label: data.n, value: data.n_prop }];
          });
          setSelectValue(obj);
        } else {
          // console.log(response.ErrToUser);
          // throw {
          //     message: response.ErrToUser,
          // };
        }
      } catch (error) {
        console.log(error.message);
      } finally {
      }
    } else {
      //selectInputRef.current.clearValue();
    }
  };

  const changeAlamat = (e) => {
    if (e) {
      console.log("onchange", e.label !== undefined ? e.label : e);
      changePortofolio("almt", e.label);
      setValueAlamat(null);
    }
  };

  return (
    <Layout
      title="Hasil Profesional - Dashboard Member | BTN Properti"
      isLoaderOpen={isLoading}
    >
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="profesional" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8">
              <h5 className="title_akun">Profesional</h5>
              <div className="tab-menu d-md-flex justify-content-between align-items-start">
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link active"
                      data-bs-toggle="tab"
                      data-bs-target="#tab-info"
                      role="tab"
                    >
                      <span>Informasi Bisnis</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link"
                      data-bs-toggle="tab"
                      data-bs-target="#tab-Portofolio"
                      onClick={() => {
                        getListDataPortofolio();
                      }}
                      style={
                        blockPortofolio
                          ? {
                            pointerEvents: "none",
                          }
                          : {}
                      }
                      role="tab"
                    >
                      <span>Portofolio</span>
                    </a>
                  </li>
                </ul>
                {/* <a href=""><i className="bi bi-eye"></i> Tandai Semua Dilihat</a> */}
              </div>
              <div className="tab-content revamp-ajukan-kprtipe">
                <div
                  className="tab-pane fade show active revamp-ajukan-kprtipe"
                  id="tab-info"
                  role="tabpanel"
                >
                  <Informasibisnis
                    closeSuccessModal={closeSuccessModal}
                    modalSuc={modalSuc}
                    setModalSuc={setModalSuc}
                    updateData={updateData}
                    data={profileMember}
                    validateData={validateData}
                    dataCheckbox={idCheckbox}
                    cekedLayanan={checkedChange}
                    changeProfil={resultListData}
                    changeJasa={handleInputChange}
                    submitProfil={submitData}
                  />
                </div>

                <div
                  className="tab-pane fade  revamp-ajukan-kprtipe"
                  id="tab-Portofolio"
                  role="tabpanel"
                >
                  <div className="d-flex justify-content-between align-items-center mb-3 title_profesional_portfolio">
                    <h5 className=" mb-0 daftar-portofolio"
                      // style={{
                      //   fontFamily: "Helvetica",
                      //   fontWeight: 700,
                      //   fontSize: 20,
                      //   width: 158,
                      //   height: 30,
                      // }}
                      >
                      Daftar Portofolio
                    </h5>
                    <div data-bs-toggle="modal" data-bs-target="#modal-tambah" className="mb-0"
                      onClick={() => {
                        setValidatePortofolio(false);
                        clearText();
                      }}
                    >
                      <a className="btn btn-main tambah-potofolio"
                        // style={{
                        //   fontFamily: "Helvetica",
                        //   fontWeight: 700,
                        //   width: 187,
                        //   height: 48,
                        //   padding: "12px 0px",
                        // }}
                      >
                        <svg width="24" height="24" viewBox="5 1 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M19 13H13V19H11V13H5V11H11V5H13V11H19V13Z" fill="white" />
                        </svg>
                        Tambah Portofolio
                      </a>
                    </div>
                  </div>
                  <div className="row list_property">
                    {listPortofolio.dataList?.length > 0 &&
                      listPortofolio?.dataList.map((data, index) => (
                        <div key={index} className="col-6 col-md-4 mb-3">
                          <ProfesionalPortofolio data={data} />
                        </div>
                      ))}
                      <div className="col-12">
                      <PaginationNew
                        length={paging?.JmlHalTotal}
                        current={Number(paging?.HalKe)}
                        onChangePage={(e) => {
                          setPage(e);
                          window.scrollTo(0, 0);
                        }}

                        // paging={paging}
                        // startRange={startRange}
                        // setPage={setPage}
                        // endRange={endRange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* {modalSucces && (
        <div className={styles.boxModal}>
          <div className={styles.boxModal__Modal}>
            <div className="text-center">
              <button
                type="button"
                style={{ background: "none", border: "none" }}
                data-bs-dismiss="modal"
                aria-label="Close"
                data-bs-target="#modalSuccessRegister"
                onClick={() => {
                  setModalSucces(false);
                  router.reload();
                }}
              >
                <svg style={{ marginLeft: 485 }} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                    fill="#0061A7"
                  />
                </svg>
              </button>
              <h4 className="modal-title mb-3">Terima Kasih</h4>

              {loading && <div className="spinner-border text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />}
              <Lottie options={defaultOptions} height={250} width={350} isStopped={false} isPaused={false} />
              <div className="desc text-center mb-5" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                Data berhasil disimpan.
              </div>
              <div className={styles.boxModal__Modal__Footer}>
                <Link href="/">
                  <button
                    type="buttom"
                    className="btn btn-main form-control btn-back px-5"
                    style={{
                      width: "490px",
                      height: "48px",
                      fontSize: "14px",
                      fontFamily: "Helvetica",
                      fontWeight: 700,
                      backgroundColor: "#0061A7",
                    }}
                  >
                    Kembali ke Beranda
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      )} */}

      {/* modal Portofolio */}

      <div
        className="modal fade"
        id="modal-tambah"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myLargeModalLabel"
        aria-hidden="true"
        style={{ fontFamily: "Helvetica" }}
      >
        <div className="modal-dialog modal-dialog-portopolio modal-dialog-centered" id="modal-tambah-portofolio">
          <div className="modal-content modal_portopolio">
            <div className="close-modal" data-bs-dismiss="modal">
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <h5
              className="modal-title"
              id="profesional-portofolio1"
              // style={{ marginLeft: "25px", textAlign: "left" }}
            >
              Unggah Portofolio
            </h5>
            <div class="container">
              <div className="row vertical-divider g-md-0 d-flex">
                <div id="modal-photo-web" className="col-md-3 col-12 custom-modal-photo">
                  <h5
                    className="modal-title"
                    id="profesional-unggah"
                  >
                    Panduan Mengunggah Foto
                  </h5>
                  <p className="profesional-foto">Foto yang tidak memenuhi panduan ini akan dihapus</p>
                  <div className="row g-md-0 justify-content-md-evenly surface-duo">
                    <div className="col-md-3 profesional-foto-child">
                      <svg
                        width="55"
                        height="53"
                        viewBox="0 0 55 53"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0_3726_2513)">
                          <path
                            d="M6.06679 44.3747C4.35993 44.3747 2.96846 42.988 2.96846 41.287V33.4197L10.8256 25.8668L19.6846 32.449C20.3432 32.939 21.2709 32.8003 21.7625 32.1439L33.6735 16.2153L44.5269 26.782V32.1254C45.5566 32.1254 46.5492 32.2733 47.4954 32.5599V6.04605C47.4954 2.70871 44.7774 0 41.4286 0H6.06679C2.718 0 0 2.70871 0 6.04605V41.287C0 44.6243 2.718 47.333 6.06679 47.333H35.2134C34.7403 46.4178 34.397 45.4194 34.2115 44.3747H6.06679ZM2.96846 6.04605C2.96846 4.34502 4.35993 2.95831 6.06679 2.95831H41.4286C43.1354 2.95831 44.5269 4.34502 44.5269 6.04605V22.6403L34.5362 12.9241C34.2301 12.6283 33.8126 12.4711 33.3859 12.5081C32.9592 12.5451 32.5696 12.7577 32.3098 13.0998L20.269 29.1949L11.5955 22.742C11.0111 22.3075 10.2041 22.363 9.67532 22.8622L2.96846 29.315V6.04605Z"
                            fill="#666666"
                          />
                          <path
                            d="M17.8011 17.9994C21.2149 17.9994 23.9978 15.226 23.9978 11.8239C23.9978 8.42186 21.2149 5.64844 17.8011 5.64844C14.3874 5.64844 11.6045 8.42186 11.6045 11.8239C11.6045 15.226 14.3781 17.9994 17.8011 17.9994ZM17.8011 8.60675C19.5822 8.60675 21.0293 10.0489 21.0293 11.8239C21.0293 13.5989 19.5822 15.0411 17.8011 15.0411C16.0201 15.0411 14.5729 13.5989 14.5729 11.8239C14.5729 10.0489 16.0201 8.60675 17.8011 8.60675Z"
                            fill="#666666"
                          />
                          <path
                            d="M47.4949 32.5508V35.7495C50.1294 36.8958 51.9847 39.5213 51.9847 42.5628C51.9847 46.6582 48.6359 49.9863 44.5264 49.9956C42.2352 49.9956 40.1851 48.9602 38.8214 47.3423H35.2129C36.9569 50.7074 40.4726 53.0094 44.5264 53.0094C50.3057 53.0094 54.9995 48.3223 54.9995 42.5721C54.9995 37.8388 51.827 33.8358 47.4949 32.5508Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5268 35.131V32.1172C38.7476 32.1172 34.0537 36.8043 34.0537 42.5545C34.0537 43.1739 34.1094 43.7748 34.2114 44.3665H37.3005C37.152 43.7841 37.0778 43.1739 37.0778 42.5545C37.0686 38.4683 40.4174 35.131 44.5268 35.131Z"
                            fill="#666666"
                          />
                          <path
                            d="M37.301 44.375H34.2119C34.3974 45.4197 34.7407 46.4181 35.2138 47.3333H38.8223C38.108 46.4828 37.5793 45.4844 37.301 44.375Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5273 32.1172V35.131C45.5848 35.131 46.5867 35.3529 47.4958 35.7504V32.5517C46.5589 32.2744 45.557 32.1264 44.5273 32.1172Z"
                            fill="#666666"
                          />
                          <path
                            d="M49.3505 39.624C48.8866 39.1618 48.1352 39.1618 47.6714 39.624L43.7289 43.553L42.5416 42.3697C42.0777 41.9075 41.3263 41.9075 40.8625 42.3697C40.3987 42.8319 40.3987 43.5808 40.8625 44.043L42.6436 45.818C42.9312 46.1046 43.3208 46.2617 43.7289 46.2617C44.1371 46.2617 44.5174 46.1046 44.8143 45.818L49.3597 41.2881C49.8143 40.8351 49.8143 40.0863 49.3505 39.624Z"
                            fill="#666666"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_3726_2513">
                            <rect width="55" height="53" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                    <div className="col-md-7 ">
                      <h5
                        className="modal-title"
                        style={{
                          color: "#00193E",
                          textAlign: "left",
                          marginBottom: "0px",
                          paddingTop: "0px",
                          fontSize: "14px",
                        }}
                      >
                        Benar
                      </h5>
                      <ul style={{marginLeft: -16, marginTop: 2, fontSize: 10, lineHeight: "130%", color: "#000"}}>
                        <li>Foto besar</li>
                        <div>(lebar 1000px atau lebih)</div>
                        <li>Format file JPG, GIF, PNG, atau 1-Page TIFF</li>
                        <li>Foto berkualitas bagus</li>
                      </ul>
                    </div>
                  </div>
                  <div className="row g-md-0 justify-content-md-evenly surface-duo">
                    <div className="col-md-3 profesional-foto-child">
                      <svg
                        width="55"
                        height="53"
                        viewBox="0 0 55 53"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0_3726_2489)">
                          <path
                            d="M46.4751 42.6903L48.284 40.8876C48.7664 40.4069 48.7664 39.6303 48.284 39.1496C47.8016 38.6689 47.0224 38.6689 46.54 39.1496L44.7311 40.9523L42.913 39.1404C42.4213 38.6689 41.6421 38.6874 41.169 39.1681C40.7052 39.6488 40.7052 40.3976 41.1783 40.8784L42.9872 42.6811L41.169 44.4931C40.9371 44.7334 40.8165 45.0385 40.8258 45.3713C40.835 45.6949 40.9649 46.0092 41.2061 46.2311C41.4473 46.4622 41.7534 46.5731 42.0595 46.5731C42.3656 46.5731 42.681 46.4529 42.9222 46.2218L44.7311 44.4191L46.5493 46.2311C46.7905 46.4622 47.0966 46.5824 47.4306 46.5731C47.7552 46.5639 48.0706 46.4344 48.2933 46.1941C48.7571 45.7134 48.7571 44.9645 48.284 44.4838L46.4751 42.6903Z"
                            fill="#666666"
                          />
                          <path
                            d="M6.06679 44.3747C4.35993 44.3747 2.96846 42.988 2.96846 41.2869V33.4197L10.8256 25.8667L19.6846 32.449C20.3432 32.939 21.2709 32.8003 21.7625 32.1439L33.6735 16.2152L44.5269 26.782V32.1254C45.5566 32.1254 46.5492 32.2733 47.4954 32.5599V6.04605C47.4954 2.7087 44.7774 0 41.4286 0H6.06679C2.718 0 0 2.7087 0 6.04605V41.2869C0 44.6243 2.718 47.333 6.06679 47.333H35.2134C34.7403 46.4178 34.397 45.4193 34.2115 44.3747H6.06679ZM2.96846 6.04605C2.96846 4.34502 4.35993 2.95831 6.06679 2.95831H41.4286C43.1354 2.95831 44.5269 4.34502 44.5269 6.04605V22.6403L34.5362 12.9241C34.2301 12.6283 33.8126 12.4711 33.3859 12.5081C32.9592 12.5451 32.5696 12.7577 32.3098 13.0998L20.269 29.1948L11.5955 22.742C11.0111 22.3075 10.2041 22.363 9.67532 22.8622L2.96846 29.315V6.04605Z"
                            fill="#666666"
                          />
                          <path
                            d="M17.8012 17.9994C21.2149 17.9994 23.9978 15.226 23.9978 11.8239C23.9978 8.42186 21.2149 5.64844 17.8012 5.64844C14.3874 5.64844 11.6045 8.42186 11.6045 11.8239C11.6045 15.226 14.3781 17.9994 17.8012 17.9994ZM17.8012 8.60675C19.5822 8.60675 21.0294 10.0489 21.0294 11.8239C21.0294 13.5989 19.5822 15.0411 17.8012 15.0411C16.0201 15.0411 14.573 13.5989 14.573 11.8239C14.573 10.0489 16.0201 8.60675 17.8012 8.60675Z"
                            fill="#666666"
                          />
                          <path
                            d="M47.4949 32.5508V35.7495C50.1294 36.8958 51.9847 39.5213 51.9847 42.5628C51.9847 46.6582 48.6359 49.9863 44.5264 49.9956C42.2352 49.9956 40.1851 48.9602 38.8214 47.3423H35.2129C36.9569 50.7074 40.4726 53.0094 44.5264 53.0094C50.3057 53.0094 54.9995 48.3223 54.9995 42.5721C54.9995 37.8388 51.827 33.8358 47.4949 32.5508Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5268 35.131V32.1172C38.7476 32.1172 34.0537 36.8043 34.0537 42.5545C34.0537 43.1739 34.1094 43.7748 34.2114 44.3665H37.3005C37.152 43.784 37.0778 43.1739 37.0778 42.5545C37.0686 38.4683 40.4173 35.131 44.5268 35.131Z"
                            fill="#666666"
                          />
                          <path
                            d="M37.301 44.375H34.2119C34.3974 45.4197 34.7407 46.4181 35.2138 47.3333H38.8223C38.108 46.4828 37.5793 45.4844 37.301 44.375Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5273 32.1172V35.131C45.5849 35.131 46.5867 35.3528 47.4958 35.7504V32.5517C46.5589 32.2743 45.557 32.1264 44.5273 32.1172Z"
                            fill="#666666"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_3726_2489">
                            <rect width="55" height="53" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                    <div className="col-md-7 ">
                      <h5
                        className="modal-title"
                        style={{
                          color: "#00193E",
                          textAlign: "left",
                          marginBottom: "0px",
                          paddingTop: "0px",
                          fontSize: "14px",
                        }}
                      >
                        Salah
                      </h5>
                      <ul style={{marginLeft: -16, marginTop: 2, fontSize: 10, lineHeight: "130%", color: "#000"}}>
                        <li>Foto komersil atau perkantoran</li>
                        <li>Format file PDF, multi-Page TIFF atau EPS</li>
                        <li>Foto berkualitas rendah</li>
                      </ul>
                    </div>
                  </div>
                </div>
                

                <div id="modal-input-container" className="col-md-5 col-12">
                  <div id="modal-portofolio-input" >
                    <label style={{ marginTop: "20px", color: "#00193E" }}>
                      Judul Portopolio<span className="modal-required">*</span>
                    </label>
                    <input
                      type="text"
                      style={{ height: "43.4px" }}
                      className="floating-label-field custom-plc-profesional"
                      name="portoffolio[n]"
                      placeholder="Pilih Jenis Dokumen"
                      value={portofolio?.n}
                      onChange={(e) => changePortofolio("n", e.target.value)}
                    />
                    {validatePortofolio && !portofolio?.n && (
                      <p
                        style={{
                          color: "#dc3545",
                          fontFamily: "Helvetica",
                          fontSize: 12,
                          transform: "translateY(5px)",
                        }}
                      >
                        Isian tidak boleh kosong
                      </p>
                    )}
                    <label style={{ color: "#00193E" }}>
                      Deskripsi<span className="modal-required">*</span>
                    </label>
                    <textarea
                      style={{ height: "122px" }}
                      className="floating-label-field custom-plc-profesional"
                      rows="4"
                      name="portofolio[dsk]"
                      placeholder="Tulis deskripsi mengenai portofolio anda"
                      value={portofolio?.dsk}
                      onChange={(e) => changePortofolio("dsk", e.target.value)}
                    ></textarea>
                    {validatePortofolio && !portofolio?.dsk && (
                      <p
                        style={{
                          color: "#dc3545",
                          fontFamily: "Helvetica",
                          fontSize: 12,
                          transform: "translateY(5px)",
                        }}
                      >
                        Isian tidak boleh kosong
                      </p>
                    )}
                    <label style={{ color: "#00193E" }}>
                      Lokasi<span className="modal-required">*</span>
                    </label>
                    {/* <input type="text" className="floating-label-field custom-plc-profesional" name="portofolio[almt]" placeholder="Pilih Jenis Dokumen" value={portofolio?.almt}
                                    onChange={(e) => changePortofolio("almt", e.target.value)}
                                /> */}
                    <Select
                      options={selectValue}
                      ref={selectInputRef}
                      onChange={changeAlamat}
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                      }}
                      styles={customStyles}
                      onInputChange={(e, action) => {
                        if (action.action === "input-change") {
                          locationSearch(e);
                        }
                      }}
                      placeholder={
                        valueAlamat || "Masukkan lokasi dari portofolio"
                      }
                      onFocus={() => {
                        selectInputRef.current.clearValue();
                        setValueAlamat("");
                        changePortofolio("almt", "");
                      }}                     
                      blurInputOnSelect
                    ></Select>                    
                    {validatePortofolio && !portofolio?.almt && (
                      <p
                        style={{
                          color: "#dc3545",
                          fontFamily: "Helvetica",
                          fontSize: 12,
                          transform: "translateY(5px)",
                        }}
                      >
                        Isian tidak boleh kosong
                      </p>
                    )}
                    <p style={{ marginTop: "10px" }}>
                      Dengan mengunggah foto, Anda menyetujui bahwa Anda
                      memiliki hak atas gambar tersebut atau bahwa Anda memiliki
                      izin atau lisensi dan setuju untuk memenuhi Syarat &
                      Ketentuan BTN Properti.
                    </p>
                  </div>
                </div>
                <div
                  id="modal-portofolio-gambar"
                  className={isTabletOrDesktop ? "col-md-4 col-12" : "col-md-4"}
                  // style={isTabletOrDesktop ? { marginTop: '20px' } : {}}
                >
                  <div
                    id="image-div" 
                    // style={{ width: "304px", height: "269px", marginLeft: "30px" }}
                    className={haveImage > 1 && `table-scroll`}
                  >
                    {imageConverted ||
                      imageConverted2 ||
                      imageConverted3 ||
                      imageConverted4 ||
                      imageConverted5 ? (
                      <div
                        className="me-3"
                        style={{ width: "100%", height: "100%" }}
                      >
                        {/* <div className={"photo_input"}> */}
                        {selectedImage && (
                          <div style={{ animation: animStr(1) }}>
                            <div
                              style={{
                                position: "relative",
                                top: "10px",
                                left: "90%",
                              }}
                            >
                              <button
                                onClick={() => {
                                  imageDelete(1);
                                }}
                                type="button"
                                class="btn-close"
                                aria-label="Close"
                              />
                            </div>
                            <div
                              style={{ height: "269px", marginTop: "-20px" }}
                            >
                              <img
                                src={imageConverted}
                                //className="img-fluid me-3" width="100px" height="100px"
                                style={{
                                  height: "100%",
                                  width: "100%",
                                  border: "1px solid #aaaaaa",
                                  borderRadius: "8px",
                                  objectFit:"cover"
                                }}
                                alt="Thumb"
                              />
                            </div>
                          </div>
                        )}
                        {selectedImage2 && (
                          <div style={{ animation: animStr(1) }}>
                            <div
                              style={{
                                position: "relative",
                                top: "10px",
                                left: "90%",
                              }}
                            >
                              <button
                                onClick={() => {
                                  imageDelete(2);
                                }}
                                type="button"
                                class="btn-close"
                                aria-label="Close"
                              />
                            </div>
                            <div
                              style={{ height: "269px", marginTop: "-20px" }}
                            >
                              <img
                                src={imageConverted2}
                                // className="img-fluid me-3" width="100px" height="100px"
                                style={{
                                  height: "100%",
                                  width: "100%",
                                  border: "1px solid #aaaaaa",
                                  borderRadius: "8px",
                                  objectFit:"cover"
                                }}
                                alt="Thumb"
                              />
                            </div>
                          </div>
                        )}
                        {selectedImage3 && (
                          <div style={{ animation: animStr(1) }}>
                            <div
                              style={{
                                position: "relative",
                                top: "10px",
                                left: "90%",
                              }}
                            >
                              <button
                                onClick={() => {
                                  imageDelete(3);
                                }}
                                type="button"
                                class="btn-close"
                                aria-label="Close"
                              />
                            </div>
                            <div
                              style={{ height: "269px", marginTop: "-20px" }}
                            >
                              <img
                                src={imageConverted3}
                                // className="img-fluid me-3" width="100px" height="100px"
                                style={{
                                  height: "100%",
                                  width: "100%",
                                  border: "1px solid #aaaaaa",
                                  borderRadius: "8px",
                                  objectFit:"cover"
                                }}
                                alt="Thumb"
                              />
                            </div>
                          </div>
                        )}
                        {selectedImage4 && (
                          <div style={{ animation: animStr(1) }}>
                            <div
                              style={{
                                position: "relative",
                                top: "10px",
                                left: "90%",
                              }}
                            >
                              <button
                                onClick={() => {
                                  imageDelete(4);
                                }}
                                type="button"
                                class="btn-close"
                                aria-label="Close"
                              />
                            </div>
                            <div
                              style={{ height: "269px", marginTop: "-20px" }}
                            >
                              <img
                                src={imageConverted4}
                                // className="img-fluid me-3" width="100px" height="100px"
                                style={{
                                  height: "100%",
                                  width: "100%",
                                  border: "1px solid #aaaaaa",
                                  borderRadius: "8px",
                                  objectFit:"cover"
                                }}
                                alt="Thumb"
                              />
                            </div>
                          </div>
                        )}
                        {selectedImage5 && (
                          <div style={{ animation: animStr(1) }}>
                            <div
                              style={{
                                position: "relative",
                                top: "10px",
                                left: "90%",
                              }}
                            >
                              <button
                                onClick={() => {
                                  imageDelete(5);
                                }}
                                type="button"
                                class="btn-close"
                                aria-label="Close"
                              />
                            </div>
                            <div
                              style={{ height: "269px", marginTop: "-20px" }}
                            >
                              <img
                                src={imageConverted5}
                                // className="img-fluid me-3" width="100px" height="100px"
                                style={{
                                  height: "100%",
                                  width: "100%",
                                  border: "1px solid #aaaaaa",
                                  borderRadius: "8px",
                                  objectFit:"cover"
                                }}
                                alt="Thumb"
                              />
                            </div>
                          </div>
                        )}
                        {/* <button onClick={removeSelectedImage}>
                                                Remove
                                            </button> */}
                      </div>
                    ) : (
                      <img
                        src="/images/icons/field_empty.png"
                        className="img-fluid me-3"
                        style={{ height: "100%", width: "100%" }}
                      />
                    )}
                  </div>
                  {/* </div> */}
                  <div id="btn-tambah-foto" className="profesional-ml">
                    <label htmlFor="upload">
                      <div
                        className="btn btn-outline-main btn_rounded tambah-btn-custom"
                        // style={{ width: "266.27px", height: "42.16px", display: "flex", alignItems: "center", justifyContent: "center", fontFamily: "Helvetica", fontWeight: 700 }}
                      >
                        + Tambah Foto
                      </div>
                    </label>
                    <input
                      type="file"
                      className="form-control"
                      accept="image/jpeg,image/jpg,image/png,application/pdf"
                      required=""
                      id="upload"
                      onChange={(e) => imageChange(e)}
                      multiple
                      hidden
                      disabled={imageConverted5}
                    />
                  </div>
                  <div id="modal-photo-mobile" className="col-12" style={{paddingBottom: "20px"}}>
                    <h5
                      className="modal-title"
                      style={{
                        textAlign: "left",
                        fontSize: "18px",
                        marginBottom: "0px",
                        // width: "100%",
                        // marginLeft: "-80px"
                      }}
                    >
                      Panduan Mengunggah Foto
                    </h5>
                  <p>Foto yang tidak memenuhi panduan ini akan dihapus</p>
                  <div className="row g-md-0 flex-around">
                    <div className="col-3">
                      <svg
                        width="55"
                        height="53"
                        viewBox="0 0 55 53"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0_3726_2513)">
                          <path
                            d="M6.06679 44.3747C4.35993 44.3747 2.96846 42.988 2.96846 41.287V33.4197L10.8256 25.8668L19.6846 32.449C20.3432 32.939 21.2709 32.8003 21.7625 32.1439L33.6735 16.2153L44.5269 26.782V32.1254C45.5566 32.1254 46.5492 32.2733 47.4954 32.5599V6.04605C47.4954 2.70871 44.7774 0 41.4286 0H6.06679C2.718 0 0 2.70871 0 6.04605V41.287C0 44.6243 2.718 47.333 6.06679 47.333H35.2134C34.7403 46.4178 34.397 45.4194 34.2115 44.3747H6.06679ZM2.96846 6.04605C2.96846 4.34502 4.35993 2.95831 6.06679 2.95831H41.4286C43.1354 2.95831 44.5269 4.34502 44.5269 6.04605V22.6403L34.5362 12.9241C34.2301 12.6283 33.8126 12.4711 33.3859 12.5081C32.9592 12.5451 32.5696 12.7577 32.3098 13.0998L20.269 29.1949L11.5955 22.742C11.0111 22.3075 10.2041 22.363 9.67532 22.8622L2.96846 29.315V6.04605Z"
                            fill="#666666"
                          />
                          <path
                            d="M17.8011 17.9994C21.2149 17.9994 23.9978 15.226 23.9978 11.8239C23.9978 8.42186 21.2149 5.64844 17.8011 5.64844C14.3874 5.64844 11.6045 8.42186 11.6045 11.8239C11.6045 15.226 14.3781 17.9994 17.8011 17.9994ZM17.8011 8.60675C19.5822 8.60675 21.0293 10.0489 21.0293 11.8239C21.0293 13.5989 19.5822 15.0411 17.8011 15.0411C16.0201 15.0411 14.5729 13.5989 14.5729 11.8239C14.5729 10.0489 16.0201 8.60675 17.8011 8.60675Z"
                            fill="#666666"
                          />
                          <path
                            d="M47.4949 32.5508V35.7495C50.1294 36.8958 51.9847 39.5213 51.9847 42.5628C51.9847 46.6582 48.6359 49.9863 44.5264 49.9956C42.2352 49.9956 40.1851 48.9602 38.8214 47.3423H35.2129C36.9569 50.7074 40.4726 53.0094 44.5264 53.0094C50.3057 53.0094 54.9995 48.3223 54.9995 42.5721C54.9995 37.8388 51.827 33.8358 47.4949 32.5508Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5268 35.131V32.1172C38.7476 32.1172 34.0537 36.8043 34.0537 42.5545C34.0537 43.1739 34.1094 43.7748 34.2114 44.3665H37.3005C37.152 43.7841 37.0778 43.1739 37.0778 42.5545C37.0686 38.4683 40.4174 35.131 44.5268 35.131Z"
                            fill="#666666"
                          />
                          <path
                            d="M37.301 44.375H34.2119C34.3974 45.4197 34.7407 46.4181 35.2138 47.3333H38.8223C38.108 46.4828 37.5793 45.4844 37.301 44.375Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5273 32.1172V35.131C45.5848 35.131 46.5867 35.3529 47.4958 35.7504V32.5517C46.5589 32.2744 45.557 32.1264 44.5273 32.1172Z"
                            fill="#666666"
                          />
                          <path
                            d="M49.3505 39.624C48.8866 39.1618 48.1352 39.1618 47.6714 39.624L43.7289 43.553L42.5416 42.3697C42.0777 41.9075 41.3263 41.9075 40.8625 42.3697C40.3987 42.8319 40.3987 43.5808 40.8625 44.043L42.6436 45.818C42.9312 46.1046 43.3208 46.2617 43.7289 46.2617C44.1371 46.2617 44.5174 46.1046 44.8143 45.818L49.3597 41.2881C49.8143 40.8351 49.8143 40.0863 49.3505 39.624Z"
                            fill="#666666"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_3726_2513">
                            <rect width="55" height="53" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                    <div className="col-md-7 col-7" style={{padding: 0}}>
                      <h5
                        className="modal-title"
                        style={{
                          color: "#00193E",
                          textAlign: "left",
                          marginBottom: "0px",
                          paddingTop: "0px",
                          fontSize: "14px",
                        }}
                      >
                        Benar
                      </h5>
                      <ul style={{marginLeft: -16, marginTop: 2, fontSize: 10, lineHeight: "130%", color: "#000"}}>
                        <li>Foto besar</li>
                        <div>(lebar 1000px atau lebih)</div>
                        <li>Format file JPG, GIF, PNG, atau 1-Page TIFF</li>
                        <li>Foto berkualitas bagus</li>
                      </ul>
                    </div>
                  </div>
                  <div className="row g-md-0 flex-around">
                    <div className="col-3 ">
                      <svg
                        width="55"
                        height="53"
                        viewBox="0 0 55 53"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0_3726_2489)">
                          <path
                            d="M46.4751 42.6903L48.284 40.8876C48.7664 40.4069 48.7664 39.6303 48.284 39.1496C47.8016 38.6689 47.0224 38.6689 46.54 39.1496L44.7311 40.9523L42.913 39.1404C42.4213 38.6689 41.6421 38.6874 41.169 39.1681C40.7052 39.6488 40.7052 40.3976 41.1783 40.8784L42.9872 42.6811L41.169 44.4931C40.9371 44.7334 40.8165 45.0385 40.8258 45.3713C40.835 45.6949 40.9649 46.0092 41.2061 46.2311C41.4473 46.4622 41.7534 46.5731 42.0595 46.5731C42.3656 46.5731 42.681 46.4529 42.9222 46.2218L44.7311 44.4191L46.5493 46.2311C46.7905 46.4622 47.0966 46.5824 47.4306 46.5731C47.7552 46.5639 48.0706 46.4344 48.2933 46.1941C48.7571 45.7134 48.7571 44.9645 48.284 44.4838L46.4751 42.6903Z"
                            fill="#666666"
                          />
                          <path
                            d="M6.06679 44.3747C4.35993 44.3747 2.96846 42.988 2.96846 41.2869V33.4197L10.8256 25.8667L19.6846 32.449C20.3432 32.939 21.2709 32.8003 21.7625 32.1439L33.6735 16.2152L44.5269 26.782V32.1254C45.5566 32.1254 46.5492 32.2733 47.4954 32.5599V6.04605C47.4954 2.7087 44.7774 0 41.4286 0H6.06679C2.718 0 0 2.7087 0 6.04605V41.2869C0 44.6243 2.718 47.333 6.06679 47.333H35.2134C34.7403 46.4178 34.397 45.4193 34.2115 44.3747H6.06679ZM2.96846 6.04605C2.96846 4.34502 4.35993 2.95831 6.06679 2.95831H41.4286C43.1354 2.95831 44.5269 4.34502 44.5269 6.04605V22.6403L34.5362 12.9241C34.2301 12.6283 33.8126 12.4711 33.3859 12.5081C32.9592 12.5451 32.5696 12.7577 32.3098 13.0998L20.269 29.1948L11.5955 22.742C11.0111 22.3075 10.2041 22.363 9.67532 22.8622L2.96846 29.315V6.04605Z"
                            fill="#666666"
                          />
                          <path
                            d="M17.8012 17.9994C21.2149 17.9994 23.9978 15.226 23.9978 11.8239C23.9978 8.42186 21.2149 5.64844 17.8012 5.64844C14.3874 5.64844 11.6045 8.42186 11.6045 11.8239C11.6045 15.226 14.3781 17.9994 17.8012 17.9994ZM17.8012 8.60675C19.5822 8.60675 21.0294 10.0489 21.0294 11.8239C21.0294 13.5989 19.5822 15.0411 17.8012 15.0411C16.0201 15.0411 14.573 13.5989 14.573 11.8239C14.573 10.0489 16.0201 8.60675 17.8012 8.60675Z"
                            fill="#666666"
                          />
                          <path
                            d="M47.4949 32.5508V35.7495C50.1294 36.8958 51.9847 39.5213 51.9847 42.5628C51.9847 46.6582 48.6359 49.9863 44.5264 49.9956C42.2352 49.9956 40.1851 48.9602 38.8214 47.3423H35.2129C36.9569 50.7074 40.4726 53.0094 44.5264 53.0094C50.3057 53.0094 54.9995 48.3223 54.9995 42.5721C54.9995 37.8388 51.827 33.8358 47.4949 32.5508Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5268 35.131V32.1172C38.7476 32.1172 34.0537 36.8043 34.0537 42.5545C34.0537 43.1739 34.1094 43.7748 34.2114 44.3665H37.3005C37.152 43.784 37.0778 43.1739 37.0778 42.5545C37.0686 38.4683 40.4173 35.131 44.5268 35.131Z"
                            fill="#666666"
                          />
                          <path
                            d="M37.301 44.375H34.2119C34.3974 45.4197 34.7407 46.4181 35.2138 47.3333H38.8223C38.108 46.4828 37.5793 45.4844 37.301 44.375Z"
                            fill="#666666"
                          />
                          <path
                            d="M44.5273 32.1172V35.131C45.5849 35.131 46.5867 35.3528 47.4958 35.7504V32.5517C46.5589 32.2743 45.557 32.1264 44.5273 32.1172Z"
                            fill="#666666"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_3726_2489">
                            <rect width="55" height="53" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                    <div className="col-md-7 col-7" style={{padding: 0}}>
                      <h5
                        className="modal-title"
                        style={{
                          color: "#00193E",
                          textAlign: "left",
                          marginBottom: "0px",
                          paddingTop: "0px",
                          fontSize: "14px",
                        }}
                      >
                        Salah
                      </h5>
                      <ul style={{marginLeft: -16, marginTop: 2, fontSize: 10, lineHeight: "130%", color: "#000"}}>
                        <li>Foto komersil atau perkantoran</li>
                        <li>Format file PDF, multi-Page TIFF atau EPS</li>
                        <li>Foto berkualitas rendah</li>
                      </ul>
                    </div>
                  </div>
                  </div>
                  <div
                    ref={modalRef}
                    style={{ display: "none" }}
                  />
                  <div id="btn-submit-foto" className="profesional-ml">
                    <div
                      onClick={() => submitDataPortofolio()} // -----> nyenggol gabisa submit !
                      // onClick={() => {
                      //   setModalSucces(true);
                      //   //router.reload();
                      // }}
                      className="btn btn-main btn_rounded btn-submit-custom"
                      // style={{ width: "266.27px", height: "42.16px", display: "flex", alignItems: "center", justifyContent: "center", fontFamily: "Helvetica", fontWeight: 700 }}
                    >
                      Submit
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {modalSucces && (
                    <div className={styles.boxModal}>
                      <div className={styles.boxModal__Modal}>
                        <div className="text-center">
                          <button
                            type="button"
                            style={{ background: "none", border: "none" }}
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            data-bs-target="#modalSuccessRegister"
                            onClick={() => {
                              setModalSucces(false);
                              //router.reload();
                            }}
                          >
                            <svg style={{ marginLeft: 485 }} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                                fill="#0061A7"
                              />
                            </svg>
                          </button>
                          <h4 className="modal-title mb-3">Terima Kasih</h4>

                          {loading && <div className="spinner-border text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />}
                          {isTabletOrDesktop ? (<Lottie options={defaultOptions} isStopped={false} isPaused={false} />) : (<Lottie options={defaultOptions} height={250} width={350} isStopped={false} isPaused={false} />)}
                          
                          <div className="desc text-center mb-5" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                            Data berhasil disimpan.
                          </div>
                          <div className={styles.boxModal__Modal__Footer}>
                            <button
                              type="buttom"
                              className="btn btn-main form-control btn-back px-5"
                              style={{
                                width: "490px",
                                height: "48px",
                                fontSize: "14px",
                                fontFamily: "Helvetica",
                                fontWeight: 700,
                                backgroundColor: "#0061A7",
                              }}
                              onClick={() => {
                                window.location.href = `${window.location.origin}/`
                                // setModalSucces(false);
                                // router.push('/');
                              }}
                            >
                              Kembali ke Beranda
                            </button>
                            {/* <Link href="/">
                            </Link> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
          </div>
        </div>
      </div>
      
    </Layout>
  );
};

function isValidJsonString(str) {
  try {
      JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}

export default (withAuth(Profesional));
// export default withAuth(Profesional);
