import { useRouter } from "next/router";
import Link from "next/link";
import MenuMember from "../../../components/MenuMember";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import { useEffect, useState } from "react";
import { useAppContext } from "../../../context";
import axios from "axios";
import withAuth from "../../../helpers/withAuth";
import parse from "html-react-parser";
import { deleteNotification } from "../../../services/member";
import { defaultHeaders } from "../../../utils/defaultHeaders";

const index = () => {
  const { userKey, userProfile } = useAppContext();

  const [datas, setDatas] = useState([]);
  const [psn, setPsn] = useState("");
  const [isUserReply, setUserReply] = useState(false);
  const [isSubmit, setSubmit] = useState(false);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  const { id } = router.query;
  const [title, setTitle] = useState(null);

  useEffect(() => {
    let tempData = sessionStorage.getItem("detailNotifikasi");
    if (tempData) {
      tempData = JSON.parse(tempData);
    } else {
      router.push("/member/pesan");
    }

    if (tempData["kategori"] === "notifikasi") {
      setTitle(tempData);
    } else {
      router.push("/member/pesan");
    }
  }, []);

  useEffect(() => {
    if (userKey && isUserReply) {
      let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/reply`;
      let body = {
        i_cht: parseInt(id),
        psn: psn,
      };
      let header = {
        headers: {
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
      };

      axios
        .post(endpoint, new URLSearchParams(body), header)
        .then((res) => {
          if (res.data.isError) {
            console.log(res.data.ErrorToDev);
            // return console.log(res.data.ErrToUser);
          }
        })
        .catch((err) => {})
        .finally(() => {
          getConversation();
        });
    }
  }, [psn]);

  useEffect(() => {
    deleteNotification(id, userProfile.id)
      .then((res) => {})
      .catch((err) => {});
  }, [id]);

  useEffect(() => {
    if (userKey) {
      getConversation();
    }
  }, [isSubmit]);

  const getConversation = async () => {
    let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/show/conversation`;
    let body = {
      i_cht: parseInt(id),
      // Limit: 10, // optional
      // Page: 1, // optional
    };
    let header = {
      headers: {
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
    };

    axios
      .post(endpoint, new URLSearchParams(body), header)
      .then((res) => {
        if (res.data.IsError) {
          console.log(res.data.ErrToDev);
          // return console.log(res.data.ErrToUser);
        } else {
          setDatas(res.data.Data.reverse());
        }
      })
      .catch((err) => {
        // console.log(err.message);
      })
      .finally(() => setUserReply(false));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setPsn(e.target[0].value);
    setUserReply(true);
    setSubmit((prev) => !prev);
    e.target[0].value = "";
  };

  function createMarkup(content) {
    return { __html: content };
  }

  return (
    <Layout title="Notifikasi - Dashboard Member | BTN Properti" isLoaderOpen={isUserReply}>
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="pesan" />
            </div>
            <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 detail_pesan">
              <h4 className="title">{title?.judul}</h4>
              <p
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: 400,
                  color: "#000000",
                }}
              >
                {title?.isi ? parse(title.isi) : ""}
              </p>
              <div 
                id="notikasi-all" 
                className="d-flex flex-column"
                style={{ marginBottom: "24px", maxWidth: "760px" }}
              >
                {datas?.map((data, index) => (
                  <div
                    id="card-jawab-pesan"
                    key={index}
                    className={"card card_jawaban" + `${data.n_pgrm === userProfile.n ? " align-self-end" : " align-self-start"}`}
                    style={{
                      maxWidth: "600px",
                      width: 'auto',
                      minHeight: "216px",
                      marginTop: "10px",
                      float: `${data.n_pgrm === userProfile.n ? "right" : "left"}`,
                    }}
                  >
                    <div
                      className="jawaban"
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        color: "#666",
                      }}
                    >
                      Jawaban:
                    </div>
                    <div className="card-body">
                      <div className="d-flex align-items-center user">
                        <img
                          src={data.n_pgrm === "System" ? "/images/user/avatar.svg" : userProfile?.gmbr ? userProfile?.gmbr : "/images/user/member-profile.jpg"}
                          className="img-fluid"
                          alt="user"
                          style={{ width: 48, height: 48, borderRadius: 30 }}
                        />
                        <div className="ms-3">
                          <h5 className="nama">{data.n_pgrm}</h5>
                          <p className="date">
                            {data.tgl
                              .split("T")
                              .join(", ")
                              .slice(0, data.tgl.length - 2)}
                          </p>
                        </div>
                      </div>
                      <div
                        dangerouslySetInnerHTML={createMarkup(data.psn)}
                        className="pt-3 text-start"
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: 400,
                          color: "#000000",
                        }}
                      />
                    </div>
                  </div>
                ))}
              </div>
              <div id="card-jawab-notfikasi" className="card card_jawaban p-1 mt-3">
                <div className="card-body">
                  <form id="replyPsn" onSubmit={handleSubmit}>
                    <textarea className="form-control" placeholder="Balasan Kamu..."></textarea>
                  </form>
                  <div className="text-end mt-3">
                    <button
                      form="replyPsn"
                      type="submit"
                      className="btn btn-main"
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        width: "123px",
                        height: "48px",
                      }}
                    >
                      Balas Pesan
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withAuth(index);
