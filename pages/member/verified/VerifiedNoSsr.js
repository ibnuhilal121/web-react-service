import React, { useEffect, useState, useRef } from "react";

///----------------------------component----------------------------///
import Layout from "../../../components/Layout";
import MenuMember from "../../../components/MenuMember";
import Breadcrumb from "../../../components/section/Breadcrumb";
import BarsLoader from "../../../components/element/BarsLoader";
// import SearchIcon from '../../../components/element/icons/SearchIcon';
import Unverified from "../../../components/verified/Unverified";
import VerifiedStatus from "../../../components/verified/VerifiedStatus";
import VerifiedFailed from "../../../components/verified/VerifiedFailed";
import VerfiedFailed3Times from "../../../components/verified/VerfiedFailed3Times";
///----------------------------service, helper----------------------------///
import withAuth from "../../../helpers/withAuth";
import { getVerifyStatus,getEkycData } from "../../../services/auth";
import { useAppContext } from "../../../context";


const VerifiedNoSsr = () => {
  ///////-----DECLARING STATES AND VARIABLES-----//////
  const { userProfile,userKey,setUserProfile } = useAppContext();
  const {isVerified = false,counterFailedVerification = 0} = userProfile || {};
  const [showBanner, setShowBanner] = useState(true);
  const [verifyStatus, setVerifyStatus] = useState(null);
  const [timesFailed,setTimesFailed] = useState(null);
  const [isLoading,setIsLoading] = useState(true)

  

  ///////-----USEFFECT-----/////////
  useEffect(()=>{
    if(userKey){
      if(isLoading){ 
        getEkycData(userProfile?.id).then((result)=>{
        const tempProfile = {
          n: userProfile?.n,
          gmbr: userProfile?.gmbr,
          id: userProfile?.id,
          agen: userProfile?.agen,
          idAgen: userProfile?.idAgen,
          namaAgensi: userProfile?.namaAgensi,
          isVerified: result.data?.isVerified,
          currentStep: result.data?.currentStep,
          counterFailedVerification:result.data?.counterFailedVerification,
          email:userProfile?.email
        }
        sessionStorage.setItem("user", JSON.stringify(tempProfile));
        setUserProfile(tempProfile)
        setVerifyStatus(result.data?.isVerified)
        setTimesFailed(result.data?.counterFailedVerification)
        }
      ).finally(()=>{
        setIsLoading(false)
      })
    }
    }
  },[])


  const RenderByStatus = (status,failedCount) => {
    return(
    
    status ? <VerifiedStatus title={"Verifikasi e-KYC Berhasil"} subTitle={"Selamat! Verifikasi e-KYC kamu berhasil. Selanjutnya, kamu dapat mengajukan kredit."} />
    : !status && failedCount < 3 && failedCount > 0 ?
    <VerifiedFailed title={"Mohon Maaf! Verifikasi e-KYC Gagal"} subTitle={"Kamu gagal melakukan verifikasi. Silakan mengulangi proses verifikasi."} />
    : !status && failedCount >= 3 ?
    <VerfiedFailed3Times title={"Verifikasi e-KYC Gagal"} subTitle={"Kamu telah 3 kali melakukan percobaan verifikasi. Data kamu tidak sesuai. Selanjutnya silakan menghubungi kantor cabang untuk melakukan pengajuan."} />
    : <Unverified showBanner={showBanner} setShowBanner={closeBanner} />
    )  
  };

  const closeBanner = ()=>{
    setShowBanner(false);
  }
  return (
    <Layout title="Verified Now - Dashboard Member | BTN Properti" isLoaderOpen={isLoading}>
      <Breadcrumb active="Akun" />
      <div className="dashboard-content mb-5">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-lg-3 col-xl-3 col-md-4 col-sm-4">
              <MenuMember active="verified" />
            </div>
            { isLoading ? 
             <div className="col-xl-9 col-lg-9 col-md-8 px-3 pe-3 d-flex align-items-center justify-content-center">
              <BarsLoader/>
             </div>
            :
            <div className="col-xl-9 col-lg-9 col-md-8 px-3 pe-3">
              {RenderByStatus(isVerified,counterFailedVerification)}
            </div>
            }
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default withAuth(VerifiedNoSsr);



