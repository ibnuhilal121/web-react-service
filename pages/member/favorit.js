import React, { useEffect, useState } from "react";
import DataEmpty from "../../components/data/DataEmpty";

import ItemUnitFavorite from "../../components/data/ItemUnitFavorite";
import ItemPerumahan from "../../components/data/ItemPerumahan";
import Layout from "../../components/Layout";
import MenuMember from "../../components/MenuMember";
import Breadcrumb from "../../components/section/Breadcrumb";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import NotFound from "../../components/element/NotFound";
import data_all from "../../sample_data/data_property_all";
import axios from "axios";
import { useAppContext } from "../../context";
import { useRouter } from "next/router";
import withAuth from "../../helpers/withAuth";
import BarsLoader from "../../components/element/BarsLoader";
import { defaultHeaders } from "../../utils/defaultHeaders";

const Pencarian = () => {
  const router = useRouter();
  const { query, asPath } = router;

  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();

  const { userKey, userFavorites,likeAction } = useAppContext();

  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingUser, setIsLoadingUser] = useState(true);

  // User favorit
  const [itemFavorite, setItemFavorite] = useState([]);
  const [listDataFavo, setListDataFavo] = useState([]);
  const [hitCount, setHitCount] = useState(0)

  // get data perumahan
  const [items, setItems] = useState([]);
  const [itemPerumahan, setItemPerumahan] = useState([])

  const [message, setMessage] = useState("");

  // item after filter like
  const [listFavorite, setListFavorite] = useState([]);
  const [listPerumahan, setListPerumahan] = useState([]);


  useEffect(() => {
    // Get list data
    if (userKey) {
      //getListData();
      //getUserFavorites();
    }
  }, [userKey]);

  useEffect(() => {
    loopfavoriteList()
  }, [itemFavorite]);

  useEffect(() => {
    console.log("ini data vavo", listDataFavo)
  }, [listDataFavo]);

  useEffect(() => {
    // delete favorite function
    if (userKey) {
      //getListData();
      getUserFavorites();
    }
  }, [userKey]);

  useEffect(() => {
    // filter list favorite tipe
    setListFavorite(
      items.filter((data) => itemFavorite.includes(data.id)).map((data) => data)
    );
  }, [itemFavorite, items]);

  useEffect(() => {
    // filter list favorite Perumahan
    setListPerumahan(
      itemPerumahan.filter((data) => itemFavorite.includes(data.ID)).map((data) => data)
    );
    //loopfavoriteList();
  }, [itemFavorite, itemPerumahan]);

  useEffect(() => {
    // filter list favorite
    console.log("List tipe", listFavorite);
  }, [listFavorite]);

  useEffect(() => {
    // filter list favorite
    console.log("List Perumahan", listPerumahan);
  }, [listPerumahan]);

  const getListData = async () => {
    try {
      setIsLoading(true);
      setMessage("");
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/properti/search`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        cancelToken: source.token,
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        console.log("Success get data", response.Data);
        setItems(response.Data);
        //setIsLoading(false);
        //getListDataPerumahan();
      } else {
        throw {
          message: response.ErrToUser,
        };
      }
    } catch (error) {
      setMessage(error.response?.data?.message);
      console.log(error.message);
      setIsLoading(false);
    } finally {
    }
  };

  const getListDataPerumahan = async () => {
    try {
      setIsLoading(true);
      setMessage("");
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/search`;
      const loadData = await fetch(endpoint, {
        method: "GET",
        headers: {
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
        cancelToken: source.token,
      });
      const response = await loadData.json();
      console.log("perumahan",response);
      if (!response.IsError) {
        console.log("Success get data perumahan", response.data);
        setItemPerumahan(response.data);
        setIsLoading(false);
      } else {
        throw {
          message: response.ErrToUser,
        };
      }
    } catch (error) {
      setMessage(error.response?.data?.message);
      console.log(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  const loopfavoriteList = () => {
    setListDataFavo([])
    itemFavorite.map((data,i) => {
      getListDatafavo(data);
    })
  }


  const getListDatafavo = async (value) => {
    try {

      setMessage("");
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/favorit/show`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({i_prpt: value}),
        cancelToken: source.token,
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        if (response.Data.length){
          const data = response.Data[0];
          setListDataFavo((previousInputs) => ([ ...previousInputs, data]));
        }
        setIsLoading(false);
      } else {
          console.log(response.ErrToUser)
          setIsLoading(false);
      }
    } catch (error) {
      setMessage(error.response?.data?.message);
      console.log(error.message);
    } finally {
      setIsLoading(false);
    }
  };


  const getUserFavorites = async () => {
    try {
      setIsLoadingUser(true);
      let accessKey = userKey;
      if (!userKey) {
        accessKey =
          JSON.parse(sessionStorage.getItem("keyMember")) ||
          JSON.parse(Cookies.get("keyMember"));
      }
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/favorit/show/id`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_Member: accessKey,
          ...defaultHeaders
        },
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        console.log(
          "Success favorit",
          response.Data.map((val) => val.i_prpt)
        );
        const value = response.Data.map((val) => val.i_prpt);
        setItemFavorite(value);
        setIsLoadingUser(false);
      } else {
        throw {
          message: response.ErrToUser,
        };
      }
    } catch (error) {
      // console.log(error.message);
      setMessage("Kamu belum memiliki list perumahan favorit");
      setIsLoading(false);
      setIsLoadingUser(false);
    } finally {
      setIsLoadingUser(false);
    }
  };

  const handleLikeButton = (id) =>{
    setListDataFavo(listDataFavo.filter((x)=>{
      return x.id !==id
    }))
    likeAction(id)
  }
  return (
    <Layout
      title="Favorit - Dashboard Member | BTN Properti"
      //isLoaderOpen={isLoading || isLoadingUser}
    >
      <Breadcrumb active="Akun" />
      {/* <div className="dashboard-content mb-5"> */}
      <section className="mb-5" id="propertyPageTipe">
        <div className="container">
          <div className="row">
            <div className="d-none d-md-block col-md-3">
              <MenuMember active="favorit" />
            </div>
            <div className="col-md-9">
              <h5
                style={{
                  fontFamily: "FuturaBT",
                  fontSize: "32px",
                  fontWeight: "700",
                }}
                className="title_akun"
              >
                Favorit
              </h5>
              <div className="#">
                {isLoading ? (
                  <BarsLoader />
                ) : (
                  listDataFavo?.length > 0 || itemPerumahan?.length > 0 ? (
                  listDataFavo?.length > 0 || listPerumahan?.length > 0 ? (
                    <div>
                      <div className="row">
                        {listDataFavo.map((data, index) => (
                          <div key={index} className="col-6 col-md-4 mb-4 card-item">
                            <ItemUnitFavorite data={data} action="false" detail="true" handleLikeButton={()=>{handleLikeButton(data.id)}} />
                          </div>
                        ))}
                      </div>

                      {/* <div className="row">
                        {listPerumahan.map((data,index) => (
                          <div key={index} className="col-md-4 mb-4 card-item">
                            <ItemPerumahan data={data} action="false" detail="true" />
                          </div>
                        ))}
                      </div> */}
                    </div>
                  ) : (
                    /* jika tidak ada data maka ambil tampilan berikut */
                    <DataEmpty title="Kamu belum memasukkan perumahan favorit" />
                  )
                ) : (
                  /* jika tidak ada data maka ambil tampilan berikut */
                  <DataEmpty title="Kamu belum memasukkan perumahan favorit" />
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* </div> */}
    </Layout>
  );
};

export default withAuth(Pencarian);
