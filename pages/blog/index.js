/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from "react";
import styles from "../../styles/News.module.scss";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";

import data_blog from "../../sample_data/data_blog";
import ItemBlogVertical from "../../components/data/ItemBlogVertical";
import ItemBlogHorizontal from "../../components/data/ItemBlogHorizontal";
import Pagination from "../../components/data/Pagination";
import Link from "next/link";
import { useRouter } from "next/router";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import PaginationNew from "../../components/data/PaginationNew";
import SearchIcon from "../../components/element/icons/SearchIcon";
import Lottie from "react-lottie";
import succes from "../../public/succes_animation.json";
import { appLogin } from "../../services/master";

export default function index() {
  const router = useRouter();
  const { kategori, sort, order, pg="1" } = router.query;
  const idKategori = kategori ? kategori : '';
  const idSort = sort;
  const [blogs, setBlogs] = useState([]);
  const [paging, setPaging] = useState(null);
  const [page, setPage] = useState(pg);
  const [categories, setCategories] = useState([]);
  const [subCategories,setSubCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(idKategori);
  const [selectedTab,setSelectedTab] = useState(null);
  const [loadedData, setLoadedData] = useState(false);
  const [loadedCategory, setLoadedCategory] = useState(false);
  const [email, setEmail] = useState("");
  const [sortBy, setSortBy] = useState(
    idSort === "jm_vw" ? "jm_vw DESC" : "t_pst DESC"
  );
  const [reload, setReload] = useState(false);
  const [filter, setFilter] = useState(false);
  const [succesModal ,setSuccesModal] = useState(false);

  const [isEmailValid, setEmailValid] = useState(true);

  useEffect(() => {
    if(!pg) {
      router.push('/blog/?pg=' + 1 + '&kategori=' + selectedCategory)
    }
    router.push('/blog/?pg='+ page + '&kategori=' + selectedCategory)
    getBlogData();
  }, [page, sortBy]);

  useEffect(() => {
    router.push('/blog/?pg='+ page + '&kategori=' + selectedCategory)
    
    // if (page === 1) {
      getBlogData();
 
    // } else {
    //   setPage(1);
    // }
  }, [selectedCategory]);

  useEffect(()=>{
    getsubCategory();
  },[])

  useEffect(() => {
    setEmailValid(true);
  }, [email]);

  const validateEmail = () => {
    const RFC5322 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!RFC5322.test(email)) {
      setEmailValid(false);
      return false;
    };
    return true;
  };


  const getsubCategory = async ()=>{
    try{
      const keyResponse = await appLogin();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw keyResponse;
      }
      // ==== Get Sub Category Data ==== //
      
      const dataSub = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/blog/kategori/show`,
        {
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            AccessKey_App: keyResponse.AccessKey,
            'Access-Control-Allow-Origin': '*',
            'X-Content-Type-Options': 'nosniff',
            'X-Frame-Options': 'SAMEORIGIN',
            'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
            'Permissions-Policy': 'geolocation=(), browsing-topics=()',
            'Referrer-Policy': 'origin-when-cross-origin'
          }
        }
      );

      const responseSub = await dataSub.json();
      const dataCategories = responseSub.Data.filter(x=>x.i_par == null);
      const dataSubCategories = responseSub.Data.filter(x=>x.i_par != null);
      setCategories(dataCategories);
      setSubCategories(dataSubCategories);
            
      console.log("RESPONSESUB",responseSub);
    }
    catch (error) {
      console.log("error get data blog : ", error);
    }
  }

  const getBlogData = async () => {
    setReload(true);

    try {
      const keyResponse = await appLogin();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw keyResponse;
      }

      // ========== Get Data Blog ==============
      const payload = {
        i_kat: selectedCategory,
        Limit: 10,
        Page: page,
        Sort: sortBy,
      };

      const dataBlog = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/blog/show`,
        {
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            AccessKey_App: keyResponse.AccessKey,
            'X-Content-Type-Options': 'nosniff',
            'X-Frame-Options': 'SAMEORIGIN',
            'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
            'Permissions-Policy': 'geolocation=(), browsing-topics=()',
            'Referrer-Policy': 'origin-when-cross-origin'
          },
          body: new URLSearchParams(payload),
        }
      );
      const responseBlog = await dataBlog.json();
      if (responseBlog.IsError) {
        setBlogs([]);
        throw responseBlog.ErrToUser;
      } else {
        setBlogs(responseBlog.Data);
        setPaging(responseBlog.Paging);
      }
      // ========== Get Data Blog ==============
    } catch (error) {
      console.log("error get data blog : ", error);
    } finally {
      setReload(false);
      setLoadedData(true);
    }
  };

  // Subscribe Newsletter
  const submitSubscribe = async (e) => {
    e.preventDefault();
    const isValid = validateEmail();
    try {
      if (!isValid) {
        throw 'Email tidak valid';
      };

      const keyResponse = await appLogin();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw keyResponse;
      }

      const subscribe = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/newsletter/insert`,
        {
          method: "POST",
          headers: getHeaderWithAccessKey(),
          body: new URLSearchParams({ e: email }),
        }
      );
      const resSubscribe = await subscribe.json();

      if (resSubscribe.IsError) {
        throw resSubscribe;
      } else {
        setSuccesModal(true);
        setEmail("");
      }
    } catch (error) {
    }
  };
  // Subscribe Newsletter

  const NotFound = () => {
    return (
      <div
        style={{
          paddingTop: 100,
          textAlign: "center",
        }}
      >
        <SearchIcon />
        <div
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "16px",
            lineHeight: "150%",
            marginTop: 40,
          }}
        >
          Data yang di cari tidak ditemukan
        </div>
      </div>
    );
  };

  if (!loadedData) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  const defaultSucces = {
    loop: true,
    autoplay: true,
    animationData:succes,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <Layout title="Blog" isLoaderOpen={reload}>
      <Breadcrumb active="Blog" />
      <section className="mb-5">
        <div className="container" style={{ padding: "0px 16px" }}>
          <div className="row g-2">
            <div
              className="col-md-3 mtop-blog"
              style={{ paddingRight: "16px" }}
            >
              <div className="sticky-top">
                <div
                  className={`${styles.card_urutkan}  
                    ${filter ? "terapkan2" : "sembunyikan"}`}
                >
                  <div className={styles._header}>
                    <h5
                      className="card-title mb-0"
                      style={{
                        fontFamily: "FuturaBT",
                        fontWeight: 700,
                        fontSize: "16px",
                      }}
                    >
                      Filter
                    </h5>
                    <img
                      src="/images/icons/close-x.svg"
                      width={16.01}
                      height={16.01}
                      alt="close"
                      className={`show-fblog ${filter ? "close-icon-filter" : "sembunyikan"
                        }`}
                      onClick={() => setFilter((filter) => !filter)}
                    />
                  </div>
                  <div className={styles._body}>
                    <label className={styles._title}>Urutkan</label>
                    <div className="form-check d-flex align-items-center">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="urutkan"
                        id="urutkan1"
                        onChange={() => setSortBy("t_pst DESC")}
                        checked={sortBy === "t_pst DESC" ? true : false}
                      />
                      <label
                        style={{ fontFamily: "Helvetica", fontWeight: "400" }}
                        className="form-check-label"
                        htmlFor="urutkan1"
                      >
                        Terbaru
                      </label>
                    </div>
                    <div className="form-check d-flex align-items-center mt-2">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="urutkan"
                        id="urutkan2"
                        onChange={() => setSortBy("jm_vw DESC")}
                        checked={sortBy === "jm_vw DESC" ? true : false}
                      />
                      <label
                        style={{ fontFamily: "Helvetica", fontWeight: "400" }}
                        className="form-check-label"
                        htmlFor="urutkan2"
                      >
                        Terpopuler
                      </label>
                    </div>
                    <div
                      className={`show-fblog ${filter ? "terapkan" : "sembunyikan"
                        }`}
                    >
                      Terapkan
                    </div>
                  </div>
                </div>
                <div
                  className={`show-fblog ${filter ? "sembunyikan" : "tampilkan"
                    }`}
                  onClick={() => setFilter((filter) => !filter)}
                >
                  <div className="mini-filter">
                    <img
                      src="/images/icons/Filter.svg"
                      width={24}
                      height={24}
                      alt="close"
                      className="filter-icon"
                    />
                    <h5 className="filter ">Filter</h5>
                  </div>
                </div>
                <div
                  id="blog_web"
                  className={"card_newslater " + styles.newslater}
                >
                  <div className={styles._body}>
                    <div className="text-center text-white">
                      <div className={styles._icon}>
                        <svg
                          width="48"
                          height="48"
                          viewBox="0 0 48 48"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M40.42 17.6401L28 5.56005C26.9479 4.55976 25.5517 4.00195 24.1 4.00195C22.6483 4.00195 21.2521 4.55976 20.2 5.56005L7.78 17.5601C7.22816 18.0418 6.78433 18.6347 6.47758 19.2999C6.17083 19.9651 6.00808 20.6876 6 21.4201V38.5801C6.02108 40.0353 6.61809 41.4228 7.66024 42.4387C8.70238 43.4547 10.1047 44.0161 11.56 44.0001H36.44C37.8953 44.0161 39.2976 43.4547 40.3398 42.4387C41.3819 41.4228 41.9789 40.0353 42 38.5801V21.4201C41.9984 20.717 41.8581 20.0212 41.5869 19.3725C41.3158 18.7238 40.9192 18.1351 40.42 17.6401ZM22.88 8.44005C23.186 8.16037 23.5855 8.00529 24 8.00529C24.4145 8.00529 24.814 8.16037 25.12 8.44005L36 19.0001L25.06 29.5601C24.754 29.8397 24.3545 29.9948 23.94 29.9948C23.5255 29.9948 23.126 29.8397 22.82 29.5601L12 19.0001L22.88 8.44005ZM38 38.5801C37.9743 38.9726 37.7974 39.3399 37.5064 39.6047C37.2155 39.8695 36.8332 40.0112 36.44 40.0001H11.56C11.1668 40.0112 10.7845 39.8695 10.4936 39.6047C10.2026 39.3399 10.0257 38.9726 10 38.5801V22.7001L18.1 30.5001L14.78 33.7001C14.4075 34.0748 14.1984 34.5817 14.1984 35.1101C14.1984 35.6384 14.4075 36.1453 14.78 36.5201C14.9659 36.7151 15.1893 36.8705 15.4367 36.977C15.6842 37.0836 15.9506 37.139 16.22 37.1401C16.7349 37.138 17.2292 36.9374 17.6 36.5801L21.14 33.1801C22.0192 33.7173 23.0296 34.0016 24.06 34.0016C25.0904 34.0016 26.1008 33.7173 26.98 33.1801L30.52 36.5801C30.8908 36.9374 31.3851 37.138 31.9 37.1401C32.1694 37.139 32.4358 37.0836 32.6833 36.977C32.9307 36.8705 33.1541 36.7151 33.34 36.5201C33.7125 36.1453 33.9216 35.6384 33.9216 35.1101C33.9216 34.5817 33.7125 34.0748 33.34 33.7001L30 30.5001L38 22.7001V38.5801Z"
                            fill="white"
                          />
                        </svg>
                      </div>

                      <h4
                        className={styles._title}
                        style={{
                          fontFamily: "FuturaBT",
                          fontWeight: "700",
                          fontStyle: "normal",
                        }}
                      >
                        Newsletter
                      </h4>
                      <p
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: "400",
                          fontStyle: "normal",
                        }}
                      >
                        Berlangganan sekarang dan dapatkan artikel atau berita
                        properti terkini.
                      </p>
                    </div>

                    <div
                      className={"input-group " + styles.inputgroup}
                      style={{ padding: "5px" }}
                    >
                      <input
                        type="email"
                        className={"form-control " + styles.formcontrol}
                        placeholder="Email"
                        aria-label="Email"
                        aria-describedby="button-addon2"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                      <button
                        className={styles.icon}
                        type="submit"
                        onClick={submitSubscribe}
                      >
                        <svg
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M16.9494 7.76372L5.28269 1.92871C4.82226 1.69949 4.30237 1.61788 3.79388 1.69502C3.2854 1.77216 2.81307 2.00429 2.44129 2.35977C2.06952 2.71524 1.8164 3.17676 1.71643 3.68139C1.61646 4.18603 1.67452 4.70922 1.88268 5.17965L3.88268 9.65593C3.92807 9.76415 3.95144 9.88034 3.95144 9.99769C3.95144 10.1151 3.92807 10.2312 3.88268 10.3395L1.88268 14.8157C1.71327 15.1964 1.64165 15.6135 1.67433 16.0289C1.70702 16.4443 1.84297 16.845 2.06984 17.1945C2.2967 17.544 2.60729 17.8313 2.97337 18.0302C3.33945 18.2291 3.74942 18.3333 4.16602 18.3334C4.55621 18.3295 4.94059 18.2384 5.29102 18.0667L16.9577 12.2317C17.3715 12.0234 17.7194 11.7043 17.9624 11.3098C18.2055 10.9153 18.3342 10.4611 18.3342 9.99769C18.3342 9.53432 18.2055 9.08006 17.9624 8.68559C17.7194 8.29112 17.3715 7.97196 16.9577 7.76372H16.9494ZM16.2077 10.7396L4.54102 16.5746C4.38782 16.6482 4.21579 16.6731 4.04801 16.6461C3.88022 16.6192 3.72469 16.5415 3.60228 16.4236C3.47987 16.3057 3.39642 16.1531 3.36313 15.9864C3.32984 15.8197 3.34829 15.6468 3.41602 15.4909L5.40768 11.0147C5.43347 10.9549 5.45573 10.8936 5.47435 10.8313H11.216C11.437 10.8313 11.649 10.7434 11.8053 10.5871C11.9616 10.4308 12.0494 10.2188 12.0494 9.99769C12.0494 9.77662 11.9616 9.5646 11.8053 9.40827C11.649 9.25195 11.437 9.16412 11.216 9.16412H5.47435C5.45573 9.10174 5.43347 9.04051 5.40768 8.98074L3.41602 4.50445C3.34829 4.34854 3.32984 4.17565 3.36313 4.00895C3.39642 3.84225 3.47987 3.68972 3.60228 3.57181C3.72469 3.45389 3.88022 3.37624 4.04801 3.34925C4.21579 3.32226 4.38782 3.34723 4.54102 3.42081L16.2077 9.25582C16.3442 9.32577 16.4587 9.43204 16.5387 9.56294C16.6187 9.69384 16.6611 9.84428 16.6611 9.99769C16.6611 10.1511 16.6187 10.3016 16.5387 10.4324C16.4587 10.5633 16.3442 10.6696 16.2077 10.7396Z"
                            fill="white"
                          />
                        </svg>
                      </button>
                    </div>
                    {!isEmailValid &&
                    (
                      <div className="error-input" style={{ color: 'white' }}>
                        <i
                          class="bi bi-exclamation-circle-fill"
                          style={{ marginRight: "5px" }}
                        ></i>
                        Format email tidak sesuai
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-9">
              <div>
                <ul
                  className="nav navbar_kategori"
                  id="nav-blog"
                  style={{
                    minHeight: "240px",
                    gap:"10px",
                  }}
                >
                  <li className="nav-item" style={{ cursor: "pointer" }}>
                    {/* <Link
                    href={{
                      pathname: "/blog",
                      query: { kategori: "semua" },
                    }}
                  >
                  </Link> */}
                    <a
                      className={`nav-link ${selectedCategory === "" && "active"
                        }`}
                      aria-current="page"
                      href="#"
                      onClick={() => {
                        setSelectedCategory("")
                        setPage(1)
                        setSelectedTab(null)
                      }}
                    >
                      Semua
                    </a>
                  </li>
                
                  {/* <li className="nav-item">
                  <Link
                    href={{
                      pathname: "/blog",
                      query: { kategori: "inspirasi" },
                    }}
                  >
                    <a
                      className={
                        kategori == "inspirasi" ? "nav-link active" : "nav-link"
                      }
                      aria-current="page"
                      href="#"
                    >
                      Inspirasi
                    </a>
                  </Link>
                </li> */}
                  <li className="nav-item">
                    <a
                      className={`nav-link ${selectedCategory == 16 && "active"}`}
                      aria-current="page"
                      onClick={() => {
                        setSelectedCategory(16)
                        setPage(1)
                        setSelectedTab(null)
                      }}
                    >
                      Berita Perusahaan
                    </a>
                  </li>
                  {categories.filter(x => x.n != "Berita & Info").sort((a,b) => a.urutan - b.urutan).map((data)=>{
                    return(
                     <li className="nav-item dropdown">
                     <a
                       className={`nav-link dropdown-toggle ${selectedTab == data.id
                         ? "active show" : ""
                         }`}
                       data-bs-toggle="dropdown"
                       role="button"
                       aria-expanded="false"
                     >
                       {data.n}
                     </a>
                     <ul className="dropdown-menu">
                      {subCategories.filter(x=>x.i_par == data.id).map((dataSub)=>{
                        return(
                         <li>
                         <a
                           className={`dropdown-item ${selectedCategory == dataSub.id && "active"
                             }`}
                           onClick={() => {
                             setSelectedCategory(dataSub.id)
                             setSelectedTab(dataSub.i_par)
                             setPage(1)
                           }}
                         >
                           {dataSub.n}
                         </a>
                       </li>
                        )
                      })}
                     </ul>
                   </li>
                    )
                  })}
                </ul>
              </div>

              {/* <ItemBlogHorizontal /> */}
              {blogs.length > 0 ? (
                <div className="row g-2" style={{ marginTop: "-158px" }}>
                  {blogs.map((data, index) => {
                    return(
                      <div key={index} className={`${index < 1 ? "col-lg-12 col-md-4" : "col-md-4"}`}
                      style={{zIndex:index < 1 ? 999 : ''}}
                      >
                        <ItemBlogVertical data={data} index={index} />
                      </div>
                    )
                  })}
                  <div className="col-12">
                    <PaginationNew
                      length={paging.JmlHalTotal}
                      current={paging.HalKe}
                      onChangePage={(e) => {
                        setPage(e);
                        window.scrollTo(0, 0);
                      }}
                    />
                  </div>
                </div>
              ) : (
                NotFound()
              )}
              <div
                id="blog_mobile"
                className={"card_newslater m-auto mt-5 " + styles.newslater}
              >
                <div className={"m-auto " + styles._body}>
                  <div className="text-center text-white">
                    <div className={styles._icon}>
                      <svg
                        width="48"
                        height="48"
                        viewBox="0 0 48 48"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M40.42 17.6401L28 5.56005C26.9479 4.55976 25.5517 4.00195 24.1 4.00195C22.6483 4.00195 21.2521 4.55976 20.2 5.56005L7.78 17.5601C7.22816 18.0418 6.78433 18.6347 6.47758 19.2999C6.17083 19.9651 6.00808 20.6876 6 21.4201V38.5801C6.02108 40.0353 6.61809 41.4228 7.66024 42.4387C8.70238 43.4547 10.1047 44.0161 11.56 44.0001H36.44C37.8953 44.0161 39.2976 43.4547 40.3398 42.4387C41.3819 41.4228 41.9789 40.0353 42 38.5801V21.4201C41.9984 20.717 41.8581 20.0212 41.5869 19.3725C41.3158 18.7238 40.9192 18.1351 40.42 17.6401ZM22.88 8.44005C23.186 8.16037 23.5855 8.00529 24 8.00529C24.4145 8.00529 24.814 8.16037 25.12 8.44005L36 19.0001L25.06 29.5601C24.754 29.8397 24.3545 29.9948 23.94 29.9948C23.5255 29.9948 23.126 29.8397 22.82 29.5601L12 19.0001L22.88 8.44005ZM38 38.5801C37.9743 38.9726 37.7974 39.3399 37.5064 39.6047C37.2155 39.8695 36.8332 40.0112 36.44 40.0001H11.56C11.1668 40.0112 10.7845 39.8695 10.4936 39.6047C10.2026 39.3399 10.0257 38.9726 10 38.5801V22.7001L18.1 30.5001L14.78 33.7001C14.4075 34.0748 14.1984 34.5817 14.1984 35.1101C14.1984 35.6384 14.4075 36.1453 14.78 36.5201C14.9659 36.7151 15.1893 36.8705 15.4367 36.977C15.6842 37.0836 15.9506 37.139 16.22 37.1401C16.7349 37.138 17.2292 36.9374 17.6 36.5801L21.14 33.1801C22.0192 33.7173 23.0296 34.0016 24.06 34.0016C25.0904 34.0016 26.1008 33.7173 26.98 33.1801L30.52 36.5801C30.8908 36.9374 31.3851 37.138 31.9 37.1401C32.1694 37.139 32.4358 37.0836 32.6833 36.977C32.9307 36.8705 33.1541 36.7151 33.34 36.5201C33.7125 36.1453 33.9216 35.6384 33.9216 35.1101C33.9216 34.5817 33.7125 34.0748 33.34 33.7001L30 30.5001L38 22.7001V38.5801Z"
                          fill="white"
                        />
                      </svg>
                    </div>

                    <h4
                      className={styles._title}
                      style={{
                        fontFamily: "FuturaBT",
                        fontWeight: "700",
                        fontStyle: "normal",
                      }}
                    >
                      Newsletter
                    </h4>
                    <p
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "400",
                        fontStyle: "normal",
                      }}
                    >
                      Berlangganan sekarang dan dapatkan artikel atau berita
                      properti terkini.
                    </p>
                  </div>

                  <div
                    className={"input-group " + styles.inputgroup}
                    style={{ padding: "5px" }}
                  >
                    <input
                      type="email"
                      className={"form-control " + styles.formcontrol}
                      placeholder="Email"
                      aria-label="Email"
                      aria-describedby="button-addon2"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <button
                      className={styles.icon}
                      type="submit"
                      onClick={submitSubscribe}
                    >
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M16.9494 7.76372L5.28269 1.92871C4.82226 1.69949 4.30237 1.61788 3.79388 1.69502C3.2854 1.77216 2.81307 2.00429 2.44129 2.35977C2.06952 2.71524 1.8164 3.17676 1.71643 3.68139C1.61646 4.18603 1.67452 4.70922 1.88268 5.17965L3.88268 9.65593C3.92807 9.76415 3.95144 9.88034 3.95144 9.99769C3.95144 10.1151 3.92807 10.2312 3.88268 10.3395L1.88268 14.8157C1.71327 15.1964 1.64165 15.6135 1.67433 16.0289C1.70702 16.4443 1.84297 16.845 2.06984 17.1945C2.2967 17.544 2.60729 17.8313 2.97337 18.0302C3.33945 18.2291 3.74942 18.3333 4.16602 18.3334C4.55621 18.3295 4.94059 18.2384 5.29102 18.0667L16.9577 12.2317C17.3715 12.0234 17.7194 11.7043 17.9624 11.3098C18.2055 10.9153 18.3342 10.4611 18.3342 9.99769C18.3342 9.53432 18.2055 9.08006 17.9624 8.68559C17.7194 8.29112 17.3715 7.97196 16.9577 7.76372H16.9494ZM16.2077 10.7396L4.54102 16.5746C4.38782 16.6482 4.21579 16.6731 4.04801 16.6461C3.88022 16.6192 3.72469 16.5415 3.60228 16.4236C3.47987 16.3057 3.39642 16.1531 3.36313 15.9864C3.32984 15.8197 3.34829 15.6468 3.41602 15.4909L5.40768 11.0147C5.43347 10.9549 5.45573 10.8936 5.47435 10.8313H11.216C11.437 10.8313 11.649 10.7434 11.8053 10.5871C11.9616 10.4308 12.0494 10.2188 12.0494 9.99769C12.0494 9.77662 11.9616 9.5646 11.8053 9.40827C11.649 9.25195 11.437 9.16412 11.216 9.16412H5.47435C5.45573 9.10174 5.43347 9.04051 5.40768 8.98074L3.41602 4.50445C3.34829 4.34854 3.32984 4.17565 3.36313 4.00895C3.39642 3.84225 3.47987 3.68972 3.60228 3.57181C3.72469 3.45389 3.88022 3.37624 4.04801 3.34925C4.21579 3.32226 4.38782 3.34723 4.54102 3.42081L16.2077 9.25582C16.3442 9.32577 16.4587 9.43204 16.5387 9.56294C16.6187 9.69384 16.6611 9.84428 16.6611 9.99769C16.6611 10.1511 16.6187 10.3016 16.5387 10.4324C16.4587 10.5633 16.3442 10.6696 16.2077 10.7396Z"
                          fill="white"
                        />
                      </svg>
                    </button>
                  </div>
                  {!isEmailValid &&
                  (
                    <div className="error-input" style={{ color: 'white' }}>
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Format email tidak sesuai
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className={`modal fade d-${succesModal? "block show" : "none"}`} id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
      style={{zIndex:"9999",background:"#00193e69"}}
      >
        <div className="modal-dialog modal-dialog-centered px-3 mx-auto">
          <div className="modal-content" style={{padding:"20px 60px",borderRadius:"8px"}}>
            <div className="modal-body">
              <div className="row justify-content-end">
                <button type="button" className="btn-close" aria-label="Close"
              onClick={()=>{
                setSuccesModal(false)
              }}
              style={{marginRight:"-30px"}}
                ></button>
              </div>
              <h2 className="text-center">Email terkirim</h2>
              <Lottie
                      options={defaultSucces}
                      height={"80%"}
                      width={"100%"}
                      isStopped={false}
                      isPaused={false}
                      max-width={350}
                    />
              <div className="row g-0">
              <div className="row g-0">
              {/* <p style={{
                fontFamily:"Helvetica",
                fontSize:"14px"
              }}>
              {alertText}
              </p> */}
              </div>
                <button type="button" className="btn btn-primary" data-bs-dismiss="modal"
                style={{
                  background:"#0061a7",
                  borderRadius:"200px",
                  lineHeight: "31px",
                  width: "100%",
                  height: "40px",
                }}
              onClick={()=>{
                setSuccesModal(false);
                router.push("/"); 
              }}
              >Kembali Ke Beranda</button>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
