import { useRouter } from "next/router";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import NewsPopuler from "../../components/section/NewsPopuler";
import NewsTerkait from "../../components/section/NewsTerkait";
import ShareSosmed from "../../components/static/ShareSosmed";
import Link from "next/link";
import { useEffect, useState } from "react";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import { convertDate } from "../../utils/DateConverter";
import parse from 'html-react-parser';
import { Toast, ToastContainer } from "react-bootstrap";
import ReactImageFallback from "react-image-fallback";

export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  const { id } = router.query;
  const [detail, setDetail] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [loadNew, setLoadNew] = useState(false);
  const [copied, setCopied] = useState(false);
  const [imgUrl, setImgUrl] = useState('/images/thumb-placeholder.png');

  useEffect(() => {
    console.log("DEBUG: EFFFECT", detail)
    if (!detail) return

    let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${detail?.gmbr.split('|')[1].split('"')[0]}`;
    fullUrl = fullUrl.replaceAll(' ', '%20');
    setImgUrl(fullUrl)
  }, [detail])

  useEffect(() => {
    setLoadNew(true);
    getDetailBlog();
  }, [id]);

  useEffect(() => {
    if (copied) {
      setTimeout(() => {
        setCopied(false);
      }, 3000);
    }
  }, [copied]);

  const getDetailBlog = async () => {
    let payload = {}
    if (id) {
      let path = id.split('-')
      let idBlog = path[path.length - 1]
      payload = {
        id: idBlog
      };
    }
    try {
      const detail = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/blog/show`, {
        method: 'POST',
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams(payload)
      })
      const responseDetail = await detail.json();
      console.log('~ data detail : ', responseDetail)
      setDetail(responseDetail.Data[0]);
    } catch (error) {
      console.log('error get data detail : ', error)
    } finally {
      setLoadNew(false);
      setLoading(false);
    }
  };

  if (isLoading) {
    return (
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh", }}>
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    )
  }

  return (
    <Layout title="Blog" isLoaderOpen={loadNew}>
      <Breadcrumb active={detail?.jdl}>
        <li className="breadcrumb-item">
          <Link href="/blog/?pg=1&kategori=">Blog</Link>
        </li>
      </Breadcrumb>
      <section id="post_detail" className="mb-5">
        <div
          aria-live="polite"
          aria-atomic="true"
          className="position-relative"
        >
          <ToastContainer className="p-3" position={'top-end'}>
            <Toast show={copied} style={{ backgroundColor: 'green' }}>
              <Toast.Body style={{ color: 'white' }}>Link telah disalin ke clipboard</Toast.Body>
            </Toast>
          </ToastContainer>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2 text-center">
              <div className="title_post">
                {detail?.jdl}
              </div>
              <div className="d-flex justify-content-center meta_post mt-3">
                <h6>{detail?.n_kat}</h6>/ {detail?.t_pst.split('T')[0]}
              </div>
              <ShareSosmed judul={detail?.jdl} setCopied={setCopied} />
            </div>
            <div className="col-12 py-3">
              <ReactImageFallback
                src={imgUrl} 
                className="img-fluid w-100"
                fallbackImage="/images/thumb-placeholder.png"
              />
            </div>
            <div id="content_post" className="col-md-8 offset-md-2">
              <p
                className="postDetailClass"
                style={{
                  fontSize: "16px",
                  color: "#00193E",
                  fontStyle: "normal",
                  fontWeight: "400",
                  fontFamily: "Helvetica",
                  lineHeight: "25.6px",
                }}
              >
                {detail?.dsk ? parse(detail?.dsk) : null}
              </p>

              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </section>
        <div id="detailBlog-news-terkait-slider">
          <NewsTerkait kategori={detail?.i_kat} />
        </div>
      <NewsPopuler />
    </Layout>
  );
}
