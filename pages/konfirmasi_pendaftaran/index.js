import { useRouter } from "next/router";
import Link from "next/link";
import Layout from "../../components/Layout";
import BreadcrumbSecondary from "../../components/section/BreadcrumbSecondary";
import { useMediaQuery } from 'react-responsive';
import React, { useEffect, useState } from "react";
import ResetPasswordNew from "../../components/auth/ResetPasswordNew";
import Cookies from "js-cookie";

export default function index(){
    const [loadingDetails, setLoadingDetails] = useState(false);
    const [titleText,setTitleText] = useState("");
    const [subTitleText,setSubTitleText] = useState("");
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1024px)' })
    const router = useRouter();
    const { id } = router.query;

    let savedProfile;
    useEffect(()=>{
        ///set prop dari komponen////
        savedProfile = JSON.parse(Cookies.get('user') || sessionStorage.getItem('user') || null);
        setTitleText(`Hi, ${savedProfile?.n||"Lorem ipsum"}!`)
        setSubTitleText("Akun anda akan aktif setelah merubah password")
    },[])


    return(
        <Layout title="Konfirmasi Pendaftaran" isLoaderOpen={loadingDetails}>
            <BreadcrumbSecondary active="Verifikasi">
                <li
                className="breadcrumb-item d-flex justify-content-center align-items-center"
                style={{
                    fontSize: "12px",
                    fontFamily: "Helvetica",
                    fontWeight: "bold",
                    lineHeight: "150%",
                }}
                >
                    <Link
                        href="/"
                        style={{ fontFamily: "Helvetica", fontWeight: "bold" }}
                    >
                        Beranda
                    </Link>
                    <Link
                        href="/profile"
                        style={{ fontFamily: "Helvetica", fontWeight: "bold" }}
                    >
                        Member
                    </Link>
                </li>
                {/* <li className="breadcrumb-item">
                <Link href="/promo">promo</Link>
                </li> */}
            </BreadcrumbSecondary>
            <ResetPasswordNew title={titleText} subTitle={subTitleText} />    
        </Layout>
    )
}