import { useRouter } from "next/router";
import Layout from "../../components/Layout";
import BreadcrumbSecondary from "../../components/section/BreadcrumbSecondary";
import React, { useEffect, useState } from "react";
import AktivasiMember from "../../components/auth/AktivasiMember";

export default function index(){
    const [loadingDetails, setLoadingDetails] = useState(false);
    const router = useRouter();
  const [code,setCode] = useState(null);

    useEffect(()=>{
        if(router.isReady){
          setCode(router.query.id);
        }
      },[router.isReady])
      
    return(
        <Layout title="Konfirmasi Pendaftaran" isLoaderOpen={loadingDetails}>
            <BreadcrumbSecondary active="Aktivasi Member"  />
           {code && <AktivasiMember code={code.split(".")[0]} />}    
        </Layout>
    )
}