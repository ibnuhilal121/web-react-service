import { useEffect, useRef, useState } from "react";
import { useAppContext } from "../../../context";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import Layout from "../../../components/Layout";
import SliderProgress from "../../../components/element/slider_progress/slider_progress";
import Breadcrumb from "../../../components/section/Breadcrumb";
import CreditStepOne from "../../../components/section/CreditStepOne";
import CreditStepTwo from "../../../components/section/CreditStepTwo";
import CreditStepThree from "../../../components/section/CreditStepThree";
import CreditStepFour from "../../../components/section/CreditStepFour";
import ModalConfirmation from "../../../components/element/modal_confirmation/modal_confirmation";
import Success from '../../../components/popup/Success'
import SuccessGenerate from "../../../components/popup/SuccesGenerate"
import withAuth from "../../../helpers/withAuth";
import styles from "../Credit.module.scss";
import { continueFillKPR, continueKPRStok } from "../../../services/ajukanKPR";
import axios from "axios";
import { defaultHeaders } from "../../../utils/defaultHeaders";
import verifiedMember from "../../../helpers/verifiedMember";

function FormAjukanKPR() {
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 912px)` })
    const router = useRouter();
    const { id } = router.query;
    const { userKey } = useAppContext();
    const { tipe } = router.query;
    const [percent, setPercent] = useState(id?.substring(0, 5) === "KPRST" ? 25 : 0);
    const [step, setStep] = useState(id?.substring(0, 5) === "KPRST" ? 2 : 1);
    const [formStepOne, setFormStepOne] = useState("{}");
    const [modalSukses, setModalSukses] = useState(false);
    const [errCode, setErrCode] = useState(-6)
    const [errorRegister, setErrorRegister] = useState("")
    const [loaded, setLoaded] = useState(id ? false : true);
    const [branches, setBranches] = useState([]);
    const [tipeKPR, setTipeKPR] = useState(tipe);
    const [memberDetail, setMemberDetail] = useState({})
    const modalKprSuccessRef = useRef(null)
    const url = process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE;
    const [dataPengajuan, setDataPengajuan] = useState({
        cabang: "",
        jenis: "",
        harga: "",
        uangMuka: "0",
        pengajuan: "",
        waktu: "",
        metode: "",
    });
    const [loadingJenis, setLoadingJenis] = useState(false);
    const [loadingCabang, setLoadingCabang] = useState(false);
    const [loadingStep, setLoadingStep] = useState(false);
    const [jenisKPR, setJenisKPR] = useState([]);
    const [KPRid, setKPRid] = useState(id || "");
    const [showPercent, setShowPercent] = useState(percent);
    const [validated, setValidated] = useState(false);
    const [idKavling, setIdKavling] = useState("");
    const openModal = document.getElementById("modalOpen")


     // get detail user
  const getDetailMember = async () => {
    try {
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
        const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
              AccessKey_Member: userKey || JSON.parse(sessionStorage.getItem("keyMember")),
                },
        });
        const response = await loadData.json()
        console.log("aaaaaaaa", response)
            
        setMemberDetail(response.Data[0])

      } catch(error) {
          console.log({error})
      }
    }

    // cek apakah user sudah di register apa belum
    const checkRegister = async () => {
      try {
          const res = await axios.post(url+'/registerCheck', {
              nik: memberDetail?.no_ktp,
              action: "check_nik"
          })
          return "USER_EXISTS_VERIFIED"
      } catch (error) {
          return error.response.data.code
      }
  }
  
  // register user ke tekenaja
  const registerUser = async () => {
    try {
        const body = {
            email: memberDetail?.e,
            mobilePhoneNumber: memberDetail?.no_hp,
            provinceId: memberDetail?.i_prop.toString(),
            districtId: memberDetail?.i_kot.toString().split("").splice(2,4).join(""),
            subDistrictId: memberDetail?.i_kec.toString().split("").splice(4,6).join(""),
            address: memberDetail?.almt,
            postalCode: memberDetail?.pos,
            userId: memberDetail?.id
        }
        const response = await axios.post(url + '/register',body);

        return response;
      } catch (error) {
        console.log("Server Error", error.response)
        const code = error?.response?.data?.code;
        if(code == "EMAIL_EXISTS") {
          setErrCode(304)
          openModal.click()
        } else if(code == "USER_EXISTS") {
          setErrCode(503)
          openModal.click()
        } else if(code == "SELFIE_UNMATCH") {
          setErrCode(505)
          openModal.click()
        } else if(code == "BIODATA_UNMATCH") {
          setErrCode(606)
          openModal.click()
        } else if(code == "EKYC_INVALID") {
          setErrCode(707)
          openModal.click()
        } else if(code == "EKYC_ERROR" || code == "EKYC_PROVIDER_TIMEOUT") {
          setErrCode(808)
          openModal.click()
        } else if(code == "PHONE_EXISTS") {
          setErrCode(202)
          openModal.click()
        } else if(code == "INVALID_PARAMETER") {
          const objMessage = error?.response?.data?.message
          if(typeof objMessage == "string") {
            setErrCode(202)
          } else {
            setErrCode(909)
          }
          openModal.click()
        } else if(code == "SYSTEM_FAILURE") {
          setErrCode(303)
          openModal.click()
        } else {
          setErrCode(303)
          openModal.click()
        }

      }
    }

  if (typeof window !== "undefined") {
    window.onbeforeunload = () => {
      localStorage.removeItem("dataPengajuan");
      localStorage.removeItem("dataDiri");
      sessionStorage.removeItem("ktp_foto");
      sessionStorage.removeItem("pas_foto");
      sessionStorage.removeItem("slip_foto");
      sessionStorage.removeItem("rek_foto");
      localStorage.removeItem("stepCredit");
      localStorage.removeItem("percentCredit");
      localStorage.removeItem("jenisKPR");
      localStorage.removeItem("kpr_id");
      localStorage.removeItem("kota");
      localStorage.removeItem("provinsi");
      localStorage.removeItem("kecamatan");
      localStorage.removeItem("kodepos");
      localStorage.removeItem("kelurahan");
    };
  }

  useEffect(() => {
    if (id) {
      sessionStorage.setItem('back_log', router.pathname);
    };
  }, []);

  useEffect(() => {
    getDetailMember()

    console.log("tipeKPR ", tipe);
    console.log("percent ", percent);
    if (localStorage.getItem("jenisKPR") !== null) {
      setTipeKPR(JSON.parse(localStorage.getItem("jenisKPR")));
    }
    if (localStorage.getItem("percentCredit") !== null) {
      setPercent(Number(localStorage.getItem("percentCredit")));
      setShowPercent(Number(localStorage.getItem("percentCredit")));
    }
    if (
      localStorage.getItem("kpr_id") !== null &&
      localStorage.getItem("kpr_id") !== ""
    ) {
      setKPRid(localStorage.getItem("kpr_id"));
    }
    if (
      localStorage.getItem("stepCredit") !== null &&
      localStorage.getItem("stepCredit") !== ""
    ) {
      setStep(Number(localStorage.getItem("stepCredit")));
    }
    if (localStorage.getItem("dataPengajuan") !== null) {
      setFormStepOne(localStorage.getItem("dataPengajuan"));
      setFormStepOne("{}");
      setDataPengajuan(JSON.parse(localStorage.getItem("dataPengajuan")));
    }

    if (id?.substring(0, 5) === "KPRNS") {
      continueFillKPR(userKey, id)
        .then((res) => {
          setTipeKPR(res.TIPE_PENGAJUAN);
          let tempPengajuan = { ...dataPengajuan };
          tempPengajuan.cabang = res.ID_CABANG;
          tempPengajuan.jenis = res.JENIS_KREDIT || res.JENIS_PEMBIAYAAN;
          tempPengajuan.harga = res.HARGA_PROPERTI;
          tempPengajuan.uangMuka = res.UANG_MUKA || "0";
          tempPengajuan.pengajuan = res.NILAI_PENGAJUAN;
          tempPengajuan.waktu = Math.round(Number(res.JANGKA_WAKTU) / 12);
          tempPengajuan.metode = res.PEMBAYARAN_ANGSURAN;
          setDataPengajuan(tempPengajuan);
          localStorage.setItem("kpr_id", id);
          localStorage.setItem("jenisKPR", res.TIPE_PENGAJUAN);
          localStorage.setItem("dataPengajuan", JSON.stringify(tempPengajuan));
          let tempDataDiri = {
            nik: "",
            nama: "",
            kotaLahir: "",
            tglLahir: "",
            email: "",
            hp: "",
            tlpn: "",
            statMenikah: "",
            alamat: "",
            rt: "",
            rw: "",
          };
          tempDataDiri.nik = res.NO_KTP;
          tempDataDiri.nama = res.NAMA_LENGKAP;
          tempDataDiri.kotaLahir = res.TEMPAT_LAHIR;
          tempDataDiri.tglLahir = res.TGL_LAHIR?.split(" ")[0];
          tempDataDiri.email = res.EMAIL;
          tempDataDiri.hp = res.NO_HP1;
          tempDataDiri.tlpn = (res.NO_TELP === "null" || !res.NO_TELP) ? "" : res.NO_TELP;
          tempDataDiri.statMenikah = res.STATUS_KAWIN;
          tempDataDiri.alamat = res.ALAMAT;
          tempDataDiri.rt = res.RT;
          tempDataDiri.rw = res.RW;
          localStorage.setItem("dataDiri", JSON.stringify(tempDataDiri));
          localStorage.setItem("provinsi", res.ID_PROPINSI);
          localStorage.setItem("kota", res.ID_KAB_KOTA);
          localStorage.setItem("kecamatan", res.ID_KECAMATAN);
          localStorage.setItem("kelurahan", res.ID_DESA_KELURAHAN);
          localStorage.setItem("kodepos", res.KODEPOS);
          sessionStorage.setItem("ktp_foto", res.FILE_KTP);
          sessionStorage.setItem("pas_foto", res.FILE_PAS_FOTO);
          sessionStorage.setItem("slip_foto", res.FILE_SLIP_PENGHASILAN);
          sessionStorage.setItem("rek_foto", res.FILE_REK_KORAN);
          let tempStep = 4;
          let tempProgress = 100;

          if (
            !res.FILE_KTP ||
            !res.FILE_PAS_FOTO ||
            !res.FILE_SLIP_PENGHASILAN ||
            !res.FILE_REK_KORAN
          ) {
            tempStep = 3;
            tempProgress = 50;
          }
          for (let data in tempDataDiri) {
            if (!tempDataDiri[data] && data != 'tlpn') {
              tempStep = 2;
              tempProgress = 25;
            }
          }
          if (
            !res.ID_PROPINSI ||
            !res.ID_KAB_KOTA ||
            !res.ID_KECAMATAN ||
            !res.ID_DESA_KELURAHAN ||
            !res.KODEPOS
          ) {
            tempStep = 2;
            tempProgress = 25;
          }
          if (
            !res.ID_CABANG ||
            (!res.JENIS_KREDIT && !res.JENIS_PEMBIAYAAN) ||
            !res.HARGA_PROPERTI ||
            !res.NILAI_PENGAJUAN ||
            !res.JANGKA_WAKTU ||
            !res.PEMBAYARAN_ANGSURAN
          ) {
            tempStep = 1;
            tempProgress = 0;
          }
          localStorage.setItem("stepCredit", tempStep);
          localStorage.setItem("percentCredit", tempProgress);

          setStep(tempStep);
          setPercent(tempProgress);
        })
        .catch((error) => {})
        .finally(() => {
          setLoaded(true);
        });
    } else if (id?.substring(0, 5) === "KPRST") {
      continueKPRStok(userKey, id)
        .then((res) => {
          setIdKavling(res.ID_KAV);
          setTipeKPR(res.TIPE_PENGAJUAN);
          let tempPengajuan = { ...dataPengajuan };
          tempPengajuan.cabang = res.ID_CABANG;
          tempPengajuan.jenis = res.JENIS_KREDIT;
          tempPengajuan.harga = res.HARGA_JUAL;
          tempPengajuan.uangMuka = res.UANG_MUKA || "0";
          tempPengajuan.pengajuan = res.NILAI_PENGAJUAN;
          tempPengajuan.waktu = Math.round(Number(res.JANGKA_WAKTU) / 12);
          tempPengajuan.metode = res.PEMBAYARAN_ANGSURAN;
          setDataPengajuan(tempPengajuan);
          localStorage.setItem("kpr_id", id);
          localStorage.setItem("jenisKPR", res.TIPE_PENGAJUAN);
          localStorage.setItem("dataPengajuan", JSON.stringify(tempPengajuan));
          let tempDataDiri = {
            nik: "",
            nama: "",
            kotaLahir: "",
            tglLahir: "",
            email: "",
            hp: "",
            tlpn: "",
            statMenikah: "",
            alamat: "",
            rt: "",
            rw: "",
          };
          tempDataDiri.nik = res.NO_ID;
          tempDataDiri.nama = res.NAMA_LENGKAP;
          tempDataDiri.kotaLahir = res.TEMPAT_LAHIR;
          tempDataDiri.tglLahir = res.TGL_LAHIR.split(" ")[0];
          tempDataDiri.email = res.EMAIL;
          tempDataDiri.hp = res.NO_HP1;
          tempDataDiri.tlpn = (res.NO_TELP === "null" || !res.NO_TELP) ? "" : res.NO_TELP;
          tempDataDiri.statMenikah = res.STATUS_KAWIN;
          tempDataDiri.alamat = res.ALAMAT;
          tempDataDiri.rt = res.RT;
          tempDataDiri.rw = res.RW;
          localStorage.setItem("dataDiri", JSON.stringify(tempDataDiri));
          localStorage.setItem("provinsi", res.ID_PROPINSI);
          localStorage.setItem("kota", res.ID_KAB_KOTA);
          localStorage.setItem("kecamatan", res.ID_KECAMATAN);
          localStorage.setItem("kelurahan", res.ID_DESA_KELURAHAN);
          localStorage.setItem("kodepos", res.KODEPOS);
          sessionStorage.setItem("ktp_foto", res.FILE_KTP);
          sessionStorage.setItem("pas_foto", res.FILE_PAS_FOTO);
          sessionStorage.setItem("slip_foto", res.FILE_SLIP_PENGHASILAN);
          sessionStorage.setItem("rek_foto", res.FILE_REK_KORAN);
          let tempStep = 4;
          let tempProgress = 100;
          if (
            !res.FILE_KTP ||
            !res.FILE_PAS_FOTO ||
            !res.FILE_SLIP_PENGHASILAN ||
            !res.FILE_REK_KORAN
          ) {
            tempStep = 3;
            tempProgress = 50;
          }
          for (let data in tempDataDiri) {
            if (!tempDataDiri[data] && data != 'tlpn') {
              tempStep = 2;
              tempProgress = 25;
            }
          }
          if (
            !res.ID_PROPINSI ||
            !res.ID_KAB_KOTA ||
            !res.ID_KECAMATAN ||
            !res.ID_DESA_KELURAHAN ||
            !res.KODEPOS
          ) {
            tempStep = 2;
            tempProgress = 25;
          }
          localStorage.setItem("stepCredit", tempStep);
          localStorage.setItem("percentCredit", tempProgress);
          setStep(tempStep);
          setPercent(tempProgress);
        })
        .catch((error) => {})
        .finally(() => {
          setLoaded(true);
        });
    }

    return () => {
      localStorage.removeItem("dataPengajuan");
      localStorage.removeItem("dataDiri");
      sessionStorage.removeItem("ktp_foto");
      sessionStorage.removeItem("pas_foto");
      sessionStorage.removeItem("slip_foto");
      sessionStorage.removeItem("rek_foto");
      localStorage.removeItem("stepCredit");
      localStorage.removeItem("percentCredit");
      localStorage.removeItem("jenisKPR");
      localStorage.removeItem("kpr_id");
      localStorage.removeItem("kota");
      localStorage.removeItem("provinsi");
      localStorage.removeItem("kecamatan");
      localStorage.removeItem("kodepos");
      localStorage.removeItem("kelurahan");
    };
  }, []);

  useEffect(() => {
    setShowPercent(percent);
  }, [percent]);

  useEffect(() => {
    if (tipeKPR) {
      console.log("calling get data cabang");
      console.log("tipeKPR ", tipeKPR);
      selectTipeKPR();
      getDataCabang();
    }
  }, [tipeKPR]);

  useEffect(() => {
    if (!isTabletOrMobile) {
      router.push("/ajukan_kpr");
    }
  });

  const setProgressCredit = (param) => {
    const stepNow = Number(localStorage.getItem("percentCredit"));
    if (param > stepNow) {
      localStorage.setItem("percentCredit", param);
      setPercent(param);
    }
  };

  const setStepCredit = (param) => {
    const stepNow = Number(localStorage.getItem("stepCredit"));
    if (param > stepNow) {
      localStorage.setItem("stepCredit", param);
    }

    setStep(param);
    setTimeout(() => {
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    }, 100);
  };

  const submitCredit = async () => {
    setLoadingStep(true);

    const payload = {
      kpr_id: KPRid,
    };

    try {
      // ================================================ hide-digital-signature
      const code = await checkRegister()
      console.log({code});
      if(code === "USER_DO_NOT_EXISTS") {
        const res =  await registerUser()
        if(res.data.status.toUpperCase() == "OK") {
          const submitFinish = await fetch(
            `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${
              id?.substring(0, 5) === "KPRST" ? "stok/setfinish" : "nonstok/setfinish"
            }`,
            {
              method: "POST",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
              },
              body: new URLSearchParams(payload),
            }
          );
          const responseFinish = await submitFinish.json();
          if (!responseFinish.status) {
            throw responseFinish.message;
          } else {
            localStorage.removeItem("dataPengajuan");
            localStorage.removeItem("dataDiri");
            sessionStorage.removeItem("ktp_foto");
            sessionStorage.removeItem("pas_foto");
            sessionStorage.removeItem("slip_foto");
            sessionStorage.removeItem("rek_foto");
            localStorage.removeItem("stepCredit");
            localStorage.removeItem("percentCredit");
            localStorage.removeItem("jenisKPR");
            localStorage.removeItem("kpr_id");
            localStorage.removeItem("kota");
            localStorage.removeItem("provinsi");
            localStorage.removeItem("kecamatan");
            localStorage.removeItem("kodepos");
            localStorage.removeItem("kelurahan");
    
          }
          setLoadingStep(false);
          // setModalSukses(true);
          // modalKprSuccessRef.current.click()
          router.push("/upload_doc/?kprId="+id)
        } else {
          setLoadingStep(false)
        }
      } else if(code == "USER_EXISTS_VERIFIED" || code == "USER_EXISTS_UNVERIFIED") {
        const submitFinish = await fetch(
          `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${
            id?.substring(0, 5) === "KPRST" ? "stok/setfinish" : "nonstok/setfinish"
          }`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            },
            body: new URLSearchParams(payload),
          }
        );
        const responseFinish = await submitFinish.json();
        if (!responseFinish.status) {
          throw responseFinish.message;
        } else {
          localStorage.removeItem("dataPengajuan");
          localStorage.removeItem("dataDiri");
          sessionStorage.removeItem("ktp_foto");
          sessionStorage.removeItem("pas_foto");
          sessionStorage.removeItem("slip_foto");
          sessionStorage.removeItem("rek_foto");
          localStorage.removeItem("stepCredit");
          localStorage.removeItem("percentCredit");
          localStorage.removeItem("jenisKPR");
          localStorage.removeItem("kpr_id");
          localStorage.removeItem("kota");
          localStorage.removeItem("provinsi");
          localStorage.removeItem("kecamatan");
          localStorage.removeItem("kodepos");
          localStorage.removeItem("kelurahan");
  
        }
        router.push("/upload_doc/?kprId="+id)
      } else {
        setErrCode(303)
        openModal.click()
        setLoadingStep(false);
      }
      // ================================================ hide-digital-signature


      // ====================================================================

      // const submitFinish = await fetch(
      //   `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${
      //     id?.substring(0, 5) === "KPRST" ? "stok/setfinish" : "nonstok/setfinish"
      //   }`,
      //   {
      //     method: "POST",
      //     headers: {
      //       "Content-Type": "application/x-www-form-urlencoded",
      //       app_key: process.env.NEXT_PUBLIC_APP_KEY,
      //       secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
      //     },
      //     body: new URLSearchParams(payload),
      //   }
      // );
      // const responseFinish = await submitFinish.json();
      // if (!responseFinish.status) {
      //   throw responseFinish.message;
      // } else {
      //   localStorage.removeItem("dataPengajuan");
      //   localStorage.removeItem("dataDiri");
      //   sessionStorage.removeItem("ktp_foto");
      //   sessionStorage.removeItem("pas_foto");
      //   sessionStorage.removeItem("slip_foto");
      //   sessionStorage.removeItem("rek_foto");
      //   localStorage.removeItem("stepCredit");
      //   localStorage.removeItem("percentCredit");
      //   localStorage.removeItem("jenisKPR");
      //   localStorage.removeItem("kpr_id");
      //   localStorage.removeItem("kota");
      //   localStorage.removeItem("provinsi");
      //   localStorage.removeItem("kecamatan");
      //   localStorage.removeItem("kodepos");
      //   localStorage.removeItem("kelurahan");

      // }
      // setLoadingStep(false);
      // setModalSukses(true)
  // ============================================================================

    } catch (error) {
      setLoadingStep(false);
      // alert("Terjadi Kesalahan")
    } finally {
      // setLoadingStep(false);
    }
  };

  const getDataCabang = async () => {
    setLoadingCabang(true);
    try {
      const dataCabang = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/kantorcabang?tipe=${tipeKPR}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            // defaultHeaders
          },
        }
      );
      const responseCabang = await dataCabang.json();
      setBranches(responseCabang.data);
    } catch (error) {
    } finally {
      setLoadingCabang(false);
    }
  };

  const selectTipeKPR = async () => {
    setLoadingJenis(true);
    try {
      const dataTipe = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/jeniskredit?tipe=${tipeKPR}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            // defaultHeaders
          },
        }
      );
      const responseTipe = await dataTipe.json();
      setJenisKPR(responseTipe.data);
    } catch (error) {
    } finally {
      setLoadingJenis(false);
    }
  };

  if (loadingJenis || loadingCabang || !loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout isLoaderOpen={loadingStep} title="Pengajuan KPR">
      <Breadcrumb active="KPR Konvensional" />
      <div
        id="credit_step_3"
        className={`col-md-6 ${
          typeof window !== "undefined" && window.innerWidth > 850 && "ms-lg-5"
        } ps-lg-4 ${styles.rightForm} p-0 px-lg-3`}
      >
        <div className={`ps-4 pe-3 ${styles.step_wrapper}`}>
          <div className={styles.step_list}>
            <div className={percent >= 25 ? styles.step_active : styles.step}>
              <span className={styles.step_number}>
                {percent > 25 ? <img src="/icons/icons/checklist.svg" /> : 1}
              </span>
            </div>
          </div>
          <div
            className={
              percent >= 50 ? styles.step_tracker_active : styles.step_tracker
            }
          ></div>
          <div className={styles.step_list}>
            <div className={percent >= 50 ? styles.step_active : styles.step}>
              <span className={styles.step_number}>
                {percent > 50 ? <img src="/icons/icons/checklist.svg" /> : 2}
              </span>
            </div>
          </div>
          <div
            className={
              percent > 50 ? styles.step_tracker_active : styles.step_tracker
            }
          ></div>
          <div className={styles.step_list}>
            <div className={percent >= 100 ? styles.step_active : styles.step}>
              <span className={styles.step_number}>
                {percent > 100 ? <img src="/icons/icons/checklist.svg" /> : 3}
              </span>
            </div>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            margin: "8px 8px 20px",
          }}
        >
          <p
            className={`${
              (step <= 2 ? styles.stepActive : "cursor-pointer",
              styles.step_caption)
            }`}
            style={{ width: "100%", textAlign: "left", whiteSpace: "nowrap" }}
            onClick={() => setStepCredit(id?.substring(0, 5) === "KPRST" ? 2 : 1)}
          >
            Pengisian Data
          </p>
          <p
            className={
              (step == 1 ? styles.menuActive : "cursor-pointer",
              styles.step_caption)
            }
            onClick={
              typeof window !== "undefined"
              ? Number(localStorage.getItem("stepCredit")) > 2
                ? () => setStepCredit(3)
                : null
              : null
            }
            style={{ width: "100%", textAlign: "center" }}
          >
            Upload Dokumen
          </p>
          <p
            className={
              (step == 2 ? styles.menuActive : "cursor-pointer",
              styles.step_caption)
            }
            onClick={
              typeof window !== "undefined"
                ? Number(localStorage.getItem("stepCredit")) > 3
                  ? () => setStepCredit(4)
                  : null
                : null
            }
            style={{ width: "100%", textAlign: "right" }}
          >
            Ringkasan
          </p>
        </div>

        <SliderProgress showPercent={showPercent} />
        {step == 1 && (
          <CreditStepOne
            setProgressCredit={(param) => setProgressCredit(param)}
            setStepCredit={(param) => setStepCredit(param)}
            formData={JSON.parse(formStepOne)}
            branches={branches}
            tipeKPR={tipeKPR}
            dataPengajuan={dataPengajuan}
            setDataPengajuan={setDataPengajuan}
            jenisKPR={jenisKPR}
            loadingStep={loadingStep}
            setLoadingStep={setLoadingStep}
            setKPRid={setKPRid}
            setShowPercent={setShowPercent}
            percent={percent}
            KPRid={KPRid}
          />
        )}
        {step == 2 && (
          <CreditStepTwo
            setProgressCredit={(param) => setProgressCredit(param)}
            setStepCredit={(param) => setStepCredit(param)}
            KPRid={KPRid}
            setLoadingStep={setLoadingStep}
            setShowPercent={setShowPercent}
            percent={percent}
          />
        )}
        {step == 3 && (
          <CreditStepThree
            setProgressCredit={(param) => setProgressCredit(param)}
            setStepCredit={(param) => setStepCredit(param)}
            KPRid={KPRid}
            setLoadingStep={setLoadingStep}
            setShowPercent={setShowPercent}
            percent={percent}
          />
        )}
        {step == 4 && (
          <CreditStepFour
            setProgressCredit={(param) => setProgressCredit(param)}
            setStepCredit={(param) => setStepCredit(param)}
            submitCredit={() => submitCredit()}
            KPRid={KPRid}
            setLoadingStep={setLoadingStep}
            tipeKPR={tipeKPR}
          />
        )}
      </div>
      {/* hide-digital-signature */}
      <SuccessGenerate isSuccess={errCode} errorRegister={errorRegister} />
      <button
        type="button"
        data-bs-target="#modalGeneratePdf"
        data-bs-toggle="modal"
        style={{display: "none"}}
        id="modalOpen"
      ></button>

      <Success kprId={id} />

      {/* {modalSukses ? <ModalConfirmation /> : null} */}
      <button
        type="button"
        data-bs-target="#modalGeneratePdf"
        data-bs-toggle="modal"
        style={{display: "none"}}
        id="modalOpen"
      ></button>
      <button
      type="button"
      data-bs-target="#modalKprSuccess"
      data-bs-toggle="modal"
      style={{display: "none"}}
      ref={modalKprSuccessRef}
      ></button>
    </Layout>
  );
}

export default verifiedMember(withAuth(FormAjukanKPR));
