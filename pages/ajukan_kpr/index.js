import Info from "../../components/element/icons/info";
import KprKonf from "../../components/element/icons/kpr_conf";
import KprSyar from "../../components/element/icons/kpr_syar";
import SliderProgress from "../../components/element/slider_progress/slider_progress";
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import styles from "./Credit.module.scss";
import CreditStepOne from "../../components/section/CreditStepOne";
import { useEffect, useRef, useState } from "react";
import CreditStepTwo from "../../components/section/CreditStepTwo";
import CreditStepThree from "../../components/section/CreditStepThree";
import CreditStepFour from "../../components/section/CreditStepFour";
import ModalConfirmation from "../../components/element/modal_confirmation/modal_confirmation";
import SuccessGenerate from "../../components/popup/SuccesGenerate"
import Success from "../../components/popup/Success"
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import { useAppContext } from "../../context";
import { useRouter } from "next/router";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import { continueFillKPR, continueKPRStok } from "../../services/ajukanKPR";
import withAuth from "../../helpers/withAuth";
import verifiedMember from "../../helpers/verifiedMember";
import { useMediaQuery } from "react-responsive";
import axios from "axios";
import { defaultHeaders } from "../../utils/defaultHeaders";

function Credit() {
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 912px)` });
  const router = useRouter();
  const { id } = router.query;
  const { userKey, kprList } = useAppContext();
  const { tipe } = router.query;
  const [percent, setPercent] = useState(
    id?.substring(0, 5) === "KPRST" ? 25 : 0
  );
  const [step, setStep] = useState(id?.substring(0, 5) === "KPRST" ? 2 : 1);
  // const [step, setStep] = useState(router.query.step);
  const url = process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE;

  const [listKpr, setListKpr] = useState([])
  const [formStepOne, setFormStepOne] = useState("{}");
  const [modalSukses, setModalSukses] = useState(false);
  const [errCode, setErrCode] = useState(-6)
  const [errorRegister, setErrorRegister] = useState("")
  const [loaded, setLoaded] = useState(id ? false : true);
  const [branches, setBranches] = useState([]);
  const [tipeKPR, setTipeKPR] = useState(tipe);
  const [modalSucces, setModalSucces] = useState(false);
  const [dataPengajuan, setDataPengajuan] = useState({
    cabang: "",
    jenis: "",
    harga: "",
    uangMuka: "",
    pengajuan: "",
    waktu: "",
    metode: "",
  });
  const [loadingJenis, setLoadingJenis] = useState(false);
  const [loadingCabang, setLoadingCabang] = useState(false);
  const [loadingStep, setLoadingStep] = useState(false);
  const [jenisKPR, setJenisKPR] = useState([]);
  const [KPRid, setKPRid] = useState(id || "");
  const [showPercent, setShowPercent] = useState(percent);
  const [validated, setValidated] = useState(false);
  const [idKavling, setIdKavling] = useState("");
  const [memberDetail, setMemberDetail] = useState({})
  const modalKprSuccessRef = useRef(null)
  const openModal = document.getElementById("modalOpen")

  console.log({listKpr});

  const getKprList = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/${id?.substring(0, 5) === "KPRST" ? "kpr/stok/show" :"kpr/nonstok/show/bymember" }`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          Limit: 1000,
          Sort: "t_ins DESC",
          // page: 1
        }),
      });
      const data = await res.json()
      if(!data.isError) {
        return Promise.resolve(data.Data.map(item => item.id))

      }else {
        throw {
          message: resData.ErrToUser,
        }
      }
        
    } catch (error) {
      console.log(error.message);
    } finally {
    //   setLoadingKpr(false);
    }
  };

  // get detail user
  const getDetailMember = async () => {
    try {
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
        const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
              AccessKey_Member: userKey || JSON.parse(sessionStorage.getItem("keyMember")),
                },
        });
        const response = await loadData.json()
        console.log("aaaaaaaa", response)
            
        setMemberDetail(response.Data[0])

      } catch(error) {
          console.log({error})
      }
    }

    // cek apakah user sudah di register apa belum
    const checkRegister = async () => {
      try {
          const res = await axios.post(url+'/registerCheck', {
              nik: memberDetail?.no_ktp,
              action: "check_nik"
          })
          return "USER_EXISTS_VERIFIED"
      } catch (error) {
          return error.response.data.code
      }
  }
  
  // register user ke tekenaja
  const registerUser = async () => {
    try {
        const body = {
            email: memberDetail?.e,
            mobilePhoneNumber: memberDetail?.no_hp,
            provinceId: memberDetail?.i_prop.toString(),
            districtId: memberDetail?.i_kot.toString().split("").splice(2,4).join(""),
            subDistrictId: memberDetail?.i_kec.toString().split("").splice(4,6).join(""),
            address: memberDetail?.almt,
            postalCode: memberDetail?.pos,
            userId: memberDetail?.id
        }
        const response = await axios.post(url + '/register',body);

        return response;

      } catch (error) {
        console.log("Server Error", error.response)
        const code = error?.response?.data?.code;
        if(code == "EMAIL_EXISTS") {
          setErrCode(304)
          openModal.click()
        } else if(code == "USER_EXISTS") {
          setErrCode(503)
          openModal.click()
        } else if(code == "SELFIE_UNMATCH") {
          setErrCode(505)
          openModal.click()
        } else if(code == "BIODATA_UNMATCH") {
          setErrCode(606)
          openModal.click()
        } else if(code == "EKYC_INVALID") {
          setErrCode(707)
          openModal.click()
        } else if(code == "EKYC_ERROR" || code == "EKYC_PROVIDER_TIMEOUT") {
          setErrCode(808)
          openModal.click()
        } else if(code == "PHONE_EXISTS") {
          setErrCode(202)
          openModal.click()
        } else if(code == "INVALID_PARAMETER") {
          const objMessage = error?.response?.data?.message
          if(typeof objMessage == "string") {
            setErrCode(202)
          } else {
            setErrCode(909)
          }
          openModal.click()
        } else if(code == "SYSTEM_FAILURE") {
          setErrCode(303)
          openModal.click()
        } else {
          setErrCode(303)
          openModal.click()
        }

      }
    }

  if (typeof window !== "undefined") {
    window.onbeforeunload = () => {
      localStorage.removeItem("dataPengajuan");
      localStorage.removeItem("dataDiri");
      sessionStorage.removeItem("ktp_foto");
      sessionStorage.removeItem("pas_foto");
      sessionStorage.removeItem("slip_foto");
      sessionStorage.removeItem("rek_foto");
      localStorage.removeItem("stepCredit");
      localStorage.removeItem("percentCredit");
      localStorage.removeItem("jenisKPR");
      localStorage.removeItem("kpr_id");
      localStorage.removeItem("kota");
      localStorage.removeItem("provinsi");
      localStorage.removeItem("kecamatan");
      localStorage.removeItem("kodepos");
      localStorage.removeItem("kelurahan");
    };
  }

  useEffect(() => {
    getDetailMember()

    if (localStorage.getItem("jenisKPR") !== null) {
      setTipeKPR(JSON.parse(localStorage.getItem("jenisKPR")));
    }
    if (localStorage.getItem("percentCredit") !== null) {
      setPercent(Number(localStorage.getItem("percentCredit")));
      setShowPercent(Number(localStorage.getItem("percentCredit")));
    }
    if (
      localStorage.getItem("kpr_id") !== null &&
      localStorage.getItem("kpr_id") !== ""
    ) {
      setKPRid(localStorage.getItem("kpr_id"));
    }
    if (
      localStorage.getItem("stepCredit") !== null &&
      localStorage.getItem("stepCredit") !== ""
    ) {
      setStep(Number(localStorage.getItem("stepCredit")));
    }
    if (localStorage.getItem("dataPengajuan") !== null) {
      setFormStepOne(localStorage.getItem("dataPengajuan"));
      setFormStepOne("{}");
      setDataPengajuan(JSON.parse(localStorage.getItem("dataPengajuan")));
    }

    const routerLog = sessionStorage.getItem('back_log');

    getKprList().then((data) => {
      console.log({id});
      if(id) {
        console.log("ABC");
        if(!data.includes(id)) {
          router.push("/404")
        }
      }
      
    })

    if (id?.substring(0, 5) === "KPRNS") {
        continueFillKPR(userKey, id)
        .then((res) => {
          if (isTabletOrMobile) {
            if (routerLog) {
              router.push('/member/kredit')
            } else {
              router.push(`/ajukan_kpr/form?tipe=${res.TIPE_PENGAJUAN}&id=${id}`)
            }
          }
          console.log("response : ", res);
          setTipeKPR(res.TIPE_PENGAJUAN);
          let tempPengajuan = { ...dataPengajuan };
          tempPengajuan.cabang = res.ID_CABANG;
          tempPengajuan.jenis = res.JENIS_KREDIT || res.JENIS_PEMBIAYAAN;
          tempPengajuan.harga = res.HARGA_PROPERTI;
          tempPengajuan.uangMuka = res.UANG_MUKA || "      0";
          tempPengajuan.pengajuan = res.NILAI_PENGAJUAN;
          tempPengajuan.waktu = Math.round(Number(res.JANGKA_WAKTU) / 12);
          tempPengajuan.metode = res.PEMBAYARAN_ANGSURAN;
          setDataPengajuan(tempPengajuan);
          localStorage.setItem("kpr_id", id);
          localStorage.setItem("jenisKPR", res.TIPE_PENGAJUAN);
          localStorage.setItem("dataPengajuan", JSON.stringify(tempPengajuan));
          let tempDataDiri = {
            nik: "",
            nama: "",
            kotaLahir: "",
            tglLahir: "",
            email: "",
            hp: "",
            tlpn: "",
            statMenikah: "",
            alamat: "",
            rt: "",
            rw: "",
          };
          tempDataDiri.nik = res.NO_KTP;
          tempDataDiri.nama = res.NAMA_LENGKAP;
          tempDataDiri.kotaLahir = res.TEMPAT_LAHIR;
          tempDataDiri.tglLahir = res.TGL_LAHIR?.split(" ")[0];
          tempDataDiri.email = res.EMAIL;
          tempDataDiri.hp = res.NO_HP1;
          tempDataDiri.tlpn = (res.NO_TELP === "null" || !res.NO_TELP) ? "" : res.NO_TELP;
          tempDataDiri.statMenikah = res.STATUS_KAWIN;
          tempDataDiri.alamat = res.ALAMAT;
          tempDataDiri.rt = res.RT;
          tempDataDiri.rw = res.RW;
          localStorage.setItem("dataDiri", JSON.stringify(tempDataDiri));
          localStorage.setItem("provinsi", res.ID_PROPINSI);
          localStorage.setItem("kota", res.ID_KAB_KOTA);
          localStorage.setItem("kecamatan", res.ID_KECAMATAN);
          localStorage.setItem("kelurahan", res.ID_DESA_KELURAHAN);
          localStorage.setItem("kodepos", res.KODEPOS);
          sessionStorage.setItem("ktp_foto", res.FILE_KTP);
          sessionStorage.setItem("pas_foto", res.FILE_PAS_FOTO);
          sessionStorage.setItem("slip_foto", res.FILE_SLIP_PENGHASILAN);
          sessionStorage.setItem("rek_foto", res.FILE_REK_KORAN);
          let tempStep = 4;
          let tempProgress = 100;

          if (
            !res.FILE_KTP ||
            !res.FILE_PAS_FOTO ||
            !res.FILE_SLIP_PENGHASILAN ||
            !res.FILE_REK_KORAN
          ) {
            tempStep = 3;
            tempProgress = 50;
          }
          for (let data in tempDataDiri) {
            if (!tempDataDiri[data] && data != 'tlpn') {
              tempStep = 2;
              tempProgress = 25;
            }
          }
          if (
            !res.ID_PROPINSI ||
            !res.ID_KAB_KOTA ||
            !res.ID_KECAMATAN ||
            !res.ID_DESA_KELURAHAN ||
            !res.KODEPOS
          ) {
            tempStep = 2;
            tempProgress = 25;
          }
          if (
            !res.ID_CABANG ||
            (!res.JENIS_KREDIT && !res.JENIS_PEMBIAYAAN) ||
            !res.HARGA_PROPERTI ||
            !res.NILAI_PENGAJUAN ||
            !res.JANGKA_WAKTU ||
            !res.PEMBAYARAN_ANGSURAN
          ) {
            tempStep = 1;
            tempProgress = 0;
          }
          localStorage.setItem("stepCredit", tempStep);
          localStorage.setItem("percentCredit", tempProgress);

          setStep(tempStep);
          setPercent(tempProgress);
        })
        .catch((error) => {})
        .finally(() => {
          setLoaded(true);
        });
      
    } else if (id?.substring(0, 5) === "KPRST") {
      continueKPRStok(userKey, id)
        .then((res) => {
          if (isTabletOrMobile) {
            if (routerLog) {
              router.push('/member/kredit/listing')
            } else {
              router.push(`/ajukan_kpr/form?tipe=${res.TIPE_PENGAJUAN}&id=${id}`)
            }
          }
          console.log("response : ", res);
          setIdKavling(res.ID_KAV);
          setTipeKPR(res.TIPE_PENGAJUAN);
          let tempPengajuan = { ...dataPengajuan };
          tempPengajuan.cabang = res.ID_CABANG;
          tempPengajuan.jenis = res.JENIS_KREDIT;
          tempPengajuan.harga = res.HARGA_JUAL;
          tempPengajuan.uangMuka = res.UANG_MUKA || "      0";
          tempPengajuan.pengajuan = res.NILAI_PENGAJUAN;
          tempPengajuan.waktu = Math.round(Number(res.JANGKA_WAKTU) / 12);
          tempPengajuan.metode = res.PEMBAYARAN_ANGSURAN;
          setDataPengajuan(tempPengajuan);
          localStorage.setItem("kpr_id", id);
          localStorage.setItem("jenisKPR", res.TIPE_PENGAJUAN);
          localStorage.setItem("dataPengajuan", JSON.stringify(tempPengajuan));
          let tempDataDiri = {
            nik: "",
            nama: "",
            kotaLahir: "",
            tglLahir: "",
            email: "",
            hp: "",
            tlpn: "",
            statMenikah: "",
            alamat: "",
            rt: "",
            rw: "",
          };
          tempDataDiri.nik = res.NO_ID;
          tempDataDiri.nama = res.NAMA_LENGKAP;
          tempDataDiri.kotaLahir = res.TEMPAT_LAHIR;
          tempDataDiri.tglLahir = res.TGL_LAHIR.split(" ")[0];
          tempDataDiri.email = res.EMAIL;
          tempDataDiri.hp = res.NO_HP1;
          tempDataDiri.tlpn = (res.NO_TELP === "null" || !res.NO_TELP) ? "" : res.NO_TELP;
          tempDataDiri.statMenikah = res.STATUS_KAWIN;
          tempDataDiri.alamat = res.ALAMAT;
          tempDataDiri.rt = res.RT;
          tempDataDiri.rw = res.RW;
          localStorage.setItem("dataDiri", JSON.stringify(tempDataDiri));
          localStorage.setItem("provinsi", res.ID_PROPINSI);
          localStorage.setItem("kota", res.ID_KAB_KOTA);
          localStorage.setItem("kecamatan", res.ID_KECAMATAN);
          localStorage.setItem("kelurahan", res.ID_DESA_KELURAHAN);
          localStorage.setItem("kodepos", res.KODEPOS);
          sessionStorage.setItem("ktp_foto", res.FILE_KTP);
          sessionStorage.setItem("pas_foto", res.FILE_PAS_FOTO);
          sessionStorage.setItem("slip_foto", res.FILE_SLIP_PENGHASILAN);
          sessionStorage.setItem("rek_foto", res.FILE_REK_KORAN);
          let tempStep = 4;
          let tempProgress = 100;
          if (
            !res.FILE_KTP ||
            !res.FILE_PAS_FOTO ||
            !res.FILE_SLIP_PENGHASILAN ||
            !res.FILE_REK_KORAN
          ) {
            tempStep = 3;
            tempProgress = 50;
          }
          for (let data in tempDataDiri) {
            if (!tempDataDiri[data] && data != 'tlpn') {
              tempStep = 2;
              tempProgress = 25;
            }
          }
          if (
            !res.ID_PROPINSI ||
            !res.ID_KAB_KOTA ||
            !res.ID_KECAMATAN ||
            !res.ID_DESA_KELURAHAN ||
            !res.KODEPOS
          ) {
            tempStep = 2;
            tempProgress = 25;
          }
          localStorage.setItem("stepCredit", tempStep);
          localStorage.setItem("percentCredit", tempProgress);
          setStep(tempStep);
          setPercent(tempProgress);
        })
        .catch((error) => {})
        .finally(() => {
          setLoaded(true);
        });
    }

    return () => {
      localStorage.removeItem("dataPengajuan");
      localStorage.removeItem("dataDiri");
      sessionStorage.removeItem("ktp_foto");
      sessionStorage.removeItem("pas_foto");
      sessionStorage.removeItem("slip_foto");
      sessionStorage.removeItem("rek_foto");
      localStorage.removeItem("stepCredit");
      localStorage.removeItem("percentCredit");
      localStorage.removeItem("jenisKPR");
      localStorage.removeItem("kpr_id");
      localStorage.removeItem("kota");
      localStorage.removeItem("provinsi");
      localStorage.removeItem("kecamatan");
      localStorage.removeItem("kodepos");
      localStorage.removeItem("kelurahan");
    };
  }, []);

  useEffect(() => {
    setShowPercent(percent);
  }, [percent]);

  useEffect(() => {
    console.log("responsive query ", isTabletOrMobile);
    if (tipeKPR) {
      console.log("calling get data cabang");
      selectTipeKPR();
      getDataCabang();
    }
  }, [tipeKPR]);

  const changeTipeKPR = (id) => {
    localStorage.setItem("jenisKPR", JSON.stringify(id));
    localStorage.removeItem("dataPengajuan");
    localStorage.removeItem("dataDiri");
    sessionStorage.removeItem("ktp_foto");
    sessionStorage.removeItem("pas_foto");
    sessionStorage.removeItem("slip_foto");
    sessionStorage.removeItem("rek_foto");
    localStorage.removeItem("stepCredit");
    localStorage.removeItem("percentCredit");
    localStorage.removeItem("kpr_id");
    localStorage.removeItem("kota");
    localStorage.removeItem("provinsi");
    localStorage.removeItem("kecamatan");
    localStorage.removeItem("kodepos");
    localStorage.removeItem("kelurahan");
    setDataPengajuan({
      cabang: "",
      jenis: "",
      harga: "",
      uangMuka: "0",
      pengajuan: "",
      waktu: "",
      metode: "",
    });
    setPercent(0);
    setStep(1);
    setJenisKPR([]);
    setKPRid("");
    setTipeKPR(id);

    if (isTabletOrMobile) {
      console.log("id kpr ", tipeKPR);
      if (id) {
        setValidated(true);
      }
    }
  };

  const setProgressCredit = (param) => {
    const stepNow = Number(localStorage.getItem("percentCredit"));
    if (param > stepNow) {
      localStorage.setItem("percentCredit", param);
      setPercent(param);
    }
  };

  const setStepCredit = (param) => {
    const stepNow = Number(localStorage.getItem("stepCredit"));
    if (param > stepNow) {
      localStorage.setItem("stepCredit", param);
    }

    setStep(param);
    setTimeout(() => {
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    }, 100);
  };

  const submitCredit = async () => {
    setLoadingStep(true);

    const payload = {
      kpr_id: KPRid,
    };

    try {
      // ================================================ hide-digital-signature
      const code = await checkRegister()
      console.log({code});
      if(code === "USER_DO_NOT_EXISTS") {
        const res =  await registerUser()
        if(res.data.status.toUpperCase() == "OK") {
          const submitFinish = await fetch(
            `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${
              id?.substring(0, 5) === "KPRST" ? "stok/setfinish" : "nonstok/setfinish"
            }`,
            {
              method: "POST",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
              },
              body: new URLSearchParams(payload),
            }
          );
          const responseFinish = await submitFinish.json();
          if (!responseFinish.status) {
            throw responseFinish.message;
          } else {
            localStorage.removeItem("dataPengajuan");
            localStorage.removeItem("dataDiri");
            sessionStorage.removeItem("ktp_foto");
            sessionStorage.removeItem("pas_foto");
            sessionStorage.removeItem("slip_foto");
            sessionStorage.removeItem("rek_foto");
            localStorage.removeItem("stepCredit");
            localStorage.removeItem("percentCredit");
            localStorage.removeItem("jenisKPR");
            localStorage.removeItem("kpr_id");
            localStorage.removeItem("kota");
            localStorage.removeItem("provinsi");
            localStorage.removeItem("kecamatan");
            localStorage.removeItem("kodepos");
            localStorage.removeItem("kelurahan");
    
          }
          setLoadingStep(false);
          // setModalSukses(true);
          // modalKprSuccessRef.current.click()
          router.push("/upload_doc/?kprId="+id)
        } else {
          setLoadingStep(false)
        }
      } else if(code == "USER_EXISTS_VERIFIED" || code == "USER_EXISTS_UNVERIFIED") {
        const submitFinish = await fetch(
          `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${
            id?.substring(0, 5) === "KPRST" ? "stok/setfinish" : "nonstok/setfinish"
          }`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            },
            body: new URLSearchParams(payload),
          }
        );
        const responseFinish = await submitFinish.json();
        if (!responseFinish.status) {
          throw responseFinish.message;
        } else {
          localStorage.removeItem("dataPengajuan");
          localStorage.removeItem("dataDiri");
          sessionStorage.removeItem("ktp_foto");
          sessionStorage.removeItem("pas_foto");
          sessionStorage.removeItem("slip_foto");
          sessionStorage.removeItem("rek_foto");
          localStorage.removeItem("stepCredit");
          localStorage.removeItem("percentCredit");
          localStorage.removeItem("jenisKPR");
          localStorage.removeItem("kpr_id");
          localStorage.removeItem("kota");
          localStorage.removeItem("provinsi");
          localStorage.removeItem("kecamatan");
          localStorage.removeItem("kodepos");
          localStorage.removeItem("kelurahan");
  
        }
        router.push("/upload_doc/?kprId="+id)
      } else {
        setErrCode(303)
        openModal.click()
        setLoadingStep(false);
      }
      // ================================================ hide-digital-signature


      // ====================================================================

          // const submitFinish = await fetch(
          //   `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${
          //     id?.substring(0, 5) === "KPRST" ? "stok/setfinish" : "nonstok/setfinish"
          //   }`,
          //   {
          //     method: "POST",
          //     headers: {
          //       "Content-Type": "application/x-www-form-urlencoded",
          //       app_key: process.env.NEXT_PUBLIC_APP_KEY,
          //       secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          //     },
          //     body: new URLSearchParams(payload),
          //   }
          // );
          // const responseFinish = await submitFinish.json();
          // if (!responseFinish.status) {
          //   throw responseFinish.message;
          // } else {
          //   localStorage.removeItem("dataPengajuan");
          //   localStorage.removeItem("dataDiri");
          //   sessionStorage.removeItem("ktp_foto");
          //   sessionStorage.removeItem("pas_foto");
          //   sessionStorage.removeItem("slip_foto");
          //   sessionStorage.removeItem("rek_foto");
          //   localStorage.removeItem("stepCredit");
          //   localStorage.removeItem("percentCredit");
          //   localStorage.removeItem("jenisKPR");
          //   localStorage.removeItem("kpr_id");
          //   localStorage.removeItem("kota");
          //   localStorage.removeItem("provinsi");
          //   localStorage.removeItem("kecamatan");
          //   localStorage.removeItem("kodepos");
          //   localStorage.removeItem("kelurahan");
    
          // }
          // setLoadingStep(false);
          // setModalSukses(true)
      // ============================================================================
      
    } catch (error) {
      setLoadingStep(false);
      // alert("Terjadi Kesalahan")

    } finally {
      // setModalSucces(true)
    }
  };

  const getDataCabang = async () => {
    setLoadingCabang(true);
    try {
      const dataCabang = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/kantorcabang?tipe=${tipeKPR}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            // defaultHeaders
          },
        }
      );
      const responseCabang = await dataCabang.json();
      setBranches(responseCabang.data);
    } catch (error) {
    } finally {
      setLoadingCabang(false);
    }
  };

  const onClickNext = (e) => {
    e.preventDefault();
    console.log("clicked next ", branches);
    router.push({
      pathname: "/ajukan_kpr/form",
      query: { tipe: tipeKPR },
    });
  };

  const selectTipeKPR = async () => {
    setLoadingJenis(true);
    try {
      const dataTipe = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/jeniskredit?tipe=${tipeKPR}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            // defaultHeaders
          },
        }
      );
      const responseTipe = await dataTipe.json();
      setJenisKPR(responseTipe.data);
    } catch (error) {
    } finally {
      setLoadingJenis(false);
    }
  };

  if (loadingJenis || loadingCabang || !loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  console.log("isi step", step);

  return (
    <Layout isLoaderOpen={loadingStep} title="Pengajuan KPR">
      <Breadcrumb active="Pengajuan Kredit" />
      <div className="container mb-5">
        <div className="row">
          <div className="col-md-12 col-lg-3 mb-4 mb-lg-0 d-flex">
            <div className="ajukan_left sticky-top w-ipad">
              <h1
                style={{
                  fontSize: "24px",
                  style: "normal",
                  fontFamily: "Futura",
                  fontWeight: "700",
                  lineHeight: "36px",
                }}
                className="title res-ipad-pengajuan"
              >
                Pengajuan Kredit
              </h1>
              <p className="res-ipad-pengajuan">
                Ajukan kredit secara online hanya dalam 10 menit saja dan
                nikmati kemudahan kepemilikan rumah bersama BTN Properti.
              </p>
              {!isTabletOrMobile ? (
                <div className={"sidebar_ajukan " + styles.sideMenu}>
                  <div
                    id="credit_step_2"
                    className={"tipe_kpr " + styles.sideMenu__typeKpr}
                  >
                    <h4
                      style={{
                        fontFamily: "FuturaBT",
                        fontSize: "16px",
                        fontStyle: "normal",
                        fontWeight: "700",
                        lineHeight: "24px",
                        letterSpacing: "0px",
                      }}
                    >
                      Tipe KPR
                    </h4>
                    <div
                      className="item_kpr"
                      style={{
                        marginBottom: "25px",
                        borderBottom: "1px solid #EEEEEE",
                      }}
                    >
                      <input
                        style={{
                          marginTop: "20px",
                          boxSizing: "border-box",
                          height: "20px",
                          width: "20px",
                          position: "unset",
                          color: "#AAAAAA",
                          pointerEvents:
                            id?.substring(0, 5) === "KPRST"
                              ? "none"
                              : "initial",
                        }}
                        className="form-check-input"
                        type="radio"
                        name="type_kpr"
                        id="kprKonf"
                        onChange={() => changeTipeKPR("1")}
                        value="1"
                        checked={tipeKPR === "1" ? true : false}
                      />
                      <label
                        style={{
                          fontFamily: "Helvetica",
                          fontSize: "16px",
                          fontStyle: "normal",
                          fontWeight: "700",
                          lineHeight: "26px",
                          letterSpacing: "0px",
                          color: "#00193E",
                          marginBottom: "20px",
                          marginTop: "16px",
                        }}
                        htmlFor="kprKonf"
                      >
                        <img
                          className="s-radio"
                          src="/images/acc/kpr-konvensional.png"
                          alt=""
                        />
                        KPR Konvensional
                        <CustomTooltips message="KPR Konvensional adalah pinjaman pemilikan rumah dengan bunga float rendah" />
                      </label>
                    </div>
                    {/* <hr style={{ color: "#EEEEEE]" }} /> */}
                    <div className="item_kpr">
                      <input
                        style={{
                          marginTop: "4px",
                          boxSizing: "border-box",
                          height: "20px",
                          width: "20px",
                          position: "unset",
                          color: "#AAAAAA",
                          pointerEvents:
                            id?.substring(0, 5) === "KPRST"
                              ? "none"
                              : "initial",
                        }}
                        className="form-check-input"
                        type="radio"
                        name="type_kpr"
                        id="kprSyar"
                        onChange={() => changeTipeKPR("2")}
                        value="2"
                        checked={tipeKPR === "2" ? true : false}
                      />
                      <label
                        style={{
                          fontFamily: "Helvetica",
                          fontSize: "16px",
                          fontStyle: "normal",
                          fontWeight: "700",
                          lineHeight: "26px",
                          letterSpacing: "0px",
                          color: "#00193E",
                        }}
                        htmlFor="kprSyar"
                      >
                        <img
                          className="s-radio"
                          src="/images/acc/kpr-syariah.png"
                          alt=""
                        />
                        KPR Syariah
                        <CustomTooltips message="KPR Syariah adalah KPR yang ditujukan untuk masyarakat dengan nilai-nilai syariah" />
                      </label>
                    </div>
                  </div>
                  <div id="credit_step_2" className={styles.sideMenu__step}>
                    <ol>
                      <li
                        className={
                          percent > 25
                            ? styles.trackerActive
                            : styles.trackerInactive
                        }
                      >
                        <li className={`${styles.one}`}>
                          <div
                            className={`${styles.iconList} ${
                              percent <= 25 ? styles.iconList__active : ""
                            }`}
                          >
                            <span
                              style={{ background: "#0061A7" }}
                              className="d-flex justify-content-center align-items-center"
                            >
                              {percent > 25 ? (
                                <img src="/icons/icons/checklist.svg" />
                              ) : (
                                1
                              )}
                            </span>
                          </div>
                        </li>
                        <div className={styles.boxMenu}>
                          <h5
                            className={`${
                              step <= 2
                                ? styles.stepActive
                                : step > 2
                                ? styles.actived
                                : "cursor-pointer"
                            }`}
                            onClick={() => {
                              id?.substring(0, 5) === "KPRST"
                                ? setStepCredit(2)
                                : setStepCredit(1);
                            }}
                          >
                            Pengisian Data
                          </h5>
                          <p
                            className={
                              step == 1
                                ? styles.menuActive
                                : step > 1
                                ? styles.actived
                                : "cursor-pointer"
                            }
                            onClick={() => setStepCredit(1)}
                            style={{
                              width: "100%",
                              pointerEvents:
                                id?.substring(0, 5) === "KPRST"
                                  ? "none"
                                  : "initial",
                            }}
                          >
                            Data Pengajuan
                          </p>
                          <p
                            className={
                              step == 2
                                ? styles.menuActive
                                : step > 2
                                ? styles.actived
                                : "cursor-pointer"
                            }
                            onClick={
                              typeof window !== "undefined"
                                ? Number(localStorage.getItem("stepCredit")) >=
                                  2
                                  ? () => setStepCredit(2)
                                  : null
                                : null
                            }
                            style={{ width: "100%" }}
                          >
                            Data Diri
                          </p>
                        </div>
                      </li>
                      <li
                        className={
                          percent > 50
                            ? styles.trackerActive
                            : styles.trackerInactive
                        }
                      >
                        <li className={`${styles.one}`}>
                          <div
                            className={`${styles.iconList} 
                            ${percent == 50 ? styles.iconList__active : ""}`}
                          >
                            <span
                              style={{
                                background:
                                  percent >= 50 ? "#0061A7" : "#aaaaaa",
                              }}
                              className="d-flex justify-content-center align-items-center"
                            >
                              {percent > 50 ? (
                                <img src="/icons/icons/checklist.svg" />
                              ) : (
                                2
                              )}
                            </span>
                          </div>
                        </li>
                        <div className={styles.boxMenu2}>
                          <h5
                            className={
                              step == 3
                                ? styles.stepActive
                                : step > 3
                                ? styles.actived
                                : "cursor-pointer"
                            }
                            onClick={
                              typeof window !== "undefined"
                                ? Number(localStorage.getItem("stepCredit")) >=
                                  3
                                  ? () => setStepCredit(3)
                                  : null
                                : null
                            }
                          >
                            Upload Dokumen
                          </h5>
                        </div>
                      </li>
                      <li>
                        <div
                          className={`${styles.iconList} 
                            ${percent >= 75 ? styles.iconList__active : ""}
                          `}
                        >
                          <span
                            style={{
                              background: percent >= 75 ? "#0061A7" : "#aaaaaa",
                            }}
                            className="d-flex justify-content-center align-items-center"
                          >
                            {percent > 100 ? (
                              <img src="/icons/icons/checklist.svg" />
                            ) : (
                              3
                            )}
                          </span>
                        </div>
                        <div className={styles.boxMenu2}>
                          <h5
                            className={
                              step == 4
                                ? styles.stepActive
                                : step > 4
                                ? styles.actived
                                : "cursor-pointer"
                            }
                            onClick={
                              typeof window !== "undefined"
                                ? Number(localStorage.getItem("stepCredit")) ==
                                  4
                                  ? () => setStepCredit(4)
                                  : null
                                : null
                            }
                          >
                            Ringkasan
                          </h5>
                        </div>
                      </li>
                    </ol>
                  </div>
                </div>
              ) : (
                <div>
                  <div
                    id="credit_step_2"
                    className={"tipe_kpr " + styles.sideMenu__typeKpr}
                  >
                    <h4
                      style={{
                        fontFamily: "FuturaBT",
                        fontSize: "16px",
                        fontStyle: "normal",
                        fontWeight: "700",
                        lineHeight: "24px",
                        letterSpacing: "0px",
                      }}
                    >
                      Tipe KPR
                    </h4>
                    <div
                      className="item_kpr"
                      style={{
                        marginBottom: "25px",
                        borderBottom: "1px solid #eeeeee",
                      }}
                    >
                      <input
                        // style={{
                        //   marginTop: "-8px",
                        //   boxSizing: "border-box",
                        //   height: "20px",
                        //   width: "20px",
                        //   position: "unset",
                        //   color: "#AAAAAA",
                        // }}
                        className="form-check-input radio-kpr radio-kpr-ipad1"
                        type="radio"
                        name="type_kpr"
                        id="kprKonf"
                        onChange={() => changeTipeKPR("1")}
                        value="1"
                        checked={tipeKPR === "1" ? true : false}
                      />
                      <label
                        style={{
                          fontFamily: "Helvetica",
                          fontSize: "16px",
                          fontStyle: "normal",
                          fontWeight: "700",
                          lineHeight: "26px",
                          letterSpacing: "0px",
                          color: "#00193E",
                        }}
                        htmlFor="kprKonf"
                      >
                        <img
                          className="img-kpr-kon"
                          src="/images/acc/kpr-konvensional.png"
                          alt=""
                        />
                        KPR Konvensional
                        <CustomTooltips message="KPR Konvensional adalah pinjaman pemilikan rumah dengan bunga float rendah" />
                      </label>
                    </div>
                    {/* <hr style={{ color: "#EEEEEE]" }} /> */}
                    <div className="item_kpr d-ipad-kpr">
                      <input
                        // style={{
                        //   marginTop: "-8px",
                        //   boxSizing: "border-box",
                        //   height: "20px",
                        //   width: "20px",
                        //   position: "unset",
                        //   color: "#AAAAAA",
                        // }}
                        className="form-check-input radio-kpr radio-kpr-ipad2"
                        type="radio"
                        name="type_kpr"
                        id="kprSyar"
                        onChange={() => changeTipeKPR("2")}
                        value="2"
                        checked={tipeKPR === "2" ? true : false}
                      />
                      <label
                        style={{
                          fontFamily: "Helvetica",
                          fontSize: "16px",
                          fontStyle: "normal",
                          fontWeight: "700",
                          lineHeight: "26px",
                          letterSpacing: "0px",
                          color: "#00193E",
                        }}
                        htmlFor="kprSyar"
                      >
                        <img
                          className="img-kpr-kon"
                          src="/images/acc/kpr-syariah.png"
                          alt=""
                        />
                        KPR Syariah
                        <CustomTooltips message="KPR Syariah adalah KPR yang ditujukan untuk masyarakat dengan nilai-nilai syariah" />
                      </label>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
          {/* side form */}
          {!isTabletOrMobile ? (
            <div
              id="credit_step_3"
              className={`col-md-6 ${
                typeof window !== "undefined" &&
                window.innerWidth > 767 &&
                "ms-lg-5"
              } ps-lg-4 ${styles.rightForm} p-0 px-lg-3`}
            >
              <SliderProgress showPercent={showPercent} />
              {step == 1 && (
                <CreditStepOne
                  setProgressCredit={(param) => setProgressCredit(param)}
                  setStepCredit={(param) => setStepCredit(param)}
                  formData={JSON.parse(formStepOne)}
                  branches={branches}
                  tipeKPR={tipeKPR}
                  dataPengajuan={dataPengajuan}
                  setDataPengajuan={setDataPengajuan}
                  jenisKPR={jenisKPR}
                  loadingStep={loadingStep}
                  setLoadingStep={setLoadingStep}
                  setKPRid={setKPRid}
                  setShowPercent={setShowPercent}
                  percent={percent}
                  KPRid={KPRid}
                  idKavling={idKavling}
                />
              )}
              {step == 2 && (
                <CreditStepTwo
                  setProgressCredit={(param) => setProgressCredit(param)}
                  setStepCredit={(param) => setStepCredit(param)}
                  KPRid={KPRid}
                  setLoadingStep={setLoadingStep}
                  setShowPercent={setShowPercent}
                  percent={percent}
                  idKavling={idKavling}
                />
              )}
              {step == 3 && (
                <CreditStepThree
                  setProgressCredit={(param) => setProgressCredit(param)}
                  setStepCredit={(param) => setStepCredit(param)}
                  KPRid={KPRid}
                  setLoadingStep={setLoadingStep}
                  setShowPercent={setShowPercent}
                  percent={percent}
                />
              )}
              {step == 4 && (
                <CreditStepFour
                  setProgressCredit={(param) => setProgressCredit(param)}
                  setStepCredit={(param) => setStepCredit(param)}
                  submitCredit={() => submitCredit()}
                  KPRid={KPRid}
                  setLoadingStep={setLoadingStep}
                  tipeKPR={tipeKPR}
                  modalSucces={modalSucces}
                  setModalSucces={setModalSucces}
                />
              )}
            </div>
          ) : (
            <div>
              <button
                type="submit"
                className="btn btn-main"
                style={{
                  width: 119,
                  height: 48,
                  padding: 0,
                  fontFamily: "Helvetica",
                  fontSize: 14,
                  fontWeight: 700,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: "200px",
                  margin: "0 auto",
                }}
                disabled={validated ? false : true}
                onClick={onClickNext}
              >
                Selanjutnya
              </button>
            </div>
          )}
        </div>
      </div>

      {/* hide-digital-signature */}
      <SuccessGenerate isSuccess={errCode} errorRegister={errorRegister} />
      <button
        type="button"
        data-bs-target="#modalGeneratePdf"
        data-bs-toggle="modal"
        style={{display: "none"}}
        id="modalOpen"
      ></button>

      <Success kprId={id} />

      {/* {modalSukses ? <ModalConfirmation /> : null} */}

      <button
      type="button"
      data-bs-target="#modalKprSuccess"
      data-bs-toggle="modal"
      style={{display: "none"}}
      ref={modalKprSuccessRef}
      ></button>
    </Layout>
  );
}

const CustomTooltips = ({ message }) => (
  <OverlayTrigger
    placement="bottom"
    overlay={<Tooltip id="tooltip-bottom">{message}</Tooltip>}
  >
    <img
      src="/icons/icons/Vector.svg"
      data-bs-toggle="tooltip"
      data-bs-placement="top"
      style={{ marginLeft: "15px" }}
    />
  </OverlayTrigger>
);

export default verifiedMember(withAuth(Credit));
