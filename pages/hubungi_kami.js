import Kontak from "../components/form/Kontak";
import Layout from "../components/Layout";
import Breadcrumb from "../components/section/Breadcrumb";
import { useState, useEffect } from "react";
import { useMediaQuery } from "react-responsive";

export default function index() {
    const [loading, setLoading] = useState(false);
    const [pageLoaded, setPageLoaded] = useState(false);
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
    const isIpad = useMediaQuery({ query: `(min-width: 577px) and (max-width: 768px)` });

    useEffect(() => {
        setTimeout(() => {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth",
            });
            setPageLoaded(true);
        }, 500);
    }, []);

    if (!pageLoaded) {
        return (
            <div
                style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100vh",
                }}
            >
                <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
            </div>
        );
    }

    return (
        <Layout title="Hubungi Kami" isLoaderOpen={loading}>
            <Breadcrumb active="Hubungi Kami" />
            <section>
                <div className="container mb-5">
                    <div className="row">
                        <div className="col-md-10 offset-md-1">
                            <h4 className="title_page">Hubungi Kami</h4>
                            <div className="card card_kontak hubungi-kami">
                                <div className={`row g-0 ${isTabletOrMobile ? 'd-flex flex-column justify-content-center align-items-center' : ''}`}>
                                    <div className="col-md-6 contact_left " style={{boxShadow:"0 0px 0px"}}>
                                        <div className="d-flex align-items-start item_kontak">
                                            <div className="icon">
                                                {/* <i className="bi bi-telephone"></i> */}
                                                <svg
                                                    width="32"
                                                    height="32"
                                                    viewBox="0 0 32 32"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M8.82667 14.3867C10.7467 18.16 13.84 21.24 17.6133 23.1733L20.5467 20.24C20.9067 19.88 21.44 19.76 21.9067 19.92C23.4 20.4133 25.0133 20.68 26.6667 20.68C27.4 20.68 28 21.28 28 22.0133V26.6667C28 27.4 27.4 28 26.6667 28C14.1467 28 4 17.8533 4 5.33333C4 4.6 4.6 4 5.33333 4H10C10.7333 4 11.3333 4.6 11.3333 5.33333C11.3333 7 11.6 8.6 12.0933 10.0933C12.24 10.56 12.1333 11.08 11.76 11.4533L8.82667 14.3867Z"
                                                        fill="white"
                                                    />
                                                </svg>
                                            </div>
                                            <div className="content">
                                                <h5>Contact Center</h5>
                                                <p>(Kode Area) 1500286</p>
                                            </div>
                                        </div>
                                        <div className="d-flex align-items-start item_kontak">
                                            <div className="icon">
                                                <svg
                                                    width="32"
                                                    height="32"
                                                    viewBox="0 0 32 32"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M15.9998 2.6665C10.8398 2.6665 6.6665 6.83984 6.6665 11.9998C6.6665 18.9998 15.9998 29.3332 15.9998 29.3332C15.9998 29.3332 25.3332 18.9998 25.3332 11.9998C25.3332 6.83984 21.1598 2.6665 15.9998 2.6665ZM15.9998 15.3332C14.1598 15.3332 12.6665 13.8398 12.6665 11.9998C12.6665 10.1598 14.1598 8.6665 15.9998 8.6665C17.8398 8.6665 19.3332 10.1598 19.3332 11.9998C19.3332 13.8398 17.8398 15.3332 15.9998 15.3332Z"
                                                        fill="white"
                                                    />
                                                </svg>
                                            </div>
                                            <div className="content">
                                                <h5>Kantor Pusat</h5>
                                                <p>
                                                    Menara Bank BTN
                                                    <br />
                                                    Jl. Gajah Mada No. 1<br />
                                                    Jakarta 10130
                                                </p>
                                            </div>
                                        </div>
                                        <div className="d-flex align-items-start item_kontak">
                                            <div className="icon">
                                                {/* <i className="bi bi-building"></i> */}
                                                <svg
                                                    width="32"
                                                    height="32"
                                                    viewBox="0 0 32 32"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M15.9998 9.33333V4H2.6665V28H29.3332V9.33333H15.9998ZM7.99984 25.3333H5.33317V22.6667H7.99984V25.3333ZM7.99984 20H5.33317V17.3333H7.99984V20ZM7.99984 14.6667H5.33317V12H7.99984V14.6667ZM7.99984 9.33333H5.33317V6.66667H7.99984V9.33333ZM13.3332 25.3333H10.6665V22.6667H13.3332V25.3333ZM13.3332 20H10.6665V17.3333H13.3332V20ZM13.3332 14.6667H10.6665V12H13.3332V14.6667ZM13.3332 9.33333H10.6665V6.66667H13.3332V9.33333ZM26.6665 25.3333H15.9998V22.6667H18.6665V20H15.9998V17.3333H18.6665V14.6667H15.9998V12H26.6665V25.3333ZM23.9998 14.6667H21.3332V17.3333H23.9998V14.6667ZM23.9998 20H21.3332V22.6667H23.9998V20Z"
                                                        fill="white"
                                                    />
                                                </svg>
                                            </div>
                                            <div className="content">
                                                <h5>Kantor Cabang</h5>
                                                <a
                                                    href="https://www.btn.co.id/id/Locations?LocationType=e8fd9eed-05f6-49e7-b894-7631893ff71c&City=&Keyword="
                                                    className="text-white"
                                                    target="_blank"
                                                    rel="noreferrer"
                                                >
                                                    Lihat Selengkapnya
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div 
                                        className={`col-md-6 ${(!isIpad && !isTabletOrMobile) ? 'p-5 pe-4 pe-xl-0' : 'py-5'} contact_right`}
                                        style={isIpad ? {paddingRight: '1rem', paddingLeft: '1rem', width: '100%'} : 
                                            isTabletOrMobile ? {paddingRight: '1.2rem', paddingLeft: '1.2rem', width: '100%'} :
                                            {}
                                        }
                                    >
                                        <Kontak setLoading={setLoading} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
