import { useState, useEffect, useRef } from "react";
import {useRouter} from "next/router";
import { PDFDocument } from 'pdf-lib';
import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import SuccessGenerate from "../../components/popup/SuccesGenerate"
import Lottie from "react-lottie";
import * as animationData from "../../public/animate_home.json";
import rupiahFormater from "../../helpers/rupiahFormater"
import { QRCode as QR } from "react-qrcode-logo";
import { useAppContext } from "../../context";
import { base64File } from "../../public/resume/base64";
import withAuth from "../../helpers/withAuth";
import axios from "axios";
import moment from 'moment';
import download from 'downloadjs';
import { encryptRsa } from "../../helpers/encryptRsa";
import Cookies from "js-cookie";
import { logout } from "../../helpers/auth";
import verifiedMember from "../../helpers/verifiedMember";




const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

const index = () => {
	const [isLoading, setIsLoading] = useState()
	const { userKey,setUserKey, setUserProfile,userProfile } = useAppContext();
	const router = useRouter()
	const {kprId} = router.query
	const [wording, setWording] = useState("")
    const [detailKPR, setDetailKPR] = useState();
    const [memberDetail, setMemberDetail] = useState({})
    const [dokPersyaratan, setDokPersyaratan] = useState();
    const [statusDoc, setStatusDoc] = useState({})
    const [listDokumen, setListDokumen] = useState();
	const btnRedirect = useRef(null)
    const modalOpen = useRef(null);
	const [redirectUrl, setRedirectUrl] = useState("")
    const [isSuccess, setIsSuccess] = useState(-1)
	const [errorRegister, setErrorRegister] = useState("")
	const [ekycData, setEkycData] = useState({});
    const url = process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE;
    const urlEkyc = process.env.NEXT_PUBLIC_API_HOST_EKYC
	const [urlQr, setUrlQr] = useState(`https://property.mylabzolution.com/verifikasi_document/`)
	console.log({ekycData});
	const singkatKata = (word, len) => {
		let a = word?.split(" ")
		let isSingkat = false
		let counter = 0
		let s = ""
		a?.forEach((word, i) => {
			counter = s.length + word.length + (a.length-(i+1)) + 2
			if(counter > len) {
				isSingkat = true
			}
			console.log({counter});
			if(counter <= len && !isSingkat) {
				s += word + " "
			} else {
				s += word[0]+" "
			}
		})
		return s.slice(0,len)
	}

	// if(redirectUrl) {
	// 	console.log({redirectUrl});
	// 	btnRedirect.current.click()
	// }
	const registerUser = async () => {
		try {
			const body = {
				email: memberDetail?.e,
				mobilePhoneNumber: memberDetail?.no_hp,
				provinceId: memberDetail?.i_prop.toString(),
				districtId: memberDetail?.i_kot.toString().split("").splice(2,4).join(""),
				subDistrictId: memberDetail?.i_kec.toString().split("").splice(4,6).join(""),
				address: memberDetail?.almt,
				postalCode: memberDetail?.pos,
				userId: memberDetail?.id
			}
			const response = await axios.post(url + '/register',body);
	
			return response;
		  } catch (error) {
			const code = error?.response?.data?.code;
			if(code == "EMAIL_EXISTS") {
				setIsSuccess(304)
				modalOpen.current.click()
			  } else if(code == "USER_EXISTS") {
				setIsSuccess(503)
				modalOpen.current.click()
			  } else if(code == "SELFIE_UNMATCH") {
				setIsSuccess(505)
				modalOpen.current.click()
			  } else if(code == "BIODATA_UNMATCH") {
				setIsSuccess(606)
				modalOpen.current.click()
			  } else if(code == "EKYC_INVALID") {
				setIsSuccess(707)
				modalOpen.current.click()
			  } else if(code == "EKYC_ERROR" || code == "EKYC_PROVIDER_TIMEOUT") {
				setIsSuccess(808)
				modalOpen.current.click()
			} else if(code == "PHONE_EXISTS") {
				setIsSuccess(202)
				openModal.click()
			  } else if(code == "INVALID_PARAMETER") {
				const objMessage = error?.response?.data?.message
				if(typeof objMessage == "string") {
				  setIsSuccess(202)
				} else {
				  setIsSuccess(909)
				}
				modalOpen.current.click()
			  } else if(code == "SYSTEM_FAILURE") {
				setIsSuccess(303)
				modalOpen.current.click()
			  } else {
				setIsSuccess(303)
				modalOpen.current.click()
			  }
		  }
		}
	const submitRelog = () => {

		  logout()
		  Cookies.remove("user");
		  Cookies.remove("keyMember");
		  sessionStorage.removeItem("user");
		  sessionStorage.removeItem("keyMember");
		  localStorage.removeItem("user");
		  localStorage.removeItem("keyMember");
		  setUserKey(null);
		  setUserProfile(null);
		  window.location.replace(`${window.location.origin}?stat=relog`)

	  };
	const getMonth = (month) => {
		switch (month) {
			case 0:
				return "Jan";
			case 1:
				return "Feb";
			case 2:
				return "Mar";
			case 3:
				return "Apr";
			case 4:
				return "Mei";
			case 5:
				return "Jun";
			case 6:
				return "Jul";
			case 7:
				return "Agu";
			case 8:
				return "Sep";
			case 9:
				return "Okt";
			case 10:
				return "Nov";
			case 11:
				return "Des";
			default:
				break;
		}
	}
	const truncate = (str) => {
        const arr = str.split(" ")
        let cuttingWord = undefined;
        let maxLength = 0

        arr.forEach((word, i) => {
            // maxLength > 35 ? cuttingWord = i : null
            maxLength += word.length

            console.log({maxLength});
            if(maxLength > 35) {
                cuttingWord = i
                return false
            }
        })
        console.log('lalalal', {firstLine: arr.slice(0,maxLength+1).join(" "),
            secLine: arr.slice(maxLength + 1, arr.length).join(" ")})
        return {
            firstLine: arr.slice(0,maxLength+1).join(" "),
            secLine: arr.slice(maxLength + 1, arr.length).join(" ")
        }
    }

	const checkDocument = async () => {
        // try {
			const body = {
                userId: userProfile.id,
                kprId: kprId
              }
            await axios.post(url + "/documentCheck", body)
			.then(response => {
				console.log(response);
				setStatusDoc(response.data.code);

			})
			.catch(error => {
				console.log(error.response.data);
				setStatusDoc(error.response.data.code);
			})
			
            // setStatusDoc(data?.code)
        // } catch (error) {
        //     console.log(error);
		// 	setStatusDoc(error.response.data.code);
        //     // setIsSuccess(-1)
        // }
    }

	const getEkycData = async () => {
		try {
			setIsLoading(true)
			const response = await axios.get(urlEkyc + "/getEKYCData/?userId=" + userProfile.id, {
				method: "GET",
				headers: {
				  'Accept': 'application/json',
				}
			  })
			setEkycData(response.data.data)
		} catch (error) {
			console.log(error);
			alert("ekyc data not found")
		}
	}
	const getPersyaratanDoc = async () => {
        try {
            const params = `i_kpr=${kprId}&i_member=${userProfile?.id}`
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/ceklist?${params}}`;
            const loadData = await fetch(endpoint, {
              method: "GET",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            },
            });
            const response = await loadData.json();
            if (!response.IsError) {
              console.log("Success get data persyaratan", response);
              if(response.data){
                  setDokPersyaratan(response.data)
              }
            }
            //setIsLoading(false);
          } catch (error) {
            console.log("error get data persyaratan: ", error.message);
            //setIsLoading(false);
          } finally {
        }
    }

	const getDetailDokumen = async () => {
        try {
          const params = `i_kpr=${kprId}&i_member=${userProfile?.id}}`
          const endpoint = `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/dokumen/show?${params}`;
          const loadData = await fetch(endpoint, {
            method: "GET",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                app_key: process.env.NEXT_PUBLIC_APP_KEY,
                secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            }
          });
          const response = await loadData.json();
          console.log("Success get data detail Dokumen", response);
          if (!response.IsError) {
            if (response.data.length) {
                setListDokumen(response.data);
            }
          }
          //setIsLoading(false);
        } catch (error) {
          console.log("error get detail Dokumen: ", error.message);
        //   setIsLoading(false);
        } finally {
        }
      };

	const getDetailMember = async () => {
        try {
            const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
            const loadData = await fetch(endpoint, {
                method: "POST",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                  AccessKey_Member: userKey || JSON.parse(sessionStorage.getItem("keyMember")) || JSON.parse(Cookies.get("keyMember")),
                    },
            });
            const response = await loadData.json()
            console.log("aaaaaaaa", response)
                
            setMemberDetail(response.Data[0])

        } catch(error) {
            console.log({error})
        }
    }

	const getDetailKPR = async () => {
        try {
          let apiHit;
          let idReq = kprId.substring(0, 5);
          if(idReq == "KPRST"){
            apiHit = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/stok/detail`;
          } else {
            apiHit = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/nonstok/show/bymember`;
          }

          setIsLoading(true);
          let accessKey = userKey;
          if (!userKey) {
            accessKey = JSON.parse(sessionStorage.getItem("keyMember")) || JSON.parse(Cookies.get("keyMember"));
          }
          const payload = {
            id: kprId
          };
          const endpoint = apiHit;
          const loadData = await fetch(endpoint, {
            method: "POST",
            headers: {
              AccessKey_Member: accessKey,
            },
            body: new URLSearchParams(payload),
          });
          const response = await loadData.json();
          if (!response.IsError) {
            console.log("Success get data detail KPR", response);
            if (response.Data.length) {
                setDetailKPR(response.Data[0])
                //fetchpointData();
            }
          }
        } catch (error) {
          console.log("error get detail KPR: ", error.message);

        } finally {
        }
      };
	  console.log({memberDetail});
	  const checkRegister = async () => {
		setIsLoading(true)
        try {
            const res = await axios.post(url+'/registerCheck', {
                nik: memberDetail?.no_ktp,
                action: "check_nik"
            })
			console.log(res);
            return res
        } catch (error) {
			console.log(error.response.data.code)
            return error.response.data.code
        }
    }
	  const resendEmail = async () => {
        try {
            const res = await axios.post(url+'/registerCheck', {
                nik: memberDetail?.no_ktp,
                action: "resend_email"
            })
			console.log(res);
            return res
        } catch (error) {
            setIsSuccess(-1)
        }
    }

	const postVerificationData = async (date) => {
		const body = {
			userId: memberDetail?.id,
			kprId,
			documentOwnerName: detailKPR?.nm_lgkp,
			// email: memberDetail?.e,
			// applicantName: detailKPR?.nm_lgkp,
			// documentName: "resume_kpr("+kprId+").pdf",
			// documentDate: date,
			// signEmail: memberDetail?.e,
			// signDate: date
		}
		console.log({body});
		try {
			await axios.post(process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE + '/documentVerification', body);

		} catch (error) {
			console.log(error);
			
		}
	}

	const uploadDoc = async (endpoint, formData, date) => {
        axios
        .post(endpoint, formData)
        .then((res) => {
			console.log({res});
            if(res.data.status == "OK") {
				// postVerificationData(date);
                setIsLoading(false);
				setRedirectUrl(res.data.data.signUrl);
				// setIsSuccess(1);
				// modalOpen.current.click()

            } else {
                setIsLoading(false)
                alert("Maaf, Server sedang bermasalah")
            }
            
        })
        .catch(async (err) => {
            console.log('upload digital sign ', err.response.data.code)
			if(err.response.data.code == "USER_UNREGISTER") {
				const response =  await registerUser()
				if(response?.data?.status?.toUpperCase() == "OK") {
					setIsLoading(false)
					setIsSuccess(0)
					modalOpen.current.click();
				}
			} else if (err.response.data.code == "USER_UNVERIFY_EMAIL") {
				resendEmail()
				setIsSuccess(0)
				modalOpen.current.click();
			} else {
				setIsSuccess(-6)
			}
			setIsLoading(false)

			modalOpen.current.click();

        })
    }

	const generatePDF = async () => {
		try {
		    setIsLoading(true)
		    checkDocument()
		    
			let idReq = kprId.substring(0, 5)
		    const base64 = base64File

		    var today = new Date();
		    var dd = String(today.getDate()).padStart(2, '0');
		    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		    var yyyy = today.getFullYear();
			var tgl = today.getDate();
			var bulan = today.getMonth();
		    var time = (today.getHours() < 10 ? "0" + today.getHours() : today.getHours() ) + ":" + (today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes() )

		    var date = yyyy+"-"+mm+"-"+dd+" " + time;
			today = dd + "/" + mm + "/" + yyyy;
		    var dateTime = today + "%20" + time;
			console.log({today});

		    // setUrlQr(`https://property.mylabzolution.com/verifikasi_document/?id=${detailKPR?.id}&document=resume_kpr.pdf&applicant=${memberDetail?.n}&date=${dateTime}&name=${memberDetail?.n}&email=${memberDetail?.e}&signDate=${dateTime}`)


		    const tgl_lahir = detailKPR.tgl_lhr?.split("T")[0]

		    // Load a PDF with form fields
		    const pdfDoc = await PDFDocument.load(base64)

		    // Get the form containing all the fields
		    const form = pdfDoc.getForm()

		    // Get all fields in the PDF by their names
		    form.getTextField('TANGGAL').setFontSize(8) 
		    form.getTextField('CABANG').setFontSize(8)
		    form.getTextField('ALAMAT_PERUSAHAAN').setFontSize(8)
		    form.getTextField('DATA_SI_ALAMAT_PERUSAHAAN').setFontSize(8)
		    form.getTextField('NAMA_LENGKAP').setFontSize(8)
		    form.getTextField('NO_KTP').setFontSize(8)
		    form.getTextField('TGL_LAHIR').setFontSize(8)
		    form.getTextField('BLN_LAHIR').setFontSize(8)
		    form.getTextField('THN_LAHIR').setFontSize(8)
		    form.getTextField('TEMPAT_LAHIR').setFontSize(8)
		    form.getTextField('KELURAHAN').setFontSize(8)
		    form.getTextField('KECAMATAN').setFontSize(8)
		    form.getTextField('RT').setFontSize(8)
		    form.getTextField('RW').setFontSize(8)
		    form.getTextField('KOTA').setFontSize(8)
		    form.getTextField('KODEPOS').setFontSize(8)
		    form.getTextField('KODEPOS_PERUSAHAAN').setFontSize(8)
		    form.getTextField('NO_TELP').setFontSize(8)
		    form.getTextField('EMAIL').setFontSize(8)
		    form.getTextField('NO_HP1').setFontSize(8)
		    form.getTextField('NO_HP2').setFontSize(8)
		    form.getTextField('KOTA_PERUSAHAAN').setFontSize(8)
		    form.getTextField('NAMA_PERUSAHAAN').setFontSize(8)
		    form.getTextField('BIDANG_USAHA').setFontSize(8)
		    form.getTextField('ALAMAT_RUMAH').setFontSize(8)
		    form.getTextField('HARGA_JUAL').setFontSize(8)
		    form.getTextField('UANG_MUKA').setFontSize(8)
		    form.getTextField('JUMLAH_KREDIT_DIMOHON').setFontSize(8)
		    form.getTextField('JUMLAH_ANGSURAN').setFontSize(8)
		    form.getTextField('JANGKA_WAKTU_KREDIT').setFontSize(8)
		    form.getTextField('PENGHASILAN_BULANAN').setFontSize(8)
		    form.getTextField('DATA_SI_NAMA_LENGKAP').setFontSize(8)
		    form.getTextField('DATA_SI_NO_KTP').setFontSize(8)
		    form.getTextField('DATA_SI_TEMPAT_LAHIR').setFontSize(8)
		    form.getTextField('ALAMAT_AGUNGAN').setFontSize(8)
		    form.getTextField('AGUNGAN_PENJUAL').setFontSize(8)
		    form.getTextField('AGUNGAN_BLOK').setFontSize(8)
		    form.getTextField('AGUNGAN_KELURAHAN').setFontSize(8)
		    form.getTextField('AGUNGAN_KECAMATAN').setFontSize(8)
		    form.getTextField('AGUNGAN_KODEPOS').setFontSize(8)
		    form.getTextField('AGUNGAN_KOTA').setFontSize(8)
		    form.getTextField('AGUNGAN_LUAS_BANGUNAN').setFontSize(8)
		    form.getTextField('ALAMAT_AGUNGAN_2').setFontSize(8)
		    form.getTextField('TTD_PASANGAN_PEMOHON').setFontSize(8)
		    form.getTextField('TTD_PASANGAN_PEMOHON').setAlignment(1)
		    form.getTextField('TTD_PEMOHON_ZCR2').setFontSize(8)
		    form.getTextField('TTD_PEMOHON_ZCR2').setAlignment(1)
		    form.getTextField('NAMA_TEMPAT').setFontSize(5)
		    form.getTextField('NAMA_TEMPAT').setAlignment(1)
		    form.getTextField('TGL_BLN').setFontSize(8)
		    form.getTextField('THN').setFontSize(8)
			form.getTextField('NAMA_IBU').setFontSize(8)
		    
		    // Fill in the basic info fields
		    form.getTextField('TANGGAL').setText(today?.toUpperCase())
		    form.getTextField('CABANG').setText(detailKPR.n_cbg ? detailKPR.n_cbg?.toUpperCase() : "")
		    form.getTextField('ALAMAT_PERUSAHAAN').setText(detailKPR.ALAMAT_PEKERJAAN ? detailKPR.ALAMAT_PEKERJAAN?.toUpperCase() : "")
		    form.getTextField('DATA_SI_ALAMAT_PERUSAHAAN').setText(detailKPR.ALAMAT_PEKERJAAN_PASANGAN ? detailKPR.ALAMAT_PEKERJAAN_PASANGAN?.toUpperCase() : "")
		    form.getTextField('NAMA_LENGKAP').setText(detailKPR.nm_lgkp ? detailKPR.nm_lgkp?.toUpperCase() : "")
		    form.getTextField('NAMA_IBU').setText(ekycData?.biologicalMotherMaidenName ? ekycData?.biologicalMotherMaidenName?.toUpperCase() : "")
		    form.getTextField('NO_KTP').setText(detailKPR.no_ktp ? " "+detailKPR.no_ktp?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('ALAMAT_RUMAH').setText(detailKPR.almt ? detailKPR.almt?.toUpperCase() : "")
		    form.getTextField('TGL_LAHIR').setText(detailKPR.tgl_lhr ? tgl_lahir?.split("-")[2]?.split("").join("    ")?.toUpperCase() : "")
		    form.getTextField('BLN_LAHIR').setText(detailKPR.tgl_lhr ? tgl_lahir?.split("-")[1]?.split("").join("    ")?.toUpperCase() : "")
		    form.getTextField('THN_LAHIR').setText(detailKPR.tgl_lhr ? tgl_lahir?.split("-")[0]?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('TEMPAT_LAHIR').setText(detailKPR.tpt_lhr ? detailKPR.tpt_lhr?.toUpperCase() : "")
		    form.getTextField('RT').setText(detailKPR.rt ? " "+detailKPR.rt?.toString()?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('RW').setText(detailKPR.rw ? " "+detailKPR.rw?.toString()?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('KELURAHAN').setText(detailKPR.n_kel ? detailKPR.n_kel?.toUpperCase() : "")
		    form.getTextField('KECAMATAN').setText(detailKPR.n_kec ? detailKPR.n_kec?.toUpperCase() : "")
		    form.getTextField('KOTA').setText(detailKPR.n_kot ? detailKPR.n_kot?.toUpperCase() : "")
		    form.getTextField('KOTA_PERUSAHAAN').setText(detailKPR.n_kotPekerjaan ? detailKPR.n_kotPekerjaan?.toUpperCase() : "")
		    form.getTextField('NAMA_PERUSAHAAN').setText(detailKPR.nm_prshn ? detailKPR.nm_prshn?.toUpperCase() : "")
		    form.getTextField('KODEPOS_PERUSAHAAN').setText(detailKPR.KODEPOS_PEKERJAAN ? " "+detailKPR.KODEPOS_PEKERJAAN?.toString()?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('KODEPOS').setText(detailKPR.pos ? " "+detailKPR.pos?.toString()?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('EMAIL').setText(detailKPR.eml ? detailKPR.eml?.toUpperCase() : "")
		    form.getTextField('NO_TELP').setText(detailKPR?.no_tlp ?" "+ detailKPR?.no_tlp?.split("").map((a,i)=> i == 3 ? a+ "    ": a).join("   ")?.toUpperCase() : "")
		    form.getTextField('NO_HP1').setText(detailKPR.no_hp1 ? " "+detailKPR.no_hp1?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('NO_HP2').setText(detailKPR.no_hp2 ? " "+detailKPR.no_hp2?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('BIDANG_USAHA').setText(detailKPR.bidangUsaha ? detailKPR.bidangUsaha?.toUpperCase() : "")
		    form.getTextField('HARGA_JUAL').setText(detailKPR?.hargaProperti ? rupiahFormater(`${detailKPR?.hargaProperti}`)?.toUpperCase() : "")
		    form.getTextField('UANG_MUKA').setText(detailKPR.uangMuka ? rupiahFormater(`${detailKPR.uangMuka}`)?.toUpperCase() : "")
		    form.getTextField('JUMLAH_KREDIT_DIMOHON').setText(detailKPR.nl_pgjn ? rupiahFormater(`${detailKPR.nl_pgjn}`)?.toUpperCase() : "")
		    form.getTextField('JUMLAH_ANGSURAN').setText(rupiahFormater(Math.round(eval(detailKPR.nl_pgjn+"/"+detailKPR.jangkaWaktu))))
		    form.getTextField('JANGKA_WAKTU_KREDIT').setText(detailKPR?.jangkaWaktu?.split("").join("   ")?.toUpperCase())
		    form.getTextField('PENGHASILAN_BULANAN').setText(detailKPR.pghsln ? `${detailKPR.pghsln?.toUpperCase()}` : "")
		    form.getTextField('DATA_SI_NAMA_LENGKAP').setText(detailKPR.nm_lgkp_psgn ? detailKPR.nm_lgkp_psgn?.toUpperCase() : "")
		    form.getTextField('DATA_SI_NO_KTP').setText(detailKPR.no_id_psgn ? " "+detailKPR.no_id_psgn?.split("").join("    ")?.toUpperCase() : "")
		    form.getTextField('DATA_SI_TEMPAT_LAHIR').setText(detailKPR.tpt_lhr_psgn ? detailKPR.tpt_lhr_psgn?.toUpperCase() : "")
		    form.getTextField('ALAMAT_AGUNGAN').setText(detailKPR.almt_prpt ? truncate(detailKPR.almt_prpt).firstLine?.toUpperCase() : "")
		    form.getTextField('ALAMAT_AGUNGAN_2').setText(detailKPR.almt_prpt ? truncate(detailKPR.almt_prpt).secLine?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_PENJUAL').setText(detailKPR.n_dev ? detailKPR.n_dev?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_BLOK').setText(detailKPR.kavling?.blk ? detailKPR.kavling.blk?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_KELURAHAN').setText(detailKPR.kavling?.n_kel ? detailKPR.kavling.n_kel?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_KECAMATAN').setText(detailKPR.kavling?.n_kec ? detailKPR.kavling.n_kec?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_KODEPOS').setText(detailKPR.kavling?.pos ? detailKPR.kavling.pos?.toString()?.split("").join("   ")?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_KOTA').setText(detailKPR.kavling?.n_kot ? detailKPR.kavling.n_kot?.toUpperCase() : "")
		    form.getTextField('AGUNGAN_LUAS_BANGUNAN').setText(detailKPR.kavling?.ls_bgn ? detailKPR.kavling.ls_bgn + " m²"?.toUpperCase() : "")
		    form.getTextField('TTD_PEMOHON_ZCR2').setText(singkatKata(detailKPR?.nm_lgkp.toUpperCase(), 26))
			
			// dalam listing
			if(idReq == "KPRST") {
				form.getTextField('HARGA_JUAL').setText(detailKPR?.hrg_jl ? rupiahFormater(`${detailKPR?.hrg_jl}`)?.toUpperCase() : "")
				form.getTextField('UANG_MUKA').setText(detailKPR.uang_mka ? rupiahFormater(`${detailKPR.uang_mka}`)?.toUpperCase() : "")
				form.getTextField('JUMLAH_ANGSURAN').setText(rupiahFormater(Math.round(eval(detailKPR.nl_pgjn+"/"+detailKPR.jng_wkt))))
				form.getTextField('JANGKA_WAKTU_KREDIT').setText(detailKPR?.jng_wkt?.toString()?.split("").join("   ")?.toUpperCase())
				
			}

		    // Jenis kelamin
		    const jenisKelamin = {
		        0: "PEREMPUAN",
		        1: "LAKI",

		    }
		    form.getCheckBox("GENDER_"+ jenisKelamin[memberDetail?.jk]).check()
		    

		    // Jenis Pekerjaan
		    const jenisPekerjaan = {
		        10: "BUMN",
		        12: "BUMN",
		        20: "WIRASWASTA",
		        25: "SWASTA",
		        30: "PNS",
		        50: "PMDN",
		        40: "PROFESSIONAL",
				62: "PNS",
				99: "PROFESSIONAL"
		    }
		    memberDetail?.jns_pkrjn && form.getCheckBox(`PROFESI_${jenisPekerjaan[memberDetail?.jns_pkrjn]}`).check()
		    // Status Menikah
		    const statusMenikah = {
				0: "MENIKAH",
		        10: "MENIKAH",
		        20: "LAJANG",
		        30: "JANDA",
		        50: "JANDA",
		        60: "LAJANG",
		        1: "LAJANG",
		        2: "MENIKAH",
		        3: "JANDA",
		        5: "JANDA",
		        6: "LAJANG"
		    }
			console.log("bbb", detailKPR, statusMenikah[detailKPR?.st_mnkh]);
		    detailKPR?.st_mnkh && form.getCheckBox(`STATUS_${statusMenikah[detailKPR?.st_mnkh]}`).check()



			// Religion
			ekycData?.religion && form.getCheckBox(`AGAMA_${ekycData?.religion}`).check();
			// enableReadOnly


		    // keperluan kredit

			// const dlmListing = {
			// 	1: "ada",
			// 	3: "ada",
			// 	4: "ada",
			// 	6: "ada",
			// 	8: "ada"

			// }
			// const luarListing = {
			// 	1: "ada",
			// 	2: "ada",
			// 	3: "ada"

			// }
			// if(detailKPR?.typ_pgjn == 1) {
			// 	dlmListing[detailKPR?.sft_krdt] && form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').check() 
			// } else {
			// 	luarListing[detailKPR?.sft_krdt] && form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').check()
			// 	form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').enableReadOnly
			// 	form.getCheckBox('KEPERLUAN_KREDIT_KONSUMTIF').enableReadOnly
			// }
			form.getCheckBox('KEPERLUAN_KREDIT_BELI_RUMAH').check() 



		    // Sistem Pembayaran
		    const sistemPembayaran = {
		        10: "KOLEKTIF",
		        20: "AGF",
		        30: "PAYROLL",
		    }
		    detailKPR?.pembayaranAngsuran && sistemPembayaran[detailKPR?.pembayaranAngsuran] && form.getCheckBox(`PEMBAYARAN_${sistemPembayaran[detailKPR?.pembayaranAngsuran]}`).check()
		    detailKPR?.sft_krdt && sistemPembayaran[detailKPR?.sft_krdt] && form.getCheckBox(`PEMBAYARAN_${sistemPembayaran[detailKPR?.sft_krdt]}`).check()
		    // Sumber dana utama ()
		    const sumberDanaUtama = {
		        10: "GAJI",
		        12: "GAJI",
		        25: "GAJI",
		        30: "GAJI",
		        20: "GAJI",
		        50: "GAJI",
		        40: "GAJI",
		        // 20: "HASIL_USAHA",
		        // 50: "INVESTASI",
		        // 40: "LAINNYA",
		    }
		    memberDetail?.jns_pkrjn && form.getCheckBox(`SOF_${sumberDanaUtama[memberDetail?.jns_pkrjn]}`).check();

		    form.getTextField('SOF_LAINNYA_FIELD').setFontSize(8);
		    // memberDetail?.jns_pkrjn == 40 && form.getTextField(`SOF_${sumberDanaUtama[memberDetail?.jns_pkrjn]}_FIELD`).setText(jenisPekerjaan[memberDetail?.jns_pkrjn]);


		    // Penghasilan Per bulan
		    memberDetail?.pghsln && form.getTextField('PENGHASILAN_BULANAN').setText(rupiahFormater(memberDetail?.pghsln));

		    // Penghasilan Per Tahun
		    form.getTextField('PENGHASILAN_TAHUNAN').setFontSize(8);
		    memberDetail?.pghsln && form.getTextField('PENGHASILAN_TAHUNAN').setText(rupiahFormater(eval(`${memberDetail?.pghsln}*12`)));

		    // Data Agunan Dalam Listing

		    // checklist dokumen
		    // 1. slip penghasilan
		    dokPersyaratan?.dok_penghasilan ? form.getCheckBox("SLIP_ADA").check() : form.getCheckBox("SLIP_TDK").check()

		    // 2. KTP
		    dokPersyaratan?.ktp_pemohon ? form.getCheckBox("KTP_ADA").check() : form.getCheckBox("KTP_TDK").check()

		    // 3. Surat Nikah
		    dokPersyaratan?.surat_nikah ? form.getCheckBox("NIKAH_ADA").check() : form.getCheckBox("CERAI_TDK").check()

		    // 4. SK
		    dokPersyaratan?.sk ? form.getCheckBox("SK_ADA").check() : form.getCheckBox("SK_TDK").check()

		    // 5. Rekening
		    dokPersyaratan?.rekening ? form.getCheckBox("REKENING_ADA").check() : form.getCheckBox("REKENING_TDK").check()

		    // 6. NPWP
		    dokPersyaratan?.npwp ? form.getCheckBox("NPWP_ADA").check() : form.getCheckBox("NPWP_TDK").check()

		    // 7. Akta Pendirian
		    dokPersyaratan?.akta_pendirian ? form.getCheckBox("PENDIRIAN_ADA").check() : form.getCheckBox("PENDIRIAN_TDK").check()

		    // 8. Perizinan
		    dokPersyaratan?.izin ? form.getCheckBox("IZIN_ADA").check() : form.getCheckBox("PERIZINAN_TDK").check()

		    // 9. Laporan Keuangan
		    dokPersyaratan?.laporan_keuangan ? form.getCheckBox("KEUANGAN_ADA").check() : form.getCheckBox("KEUANGAN_TDK").check()

		    // 10. Surat Pernyataan lain-lain
		    dokPersyaratan?.lain_lain ? form.getCheckBox("PERNYATAAN_ADA").check() : form.getCheckBox("PERNYATAAN_TDK").check()

		    // 11. Surat Pernyataan belum memiliki rumah
		    dokPersyaratan?.blm_memiliki_rumah ? form.getCheckBox("PENRNYATAAN_BLMRUMAH_ADA").check() : form.getCheckBox("PERNYATAAN_BLMRUMAH_TDK").check()

		    // 12. Surat pernyataan permohonan kpr bersubsidi
		    dokPersyaratan?.kpr_subsidi ? form.getCheckBox("KPR_ADA").check() : form.getCheckBox("KPR_TDK").check()

		    // 13. Dokumen Jaminan
		    dokPersyaratan?.jaminan ? form.getCheckBox("JAMINAN_ADA").check() : form.getCheckBox("JAMINAN_TDK").check()


		    // 14. KK
		    dokPersyaratan?.kartu_keluarga ? form.getCheckBox("KK_ADA").check() : form.getCheckBox("KK_TDK").check()

		    // nama & ttd
		    dokPersyaratan?.nm_lgkp && form.getTextField('TTD_PEMOHON_ZCR2').setText(singkatKata(detailKPR?.nm_lgkp.toUpperCase(), 26))
		    dokPersyaratan?.nm_lgkp_psgn && form.getTextField('TTD_PASANGAN_PEMOHON').setText(detailKPR?.nm_lgkp_psgn.toUpperCase())
		    // form.getTextField('TTD_PEMOHON_ZCR2').setText("HAYABUSA")
		    // form.getTextField('TTD_PASANGAN_PEMOHON').setText("KAGURA")

		    // tempat, cabang, tanggal
		    const dateMomen = moment(new Date())
		    // form.getTextField('NAMA_TEMPAT').setText(detailKPR?.n_cbg ? detailKPR?.n_cbg?.toUpperCase() : "KOSONG")
		    form.getTextField('TGL_BLN').setText(`${tgl} ${getMonth(bulan)?.toUpperCase()}`)
		    form.getTextField('THN').setText(dateMomen.format("YY"))

			// enable read only TEXTFIELD
			const formFields = form.getFields();
            formFields.forEach(field => {
				field.enableReadOnly()

                // if(type == "e") {
                
                // } else {
				// 	form.getCheckBox(name).enableReadOnly()
				// }
            })


// ============== inset Qr to pdf ===================
		    // const dataUrl = document.getElementById("QR").toDataURL("image/jpeg")
		    // console.log({dataUrl})
		    // const pages = pdfDoc.getPages();
		    // const firstPage = pages[3];
		      
		    // Fetch JPEG image
		    // const jpgUrl = 'https://pdf-lib.js.org/assets/cat_riding_unicorn.jpg';
		    // const jpgImageBytes = await fetch(jpgUrl).then((res) => res.arrayBuffer());
		        
		    // const jpgImage = await pdfDoc.embedJpg(dataUrl);
		    // const jpgDims = jpgImage.scale(0.25);

		    
		      

		    // Get the width and height of the first page
		    // const { width, height } = firstPage.getSize();
	        // firstPage.drawImage( jpgImage ,{
	        //     x: 460,
	        //     y: height / 2 - 250,
	        //     width: 70,
	        //     height: 70,
	        //     opacity: 0.75,
	        // })

                
			

              
            // Serialize the PDFDocument to bytes (a Uint8Array)
            const pdfBytes = await pdfDoc.save();

            const file = new File([pdfBytes], "Dokumen Permohonan Kredit("+kprId+").pdf", { type: "application/pdf" });
            console.log({file});

            const formData = new FormData();

            const signature = JSON.stringify([{
                email: memberDetail?.e,
            }])

            formData.append("userId", memberDetail?.id);
            formData.append("kprId", kprId);
            formData.append("document", file);
            formData.append("documentOwnerName", detailKPR?.nm_lgkp);
            // formData.append("email", "cobaemail997@gmail.com");
            formData.append("signature", signature);
            console.log({formData});


            let endpoint = `${process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE}/upload`;

			const res = await checkRegister()
			console.log({res});
			if(res.data?.status == "OK") {
				console.log({statusDoc});
				if(statusDoc === "DOCUMENT_NOT_FOUND") {
					console.log("ke upload");
					await uploadDoc(endpoint, formData, date);
					await checkDocument()

				} else {
					router.push("/member/kredit/detail/?id=" + kprId)
					setIsLoading(false)
				}

			}
			else if(res == "USER_EXISTS_UNVERIFIED") {
				await resendEmail()
				setIsLoading(false)
				setIsSuccess(0)
				modalOpen.current.click();
			}
			else if(res == "USER_DO_NOT_EXISTS") {
				setIsLoading(false)
				alert("Kamu belum terdaftar di tekenAja!, silahkan ulangi pengajuan")
			}
			else if(res == "USER_EXISTS_CERTIFICATE_EXPIRED") {
				setIsLoading(false)
				const response =  await registerUser()
				if(response.data.status.toUpperCase() == "OK") {
					setIsLoading(false)
					setIsSuccess(0)
					modalOpen.current.click();
				}
			}
    
                // Trigger the browser to download the PDF document
            // download(pdfBytes, "resume_kpr.pdf", "application/pdf");
			// setIsLoading(false)
        } catch(err){
            console.log("error generate pdf : ", err)
            setIsLoading(false)
            setIsSuccess(-1)
            modalOpen.current.click()
        }
    }

	useEffect(() => {
		if(redirectUrl) {
			btnRedirect.current.click()
			router.push("/member/kredit/detail/?id="+ kprId)
		}
	}, [redirectUrl])

	useEffect( async () => {
		if(memberDetail.no_ktp) {			
			const code = await checkRegister()
			if (code.data?.status == "OK") {
				setWording("1 langkah selanjutnya kamu akan diarahkan ke halaman mitra Bank BTN untuk proses penandatanganan Form Aplikasi Kredit")
				
			} else if (code == "USER_EXISTS_UNVERIFIED") {
				setWording("Silakan cek email kamu untuk melakukan aktivasi sertifkat digital. Kamu akan diarahkan ke halaman mitra BTN untuk proses penandatanganan Form Aplikasi Kredit")
				
			}
			setIsLoading(false)
		}
	}, [memberDetail])
	
	useEffect(() => {
		if(!sessionStorage.user) {
			submitRelog()
			
		}
	}, [])

	useEffect(() => {
		if(router.isReady) {
			getDetailKPR()
			getDetailMember()
			getPersyaratanDoc()
			checkDocument()
			getDetailDokumen()
			getEkycData()

		}

	}, [])
	return (
		(
			<Layout title="Verified Now - Dashboard Member | BTN Properti" isLoaderOpen={isLoading}>
			<Breadcrumb active="Tanda tangan" />
			<div className="dashboard-content mb-5">
				<div className="container">
				<div className="row justify-content-center align-items-center px-4">
					<div className="col-md-5 text-start">
					<SuccessGenerate isSuccess={isSuccess} url={redirectUrl} errorRegister={errorRegister} />
						<a ref={btnRedirect} id="redirect" href={redirectUrl} target="_blank" style={{display: "none"}}></a>
						<button
							type="button"
							data-bs-target="#modalGeneratePdf"
							data-bs-toggle="modal"
							style={{display: "none"}}
							ref={modalOpen}
						></button>
					<div style={{display: "none"}}>
						{/* <QR
						value={
							"https://property.mylabzolution.com/verifikasi_document/?id="+kprId
						} // here you should keep the link/value(string) for which you are generation promocode (QR URL FOR DEVELOPMENT)
						size={250} // the dimension of the QR code (number)
						logoImage="/images/qr_logo3.jpg" // URL of the logo you want to use, make sure it is a dynamic url
						logoHeight={65}
						logoWidth={65}
						logoOpacity={1}
						enableCORS={true} // enabling CORS, this is the thing that will bypass that DOM check
						qrStyle="squares" // type of qr code, wether you want dotted ones or the square ones
						eyeRadius={10} // radius of the promocode eye
						id={"QR"}   
						/> */}
					</div>
					<Lottie
						options={defaultOptions}
						isStopped={false}
						isPaused={false}
					/>
					</div>
					<div className="col-md-5">
					<h1
						style={{
						fontFamily: "FuturaBT",
						fontWeight: 700,
						color: "rgb(0, 25, 62)",
						}}
					>
						Pengajuan KPR Kamu Telah Berhasil!
					</h1>
					<p
						style={{
						fontFamily: "Helvetica",
						}}
					>
						{wording}
					</p>
					<button
						className="btn btn-outline-main btn_rounded  w-100"
						style={{
						maxWidth: "300px",
						padding: "10px",
						fontFamily: "Helvetica",
						fontSize: "14px",
						fontWeight: 700,
						display: "flex",
						justifyContent: "center",
						alignItems: "center",
						borderRadius: "200px",
						}}
						onClick={generatePDF}
					>
						Tanda tangan Form Aplikasi
					</button>
					</div>
				</div>
				</div>
			</div>
			</Layout>
	
			)
	)
}



export default withAuth(index);