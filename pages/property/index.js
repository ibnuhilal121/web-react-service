import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import ItemPerumahan from "../../components/data/ItemPerumahan";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";
import axios from "axios";
import qs from "qs";
import LayoutPencarianProperti from "../../components/section/pencarianProperti/LayoutPencarianProperti";
import NotFound from "../../components/element/NotFound";
// import { useAppContext } from "../../context";
import GoogleMapClickable from "../../components/GoogleMapClickable";
import BarsLoader from "../../components/element/BarsLoader";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function property() {
  const router = useRouter();
  const { query, asPath } = router;
  const { lokasi, budget, hargaMin, kamarTidur, kamarMandi, fasilitas, subsidi, sort, luasTanahMin, luasTanahMax, luasBangunanMin, luasBangunanMax, virtual } = query;
  const [items, setItems] = useState([]);
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [paginationLength, setPaginationLength] = useState(1);
  const [view, setView] = useState("grid");
  const [userLoc, setUserLoc] = useState(null);
  const [mapCenter, setMapCenter] = useState(null);
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });

  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();

  const tipeProperti = query.tipeProperti?.length > 0 ? query.tipeProperti?.split(",") : null;
  const queryObj = {
    ...(lokasi && { lokasi }),
    ...(tipeProperti && { tipeProperti }),
    ...(sort && { sort }),
    ...(subsidi && { subsidi }),
    ...(virtual && { virtual }),
    // numeric values untuk keperluan fields
    ...(budget && { hargaMax: budget * 1 }),
    ...(hargaMin && { hargaMin: hargaMin * 1 }),
    ...(kamarTidur && { kamarTidur: kamarTidur * 1 }),
    ...(kamarMandi && { kamarMandi: kamarMandi * 1 }),
    ...(luasTanahMin && { luasTanahMin: luasTanahMin * 1 }),
    ...(luasTanahMax && { luasTanahMax: luasTanahMax * 1 }),
    ...(luasBangunanMin && { luasBangunanMin: luasBangunanMin * 1 }),
    ...(luasBangunanMax && { luasBangunanMax: luasBangunanMax * 1 }),
    // array
    ...(fasilitas && { fasilitas: fasilitas.split(",") }),
  };

  useEffect(() => {
    let newQueryObj = {
      ...queryObj,
      halKe: query.currentPage || 1,
      ...(tipeProperti && { tipeProperti: tipeProperti?.toString() }),
    };
    delete newQueryObj.fasilitas;

    const queryString = fasilitas ? qs.stringify(newQueryObj) + `&fasilitas=[${fasilitas}]` : qs.stringify(newQueryObj);
    // console.log('~ queryString', queryString)
    setMessage("");
    setIsLoading(true);
    axios({
      url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/search?${queryString}&Limit=15`,
      method: "GET",
      headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },

      cancelToken: source.token,
    })
      .then((res) => {
        // console.log("~ res.data", res.data);
        // console.log("~ type", typeof res.data);
        setIsDataNotFound(false)
        if (res.data.IsError) {
          setIsDataNotFound(true)
          throw res.data.ErrToDev;
        } else {
          if(res.data.data.length > 0) {
            setItems(res.data.data);
            setPaginationLength(res.data.pagging.jumlahHalTotal);
            setCurrentPage(newQueryObj.halKe * 1);
            setIsLoading(false);
          } else {
            setIsDataNotFound(true)
          }
        }
      })
      .catch((err) => {
        console.log("~ err", err.response);
        // console.log('~ err.response', err.response)
        if(err?.response?.status == 404) {
        setIsDataNotFound(true)
        }
        setItems([]);
        setMessage(err.response?.data?.message);
        setIsLoading(false);
      });
    // console.log('~ lokasi, tipe, budget', lokasi, tipe, budget)
    // console.log('~ asPath', asPath)
    return () => source.cancel("useEffect states changed.");
  }, [asPath]);
  function handleSubmit(data) {
    // console.log('~ data', data)
    const { hargaMax, fasilitas, tipeProperti, ...restData } = data;
    const newQueryData = {
      budget: data.hargaMax,
      ...(tipeProperti?.length > 0 && {
        tipeProperti: tipeProperti.toString(),
      }),
      ...(fasilitas?.length > 0 && { fasilitas: fasilitas.toString() }),
      tab: "perumahan",
      ...restData,
    };
    router.push(`?${qs.stringify(newQueryData)}`, undefined, { scroll: false });
  }
  function onChangePage(n) {
    setCurrentPage(n);
    handleSubmit({
      ...queryObj,
      currentPage: n,
    });
  }
  function handleFoundCoords(coords) {
    console.log("~ coords", coords);
    setUserLoc(coords);
    setMapCenter(coords);
  }

  return (
    <Layout title="Daftar Properti | BTN Properti">
      <Breadcrumb active="Cari Rumah" />
      <section className="mb-5" id="propertyPagePerumahan">
        <div className="container">
          <LayoutPencarianProperti
            queryObj={queryObj}
            handleSubmit={handleSubmit}
            paginationLength={paginationLength}
            currentPage={currentPage}
            onChangePage={onChangePage}
            tab={"properti"}
            noResult={view === "grid" ? !items?.length > 0 : true}
            view={view}
            setView={setView}
            onFoundCoords={handleFoundCoords}
          >
            {isLoading ? (
              <BarsLoader />
            ) : items?.length > 0 ? (
              view === "grid" ? (
                <div className="row w-100 d-flex justify-content-start">
                  {items.map((data, index) => (
                    <div key={index} className="col-6 col-md-6 col-lg-4 mb-4 card-item">
                      <ItemPerumahan data={data} action="false" detail="false" />
                    </div>
                  ))}
                </div>
              ) : (
                <div className="row" style={{ width: "100%", height: 650 }}>
                  <GoogleMapClickable
                    markers={items.map((val) => ({
                      lat: val.LATITUDE * 1,
                      lng: val.LONGITUDE * 1,
                      id: val.ID,
                      priceString: val.HARGA_MULAI,
                      card: (
                        <>
                          {isTabletOrMobile ? (
                            <div id="card-maps-search" className="card-item mb-2">
                              <ItemPerumahan view="map" wrapperClass="d-flex flex-row" data={val} action="false" detail="false" />
                            </div>
                          ) : (
                            <div className="card-item">
                              <ItemPerumahan data={val} action="false" detail="false" />
                            </div>
                          )}
                        </>
                      ),
                    }))}
                    mapCenter={mapCenter}
                    userLoc={userLoc}
                  />
                </div>
              )
            ) : (
              isDataNotFound ? <NotFound /> : <BarsLoader />
            )}
          </LayoutPencarianProperti>
        </div>
      </section>
    </Layout>
  );
}
