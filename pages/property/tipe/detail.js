/* eslint-disable react/jsx-key */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
import { useRouter } from "next/router";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import ShareSosmed from "../../../components/static/ShareSosmed";
import Link from "next/link";
import GaleryUnit from "../../../components/section/GaleryUnit";

import styles from "../../../styles/property_unit.module.scss";
import OverflowKpr from "../../../components/section/OverflowKpr";
import { useEffect, useState } from "react";
import {
  doGetAreaSekitar,
  doGetIklanTipeRumahById,
  doGetKavling,
  doGetDeveloperById,
  doGetProperById,
  doGetTipeRumahById,
} from "../../../services/property";
import { generateImageUrl } from "../../../utils/generateImageUrl";
import GoogleMaps from "../../../components/GoogleMaps";
import ReactImageFallback from "react-image-fallback";
import { FACILITIES_PATH_ICONS } from "../../../constants/fasilitasIconPath";
import { useMediaQuery } from "react-responsive";
import AntdCarousel from "../../../components/carousel/AntdCarousel"
import { useIdToLabel } from "../../../helpers/pencarianProperti";

export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  // const { id } = router.query;
  const query = router.query.id;
  const isMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const [isLoading, setIsLoading] = useState(false);
  const [dataDetail, setDataDetail] = useState({
    ID: null,
    NAMA: null,
    ID_DEV: null,
    ID_PROPER: null,
    JENIS: null,
    KLASTER: null,
    BLOK: null,
    SUBSIDI: null,
    GBR1: null,
    GBR2: null,
    GBR3: null,
    GBR4: null,
    GBR5: null,
    LATITUDE: null,
    LONGITUDE: null,
    DESKRIPSI: null,
    KAMAR_TIDUR: null,
    KAMAR_MANDI: null,
    FASILITAS: null,
    LANTAI: null,
    DAYA_LISTRIK: null,
    LUAS_TANAH: null,
    LUAS_BANGUNAN: null,
    HARGA_MULAI: null,
    HARGA_SAMPAI: null,
    BOOKING_FEE: null,
    SERTIFIKAT: null,
    ALAMAT: null,
    KODEPOS: null,
    ID_PROPINSI: null,
    ID_KAB_KOTA: null,
    ID_KECAMATAN: null,
    ID_DESA_KELURAHAN: null,
    TGL_INSERT: null,
    KEYWORD: null,
    CATATAN: null,
    GARASI_LAMA: null,
    JALUR_TELEPON_LAMA: null,
    JALUR_PDAM_LAMA: null,
    NAMA_PROPINSI_LAMA: null,
    NAMA_KAB_KOTA_LAMA: null,
    NAMA_KECAMATAN_LAMA: null,
    NAMA_DESA_KELURAHAN_LAMA: null,
    VIDEO: null,
    STATUS_PENGAJUAN: null,
    TGL_UPDATE_STATUS: null,
    STATUS: null,
    NAMA_PROPER: null,
    NAMA_DEV: null,
    PROPINSI: null,
    KAB_ATAU_KOTA: null,
    KAB_KOTA: null,
    KECAMATAN: null,
    DESA_ATAU_KEL: null,
    DESA_KEL: null,
    JML_KAVLING: null,
    JML_DILIHAT: null,
    EMBED360: null,
    URL_PROMO: null,
    STATUS_PROMO: null,
    pmt_konvensional: null,
    pmt_syariah: null,
    sk_bga: null,
    sk_bga_syariah: null,
  });

  const [dataKpr, setDataKpr] = useState({
    id: null,
    propertyName: null,
    propertyId: null,
    lokasi: null,
    lb: null,
    lt: null,
    price: 0,
    sukuBunga: 0,
    sukuBungaSyariah: 0,
    jumlahKamarTidur: null,
    jumlahKamarMandi: null,
    pmt_konvensional: 0,
    pmt_syariah: 0,
    jenisSubsidi: 1,
  });

  const [dataProperti, setDataProperti] = useState({});

  const [contactInfo, setContactInfo] = useState({
    name: null,
    location: null,
    phone: null,
    email: null,
    website: null,
  });

  const [imagesGalery, setImagesGalery] = useState([]);
  const [fasilitas, setFasilitas] = useState([]);
  const [kavlingList, setKavlingList] = useState([]);
  const [selectedKav, setSelectedKav] = useState(0);
  const [positionMap, setPositionMap] = useState({
    lat: -7.7956,
    lng: 110.3695,
  });

  const [areaSekitar, setAreaSekitar] = useState([])
  const idToLabel = useIdToLabel()

  let id = ""
  if (query) {
    let path = query.split('-')
    id = path[path.length - 1]
  }

  // HANDLER FOR STATE
  function loadingHandler(bool) {
    setIsLoading(bool);
  }

  // useEffect(() => {
  //   if (sessionStorage.getItem('previous') == '/tools/komparasi') {
  //     router.push(router.asPath + "?showForm=1");
  //     sessionStorage.removeItem('previous');
  //   };
  // }, [id]);

  useEffect(() => {
    // console.log('~ dataDetail', dataDetail)
  }, [dataDetail])

  useEffect(() => {
    if (id?.includes("IKL")) {
      doGetIklanTipeRumahById(loadingHandler, id)
        .then(({ Data }) => {
          const data = Data?.[0]
          console.log('~ log alvin data', data)
          setDataDetail({
            NAMA_PROPER: data?.jdl,
            NAMA_KECAMATAN_LAMA: data?.n_kec,
            NAMA_KAB_KOTA_LAMA: data?.n_kot,
            NAMA_DEV: data?.n_dev || data?.n_memb,
            KLASTER: data?.clstr,
            BLOK: data?.blk || data?.lt,
            ALAMAT: data?.almt,
            DAYA_LISTRIK: data?.dy_lstr,
            JALUR_PDAM_LAMA: '-',
            JALUR_TELEPON_LAMA: '-',
            LANTAI: data?.lt,
            SERTIFIKAT: data?.srtfkt,
            SUBSIDI: data?.st_ssd == "0" ? "Non Subsidi"
            : dataDetail.SUBSIDI == "1" ? 'Subsidi'
              : "-",
            JENIS: data?.jns == 1 ? "Rumah Baru"
              : data?.jns == 2 ? "Rumah Bekas"
                : data?.jns == 3 ? "Apartemen Baru"
                  : data?.jns == 5 ? "Apartemen Bekas"
                  : dataDetail.JENIS == 6 ? "Ruko"
                  : dataDetail.JENIS == 7 ? "Kantor"
                    : "-",
            NO_TELP: data?.tlp_memb
          })
          setDataKpr({
            id: data?.id,
            propertyName: `${data.clstr === 'null' ? "-" : data.clstr } | ${data.jdl}`,
            propertyId: data.i_prop,
            lokasi: data.n_kot,
            lb: data.ls_bgn,
            lt: data.ls_tnh,
            price: data.hrg,
            sukuBunga: data.sk_bga,
            sukuBungaSyariah: data.sk_bga_syariah,
            jumlahKamarTidur: data.km_tdr,
            jumlahKamarMandi: data.km_mnd,
            pmt_konvensional: data.pmt_konvensional,
            pmt_syariah: data.pmt_syariah,
          });
          setPositionMap({
            lat: parseFloat(data.lat),
            lng: parseFloat(data.lon),
          });
          const imgGalery = [];
          if (data.gbr1) {
            imgGalery.push(data.gbr1);
          }
          if (data.gbr2) {
            imgGalery.push(data.gbr2);
          }
          if (data.gbr3) {
            imgGalery.push(data.gbr3);
          }
          if (data.gbr4) {
            imgGalery.push(data.gbr4);
          }
          if (data.gbr5) {
            imgGalery.push(data.gbr5);
          }
          setImagesGalery(imgGalery);
          const facilitiesObj = JSON.parse(data.fslts);
          // console.log('~ facilitiesObj', facilitiesObj)
          const facilitiesArr = Object.entries(
            facilitiesObj["Fasilitas"]
          );
          const newFacilitiesArr = [];
          facilitiesArr.map((item) => {
            let iconPath = "M13 17L18 12L13 7";
            FACILITIES_PATH_ICONS.map((icon) => {
              if (icon.code === parseInt(item[0])) {
                iconPath = icon.path;
              }
            });
            newFacilitiesArr.push({
              code: item[0],
              label: item[1],
              iconPath: iconPath,
            });
          });
          setFasilitas(newFacilitiesArr);
          const aksesArr = Object.entries(
            facilitiesObj["Akses"]
          );
          const newAksesArr = aksesArr?.map(val => ({ label: val[1] }))
          setAreaSekitar(newAksesArr);

          setDataProperti({
            NAMA_PROPER: data.jdl,
            ALAMAT: data.almt,
            NO_TELP: data.tlp_memb,
          })

          doGetDeveloperById(loadingHandler, data.i_memb)
            .then((response) => {
              const { data } = response;
              if (data) {
                setContactInfo(data);
              }
            })
            .catch((err) => {
              console.log("Error Get Data Developer", err);
            });

          doGetKavling(loadingHandler, id)
            .then((response) => {
              const { data } = response;
              if (data) {
                setKavlingList(data);
              }
            })
            .catch((err) => {
              console.log("Error Get Data Kavling", err);
            });
        }).catch((err) => {
          console.log('~ err', err)

        });
    }
    else if (id) {
      doGetTipeRumahById(loadingHandler, id)
        .then((response) => {
          // console.log("+++ response.data", response.data);
          const { data } = response;
          if (data) {
            console.log('log alvin ', data)
            setDataDetail(data);
            setDataKpr({
              id: data.ID,
              propertyName: `${data.KLASTER} | ${data.NAMA_PROPER}`,
              propertyId: data.ID_PROPER,
              lokasi: data.KAB_KOTA,
              lb: data.LUAS_BANGUNAN,
              lt: data.LUAS_TANAH,
              price: data.HARGA_MULAI,
              sukuBunga: data.sk_bga,
              sukuBungaSyariah: data.sk_bga_syariah,
              jumlahKamarTidur: data.KAMAR_TIDUR,
              jumlahKamarMandi: data.KAMAR_MANDI,
              pmt_konvensional: data.pmt_konvensional,
              pmt_syariah: data.pmt_syariah,
              jenisSubsidi: data.SUBSIDI
            });
            setPositionMap({
              lat: parseFloat(data.LATITUDE),
              lng: parseFloat(data.LONGITUDE),
            });
            const imgGalery = [];
            if (data.GBR1) {
              // console.log("+++ data.GBR1",data.GBR1);
              imgGalery.push(data.GBR1);
            }
            if (data.GBR2) {
              imgGalery.push(data.GBR2);
            }
            if (data.GBR3) {
              imgGalery.push(data.GBR3);
            }
            if (data.GBR4) {
              imgGalery.push(data.GBR4);
            }
            if (data.GBR5) {
              imgGalery.push(data.GBR5);
            }
            setImagesGalery(imgGalery);
            // FASILITAS
            const facilitiesObj = JSON.parse(data.FASILITAS);
            const facilitiesArr = Object.entries(
              facilitiesObj["Kelengkapan Rumah"]
            );
            // console.log("+++ facilitiesArr",facilitiesArr);
            const newFacilitiesArr = [];
            facilitiesArr.map((item) => {
              // console.log("+++ item",item);
              let iconPath = "M13 17L18 12L13 7";
              FACILITIES_PATH_ICONS.map((icon) => {
                if (icon.code === parseInt(item[0])) {
                  iconPath = icon.path;
                }
              });
              newFacilitiesArr.push({
                code: item[0],
                label: item[1],
                iconPath: iconPath,
              });
            });
            // console.log("+++ newFacilitiesArr",newFacilitiesArr);
            setFasilitas(newFacilitiesArr);

            doGetProperById(loadingHandler, data.ID_PROPER)
            .then(res=>{
              setDataProperti(res.data)
            })
            .catch((err) => {
              console.log("Error Get Data Properti", err);
            });
            // GET DEVELOPER CONTACT
            doGetDeveloperById(loadingHandler, data.ID_DEV)
              .then((response) => {
                const { data } = response;
                if (data) {
                  setContactInfo(data);
                }
              })
              .catch((err) => {
                console.log("Error Get Data Developer", err);
              });
            doGetKavling(loadingHandler, id)
              .then((response) => {
                const { data } = response;
                if (data) {
                  setKavlingList(data);
                }
              })
              .catch((err) => {
                console.log("Error Get Data Kavling", err);
              });
            doGetAreaSekitar(loadingHandler, data.ID_PROPER)
              .then((data) => {
                // console.log('~ data', data)
                if (data?.Status == 200) {
                  setAreaSekitar(data.data)
                } else {
                  throw data?.ErrToDev
                }
              }).catch((err) => {
                console.log('~ err', err)
              });
          }
        })
        .catch((err) => {
          console.log("Error Get Tipe Rumah", err);
        });
    }
  }, [id]);
  console.log("aaa", dataDetail);

  return (
    <Layout title="Informasi" isLoaderOpen={isLoading} mb_tipe_properti={143}>
      <Breadcrumb active={dataDetail.NAMA_PROPER}>
        <li
          className="breadcrumb-item"
          style={{ fontFamily: "Helvetica", fontWeight: 700 }}
        >
          <Link href="/property/tipe/">Cari Rumah</Link>
        </li>
      </Breadcrumb>
      <section className="mb-5" id="propertyPageTipeDetail">
        <div className="container">
          <div className="row">
            {isMobile && 
              <div className="col-md-4 col-lg-3 filter-container">
                <div style={{height: "320px"}}>
                  <AntdCarousel images={imagesGalery}/>
                </div>
              </div>
            }
            <div className="col-md-4 col-lg-3 filter-container">
              <div id="pencarianProp">
                <div className="card card_left_perumahan" id="informasiperumahan" style={{ wordBreak: "break-word" }}>
                  {/* <img src='icons/icons/icon_share.svg' /> */}
                  <h6>Informasi Perumahan</h6>
                  <h4>{dataProperti.NAMA_PROPER}</h4>
                  {/* <img src='icons/icons/icon_share.svg' /> */}
                  <div className="card_left_perumahan_lihat">
                    <Link href={{pathname: "/property/perumahan/detail", query: {id: `${dataProperti.NAMA_PROPER?.split(" ").join("-")}-${dataProperti.ID}`, page: 1}}}>Lihat Properti</Link>
                  </div>
                  <ul className="nav flex-column">
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        <MapPin /> {dataProperti.ALAMAT || "-"}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        <Phone /> {dataProperti.NO_TELP || "-"}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={dataProperti.EMAIL ? "mailto:" + dataProperti.EMAIL : "#"}>
                        <Envelope /> {dataProperti.EMAIL || "-"}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={dataProperti.WEBSITE ? "https://" + dataProperti.WEBSITE : "#"}>
                        <Web /> {dataProperti.WEBSITE || "-"}
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="card card_left_perumahan" id="perumahandeveloper">
                  <h6>Developer</h6>
                  <div className="d-flex" style={{alignItems: "center"}}>
                    <div className="company-logo">
                    <ReactImageFallback
                        src={contactInfo.LOGO ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + contactInfo.LOGO.replace("1|", "") : null}
                        fallbackImage="/images/thumb-placeholder.png"
                        className="img-fluid"
                        width="32px"
                        style={{ borderRadius: "50%",maxHeight:32,alignSelf:"center" }}
                      />
                    </div>
                    <h4>{contactInfo.NAMA || dataDetail.NAMA_DEV || "-"}</h4>
                  </div>
                  <div
                    className="card_left_perumahan_lihat"
                    style={{ marginLeft: 40, marginTop: 4 }}
                  >
                    <Link href={
                      {
                        pathname:"/developer/detail",
                        query: {
                          id: `${dataProperti.NAMA_DEV?.split(" ").join("-")}-${dataProperti.ID_DEV}`
                        }
                      }
                    }>
                      Lihat Detail
                    </Link>
                  </div>

                  <ul className="nav flex-column">
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        <MapPin />{" "}
                        {contactInfo.ALAMAT || dataDetail.ALAMAT || "-"}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#">
                        <Phone /> {contactInfo.NO_TELP || dataDetail.NO_TELP || "-"}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={contactInfo.EMAIL ? "mailto:" + contactInfo.EMAIL : "#"}>
                        <Envelope /> {contactInfo.EMAIL || "-"}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={contactInfo.WEBSITE || "#"}>
                        <Web /> {contactInfo.WEBSITE || "-"}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-8 col-lg-9 detail_unit" >
              {!isMobile && 
                <div>
                  <GaleryUnit images={imagesGalery} />
                </div>
              }
              <div id="PropertiInfo">
                <h3
                  className="title_info"
                  style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
                >
                  Informasi Properti
                </h3>
                <div
                  style={{ borderBottom: "1px solid rgba(238, 238, 238, 1)" }}
                  className="row mb-2"
                >
                  <div className="col-md-6">
                    <div className="row" style={{ marginRight: 24 }}>
                      <div className="col-6 mb-2">
                        <p
                          className="text"
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: 400,
                            color: "#666666",
                            marginRight: 24,
                            lineHeight: "150%",
                          }}
                        >
                          Cluster/Tower
                        </p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: 700,
                            color: "#00193E",
                            marginRight: 24,
                            lineHeight: "150%",
                          }}
                        >
                          {dataDetail.KLASTER == 'null' ? '-' : dataDetail.KLASTER }
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: 400,
                            color: "#666666",
                            lineHeight: "150%",
                          }}
                        >
                          Blok/Lantai
                        </p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: 700,
                            color: "#00193E",
                            lineHeight: "150%",
                          }}
                        >
                          {dataDetail.BLOK  == 'null' ? '-' : dataDetail.BLOK }  
                          {dataDetail.LANTAI  == 'null' ? '-' : dataDetail.JENIS == "1" || dataDetail.JENIS == "3" ? "" : " / " +dataDetail.LANTAI }
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: 400,
                            color: "#666666",
                            lineHeight: "150%",
                          }}
                        >
                          Alamat
                        </p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: 700,
                            color: "#00193E",
                            lineHeight: "150%",
                          }}
                        >
                          {dataDetail.ALAMAT?.replace (/\w\S*/g,(txt) => {  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); } ) }
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: 400,
                            color: "#666666",
                            lineHeight: "150%",
                          }}
                        >
                          Jenis Properti
                        </p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: 700,
                            color: "#00193E",
                            lineHeight: "150%",
                          }}
                        >
                          {idToLabel(dataDetail.JENIS)}
                           {/* {dataDetail.JENIS == "1"
                            ? "Rumah"
                            : dataDetail.JENIS == "2"
                            ? "Rumah Bekas"
                            : dataDetail.JENIS == "3"
                            ? "Apartemen"
                            : dataDetail.JENIS ? dataDetail.JENIS : "-"} */}
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: 400,
                            color: "#666666",
                            lineHeight: "150%",
                          }}
                        >
                          Daya Listrik
                        </p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: 700,
                            color: "#00193E",
                            lineHeight: "150%",
                          }}
                        >
                          {dataDetail.DAYA_LISTRIK} watt
                        </h6>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div
                      className="row"
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: 400,
                        color: "#666666",
                        marginRight: 24,
                        lineHeight: "150%",
                      }}
                    >
                      <div className="col-6 mb-2">
                        <p>Jalur PDAM</p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: "bold",
                            color: "#00193E",
                          }}
                        >
                          {dataDetail.JALUR_PDAM_LAMA ? dataDetail.JALUR_PDAM_LAMA?.replace (/\w\S*/g,(txt) => {  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); } ) : '-' }
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p>Jalur Telepon</p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: "bold",
                            color: "#00193E",
                          }}
                        >
                          {dataDetail.JALUR_TELEPON_LAMA ? dataDetail.JALUR_TELEPON_LAMA?.replace (/\w\S*/g,(txt) => {  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); } ) : '-' }
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p>Sertifikat Unit</p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: "bold",
                            color: "#00193E",
                          }}
                        >
                          {dataDetail.SERTIFIKAT}
                        </h6>
                      </div>
                      <div className="col-6 mb-2">
                        <p>Status Properti</p>
                      </div>
                      <div className="col-6 mb-2">
                        <h6
                          style={{
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: "bold",
                            color: "#00193E",
                          }}
                        >
                          {dataDetail.SUBSIDI == "0" ? "Non Subsidi" : "Subsidi"}
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>

                <h3
                  className="title_info"
                  style={{ fontFamily: "FuturaBT", fontWeight: 700 }}
                >
                  Fasilitas
                </h3>
                <div className="row list_fasilitas mb-3">
                  {fasilitas.map((item) => {
                    return (
                      <div className="col-md-6 item_fasilitas">
                        <div className="d-flex">
                          <svg
                            style={{ marginLeft: 8, width: 24, height: 24 }}
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d={item.iconPath} fill="#00193E" />
                          </svg>
                          <p style={{ fontFamily: "Helvetica", fontWeight: "bold" }}>
                            {item.label}
                          </p>
                        </div>
                      </div>
                    );
                  })}
                </div>

                <h3 className="title_info mb-3">Kavling</h3>
                {kavlingList == 0 ? (<p style={{
                  padding: "20px 20px",
                  border: "1px solid #e3e4e6",
                  borderRadius: "3px",
                  background: "#e3e4e6",
                  fontFamily: "Helvetica",
                  fontWeight: 700,
                  fontSize: "14px",
                  fontStyle: "normal"
                }}>Properti tidak ditemukan</p>) : ("")}
                <div className="row">
                  <div className="col-12 kavling-btn-container">
                    {kavlingList.sort((a, b) => (a.NO * 1) - (b.NO * 1)).map((item, index) => {
                      return (
                        <button
                          className={
                            selectedKav === index
                              ? "btn btn-main me-3 mb-3 active"
                              : "btn btn-main me-3 mb-3"
                          }
                          onClick={() => setSelectedKav(index)}
                        >
                          {item.NO}
                        </button>
                      );
                    })}
                  </div>
                  {kavlingList.length > 0 && (
                    <div className="col-11 kavling-image">
                      <ReactImageFallback
                        src={
                          `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                          kavlingList[selectedKav].GBR1?.replace("1|", "")
                        }
                        fallbackImage="/images/thumb-placeholder.png"
                        className="img-fluid"
                        style={{ height: 460, width: "100%", objectFit: "cover" }}
                      />
                    </div>
                  )}
                </div>

                <h3 className="title_info mt-3">Area Sekitar</h3>
                <div className="row" style={{ width: "100%", height: 280 }}>
                  <GoogleMaps position={positionMap} />
                </div>
                <div className="row">
                  <div className="row list_area">
                    {areaSekitar.map(val => (
                      <div className="col-md-6 item_area_sekitar">
                        <div className="d-flex align-items-center">
                          <div className="icon">
                            <svg
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M13.3333 2.66666H2.66667V3.99999H13.3333V2.66666ZM14 9.33332V7.99999L13.3333 4.66666H2.66667L2 7.99999V9.33332H2.66667V13.3333H9.33333V9.33332H12V13.3333H13.3333V9.33332H14ZM8 12H4V9.33332H8V12Z"
                                fill="white"
                              />
                            </svg>
                          </div>
                          <p className="mb-0">
                            {val.label ? val.label : `${val.jrk}m dari ${val.n}`}
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <OverflowKpr
          data={dataKpr}
          dataKavling={kavlingList}
          selectedKav={selectedKav}
          onChangeSelectedKav={(newIndex) => setSelectedKav(newIndex)}
          isLoading={isLoading}
          dataDetail={dataDetail}
        />
      </section>
    </Layout>
  );
}

const MapPin = () => {
  return (
    <div
      style={{
        width: 20,
        marginRight: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <svg
        width="11.67"
        height="16.67"
        viewBox="0 0 11.67 16.67"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M6.00033 0.666748C2.77533 0.666748 0.166992 3.27508 0.166992 6.50008C0.166992 10.8751 6.00033 17.3334 6.00033 17.3334C6.00033 17.3334 11.8337 10.8751 11.8337 6.50008C11.8337 3.27508 9.22533 0.666748 6.00033 0.666748ZM6.00033 8.58342C4.85033 8.58342 3.91699 7.65008 3.91699 6.50008C3.91699 5.35008 4.85033 4.41675 6.00033 4.41675C7.15033 4.41675 8.08366 5.35008 8.08366 6.50008C8.08366 7.65008 7.15033 8.58342 6.00033 8.58342Z"
          fill="#00193E"
        />
      </svg>
    </div>
  );
};

const Phone = () => {
  return (
    <div
      style={{
        width: 20,
        marginRight: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <svg
        width="15"
        height="15"
        viewBox="0 0 15 15"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M3.51667 6.99167C4.71667 9.35 6.65 11.275 9.00833 12.4833L10.8417 10.65C11.0667 10.425 11.4 10.35 11.6917 10.45C12.625 10.7583 13.6333 10.925 14.6667 10.925C15.125 10.925 15.5 11.3 15.5 11.7583V14.6667C15.5 15.125 15.125 15.5 14.6667 15.5C6.84167 15.5 0.5 9.15833 0.5 1.33333C0.5 0.875 0.875 0.5 1.33333 0.5H4.25C4.70833 0.5 5.08333 0.875 5.08333 1.33333C5.08333 2.375 5.25 3.375 5.55833 4.30833C5.65 4.6 5.58333 4.925 5.35 5.15833L3.51667 6.99167Z"
          fill="#00193E"
        />
      </svg>
    </div>
  );
};

const Envelope = () => {
  return (
    <div
      style={{
        width: 20,
        marginRight: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <svg
        width="16.67"
        height="13.33"
        viewBox="0 0 18 13.33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M15.667 0.333252H2.33366C1.41699 0.333252 0.675326 1.08325 0.675326 1.99992L0.666992 11.9999C0.666992 12.9166 1.41699 13.6666 2.33366 13.6666H15.667C16.5837 13.6666 17.3337 12.9166 17.3337 11.9999V1.99992C17.3337 1.08325 16.5837 0.333252 15.667 0.333252ZM15.667 3.66658L9.00033 7.83325L2.33366 3.66658V1.99992L9.00033 6.16658L15.667 1.99992V3.66658Z"
          fill="#00193E"
        />
      </svg>
    </div>
  );
};

const Web = () => {
  return (
    <div
      style={{
        width: 20,
        marginRight: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <svg
        width="16.67"
        height="16.67"
        viewBox="0 0 16.67 16.67"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M8.65899 0.666626C4.24299 0.666626 0.666992 4.25063 0.666992 8.66663C0.666992 13.0826 4.24299 16.6666 8.65899 16.6666C13.083 16.6666 16.667 13.0826 16.667 8.66663C16.667 4.25063 13.083 0.666626 8.65899 0.666626ZM14.203 5.46663H11.843C11.587 4.46663 11.219 3.50663 10.739 2.61863C12.211 3.12263 13.435 4.14663 14.203 5.46663ZM8.66699 2.29863C9.33099 3.25863 9.85099 4.32263 10.195 5.46663H7.13899C7.48299 4.32263 8.00299 3.25863 8.66699 2.29863ZM2.47499 10.2666C2.34699 9.75463 2.26699 9.21863 2.26699 8.66663C2.26699 8.11463 2.34699 7.57863 2.47499 7.06663H5.17899C5.11499 7.59463 5.06699 8.12263 5.06699 8.66663C5.06699 9.21063 5.11499 9.73863 5.17899 10.2666H2.47499ZM3.13099 11.8666H5.49099C5.74699 12.8666 6.11499 13.8266 6.59499 14.7146C5.12299 14.2106 3.89899 13.1946 3.13099 11.8666V11.8666ZM5.49099 5.46663H3.13099C3.89899 4.13863 5.12299 3.12263 6.59499 2.61863C6.11499 3.50663 5.74699 4.46663 5.49099 5.46663V5.46663ZM8.66699 15.0346C8.00299 14.0746 7.48299 13.0106 7.13899 11.8666H10.195C9.85099 13.0106 9.33099 14.0746 8.66699 15.0346ZM10.539 10.2666H6.79499C6.72299 9.73863 6.66699 9.21063 6.66699 8.66663C6.66699 8.12263 6.72299 7.58663 6.79499 7.06663H10.539C10.611 7.58663 10.667 8.12263 10.667 8.66663C10.667 9.21063 10.611 9.73863 10.539 10.2666ZM10.739 14.7146C11.219 13.8266 11.587 12.8666 11.843 11.8666H14.203C13.435 13.1866 12.211 14.2106 10.739 14.7146V14.7146ZM12.155 10.2666C12.219 9.73863 12.267 9.21063 12.267 8.66663C12.267 8.12263 12.219 7.59463 12.155 7.06663H14.859C14.987 7.57863 15.067 8.11463 15.067 8.66663C15.067 9.21863 14.987 9.75463 14.859 10.2666H12.155Z"
          fill="#00193E"
        />
      </svg>
    </div>
  );
};
