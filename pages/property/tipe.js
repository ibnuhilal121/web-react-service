import Layout from "../../components/Layout";
import Breadcrumb from "../../components/section/Breadcrumb";
import ItemUnit from "../../components/data/ItemUnit";
import WidgetKomparasi from "../../components/section/WidgetKomparasi";
import { useEffect, useState } from "react";
import axios from "axios";
import qs from "qs";
import { useIdToSlug, useSlugToId } from "../../helpers/pencarianProperti";
import LayoutPencarianProperti from "../../components/section/pencarianProperti/LayoutPencarianProperti";
import NotFound from "../../components/element/NotFound";
import { useRouter } from "next/router";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import GoogleMapClickable from "../../components/GoogleMapClickable";
import BarsLoader from "../../components/element/BarsLoader";
import ItemTipe from "../../components/data/ItemTipe";
import { async } from "@firebase/util";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function Unit() {
  const router = useRouter();
  const { query, asPath } = router;
  const { lokasi, tipe, budget, hargaMin, kamarTidur, kamarMandi, fasilitas, subsidi, sort, luasTanahMin, luasTanahMax, luasBangunanMin, luasBangunanMax, komparasi, virtual } = query;
  const slugToId = useSlugToId();
  const idToSlug = useIdToSlug();
  const [items, setItems] = useState([]);
  const [itemsCompare, setItemsCompare] = useState([]);
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [paginationLength, setPaginationLength] = useState(1);
  const [view, setView] = useState("grid");
  const [userLoc, setUserLoc] = useState(null);
  const [mapCenter, setMapCenter] = useState(null);

  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();

  const tipeProperti = query.tipeProperti?.length > 0 ? query.tipeProperti?.split(",") : null;
  const queryObj = {
    ...(lokasi && { lokasi }),
    ...(tipeProperti && { tipeProperti }),
    ...(sort && { sort }),
    ...(subsidi && { subsidi }),
    ...(budget && { hargaMax: budget * 1 }),
    ...(hargaMin && { hargaMin: hargaMin * 1 }),
    ...(kamarTidur && { kamarTidur: kamarTidur * 1 }),
    ...(kamarMandi && { kamarMandi: kamarMandi * 1 }),
    ...(luasTanahMin && { luasTanahMin: luasTanahMin * 1 }),
    ...(luasTanahMax && { luasTanahMax: luasTanahMax * 1 }),
    ...(luasBangunanMin && { luasBangunanMin: luasBangunanMin * 1 }),
    ...(luasBangunanMax && { luasBangunanMax: luasBangunanMax * 1 }),
    ...(fasilitas && { fasilitas: fasilitas.split(",") }),
    ...(virtual && { virtual })
  };

  useEffect(() => {
    let tipe
    if (query.tipeProperti == 1) {
      tipe = 2
    } else if (query.tipeProperti == 2) {
      tipe = 3
    }
    let newQueryObj = {
      page: query.currentPage || 1,
      halKe: query.currentPage || 1,
      // ...(lokasi && { kywd: lokasi }),
      // ...(tipe && { tipe }),
      ...(lokasi && { lokasi }),
      ...(tipeProperti && { tipeProperti: query.tipeProperti }),
      ...(sort && { sort }),
      ...(subsidi && { subsidi }),
      ...(budget && { hargaMax: budget * 1 }),
      ...(hargaMin && { hargaMin: hargaMin * 1 }),
      ...(kamarTidur && { kamarTidur: kamarTidur * 1 }),
      ...(kamarMandi && { kamarMandi: kamarMandi * 1 }),
      ...(luasTanahMin && { luasTanahMin: luasTanahMin * 1 }),
      ...(luasTanahMax && { luasTanahMax: luasTanahMax * 1 }),
      ...(luasBangunanMin && { luasBangunanMin: luasBangunanMin * 1 }),
      ...(luasBangunanMax && { luasBangunanMax: luasBangunanMax * 1 }),
      ...(virtual && { virtual })
    };

    const queryString = fasilitas ? qs.stringify(newQueryObj) + `&fasilitas=[${fasilitas}]` : qs.stringify(newQueryObj);
    // console.log('~ queryString', queryString)
    setMessage("");
    setIsLoading(true);
    axios({
      url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/searchTipe?${queryString}&Limit=15`,
      method: "GET",
      headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      // cancelToken: source.token,
    })
      .then((res) => {
        // console.log('~ res.data', res.data)
        // console.log('~ type', typeof res.data)
        if (!res.data.status) {
          throw res.data.ErrToDev;
        } else {
          console.log('~ data : ', res.data.data)
          setItems(res.data.data);
          setPaginationLength(res.data.pagging.jumlahHalTotal);
          setCurrentPage(newQueryObj.page * 1);
          setIsLoading(false);
        }
      })
      .catch((err) => {
        console.log("~ err", err);
        // console.log('~ err.response', err.response)
        setItems([]);
        setMessage(err.response?.data?.message);
        setIsLoading(false);
      });
    // console.log('~ lokasi, tipe, budget', lokasi, tipe, budget)
    // console.log('~ asPath', asPath)
    return () => source.cancel("useEffect states changed.");
  }, [asPath]);
  function handleSubmit(data) {
    // console.log('~ data', data)
    const { hargaMax, fasilitas, tipeProperti, ...restData } = data;
    const newQueryData = {
      budget: data.hargaMax,
      ...(tipeProperti?.length > 0 && {
        tipeProperti: tipeProperti.toString(),
      }),
      ...(fasilitas?.length > 0 && { fasilitas: fasilitas.toString() }),
      ...(komparasi && { komparasi: "1" }),
      tab: "tipe",
      ...restData,
    };
    router.push(`?${qs.stringify(newQueryData)}`, undefined, {scroll: false});
  }
  function onChangePage(n) {
    setCurrentPage(n);
    handleSubmit({
      ...queryObj,
      currentPage: n,
    });
  }

  function resultDataCompare(data) {
    const result = [...itemsCompare, data];
    //setItemsCompare((oldArray) => [...oldArray, data]);
    setItemsCompare(result);
    localStorage.setItem("itemCompare", JSON.stringify(result));
    console.log("INI DATA COMPARE", result)
  }
  const setCompare = async (e) => {
    const result = [...itemsCompare, e];
    //setItemsCompare((oldArray) => [...oldArray, data]);
    try{
    setItemsCompare(result);
    localStorage.setItem("itemCompare", JSON.stringify(result));
    console.log("INI DATA COMPARE", result)
    }catch(err){

    }finally{
        console.log("DATA QUERY",e.ID)
        router.push({
          pathname: "/tools/komparasi",
          query: { data: [e.ID] },
        });
    }       
   
  };
  function deleteHandlerCompare(index) {
    const result = itemsCompare.filter((item) => item.ID !== index.ID);
    //setItemsCompare(itemsCompare.filter((item) => item.id !== index.id));
    setItemsCompare(result);
    localStorage.setItem("itemCompare", JSON.stringify(result));
  }

  function handleFoundCoords(coords) {
    console.log("~ coords", coords);
    setUserLoc(coords);
    setMapCenter(coords);
  }

  useEffect(() => {
    if (localStorage.getItem("itemCompare") && localStorage.getItem("itemCompare") !== "null") {
      const objek = localStorage.getItem("itemCompare");
      const myArray = JSON.parse(objek);
      let dataCompare = Object.keys(myArray).map((key) => myArray[key]);
      setItemsCompare(dataCompare);
    }

    if (komparasi != "1") {
      localStorage.removeItem("itemCompare");
      setItemsCompare([]);
    }
  }, []);

  useEffect(() => {
    console.log("data array", itemsCompare);
  }, [itemsCompare]);

  return (
    <Layout title="Daftar Properti | BTN Properti" mb_tipe_properti={208}>
      <Breadcrumb active="Cari Rumah" />
      <section className="mb-5" id="propertyPageTipe">
        <div className="container">
          <LayoutPencarianProperti
            queryObj={queryObj}
            handleSubmit={handleSubmit}
            paginationLength={paginationLength}
            currentPage={currentPage}
            onChangePage={onChangePage}
            tab={"tipe"}
            noResult={view === "grid" ? !items?.length > 0 : true}
            view={view}
            setView={setView}
            onFoundCoords={handleFoundCoords}
          >
            {isLoading ? (
              <BarsLoader />
            ) : items?.length > 0 ? (
              view === "grid" ? (
                <div id="tipe-cards">
                <div className="row w-100 d-flex justify-content-start justify-content-md-around align-content justify-content-lg-start">
                  {items.map((data, index) => (
                    <div key={index} className="col-6 col-md-6 col-lg-4 mb-4 card-item">
                      <ItemTipe data={data} compare={itemsCompare} 
                      dataCompare={resultDataCompare} 
                      action="false" detail="true" banding="true" 
                      deleteCompare={deleteHandlerCompare}
                      setCompare={setCompare} />
                    </div>
                  ))}
                </div>
                </div>
              ) : (
                <div className="row" style={{ width: "100%", height: 650 }}>
                  <GoogleMapClickable
                    markers={items.map((val) => ({
                      lat: val.LATITUDE * 1,
                      lng: val.LONGITUDE * 1,
                      id: val.ID,
                      priceString: val.HARGA_MULAI,
                      card: (
                        <div className="card-item">
                          <ItemTipe data={val} compare={itemsCompare} dataCompare={resultDataCompare} detail="true" deleteCompare={deleteHandlerCompare} />
                        </div>
                      ),
                    }))}
                    mapCenter={mapCenter}
                    userLoc={userLoc}
                  />
                </div>
              )
            ) : (
              <NotFound />
            )}
          </LayoutPencarianProperti>
        </div>
      </section>
      {komparasi == "1" && <WidgetKomparasi deleteCompare={deleteHandlerCompare} data={itemsCompare} />}
    </Layout>
  );
}
