import { useRouter } from "next/router";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import ShareSosmed from "../../../components/static/ShareSosmed";
import Link from "next/link";
import GaleryUnit from "../../../components/section/GaleryUnit";

import styles from "../../../styles/property_unit.module.scss";
import { getReviewProperti } from "../../../services/property";
import { useEffect, useState } from "react";
import parse from 'html-react-parser';
import SearchIcon from '../../../components/element/icons/SearchIcon';

export default function index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const router = useRouter();
  const { id, name } = router.query;
  const [detail, setDetail] = useState(null);
  const [loaded, setLoaded] = useState(false);
  const [images, setImages] = useState([]);

  useEffect(() => {
    if (id) {
      getReviewProperti(id)
        .then(res => {
          if (res.status) {
            const objData = res.data;
            let tempImages = [];
            for (let data in objData) {
              if (data.includes('GBR')) {
                tempImages.push(objData[data]);
              };
            };
            setImages(tempImages);
            setDetail(res.data);
          } else {
            throw res;
          };
        })
        .catch(error => { })
        .finally(() => {
          setLoaded(true);
        });
    }
  }, [id]);

  const NotFound = () => {
    return (
      <div
        style={{
          paddingTop: 100,
          textAlign: "center",
        }}
      >
        <SearchIcon />
        <div
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "16px",
            lineHeight: "150%",
            marginTop: 40,
          }}
        >
          Belum ada review
        </div>
      </div>
    );
  };

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  return (
    <Layout title="Informasi ">
      <Breadcrumb 
        active={
          <Link href={"/property/perumahan/detail"}>{detail?.NAMA || name}</Link>
        }
      >
        <li className="breadcrumb-item">
          <Link href="/property/">Cari Rumah</Link>
        </li>
      </Breadcrumb>
      <section className="mb-5">
        <div className="container">
          {detail ?
            (
              <div className="row">
                <div className="col-md-4 col-lg-3">
                  <div className="card card_review_left sticky-top">
                    <div className="card-body">
                      <h5
                        style={{
                          fontFamily: "FuturaBT",
                          fontWeight: 700
                        }} >Review Properti</h5>

                      <nav className="nav nav-pills flex-column" id="review_property" >
                        <a className="nav-link" href="#Perkenalan" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Perkenalan</a>
                        <a className="nav-link" href="#Analisis" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Analisis</a>
                        <a className="nav-link" href="#DetailProperti" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Detail Properti</a>
                        <a className="nav-link" href="#Lokasi" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Lokasi</a>
                        <a className="nav-link" href="#Ringkasan" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Ringkasan</a>
                        <a className="nav-link" href="#Hubungi" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Hubungi</a>

                      </nav>

                    </div>
                  </div>
                </div>
                <div className="col-md-8 col-lg-9 ">
                  <GaleryUnit images={images} />
                  <div className="review_content" data-bs-spy="scroll"
                  >
                    <h6 style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Review Properti</h6>
                    <div className="d-flex justify-content-between align-items-center">
                      <h4 className="title_perumahan" style={{ fontFamily: "FuturaBT", fontWeight: 700 }}>{detail.NAMA}</h4>
                      <Link href={"/property/perumahan/detail"}>
                        <a className="link_detail" style={{ fontFamily: "Helvetica", fontWeight: 700 }}>Lihat Proyek Perumahan</a>
                      </Link>
                    </div>
                    <div className="desc">
                      <p>{detail.DESKRIPSI ? parse(detail.DESKRIPSI) : detail.DESKRIPSI}</p>
                    </div>
                    <div className="title" id="Perkenalan">A. Perkenalan</div>
                    <div className="desc">
                      {detail.PERKENALAN ? parse(detail.PERKENALAN) : detail.PERKENALAN}
                    </div>
                    <div className="title" id="Analisis">B. Analisis</div>
                    <div className="desc">
                      {detail.ANALISIS ? parse(detail.ANALISIS) : detail.ANALISIS}
                    </div>
                    <div className="title" id="DetailProperti">C. Detail Properti</div>
                    <div className="desc">
                      {detail.DETAIL_PROPERTI ? parse(detail.DETAIL_PROPERTI) : detail.DETAIL_PROPERTI}
                    </div>
                    <div className="title" id="Lokasi">D. Lokasi</div>
                    <div className="desc">
                      {detail.LOKASI ? parse(detail.LOKASI) : detail.LOKASI}
                    </div>
                    <div className="title" id="Ringkasan">E. Ringkasan</div>
                    <div className="desc">
                      {detail.RINGKASAN ? parse(detail.RINGKASAN) : detail.RINGKASAN}
                    </div>
                    <div className="title" id="Hubungi">F. Hubungi</div>
                    <div className="desc">
                      <p>{detail.HUBUNGI ? parse(detail.HUBUNGI) : detail.HUBUNGI}</p>
                    </div>
                  </div>
                </div>
              </div>
            ) :
            (
              NotFound()
            )}
        </div>
      </section>
    </Layout>
  );
}
