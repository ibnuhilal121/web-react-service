/* eslint-disable react-hooks/rules-of-hooks */
import { useRouter } from "next/router";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/section/Breadcrumb";
import ShareSosmed from "../../../components/static/ShareSosmed";
import Link from "next/link";
import GaleryUnit from "../../../components/section/GaleryUnit";
import AntdCarousel from "../../../components/carousel/AntdCarousel"
import ItemUnit from "../../../components/data/ItemUnit";
import data_all from "../../../sample_data/data_property_developer";
import { useEffect, useRef, useState } from "react";
import axios from "axios";
import NumberFormat from "react-number-format";
import { useIdToCT, useIdToLabel } from "../../../helpers/pencarianProperti";
import CopyToClipboard from "react-copy-to-clipboard";
import ReactImageFallback from "react-image-fallback";
import Overlay from "react-bootstrap/Overlay";
import Toast from "react-bootstrap/Toast";
import GoogleMaps from "../../../components/GoogleMaps";
import { useMediaQuery } from "react-responsive";
import { doGetProperById } from "../../../services/property";
import PaginationNew from "../../../components/data/PaginationNew";
import { defaultHeaders } from "../../../utils/defaultHeaders";
// import {}

export default function index() {
  const router = useRouter();
  // const { id } = router.query;
  const query = router.query.id;
  const {asPath} = router
  const [content, setContent] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [tipeItems, setTipeItems] = useState([]);
  const [page, setPage] = useState(router.query.page || 1)
  const [pagging, setPagging] = useState({
    jumlahHalTotal: "1"
  })
  const idToLabel = useIdToLabel();
  const idToCt = useIdToCT();
  const isTabletOrMobile = useMediaQuery({ query: "(min-width: 768px) and (max-width: 1024px)" });
  const isResponsive = useMediaQuery({ query: `(max-width: 1024px)` });
  const isMobile = useMediaQuery({ query: `(max-width: 576px)` });
  let id = "";
  if (query) {
    let path = query.split("-");
    id = path[path.length - 1];
  }

  console.log("ini data", tipeItems);

  function onChangePage(n) {
    setPage(n);
    router.push(asPath.split('&')[0] +`&page=${n}`)
  }

  // share button
  const [show, setShow] = useState(false);
  const target = useRef(null);
  // end share button

  const [copyUrl, setCopyUrl] = useState(false);
  let url = "";
  if (typeof window !== "undefined") {
    url = window.location.href;
  }

  function getTipeRumah() {
    return axios({
      url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getTipeRumah?proper_id=${id}&halKe=${page}`,
      method: "GET",
      headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
    });
  }
  
  useEffect(() => {
    if (id) {
      setIsLoading(true);
      const promises = [doGetProperById(()=>{},id), getTipeRumah()];
      Promise.all(promises)
        .then((res) => {
          const [resProperti, resTipe] = res;
          console.log("~ res", res);
          setIsLoading(false);
          if (resProperti.status && resTipe.data.status) {
            setContent(resProperti.data);
            setPagging(resTipe.data.data.pagging)
            const newDataTipe = resTipe.data.data.data || resTipe.data.data;
            setTipeItems(
              newDataTipe.map((val) => ({
                ...val,
                id: val.ID,
                gbr1: val.GBR1,
                n_kot: val.KAB_KOTA,
                jns: val.JENIS,
                clstr: val.KLASTER,
                n: val.NAMA_PROPER,
                hrg_rdh: val.HARGA_MULAI * 1,
                i_dev: val.ID_DEV,
                n_dev: val.NAMA_DEV,
                km_tdr: val.KAMAR_TIDUR,
                km_mnd: val.KAMAR_MANDI,
                ls_bgn: val.LUAS_BANGUNAN,
                ls_tnh: val.LUAS_TANAH,
              }))
            );
            console.log('~ resProperti.data.data', resProperti.data.data)
          } else {
            throw resProperti.message;
          }
        })
        .catch((err) => {
          setIsLoading(false);
          console.log("~ err", err);
          console.log(err);
        });
    }
  }, [id, page]);

  const [positionMap, setPositionMap] = useState({
    lat: -7.7956,
    lng: 110.3695,
  });

  let sekitar = [];
  try {
    sekitar = JSON.parse(content.FASILITAS_SEKITAR);
  } catch (error) {
    // console.log('~ error', error)
  }
  const setCompare = async (e) => {
    try{
      localStorage.setItem("itemCompare", JSON.stringify([e]));
    }catch(err){

    }finally{
        console.log("DATA QUERY",e.ID)
        router.push({
          pathname: "/tools/komparasi",
          query: { data: [e.ID] },
        });
    }       
  }

  console.log("ini daerah --->", content);
  return (
    <Layout title="Daftar Properti | BTN Properti" isLoaderOpen={isLoading}>
      <Breadcrumb active={content.NAMA_PROPER}>
        <li className="breadcrumb-item">
          <Link href="/property">Cari Rumah</Link>
        </li>
      </Breadcrumb>
      <section className="mb-5" id="propertyPagePerumahan" onClick={() => show && setShow(false)}>
        <div className="container">
          <div className="row">
          {isMobile &&
            <div className="col-md-4 col-lg-3">
              <div style={{height: "310px"}}>
                <AntdCarousel images={[content.GBR1, content.GBR2, content.GBR3, content.GBR4, content.GBR5]}/>
              </div>
            </div>
          }
            <div className="col-md-4 col-lg-3">
              <div className="sticky-top">
                <div className="card card_left_perumahan" style={{ wordBreak: "break-word" }}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      fontFamily: "FuturaBT",
                      fontWeight: 700,
                    }}
                  >
                    <h4>{content.NAMA_PROPER}</h4>
                    <div style={{ position: "relative" }}>
                      <button className="btn" style={{ width: 25, height: 25, padding: 0 }} onClick={() => setShow(!show)}>
                        <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M16.5 14.7399C15.8033 14.7399 15.18 15.0149 14.7033 15.4458L8.1675 11.6416C8.21333 11.4308 8.25 11.2199 8.25 10.9999C8.25 10.7799 8.21333 10.5691 8.1675 10.3583L14.63 6.59075C15.125 7.04909 15.7758 7.33325 16.5 7.33325C18.0217 7.33325 19.25 6.10492 19.25 4.58325C19.25 3.06159 18.0217 1.83325 16.5 1.83325C14.9783 1.83325 13.75 3.06159 13.75 4.58325C13.75 4.80325 13.7867 5.01409 13.8325 5.22492L7.37 8.99242C6.875 8.53409 6.22417 8.24992 5.5 8.24992C3.97833 8.24992 2.75 9.47825 2.75 10.9999C2.75 12.5216 3.97833 13.7499 5.5 13.7499C6.22417 13.7499 6.875 13.4658 7.37 13.0074L13.8967 16.8208C13.8508 17.0133 13.8233 17.2149 13.8233 17.4166C13.8233 18.8924 15.0242 20.0933 16.5 20.0933C17.9758 20.0933 19.1767 18.8924 19.1767 17.4166C19.1767 15.9408 17.9758 14.7399 16.5 14.7399Z"
                            fill="#0061A7"
                          />
                        </svg>
                      </button>
                      {show && (
                        <div className="socmed-container"
                          style={{
                            padding: "5px 10px",
                            position: "absolute",
                            top: "38px",
                            left: -50,
                            boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                            backgroundColor: "#fff",
                            borderRadius: 8,
                          }}
                        >
                          <ul className="nav" style={{ display: "flex", flexWrap: "nowrap" }}>
                            <li className="nav-item">
                              <a className="nav-link" href={`https://www.facebook.com/share.php?u=${document.URL}`} target="_blank" rel="noreferrer" style={{ padding: 0 }}>
                                <img src="/images/icons/sosmed/facebook.png" alt="" style={{ width: 28 }} />
                              </a>
                            </li>
                            <li className="nav-item">
                              <a className="nav-link" href={`https://twitter.com/intent/tweet?text=Informasi ${content.NAMA_PROPER}&url=${document.URL}`} target="_blank" rel="noreferrer" style={{ padding: 0 }}>
                                <img src="/images/icons/sosmed/twitter.png" alt="" style={{ width: 28 }} />
                              </a>
                            </li>
                            {/* <li className="nav-item">
                              <a
                                className="nav-link"
                                href="https://www.instagram.com/bankbtn/"
                                target="_blank"
                                rel="noreferrer"
                                style={{ padding: 0}}
                              >
                                <img src="/images/icons/sosmed/line.png" alt="" style={{width: 28}} />
                              </a>
                            </li> */}
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                href={`mailto:?subject=Informasi ${content.NAMA_PROPER}&body=${document.URL}`}
                                target="_blank"
                                rel="noreferrer"
                                style={{
                                  padding: 0,
                                  width: 28,
                                  height: 28,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                  backgroundColor: "salmon",
                                  fontSize: 16,
                                  borderRadius: 4,
                                  marginRight: "12.5px",
                                }}
                              >
                                <i className="bi bi-envelope-open" style={{ color: "white" }} />
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                // href="https://www.youtube.com/channel/UCWpG0IG6UaUfBy4FWlJfOJA"
                                target="_blank"
                                rel="noreferrer"
                                style={{
                                  padding: 0,
                                  width: 28,
                                  height: 28,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                  backgroundColor: "#F8C146",
                                  fontSize: 20,
                                  borderRadius: 4,
                                }}
                              >
                                <CopyToClipboard
                                  onCopy={() => {
                                    setCopyUrl(true);
                                    setTimeout(() => {
                                      setCopyUrl(false);
                                    }, 2000);
                                  }}
                                  text={url}
                                  style={{ cursor: "pointer", color: "white" }}
                                >
                                  <i className="bi bi-link" />
                                </CopyToClipboard>
                              </a>
                            </li>
                          </ul>
                        </div>
                      )}
                    </div>
                  </div>

                  <Link href={
          {
            pathname:"/developer/detail",
            query: {
              id: `${content?.NAMA_DEV?.split(" ").join("-")}-${content?.ID_DEV}`
            }
          }
        }>
                    <a>
                    <div className="d-flex p-list-detail-dev" style={{alignItems:"center"}} >
                      {/* <img
                        src={content.LOGO}
                      /> */}
                      <ReactImageFallback
                        src={content.LOGO ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + content.LOGO.replace("1|", "") : null}
                        fallbackImage="/images/thumb-placeholder.png"
                        className="img-fluid"
                        width="32px"
                        style={{ borderRadius: "50%",maxHeight:32,alignSelf:"center" }}
                      />
                      <div className="ms-1">
                        <h4
                          style={{
                            fontFamily: "Helvetica",
                            fontWeight: 400,
                            color: "#0061A7",
                            marginTop: 8,
                            fontStyle: "normal",
                            fontWeight: "normal",
                            fontSize: 12,
                          }}
                        >
                          {content.NAMA_DEV}
                        </h4>
                      </div>
                    </div>
                    </a>
                  </Link>
                  <div className="devider row mt-0"></div>
                  <table className="table table-borderless tabel_info hideScrollbar">
                    <tbody style={{ overflow: "hidden" }}>
                      <tr>
                        <td className="info" style={{ paddingRight: "0px", width: isTabletOrMobile ? "80px" : "auto" }}>
                          Harga Minimum
                        </td>
                        <td className="price" style={{ paddingLeft: "0px" }}>
                          <NumberFormat value={content.HARGA_MULAI != null ? content.HARGA_MULAI * 1 : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
                        </td>
                      </tr>
                      <tr>
                        <td className="info" style={{ paddingRight: "0px", width: isTabletOrMobile ? "80px" : "auto" }}>
                          Harga Maksimum
                        </td>
                        <td className="price" style={{ paddingLeft: "0px" }}>
                          <NumberFormat value={content.HARGA_SAMPAI != null ? content.HARGA_SAMPAI * 1 : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
                        </td>
                      </tr>
                      <tr>
                        <td className="info" style={{ paddingRight: "0px", width: isTabletOrMobile ? "80px" : "auto" }}>
                          KPR mulai dari
                        </td>
                        <td className="price" style={{ paddingLeft: "0px" }}>
                          <NumberFormat value={content.pmt_konvensional != null ? content.pmt_konvensional * 1 : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
                          /bln
                        </td>
                      </tr>
                      <tr>
                        <td className="info" style={{ width: isTabletOrMobile ? "80px" : "auto" }}>
                          Suku Bunga dari
                        </td>
                        <td className="price" style={{ paddingLeft: "0px" }}>
                          {content.sk_bga}%
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div className="devider row mt-0"></div>
                  <h5 className="title_info">Kontak Perumahan</h5>
                  <ul className="nav flex-column">
                    <li className="nav-item">
                      <a className="nav-link d-flex">
                        <img
                          src="/icons/icons/phone.svg"
                          style={{
                            width: 15,
                            height: 15,
                            marginTop: "5px",
                          }}
                        />
                        {content.NO_TELP}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        style={{
                          width: "90%",
                          whiteSpace: "nowrap",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                        }}
                        className="nav-link"
                        href={"mailto:" + content.EMAIL}
                      >
                        <img src="/icons/icons/email.svg" style={{ width: "16.67px", height: "13.33px" }} />

                        {content.EMAIL}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={"http://" + content.WEBSITE}>
                        <img
                          src="/icons/icons/site.svg"
                          style={{
                            width: "16.67px",
                            height: "16.67px",
                            filter: "invert(11%) sepia(30%) saturate(3416%) hue-rotate(194deg) brightness(87%) contrast(108%)",
                          }}
                        />
                        {content.WEBSITE || "-"}
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="card card-body card-review">
                  <div className={`d-flex align-content-start ${isMobile ? 'justify-content-start' : 'justify-content-between'}`}>
                    {isResponsive ? (
                      <div className="review-icon" style={{ paddingTop: "3px" }}>
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="20" cy="20" r="20" fill="#0061A7" />
                          <g clip-path="url(#clip0_1671_8508)">
                            <g filter="url(#filter0_d_1671_8508)">
                              <path
                                d="M31 20L28.56 17.22L28.9 13.54L25.29 12.72L23.4 9.54004L20 11L16.6 9.54004L14.71 12.72L11.1 13.53L11.44 17.21L9 20L11.44 22.78L11.1 26.47L14.71 27.29L16.6 30.47L20 29L23.4 30.46L25.29 27.28L28.9 26.46L28.56 22.78L31 20Z"
                                fill="#FFB800"
                              />
                            </g>
                            <path d="M16.6 9.54004L20 11V29L16.6 30.47L14.71 27.29L11.1 26.47L11.44 22.78L9 20L11.44 17.21L11.1 13.53L14.71 12.72L16.6 9.54004Z" fill="#FFD15A" />
                            <path d="M18.0001 22.7799L15.2201 19.9999L14.2734 20.9399L18.0001 24.6666L26.0001 16.6666L25.0601 15.7266L18.0001 22.7799Z" fill="white" />
                          </g>
                          <defs>
                            <filter id="filter0_d_1671_8508" x="7" y="8.54004" width="26" height="24.9299" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                              <feFlood flood-opacity="0" result="BackgroundImageFix" />
                              <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                              <feOffset dy="1" />
                              <feGaussianBlur stdDeviation="1" />
                              <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.0974463 0 0 0 0 0.241667 0 0 0 0.11 0" />
                              <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1671_8508" />
                              <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1671_8508" result="shape" />
                            </filter>
                            <clipPath id="clip0_1671_8508">
                              <rect width="24" height="24" fill="white" transform="translate(8 8)" />
                            </clipPath>
                          </defs>
                        </svg>
                      </div>
                    ) : (
                      <div className="review-icon" style={{ paddingTop: "3px" }}>
                        <img src="/images/icons/review.png" className="img-fluid" />
                      </div>
                    )}
                    <div className="content">
                      <h5>Review Properti</h5>
                      <p>Proyek perumahan ini sudah mendapat jaminan review dari user.</p>
                      <Link href={{
                        pathname: `/property/review/detail/`, 
                        query: {
                          id: content.ID, 
                          name: content.NAMA_PROPER
                      }}}>
                        <a className="lihat-review">
                          <span style={{ color: "#0061A7" }}>Lihat Review</span>
                          <img src="/icons/icons/chevron-right.svg" />
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-8 col-lg-9 detail_unit">
              {!isMobile && <GaleryUnit images={[content.GBR1, content.GBR2, content.GBR3, content.GBR4, content.GBR5]} /> }
              <h3 className="title_info" style={{ fontFamily: "FuturaBT", fontWeight: 700 }}>
                Informasi Properti
              </h3>
              <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                  <button className="nav-link active" id="Informasi-tab" data-bs-toggle="tab" data-bs-target="#Informasi" type="button" role="tab" aria-controls="Informasi" aria-selected="true">
                    Informasi
                  </button>
                </li>
                <li className="nav-item" role="presentation">
                  <button className="nav-link" id="Deskripsi-tab" data-bs-toggle="tab" data-bs-target="#Deskripsi" type="button" role="tab" aria-controls="Deskripsi" aria-selected="false">
                    Deskripsi
                  </button>
                </li>
              </ul>

              <div className="tab-content">
                <div className="tab-pane active" id="Informasi" role="tabpanel" aria-labelledby="Informasi-tab">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="row">
                        <div className="col-4 mb-3">
                          <p className="txt">Cluster/Tower</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6 className="dark" style={{marginLeft: isResponsive ? "60px" : ""}}>{idToCt(content.JENIS)}</h6>
                        </div>
                        <div className="col-4 mb-3">
                          <p className="txt">Jenis Properti</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6 className="dark" style={{marginLeft: isResponsive ? "60px" : ""}}>{idToLabel(content.JENIS)}</h6>
                        </div>
                        <div className="col-4 mb-3">
                          <p className="txt">Alamat</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6 className="dark" style={{marginLeft: isResponsive ? "60px" : ""}}>{content.ALAMAT}</h6>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="row">
                        <div className="col-4 mb-3">
                          <p className="txt">Jalur PDAM</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6 className="dark" style={{marginLeft: isResponsive ? "60px" : ""}}>
                            {content.FASILITAS?.includes('"6"') ? "" : "Tidak "}
                            Ada
                          </h6>
                        </div>
                        <div className="col-4 mb-3">
                          <p className="txt">Jalur Telepon</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6 className="dark" style={{marginLeft: isResponsive ? "60px" : ""}}>
                            {content.FASILITAS?.includes('"4"') ? "" : "Tidak "}
                            Ada
                          </h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane"
                  id="Deskripsi"
                  role="tabpanel"
                  aria-labelledby="Deskripsi-tab"
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 14,
                    fontStyle: "normal",
                    fontWeight: 400,
                    lineHeight: "24px",
                  }}
                >
                  <div className="deskripsi">
                    <div style={{ fontFamily: "Helvetica", fontWeight: 400 }}>{content.DESKRIPSI}</div>
                  </div>
                </div>
              </div>
              <h3
                className="title_info"
                style={{
                  borderTop: "1px solid #eeeeee",
                  paddingTop: 32,
                  marginTop: 16,
                }}
              >
                Tipe Unit
              </h3>
              {tipeItems == 0 ? (
                <p
                  style={{
                    padding: "20px 20px",
                    border: "1px solid #e3e4e6",
                    borderRadius: "3px",
                    background: "#e3e4e6",
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    fontSize: "14px",
                    fontStyle: "normal",
                  }}
                >
                  Perumahan tidak ditemukan
                </p>
              ) : (
                ""
              )}
                <section id="detailTipePropertiPerumahan">
              <div className="row">
                {tipeItems.map((data, index) => (
                  <div key={index} className="col-md-4 col-6 mb-4">
                    <ItemUnit data={data} action="false" detail="true" banding="true" setCompare={() => setCompare(data)} />
                  </div>
                ))}
                {console.log("ini data itemUnit:", tipeItems)}
              </div>
              <PaginationNew
                current={Number(pagging.halKe)}
                length={Number(pagging.jumlahHalTotal)}
                onChangePage={onChangePage}
              />
              </section>

              {!content.embed360 || content.embed360 === "undefined" ? (
                ""
              ) : (
                <>
                  <h3
                    className="title_info"
                    style={{
                      borderTop: "1px solid #eeeeee",
                      paddingTop: 32,
                      marginTop: 8,
                    }}
                  >
                    Virtual Tur
                  </h3>
                  <iframe
                    src={content.embed360}
                    frameborder="0"
                    style={{
                      width: "100%",
                      height: "470px",
                      borderRadius: "8px",
                    }}
                  ></iframe>
                </>
              )}

              <h3
                className="title_info"
                style={{
                  borderTop: "1px solid #eeeeee",
                  paddingTop: 34,
                  marginTop: 35,
                }}
              >
                Area Sekitar
              </h3>
              {content.LATITUDE === null && content.LONGITUDE === null ? <p style={{ fontSize: 14, }}>Area sekitar tidak tersedia</p> : 
              <div className="row" style={{ width: "100%", height: 280 }}>
                <GoogleMaps
                  position={
                    content
                      ? {
                        lat: content.LATITUDE * 1,
                        lng: content.LONGITUDE * 1,
                      }
                      : null
                  }
                />
              </div>}
              <div className="row">
                <div className="row list_area">
                  {sekitar?.map((val) => (
                    <div className="col-md-6 item_area_sekitar">
                      <div className="d-flex align-items-center">
                        <div className="icon">
                          <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                              d="M13.3333 2.66666H2.66667V3.99999H13.3333V2.66666ZM14 9.33332V7.99999L13.3333 4.66666H2.66667L2 7.99999V9.33332H2.66667V13.3333H9.33333V9.33332H12V13.3333H13.3333V9.33332H14ZM8 12H4V9.33332H8V12Z"
                              fill="white"
                            />
                          </svg>
                        </div>
                        <p className="mb-0">{val}</p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div
        style={{
          position: "fixed",
          top: 100,
          right: 20,
          zIndex: "99999999999",
        }}
      >
        <Toast onClose={() => setCopyUrl(null)} show={copyUrl} delay={3000} autohide bg="success">
          <Toast.Body>
            <h5 style={{ color: "white", fontFamily: "Helvetica", fontSize: 18 }}>Link telah disalin</h5>
          </Toast.Body>
        </Toast>
      </div>
    </Layout>
  );
}
