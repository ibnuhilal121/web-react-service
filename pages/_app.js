/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/exhaustive-deps */
import { Provider } from "next-auth/client";
import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.scss";
import "../styles/property.scss";
import "../styles/SejarahSlide.scss";
import initApp, { getSettingShow } from "../utils/initApp";
import { useEffect, useState } from "react";
import { AppWrapper } from "../context";
import { useRouter } from "next/router";
import TagManager from "react-gtm-module";

import * as ga from '../lib/ga'

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  // start CHECK ACCESS KEY & SETTINGS VARIABEL
  const [isInitApp, setIsInitApp] = useState(false);
  let accessKey;
  let settings;
  if (typeof window !== "undefined") {
    accessKey = localStorage.getItem("accessKey");
    settings = sessionStorage.getItem("settings");
  }

  useEffect(() => {
    TagManager.initialize({ gtmId: 'GTM-TSMQ3WV' });
  }, []);
  
  useEffect(() => {
    setIsInitApp(true);
    const handleRouteChange = (url) => {
      ga.pageview(url)
    }
    const doInitApp = async () => {
      await initApp();
      if (!settings) {
        await getSettingShow();
      }
      setIsInitApp(false);
    };
    doInitApp();

    router.events.on("routeChangeStart", doInitApp);
    router.events.on("routeChangeComplete", () => {
      handleRouteChange
      setTimeout(() => {
        // window.scroll({
        //   top: 1,
        //   // left: 1,
        //   behavior: 'smooth'
        // })
      }, 1000)
    });
  }, [router.events]);
  // end CHECK ACCESS KEY & SETTINGS VARIABEL
  return (
    <>
      <Provider session={pageProps.session}>
        <AppWrapper>
          {isInitApp ? (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh",
              }}
            >
              {/* <div className="spinner-border text-primary" style={{marginLeft: 10}} role="status" aria-hidden="true"/> */}
              <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
            </div>
          ) : (
            <Component {...pageProps} />
          )}
        </AppWrapper>
      </Provider>
    </>
  );
}

export default MyApp;
