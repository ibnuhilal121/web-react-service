module.exports = {
  trailingSlash: true,
  reactStrictMode: true,
  staticPageGenerationTimeout: 1000,
  // distDir: 'build',
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  images: {
    loader: 'imgix',
    path: '',
    domains: ['tech-ok.online']
  },
  webpack5: true,
  webpack: (config) => {
    config.resolve.fallback = { fs: false, path: false,crypto:false,constants:false };
    return config;
  },
  env: {
    NEXT_PUBLIC_API_HOST_OTHER_PENGAJUAN: process.env.NEXT_PUBLIC_API_HOST_OTHER_PENGAJUAN
  }
}
