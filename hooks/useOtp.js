import { useEffect, useState } from "react"
import axios from "axios"
const url = process.env.NEXT_PUBLIC_API_HOST
const payload = {}
const body = {
    method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        AccessKey_App: localStorage.getItem("accessKey"),
      },
      body: new URLSearchParams(payload)
}

const useOtp = (tag) => {
    const [dataOtp, setDataOtp] = useState(null)
    const fetchData = async tag => {
        const {data} = await axios(url, body)
        setDataOtp(data)
    }
    useEffect(() => {
        fetchData(tag)
    }, [tag])

    return {dataOtp, fetchData}
}

export default useOtp