import axios from "axios";
import Cookies from "js-cookie";
import React, {createContext, useContext, useEffect, useState} from 'react';
import { defaultHeaders } from "../utils/defaultHeaders";
const AppContext = createContext();

export function AppWrapper({children}) {
  const [userKey, setUserKey] = useState(null);
  const [userProfile, setUserProfile] = useState(null);
  const [userFavorites, setUserFavorites] = useState([])
  const [kprList, setKprList] = useState([])
  const ISSERVER = typeof window === "undefined";
  let savedKeyMember
  let savedProfile
  if(!ISSERVER){
    savedKeyMember = Cookies.get('keyMember') || sessionStorage.getItem('keyMember') || null
    savedProfile = Cookies.get('user') || sessionStorage.getItem('user') || null
  }

  useEffect(() => {
    if(savedKeyMember){
      setUserKey(JSON.parse(savedKeyMember))
      setUserProfile(JSON.parse(savedProfile))
    }
    // console.log('~ savedKeyMember', savedKeyMember)
  }, [savedKeyMember])

  useEffect(() => {
    if(userKey){
      getUserFavorites()
    }
    // console.log('~ userKey', userKey)
  }, [userKey])

  function getUserFavorites(){
    axios({
        url: `${process.env.NEXT_PUBLIC_API_HOST}/member/favorit/show/id`,
        method: 'POST',
        headers: {
            'AccessKey_Member': userKey,
            ...defaultHeaders
        },
    })
    .then((res) => {
      // console.log('~ res.data', res.data)
      if(!res.data.IsError){
        setUserFavorites(res.data.Data.map(val=>val.i_prpt))
      }else{
        throw res.data.ErrToDev
      }
    }).catch((err) => {
      console.log('~ err', err)
    });
  }

  function likeAction(propId){
    if(userFavorites && propId && userKey){
      const isAdd = !userFavorites.includes(propId)
      axios({
          url: `${process.env.NEXT_PUBLIC_API_HOST}/member/favorit/${isAdd?'insert':'delete'}?i_prpt=${propId}`,
          method: 'POST',
          headers: {
              'AccessKey_Member': userKey,
              ...defaultHeaders
          },
      })
      .then((res) => {
        // console.log('~ res.data', res.data)
        if(!res.data.IsError){
          getUserFavorites()
        }else{
          throw res.data.ErrToDev
        }
      }).catch((err) => {
        console.log('~ err', err)
      });
    }
  }

  return (
    <AppContext.Provider 
      value={{
        userKey, 
        setUserKey, 
        userProfile, 
        setUserProfile,
        userFavorites,
        getUserFavorites,
        likeAction,
        kprList, 
        setKprList
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export function useAppContext() {
  return useContext(AppContext)
}