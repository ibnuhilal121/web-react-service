export const defaultHeaders = {
    'X-Content-Type-Options': 'nosniff',
    'X-Frame-Options': 'SAMEORIGIN',
    'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
    'Permissions-Policy': 'geolocation=(), browsing-topics=()',
    'Referrer-Policy': 'origin-when-cross-origin'
}