export const getAccessKey = () => {
    if (typeof window !== 'undefined') 
        return localStorage.getItem('accessKey');
};
 
const getHeaderWithAccessKey = () => {
    const accessKey = getAccessKey();
    return {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': accessKey,
        'X-Content-Type-Options': 'nosniff',
        'X-Frame-Options': 'SAMEORIGIN',
        'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
        'Permissions-Policy': 'geolocation=(), browsing-topics=()',
        'Referrer-Policy': 'origin-when-cross-origin'
    }
}

export default getHeaderWithAccessKey