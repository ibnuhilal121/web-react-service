import { headersUniversal } from "../../constants";
import getHeaderWithAccessKey from "../getHeaderWithAccessKey";
// import Cookie from "js-cookie"

const getIP = async () => {
  try {
    const getIpReq = await fetch("https://geolocation-db.com/json/");
    const getIpRes = await getIpReq.json();
    // console.log("+++ getIpRes", getIpRes)
    return getIpRes.IPv4;
  } catch (e) {
    // console.log("~ e", e);
    return "";
  }
};

const appLogin = async () => {
  const ip = await getIP();
  const payload = {
    AppKey: process.env.NEXT_PUBLIC_APP_KEY,
    AppSecret: process.env.NEXT_PUBLIC_APP_SECRET
  }
  // const ip = "36.72.213.62"
  const appLoginReq = await fetch(
    `${process.env.NEXT_PUBLIC_API_HOST}/app/login`,
    {
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Origin': '*',
        'X-Content-Type-Options': 'nosniff',
        'X-Frame-Options': 'SAMEORIGIN',
        'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
        'Permissions-Policy': 'geolocation=(), browsing-topics=()',
        'Referrer-Policy': 'origin-when-cross-origin'
      },
      body: new URLSearchParams(payload),
    //   headers: headersUniversal,
    //   body: new URLSearchParams({
    //     AppKey: process.env.NEXT_PUBLIC_APP_KEY,
    //     AppSecret: process.env.NEXT_PUBLIC_APP_SECRET,
    //     Ip: ip
    // })
    }
  );
  const appLoginRes = await appLoginReq.json();
  // console.log("+++ appLoginRes", appLoginRes)
  const { AccessKey } = appLoginRes;
  if (!appLoginRes?.IsError) {
    // Simpan AksesKey di localStorage
    // Cookie.set('accessKey', AccessKey)
    if (typeof window !== "undefined") {
      localStorage.setItem("accessKey", AccessKey);
    }
    // await getSettingShow(AccessKey)
  } 
};

export const getSettingShow = async () => {
  const headers = getHeaderWithAccessKey();
  const getSettingsReq = await fetch(
    `${process.env.NEXT_PUBLIC_API_HOST}/app/settings/show`,
    {
      method: "POST",
      headers,
    }
  );
  const getSettingsRes = await getSettingsReq.json();
  // console.log("+++ getSettingsRes", data)
  if (!getSettingsRes.IsError) {
    // Update state
    const data = getSettingsRes.Data[0];
    if (typeof window !== "undefined") {
      sessionStorage.setItem("settings", JSON.stringify(data));
    }
  }
};

export default appLogin;
