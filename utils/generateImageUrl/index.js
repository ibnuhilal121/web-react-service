export const generateImageUrl = (url) => {
    if (!url) return '/images/thumb-placeholder.png';
  
    const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}${url.split('"')[0]}`;
    // console.log("+++ fullUrl",fullUrl);
    var img = new Image();
    img.src = fullUrl;
    const isImageExist = img.height != 0;
    
    if (isImageExist) {
      return fullUrl
    } else {
      return '/images/thumb-placeholder.png';
    }
  }