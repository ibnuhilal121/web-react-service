export default function limitString(text, count = 50){
    return text.slice(0, count) + (text.length > count ? "..." : "");
}