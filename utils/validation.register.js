import { RFC5322, numeric, passwordFormat, phoneNumberValidation, phoneLength } from '../helpers/regex';

// expected value: BOOLEAN
export const isInvalid = (i, v) => {
    switch (i) {
        case 'fullname':
            return 
        case 'email':
            return !RFC5322.test(String(v))
        case 'phone':
            return !numeric.test(v)
        case 'phone_validation':
            return !phoneNumberValidation.test(v)
        case 'phone_length_validation':
            return !phoneLength.test(v)
        case 'password':
            return !passwordFormat.test(v)
        case 'repassword':
            return !passwordFormat.test(v)
        default:
            break;
    }
}