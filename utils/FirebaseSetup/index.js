// Import the functions you need from the SDKs you need
import { initializeApp, getApps } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyChvqtwYtDjsBQDtyJ0W1yGN-L4lc7DWkg",
  authDomain: "btn-properti-revamp.firebaseapp.com",
  projectId: "btn-properti-revamp",
  storageBucket: "btn-properti-revamp.appspot.com",
  messagingSenderId: "793024022825",
  appId: "1:793024022825:web:ddb24724c6710f0a4ecf38",
  measurementId: "G-HVXM44W57B"
};

// Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

if (!getApps().length) {
    initializeApp(firebaseConfig);
};

export const auth = getAuth();

export default firebaseConfig;