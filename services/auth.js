import { appLogin } from "./master";
import md5 from "md5";
import { sha256 } from "js-sha256";
import { defaultHeaders } from "../utils/defaultHeaders";
import  JSEncrypt  from "../public/backupjsencrypt/tes/jsencrypt";

export const doLogin = async(email, password) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };
  
    const encrmd5 = md5(password.trim());
    const hashedPassword = sha256.hmac("BtNPr0p3rt!", encrmd5);
  
    const payload = {
      Email: email.trim(),
      Pwd: hashedPassword,
    };
  
    const memberLogin = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        AccessKey_App: keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams(payload),
    });
    const resMemberLogin = await memberLogin.json();
    return Promise.resolve(resMemberLogin);
  } catch (error) {
    return Promise.reject(error);
  };
};

export const encryptPass = (password) => {
  const secret = process.env.NEXT_PUBLIC_KEY_SECRET
  const hashedPass = sha256.hmac(secret, md5(password))
  return hashedPass
}


//GET CURRENT STEP(VERIFY STATUS EKYC)
export const getVerifyStatus = async(id) =>{
  try {
    const verified = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_EKYC}/getEKYCData/${id}`, {
      method: "GET",
      headers: {
        'Accept': 'application/json',
      }
    },);
    const resMemberLogin = await verified.json();
    return Promise.resolve(resMemberLogin);
  } catch (error) {
    return Promise.reject({});
  };
}

//GET EKYC DATA
export const getEkycData = async(id) =>{
  try {
    const encryptString = (userId) =>{
      const encrypt = new JSEncrypt();
      const RSA_PUBLIC_KEY = `
      -----BEGIN PUBLIC KEY-----
      MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCipAV5BicvV9tjYWhOGlNEtxcp
      WtOF4qNksMrcbZ25tMcb5SK2DvtukzBZW7gmc0ZQX2w6tEsw8cIglt8a0sXQ2OSb
      JcvTd1ds5YScMuhmZ1V3BIQgTPM6Yjb4FAAfFl903oF+5K5g0M35Ccdb4X1ssrQQ
      F7bEcQoU90SdBTqzEQIDAQAB
      -----END PUBLIC KEY-----`
      encrypt.setPublicKey(RSA_PUBLIC_KEY)
      const encrypted = encrypt.encrypt(userId.toString());
      return encodeURIComponent(encrypted.replaceAll('\n',''));
    }

    const verified = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_EKYC}/getCurrentStep/?userId=${encryptString(id)}`, {
      method: "GET",
      headers: {
        'Accept': 'application/json',
      }
    },);
    const resMemberLogin = await verified.json();
    return Promise.resolve(resMemberLogin);
  } catch (error) {
    return Promise.reject({isVerified:false,counterFailedVerification: 0});
  };
}

// GET BENEFIT e-KYC
export const getBenefitEkyc = async () => {
  try {
    const benefitFetch = await fetch(process.env.NEXT_PUBLIC_API_HOST_EKYC +"/getBenefit", {
      method: "GET",
      headers: {
        'Accept': 'application/json',
      }
    },);
    const benefit = await benefitFetch.json();
    return Promise.resolve(benefit);
  } catch (error) {
    return Promise.reject(error);
  }
}

// GET TnC e-KYC
export const getTncEkyc = async () => {
  try {
    const tncFetch = await fetch(process.env.NEXT_PUBLIC_API_HOST_EKYC + "/getTOC", {
      method: "GET",
      headers: {
        'Accept': 'application/json',
      }
    },);
    const tnc = await tncFetch.json();
    return Promise.resolve(tnc);
  } catch (error) {
    return Promise.reject(error);
  }
}

// Get profile data

export const getDetailMember = async (userKey) => {
  try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/detail`;
      const loadData = await fetch(endpoint, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_Member: userKey || JSON.parse(sessionStorage.getItem("keyMember")) || JSON.parse(Cookies.get("keyMember")),
              },
      });
      const response = await loadData.json()
      console.log("aaaaaaaa", response)
          
      return response.Data[0]

  } catch(error) {
      console.log({error})
      return {}
  }
}