import Axios from 'axios';
import Cookies from "js-cookie";
import { defaultHeaders } from '../utils/defaultHeaders';

export const doGetTipeRumahById = async(loaderHandler, id)=>{
    // console.log("+++ id",id);
    try {
        loaderHandler(true);
        const result = await Axios({
          url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getTipeRumahById?tipeRumah_id=${id}`,
          method: 'GET',
          headers: {
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
              ...defaultHeaders
          },
        });
        loaderHandler(false);
        const dataRes = result.data;
        return dataRes;
      } catch (err) {
        console.log(`Error Fetching Data Detail Tipe Property...! \n${err}`);
        loaderHandler(false);
      }

}

export const doGetIklanTipeRumahById = async(loaderHandler, id)=>{
    // console.log("+++ id",id);
    try {
        let AccessKey_Member,
          sessionKey = sessionStorage.getItem("keyMember"),
          cookieKey = Cookies.get("keyMember");
        AccessKey_Member = cookieKey ? JSON.parse(cookieKey) : sessionKey ? JSON.parse(sessionKey) : ''
        console.log('~ AccessKey_Member', AccessKey_Member)
        loaderHandler(true);
        const result = await Axios({
          url: `${process.env.NEXT_PUBLIC_API_HOST}/member/iklan/show`,
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            AccessKey_Member: AccessKey_Member,
            ...defaultHeaders
          },
          data: `id=${id}`
        });
        loaderHandler(false);
        const dataRes = result.data;
        return dataRes;
      } catch (err) {
        console.log(`Error Fetching Data Detail Tipe Property...! \n${err}`);
        loaderHandler(false);
      }

}

export const doGetDeveloperById = async(loaderHandler, id)=>{
    // console.log("+++ id",id);
    try {
        loaderHandler(true);
        const result = await Axios({
          url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getDeveloperById?developer_id=${id}`,
          method: 'GET',
          headers: {
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
              ...defaultHeaders
          },
        });
        loaderHandler(false);
        const dataRes = result.data;
        return dataRes;
      } catch (err) {
        console.log(`Error Fetching Data Developer...! \n${err}`);
        loaderHandler(false);
      }

}

export const doGetProperById = async(loaderHandler, id)=>{
    // console.log("+++ id",id);
    try {
        loaderHandler(true);
        const result = await Axios({
          url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getProperById?proper_id=${id}`,
          method: 'GET',
          headers: {
              app_key: process.env.NEXT_PUBLIC_APP_KEY,
              secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
              ...defaultHeaders
          },
        });
        loaderHandler(false);
        const dataRes = result.data;
        return dataRes;
      } catch (err) {
        console.log(`Error Fetching Data Developer...! \n${err}`);
        loaderHandler(false);
      }

}

export const doGetKavling = async(loaderHandler, id)=>{
  // console.log("+++ id",id);
  try {
      loaderHandler(true);
      const result = await Axios({
        url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getKavling?tipeRumah_id=${id}`,
        method: 'GET',
        headers: {
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
        },
      });
      loaderHandler(false);
      const dataRes = result.data;
      return dataRes;
    } catch (err) {
      console.log(`Error Fetching Data Kavling...! \n${err}`);
      loaderHandler(false);
    }

}

export const getReviewProperti = async(id) => {
  try {
    const reviewProperti = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getProperReview?id_proper=${id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      }
    });
    const resReviewProperti = await reviewProperti.json();
    return Promise.resolve(resReviewProperti);
  } catch (error) {
    
  }
}

export const doGetAreaSekitar = async(loaderHandler, id)=>{
  // console.log("+++ id",id);
  try {
    let AccessKey_App,
      localKey = localStorage.getItem("accessKey"),
      sessionKey = sessionStorage.getItem("keyMember"),
      cookieKey = Cookies.get("keyMember");
    AccessKey_App = localKey || sessionKey || cookieKey
    loaderHandler(true);
    const result = await Axios({
      url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/areasekitar/show?AccessKey_App=${AccessKey_App}&i_prpt=${id}`,
      method: 'GET',
      headers: {
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
      },
    });
    loaderHandler(false);
    const dataRes = result.data;
    // console.log('~ dataRes', dataRes)
    return dataRes;
  } catch (err) {
    console.log('~ err', err)
    loaderHandler(false);
  }
}