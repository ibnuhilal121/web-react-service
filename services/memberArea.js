import { headersUniversal } from "../constants";
import getHeaderWithAccessKey from "../utils/getHeaderWithAccessKey";
import { appLogin } from "./master";
import { defaultHeaders } from "../utils/defaultHeaders";

export const doCheckRegisterMemberEmail = async(loaderHandler, payload)=>{
    try {
        loaderHandler(true);
        const keyResponse = await appLogin();
        if (!keyResponse.IsError) {
            localStorage.setItem("accessKey", keyResponse.AccessKey);
        } else {
            throw keyResponse;
        };

        // const headers = getHeaderWithAccessKey();
        const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/checkemail`, {
        method: 'POST',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/x-www-form-urlencoded',
            'AccessKey_App': keyResponse.AccessKey,
            ...defaultHeaders
        },
        body: new URLSearchParams(payload),
        });
        const response = await request.json();
        loaderHandler(false);
        return response;
    } catch (error) {
        loaderHandler(false);
    }
}

export const doMemberRegister = async(loaderHandler, payload)=>{
    const headers = getHeaderWithAccessKey();
    loaderHandler(true);
    const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/insert`, {
    method: 'POST',
    headers,
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    loaderHandler(false);
    return response;
}

export const doResendActivation = async(loaderHandler, payload)=>{
    try {
        loaderHandler(true);
        const keyResponse = await appLogin();
        if (!keyResponse.IsError) {
            localStorage.setItem("accessKey", keyResponse.AccessKey);
        } else {
            throw keyResponse;
        };

        // const headers = getHeaderWithAccessKey();
        const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/resendactivation`, {
        method: 'POST',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/x-www-form-urlencoded',
            'AccessKey_App': keyResponse.AccessKey,
            ...defaultHeaders
        },
        body: new URLSearchParams(payload),
        });
        const response = await request.json();
        loaderHandler(false);
        return response;
    } catch (error) {
        loaderHandler(false);
    }
}

export const doSendForgotPassword = async(loaderHandler, payload)=>{
    try {
        loaderHandler(true);
        const keyResponse = await appLogin();
        if (!keyResponse.IsError) {
            localStorage.setItem("accessKey", keyResponse.AccessKey);
        } else {
            throw keyResponse;
        };
        // const headers = getHeaderWithAccessKey();
        const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/forgotpassword`, {
        method: 'POST',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/x-www-form-urlencoded',
            'AccessKey_App': keyResponse.AccessKey,
            ...defaultHeaders
        },
        body: new URLSearchParams(payload),
        });
        const response = await request.json();
        loaderHandler(false);
        return response;
    } catch (error) {
        loaderHandler(false);
    }
}