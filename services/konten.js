import getHeaderWithAccessKey from "../utils/getHeaderWithAccessKey";

export const doFetchKategoriPromo = async()=>{
    const headers = getHeaderWithAccessKey();
    // console.log("+++ headers", headers)
    const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/promo/kategori/show`, {
    method: 'POST',
    headers,
    body: new URLSearchParams({}),
    });
    const response = await request.json();
    return response;
}

export const doFetchPromos = async(loaderHandler, payload)=>{
    const headers = getHeaderWithAccessKey();
    loaderHandler(true);
    const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/promo/show`, {
    method: 'POST',
    headers,
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    loaderHandler(false);
    return response;
}

export const doFetchFeatures = async(loaderHandler) => {
    loaderHandler(true);
    const request = await fetch(`https://6368b3ec15219b8496046c0b.mockapi.io/features`, {
        method: 'GET'
    });
    const response = await request.json();
    loaderHandler(false);
    return response;
}

export const getGenerateLinkPreApproval = async (payload) => {
    const request = await fetch(`${process.env.NEXT_PUBLIC_WEBSERVICE}/generate-url`, {
    method: 'POST',
    headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
    },
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    console.log(response);
    return response;
}

export const getGenerateHomeServiceToken = async (payload) => {
    // const url = `${process.env.NEXT_PUBLIC_WEBSERVICE}/homeservice/token`;// Ini menggunakan Gateaway
    const url = `${process.env.NEXT_PUBLIC_TOKEN_HOME_SERVICE}` 
    const request = await fetch(url, {
    method: 'POST',
    headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
    },
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    console.log(response);
    return response;
}

export const getGenerateProfesionaListingToken = async (payload) => {
    // const url = `${process.env.NEXT_PUBLIC_WEBSERVICE}/proflisting/token`;// Ini menggunakan Gateaway
    const url = `${process.env.NEXT_PUBLIC_TOKEN_PROFESIONAL_LISTING}` 
    const request = await fetch(url, {
    method: 'POST',
    headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
    },
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    console.log(response);
    return response;
}

export const getGenerateHomeServiceLink = async ( payload, type) => {
    const tokenHome = localStorage.getItem("generate-token")
    const body =JSON.parse(payload)
    // console.log("payload",JSON.parse(payload).platform_id)
    // const url = `${process.env.NEXT_PUBLIC_WEBSERVICE}/homeservice/link-generate`;// Ini menggunakan Gateaway
    const url = `${process.env.NEXT_PUBLIC_GENERATE_LINK_HOME_SERVICE}` 
    const request = await fetch(url, {
    method: 'POST',
    headers: {
        "Authorization": `Bearer ${tokenHome}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        // Authorization: `Bearer ${token}`
        // Authorization: "Bearer" + localStorage.getItem("generate-token")
    },
   body: payload,
    });
    const response = await request.json();
    console.log(response);
    return response;
}

export const getGenerateProfesionaListingLink = async ( payload, type) => {
    const tokenProfesiona = localStorage.getItem("generate-token-profesiona-listing")
    const body =JSON.parse(payload)
    // console.log("payload",JSON.parse(payload).platform_id)
    // const url = `${process.env.NEXT_PUBLIC_WEBSERVICE}/proflisting/link-generate`;// Ini menggunakan Gateaway
    const url = `${process.env.NEXT_PUBLIC_GENERATE_LINK_PROFESIONAL_LISTING}` 
    const request = await fetch(url, {
    method: 'POST',
    headers: {
        "Authorization": `Bearer ${tokenProfesiona}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        // Authorization: `Bearer ${token}`
        // Authorization: "Bearer" + localStorage.getItem("generate-token")
    },
   body: payload,
    });
    const response = await request.json();
    console.log(response);
    return response;
}




