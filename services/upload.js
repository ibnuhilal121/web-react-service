import { appLogin } from "./master"
import { defaultHeaders } from "../utils/defaultHeaders";

export const uploadImage = async(folderName, extension, file) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };

    const uploading = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/file/upload`, {
      method: "POST",
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        FolderName: folderName,
        FileExtension: extension.toLowerCase(),
        FileBase64: file.split('base64,')[1]
      })
    });
    const resUploading = await uploading.json();
    return Promise.resolve(resUploading);
  } catch (error) {
    return Promise.reject(error);
  }
}

export const uploadImageIklan = async(folderName, extension, file) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };

    const uploading = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/image/upload`, {
      method: "POST",
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        TipeUpload: "1",
        FolderName: folderName,
        FileExtension: extension.toLowerCase(),
        FileBase64: file.split('base64,')[1],
        // Setting: JSON.stringify({
        //   "Size": "100x100",
        //   "Type": `${extension.toLowerCase()}`,
        //   "Quality": "100",
        //   "WithWatermark": true,
        //   "PositionWatermark": "center"
        // })
      })
    });
    const resUploading = await uploading.json();
    return Promise.resolve(resUploading);
  } catch (error) {
    return Promise.reject(error);
  }
}