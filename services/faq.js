import { defaultHeaders } from "../utils/defaultHeaders";

export const fetchFAQ = async(keyword = '', tipe = '') => {
  let params = '';
  if (keyword) {
    params = `search=${keyword}`
  } else {
    params = `topik=${tipe}`
  }
  try {
    const dataFAQ = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/faq/show?${params}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      }
    });
    const resDataFAQ = await dataFAQ.json();
    if (resDataFAQ.status) {
      return Promise.resolve(resDataFAQ.data);
    } else {
      throw resDataFAQ;
    };
  } catch (error) {
    
  }
};

export const detailFAQ = async(id, id_member) => {
  try {
    const dataFAQ = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/faq/show?id=${id}&id_member=${id_member}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      }
    });
    const resDataFAQ = await dataFAQ.json();
    if (resDataFAQ.status) {
      return Promise.resolve(resDataFAQ.data);
    } else {
      throw resDataFAQ;
    };
  } catch (error) {
    
  }
};

export const actionLike = async(action, id, id_member) => {
  try {
    const likeFAQ = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/faq/add${action}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        id,
        id_member
      })
    });
    const resLikeFAQ = await likeFAQ.json();
    if (resLikeFAQ.status) {
      return Promise.resolve(resLikeFAQ.data);
    } else {
      throw resLikeFAQ;
    };
  } catch (error) {
    
  }
};