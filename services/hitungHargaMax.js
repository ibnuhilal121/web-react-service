import { defaultHeaders } from "../utils/defaultHeaders";

export const hitungHargaMax = async(penghasilan, pengeluaran, waktu) => {
  try {
    const dataHarga = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/simulasi/hargamax`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        penghasilan,
        pengeluaran,
        jangka_waktu: waktu
      })
    });
    const resDataHarga = await dataHarga.json();
    return Promise.resolve(resDataHarga);
  } catch (error) {
    
  }
};