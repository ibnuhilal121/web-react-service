import getHeaderWithAccessKey from "../utils/getHeaderWithAccessKey";

export const doFetchAreaSekitar = async(loaderHandler, payload)=>{
    const headers = getHeaderWithAccessKey();
    loaderHandler(true);
    const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/areasekitar/kategori/show`, {
    method: 'POST',
    headers,
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    loaderHandler(false);
    return response;
}

export const doFetchBlogShow = async(loaderHandler, payload)=>{
    const headers = getHeaderWithAccessKey();
    loaderHandler(true);
    const request = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/blog/show`, {
    method: 'POST',
    headers,
    body: new URLSearchParams(payload),
    });
    const response = await request.json();
    loaderHandler(false);
    return response;
}