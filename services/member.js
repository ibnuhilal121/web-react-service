import { appLogin } from "./master";
import { defaultHeaders } from "../utils/defaultHeaders";

export const deleteIklan = async(id, userKey) => {
  try {
    const deleteData = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/iklan/delete`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        id
      })
    });
    const resDeleteData = await deleteData.json();
    if (resDeleteData.IsError) {
      throw resDeleteData.ErrToUser;
    } else {
      return Promise.resolve(resDeleteData);
    };
  } catch (error) {
    
  }
}

export const addIklan = async(userKey, payload, type) => {
  try {
    const submitIklan = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/iklan/${type === 'new' ? 'insert' : 'update'}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
      body: new URLSearchParams(payload)
    });
    const resSubmitIklan = await submitIklan.json();
    return Promise.resolve(resSubmitIklan);
  } catch (error) {
    
  }
};

export const updateAreaSekitar = async(userKey, idIklan, area) => {
  try {
    const updateArea = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/iklan/update/areasekitar`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        id: idIklan,
        area_sktr: area
      })
    });
    const resUpdateArea = await updateArea.json();
    return Promise.resolve(resUpdateArea);
  } catch (error) {
    
  }
}

export const getDetailIklan = async(userKey, id) => {
  try {
    const detailIklan = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/iklan/show`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({ id })
    });
    const resDetailIklan = await detailIklan.json();
    return Promise.resolve(resDetailIklan);
  } catch (error) {
    
  }
};

export const getAgent = async(body) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };

    const agent = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/agensi/show`, {
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams(body)
    });
    const resAgent = await agent.json();
    return Promise.resolve(resAgent);
  } catch (error) {
    
  }
};

export const getPesan = async(userKey, body) => {
  try {
    const dataPesan = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/show`, {
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_Member': userKey,
        ...defaultHeaders
      },
      body: new URLSearchParams(body)
    });
    const resPesan = await dataPesan.json();
    if (resPesan.IsError) {
      return Promise.reject(resPesan);
    } else {
      return Promise.resolve(resPesan);
    };
  } catch (error) {
    
  }
};

export const getNotification = async(idUser, page, limit, userKey) => {
  try {
    const dataNotification = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/member/notifikasi/show?AccessKey_Member=${userKey}&Page=${page}&Limit=${limit}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      }
    });
    const resNotification = await dataNotification.json();
    if (resNotification.status) {
      return Promise.resolve(resNotification);
    } else {
      return Promise.reject(resNotification);
    };
  } catch (error) {
    
  }
};

export const deleteNotification = async(idNotif, idUser) => {
  try {
    const actionDelete = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/member/notifikasi/delete`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        id: idNotif,
        id_member: idUser
      })
    });
    const responseDelete = await actionDelete.json();
    if (responseDelete.status) {
      return Promise.resolve(responseDelete);
    } else {
      return Promise.reject(responseDelete);
    };
  } catch (error) {
    
  }
};

export const deleteAllNotif = async(idUser) => {
  try {
    const actionDeleteAll = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/member/notifikasi/deleteall`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        id_member: idUser
      })
    });
    const responseDeleteAll = await actionDeleteAll.json();
    if (responseDeleteAll.status) {
      return Promise.resolve(responseDeleteAll);
    } else {
      return Promise.reject(responseDeleteAll);
    };
  } catch (error) {
    
  }
};

export const getMemberDetail = async(userKey) => {
  try {
    const dataProfile = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/detail`, {
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_Member': userKey,
        ...defaultHeaders
      }
    });
    const resProfile = await dataProfile.json();
    if (resProfile.IsError) {
      return Promise.reject(resProfile);
    } else {
      return Promise.resolve(resProfile);
    };
  } catch (error) {
    
  }
};