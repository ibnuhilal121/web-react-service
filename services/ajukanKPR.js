import { async } from "@firebase/util";
import axios from "axios";
import { generateKey } from "../helpers/generateKey";
import { defaultHeaders } from "../utils/defaultHeaders";

export const continueFillKPR = async(userKey, id) => {
  try {
    const userId = JSON.parse(sessionStorage.getItem('user')).id
    const pengajuanMember = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/detailpengajuan`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        kpr_id: id,
        AccessKey_Member: userKey,
        key: generateKey(userId)
      })
    });
    const resPengajuan = await pengajuanMember.json();
    if (resPengajuan.status) {
      return Promise.resolve(resPengajuan.data);
    } else {
      throw resPengajuan;
    };
    
  } catch (error) {
    
  };
};

export const continueKPRStok = async(userKey, id) => {
  try {
    const pengajuanMember = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v2/pengajuan/stok/detailpengajuan`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        AccessKey_Member: userKey,
        kpr_id: id
      })
    });
    const resPengajuan = await pengajuanMember.json();
    if (resPengajuan.status) {
      return Promise.resolve(resPengajuan.data);
    } else {
      throw resPengajuan;
    };
    
  } catch (error) {
    
  };
};

export const uploadDokumenKPR = async(kpr_id, image, type) => {
  const payload = {
    extension: image.extension.toLowerCase(),
    jenis_file: type,
    file: encodeURI(image.file.split("base64,")[1]),
  };

  try {
    const dataUploaded = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${kpr_id?.substring(0, 5) === 'KPRST' ? 'stok' : 'nonstok'}/uploaddokumen`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams(payload),
    })
    const resUploaded = await dataUploaded.json();
    return Promise.resolve(resUploaded);
  } catch (error) {};
};

export const getCountdown = async () => {
  try {
    const payload = {

    }
    const { data } = await axios(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
      },
      body: new URLSearchParams(payload),
    })
    return data;
  } catch (error) {
    console.log(error);
  }
}