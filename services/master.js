import { getAccessKey } from "../utils/getHeaderWithAccessKey";
import { defaultHeaders } from "../utils/defaultHeaders";

export const appLogin = async() => {
  try {
    const payload = {
      AppKey: process.env.NEXT_PUBLIC_APP_KEY,
      AppSecret: process.env.NEXT_PUBLIC_APP_SECRET
    }
    // GET NEW ACCESS KEY
    const accessKey = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/app/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Origin': '*',
        ...defaultHeaders
      },
      body: new URLSearchParams(payload)
    });
    const keyResponse = await accessKey.json();
    return Promise.resolve(keyResponse);
  } catch (error) {
    return Promise.reject(error);
  };
}

export const getFasilitas = async() => {
  try {
    const dataFasilitas = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/master/fasilitas/show`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        AccessKey_App: getAccessKey(),
        ...defaultHeaders
      },
      body: new URLSearchParams({i_par: 1})
    })
    const resDataFasilitas = await dataFasilitas.json();
    return Promise.resolve(resDataFasilitas);
  } catch (error) {
    
  };
};

export const getProvince = async() => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };
    
    const propinsi = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/master/propinsi/show`, {
      method: "POST",
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        Sort: "n ASC",
      }),
    });
    const dataPropinsi = await propinsi.json();
    return Promise.resolve(dataPropinsi);
  } catch (error) {
    
  }
}

export const getKota = async(provinsi) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };
    
    const kota = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/master/kota/show`, {
      method: "POST",
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        i_prop: provinsi,
        Sort: "n ASC"
      }),
    });
    const dataKota = await kota.json();
    return Promise.resolve(dataKota);
  } catch (error) {
    
  }
}

export const getKecamatan = async(kota) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };
    
    const kecamatan = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/master/kecamatan/show`, {
      method: "POST",
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        i_kot: kota,
        Sort: "n ASC"
      }),
    });
    const dataKecamatan = await kecamatan.json();
    return Promise.resolve(dataKecamatan);
  } catch (error) {
    
  }
}

export const getKelurahan = async(kecamatan) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };
    
    const kelurahan = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/master/kelurahan/show`, {
      method: "POST",
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded',
        'AccessKey_App': keyResponse.AccessKey,
        ...defaultHeaders
      },
      body: new URLSearchParams({
        i_kec: kecamatan,
        Sort: "n ASC"
      }),
    });
    const dataKelurahan = await kelurahan.json();
    return Promise.resolve(dataKelurahan);
  } catch (error) {
    
  }
}

export const getAreaSekitar = async(i_prpt) => {
  try {
    const keyResponse = await appLogin();
    if (!keyResponse.IsError) {
      localStorage.setItem("accessKey", keyResponse.AccessKey);
    } else {
      throw keyResponse;
    };

    const areaSekitar = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/areasekitar/show?i_prpt=${i_prpt}&AccessKey_App=${keyResponse.AccessKey}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
        // 'Access-Control-Allow-Origin': '*',
        // 'Content-Type': 'application/x-www-form-urlencoded',
        // AccessKey_App: keyResponse.AccessKey
      }
    });
    const resAreaSekitar = await areaSekitar.json();
    return Promise.resolve(resAreaSekitar);
  } catch (error) {
    
  }
};

export const getCabangStok = async(tipeKPR) => {
  try {
    const dataCabang = await fetch(
      `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/kantorcabang?tipe=${tipeKPR}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          app_key: process.env.NEXT_PUBLIC_APP_KEY,
          secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          ...defaultHeaders
        },
      }
    );
    const responseCabang = await dataCabang.json();
    if (responseCabang.status) {
      return Promise.resolve(responseCabang.data);
    } else {
      return Promise.reject(responseCabang);
    };
  } catch (error) {
    
  }
};