import Link from "next/link";
import Cookies from "js-cookie";
import { signOut } from "next-auth/client";
import { useMediaQuery } from "react-responsive";
import { useRouter } from "next/router";
import { useAppContext } from "../context";
import { useSession } from "next-auth/client";
import { logout } from "../helpers/auth";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../utils/FirebaseSetup";
import { useEffect, useState } from "react";
import styles from "../components/element/modal_confirmation//modal_confirmation.module.scss";
import Lottie from "react-lottie";
import * as animationData from "../public/animate_home.json";
import {CSSTransition} from 'react-transition-group';



export default function MenuMember(props) {
  const isTabletOrDesktop = useMediaQuery({ query: `(max-width: 768px)` });
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const router = useRouter();
  const { userKey,setUserKey, setUserProfile,userProfile } = useAppContext();
  const {isVerified = false} = userProfile || {}
  const { active, isDashboard } = props;
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const [user, setUser] = useState(null);


  useEffect(() => {

    onAuthStateChanged(auth, (user) => {
      if (user) {
        setUser({
          provider: user.providerData[0].providerId,
          name: user.providerData[0].displayName,
          email: user.providerData[0].email,
          image: user.providerData[0].photoURL,
          id: user.providerData[0].uid,
          token: user.accessToken
        })
      } else {
        setUser(null)
      }
    })
  }, [])


  const submitLogout = () => {
    if (user) {
      logout()
      Cookies.remove("user");
      Cookies.remove("keyMember");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("keyMember");
      localStorage.removeItem("user");
      localStorage.removeItem("keyMember");
      localStorage.removeItem("generate-url");
      localStorage.removeItem("generate-token");
      localStorage.removeItem("generate-token-profesiona-listing");
      localStorage.removeItem("body-homeservice-link");
      localStorage.removeItem("body-homeservice-token");
      localStorage.removeItem("body-profesionallisting-token");
      localStorage.removeItem("body-profesionallisting-link");
      localStorage.removeItem("body-preapproval");
      localStorage.removeItem("url-home-services");
      localStorage.removeItem("url-profesiona-listing");
      localStorage.removeItem("title");
      localStorage.removeItem("navigate");
      setUserKey(null);
      setUserProfile(null);
      setUser(null);
      router.push("/");
    } else {
      Cookies.remove("user");
      Cookies.remove("keyMember");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("keyMember");
      localStorage.removeItem("generate-url");
      localStorage.removeItem("generate-token");
      localStorage.removeItem("generate-token-profesiona-listing");
      localStorage.removeItem("body-homeservice-link");
      localStorage.removeItem("body-homeservice-token");
      localStorage.removeItem("body-profesionallisting-token");
      localStorage.removeItem("body-profesionallisting-link");
      localStorage.removeItem("body-preapproval");
      localStorage.removeItem("url-home-services");
      localStorage.removeItem("url-profesiona-listing");
      localStorage.removeItem("title");
      localStorage.removeItem("navigate");
      localStorage.removeItem("user");
      localStorage.removeItem("keyMember");
      setUserKey(null);
      setUserProfile(null);
      router.push("/");
    }
  };
  console.log("dataprofile",userProfile)

  return (
    <>
    <div className="sticky-top">
      <div className="dashboard-sidebar-menu">
        <ul className="list-group menu_member">
          <Link href="/member/profil">
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'profil')} className={active == "profil" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}>
              Profil <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href={"/member/kredit"}>
          {/* <Link href={"/member/kredit"}> */}
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'kredit')} className={active == "kredit" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}
             >
              Pengajuan KPR <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href="/member/pencarian">
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'pencarian')} className={active == "pencarian" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}>
              Hasil Pencarian <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href="/member/favorit">
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'favorit')} className={active == "favorit" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}>
              Favorit <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href="/member/pesan">
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'pesan')} className={active == "pesan" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}>
              Pesan <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href="/member/reward">
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'reward')} className={active == "reward" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}>
              Reward <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href="/member/verified">
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'verified')} className={active == "verified" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}>
              Verified Now <div id="arrow-right"></div>
            </li>
          </Link>
          
        </ul>

        {router.pathname === "/member/verified" ?
        
          <div className="card mb-5" style={{padding:"25px 20px",maxHeight:"119px"}}>
          <h4 className="mb-0" style={{fontFamily:"Futura",fontSize:"18px",fontWeight:"700",lineHeight:"150%",color:"#40536e",whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  minHeight:"28px"
                }}
          >{userProfile?.n}</h4>
          {isVerified == true ?
            
            <p className="mt-3" style={{fontFamily:"Futura",fontWeight:"400",fontSize:"12px",color:"#fff",backgroundColor:"#0061A7", padding:"4px",display:"flex",width:"fit-content",borderRadius:"4px"}}>
              Verified Member 
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" fill="currentColor" className="bi bi-patch-check-fill ms-1" viewBox="0 0 16 16">
                <path
                stroke="#0061A7"
                    stroke-width="1"
                 d="M10.067.87a2.89 2.89 0 0 0-4.134 0l-.622.638-.89-.011a2.89 2.89 0 0 0-2.924 2.924l.01.89-.636.622a2.89 2.89 0 0 0 0 4.134l.637.622-.011.89a2.89 2.89 0 0 0 2.924 2.924l.89-.01.622.636a2.89 2.89 0 0 0 4.134 0l.622-.637.89.011a2.89 2.89 0 0 0 2.924-2.924l-.01-.89.636-.622a2.89 2.89 0 0 0 0-4.134l-.637-.622.011-.89a2.89 2.89 0 0 0-2.924-2.924l-.89.01-.622-.636zm.287 5.984-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708.708z"/>
              </svg>
              </p>
             
            :
            <a className="btn p-0 text-start d-flex mt-4" data-bs-toggle="modal" data-bs-target="#verifiedModal" style={{width:"fit-content",padding:"4px"}}>
            <p className="m-0" style={{fontFamily:"Futura",fontWeight:"400",fontSize:"12px",color:"#FFB800"}}>
            Unverified Member
            <svg className="ms-1 bi bi-exclamation-circle-fill" xmlns="http://www.w3.org/2000/svg" width="13" height="13" fill="currentColor" viewBox="0 0 16 16" style={{transform:"translateY(-1px)"}}>
              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
            </svg>
            </p>
            </a> 
          }
          </div>
         
        :<></>}
        <ul className="list-group menu_member">
          <Link href={"/member/properti"}>
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'properti')} className={active == "properti" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}
           >
              Properti <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href={"/member/profesional"}>
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'profesional')} className={active == "profesional" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}
           >
              Profesional <div id="arrow-right"></div>
            </li>
          </Link>
          <Link href={"/member/agen"}>
            <li onClick={() => sessionStorage.setItem('log-rts-mmbr', 'agen')} className={active == "agen" ? "list-group-item active d-flex align-items-center justify-content-between" : "list-group-item d-flex align-items-center justify-content-between"}
             >
              agen <div id="arrow-right"></div>
            </li>
          </Link>
        </ul>
       
         
         <div>



       
              </div>
        {isDashboard ? (
          <div className="form-group button-submit-area text-right d-flex justify-content-start m-3" style={{ maxWidth: 480 }}>
            <div
              type="submit"
              className="btn btn-main btn-profesional d-flex justify-content-center"
              onClick={submitLogout}
              style={{
                fontSize: 14,
                fontFamily: "Helvetica",
                fontWeight: "700",
                width: 164,
                height: 46,
                lineHeight: "22.4px",
                textAlign: "center",
                alignItems:"center",
                padding: "10px 10px",
              }}
            >
              Keluar
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
    <div className="modal fade" id="verifiedModal" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered m-auto px-0">
          <div className="modal-content modal_delete h-100" style={{padding:"20px 29px"}}>
            
            <div className="modal-body text-center pt-3 px-0">
            <div className="text-center">
                        
                          <h4 className="modal-title mb-3" style={{fontFamily:"Futura",fontStyle:"normal",lineHeight:"130%", fontWeight:"700",color:"#000000"}}>Lakukan e-KYC</h4>
                          <div className="desc text-center mb-0" style={{ fontFamily: "Helvetica", fontWeight: 400,fontSize:"16px",color:'#000000' }}>
                          Silakan lakukan e-KYC melalui Mobile App BTN Properti
                          </div>
                          {isTabletOrDesktop ? (<Lottie options={defaultOptions} isStopped={false} isPaused={false} />) 
                          : (<Lottie options={defaultOptions} height={146} width={212} isStopped={false} isPaused={false} style={{marginBottom:"121px",marginTop:"125px"}} />)}
                          
                         
                          
                            <button
                              type="buttom"
                              className="btn btn-main form-control btn-back px-5"
                              style={{
                                width:"100%",
                                maxWidth: "540px",
                                height: "48px",
                                fontSize: "14px",
                                fontFamily: "Helvetica",
                                fontWeight: 700,
                                backgroundColor: "#0061A7",
                              }}
                              data-bs-dismiss="modal"
                              onClick={(()=>{window.location.replace("/member/verified/")})}
                            >
                              Oke
                            </button>
                            {/* <Link href="/">
                            </Link> */}
                         
                        </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
