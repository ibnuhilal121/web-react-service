import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Slider from "react-slick";
import MinimalProperty from "../data/MinimalProperty";

export default function PropertySlide(props) {
  const { data_all } = props;
  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          // centerMode: true,
          // centerPadding: '20%',
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          // centerMode: true,
          // centerPadding: '20%',
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      <div id="webView">
        <Slider {...settings} className="card_property_minimal_container" id="propertiminimal">
          {data_all.map((data, index) => (
            <div key={index}>
              <MinimalProperty data={data} />
            </div>
          ))}
        </Slider>
      </div>
      <div id="mobileView">
        <div id="news-Blog-Slide">
          {data_all.map((data, index) => (
            <div key={index} className="">
              <MinimalProperty data={data} />
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
