import React from "react";
import Slider from "react-slick";
import ReactImageFallback from "react-image-fallback";

export default function AntdCarousel(props) {
    const {images = []} = props

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: dots => (
            <div
            style={{
                //backgroundColor: '#ddd',
                padding: '5px',
                display: 'flex',
            }}
            >
            <ul style={{zIndex: 30, marginBottom: '30px', padding: '0px' }}> {dots} </ul>
            </div>
        ),
        customPaging: function(i) {
            return <div style={{opacity: 0}}>clicked</div>;
        },
        dotsClass: "slick-dots slick-thumb"
    };

return(
    <div id="antdCarousel">
        <Slider {...settings}>
            {images?.map((data,index) => 
                <div>
                    <ReactImageFallback
                        src={data ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.replace('1|', '') : null}
                        fallbackImage="/images/thumb-placeholder.png"
                        className="img-fluid" 
                        style={{width: "100%", height: 310, objectFit: "cover"}}
                    />
                </div>
            )}
        </Slider>
    </div>
);
}
