import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import React, { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import { useMediaQuery } from "react-responsive";

export default function SejarahSlide({
  data = [],
  step,
  setStep,
}) {
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  let sliderRef = useRef()
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    sliderRef.slickGoTo(step);
  }, [step])

  const settings_2 = {
    dots: false,
    arrows: false,
    infinite: false,
    speed: 1000,
    slidesToShow: isTabletOrMobile ? 0.95 : 1.2,
    slidesToScroll: 1,
    beforeChange: (prev, next) => {
      if (next < 0 || Math.ceil(next) >= data.length) return;
      setStep(Math.ceil(next))
    },
  };

  return (
    <div id="SejarahSlide">
      <Slider ref={slider => (sliderRef = slider)} {...settings_2}>
        {data.map((data, index) => (
          <div onLoad={() => sliderRef?.slickGoTo(step) }>
            <h1 className="item">{data.TANGGAL}</h1>
            <div className="content">
              <img src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.GAMBAR.replace("1|", "")}`} alt="" />
              <div>
                <h5 className="judul">{data.JUDUL}</h5>
                <p className="deskripsi">{data.DESKRIPSI}</p>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
