import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import HighlightOffOutlinedIcon from "@mui/icons-material/HighlightOffOutlined";
import { IconButton } from "@material-ui/core";
import { Image } from "react-bootstrap";
import { height, width } from "@mui/system";
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

const useStyles = makeStyles((theme) => ({
  paper: {
    overflowY: "unset",
    borderRadius: "25px",
    width: "480px",
    height: "auto",
    '@media only screen and (max-width: 500px)' : {
      width: '240px'
    }
  },
  customizedButton: {
    borderWidth: 2,
    borderColor: "black",
    borderStyle: "solid",
    position: "absolute",
    left: "92%",
    top: "-3%",
    backgroundColor: "#ffffff",
    padding: "10px",
    color: "black",
    "&:hover": {
      backgroundColor: "lightgrey",
      color: "black",
    },
    "& svg": {
      // fontSize: 15
      outlinedColor: "black",
      outlined: "black",
    },
    '@media only screen and (max-width: 500px)' : {
      left: "88%",
      top: "-4%",
      padding: "7px",
    }
  },
  customizedImage: {
    width: "350px",
    height: "auto",
    '@media only screen and (max-width: 500px)' : {
      width: '200px'
    }
  },
  customizedText1: {
    color: "#6e6e6e", 
    fontSize:"13px",
    '@media only screen and (max-width: 500px)' : {
      fontSize:"9px",
    }
  },
  customizedText2: {
    fontSize: "15px", 
    color: "#6d6d6d", 
    '@media only screen and (max-width: 500px)' : {
      fontSize:"12px",
    }
  },
  customizedContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    textAlign: "center",
    paddingBottom: "75px",
    paddingTop:"70px",
    '@media only screen and (max-width: 500px)' : {
      paddingBottom: "29px",
      paddingTop:"41px",
      }
  },
}));

const Modal = ({ show, onClose, children, title }) => {
  const [isBrowser, setIsBrowser] = useState(false);

  const titleModal = localStorage.getItem("title");
  const bodyScrollLock = require('body-scroll-lock');
  const targetElement = document.getElementById("popup");
  

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  // useEffect(() => {
  //   console.log("open is changed 2")
  //   if(show){
  //     bodyScrollLock.disableBodyScroll(targetElement);
  //   }else{
  //     bodyScrollLock.enableBodyScroll(targetElement);
  //   }
  // },[show]); 
  
  // useEffect(() => {
  //   console.log("open is changed")
  //   if(show){
  //     document.getElementsByTagName('html')[0].style.overflow = "hidden";
  //   }else{
  //     document.getElementsByTagName('html')[0].style.overflow = "auto";
  //   }
  // },[show]); 

  const classes = useStyles();

  const handleCloseClick = (e) => {
    e.preventDefault();
    onClose();
  };

  const modalContent = show ? (
  <div class="popup" id="popup">
    <Dialog
      open={open}
      classes={{ paper: classes.paper }}
      onClose={handleCloseClick}
      width="1000px"
    >
      <div
        className={classes.customizedContainer}
      >
        <Image
          src="/images/modal/roket.png"
          className={classes.customizedImage}
          // position='relative'
          // objectFit='contain'
        />
        {titleModal == 'Virtual Expo' ? 
        <DialogContent>
          <DialogContentText id="alert-dialog-description" className={classes.customizedText1} >
            Akan Segera Hadir BTN Virtual Expo, rasakan pengalaman baru
            mengikuti Pameran Properti Virtual.
          </DialogContentText>
          <DialogContentText id="alert-dialog-description" className={classes.customizedText2}>
            <i><b>Create your new experiences with BTN Virtual Expo.</b></i>
          </DialogContentText>
        </DialogContent> 
        : titleModal == 'Home Service' ? <DialogContent>
        <DialogContentText id="alert-dialog-description" className={classes.customizedText1} >
          Akan segera hadir Fitur terbaru BTN Properti.<br></br>
          Mempermudah hidupmu dengan layanan Home Service dari BTN Properti.
        </DialogContentText>
        <DialogContentText id="alert-dialog-description" className={classes.customizedText2}>
          <i><b>Easy Life With BTN Properti.</b></i>
        </DialogContentText>
      </DialogContent>
       : titleModal == 'Jasa Profesional' ? <DialogContent>
        <DialogContentText id="alert-dialog-description" className={classes.customizedText1} >
          Akan segera hadir Fitur terbaru BTN Properti.<br></br>
          Temukan inspirasi desain bangun hunianmu bersama Jasa Profesional BTN Properti x Arsitag.
        </DialogContentText>
        <DialogContentText id="alert-dialog-description" className={classes.customizedText2}>
          <i><b>Create your Dream Home with BTN Properti.</b></i>
        </DialogContentText>
     </DialogContent>    
      : titleModal == 'Kredit Komersial & UMKM' ? <DialogContent>
        <DialogContentText id="alert-dialog-description" className={classes.customizedText1} >
          Akan segera hadir Fitur terbaru BTN Properti.<br></br>
          Nikmati kemudahan pengajuan Kredit Komersial dan UMKM kapanpun dan dimanapun.
        </DialogContentText>
        {/* <DialogContentText id="alert-dialog-description" className={classes.customizedText2}>
          <i><b>Create your Dream Home with BTN Properti.</b></i>
        </DialogContentText> */}
      </DialogContent> : null }
       
        <IconButton
          onClick={handleCloseClick}
          color="primary"
          oulined="black"
          className={classes.customizedButton}
        >
          <CloseIcon />
          {/* <HighlightOffOutlinedIcon /> */}
        </IconButton>
      </div>
    </Dialog>
    </div>
  ) : null;

  return modalContent;
};

export default Modal;
