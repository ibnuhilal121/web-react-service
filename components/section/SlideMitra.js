// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

import React from "react";
import Slider from "react-slick";
import { useMediaQuery } from "react-responsive";
import data_mitra from '../../sample_data/data_mitra'
import styles from "./SlideMitra.module.scss";

export default function Slidemitra({data}) {

    const [index, setIndex] = React.useState(0);
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });

    var settings = {
        dots: true,
        //arrows: true,
        infinite: false,
        className: "center",
        // centerMode: true,
        centerPadding: "60px",
        afterChange: index => setIndex(index),
        slidesToShow: isTabletOrMobile ? 4 : 5,
        slidesToScroll: 4,
        speed: 500,
        rows: 2,
        slidesPerRow: 1,
        focusOnSelect: true,
        nextArrow: (
            <div>
                <img 
                    className={isTabletOrMobile ? styles.slideRight : ""}
                    id="nextArrow"
                    src={index === (isTabletOrMobile ? 5 : 4) ? "/images/icons/Left.svg" : "/images/icons/arrow_slide.svg"} 
                    style={{transform: index === (isTabletOrMobile ? 5 : 4) ? "rotate(180deg)" : "rotate(0deg)", marginLeft: isTabletOrMobile ? 0 : 22}}
                />
            </div>),
        prevArrow: (
            <div className="slick-prev mitra-slider">
                <img 
                    className={isTabletOrMobile ? styles.slideLeft : ""}
                    id="prevArrow"
                    src={index === 0 ? "/images/icons/Left.svg" : "/images/icons/arrow_slide.svg"} 
                    style={{transform: index === 0 ? "rotate(0deg)" : "rotate(180deg)"}}
                />
            </div>)
    };


    
  return (
    <Slider {...settings}>
        { data.map((dev, index) =>
            <div key={index + 1} >
                <div className="item_mitra text-center" >
                    {/* <center style={{
                        height: isTabletOrMobile ? 80 : 120,
                        width: isTabletOrMobile ? 80 : 120,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: "8px",
                        border: "1px solid #EEEEEE",
                    }}> */}
                    <img
                    style={{
                        height: isTabletOrMobile ? 78 : 118,
                        width: isTabletOrMobile ? 78 : 118,
                        margin: "0 auto"
                    }}
                     src={generateImageUrl(dev.logo)} className="img-fluid" 
                    />
                    {/* </center> */}
                </div>
            </div>
        )}
     
    </Slider>
  );
}

const generateImageUrl = (url) => {
    if (!url) return '/images/thumb-placeholder.png';
    console.log("URL", url)
    const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.replace("1|", "")}`;
    var img = new Image();
    img.src = fullUrl;
    const isImageExist = img.height != 0;
    
    if (isImageExist) {
      return fullUrl
    } else {
      return '/images/thumb-placeholder.png';
    }
  }