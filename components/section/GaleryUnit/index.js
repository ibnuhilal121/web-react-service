import styles from "./galery_unit.module.scss";
import data_galery from "../../../sample_data/data_galery.json";
import ReactImageFallback from "react-image-fallback";
import ModalGalery from "./ModalGalery";
export default function GaleryUnit(props) {
  const {images = []} = props
  return (
    <div className={styles.galery}>
      <div className="row">
        <div className="col-md-9 mb-4">
          <div>
            <a data-bs-toggle="modal" data-bs-target="#modalGalery" href="#">
              <ReactImageFallback
                src={images[0] ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + images[0].replace('1|', '') : null}
                fallbackImage="/images/thumb-placeholder.png"
                className="img-fluid" 
                style={{height: 460, objectFit: "cover"}}
              />
            </a>
          </div>
        </div>
        <div className="col-md-3">
          <div className="row">
            <div className="col-md-12 mb-4" id="galeryunit">
              <div>
                <a data-bs-toggle="modal" data-bs-target="#modalGalery" href="#">
                  <ReactImageFallback
                      src={images[1] ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + images[1].replace('1|', '') : null}
                      fallbackImage="/images/thumb-placeholder.png"
                      className="img-fluid" 
                      style={{height: 215, objectFit: "cover"}}
                    />
                </a>
              </div>
            </div>
            { images.length > 2 ? (
              <div className="col-md-12 mb-4">
              <div className="position-relative">
                <a data-bs-toggle="modal" data-bs-target="#modalGalery" href="#">
                  <ReactImageFallback
                    src={images[2] ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + images[2].replace('1|', '') : null}
                    fallbackImage="/images/thumb-placeholder.png"
                    className="img-fluid" 
                    style={{height: 215, objectFit: "cover"}}
                  />
                  { images.length > 3 && (
                      <div className={styles.galery__more}>
                          <h6>+{images.length - 3}</h6>
                      </div>)
                  }
                </a>
              </div>
            </div>
            ) : null}
          </div>
        </div>
      </div>
      {/* MODAL */}
      <ModalGalery images={images}/>
      
    </div>
  );
}
