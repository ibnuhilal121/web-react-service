/* eslint-disable react/jsx-key */
import React from "react";
import ReactImageFallback from "react-image-fallback";
import Slider from "react-slick";
import styles from "./galery_unit.module.scss";

export default function ModalGalery({images = []}) {
  // console.log("+++ images",images);
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true
  };
  return (
    <div
      className="modal fade "
      id="modalGalery"
      aria-hidden="true"
      aria-labelledby="modalGalery"
      tabIndex="-1"
    >
      <div
        className="modal-dialog modal-auth"
        style={{ marginTop: 58, marginBottom: 57, maxWidth: "50%" }}
      >
        <div className="modal-content" style={{ backgroundClip: "transparent" }}>
          <div style={{ borderRadius: "20px" }}>
            <div className={styles.sliderContainer}> 
            <Slider {...settings}>
              {images.map((item, index)=>{
                  return (
                      <ReactImageFallback
                        src={item ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + item.replace('1|', '') : null}
                        fallbackImage="/images/thumb-placeholder.png"
                        className="img-fluid" 
                      />
                  )
              })}
            </Slider>
            </div> 
          </div>
        </div>
      </div>
    </div>
  );
}
