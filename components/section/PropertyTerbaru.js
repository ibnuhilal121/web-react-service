import MinimalProperty from "../data/MinimalProperty";
import data_terbaru from "../../sample_data/data_property_terbaru";
import Link from "next/link";
import PropertySlide from "../carousel/PropertySlide";

export default function PropertyTerbaru({ listProperti }) {
  // console.log('list properti terbaru : ', listProperti)
  return (
    <section id="listing_terbaru">
      <div className="container">
        <div className="header_listing">
          <h4 className="fw-bold" style={{ fontFamily: "futuraBT" }}>
            Listingan terbaru
          </h4>
          <Link href="/property?sort=2">
            <a className="d-none d-sm-none d-md-block">
              <span>Lihat Semua </span>
              <img src="/images/icons/chevron-right.svg" />
            </a>
          </Link>
        </div>
        <div className="list_property">
          <PropertySlide data_all={listProperti} />
        </div>
        <Link href="/property?sort=2">
          <a className="btn btn-outline-primary d-block d-sm-block d-md-none fw-bold">
            Lihat Semua
          </a>
        </Link>
      </div>
    </section>
  );
}
