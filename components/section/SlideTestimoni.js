import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import React from "react";
import Slider from "react-slick";

import data_testimoni from "../../sample_data/data_testimoni";

export default function SlideTestimoni({data}) {
  var settings = {
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Slider {...settings}>
      {data.map((data, index) => (
        <div key={index}>
          <div className="item_testimoni text-center">
            <center>
              <img src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.g_memb.replace("1|", "")}`} className="img-fluid avatar" />
            </center>
            <div className="content">
              <center>
                <img
                  src="/images/icons/quote.png"
                  className="img-fluid mt-3 mb-2"
                />
              </center>
              {data.tstmoni}
            </div>
            <div className="mitra">{data.pkrjn}</div>
            <div className="name">{data.n_memb}</div>
          </div>
        </div>
      ))}
    </Slider>
  );
}
