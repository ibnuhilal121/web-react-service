import next from "next";
import React, { useEffect, useState, useRef } from "react";
import { useForm } from "react-hook-form";
import NumberFormat from "react-number-format";
import { useAppContext } from "../../context";
import styles from "../../pages/ajukan_kpr/Credit.module.scss";
import { useMediaQuery } from "react-responsive";
import { useRouter } from "next/router";
import { defaultHeaders } from "../../utils/defaultHeaders";
import { generateKey } from "../../helpers/generateKey";

export default function CreditStepOne(props = {}) {
  const { userKey, userProfile } = useAppContext();
  const router = useRouter();
  const isTabletOrDesktop = useMediaQuery({ query: `(min-width: 900px)` });
  const isSmallDevice = useMediaQuery({ query: `(max-width: 912px)` });
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const { branches, dataPengajuan, percent, tipeKPR, setDataPengajuan, jenisKPR, setLoadingStep, setKPRid, setShowPercent, KPRid, idKavling } = props;
  const [validated, setValidated] = useState(false);
  const [showErr, setShowErr] = useState({
    cabang: false,
    jenis: false,
    harga: false,
    uangMuka: false,
    pengajuan: false,
    waktu: false,
    metode: false,
  });

  useEffect(() => {
    console.log("~ updated data pengajuan : ", dataPengajuan);
    let temp = true;
    for (let data in dataPengajuan) {
      if (!dataPengajuan[data] && data !== "pengajuan") {
        temp = false;
      }
    }
    setValidated(temp);
  }, [dataPengajuan]);

  useEffect(() => {
    if (validated) {
      if (percent < 25) {
        setShowPercent(25);
      }
    }
  }, [validated]);

  const createPengajuan = () => {
    let errTemp = { ...showErr };
    let readyToSubmit = true;
    for (let data in dataPengajuan) {
      if (!dataPengajuan[data]) {
        errTemp[data] = true;
        readyToSubmit = false;
      }
    }
    setShowErr(errTemp);
  };

  const handlePressEnter = (e) => {
    if (e.key === "Enter") {
      createPengajuan();
    }
  };

  const handleBlur = (category, value) => {
    let errTemp = { ...showErr };
    if (value) {
      errTemp[category] = false;
    } else {
      errTemp[category] = true;
    }
    setShowErr(errTemp);
  };

  const nextStep = (e) => {
    e.preventDefault();
    let objTemp = { ...dataPengajuan };
    for (let value in objTemp) {
      if (objTemp[value]) {
        switch (value) {
          case "harga":
            const tempHarga = objTemp[value].replace("Rp. ", "").replaceAll(".", "");
            objTemp[value] = tempHarga;
            break;
          case "uangMuka":
            const tempMuka = objTemp[value].replace("Rp. ", "").replaceAll(".", "");
            objTemp[value] = tempMuka;
            break;
          case "pengajuan":
            const tempPengajuan = objTemp[value].replace("Rp. ", "").replaceAll(".", "");
            objTemp[value] = tempPengajuan;
            break;
          default:
            break;
        }
      }
    }
    localStorage.setItem("dataPengajuan", JSON.stringify(objTemp));
    localStorage.setItem("jenisKPR", JSON.stringify(tipeKPR));
    submitPengajuan();
  };

  useEffect(()=>{
    changeUangMuka(dataPengajuan.uangMuka);
  },[])

  const changeData = (type, value) => {
    let temp = { ...dataPengajuan };

    if (["harga", "pengajuan"].includes(type) && !temp[type] && value.includes("0")) {
      temp[type] = "";
    } else {
      if (["harga"].includes(type) && !value.replaceAll(" ", "")) {
        temp["pengajuan"] = "";
      } else if (type === "harga") {
        if (Number(value.replaceAll(" ", "").replaceAll(".", "")) >= Number(dataPengajuan.uangMuka?.replaceAll(" ", "").replaceAll(".", ""))) {
          temp["pengajuan"] = (Number(value.replaceAll(" ", "").replaceAll(".", "")) - Number(dataPengajuan.uangMuka?.replaceAll(" ", "").replaceAll(".", ""))).toString();
        } else {
          temp["pengajuan"] = "";
        }
      }
      temp[type] = value;
    }
    setDataPengajuan(temp);
  };

  const changeUangMuka = (input) => {
    let value = input.replaceAll(' ', '');
    let temp = { ...dataPengajuan };
    if (value === "") {
      temp["uangMuka"] = "0";
    } else if(temp["uangMuka"].replaceAll(' ', '') == 0) {
      if(+value >= 10 ){
        temp["uangMuka"] = `${+value/10}`;
      }
      else if (+value>0 && +value < 10) {
        value = value.toString()[1];
        temp["uangMuka"] = `${value}`;
      }        
    } else {
      temp["uangMuka"] = `${value.replace(/[^0-9]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
    }

    if (dataPengajuan.harga) {
      if (Number(dataPengajuan.harga.replaceAll(" ", "").replaceAll(".", "")) >= Number(temp["uangMuka"].replaceAll(".", "").replaceAll(" ", ""))) {
        temp["pengajuan"] = (Number(dataPengajuan.harga.replaceAll(" ", "").replaceAll(".", "")) - Number(temp["uangMuka"].replaceAll(".", "").replaceAll(" ", ""))).toString();
      } else {
        temp["pengajuan"] = "";
      }
    }
    setDataPengajuan(temp);
  };

  const getDeviceType = () => {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return "web tablet";
    }
    if (/Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(ua)) {
      return "web mobile";
    }
    return "web desktop";
  };

  const isIncludeLetterE = (word) => {
    return word.toString().toLowerCase().includes("e")
  }
  const submitPengajuan = async () => {
    setLoadingStep(true);

    const payload = {
      type_pengajuan: tipeKPR,
      jenis_kredit: dataPengajuan.jenis,
      id_cabang: dataPengajuan.cabang,
      nilai_pengajuan: dataPengajuan.pengajuan.replaceAll("Rp. ", "").replaceAll(".", "").replaceAll(" ", ""),
      harga_properti: dataPengajuan.harga.replaceAll("Rp. ", "").replaceAll(".", "").replaceAll(" ", ""),
      jangka_waktu: Math.round(Number(dataPengajuan.waktu) * 12),
      uang_muka: dataPengajuan.uangMuka.replaceAll("Rp. ", "").replaceAll(".", "").replaceAll(" ", ""),
      pembayaran_angsuran: dataPengajuan.metode,
      source: getDeviceType(),
      AccessKey_Member: userKey,
      id: KPRid,
      key: generateKey(userProfile.id)
    };
    console.log(isIncludeLetterE(1e3), isIncludeLetterE(payload.harga_properti), isIncludeLetterE(payload.uang_muka));
    if(isIncludeLetterE(payload.nilai_pengajuan) || isIncludeLetterE(payload.harga_properti) || isIncludeLetterE(payload.uang_muka)) {
      alert("Value Harga properti, uang muka, dan nilai pengajuan harus angka")
    } else {
      try {
        const submitData = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/nonstok/setpengajuan`, {
          method: "POST",
          headers: {
            // "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
            // AccessKey_Member: userKey,
          },
          body: new URLSearchParams(payload),
        });
        const responseSubmit = await submitData.json();
        if (!responseSubmit.status) {
          throw responseSubmit.message;
        } else {
          localStorage.setItem("kpr_id", responseSubmit.kpr_id);
          setKPRid(responseSubmit.kpr_id);
          props.setProgressCredit(25);
          props.setStepCredit(2);
          if (isSmallDevice) {
            router.push(`/ajukan_kpr/form?tipe=${tipeKPR}&id=${responseSubmit.kpr_id}`);
          } else {
            router.push(`/ajukan_kpr?id=${responseSubmit.kpr_id}`);
          }
        }
      } catch (error) {
        console.log("error set pengajuan", error);
      } finally {
        setLoadingStep(false);
      }
    }

    
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  return (
    <div className="form-custom">
      <p
        className="mb-0 ms-3 ms-lg-0 step-1"
        // style={{
        //   fontFamily: "Helvetica",
        //   fontWeight: "700",
        //   color: "#666666",
        // }}
      >
        Step 1
      </p>
      <h2 className={`mb-5 mt-2 ${styles.titleForm} ps-3 ps-lg-0`}>Data Pengajuan</h2>
      <form
        onSubmit={nextStep}
        // onSubmit={handleSubmit(nextStep)}
      >
        <div id="credit_step_input" id="pengajuanstep1" className="floating-label-wrap mt-5 px-3 px-lg-0" color="#666666">
          <select
            id={`input1 ${showErr.cabang ? "floatingInputInvalid" : ""}`}
            className={`floating-label-select custom-select-profile form-select w-100  ${errors.cabang ? "is-invalid " : ""} ${showErr.cabang ? "error-border" : ""}`}
            value={dataPengajuan.cabang}
            onChange={(e) => changeData("cabang", e.target.value)}
            disabled={!tipeKPR ? true : false}
            onKeyPress={handlePressEnter}
            onBlur={(e) => handleBlur("cabang", e.target.value)}
            required
            style={{ color: "#00193E",maxWidth:"480px" }}
          >
            <option value="">Kantor Cabang</option>
            {branches.map((branch, index) => {
              return (
                <option key={index} value={branch.id}>
                  {branch.cbg}
                </option>
              );
            })}
          </select>
          <label htmlFor="input1" className="floating-label px-3 px-lg-0">
            Kantor Cabang
          </label>
          {showErr.cabang && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian Tidak Boleh Kosong
            </div>
          )}
          <div id="input1" className="invalid-feedback">
            required.
          </div>
        </div>
        <div id="credit_step_input"  id="pengajuankredit" className="floating-label-wrap px-3 px-lg-0">
          <select
            id="input2"
            className={`floating-label-select custom-select-profile w-100 form-select${errors.jenis_kredit ? "is-invalid" : ""} ${showErr.jenis ? "error-border" : ""}`}
            value={dataPengajuan.jenis}
            onChange={(e) => changeData("jenis", e.target.value)}
            disabled={!tipeKPR ? true : false}
            onKeyPress={handlePressEnter}
            onBlur={(e) => handleBlur("jenis", e.target.value)}
            required
            style={{ color: "#00193E",maxWidth:"480px" }}
          >
            <option value="">{tipeKPR === "2"? 'Jenis Pembiayaan' : 'Jenis Kredit'}</option>
            {jenisKPR.map((jenis, index) => {
              return (
                <option key={index} value={jenis.kd}>
                  {jenis.nl}
                </option>
              );
            })}
          </select>
          <label htmlFor="input2" className="floating-label px-3 px-lg-0">
          {tipeKPR === "2"? 'Jenis Pembiayaan' : 'Jenis Kredit'}
          </label>
          {showErr.jenis && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian Tidak Boleh Kosong
            </div>
          )}
          <div id="input2" className="invalid-feedback">
            required.
          </div>
        </div>

        <div id="credit_step_input" id="pengajuanhrgprop" className="floating-label-wrap px-3 px-lg-0" style={{ position: "relative" }}>
          <NumberFormat
            isAllowed={(values) => {
              const { value } = values;
              return value[0] == "0" ? false : true;
            }}
            allowNegative={false}
            prefix="      "
            // prefix="Rp. "
            thousandSeparator="."
            decimalSeparator=","
            className={`floating-label-field border-active w-100 ${errors.harga_properti ? "is-invalid" : ""} ${showErr.harga ? "error-border" : ""} `}
            placeholder="Harga Properti"
            style={{ paddingLeft: "22px",maxWidth:"480px" }}
            value={dataPengajuan.harga}
            onChange={(e) => changeData("harga", e.target.value)}
            disabled={!tipeKPR ? true : false}
            onBlur={(e) => handleBlur("harga", e.target.value)}
            onKeyPress={handlePressEnter}
          />

          <label htmlFor="harga_property" className="floating-label px-3 px-lg-0" style={{ marginTop: "-6px" }}>
            Harga Properti
          </label>
          {dataPengajuan.harga && (
            <p
              style={{
                position: "absolute",
                top: "17px",
                marginLeft: "18px",
                fontFamily: "Helvetica",
                fontWeight: 700,
                color: "#00193E",
                fontSize: "14px",
              }}
            >
              Rp
            </p>
          )}
          {showErr.harga && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian Tidak Boleh Kosong
            </div>
          )}
        </div>
        <div id="credit_step_input" id="pengajuanuangmuka" className="floating-label-wrap px-3 px-lg-0" style={{ position: "relative" }}>
          <input 
            prefix="      "
            type={'text'}
            className={`floating-label-field border-active w-100 ${errors.uang_muka ? "is-invalid" : ""} ${showErr.uangMuka ? "error-border" : ""}`}
            placeholder={!tipeKPR && "Uang Muka"}
            value={tipeKPR && dataPengajuan.uangMuka}
            disabled={!tipeKPR ? true : false}
            style={ !tipeKPR ? { paddingLeft: "22px",maxWidth:"480px" }: { paddingLeft: "44px",maxWidth:"480px" }}
            onChange={(e) => changeUangMuka(`${e.target.value}`)}
          />
          {/* <NumberFormat
            prefix="      "
            allowNegative={false}
            thousandSeparator="."
            decimalSeparator=","
            className={`floating-label-field border-active ${errors.uang_muka ? "is-invalid" : ""} ${showErr.uangMuka ? "error-border" : ""}`}
            placeholder={!tipeKPR && "Uang Muka"}
            value={dataPengajuan.uangMuka}
            disabled={!tipeKPR ? true : false}
            allowLeadingZeros={false}
            onValueChange={(values) => {
              const { value } = values;
              changeUangMuka(value);
            }}
            onKeyPress={handlePressEnter}
            style={{ paddingLeft: "22px" }}
          /> */}
          <label htmlFor="uang_muka" className="floating-label px-3 px-lg-0" style={{ marginTop: "-6px" }}>
            Uang Muka
          </label>

          {tipeKPR && (
            <p
              style={{
                position: "absolute",
                top: "17px",
                marginLeft: "18px",
                fontFamily: "Helvetica",
                fontWeight: 700,
                color: "#00193E",
                fontSize: "14px",
              }}
            >
              Rp
            </p>
          )}
          {/* {showErr.uangMuka && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isian Tidak Boleh Kosong
            </div>
          )} */}
          {dataPengajuan.uangMuka && dataPengajuan.harga && Number(dataPengajuan.uangMuka.replaceAll(".", "").replaceAll(" ", "")) >= Number(dataPengajuan.harga.replaceAll(" ", "").replaceAll(".", "")) && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian harus kurang dari harga properti
            </div>
          )}
        </div>
        <div id="credit_step_input" id="pengajuannilai" className="floating-label-wrap px-3 px-lg-0" style={{ position: "relative" }}>
          <NumberFormat
            // isAllowed={(values) => {
            //   const { value } = values;
            //   return value[0] == "0" ? false : true;
            // }}
            allowNegative={false}
            prefix="      "
            // prefix="Rp. "
            thousandSeparator="."
            decimalSeparator=","
            className={`floating-label-field border-active w-100 ${
              errors.nilai_pengajuan ? "is-invalid" : ""
            } ${showErr.pengajuan ? "error-border" : ""}`}
            placeholder={
              dataPengajuan.uangMuka &&
              dataPengajuan.harga &&
              Number(dataPengajuan.harga.replaceAll(" ", "").replaceAll(".", "")) >=
              Number(dataPengajuan.uangMuka.replaceAll(".", "").replaceAll(" ", ""))
                ? ""
                : "Nilai Pengajuan"
            }
            value={dataPengajuan.pengajuan}
            // onChange={(e) => changeData("pengajuan", e.target.value)}
            disabled={true}
            // onBlur={(e) => handleBlur("pengajuan", e.target.value)}
            // onKeyPress={handlePressEnter}
            style={{ paddingLeft: "22px",maxWidth:"480px" }}
          />

          {dataPengajuan.uangMuka &&
            dataPengajuan.harga &&
            Number(dataPengajuan.harga.replaceAll(" ", "").replaceAll(".", "")) >=
            Number(dataPengajuan.uangMuka.replaceAll(".", "").replaceAll(" ", "")) && (
              <>
                <span
                  style={{
                    position: "absolute",
                    top: "-17px",
                    marginTop: "-6px",
                    fontFamily: "FuturaBT",
                    fontWeight: "700",
                  }}
                >
                  Nilai Pengajuan
                </span>
                <p
                  style={{
                    position: "absolute",
                    top: "17px",
                    marginLeft: "18px",
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    color: "#00193E",
                    fontSize: "14px",
                  }}
                >
                  Rp
                </p>
                {/* <p
                style={{
                  position: "absolute",
                  top: "17px",
                  right: "383px",
                  marginLeft: "18px",
                  fontFamily: "Helvetica",
                  fontWeight: 400,
                  color: "#666666",
                  fontSize: "14px",
                }}
              >
                {numberWithCommas(dataPengajuan.pengajuan)}
              </p> */}
            </>
          )}
          {showErr.pengajuan && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian Tidak Boleh Kosong
            </div>
          )}
        </div>

        <div id="credit_step_input" id="pengajuanjgkwaktu" className="floating-label-wrap px-3 px-lg-0">
          <NumberFormat
            isAllowed={(values) => {
              const { formattedValue, floatValue, value } = values;
              return (formattedValue === "" || (floatValue <= 30 && floatValue >= 1)) && !(value[0] == "0");
            }}
            className={`floating-label-field w-100 border-active${errors.jangka_waktu ? "is-invalid" : ""} ${showErr.waktu ? "error-border" : ""}`}
            placeholder="Jangka Waktu"
            value={dataPengajuan.waktu}
            onChange={(e) => changeData("waktu", e.target.value)}
            disabled={!tipeKPR ? true : false}
            onKeyPress={handlePressEnter}
            onBlur={(e) => handleBlur("waktu", e.target.value)}
            style={{maxWidth:"480px"}}
            // style={{ color: "#666" }}
          />
          <label htmlFor="jangka_waktu" className="floating-label px-3 px-lg-0" style={{maxWidth:"480px"}}>
            Jangka Waktu
            <label
              className="text-end px-3"
              id="jangkawaktuajukan"
              style={isTabletOrDesktop ? { left: '72%' } : {}}
              // style={{
              //   transform: "translateX(-118px)",
              //   fontFamily: "Helvetica",
              //   fontSize: "14px",
              //   fontWeight: "700px",
              //   marginTop: "15px",
              // }}
            >
              Tahun
            </label>
          </label>
          {showErr.waktu && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian Tidak Boleh Kosong
            </div>
          )}
        </div>

        <div id="credit_step_input" id="pengajuanpembayaran" className="floating-label-wrap px-3 px-lg-0">
          <select
            id="input7"
            className={`floating-label-select custom-select-profile form-select w-100 border-active${errors.cara_pembayaran ? "is-invalid" : ""} ${showErr.metode ? "error-border" : ""}`}
            value={dataPengajuan.metode}
            onChange={(e) => changeData("metode", e.target.value)}
            disabled={!tipeKPR ? true : false}
            onKeyPress={handlePressEnter}
            onBlur={(e) => handleBlur("metode", e.target.value)}
            style={{maxWidth:"480px", paddingBottom: 13}}
            required
          >
            <option value="">Cara Pembayaran Angsuran</option>
            <option value="10">Kolektif / Potong Gaji</option>
            <option value="20">AGF / Transfer</option>
            <option value="30">Payroll</option>
            <option value="40">ATM</option>
            <option value="50">Tunai-Loket</option>
            <option value="60">Kantor Pos</option>
            <option value="70">Lainnya</option>
          </select>
          <label htmlFor="input6" className="floating-label px-3 px-lg-0">
            Cara Pembayaran Angsuran
          </label>
          {showErr.metode && (
            <div className="error-input-kpr">
              <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
              Isian Tidak Boleh Kosong
            </div>
          )}
          <div id="input7" className="invalid-feedback">
            required.
          </div>
        </div>
        <button
          type="submit"
          className="btn btn-main m-3 m-lg-0"
          style={{
            width: 119,
            height: 48,
            padding: 0,
            fontFamily: "Helvetica",
            fontSize: 14,
            fontWeight: 700,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: "200px",
          }}
          disabled={validated && Number(dataPengajuan.uangMuka.replaceAll(".", "").replaceAll(" ", "")) < Number(dataPengajuan.harga?.replaceAll(" ", "").replaceAll(".", "")) ? false : true}
        >
          Selanjutnya
        </button>
      </form>
    </div>
  );
}
