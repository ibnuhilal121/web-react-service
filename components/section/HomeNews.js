import ItemBlogMinimal from "../data/ItemBlogMinimal";
import data_blog from '../../sample_data/data_blog_home'
import { doFetchBlogShow } from "../../services/homepage";
import { useEffect, useState } from "react";

export default function HomeNews() {

    const [loading, setLoading] = useState(false)
    const [dataBlog, setDataBlog] = useState([])

    // HANDLER FOR STATE
    function loadingHandler(bool) {
        setLoading(bool);
    }
    useEffect(() => {
        const limit = 3;
        const payload = {
            Sort: "t_pst DESC",
            Limit: limit,
            Page: null,
        }
        doFetchBlogShow(loadingHandler, payload)
            .then((response) => {
              console.log("+++ response blog", response);
                if (!response?.IsError) {
                //   console.log(response.Data);
                const dataToSet = [];
                response.Data.slice(0, limit).map((item)=>{
                    const itemToArr = {
                        id:item.id,
                        images : item.gmbr,
                        title : item.jdl,
                        kategori : item.n_kat,
                        date : item.t_ins,
                        content : item.rgksn,
                        seo: item.seo
                    };
                    dataToSet.push(itemToArr);
                })
               
                
               
                // console.log("+++ dataToSet",dataToSet);
                setDataBlog(dataToSet)
                console.log(dataBlog);
                }else{
                console.log(response.ErrToUser);
                }
            }).catch((err) => {
                // console.log(`Something wrong:${err}`);
                loadingHandler(false);
            });
    }, [])

    return (
        <section id="listing_news">
            <div className="container">
                
                <div  className="row list_property">
                    { dataBlog.map((data, index) =>
                        <div key={index}  className="col-md-4">
                            <ItemBlogMinimal data={data} />
                        </div>
                    )}
                </div>

            </div>
        </section>
    )
}
