import React, { useEffect, useState, useRef, useReducer } from "react";
import { useRouter } from "next/router";
import NumberFormat from "react-number-format";

export default function SearchForm(props){
    const router = useRouter();
    const { lokasi, tipe, budget, setBudget, setTipe, setLokasi, onSubmit } = props;

    const submitSearch = async () => {
        // e.preventDefault()
        console.log('onsubmit search')
        // router.push(`/property?lokasi=${lokasi}&tipeProperti=${tipe}&budget=${budget}`)
    }

    const handleKeyDown = (ev)=>{
        if(ev.keyCode ===13){ // enter button
            if (router.asPath.includes('komparasi=1')) {
                router.push(`/property/tipe?komparasi=1&lokasi=${lokasi}&tipeProperti=${tipe}&budget=${(budget.split(".").join("")).split("Rp ").join("")}`)
            } else {
                router.push(`/property?lokasi=${lokasi}&tipeProperti=${tipe}&budget=${(budget.split(".").join("")).split("Rp ").join("")}`)
            }
        }
    }

    useEffect(() => {
        console.log("budget ", (budget.split(".").join("")).split("Rp ").join(""))
    })

    return(
        <div onKeyDown={handleKeyDown} id="search_navbar_form" className="w-100 d-block flex-row">
            <form onSubmit={submitSearch} className="w-100 search-form">
                <div className="form-group">
                    <label>Lokasi</label>
                    <input 
                        placeholder="Daerah, kota atau kodepos"
                        value={lokasi}
                        onChange={setLokasi}
                        className="form-control"
                    />
                </div>
                <hr className="row"></hr>
                <div className="row">
                    <div className="col-6 form-group">
                        <label>Tipe</label>
                        <select 
                            id="kategori-profesional"
                            className="form-select"
                            placeholder="Tipe"
                            value={tipe}
                            onChange={setTipe}
                        >
                            <option value={""}>Semua Kategori</option>
                            <option value={1}>Rumah</option>
                            <option value={2}>Apartemen</option>
                            <option value={3}>Ruko</option>
                            <option value={4}>Kantor</option>
                        </select>
                    </div>
                    <div class="vr p-0"></div>
                    <div className="col-5 form-group">
                        <label>Budget</label>
                        <NumberFormat
                            allowNegative={false}
                            prefix="      "
                            prefix="Rp. "
                            thousandSeparator="."
                            decimalSeparator=","
                            value={budget}
                            onChange={setBudget}
                            disabled={false}
                            className="form-control"
                            placeholder="Kisaran harga"
                        />
                    </div>
                </div>
            </form>
        </div>
    )
}