import React, { useEffect, useState } from "react";
import data_terkait from "../../sample_data/data_blog_terkait";
import ItemBlogVertical from "../data/ItemBlogVertical";
import Link from "next/link";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import Slider from "react-slick";
import ItemNewsBlogHorizontal from "../data/ItemNewsBlogHorizontal";

export default function NewsTerkait({ kategori }) {
  const [news, setNews] = useState([]);
  const [currentSlide, setCurrentSlide] = useState(0);

  const PrevButton = (props) => {
    return (
      <div
        className="arrow-left"
        style={{ zIndex: 1, top: "245px", display: "inline-block" }}
      >
        <button
          type="button"
          {...props}
          class="btn slide_button "
          style={
            currentSlide === 0
              ? { backgroundColor: "#aaaaaa", pointerEvents: "none" }
              : {}
          }
        >
          {/* <i class="bi bi-arrow-left"></i> */}
          <div>
            <svg
              width="12"
              height="14"
              viewBox="0 0 12 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.0799999 6.61994C0.127594 6.49719 0.19896 6.38505 0.290001 6.28994L5.29 1.28994C5.38324 1.1967 5.49393 1.12274 5.61575 1.07228C5.73757 1.02182 5.86814 0.99585 6 0.99585C6.2663 0.99585 6.5217 1.10164 6.71 1.28994C6.80324 1.38318 6.8772 1.49387 6.92766 1.61569C6.97812 1.73751 7.00409 1.86808 7.00409 1.99994C7.00409 2.26624 6.8983 2.52164 6.71 2.70994L3.41 5.99994H11C11.2652 5.99994 11.5196 6.1053 11.7071 6.29283C11.8946 6.48037 12 6.73472 12 6.99994C12 7.26516 11.8946 7.51951 11.7071 7.70705C11.5196 7.89458 11.2652 7.99994 11 7.99994H3.41L6.71 11.2899C6.80373 11.3829 6.87812 11.4935 6.92889 11.6154C6.97966 11.7372 7.0058 11.8679 7.0058 11.9999C7.0058 12.132 6.97966 12.2627 6.92889 12.3845C6.87812 12.5064 6.80373 12.617 6.71 12.7099C6.61704 12.8037 6.50644 12.8781 6.38458 12.9288C6.26272 12.9796 6.13201 13.0057 6 13.0057C5.86799 13.0057 5.73728 12.9796 5.61542 12.9288C5.49356 12.8781 5.38296 12.8037 5.29 12.7099L0.290001 7.70994C0.19896 7.61484 0.127594 7.50269 0.0799999 7.37994C-0.0200176 7.13648 -0.0200176 6.8634 0.0799999 6.61994Z"
                fill="white"
              />
            </svg>
          </div>
        </button>
      </div>
    );
  };

  const NextButton = (props) => {
    return (
      <div
        className="arrow-right"
        style={{
          display: "inline-block",
          left: "97%",
          bottom: "313px",
        }}
      >
        <button
          type="button"
          {...props}
          class="btn slide_button"
          style={
            currentSlide === news.length - 4
              ? { backgroundColor: "#aaaaaa", pointerEvents: "none" }
              : {}
          }
        >
          {/* <i class="bi bi-arrow-right"></i> */}
          <div>
            <svg
              width="14"
              height="14"
              viewBox="0 0 12 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.92 6.61994C11.8724 6.49719 11.801 6.38505 11.71 6.28994L6.71 1.28994C6.61676 1.1967 6.50607 1.12274 6.38425 1.07228C6.26243 1.02182 6.13186 0.99585 6 0.99585C5.7337 0.99585 5.4783 1.10164 5.29 1.28994C5.19676 1.38318 5.1228 1.49387 5.07234 1.61569C5.02188 1.73751 4.99591 1.86808 4.99591 1.99994C4.99591 2.26624 5.1017 2.52164 5.29 2.70994L8.59 5.99994H1C0.734784 5.99994 0.48043 6.1053 0.292893 6.29283C0.105357 6.48037 0 6.73472 0 6.99994C0 7.26516 0.105357 7.51951 0.292893 7.70705C0.48043 7.89458 0.734784 7.99994 1 7.99994H8.59L5.29 11.2899C5.19627 11.3829 5.12188 11.4935 5.07111 11.6154C5.02034 11.7372 4.9942 11.8679 4.9942 11.9999C4.9942 12.132 5.02034 12.2627 5.07111 12.3845C5.12188 12.5064 5.19627 12.617 5.29 12.7099C5.38296 12.8037 5.49356 12.8781 5.61542 12.9288C5.73728 12.9796 5.86799 13.0057 6 13.0057C6.13201 13.0057 6.26272 12.9796 6.38458 12.9288C6.50644 12.8781 6.61704 12.8037 6.71 12.7099L11.71 7.70994C11.801 7.61484 11.8724 7.50269 11.92 7.37994C12.02 7.13648 12.02 6.8634 11.92 6.61994Z"
                fill="white"
              />
            </svg>
          </div>
        </button>
      </div>
    );
  };

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    adaptiveHeight: false,
    arrows: true,
    prevArrow: <PrevButton />,
    nextArrow: <NextButton />,
    afterChange: (i) => {
      setCurrentSlide(i);
    },
  };

  useEffect(() => {
    getNewsTerkait();
  }, [kategori]);

  const getNewsTerkait = async () => {
    const payload = {
      i_kat: kategori,
      Sort: "t_pst DESC",
    };

    try {
      const terkait = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/blog/show`,
        {
          method: "POST",
          headers: getHeaderWithAccessKey(),
          body: new URLSearchParams(payload),
        }
      );
      const responseTerkait = await terkait.json();
      if (responseTerkait.IsError) {
        throw responseTerkait.ErrToUser;
      } else {
        setNews(responseTerkait.Data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <section id="listing_populer" style={{ padding: "0px" }}>
      <div className="container">
        <div className="header_listing custom" style={news.length < 5 ? {top:0} : {}}>
          <h4>Artikel Terkait</h4>
          <Link href={`/blog?kategori=${kategori}`}>
            <a
              id="blog_web"
              className={{
                fontFamily: "Helvetica",
                fontWeight: "700",
                fontsize: "18px",
              }}
            >
              Lihat Semua{" "}
              <i
                style={{ fontSize: "13px" }}
                className="bi bi-chevron-right"
              ></i>
            </a>
          </Link>
        </div>

        {/* <div className="arrow-left m-0">
          <button type="button" class="btn slide_disabled ">
            <i class="bi bi-arrow-left"></i>
          </button>
        </div> */}
        <div id="blog_web">
          <Slider {...settings}>
            {news.map((data, index) => (
              <div key={index} className="col-md-3">
                <ItemBlogVertical data={data} />
              </div>
            ))}
          </Slider>
        </div>
        <div id="blog_mobile">
          <div id="news-Blog-Slide">
            {news.map((data, index) => (
              <div key={index} className="">
                <ItemNewsBlogHorizontal data={data} />
              </div>
            ))}
          </div>
        </div>
        {/* <div className="d-flex flex-wrap list_property m-0">
        </div> */}
        {/* <div className="arrow-right">
          <button type="button" class="btn slide_button">
            <i class="bi bi-arrow-right"></i>
          </button>
        </div> */}
      </div>
      <Link href={`/blog?kategori=${kategori}`}>
        <button
          className="btn btn-outline-main"
          id="mobileView"
          style={{
            width: "94%",
            fontFamily: "Helvetica",
            fontWeight: "700",
            fontsize: "18px",
            margin: "12px 12px 80px",
          }}
        >
          Lihat Semua{" "}
        </button>
      </Link>
    </section>
  );
}
