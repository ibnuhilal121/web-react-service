import React, { useEffect, useState } from "react";
import PaginationNew from "../../data/PaginationNew";
import SearchProperty from "../../form/SearchProperty";
import NavProperty from "../../static/NavProperty";
import qs from "qs";
import { useMediaQuery } from "react-responsive";

const LayoutPencarianProperti = ({
  children,
  queryObj,
  handleSubmit,
  paginationLength,
  onChangePage,
  currentPage,
  tab,
  noResult,
  view,
  setView,
  onFoundCoords
}) => {
  const [filterData, setFilterData] = useState({});
  const iPad = useMediaQuery({ query: `(min-width: 769px) and (max-width: 803px)` });
  function handleChangeFilter(data) {
    // console.log("~ data", data);
    if (data.value != null) {
      setFilterData({ ...filterData, [data.key]: data.value });
      handleSubmit({ ...filterData, [data.key]: data.value });
    } else {
      const newFilterData = { ...filterData };
      delete newFilterData[data.key];
      setFilterData(newFilterData);
    }
  }
  function handleChangeLokasi(e) {
    setFilterData((f) => ({
      ...f,
      lokasi: e.target.value,
    }));
  }
  function onClickSearch(keyword) {
    handleSubmit(filterData);
  }
  useEffect(() => {
    // console.log("~ filterData", filterData);
  }, [filterData]);
  useEffect(() => {
    // console.log("~ queryObj", queryObj);
    setFilterData(queryObj);
  }, [queryObj]);
  return (
    <div className="row" style={iPad ? { marginTop: '30px' } : {}}>
      <div className="col-md-0 col-lg-3 filter-container">
        <SearchProperty handleChange={handleChangeFilter} values={filterData} />
      </div>
      <div className="col-md-12 col-lg-9 content-container">
        <NavProperty
          type={tab}
          view={view === "map" ? "map" : "grid"}
          handleClickSearch={onClickSearch}
          lokasi={filterData.lokasi}
          onChangeLokasi={handleChangeLokasi}
          setView={setView}
          onFoundCoords={onFoundCoords}
        />
        {children}
        {!noResult && (
          <PaginationNew
            current={currentPage}
            length={paginationLength}
            onChangePage={onChangePage}
          />
        )}
      </div>
    </div>
  );
};

export default LayoutPencarianProperti;
