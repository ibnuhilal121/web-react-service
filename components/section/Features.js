import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
import { useState } from "react";
import { doFetchFeatures } from "../../services/konten";
import { getGenerateLinkPreApproval } from "../../services/konten";
import { useAppContext } from "../../context";
import Modal from "./Modal";
import { useRouter } from "next/router";
export default function Features() {
  const [loading, setLoading] = useState(false);
  const [features, setFeatures] = useState([]);
  const { userKey } = useAppContext();
  const history = useRouter();
  const [showModal, setShowModal] = useState(false);

  function loadingHandler(bool) {
    setLoading(bool);
  }

  const redirect = (res) => {
    localStorage.setItem("navigate", res.navigateWeb);
    localStorage.setItem("title", res.title);
    localStorage.getItem("generate-url");
    // console.log([res.title === "Virtual Expo" && process.env.NEXT_PUBLIC_ENABLE_VIRTUAL_EXPO === 'false',
    // res.title === "Home Service" && process.env.NEXT_PUBLIC_ENABLE_HOME_SERVICE === 'false', res.title === "Jasa Profesional" &&
    // process.env.NEXT_PUBLIC_ENABLE_JASA_PROFESIONAL === 'false']);
    if (res.title === "Virtual Expo" && !res.isShowing) {
      setShowModal(true);
      return;
    } else if ( res.title === "Home Service" && !res.isShowing) {
      setShowModal(true);
      return;
    } else if (res.title === "Jasa Profesional" && !res.isShowing ) {
      setShowModal(true);
      return;
    } else if (res.title === "Kredit Komersial & UMKM" && !res.isShowing ) {
      setShowModal(true);
      return;
    }
    const title = localStorage.getItem("title");
    // navigate
    history.push(
        !userKey && res.isAuth
        ? ""
        : title == "Pre Approval"
        ? res.navigateWeb + "/link/" + localStorage.getItem("generate-url")
        // ? console.log(res.navigateWeb + "/link/" + localStorage.getItem("generate-url"))
        : title == "Virtual Expo"
        ? res.navigateWeb + localStorage.getItem("generate-url")
        : title == "Home Service"
        ? localStorage.getItem("url-home-services")
        : title == "Jasa Profesional"
        ? localStorage.getItem("url-profesiona-listing")
        : res.navigateWeb
    );

    // if (!userKey && res.isAuth) {
    //   history.push("")
    // } else if(title == "Pre Approval"){
    //   history.push(res.navigateWeb + "link" + localStorage.getItem("key"))
    // } else{
    //   history.push(res.navigateWeb)
    // }
  };

  useEffect(() => {
    doFetchFeatures(loadingHandler)
      .then((res) => {
        // console.log("ini data respon fitur " +  res);
        // const dataFitur = [];
        // dataFitur.push(res);
        // console.log(JSON.stringify(res));
        setFeatures(res);
      })
      .catch((err) => {
        console.log("ini error fitur" + err);
      });
  }, []);

  console.log("Ini respon array" + features.length);
  
  return (
    <div>
            <Modal
                onClose={() => setShowModal(false)}
                show={showModal}
            >
                Hello from the modal!
            </Modal>
      <section id="home_features">
        <div className="container">
          <div id="feature_content">
            <div className="row d-flex align-items-center">
              <div className="col-12 col-md-4 title_section">
                <img src="/images/bg/bg_icon.svg" className="acc img-fluid" />
                <h4>Siap untuk memiliki rumah impian?</h4>
                <img src="/images/acc/title.svg" className="img-fluid" />
                <div className="desc">
                  Kami dapat membantu kamu menemukan yang kamu inginkan.
                </div>
              </div>
              <div className="col-12 col-md-8">
                <div className="row">
                  {features.map((res) => (
                    <div className="col-md-4 col-4">
                      {/* <Link href={!userKey && res.isAuth ? "" : res.navigateWeb}> */}
                      {res.title == 'Pre Approval' ? 
                      <a style={{ cursor: "pointer" }} href={res.navigateWeb + "/link/" + localStorage.getItem("generate-url")}>
                      <a
                            data-bs-toggle={
                              !userKey && res.isAuth && res.isShowing? "modal" : null
                            }
                            data-bs-target={
                              !userKey && res.isAuth && res.isShowing? "#modalLogin" : null
                            }
                            style={{
                              fontFamily: "Helvetica",
                            }}
                          >
                            {/* <Modal
                                onClose={() => setShowModal(false)}
                                show={showModal}
                            >
                                Hello from the modal!
                            </Modal> */}
                            <div className="item_feature">
                              <div className="image">
                                <Image
                                  src={res.iconUrl}
                                  className="icon"
                                  width="48"
                                  height="48"
                                />
                              </div>
                              <h6>{res.title}</h6>
                              <p>{res.desc}</p>
                            </div>
                          </a>
                      </a>    
                      : <a style={{ cursor: "pointer" }} onClick={() => redirect(res)}>
                        <a
                            data-bs-toggle={
                              !userKey && res.isAuth && res.isShowing? "modal" : null
                            }
                            data-bs-target={
                              !userKey && res.isAuth && res.isShowing? "#modalLogin" : null
                            }
                            style={{
                              fontFamily: "Helvetica",
                            }}
                          >
                            {/* <Modal
                                onClose={() => setShowModal(false)}
                                show={showModal}
                            >
                                Hello from the modal!
                            </Modal> */}
                            <div className="item_feature">
                              <div className="image">
                                <Image
                                  src={res.iconUrl}
                                  className="icon"
                                  width="48"
                                  height="48"
                                />
                              </div>
                              <h6>{res.title}</h6>
                              <p>{res.desc}</p>
                            </div>
                          </a>
                      </a> }

                      {/* <a
                        style={{ cursor: "pointer" }}
                        onClick={() => redirect(res)}
                        // href={res.navigateWeb}
                      > */}
                        
                          {/* <a
                            data-bs-toggle={
                              !userKey && res.isAuth && res.isShowing? "modal" : null
                            }
                            data-bs-target={
                              !userKey && res.isAuth && res.isShowing? "#modalLogin" : null
                            }
                            style={{
                              fontFamily: "Helvetica",
                            }}
                          > */}
                            {/* <Modal
                                onClose={() => setShowModal(false)}
                                show={showModal}
                            >
                                Hello from the modal!
                            </Modal> */}
                            {/* <div className="item_feature">
                              <div className="image">
                                <Image
                                  src={res.iconUrl}
                                  className="icon"
                                  width="48"
                                  height="48"
                                />
                              </div>
                              <h6>{res.title}</h6>
                              <p>{res.desc}</p>
                            </div>
                          </a> */}
                        
                      
                      {/* </a> */}
                    </div>
                  ))}
                  {/*  */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
