// import data_konsultasi from '../../sample_data/data_konsultasi'
import ItemKonsultasiPertanyaan from "../data/ItemKonsultasiPertanyaan";
import Pagination from "../data/Pagination";
import PaginationNew from "../data/PaginationNew";
import SearchIcon from '../../components/element/icons/SearchIcon';

export default function KonsultasiList({
  data,
  paging,
  startRange,
  setPage,
  endRange,
  getKonsultasi,
}) {
  const data_konsultasi = data;

  const NotFound = () => {
    return (
      <div
        style={{
          paddingTop: 50,
          paddingBottom: 50,
          textAlign: "center",
        }}
      >
        <SearchIcon />
        <div
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "16px",
            lineHeight: "150%",
            marginTop: 40,
          }}
        >
          Data yang dicari tidak ditemukan
        </div>
      </div>
    );
  };

  return (
    <section>
      <div className="container">
        <div className="row justify-content-center ">
          <div className="col-md-10 ">
            <div className="row list_property">
              {data_konsultasi.length > 0 ? data_konsultasi.map((data, index) => (
                <div
                  key={index}
                  className="col-md-12s"
                  id="accordionKonsultasi"
                >
                  <ItemKonsultasiPertanyaan
                    data={data}
                    getKonsultasi={getKonsultasi}
                  />
                </div>)) :
                NotFound()
              }

              <div className="col-12">
                <PaginationNew
                  length={paging?.jumlahHalTotal}
                  current={Number(paging?.halKe)}
                  onChangePage={(e) => {
                    setPage(e);
                    window.scrollTo(0, 0);
                  }}

                  // paging={paging}
                  // startRange={startRange}
                  // setPage={setPage}
                  // endRange={endRange}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
