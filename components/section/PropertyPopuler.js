import MinimalProperty from "../data/MinimalProperty";

import data_populer from "../../sample_data/data_property_populer";
import Link from "next/link";
import PropertySlide from "../carousel/PropertySlide";
import { useEffect, useState } from "react";

import { doFetchAreaSekitar } from "../../services/homepage";

export default function PropertyPopuler({ tipeRumah }) {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  // HANDLER FOR STATE
  // function loadingHandler(bool) {
  //   setLoading(bool);
  // }
  // useEffect(() => {
  //   const limit = 3;
  //   const payload = {
  //     id: null,
  //     Sort: null,
  //     Limit: limit,
  //     Page: null,
  //   };
  //   doFetchAreaSekitar(loadingHandler, payload)
  //     .then((response) => {
  //       console.log("+++ response", response);
  //       if (!response?.IsError) {
  //         //   console.log(response.Data);
  //         const dataToSet = response.Data.slice(0, limit);
  //         // console.log("+++ dataToSet",dataToSet);
  //         setData(dataToSet);
  //       } else {
  //         console.log(response.ErrToUser);
  //       }
  //     })
  //     .catch((err) => {
  //       // console.log(`Something wrong:${err}`);
  //       loadingHandler(false);
  //     });
  // }, []);

  return (
    <section id="listing_populer">
      <div className="container">
        <div className="header_listing">
          <h4 className="fw-bold" style={{ fontFamily: "futuraBT" }}>
            Paling banyak dilihat oleh sekitarmu
          </h4>

          <Link href="/property?sort=1">
            <a className="d-none d-sm-none d-md-block">
              <span className="fw-bold">Lihat Semua</span>
              <img src="/images/icons/chevron-right.svg" />
            </a>
          </Link>
        </div>
        <div className="list_property">
          <PropertySlide data_all={tipeRumah} />
        </div>
        <Link href="/property?sort=5">
          <a className="btn btn-outline-primary d-block d-sm-block d-md-none fw-bold">
            Lihat Semua
          </a>
        </Link>
      </div>
    </section>
  );
}
