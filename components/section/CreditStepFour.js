import { useEffect, useRef, useState } from "react";
import axios from "axios";
import styles from "../../pages/ajukan_kpr/Credit.module.scss";
import styles_modal from "../../components/element/modal_confirmation/modal_confirmation.module.scss";
import IconEdit from "../element/icons/icon_edit";
import { useAppContext } from "../../context";
import { continueKPRStok, getCountdown } from "../../services/ajukanKPR";
import Lottie from "react-lottie";
import Link from "next/link";
import * as animationData from "../../public/succes_animation.json"
import {
  getCabangStok,
  getProvince,
  getKota,
  getKecamatan,
  getKelurahan,
} from "../../services/master";
import Otp from '../../components/popup/Otp'
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function CreditStepFour(props) {
  const { userKey, userProfile } = useAppContext();
  const [allData, setAllData] = useState({});
  const [branches, setBranches] = useState([]);
  const [provinsi, setProvinsi] = useState([]);
  const [kota, setKota] = useState([]);
  const [kecamatan, setKecamatan] = useState([]);
  const [kelurahan, setKelurahan] = useState([]);
  const [modalOtp, setModalOtp] = useState(false)
  const otpRef = useRef(null)

  const jenisKpr = props.KPRid?.substring(0, 5)

  const validatingNegativeValue = () => {
    if(jenisKpr === "KPRST") {
      return (allData?.HARGA_JUAL < 0 || allData?.UANG_MUKA < 0 || allData?.NILAI_PENGAJUAN < 0)
    } else if(jenisKpr === "KPRNS") {
      return (allData?.hargaProperti < 0 || allData?.uangMuka < 0 || allData?.nl_pgjn < 0)
    }
  }

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
   const getJangkaWaktu = (n) => {
    var tahun = Math.floor(n/12);
    var bulan = n % 12;

    tahun = tahun > 0 ? (tahun + " Tahun ") : ""
    bulan = bulan > 0 ? bulan + " Bulan" : ""
    console.log({tahun, bulan});

    return tahun + bulan
   }
  useEffect(() => {
    if (props.KPRid) {
      if (props.KPRid?.substring(0, 5) === "KPRST") {
        props.setLoadingStep(true);
        const arrPromises = [
          continueKPRStok(userKey, props.KPRid),
          getCabangStok(props.tipeKPR),
        ];
        Promise.all(arrPromises)
          .then((res) => {
            const [resRingkasan, resCabang] = res;
            setAllData(resRingkasan);
            setBranches(resCabang);

            const domisiliPromises = [
              getProvince(),
              getKota(resRingkasan.ID_PROPINSI),
              getKecamatan(resRingkasan.ID_KAB_KOTA),
              getKelurahan(resRingkasan.ID_KECAMATAN),
            ];
            return Promise.all(domisiliPromises);
          })
          .then((res) => {
            console.log("~ domisili response : ", res);
            const [
              responseProvinsi,
              responseKota,
              responseKecamatan,
              responseKelurahan,
            ] = res;
            setProvinsi(responseProvinsi.Data);
            setKota(responseKota.Data);
            setKecamatan(responseKecamatan.Data);
            setKelurahan(responseKelurahan.Data);
          })
          .catch((err) => {})
          .finally(() => props.setLoadingStep(false));
      } else {
        getRingkasanData();
      }
    }
  }, [props.KPRid]);
  console.log({allData});

  const getRingkasanData = async () => {
    props.setLoadingStep(true);
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/nonstok/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: userKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          id: props.KPRid,
        }),
      });
      const resData = await res.json();
      if (!resData.IsError) {
        setAllData(resData.Data[0]);
        setFotoKtp(generateImageUrl(resData.Data[0].fl_ktp.split("|")[1] || resData.Data[0].FILE_KTP.split("|")[1]))
        setPasfoto(generateImageUrl(resData.Data[0].fl_pasFoto.split("|")[1] || resData.Data[0].FILE_PAS_FOTO?.split("|")[1]))
        setSlipFoto(generateImageUrl(resData.Data[0].fl_slp_pghsln.split("|")[1] || resData.Data[0].FILE_SLIP_PENGHASILAN?.split("|")[1]))
        setRekKoran(generateImageUrl(resData.Data[0].fl_rek_koran.split("|")[1] ||resData.Data[0].FILE_REK_KORAN?.split("|")[1]))
        console.log("GET DATA", resData.Data[0]);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
    } finally {
      props.setLoadingStep(false);
    }
  };

  const generateCabang = (id) => {
    for (let i = 0; i < branches.length; i++) {
      if (branches[i].id == id) {
        return branches[i].cbg;
      }
    }
  };

  const generateProvinsi = (id) => {
    for (let i = 0; i < provinsi.length; i++) {
      if (provinsi[i].id == id) {
        return provinsi[i].n;
      }
    }
  };

  const generateKota = (id) => {
    for (let i = 0; i < kota.length; i++) {
      if (kota[i].id == id) {
        return kota[i].n;
      }
    }
  };

  const generateKecamatan = (id) => {
    for (let i = 0; i < kecamatan.length; i++) {
      if (kecamatan[i].id == id) {
        return kecamatan[i].n;
      }
    }
  };

  const generateKelurahan = (id) => {
    for (let i = 0; i < kelurahan.length; i++) {
      if (kelurahan[i].id == id) {
        return kelurahan[i].n;
      }
    }
  };

  const tipePembayaran = (id) => {
    switch (id) {
      case "10":
        return "Kolektif / Potong Gaji";
      case "20":
        return "AGF / Transfer";
      case "30":
        return "Payroll";
      case "40":
        return "ATM";
      case "50":
        return "Tunai-Loket";
      case "60":
        return "Kantor Pos";
      case "70":
        return "Lainnya";
      default:
        break;
    }
  };

  const statusNikah = (id) => {
    if(id == 0 || "0"){
      return "Belum Menikah"
    }
    switch (id) {
      
      case 1 || "1":
        return "Sudah Menikah";
      case 10 || "10":
        return "Sudah Menikah";
      case 20 || "20":
        return "Belum Menikah";
      case 30 || "30":
        return "Duda / Janda";
      case 50 || "50":
        return "Cerai";
      case 60 || "60":
        return "Lainnya";
      default:
        break;
    }
  };

  const jenisKredit = (id) => {
    if (props.tipeKPR === "1") {
      switch (id) {
        case "1":
          return "KPR / KPA Baru";
        case "2":
          return "Kredit Angunan Rumah";
        case "3":
          return "KPR Second";
        case "4":
          return "KPR Take Over";
        case "5":
          return "Kredit Ringan";
        case "6":
          return "KPR GAEESSS";
        case "7":
          return "Kredit Agunan Rumah (KAR Isi Ulang)";
        default:
          break;
      }
    } else if (props.tipeKPR === "2") {
      switch (id) {
        case "1":
          return "KPR BTN PLATINUM iB MAX";
        case "2":
          return "KPR BTN INDENT iB";
        case "3":
          return "KPR BTN SEJAHTERA iB";
        case "4":
          return "BANGUN RUMAH BTN iB";
        default:
          break;
      }
    }
  };

  return (
    <div
      style={{
        paddingTop: "32px",
      }}
    >
      <p
        className="mb-0"
        style={{
          fontFamily: "Helvetica",
          fontWeight: "700",
          color: "#666666",
        }}
      >
        Step 4
      </p>
      <h2
        style={{
          fontSize: "32px",
          fontFamily: "FuturaBT",
          fontStyle: "normal",
          fontWeight: "700",
          lineHeight: "48px",
          letterSpacing: "0px",
          marginBottom: "32px",
        }}
        className=" mt-0"
      >
        Ringkasan
      </h2>
      <h2
        style={{
          fontSize: "20px",
          lineHeight: "18.2px",
          color: "#00193E",
          fontFamily: "FuturaBT",
          fontStyle: "normal",
          fontWeight: "700",
        }}
        className="mt-3 mb-4 pb-2 mt-2"
      >
        Pengisian Data
      </h2>
      <div className={`mb-4 ${styles.titleStep}`}>
        <h6
          style={{
            fontSize: "14px",
            lineHeight: "18.2px",
            color: "#00193E",
            fontFamily: "FuturaBT",
            fontStyle: "normal",
            fontWeight: "700",
            marginBottom: "16px",
          }}
        >
          Data Pengajuan
        </h6>{" "}
        {props.KPRid?.substring(0, 5) === "KPRST" ? null : (
          <div
            onClick={() => props.setStepCredit(1)}
            className="cursor-pointer"
          >
            <IconEdit />
          </div>
        )}
      </div>
      <div className="row detail_ringkasan" style={{ marginBottom: "18px" }}>
        <div className="col-6 mb-2">
          <h6>Kantor Cabang</h6>
          <p className="value">
            {allData?.n_cbg || generateCabang(allData?.ID_CABANG)}
          </p>
        </div>

        <div className="col-6 mb-2">
          <h6>{props.tipeKPR === "2"? 'Jenis Pembiayaan' : 'Jenis Kredit'}</h6>
          <p className="value">
            {jenisKredit(
              allData?.jns_krdt || allData?.jns_pmbyn || allData?.SIFAT_KREDIT
            )}
          </p>
        </div>
        <div className="col-6 mb-2">
          <h6>Harga Properti</h6>
          <p className="value">
            Rp {allData?.hargaProperti
              ?.toString()
              .replace(/\B(?=(\d{3})+(?!\d))/g, ".") ||
              allData?.HARGA_JUAL?.toString().replace(
                /\B(?=(\d{3})+(?!\d))/g,
                "."
              )}
          </p>
        </div>

        <div className="col-6 mb-2">
          <h6>Uang Muka</h6>
          <p className="value">
            Rp {allData?.uangMuka
              ?.toString()
              .replace(/\B(?=(\d{3})+(?!\d))/g, ".") ||
              allData?.UANG_MUKA?.toString().replace(
                /\B(?=(\d{3})+(?!\d))/g,
                "."
              )}
          </p>
        </div>
        <div className="col-12 mb-2">
          <h6>Nilai Pengajuan</h6>
          <p className="value">
            Rp {allData?.nl_pgjn
              ?.toString()
              .replace(/\B(?=(\d{3})+(?!\d))/g, ".") ||
              allData?.NILAI_PENGAJUAN?.toString().replace(
                /\B(?=(\d{3})+(?!\d))/g,
                "."
              )}
          </p>
        </div>

        <div className="col-6 mb-0">
          <h6>Jangka Waktu</h6>
          <p className="value">
            {getJangkaWaktu(Number(allData?.jangkaWaktu || allData?.JANGKA_WAKTU))}
          </p>
        </div>
        {props.KPRid?.substring(0, 5) === "KPRST" ? null : (
          <div className="col-6 mb-0">
            <h6>Cara Pembayaran</h6>
            <p className="value">
              {tipePembayaran(allData?.pembayaranAngsuran)}
            </p>
          </div>
        )}
      </div>
      <div className={`mb-4 ${styles.titleStep}`}>
        <h6
          style={{
            fontSize: "14px",
            lineHeight: "18.2px",
            fontFamily: "FuturaBT",
            fontWeight: "700",
            color: "#00193e",
          }}
        >
          Data Diri
        </h6>
        <div className="cursor-pointer" onClick={() => props.setStepCredit(2)}>
          <IconEdit />
        </div>
      </div>
      <div className="row detail_ringkasan" style={{ marginBottom: "36px" }}>
        <div className="col-6 mb-2">
          <h6>Nomor KTP</h6>
          <p className="value">{allData?.no_ktp || allData?.NO_ID}</p>
        </div>

        <div className="col-6 mb-2">
          <h6>Nama Lengkap</h6>
          <p className="value">{allData?.nm_lgkp || allData?.NAMA_LENGKAP}</p>
        </div>
        <div className="col-6 mb-2">
          <h6>Kota Kelahiran</h6>
          <p className="value">{allData?.tpt_lhr || allData?.TEMPAT_LAHIR}</p>
        </div>

        <div className="col-6 mb-2">
          <h6>Tanggal Lahir</h6>
          <p className="value">
            {allData?.tgl_lhr?.split("T")[0] ||
              allData?.TGL_LAHIR?.split(" ")[0]}
          </p>
        </div>
        <div className="col-12 mb-2">
          <h6>Email</h6>
          <p className="value">{allData?.eml || allData?.EMAIL}</p>
        </div>

        <div className="col-6 mb-2">
          <h6>Nomor Handphone</h6>
          <p className="value">{allData?.no_hp1 || allData?.NO_HP1}</p>
        </div>
        <div className="col-6 mb-2">
          <h6>Nomor Telepon</h6>
          <p className="value">{allData?.no_tlp || allData?.NO_TELP}</p>
        </div>
        <div className="col-12 mb-2">
          <h6>Status Menikah</h6>
          <p className="value">
            {statusNikah(allData.st_mnkh || allData.STATUS_KAWIN)}
          </p>
        </div>
        <div className="col-12 mb-2">
          <h6>Alamat KTP</h6>
          <p className="value">{allData?.almt || allData?.ALAMAT}</p>
        </div>

        <div className="col-6 mb-2">
          <h6>RT</h6>
          <p className="value">{allData?.rt || allData?.RT}</p>
        </div>
        <div className="col-6 mb-2">
          <h6>RW</h6>
          <p className="value">{allData?.rw || allData?.RW}</p>
        </div>
        <div className="col-6 mb-2">
          <h6>Provinsi</h6>
          <p className="value">
            {allData?.n_prop || generateProvinsi(allData?.ID_PROPINSI)}
          </p>
        </div>
        <div className="col-6 mb-2">
          <h6>Kota/Kabupaten</h6>
          <p className="value">
            {allData?.n_kot || generateKota(allData?.ID_KAB_KOTA)}
          </p>
        </div>
        <div className="col-6 mb-2">
          <h6>Kecamatan</h6>
          <p className="value">
            {allData?.n_kec || generateKecamatan(allData?.ID_KECAMATAN)}
          </p>
        </div>
        <div className="col-6 mb-2">
          <h6>Kelurahan</h6>
          <p className="value">
            {allData?.n_kel || generateKelurahan(allData?.ID_DESA_KELURAHAN)}
          </p>
        </div>
      </div>

      <div className={`mb-2 ${styles.titleStep}`}>
        <h6
          style={{
            fontSize: "20px",
            lineHeight: "26px",
            color: "#00193E",
            fontFamily: "FuturaBT",
            fontStyle: "normal",
            fontWeight: "700",
            marginBottom: "16px",
          }}
        >
          Upload Dokumen
        </h6>{" "}
        <div className="cursor-pointer" onClick={() => props.setStepCredit(3)}>
          <IconEdit />
        </div>
      </div>
      <div className={`row mb-4 ${styles.titleStep}`}>
        <div className="col-12 mb-4">
          <h6
            style={{
              color: "#00193E",
              fontFamily: "FuturaBT",
              fontStyle: "normal",
              fontWeight: "700",
              fontSize: "14px",
            }}
          >
            Foto KTP
          </h6>
          <div className="row">
            <div className="col-3">
              <div className={styles.displayImage}>
                <embed
                  src={generateImageUrl(
                    allData?.fl_ktp?.split("|")[1] ||
                      allData?.FILE_KTP?.split("|")[1]
                  )}
                  alt=""
                  className={styles.imagePrev}
                />
              </div>
            </div>
            <div className="col-9 d-flex align-items-center">
              <p
                style={{
                  color: "#000000",
                  fontFamily: "Helvetica",
                  fontStyle: "normal",
                  fontWeight: "400",
                }}
              >
                {allData?.fl_ktp?.split("/").pop() ||
                  allData?.FILE_KTP?.split("/").pop()}
              </p>
            </div>
          </div>
        </div>
        <div className="col-12 mb-4">
          <h6
            style={{
              fontSize: "14px",
              color: "#00193E",
              fontFamily: "FuturaBT",
              fontStyle: "normal",
              fontWeight: "700",
            }}
          >
            Pas Foto
          </h6>
          <div className="row">
            <div className="col-3">
              <div className={styles.displayImage}>
                <embed
                  src={generateImageUrl(
                    allData?.fl_pasFoto?.split("|")[1] ||
                      allData?.FILE_PAS_FOTO?.split("|")[1]
                  )}
                  alt=""
                  className={styles.imagePrev}
                />
              </div>
            </div>
            <div className="col-9 d-flex align-items-center">
              <p
                style={{
                  color: "#000000",
                  fontFamily: "Helvetica",
                  fontStyle: "normal",
                  fontWeight: "400",
                }}
              >
                {allData?.fl_pasFoto?.split("/").pop() ||
                  allData?.FILE_PAS_FOTO?.split("/").pop()}
              </p>
            </div>
          </div>
        </div>
        <div className="col-12 mb-4">
          <h6
            style={{
              fontSize: "14px",
              color: "#00193E",
              fontFamily: "FuturaBT",
              fontStyle: "normal",
              fontWeight: "700",
            }}
          >
            Slip Gaji
          </h6>
          <div className="row">
            <div className="col-3">
              <div className={styles.displayImage}>
                <embed
                  src={generateImageUrl(
                    allData?.fl_slp_pghsln?.split("|")[1] ||
                      allData?.FILE_SLIP_PENGHASILAN?.split("|")[1]
                  )}
                  alt=""
                  className={styles.imagePrev}
                />
              </div>
            </div>
            <div className="col-9 d-flex align-items-center">
              <p
                style={{
                  color: "#000000",
                  fontFamily: "Helvetica",
                  fontStyle: "normal",
                  fontWeight: "400",
                }}
              >
                {allData?.fl_slp_pghsln?.split("/").pop() ||
                  allData?.FILE_SLIP_PENGHASILAN?.split("/").pop()}
              </p>
            </div>
          </div>
        </div>
        <div className="col-12 mb-4">
          <h6
            style={{
              fontSize: "14px",
              color: "#00193E",
              fontFamily: "FuturaBT",
              fontStyle: "normal",
              fontWeight: "700",
            }}
          >
            Rekening Koran
          </h6>
          <div className="row">
            <div className="col-3">
              <div className={styles.displayImage}>
                <embed
                  src={generateImageUrl(allData?.fl_rek_koran?.split("|")[1] ||allData?.FILE_REK_KORAN?.split("|")[1])}
                  alt=""
                  className={styles.imagePrev}
                />
              </div>
            </div>
            <div className="col-9 d-flex align-items-center">
              <p
                style={{
                  color: "#000000",
                  fontFamily: "Helvetica",
                  fontStyle: "normal",
                  fontWeight: "400",
                }}
              >
                {allData?.fl_rek_koran?.split("/").pop() ||
                  allData?.FILE_REK_KORAN?.split("/").pop()}
              </p>
            </div>
          </div>
        </div>
      </div>
      <button
        style={{
          fontFamily: "Helvetica",
          fontWeight: "700",
          width: "151px",
          height: "48px",
        }}
        type="button"
        className="btn btn-main mr-3"
        // onClick={openModalOtp}
        // data-bs-target="#modalOtp"
        // data-bs-toggle="modal"
        // disabled={errors == {} ? false : true}
        onClick={() => {
          if(jenisKpr === "KPRST") {
            if(Number(allData?.HARGA_JUAL) - Number(allData?.NILAI_PENGAJUAN) == Number(allData?.UANG_MUKA)) {
              props.submitCredit()
            } else {
              alert("Nilai pengajuan atau uang muka tidak valid, mohon lakukan pengajuan ulang")
            }

          } else if(jenisKpr === "KPRNS") {
            if(Number(allData?.hargaProperti) - Number(allData?.nl_pgjn) == Number(allData?.uangMuka)) {
              props.submitCredit()
            } else {
              alert("Nilai pengajuan atau uang muka tidak valid, mohon lakukan pengajuan ulang")
            }
          } 
        }}
        disabled={validatingNegativeValue()}
      >
        Kirim Pengajuan
      </button>
      {/* {props.modalSucces && (
        <div className={styles_modal.boxModal} id="modal-upload-doc" style={{ zIndex: "9999" }}>
          <div className={styles_modal.boxModal__Modal} style={{ top: "50%" }}>
            <div className="text-center">
              <button
                type="button"
                style={{ background: "#ffffff", border: "none", width: "100%", textAlign: "end" }}
                data-bs-dismiss="modal"
                aria-label="Close"
                data-bs-target="#modalSuccessRegister"
                onClick={() => {
                  props.setModalSucces(false);
                }}
              >
                <svg style={{ transform: "translateX(32px)" }} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                    fill="#0061A7"
                  />
                </svg>
              </button>
              <h4 className="modal-title mb-3">Terima Kasih</h4>

              <Lottie
                options={defaultOptions}
                height={350}
                width={350}
                isStopped={false}
                isPaused={false}
              />
              <div className="desc text-center mb-5" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                Pengajuan berhasil dikirim.
              </div>
              <div className={`button-kembali ${styles_modal.boxModal__Modal__Footer}`}>
                <Link href="/">
                  <button
                    type="buttom"
                    className="btn btn-main form-control btn-back px-5"
                    style={{
                      maxWidth: "490px",
                      height: "48px",
                      fontSize: "14px",
                      fontFamily: "Helvetica",
                      fontWeight: 700,
                      backgroundColor: "#0061A7",
                    }}
                  >
                    Kembali ke Beranda
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      )} */}
      {/* {modalOtp && <Otp submitCredit={() => props.submitCredit()} setLoadingStep={props.setLoadingStep} />} */}
      {/* <button
        type="button"
        data-bs-target="#modalOtp"
        data-bs-toggle="modal"
        ref={otpRef}
        style={{display: 'none'}}
      ></button> */}
    </div>
  );
}

const generateImageUrl = (url) => {
  if (!url) return "/images/icons/thumb.png";

  let fullUrl = `https://www.btnproperti.co.id/cdn1/files/${url.split('"')[0]}`;
  fullUrl = fullUrl.replaceAll(" ", "%20");

  var img = new Image();
  const getImage = async ()=>{
    img.src = fullUrl;
    await img.decode();
    return img.height
     };
  const isImageExist = getImage() != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/icons/thumb.png";
  }
};
