import { useEffect, useState } from "react";
import { useAppContext } from "../../context";
import { getMemberDetail } from "../../services/member";
import { useRouter } from "next/router";
import { getAge } from "../../helpers/getAge";
import { useMediaQuery } from "react-responsive";
import { continueFillKPR } from "../../services/ajukanKPR";
import { defaultHeaders } from "../../utils/defaultHeaders";
import Cookies from "js-cookie";
import { isInvalid } from "../../utils/validation.register";

export default function CreditStepTwo(props) {
  //const { userKey } = useAppContext();
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 912px)` });
  const router = useRouter();
  const { id } = router.query;
  const [dataDiri, setDataDiri] = useState({
    nik: "",
    nama: "",
    kotaLahir: "",
    tglLahir: "",
    email: "",
    hp: "",
    tlpn: "",
    statMenikah: "",
    alamat: "",
    rt: "",
    rw: "",
  });
  const [validated, setValidated] = useState(false);
  const [emailInvalid, setEmailInvalid] = useState(false);
  const [NIKInvalid, setNIKInvalid] = useState(false);
  const [nameInvalid, setNameInvalid] = useState(false);
  const [kotaLahirInvalid, setKotaLahirInvalid] = useState(false);
  const [tglLahirInvalid, setTglLahirInvalid] = useState(false);
  const [tglLahirValidation, setTglLahirValidation] = useState('');
  const [phoneInvalid, setPhoneInvalid] = useState(false);
  const [tlpnInvalid, setTlpnInvalid] = useState(false);
  const [statusInvalid, setStatusInvalid] = useState(false);
  const [alamatInvalid, setAlamatInvalid] = useState(false);
  const [RTInvalid, setRTInvalid] = useState(false);
  const [RWInvalid, setRWInvalid] = useState(false);
  const [provinsiInvalid, setprovinsiInvalid] = useState(false);
  const [kotaInvalid, setkotaInvalid] = useState(false);
  const [kecInvalid, setkecInvalid] = useState(false);
  const [kelInvalid, setkelInvalid] = useState(false);
  const [kodeposInvalid, setkodeposInvalid] = useState(false);

  /* ----------------------------- FETCH ADDRESS ----------------------------- */
  const [allProvince, setAllProvince] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState("");
  const [allCity, setAllCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState("");
  const [allKecamatan, setAllKecamatan] = useState([]);
  const [selectedKecamatan, setSelectedKecamatan] = useState("");
  const [allKelurahan, setAllKelurahan] = useState([]);
  const [selectedKelurahan, setSelectedKelurahan] = useState("");
  const [selectedKodePos, setSelectedKodePos] = useState("");

  
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  const [isPhoneValid, setPhoneValid] = useState(true);

  const UNFILLED = "Isian tidak boleh kosong";
  function print(a) {
    return "Isi dengan " + a;
  }

// handle blur =========================================

const handleBlurPhone = (e) => {
  const id = e.target.id;
  const value = e.target.value;
  console.log({value});

  if (!value) {
    setPhoneErrorMessage(UNFILLED);
    setValidated(false);
    return setPhoneValid(false);
  } else if (isInvalid(id, value)) {
    setPhoneErrorMessage(print("angka"));
    setValidated(false);
    return setPhoneValid(false);
  } else if (isInvalid('phone_validation', value)) {
    setPhoneErrorMessage("Isi Format Nomor Handphone dengan 08XXXX");
    setValidated(false);
    return setPhoneValid(false);
  } else if (isInvalid('phone_length_validation', value)) {
    setPhoneErrorMessage("Nomor harus berisi minimal 10 karakter");
    setValidated(false);
    return setPhoneValid(false);
  } else {
    setPhoneValid(true)
    setValidated(true);
  }
}
// ======================================================




  // date
  const dateNow = new Date()
  const year = dateNow.getFullYear()
  const month = dateNow.getMonth()
  const date = dateNow.getDate()

  const max = `${year-18}-${month < 10 ? "0" + (month + 1) : (month + 1)}-${date < 10 ? "0" + date : date}`
  const min = `${year-65}-${month < 10 ? "0" + (month + 1) : (month + 1)}-${date < 10 ? "0" + date : date}`

  const userKey = JSON.parse(Cookies.get("keyMember"))
  const autoFill = () => {
    getMemberDetail(userKey)
          .then((res) => {
            const responseData = res.Data[0];
            setSelectedProvince(responseData.i_prop);
            setSelectedCity(responseData.i_kot);
            setSelectedKecamatan(responseData.i_kec);
            setSelectedKelurahan(responseData.i_kel);
            setSelectedKodePos(responseData.pos);
            let tempDetail = { ...dataDiri };
            tempDetail.nik = responseData.no_ktp;
            tempDetail.nama = responseData.n;
            tempDetail.kotaLahir = responseData.tpt_lhr;
            tempDetail.tglLahir = responseData.tgl_lhr?.split("T")[0];
            tempDetail.email = responseData.e;
            tempDetail.hp = responseData.no_hp;
            tempDetail.tlpn = responseData.no_tlp;
            tempDetail.statMenikah = responseData.st_kwn?.toString();
            tempDetail.alamat = responseData.almt;
            getRingkasanData(props.KPRid).then((resData)=>{
              resData.Data[0].rt ? tempDetail.rt = resData.Data[0].rt : "";
              resData.Data[0].rw ? tempDetail.rw = resData.Data[0].rw : "";
              setDataDiri(tempDetail);
            })
          })
       
  }

  useEffect(() => {
    // autoFill()
    if (JSON.parse(localStorage.getItem("dataDiri"))?.nik) {
      setDataDiri(JSON.parse(localStorage.getItem("dataDiri")));
      setSelectedProvince(localStorage.getItem("provinsi"));
      setSelectedCity(localStorage.getItem("kota"));
      setSelectedKecamatan(localStorage.getItem("kecamatan"));
      setSelectedKelurahan(localStorage.getItem("kelurahan"));
      if (localStorage.getItem("kodepos") && localStorage.getItem("kodepos") !== null && localStorage.getItem("kodepos") !== "null") {
        setSelectedKodePos(localStorage.getItem("kodepos"));
      };
    } else {
      if (props.KPRid?.substring(0, 5) === "KPRST") {
        getMemberDetail(userKey)
          .then((res) => {
            console.log("~ res detail member : ", res);
            const responseData = res.Data[0];
            setSelectedProvince(responseData.i_prop);
            setSelectedCity(responseData.i_kot);
            setSelectedKecamatan(responseData.i_kec);
            setSelectedKelurahan(responseData.i_kel);
            setSelectedKodePos(responseData.pos);
            let tempDetail = { ...dataDiri };
            tempDetail.nik = responseData.no_ktp;
            tempDetail.nama = responseData.n;
            tempDetail.kotaLahir = responseData.tpt_lhr;
            tempDetail.tglLahir = responseData.tgl_lhr?.split("T")[0];
            tempDetail.email = responseData.e;
            tempDetail.hp = responseData.no_hp;
            tempDetail.tlpn = responseData.no_tlp;
            tempDetail.statMenikah = responseData.st_kwn?.toString();
            tempDetail.alamat = responseData.almt;
            getRingkasanData(props.KPRid).then((resData)=>{
              resData.Data[0].rt ? tempDetail.rt = resData.Data[0].rt : "";
              resData.Data[0].rw ? tempDetail.rw = resData.Data[0].rw : "";
              setDataDiri(tempDetail);
            })
            .catch((error)=>{
              console.log("err adhin",error)
              setDataDiri(tempDetail)
            })
          })
          .catch((err) => {});
      }else{
        getMemberDetail(userKey)
        .then((res) => {
          console.log("~ res detail member : ", res);
          const responseData = res.Data[0];
          setSelectedProvince(responseData.i_prop);
          setSelectedCity(responseData.i_kot);
          setSelectedKecamatan(responseData.i_kec);
          setSelectedKelurahan(responseData.i_kel);
          setSelectedKodePos(responseData.pos);
          let tempDetail = { ...dataDiri };
          tempDetail.nik = responseData.no_ktp;
          tempDetail.nama = responseData.n;
          tempDetail.kotaLahir = responseData.tpt_lhr;
          tempDetail.tglLahir = responseData.tgl_lhr?.split("T")[0];
          tempDetail.email = responseData.e;
          tempDetail.hp = responseData.no_hp;
          tempDetail.tlpn = responseData.no_tlp;
          tempDetail.statMenikah = responseData.st_kwn?.toString();
          tempDetail.alamat = responseData.almt;
          getRingkasanData(props.KPRid).then((resData)=>{
            resData.Data[0].rt ? tempDetail.rt = resData.Data[0].rt : "";
            resData.Data[0].rw ? tempDetail.rw = resData.Data[0].rw : "";
            setDataDiri(tempDetail);
          })
          .catch((error)=>{
            console.log("err adhin",error)
            setDataDiri(tempDetail)
          })
        })
        .catch((err) => {});
      }
    }
  }, []);

  useEffect(() => {
    if (validated) {
      if (props.percent < 50) {
        props.setShowPercent(50);
      }
    }
  }, [validated]);

  const nextStep = (e) => {
    e.preventDefault();
    localStorage.setItem("dataDiri", JSON.stringify(dataDiri));
    localStorage.setItem("provinsi", selectedProvince);
    localStorage.setItem("kota", selectedCity);
    localStorage.setItem("kecamatan", selectedKecamatan);
    localStorage.setItem("kelurahan", selectedKelurahan);
    localStorage.setItem("kodepos", selectedKodePos);
    submitData();
  };

  const changeData = (type, value) => {
    const nameFormat = /^[a-zA-Z\s]*$/;
    let tempObj = { ...dataDiri };
    if (["nik", "hp", "tlpn", "rt", "rw"].includes(type)) {
      tempObj[type] = value.replace(/[^\d]/g, "");
    } else {
      if (type === "nama") {
        if (nameFormat.test(value)) {
          tempObj[type] = value;
        }
      } else {
        tempObj[type] = value;
      }
    }
    setDataDiri(tempObj);
  };

  const submitData = async () => {
    props.setLoadingStep(true);

    const payload = {
      AccessKey_Member: userKey,
      kpr_id: props.KPRid,
      nama_lengkap: dataDiri.nama,
      no_ktp: dataDiri.nik,
      tempat_lahir: dataDiri.kotaLahir,
      tgl_lahir: dataDiri.tglLahir,
      status_nikah: dataDiri.statMenikah,
      email: dataDiri.email,
      no_telp: dataDiri.tlpn,
      no_hp1: dataDiri.hp,
      alamat: dataDiri.alamat,
      rt: dataDiri.rt,
      rw: dataDiri.rw,
      kode_pos: selectedKodePos,
      id_provinsi: selectedProvince,
      id_kota: selectedCity,
      id_kecamatan: selectedKecamatan,
      id_kelurahan: selectedKelurahan,
    };

    try {
      const dataDiri = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/${props.KPRid?.substring(0, 5) === "KPRST" ? "v2" : "v1"}/pengajuan/${props.KPRid?.substring(0, 5) === "KPRST" ? "stok" : "nonstok"}/setdatadiri`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            // defaultHeaders
          },
          body: new URLSearchParams(payload),
        }
      );
      const responseData = await dataDiri.json();
      if (!responseData.status) {
        throw responseData.message;
      } else {
        props.setStepCredit(3);
        props.setProgressCredit(50);
      }
    } catch (error) {
      console.log("error submit data diri : ", error);
    } finally {
      props.setLoadingStep(false);
    }
  };

  const getRingkasanData = async (id) => {
    props.setLoadingStep(true);
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/kpr/nonstok/show`;
      const res = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_Member: userKey,
          defaultHeaders
        },
        body: new URLSearchParams({
          id: id,
        }),
      });
      const resData = await res.json();


      if (!resData.IsError) {
        return Promise.resolve(resData);
      } else {
        throw {
          message: resData.ErrToUser,
        };
      }
    } catch (error) {
        return Promise.reject(error);
    } finally {
        props.setLoadingStep(false);
    }
  };

  const getRingkasanDataListing = async (id) =>{

      props.setLoadingStep(true);
      try {
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/v1/pengajuan/stok/detailpengajuan`;
        const res = await fetch(endpoint, {
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            AccessKey_Member: userKey,
            defaultHeaders
          },
          body: new URLSearchParams({
            id: id,
          }),
        });
        const resData = await res.json();
  
  
        if (!resData.IsError) {
          return Promise.resolve(resData);
        } else {
          throw {
            message: resData.ErrToUser,
          };
        }
      } catch (error) {
          return Promise.reject(error);
      } finally {
          props.setLoadingStep(false);
      }
  }

  const validateEmail = () => {
    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!dataDiri?.email?.match(mailFormat)) {
      setEmailInvalid(true);
    } else {
      setEmailInvalid(false);
    }
  };

  const validatePhone = () => {
    if (!dataDiri?.hp) {
      setPhoneInvalid(true);
    } else {
      setPhoneInvalid(false);
    }
  };

  const validateNIK = () => {
    if (dataDiri?.nik?.length < 16) {
      setNIKInvalid(true);
    } else {
      setNIKInvalid(false);
    }
  };

  const validateName = () => {
    if (!dataDiri?.nama) {
      setNameInvalid(true);
    } else {
      setNameInvalid(false);
    }
  };

  const validateCityBirth = () => {
    if (!dataDiri?.kotaLahir) {
      setKotaLahirInvalid(true);
    } else {
      setKotaLahirInvalid(false);
    }
  };

  const validateTlpn = () => {
    if (!dataDiri?.tlpn) {
      setTlpnInvalid(true);
    } else {
      setTlpnInvalid(false);
    }
  };

  const validateStatus = () => {
    if (!dataDiri?.statMenikah) {
      setStatusInvalid(true);
    } else {
      setStatusInvalid(false);
    }
  };

  const validateAlamat = () => {
    if (!dataDiri?.alamat) {
      setAlamatInvalid(true);
    } else {
      setAlamatInvalid(false);
    }
  };

  const validateRT = () => {
    if (!dataDiri?.rt) {
      setRTInvalid(true);
    } else {
      setRTInvalid(false);
    }
  };

  const validateRW = () => {
    if (!dataDiri?.rw) {
      setRWInvalid(true);
    } else {
      setRWInvalid(false);
    }
  };

  const validateProvinsi = () => {
    if (!selectedProvince) {
      setprovinsiInvalid(true);
    } else {
      setprovinsiInvalid(false);
    }
  };

  const validateKota = () => {
    if (!selectedCity) {
      setkotaInvalid(true);
    } else {
      setkotaInvalid(false);
    }
  };

  const validateKec = () => {
    if (!selectedKecamatan) {
      setkecInvalid(true);
    } else {
      setkecInvalid(false);
    }
  };

  const validateKel = () => {
    if (!selectedKelurahan) {
      setkelInvalid(true);
    } else {
      setkelInvalid(false);
    }
  };

  const validateKodepos = () => {
    if (!selectedKodePos || selectedKodePos?.length !== 5) {
      setkodeposInvalid(true);
    } else {
      setkodeposInvalid(false);
    }
  };

  const validateDateofBirth = (e) => {
    if (!dataDiri?.tglLahir) {
      e.currentTarget.type = "text";
    }
    
    if (!dataDiri?.tglLahir) {
      setTglLahirValidation('');
      setTglLahirInvalid(true);
    } else {
      if (getAge(e.target.value) < 18) {
        setTglLahirValidation('Usia minimal 18 tahun');
        setTglLahirInvalid(true);
      } else if(getAge(e.target.value) >65) {
        setTglLahirValidation('Usia maksimal 65 tahun');
        setTglLahirInvalid(true);
      } else {
        setTglLahirInvalid(false);
      };
    }
  };

  const createDataDiri = () => {
    validateNIK();
    validateName();
    validateCityBirth();
    validateDateofBirth();
    validateEmail();
    validatePhone();
    // validateTlpn();
    validateStatus();
    validateAlamat();
    validateRT();
    validateRW();
    validateProvinsi();
    validateKota();
    validateKec();
    validateKel();
  };

  const handlePressEnter = (e) => {
    if (e.key === "Enter") {
      createDataDiri();
    }
  };

  /* --------------------- GET ALL PROVINCE ON FIRST LOAD --------------------- */
  useEffect(() => {
    getAllProvinceData();
  }, []);
  useEffect(() => {
    const value = dataDiri?.hp;
    console.log({value});

    if (!value) {
      setPhoneErrorMessage(UNFILLED);
      setValidated(false);
      return setPhoneValid(false);
    } else if (isInvalid(id, value)) {
      setPhoneErrorMessage(print("angka"));
      setValidated(false);
      return setPhoneValid(false);
    } else if (isInvalid('phone_validation', value)) {
      setPhoneErrorMessage("Isi Format Nomor Handphone dengan 08XXXX");
      setValidated(false);
      return setPhoneValid(false);
    } else if (isInvalid('phone_length_validation', value)) {
      setPhoneErrorMessage("Nomor harus berisi minimal 10 karakter");
      setValidated(false);
      return setPhoneValid(false);
    } else {
      setPhoneValid(true)
      setValidated(true);
    }
  }, [dataDiri]);


  useEffect(() => {
    let temp = true;
    if (!props.KPRid) {
      temp = false;
    }
    if (!selectedCity || selectedCity === 'null') {
      temp = false;
    }
    if (!selectedKecamatan || selectedKecamatan === 'null') {
      temp = false;
    }
    if (!selectedKelurahan || selectedKelurahan === 'null') {
      temp = false;
    }
    if (!selectedProvince || selectedProvince === 'null') {
      temp = false;
    }
    if (emailInvalid) {
      temp = false;
    }
    for (let data in dataDiri) {
      if ((!dataDiri[data] && data != 'tlpn') || dataDiri?.nik?.length !== 16) {
        temp = false;
      }
    }
    if (tglLahirInvalid) {
      temp = false;
    };
    setValidated(temp);
  }, [
    dataDiri,
    selectedCity,
    selectedKecamatan,
    selectedKelurahan,
    selectedKodePos,
    selectedProvince,
    emailInvalid,
    tglLahirInvalid
  ]);

  const getAllProvinceData = async () => {
    try {
      const payload = {
        AppKey: process.env.NEXT_PUBLIC_APP_KEY,
        AppSecret: process.env.NEXT_PUBLIC_APP_SECRET,
      };
      // GET NEW ACCESS KEY
      const keyEndpoint = `${process.env.NEXT_PUBLIC_API_HOST}/app/login`;
      const accessKey = await fetch(keyEndpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Access-Control-Allow-Origin": "*",
          defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const keyResponse = await accessKey.json();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw {
          message: keyResponse.ErrToUser,
        };
      }
      // GET NEW ACCESS KEY

      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/propinsi/show`;
      const propinsi = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_App: keyResponse.AccessKey,
          defaultHeaders
        },
        body: new URLSearchParams({
          Sort: "n ASC",
        }),
      });
      const dataPropinsi = await propinsi.json();
      if (!dataPropinsi.IsError) {
        setAllProvince(dataPropinsi.Data);
      } else {
        throw {
          message: dataPropinsi.ErrToUser,
        };
      }
    } catch (error) {}
  };

  /* ------------------ GET CITIES WHEN PROVINCE IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedProvince) {
      getCityData();
    } else {
      setAllCity([]);
      setAllKecamatan([]);
      setAllKelurahan([]);
    }
  }, [selectedProvince]);

  const selectProvince = (e) => {
    setSelectedProvince(e.target.value);
    setSelectedCity("");
    setSelectedKecamatan("");
    setSelectedKelurahan("");
    setSelectedKodePos("");
    setAllKecamatan([]);
    setAllKelurahan([]);
  };

  const selectCity = (e) => {
    setSelectedCity(e.target.value);
    setSelectedKecamatan("");
    setSelectedKelurahan("");
    setSelectedKodePos("");
    setAllKelurahan([]);
  };

  const selectKecamatan = (e) => {
    setSelectedKecamatan(e.target.value);
    setSelectedKelurahan("");
    setSelectedKodePos("");
  };

  const selectKelurahan = (e) => {
    if (!e.target.value) {
      setSelectedKodePos("");
    }
    setSelectedKelurahan(e.target.value);
  };

  const getCityData = async () => {
    try {
      const payload = {
        AppKey: process.env.NEXT_PUBLIC_APP_KEY,
        AppSecret: process.env.NEXT_PUBLIC_APP_SECRET,
      };
      // GET NEW ACCESS KEY
      const keyEndpoint = `${process.env.NEXT_PUBLIC_API_HOST}/app/login`;
      const accessKey = await fetch(keyEndpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Access-Control-Allow-Origin": "*",
          defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const keyResponse = await accessKey.json();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw {
          message: keyResponse.ErrToUser,
        };
      }
      // GET NEW ACCESS KEY

      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kota/show`;
      const cities = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_App: keyResponse.AccessKey,
          defaultHeaders
        },
        body: new URLSearchParams({
          i_prop: selectedProvince,
          Sort: "n ASC",
        }),
      });
      const dataCities = await cities.json();
      if (!dataCities.IsError) {
        setAllCity(dataCities.Data);
      } else {
        throw {
          message: dataCities.ErrToUser,
        };
      }
    } catch (error) {}
  };

  /* ------------------- GET KECAMATAN WHEN CITY IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedCity) {
      getKecamatanData();
    } else {
      setAllKecamatan([]);
      setAllKelurahan([]);
    }
  }, [selectedCity]);

  const getKecamatanData = async () => {
    try {
      const payload = {
        AppKey: process.env.NEXT_PUBLIC_APP_KEY,
        AppSecret: process.env.NEXT_PUBLIC_APP_SECRET,
      };
      // GET NEW ACCESS KEY
      const keyEndpoint = `${process.env.NEXT_PUBLIC_API_HOST}/app/login`;
      const accessKey = await fetch(keyEndpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Access-Control-Allow-Origin": "*",
          defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const keyResponse = await accessKey.json();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw {
          message: keyResponse.ErrToUser,
        };
      }
      // GET NEW ACCESS KEY

      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kecamatan/show`;
      const kecamatan = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_App: keyResponse.AccessKey,
          defaultHeaders
        },
        body: new URLSearchParams({
          i_kot: selectedCity,
          Sort: "n ASC",
        }),
      });
      const dataKecamatan = await kecamatan.json();
      if (!dataKecamatan.IsError) {
        setAllKecamatan(dataKecamatan.Data);
      } else {
        throw {
          message: dataKecamatan.ErrToUser,
        };
      }
    } catch (error) {}
  };

  /* ------------------- GET KELURAHAN WHEN KECAMATAN IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedKecamatan) {
      getKelurahanData();
    } else {
      setAllKelurahan([]);
    }
  }, [selectedKecamatan]);

  const getKelurahanData = async () => {
    try {
      const payload = {
        AppKey: process.env.NEXT_PUBLIC_APP_KEY,
        AppSecret: process.env.NEXT_PUBLIC_APP_SECRET,
      };
      // GET NEW ACCESS KEY
      const keyEndpoint = `${process.env.NEXT_PUBLIC_API_HOST}/app/login`;
      const accessKey = await fetch(keyEndpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Access-Control-Allow-Origin": "*",
          defaultHeaders
        },
        body: new URLSearchParams(payload),
      });
      const keyResponse = await accessKey.json();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw {
          message: keyResponse.ErrToUser,
        };
      }
      // GET NEW ACCESS KEY

      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kelurahan/show`;
      const kelurahan = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_App: keyResponse.AccessKey,
          defaultHeaders
        },
        body: new URLSearchParams({
          i_kec: selectedKecamatan,
          Sort: "n ASC",
        }),
      });
      const dataKelurahan = await kelurahan.json();
      if (!dataKelurahan.IsError) {
        console.log("GET KELURAHAN", dataKelurahan.Data);
        setAllKelurahan(dataKelurahan.Data);
      } else {
        throw {
          message: dataKelurahan.ErrToUser,
        };
      }
    } catch (error) {}
  };

  /* ------------------- GET KODE POS WHEN KELURAHAN IS SELECTED ------------------ */
  useEffect(() => {
    if (selectedKelurahan && allKelurahan.length > 0) {
      const tempKodepos = allKelurahan.filter((data, index) => {
        return data.id == selectedKelurahan;
      });
      setSelectedKodePos(tempKodepos[0]?.pos);
    }
  }, [selectedKelurahan, allKelurahan]);

  return (
    <div className="form-custom">
      <p
        className="mb-0 ms-3 ms-lg-0 step-1"
        // style={{
        //   fontFamily: "Helvetica",
        //   fontWeight: "700",
        //   color: "#666666",
        // }}
      >
        Step 2
      </p>
      <h2
        className="mb-5 mt-2 ps-3 ps-lg-0"
        style={{
          fontSize: "32px",
          fontFamily: "FuturaBT",
          fontStyle: "normal",
          fontWeight: "700",
          lineHeight: "48px",
          letterSpacing: "0px",
          color: "#000000",
        }}
      >
        Data Diri
      </h2>
      <form onSubmit={nextStep} id="formsubmit">
        <div className="floating-label-wrap" id="credit-form-step2">
          <input
            type="text"
            className={`floating-label-field step-2-ipond required number border-active ${
              NIKInvalid ? "error-border" : ""
            }`}
            name="profil[no_ktp]"
            maxLength="16"
            minLength="16"
            placeholder="Nomor KTP"
            value={dataDiri?.nik}
            onBlur={validateNIK}
            onChange={(e) => changeData("nik", e.target.value)}
            autoComplete="off"
            onKeyPress={handlePressEnter}
            required
            style={{ color: "#00193E" }}
            disabled
          />
          <label className="floating-label label-input">Nomor KTP </label>
          {NIKInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Masukkan 16 digit NIK
            </div>
          )}
        </div>
        <div className="floating-label-wrap" id="credit-namalengkap">
          <input
            type="text"
            className={`floating-label-field step-2-ipond border-active ${
              nameInvalid ? "error-border" : ""
            }`}
            name="profil[n]"
            placeholder="Nama Lengkap"
            value={dataDiri.nama}
            onBlur={validateName}
            onChange={(e) => changeData("nama", e.target.value)}
            autoComplete="off"
            onKeyPress={handlePressEnter}
            required
            style={{ color: "#00193E" }}
            disabled
          />
          <label className="floating-label">Nama Lengkap </label>
          {nameInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi nama lengkap sesuai KTP
            </div>
          )}
        </div>
        <div className="row">
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-kotakelahiran">
              <input
                type="text"
                className={`floating-label-field step-2-ipond-2 border-active ${
                  kotaLahirInvalid ? "error-border" : ""
                }`}
                name="profil[tpt_lhr]"
                placeholder="Kota Kelahiran"
                value={dataDiri.kotaLahir}
                onBlur={validateCityBirth}
                autoComplete="off"
                onChange={(e) => changeData("kotaLahir", e.target.value)}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E" }}
                disabled
              />
              <label className="floating-label">Kota Kelahiran </label>
              {kotaLahirInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi tempat lahir sesuai KTP
                </div>
              )}
            </div>
          </div>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-tgllahir">
              <input
                type="text"
                className={`floating-label-field step-2-ipond-2 custom-calender border-active ${
                  tglLahirInvalid ? "error-border" : ""
                }`}
                max={max}
                min={min}
                onFocus={(e) => (e.currentTarget.type = "date")}
                onMouseOver={isTabletOrMobile ? (e) => (e.currentTarget.type = "date") : null}
                onBlur={validateDateofBirth}
                placeholder="Tanggal lahir"
                id="tgl_lhr_pengajuan"
                name="profil[tgl_lhr]"
                value={dataDiri.tglLahir}
                onChange={(e) => changeData("tglLahir", e.target.value)}
                onKeyPress={handlePressEnter}
                required
                style={{ color: "#00193E" }}
                disabled
              />
              <label className="floating-label">Tanggal Lahir </label>
              {tglLahirInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  {tglLahirValidation || 'Isi tanggal lahir sesuai KTP'}
                </div>
              )}
            </div>
          </div>
          {/* <div className="" style={{ display: "contents" }}>
          </div> */}

          <div className="col-12">
            <div className="floating-label-wrap" id="credit-email">
              <input
                type="email"
                className={`floating-label-field step-2-ipond border-active ${
                  emailInvalid ? "error-border" : ""
                }`}
                name="profil[e]"
                placeholder="Email"
                value={dataDiri.email}
                onBlur={validateEmail}
                autoComplete="off"
                onChange={(e) => changeData("email", e.target.value.replace(/[^a-zA-Z0-9 _@.~-]+/, ''))}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E" }}
              />
              <label className="floating-label">Email </label>
              {emailInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Email tidak valid
                </div>
              )}
            </div>
          </div>

          <div className="col-6">
            <div className="floating-label-wrap" id="credit-nohp">
              <input
                type="text"
                className={`floating-label-field step-2-ipond-2 border-active ${
                  phoneInvalid ? "error-border" : ""
                }`}
                name="profil[no_hp]"
                placeholder="no handphone"
                value={dataDiri?.hp !== "null" ? dataDiri?.hp?.replace(/[^0-9]+/g, '') : ""}
                onBlur={(e) => {
                  validatePhone()
                  handleBlurPhone(e)
                }
                }
                autoComplete="off"
                onChange={(e) => {
                  changeData("hp", e.target.value)
                  setPhoneValid(true)
                }}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E" }}
                maxLength="13"
              />
              <label className="floating-label">Nomor Handphone </label>
              {!isPhoneValid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  {/* Isi nomor handphone */}
                  {phoneErrorMessage}
                </div>
              )}
            </div>
          </div>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-notelp">
              <input
                type="text"
                className={`floating-label-field step-2-ipond-2 border-active ${
                  tlpnInvalid ? "error-border" : ""
                }`}
                name="profil[no_tlp]"
                placeholder="nomor telepon"
                value={dataDiri?.tlpn !== "null" ? dataDiri?.tlpn?.replace(/[^0-9]+/g, '') : ""}
                // onBlur={validateTlpn}
                autoComplete="off"
                onChange={(e) => changeData("tlpn", e.target.value)}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E" }}
                maxLength="13"
              />
              <label className="floating-label">Nomor Telepon</label>
              {/* {tlpnInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi nomor telepon
                </div>
              )} */}
            </div>
          </div>
        </div>

        <div className="floating-label-wrap" id="credit-status">
          <select
            id="input2"
            className={`floating-label-select step-2-ipond form-select custom-select border-active ${
              statusInvalid ? "error-border" : ""
            }`}
            onChange={(e) => changeData("statMenikah", e.target.value)}
            onBlur={validateStatus}
            autoComplete="off"
            onKeyPress={handlePressEnter}
            style={{ color: "#00193E",fontWeight:400, backgroundColor: "#f0f0f0"}}
            required
            disabled
          >
            <option value="">Status Menikah</option>
            <option value="10" selected={parseInt(dataDiri.statMenikah) == 2 && true || parseInt(dataDiri.statMenikah) == 10 && true }>Sudah Menikah</option>
            <option value="20" selected={parseInt(dataDiri.statMenikah) == 1 && true || parseInt(dataDiri.statMenikah) == 20 && true}>Belum Menikah</option>
            <option value="30">Duda / Janda</option>
            <option value="50">Cerai</option>
            <option value="60">Lainnya</option>
          </select>
          <label htmlFor="input2" className="floating-label">
            Status Menikah
          </label>
          {statusInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi status pernikahan
            </div>
          )}
        </div>
        <div className="row">
          {" "}
          <div className="floating-label-wrap">
            <textarea
              className={`floating-label-field text-area-ajukan-kpr border-active ${
                alamatInvalid ? "error-border" : ""
              }`}
              id="credit-alamat"
              rows="4"
              name="profil[almt]"
              placeholder="Alamat KTP"
              value={dataDiri.alamat}
              onChange={(e) => changeData("alamat", e.target.value)}
              // style={{ height: "128px", width: "480px", color: "#00193E" }}
              onBlur={validateAlamat}
              autoComplete="off"
            ></textarea>
            <label
              className="floating-label"
              style={{ 
                left: "14px", 
                // paddingBottom: "8px !important" 
              }}
            >
              Alamat KTP{" "}
            </label>
            {alamatInvalid && (
              <div className="error-input-kpr">
                <i
                  class="bi bi-exclamation-circle-fill"
                  style={{ marginRight: "5px" }}
                ></i>
                Isi alamat sesuai KTP
              </div>
            )}
          </div>
          {/* <div className="hidden"></div> */}
        </div>

        <div className="row" style={{ marginTop: "-10px" }}>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-rt">
              <input
                type="text"
                className={`floating-label-field step-2-ipond-2 border-active ${
                  RTInvalid ? "error-border" : ""
                }`}
                name="profil[rt]"
                placeholder="RT"
                value={dataDiri.rt}
                onBlur={validateRT}
                autoComplete="off"
                maxLength={3}
                onChange={(e) => changeData("rt", e.target.value)}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E" }}
              />
              <label className="floating-label">RT </label>
              {RTInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi RT tempat tinggal
                </div>
              )}
            </div>
          </div>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-rw">
              <input
                type="text"
                className={`floating-label-field step-2-ipond-2 border-active ${
                  RWInvalid ? "error-border" : ""
                }`}
                name="profil[rw]"
                placeholder="RW"
                value={dataDiri.rw }
                onBlur={validateRW}
                autoComplete="off"
                maxLength={3}
                onChange={(e) => changeData("rw", e.target.value)}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E" }}
              />
              <label className="floating-label">RW</label>
              {RWInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi RW tempat tinggal
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-prov">
              <select
                className={`floating-label-select form-select custom-select step-2-ipond-2 border-active ${
                  provinsiInvalid ? "error-border" : ""
                }`}
                name="profil[i_prop]"
                id="provinsi"
                required
                onChange={selectProvince}
                value={selectedProvince}
                onBlur={validateProvinsi}
                autoComplete="off"
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
              }}
              >
                <option value="">Provinsi</option>
                {allProvince.length &&
                  allProvince.map((province) => (
                    <option value={province.id}>{province.n}</option>
                  ))}
              </select>
              <label htmlFor="provinsi" className="floating-label">
                Provinsi
              </label>
              {provinsiInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi provinsi tempat tinggal
                </div>
              )}
            </div>
          </div>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-kotakab">
              <select
                className={`floating-label-select form-select custom-select step-2-ipond-2  border-active ${
                  kotaInvalid ? "error-border" : ""
                }`}
                name="profil[i_kot]"
                id="kota"
                required
                onChange={selectCity}
                onBlur={validateKota}
                autoComplete="off"
                value={selectedCity}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
                      }}
              >
                <option value="">Kota/Kabupaten</option>
                {allCity.length &&
                  allCity.map((city) => (
                    <option value={city.id}>{city.n}</option>
                  ))}
              </select>
              <label className="floating-label">Kota/Kabupaten</label>
              {kotaInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi kota tempat tinggal
                </div>
              )}
            </div>
          </div>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-kecamatan">
              <select
                className={`floating-label-select form-select custom-select step-2-ipond-2  border-active ${
                  kecInvalid ? "error-border" : ""
                }`}
                name="profil[i_kec]"
                id="kecamatan"
                required
                onBlur={validateKec}
                autoComplete="off"
                onChange={selectKecamatan}
                value={selectedKecamatan}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E",
                          whiteSpace: "nowrap",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                      }}
              >
                <option value="">Kecamatan</option>
                {allKecamatan.length &&
                  allKecamatan.map((kecamatan) => (
                    <option value={kecamatan.id}>{kecamatan.n}</option>
                  ))}
              </select>
              <label className="floating-label">Kecamatan </label>
              {kecInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi kecamatan tempat tinggal
                </div>
              )}
            </div>
          </div>
          <div className="col-6">
            <div className="floating-label-wrap" id="credit-kelurahan">
              <select
                className={`floating-label-select form-select custom-select step-2-ipond-2 border-active ${
                  kelInvalid ? "error-border" : ""
                }`}
                name="profil[i_kel]"
                id="kelurahan"
                required
                onBlur={validateKel}
                autoComplete="off"
                onChange={selectKelurahan}
                value={selectedKelurahan}
                onKeyPress={handlePressEnter}
                style={{ color: "#00193E",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
                      }}
              >
                <option value="">Kelurahan</option>
                {allKelurahan.length &&
                  allKelurahan.map((kelurahan) => (
                    <option value={kelurahan.id}>{kelurahan.n}</option>
                  ))}
              </select>
              <label className="floating-label">Kelurahan </label>
              {kelInvalid && (
                <div className="error-input-kpr">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isi kelurahan tempat tinggal
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="floating-label-wrap" id="credit-kodepos">
          <input
            type="text"
            className={`floating-label-field step-2-ipond border-active ${
              kodeposInvalid ? "error-border" : ""
            }`}
            name="profil[pos]"
            id="kodepos"
            placeholder=" Kodepos"
            minLength="5"
            maxLength="5"
            onBlur={validateKodepos}
            // autoComplete="off"
            value={selectedKodePos}
            disabled
            // onChange={changeKodepos}
            onChange={(e) => setSelectedKodePos(e.target.value)}
            style={{ color: "#00193E" }}
          />
          <label className="floating-label">Kodepos </label>
          {kodeposInvalid && (
            <div className="error-input-kpr">
              <i
                class="bi bi-exclamation-circle-fill"
                style={{ marginRight: "5px" }}
              ></i>
              Isi kodepos tempat tinggal yang terdaftar
            </div>
          )}
        </div>
        <div id="btn-ajukankpr" style={{ display: "flex", marginLeft: 15 }}>
          <div className="me-3" style={{ width: "119px", height: "48px" }}>
            <button
              style={{
                width: "119px",
                height: "48px",
                padding: "0",
                fontWeight: "700",
              }}
              disabled={id?.substring(0, 5) === "KPRST"}
              type="button"
              className="btn btn-outline-main"
              onClick={() => props.setStepCredit(1)}
            >
              Sebelumnya
            </button>
          </div>
          <div style={{ width: "119px", height: "48px" }}>
            <button
              // style={{ width: "119px", height: "48px", fontWeight: "700", paddingTop:"10px" }}
              style={{
                width: 119,
                height: 48,
                padding: 0,
                fontFamily: "Helvetica",
                fontSize: 14,
                fontWeight: 700,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "200px",
              }}
              type="submit"
              className="btn btn-main"
              disabled={!validated}
              // disabled={errors == {} ? false : true}
            >
              Selanjutnya
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}
