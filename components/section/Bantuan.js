import Link from "next/link";
import { useRouter } from "next/router";

export default function Bantuan({ userKey }) {
  const router = useRouter();

  const checkKPR = () => {
    router.push("/member/kredit");
  };

  return (
    <div>
      <section id="home_bantuan">
        <div className="container">
          <div className="row">
            <div id="bantuan-sub-title" className="col-md-8">
              <div className="title_bantuan">
                <h6>BUTUH BANTUAN?</h6>
                <h4 className="sub_title_bantuan">Beri tahu bagaimana kami dapat membantu kamu</h4>
                <div className="small-sub-bantuan">
                  <div>Beri tahu</div>
                  <div>bagaimana kami</div>
                  <div>dapat membantu kamu</div>
                </div>
              </div>
            </div>
          </div>
          <div id="body-bantuan">
            <div id="card-bantaun" className="me-md-2 me-0">
              <Link href="/tools/konsultasi">
                <div className="item_bantuan h-100 align-items-start">
                  <img src="/images/icons/smile.svg" className="img-fluid h-100" />
                  <div className="body d-flex flex-column">
                    <h5
                    style={{
                      whiteSpace:"nowrap"
                    }}
                    >
                      Konsultasi
                    </h5>
                    <a
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "400",
                      }}
                    >
                      Tanyakan kepada kami segala hal mengenai KPR untuk membantu kamu membeli rumah idaman.
                    </a>
                  </div>
                </div>
              </Link>
            </div>
            <div id="card-bantaun" className="me-md-2 me-0">
              <Link href="/hubungi_kami">
                <div className="item_bantuan h-100 align-items-start">
                  <img src="/images/icons/headset.svg" className="img-fluid" />
                  <div className="body d-flex flex-column">
                    <h5
                   style={{
                    whiteSpace:"nowrap"
                  }}
                    >
                      Hubungi kami
                    </h5>
                    <a
                      style={{
                        fontFamily: "Helvetica",
                      }}
                    >
                      Jika kamu mengalami kendala pada layanan BTN Properti beri tahu kami secepatnya.
                    </a>
                  </div>
                </div>
              </Link>
            </div>
            <div id="card-bantaun" className="me-md-2 m-0">
              <div className="item_bantuan h-100 align-items-start" data-bs-toggle={!userKey ? "modal" : null} data-bs-target={!userKey ? "#modalLogin" : null} onClick={userKey ? checkKPR : null}>
                <img src="/images/icons/status_check.svg" className="img-fluid" />
                <div className="body d-flex flex-column">
                  <h5
                 style={{
                  whiteSpace:"nowrap"
                }}
                  >
                    Cek status KPR
                  </h5>
                  <a
                    style={{
                      fontFamily: "Helvetica",
                    }}
                  >
                    Cek status pengajuan kredit kamu yang sudah diproses di Kantor Cabang melalui BTN Properti.
                  </a>
                </div>
              </div>
              {/* <Link href="/member/kredit">
              </Link> */}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
