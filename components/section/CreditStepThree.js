import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import styles from "../../pages/ajukan_kpr/Credit.module.scss";
import { useAppContext } from "../../context";
import { useRouter } from "next/router";
import { uploadDokumenKPR } from "../../services/ajukanKPR";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function CreditStepThree(props) {
  const isMobileOrTablet = useMediaQuery({ query: `(max-width: 900px)` });
  const { userKey } = useAppContext();
  const router = useRouter();
  const { id } = router.query;
  const [isMacOs, setIsMacOs] = useState(false)
  useEffect(() => {
    if (navigator.userAgent.indexOf("Win") == -1){
      setIsMacOs(true)
    }
  }, [])

  const [pasFoto, setPasFoto] = useState({
    file: "",
    extension: "",
  });
  const [ktpFoto, setKtpFoto] = useState({
    file: "",
    extension: "",
  });
  const [rekFoto, setRekFoto] = useState({
    file: "",
    extension: "",
  });
  const [slipFoto, setSlipFoto] = useState({
    file: "",
    extension: "",
  });

  const [imageKTP, setImageKTP] = useState("");
  const [imagePas, setImagePas] = useState("");
  const [imageSlip, setImageSlip] = useState("");
  const [imageRek, setImageRek] = useState("");
  const [errImage, setErrImage] = useState("");
  const [textErr, setTextErr] = useState("");
  const [submitted, setSubmitted] = useState(false);

  const [imageShowed, setImageShowed] = useState("");

  const [uploaded, setUploaded] = useState({
    ktp: false,
    pas: false,
    slip: false,
    rek: false
  });

  useEffect(() => {
    if (
      sessionStorage.getItem("ktp_foto") &&
      sessionStorage.getItem("ktp_foto") !== "null"
    ) {
      if (!sessionStorage.getItem("ktp_foto").includes("base64,")) {
        setImageKTP(sessionStorage.getItem("ktp_foto"));
      } else {
        setKtpFoto(JSON.parse(sessionStorage.getItem("ktp_foto")));
      }
    }

    if (
      sessionStorage.getItem("pas_foto") &&
      sessionStorage.getItem("pas_foto") !== "null"
    ) {
      if (!sessionStorage.getItem("pas_foto").includes("base64,")) {
        setImagePas(sessionStorage.getItem("pas_foto"));
      } else {
        setPasFoto(JSON.parse(sessionStorage.getItem("pas_foto")));
      }
    }

    if (
      sessionStorage.getItem("rek_foto") &&
      sessionStorage.getItem("rek_foto") !== "null"
    ) {
      if (!sessionStorage.getItem("rek_foto").includes("base64,")) {
        setImageRek(sessionStorage.getItem("rek_foto"));
      } else {
        setRekFoto(JSON.parse(sessionStorage.getItem("rek_foto")));
      }
    }

    if (
      sessionStorage.getItem("slip_foto") &&
      sessionStorage.getItem("slip_foto") !== "null"
    ) {
      if (!sessionStorage.getItem("slip_foto").includes("base64,")) {
        setImageSlip(sessionStorage.getItem("slip_foto"));
      } else {
        setSlipFoto(JSON.parse(sessionStorage.getItem("slip_foto")));
      }
    }
  }, []);

  useEffect(() => {
    if (submitted) {
      for (let value in uploaded) {
        if (uploaded[value]) {
          return submitAll();
        }
      }
      setSubmitted(false);
      props.setProgressCredit(100);
      props.setStepCredit(4);
      props.setLoadingStep(false);
    }
  }, [submitted]);

  useEffect(() => {
    if (
      ktpFoto?.extension &&
      rekFoto?.extension &&
      slipFoto?.extension &&
      pasFoto?.extension
    ) {
      if (props.percent < 75) {
        props.setShowPercent(75);
      }
    }
  }, [ktpFoto, rekFoto, slipFoto, pasFoto]);

  const submitAll = async () => {
    console.log('~ images payload : ', {
      kpr_id: props.KPRid,
      file_ktp: imageKTP,
      file_slip_gaji: imageSlip,
      file_foto: imagePas,
      file_rekening: imageRek,
    })
    try {
      const allPayload = {
        kpr_id: props.KPRid,
        file_ktp: imageKTP,
        file_slip_gaji: imageSlip,
        file_foto: imagePas,
        file_rekening: imageRek,
      };

      const allUpload = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/pengajuan/${props.KPRid?.substring(0, 5) === 'KPRST' ? 'stok' : 'nonstok'}/setdokumen`,
        {
          method: "POST",
          headers: {
            // "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
          },
          body: new URLSearchParams(allPayload),
        }
      );
      const responseAll = await allUpload.json();
      if (!responseAll.status) {
        throw responseAll.message;
      } else {
        props.setProgressCredit(100);
        props.setStepCredit(4);
      }
    } catch (error) {
      // console.log('error set dokumen : ', error)
    } finally {
      props.setLoadingStep(false);
      setSubmitted(false);
    }
  };

  const nextStep = (e) => {
    e.preventDefault();
    props.setLoadingStep(true);
    const tempUploaded = { ...uploaded };
    let arrPromises = [];
    let arrUpload = [];
    if (ktpFoto.file) {
      arrPromises.push(uploadDokumenKPR(props.KPRid, ktpFoto, "ktp_"));
      arrUpload.push('ktp');
    }

    if (pasFoto.file) {
      arrPromises.push(uploadDokumenKPR(props.KPRid, pasFoto, "foto_"));
      arrUpload.push('foto');
    }

    if (slipFoto.file) {
      arrPromises.push(uploadDokumenKPR(props.KPRid, slipFoto, "slip_"));
      arrUpload.push('slip');
    }

    if (rekFoto.file) {
      arrPromises.push(uploadDokumenKPR(props.KPRid, rekFoto, "rek_"));
      arrUpload.push('rek');
    }
    Promise.all(arrPromises)
    .then(res => {
      console.log('jalanin data apa : ', arrUpload)
      try {
        for (let i = 0; i < arrUpload.length; i++) {
          console.log('jalanin data apa : ', arrUpload[i])
          switch (arrUpload[i]) {
            case 'ktp':
              if (res[i].status) {
                setImageKTP(res[i].file_name);
                tempUploaded.ktp = true;
                sessionStorage.setItem("ktp_foto", JSON.stringify(res[i].file_name));
              }
              break;
            case 'foto':
              if (res[i].status) {
                setImagePas(res[i].file_name);
                tempUploaded.pas = true;
                sessionStorage.setItem("pas_foto", JSON.stringify(res[i].file_name));
              }
              break;
            case 'slip':
              if (res[i].status) {
                setImageSlip(res[i].file_name);
                tempUploaded.slip = true;
                sessionStorage.setItem("slip_foto", JSON.stringify(res[i].file_name));
              }
              break;
            case 'rek':
              if (res[i].status) {
                setImageRek(res[i].file_name);
                tempUploaded.rek = true;
                sessionStorage.setItem("rek_foto", JSON.stringify(res[i].file_name));
              }
              break;
            default:
              break;
          }
        }
      } catch (error) {
        console.log('~ error catch promise : ', error)
      }
      console.log('~ output res upload dokumen : ', res)
    })
    .catch(err => props.setLoadingStep(false))
    .finally(() => {
      setUploaded(tempUploaded);
      setTimeout(() => {
        setSubmitted(true);
      }, 500);
    })
  };

  const imageChange = (e, kategori, maxSize) => {
    console.log('~ data embed pdf : ', e.target.files[0])
    if (
      (e.target.files[0]?.type.includes("jpeg") ||
        e.target.files[0]?.type.includes("jpg") ||
        e.target.files[0]?.type.includes("png") ||
        e.target.files[0]?.type.includes("pdf")) &&
      // (e.target.files[0].size / 1048576).toFixed(2) < maxSize
      +(e.target.files[0]?.size) <= (maxSize * 1000000)
    ) {
      setErrImage("");
      setTextErr("");
      if (e.target.files && e.target.files.length > 0) {
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = function () {
          const payload = {
            file: reader.result,
            extension: e.target.files[0].name.split(".").pop(),
          };
          switch (kategori) {
            case "pas_foto":
              setPasFoto(payload);
              sessionStorage.setItem("pas_foto", JSON.stringify(payload));
              break;
            case "slip_foto":
              setSlipFoto(payload);
              sessionStorage.setItem("slip_foto", JSON.stringify(payload));
              break;
            case "rek_foto":
              setRekFoto(payload);
              sessionStorage.setItem("rek_foto", JSON.stringify(payload));
              break;
            case "ktp_foto":
              setKtpFoto(payload);
              sessionStorage.setItem("ktp_foto", JSON.stringify(payload));
              break;
            default:
              break;
          }
        };

        reader.onerror = function (error) {
          // console.log('error attach image : ', error);
        };
      }
    } else {
      setErrImage(kategori);
      if (!e.target.files[0]?.type.includes("jpeg") &&
      !e.target.files[0]?.type.includes("jpg") &&
      !e.target.files[0]?.type.includes("png") &&
      !e.target.files[0]?.type.includes("pdf")) {
        setTextErr("File yang dimasukan tidak sesuai format");
      } else if (+(e.target.files[0]?.size) > (maxSize * 1000000)) {
        setTextErr("Image lebih dari 3MB");
      };
    };
  };

  return (
    <div className="m-3 m-lg-0">
      <p
        className="mb-0"
        style={{
          fontFamily: "Helvetica",
          fontWeight: "700",
          color: "#666666",
        }}
      >
        Step 3
      </p>
      <h2
        style={{
          fontSize: "32px",
          fontFamily: "FuturaBT",
          fontStyle: "normal",
          fontWeight: "700",
          lineHeight: "48px",
          letterSpacing: "0px",
          color: "#000000",
        }}
        className="mb-5 mt-2"
      >
        Upload Dokumen
      </h2>
      <form onSubmit={nextStep}>
        <h6
          className="mb-2 pb-2"
          style={{
            fontSize: "14px",
            fontFamily: "FuturaBT",
            fontWeight: "700",
            color: "#00193E",
          }}
        >
          Unggah Foto KTP
        </h6>
        <div className={`row ${styles.cardImageUpload}`}>
          <div className="d-flex">
            <div className="me-3" style={{ width: "100px", height: "100px" }}>
              <div
                className={
                  "ratio ratio-1x1 photo_input  " + styles.displayImage
                }
              >
                <a 
                  data-bs-toggle="modal" 
                  data-bs-target="#modalBerkasKPR" 
                  href="#" 
                  onClick={() => setImageShowed(ktpFoto?.file || generateImageUrl(imageKTP?.split("|")[1]))}
                  style={ktpFoto?.file || imageKTP ? {} : { pointerEvents: 'none' }}
                >
                  <embed
                    src={
                      ktpFoto?.file || generateImageUrl(imageKTP?.split("|")[1])
                    }
                    // src={ktpFoto.file ? ktpFoto.file : (imageKTP ? generateImageUrl(imageKTP.split("|")[1]) : "/images/icons/thumb.png")}
                    alt=""
                    className={styles.imagePrev}
                  />
                </a>
              </div>
            </div>
            <div>
              <p
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  fontSize: "14px",
                  color: "#000000",
                }}
              >
                Unggah file dalam format JPG, JPEG, PNG atau PDF dengan ukuran maksimal 3MB.
              </p>
              {errImage === "ktp_foto" && (
                <div
                  className="error-input"
                  style={{ marginBottom: "10px", marginTop: "-10px" }}
                >
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  {textErr}
                </div>
              )}
              <label htmlFor="input1" className="btn btn-input">
                Pilih File
              </label>
              <input
                type="file"
                id="input1"
                className={styles.buttonSelectImage}
                onChange={(e) => imageChange(e, "ktp_foto", 3)}
              />
            </div>
          </div>
        </div>
        <h6
          className="mb-2 pb-2"
          style={{
            fontSize: "14px",
            fontFamily: "FuturaBT",
            fontWeight: "700",
            color: "#00193E",
          }}
        >
          Unggah Pas Foto
        </h6>
        <div className={`d-flex ${styles.cardImageUpload}`}>
          <div className="me-3" style={{ width: "100px", height: "100px" }}>
            <div
              className={"ratio ratio-1x1 photo_input " + styles.displayImage}
            >
              <a 
                data-bs-toggle="modal" 
                data-bs-target="#modalBerkasKPR" 
                href="#" 
                onClick={() => setImageShowed(pasFoto?.file || generateImageUrl(imagePas?.split("|")[1]))}
                style={pasFoto?.file || imagePas ? {} : { pointerEvents: 'none' }}
              >
                <embed
                  src={pasFoto?.file || generateImageUrl(imagePas?.split("|")[1])}
                  alt=""
                  className={styles.imagePrev}
                />
              </a>
            </div>
          </div>
          <div>
            <p
              style={{
                fontFamily: "Helvetica",
                fontWeight: "400",
                fontSize: "14px",
                color: "#000000",
              }}
            >
              Unggah file dalam format JPG, JPEG, PNG atau PDF dengan ukuran maksimal 3MB.
            </p>
            {errImage === "pas_foto" && (
              <div
                className="error-input"
                style={{ marginBottom: "10px", marginTop: "-10px" }}
              >
                <i
                  class="bi bi-exclamation-circle-fill"
                  style={{ marginRight: "5px" }}
                ></i>
                {textErr}
              </div>
            )}
            <label htmlFor="input2" className="btn btn-input">
              Pilih File
            </label>
            <input
              type="file"
              id="input2"
              className={styles.buttonSelectImage}
              onChange={(e) => imageChange(e, "pas_foto", 3)}
            />
          </div>
        </div>

        {/* Fitur Tambahan */}
        <h6
          className="mb-2 pb-2"
          style={{
            fontSize: "14px",
            fontFamily: "FuturaBT",
            fontWeight: "700",
            color: "#00193E",
          }}
        >
          Unggah Dokumen Slip Penghasilan
        </h6>
        <div className={`d-flex ${styles.cardImageUpload}`}>
          <div className="me-3" style={{ width: "100px", height: "100px" }}>
            <div
              className={"ratio ratio-1x1 photo_input " + styles.displayImage}
            >
              <a 
                data-bs-toggle="modal" 
                data-bs-target="#modalBerkasKPR" 
                href="#" 
                onClick={() => setImageShowed(slipFoto?.file || generateImageUrl(imageSlip?.split("|")[1]))}
                style={slipFoto?.file || imageSlip ? {} : { pointerEvents: 'none' }}
              >
                <embed
                  src={
                    slipFoto?.file || generateImageUrl(imageSlip?.split("|")[1])
                  }
                  alt=""
                  className={styles.imagePrev}
                />
              </a>
            </div>
          </div>
          <div>
            <p
              style={{
                fontFamily: "Helvetica",
                fontWeight: "400",
                fontSize: "14px",
              }}
            >
              Unggah file dalam format JPG, JPEG, PNG atau PDF dengan ukuran maksimal 3MB.
            </p>
            {errImage === "slip_foto" && (
              <div
                className="error-input"
                style={{ marginBottom: "10px", marginTop: "-10px" }}
              >
                <i
                  class="bi bi-exclamation-circle-fill"
                  style={{ marginRight: "5px" }}
                ></i>
                {textErr}
              </div>
            )}
            <label htmlFor="input3" className="btn btn-input">
              Pilih File
            </label>
            <input
              type="file"
              id="input3"
              className={styles.buttonSelectImage}
              onChange={(e) => imageChange(e, "slip_foto", 3)}
            />
          </div>
        </div>

        <h6
          className="mb-2 pb-2"
          style={{
            fontSize: "14px",
            fontFamily: "FuturaBT",
            fontWeight: "700",
            color: "#00193E",
          }}
        >
          Unggah Dokumen Rekening Koran / Tabungan 3 Bulan Terakhir
        </h6>
        <div className={`d-flex ${styles.cardImageUpload}`}>
          <div className="me-3" style={{ width: "100px", height: "100px" }}>
            <div
              className={"ratio ratio-1x1 photo_input " + styles.displayImage}
            >
              <a 
                data-bs-toggle="modal" 
                data-bs-target="#modalBerkasKPR" 
                href="#" 
                onClick={() => setImageShowed(rekFoto?.file || generateImageUrl(imageRek?.split("|")[1]))}
                style={rekFoto?.file || imageRek ? {} : { pointerEvents: 'none' }}
              >
                <embed
                  src={rekFoto?.file || generateImageUrl(imageRek?.split("|")[1])}
                  alt=""
                  className={styles.imagePrev}
                />
              </a>
            </div>
          </div>
          <div>
            <p
              style={{
                fontFamily: "Helvetica",
                fontWeight: "400",
                fontSize: "14px",
              }}
            >
              Unggah file dalam format JPG, JPEG, PNG atau PDF dengan ukuran maksimal 3MB.
            </p>
            {errImage === "rek_foto" && (
              <div
                className="error-input"
                style={{ marginBottom: "10px", marginTop: "-10px" }}
              >
                <i
                  class="bi bi-exclamation-circle-fill"
                  style={{ marginRight: "5px" }}
                ></i>
                {textErr}
              </div>
            )}
            <label htmlFor="input4" className="btn btn-input">
              Pilih File
            </label>
            <input
              type="file"
              id="input4"
              className={styles.buttonSelectImage}
              onChange={(e) => imageChange(e, "rek_foto", 3)}
            />
          </div>
        </div>
        {/* Fitur Tambahan */}

        <div className="mt-5">
          <div style={{ display: "flex" }}>
            <div className="me-3" style={{ width: "119px", height: "48px" }}>
              <button
                style={{
                  width: "119px",
                  height: "48px",
                  padding: "0",
                  fontWeight: "700",
                }}
                type="button"
                className="btn btn-outline-main"
                onClick={() => props.setStepCredit(2)}
              >
                Sebelumnya
              </button>
            </div>
            <div style={{ width: "119px", height: "48px" }}>
              <button
                style={{ width: "119px", height: "48px", fontWeight: "700", paddingTop: isMacOs ? 12 : 6 }}
                type="submit"
                className="btn btn-main px-3"
                disabled={
                  (!pasFoto?.file && !imagePas) ||
                  (!ktpFoto?.file && !imageKTP) ||
                  (!rekFoto?.file && !imageRek) ||
                  (!slipFoto?.file && !imageSlip)
                    ? true
                    : false
                }
              >
                Selanjutnya
              </button>
            </div>
          </div>
        </div>
      </form>

      <div
        className="modal fade "
        id="modalBerkasKPR"
        aria-hidden="true"
        aria-labelledby="modalBerkasKPR"
        tabIndex="-1"
      >
        <div
          className="modal-dialog modal-auth"
          style={isMobileOrTablet ? 
            { marginTop: 58, marginBottom: 57, maxWidth: "50%" } :
            { marginTop: 58, marginBottom: 57, maxHeight: "50%" }
          }
        >
          <div className="modal-content" style={{ backgroundClip: "transparent" }}>
            <div style={{ borderRadius: "20px" }}>
              <img
                src={imageShowed}
                alt=""
                className={styles.imagePrev}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const detectImage = (url) => {
  let dataImage = new Image();
  dataImage.src = url;

  dataImage.onload = () => {
    if (dataImage.height > dataImage.width) {
      return "vertical";
    } else {
      return "horizontal";
    }
  };
};

const generateImageUrl = (url) => {
  if (!url) return "/images/icons/thumb.png";

  let fullUrl = `https://www.btnproperti.co.id/cdn1/files/${url.split('"')[0]}`;
  fullUrl = fullUrl.replaceAll(" ", "%20");

  var img = new Image();
  const getImage = async ()=>{
    img.src = fullUrl;
    await img.decode();
    return img.height
     };
  const isImageExist = getImage() != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/icons/thumb.png";
  }
};
