import Link from "next/link";
import NumberFormat from "react-number-format";
import ReactImageFallback from "react-image-fallback";
import { useRouter } from "next/router";
import { useState } from "react";
import { useEffect } from "react";
import LikeButton from "../element/LikeButton";
import { deleteIklan } from "../../services/member";
import { useAppContext } from "../../context";
import { useMediaQuery } from "react-responsive";

export default function ItemUnit(props) {
  const isTabletOrMobile = useMediaQuery({
    query: "(min-width: 768px) and (max-width: 948px)",
  });
  const { data, compare, index, selected, getProperti } = props;
  const { jns } = data;
  const { userKey, userProfile } = useAppContext();
  const router = useRouter();
  const { komparasi } = router.query;
  const tipeProperti =
    jns == "1" ? "RUMAH" : jns == "2" ? "APARTEMEN" : "UNCATEGORIZED";
  const tipeLow = tipeProperti.toLowerCase();
  const tipeSlug = tipeLow != "uncategorized" ? tipeLow : "";
  const [borderColor, SetBorderColor] = useState(false);

  const formatter = (e) => {
    const million = 1000000;
    if (e >= million) {
      return `${Math.floor(e / million)}jt/bln`;
    } else {
      return `${e}/bln`;
    }
  };

  const CSSVariables = {
    border: {
      border: "2px solid rgba(0, 97, 167, 0.75)",
    },
    noBorder: {
      border: "none",
    },
  };

  const compareSelect = (data) => {
    if (!borderColor) {
      if (compare.length != 4) {
        //SetBorderColor(true);
        props.dataCompare(data);
      }
    }
  };

  useEffect(() => {
    if (komparasi == "1") {
      SetBorderColor(false);
      if (compare?.length > 0) {
        compare
          .filter((list) => list.id.includes(data.id))
          .map((filtered) => {
            SetBorderColor(true);
          });
      }
    }
  }, [compare]);

  const handleDelete = (id) => {
    deleteIklan(id, userKey)
      .then((res) => {
        getProperti();
      })
      .catch((error) => {});
  };

  return (
    <div>
      {komparasi == "1" ? (
        <div
          key={data.id}
          className="card card_unit_properti"
          onClick={() => compareSelect(data)}
          style={borderColor ? CSSVariables.border : CSSVariables.noBorder}
        >
          <div className="card_img">
            <div className="ratio ratio-1x1">
              <a>
                <ReactImageFallback
                  src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.gbr1}
                  fallbackImage="/images/thumb-placeholder.png"
                  className="img-fluid"
                />
              </a>
            </div>
            <a className="link">
              <div
                className={"kategori " + (tipeSlug || "rumah")}
                style={{ fontSize: "12px", textTransform: "uppercase" }}
              >
                {tipeProperti}
              </div>
            </a>
            <LikeButton propId={data.id} />
          </div>

          <div className="card-body">
            <h6
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: "10px",
                lineHeight: "130%",
              }}
              className="lokasi"
            >
              {data.lokasi || data.n_kot || "-"}
            </h6>
            <a>
              <h5
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "700",
                  fontSize: "20px",
                  lineHeight: "150%",
                }}
                className="title"
              >
                {data.clstr} {data.n}
              </h5>
            </a>
            <h5
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: isTabletOrMobile ? "13px" : "16px",
                lineHeight: "160%",
              }}
              className="price"
            >
              <NumberFormat
                value={data.hrg_rdh}
                displayType={"text"}
                prefix={"Rp"}
                thousandSeparator="."
                decimalSeparator=","
              />
            </h5>
            <div
              style={{
                fontFamily: "Helvetica",
                fontWeight: "400",
                fontSize: "12px",
                lineHeight: "160%",
              }}
              className="cicilan"
            >
              Cicilan dari{" "}
              <span
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "700",
                  fontSize: "12px",
                  lineHeight: "160%",
                }}
              >
                <NumberFormat
                  value={data.pmt_konvensional ? formatter(data.pmt_konvensional) : ""}
                  displayType={"text"}
                  prefix={"Rp"}
                  thousandSeparator="."
                  decimalSeparator=","
                  suffix=" Jt/bln"
                />
              </span>
              <span
                className="divider"
                style={{
                  borderRight: "1px solid #666666",
                  height: "12px",
                  margin: "0 8px",
                  display: "inline-block",
                  transform: "translateY(2px)",
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  fontSize: "12px",
                  lineHeight: "160%",
                }}
              />
              Suku Bunga dari{" "}
              <span
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "700",
                  fontSize: "12px",
                  lineHeight: "160%",
                }}
                className="price"
              >
                {data.sk_bga}%
              </span>
            </div>
            <a>
              <div className="developer">
                <div className="logo">
                  <img
                    src={
                      `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                      data.g_dev?.replace("1|", "")
                    }
                    className="img-fluid"
                  />
                </div>
                <div
                  className="developer-name"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  {data.n_dev}
                </div>
              </div>
            </a>
          </div>
          {props.detail == "true" && (
            <div className="card-footer">
              <ul className="nav nav-pills nav-fill">
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/bed.png"
                    className="img-fluid"
                    height="22px"
                    width="22px"
                  />{" "}
                  {data.km_tdr}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/shower.png"
                    className="img-fluid"
                    height="17.14px"
                    width="17.14px"
                  />
                  {data.km_mnd}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/lb.png"
                    className="img-fluid"
                    height="22px"
                    width="22px"
                  />
                  {data.ls_bgn}m<sup>2</sup>
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/lt.png"
                    className="img-fluid"
                    height="22px"
                    width="22px"
                  />
                  {data.ls_tnh}m<sup>2</sup>
                </li>
              </ul>
            </div>
          )}
          {/* <div className="bandingkan">
                <Link href="/tools/komparasi">Bandingkan</Link>
              </div> */}
        </div>
      ) : (
        <div className="card card_unit_properti">
          <div className="card_img">
            <div className="ratio ratio-1x1">
              <Link href={{pathname: "/property/tipe/detail", query: {id: data.id}}}>
                <a>
                  <ReactImageFallback
                    src={
                      data.images != null
                        ? data.images
                        : data.gbr1
                        ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                          data.gbr1.replace("1|", "")
                        : null
                    }
                    fallbackImage="/images/thumb-placeholder.png"
                    className="img-fluid"
                    title={data.clstr + " " + data.n}
                    style={{ width: "100%", objectFit: "cover" }}
                  />
                </a>
              </Link>
            </div>
            <Link
              href={{
                pathname: "/property",
                query: { tipe: tipeSlug },
              }}
              passHref
            >
              <a className="link">
                <div
                  className={"kategori " + (tipeSlug || "rumah")}
                  style={{ fontSize: "12px", textTransform: "uppercase" }}
                >
                  {data.kategori != null ? data.kategori : tipeProperti}
                </div>
              </a>
            </Link>
            {/* { typeof window !== "undefined" && window.location.href == `${window.location.origin}/member/favorit/` ? (
              <div className="like" style={{ lineHeight: "44px" }}>
              <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M12.7499 0.169922C11.2998 0.169922 9.90818 0.755837 8.99985 1.68173C8.09151 0.755837 6.69984 0.169922 5.24984 0.169922C2.68317 0.169922 0.666504 1.92043 0.666504 4.14835C0.666504 6.87563 3.48537 9.09915 7.75862 12.4699L7.79151 12.4958L8.99985 13.4434L10.2082 12.4886L10.2187 12.4803C14.5045 9.10689 17.3332 6.88038 17.3332 4.14835C17.3332 1.92043 15.3165 0.169922 12.7499 0.169922Z"
                  fill="#DC0714"
              />
              </svg>
              </div>
            ) : ( */}
            <LikeButton propId={data.id} />
            {/* )} */}
          </div>

          <div className="card-body">
            <h6
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: "10px",
                lineHeight: "130%",
              }}
              className="lokasi"
            >
              {data.lokasi || data.n_kot || "-"}
            </h6>

            <Link href={{pathname: "/property/tipe/detail", query: {id: data.id}}} passHref>
              {data.title != null || data.jdl != null ? (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={data.title || data.jdl}
                  >
                    {data.title || data.jdl}
                  </h5>
                </a>
              ) : (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={data.clstr + " " + data.n}
                  >
                    {data.clstr} |{data.n_perum}
                  </h5>
                </a>
              )}
            </Link>
            <h5
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: isTabletOrMobile ? "13px" : "16px",
                lineHeight: "160%",
              }}
              className="price"
            >
              <NumberFormat
                value={
                  data.price != null || data.hrg != null
                    ? data.price || data.hrg
                    : data.hrg_rdh != null
                    ? data.hrg_rdh * 1
                    : "-"
                }
                displayType={"text"}
                prefix={"Rp"}
                thousandSeparator="."
                decimalSeparator=","
              />
            </h5>
            <div
              style={{
                fontFamily: "Helvetica",
                fontWeight: "400",
                fontSize: "12px",
                lineHeight: "160%",
              }}
              className="cicilan"
            >
              Cicilan dari{" "}
              <span
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "700",
                  fontSize: "12px",
                  lineHeight: "160%",
                }}
              >
                {" "}
                {data.cicilan != null ? (
                  <>
                    <span
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "12px",
                        lineHeight: "160%",
                      }}
                      className="price"
                    >
                      {data.cicilan}
                    </span>
                  </>
                ) : (
                  <>
                    <NumberFormat
                      value={formatter(data.pmt_konvensional)}
                      displayType={"text"}
                      prefix={"Rp"}
                      suffix=" Jt/bln"
                      thousandSeparator="."
                      decimalSeparator=","
                    />
                  </>
                )}
              </span>
              <span
                id="divider_card"
                style={{
                  position: "relative",
                  top: "3px",

                  borderRight: "1px #666666 solid",
                  height: "12px",
                  width: "0px",
                }}
              />
              Suku Bunga dari{" "}
              <span
                id="item_bunga"
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "700",
                  fontSize: "12px",
                  lineHeight: "160%",
                }}
                className="price"
              >
                {data.suku_bunga != null ? data.suku_bunga : data.sk_bga}%
              </span>
            </div>
            <Link href={
          {
            pathname:"/developer/detail",
            query: {
              id: (data.i_dev || data.i_memb)
            }
          }
        } passHref>
              <a>
                <div className="developer">
                  <div className="logo">
                    {/* <img
                      src={ data.developer_avatar != null ? data.developer_avatar :
                        `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                        data.g_dev?.replace("1|", "")
                      }
                      className="img-fluid"
                    /> */}
                    <ReactImageFallback
                      src={
                        data.developer_avatar || data.g_memb
                          ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                            (data.developer_avatar || data.g_memb).replace(
                              "1|",
                              ""
                            )
                          : null
                      }
                      fallbackImage="/images/thumb-placeholder.png"
                      className="img-fluid"
                    />
                  </div>
                  <div
                    className="developer-name"
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "400",
                      fontSize: "12px",
                      lineHeight: "160%",
                    }}
                  >
                    {data.developer_name != null
                      ? data.developer_name
                      : data.n_dev || data.n_memb}
                  </div>
                </div>
              </a>
            </Link>
          </div>
          {props.detail == "true" && (
            <div className="card-footer">
              <ul className="nav nav-pills nav-fill">
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/bed.png"
                    className="img-fluid"
                    height="22px"
                    width="22px"
                  />{" "}
                  {data.kamar_tidur != null ? data.kamar_tidur : data.km_tdr}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/shower.png"
                    className="img-fluid"
                    height="17.14px"
                    width="17.14px"
                  />
                  {data.kamar_mandi != null ? data.kamar_mandi : data.km_mnd}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/lb.png"
                    className="img-fluid"
                    height="22px"
                    width="22px"
                  />
                  {data.lb != null ? data.lb : data.ls_bgn}m<sup>2</sup>
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img
                    src="/images/icons/lt.png"
                    className="img-fluid"
                    height="22px"
                    width="22px"
                  />
                  {data.ls_tnh != null ? data.ls_tnh : data.lt}m<sup>2</sup>
                </li>
              </ul>
            </div>
          )}
          {props.action == "true" && (
            <div className="card-action">
              <div className="row">
                <div className="col">
                  <Link href={`/member/update-properti?id=${data.id}`}>
                    Edit
                  </Link>
                </div>
                <div className="col">
                  <Link href="#">
                    <a
                      href=""
                      data-bs-toggle="modal"
                      data-bs-target={"#delete" + data.id}
                    >
                      Hapus
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          )}
          {props.banding == "true" && (
            <div className="bandingkan">
              <Link href="/tools/komparasi">Bandingkan</Link>
            </div>
          )}
        </div>
      )}
      <div
        className="modal fade"
        id={"delete" + data.id}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabIndex="-1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered ">
          <div className="modal-content modal_delete">
            <div className="close-modal" data-bs-dismiss="modal">
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <div className="modal-body text-center pt-3">
              <h5 className="modal-title">Hapus Iklan Properti</h5>
              <p>Apakah Anda akan menghapus iklan ini?</p>

              <button
                onClick={() => handleDelete(data.id)}
                type="button"
                data-bs-dismiss="modal"
                className="btn btn-primary w-100 mt-4 mb-3"
              >
                Hapus
              </button>
              <a type="button" data-bs-dismiss="modal">
                Batal
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
