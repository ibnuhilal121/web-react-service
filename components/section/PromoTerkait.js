import React from "react";
import data_terkait from "../../sample_data/data_promo_terkait";
import ItemPromo from "../data/ItemPromo";
import Link from "next/link";
import Chevron from "../element/icons/Chevron";
import { useMediaQuery } from "react-responsive";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

export default function PromoTerkait({data}) {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1024px)" });
  const settings = {
    dots: false,
    arrows: false,
    infinite: false,
    //centerMode: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          // centerMode: true,
          // centerPadding: '20%',
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          // centerMode: true,
          // centerPadding: '20%',
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          // centerMode: true,
          centerPadding: "17px",
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  console.log("terkait", data);

  return (
    <section id="listing_populer">
      <div className="container">
        {isTabletOrMobile ? (
          <>
            <div style={{ paddingLeft: "-14px" }}>
              <div className="header_listing">
                <h4>Promo Terkait</h4>
              </div>
              <div>
              <Slider {...settings} className="card_property_minimal_container">
                {data.map((item, index) => (
                  <div key={index}>
                    <ItemPromo data={item} />
                  </div>
                ))}
              </Slider>
              </div>
            </div>
            <div style={{paddingBottom : "40px"}}>
              <Link href={"/promo?kategori=" + data[0].kategoriId}>
                <a
                  style={{
                    position: "absolute",
                    display: "flex",
                    marginRight: 8,
                    width: "95%",
                  }}
                  className="btn btn-outline-primary d-block d-sm-block"
                >
                  Lihat Semua
                </a>
              </Link>
            </div>
          </>
        ) : (
          <>
            <div className="header_listing">
              <h4>Promo Terkait</h4>
              <Link href={"/promo?kategori=" + data[0].kategoriId}>
                <a style={{ display: "flex" }}>
                  <span >Lihat Semua</span>
                  <Chevron />
                </a>
              </Link>
            </div>
            <div className="row g-2 list_property">
              {data.map((item, index) => (
                <div key={index} className="col-md-3">
                  <ItemPromo data={item} />
                </div>
              ))}
            </div>
          </>
        )}
      </div>
    </section>
  );
}
