import MinimalProperty from "../data/MinimalProperty";
import data_subsidi from "../../sample_data/data_property_subsidi";
import Link from "next/link";
import PropertySlide from "../carousel/PropertySlide";

export default function PropertySubsidi({ propSubsidi }) {
  return (
    <section id="listing_subsidi">
      <div className="container">
        <div className="header_listing">
          <h4 className="fw-bold" style={{ fontFamily: "futuraBT" }}>
            Properti bersubsidi pilihan
          </h4>
          <Link href="/property?subsidi=1">
            <a className="d-none d-sm-none d-md-block">
              <span>Lihat Semua </span>
              <img src="/images/icons/chevron-right.svg" />
            </a>
          </Link>
        </div>
        <div className="list_property">
          <PropertySlide data_all={propSubsidi} />
        </div>
        <Link href="/property?subsidi=1">
          <a className="btn btn-outline-primary d-block d-sm-block d-md-none fw-bold">
            Lihat Semua
          </a>
        </Link>
      </div>
    </section>
  );
}
