import React from "react";
import IconPlay from "../element/icons/play";

export default function Cerita() {
  return (
    <div>
      <section id="home_cerita">
        <div className="container">
          <div id="cerita_content">
            {/* <div> */}
            {/* <img src="/images/bg/cerita.png" className="img-fluid" style={{width:1280,height:545,left:0,top:-3}}/></div> */}
            <img src="/images/btn_yellow.svg" />
            <div className="row">
              <div className="col-md-6 col-lg-6 desc_cerita">
                <h6>CERITA MEREKA</h6>
                <h3>Bagaimana Febrina memanfaatkan konsep slow decorating</h3>
                <button
                  type="button"
                  className="btn btn-main"
                  data-bs-toggle="modal"
                  data-bs-target="#modalVideo"
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      width: 153,
                      height: 52,
                    }}
                  >
                    <div style={{ marginLeft: "15px" }}>
                      <IconPlay />
                    </div>
                    <div
                      style={{ marginLeft: 6, fontWeight: 700, fontSize: 16 }}
                    >
                      Putar Video
                    </div>
                  </div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="modal fade" id="modalVideo">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body p-0">
              <iframe
                width="100%"
                height="315"
                src="https://www.youtube.com/embed/eWTvPU2wcYQ"
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
