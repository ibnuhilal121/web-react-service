import Link from "next/link";
import { useMediaQuery } from "react-responsive";

export default function Breadcrumb(props) {
  const { active } = props;
  const isTabletOrMobile = useMediaQuery({
    query: "(min-width: 769px) and (max-width: 948px)",
  });
  const isMobile = useMediaQuery({ query: `(max-width: 576px)` });

  function truncate(source, size) {
    return source?.length > size ? source.slice(0, size - 0) + "…" : source;
  }

  return (
    <div>
      <div
        className={`breadcrumb_page ${
          isTabletOrMobile ? "breadcrumb-pt" : null
        }`}
        // style={isTabletOrMobile ? { paddingTop: "170px" } : null}
        // style={{ fontFamily: "FuturaBT", fontWeight: 700, lineHeight: "150%" }}
      >
        <div className="container">
          <nav
            aria-label="breadcrumb"
            // style={{ fontFamily: "FuturaBT", fontWeight: 700, lineHeight: "150%" }}
          >
            <ol className="breadcrumb ">
              <li
                className={`breadcrumb-item ${props.classes}`}
                // style={{
                //   fontSize: "12px",
                //   fontFamily: "FuturaBT",
                //   fontWeight: 700,
                //   lineHeight: "150%",
                // }}
              >
                <Link href="/">Beranda</Link>
              </li>
              {props.children}
              <li
                className={`breadcrumb-item ${
                  props.classes === "FuturaStyleA" ? "activeFutura" : "active"
                }`}
                aria-current="page"
                // style={{
                //   fontSize: "12px",
                //   fontFamily: "FuturaBT",
                //   fontWeight: 700,
                //   lineHeight: "150%",
                // }}
              >
                {isMobile ? truncate(active, 17) : active}
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  );
}
