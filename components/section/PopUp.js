import React, { useEffect, useState } from 'react'
import { Box, Typography, Dialog, TextField, Button } from "@material-ui/core";
import { Image } from "react-bootstrap";
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';
import Link from "next/link";
import { useAppContext } from "../../context";
import {useRouter} from "next/router"

function PopUp() {
  const { userKey } = useAppContext();
  const history = useRouter();
  const useStyles = makeStyles((theme) => ({
    paper: {
      overflowY: 'unset',
    },
    customizedButton: {
      position: 'absolute',
      left: '96%',
      top: '-2%',
      backgroundColor: '#eeeeee',
      padding: '2px',
      color: 'black',
      '&:hover': {
        backgroundColor: '#eeeeee',
        color: 'black',
      },
      '& svg': {
        fontSize: 15
      }
    },
  }));
    const classes = useStyles();
    const [showAd, setShowAd] = useState(true);

    const openAd = () => setWelcomeMessage(true);
    const closeAd = () => {
      setWelcomeMessage(false)
      setShowAd(false)
    };
    let token = localStorage.getItem("generate-url")

    const redirect = () => {
      localStorage.setItem("navigate","https://expo.btnproperti.co.id/visitor/auth/external?token=")
      localStorage.setItem("title", "Virtual Expo")
      localStorage.getItem("generate-url")
      const title = localStorage.getItem("title");
      // navigate
      history.push(!userKey ? "" : "https://expo.btnproperti.co.id/visitor/auth/external?token=" + localStorage.getItem("generate-url"));
      // if (!userKey && res.isAuth) {
      //   history.push("")
      // } else if(title == "Pre Approval"){
      //   history.push(res.navigateWeb + "link" + localStorage.getItem("key"))
      // } else{
      //   history.push(res.navigateWeb)
      // }
    }
    useEffect(() => {
      console.log("open is changed")
      if(showAd){
        document.getElementsByTagName('html')[0].style.overflow = "hidden";
      }else{
        document.getElementsByTagName('html')[0].style.overflow = "auto";
      }
    },[showAd])  
    
    let [welcomeMessage, setWelcomeMessage] = useState(<Dialog
        open={showAd} 
        onClose={closeAd}
        BackdropProps={{style: {backgroundColor: 'transparent'}}}
        aria-labelledby="draggable-dialog-title"
        classes={{ paper: classes.paper }}
      >
         <a href={`https://expo.btnproperti.co.id/visitor/auth/external?token=` + localStorage.getItem("generate-url")} onClick={() => redirect()}>
          <a
                          data-bs-toggle={!userKey ? "modal" : null}
                          data-bs-target={!userKey ? "#modalLogin" : null}
                          style={{
                            fontFamily: "Helvetica",
                          }}
                          onClick={closeAd}
                        >
          <Image
                  src="/images/modal/popup-kpr.jpg"
                  layout='fill'
                  width='300PX'
                  max-width='300px'
                  height='auto'
                  position='relative'
                  objectFit='contain'
              />
              </a>
        </a>
          <IconButton
            onClick={closeAd}
            color="primary"
            className={classes.customizedButton}
          >
            <CloseIcon />
          </IconButton>
      </Dialog>
      )

  useEffect(() => {
    // checking if localStorage has a "hasVisited" key
    if (sessionStorage.getItem("hasVisited")){
       setWelcomeMessage("")  
       setShowAd(false)
    } else {
        // creating the "hasVisited" key value pair in sessionStorage if it doesnt exist
      sessionStorage.setItem("hasVisited", "true")
    }
    // we are only running this useEffect on the first render of app because we passed an
    // empty array
  },[])

  return (
    <div className="App">
    {welcomeMessage}
    </div>
  );
}

export default PopUp;