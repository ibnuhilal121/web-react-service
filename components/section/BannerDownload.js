import React from "react";

export default function BannerDownload() {
  return (
    <div className="section" id="banner_download">
      <div className="container">
        <div className="download_content ">
          <div className="row" style={{ marginTop: "-11px" }}>
            <div className="col-md-7 mt-0">
              <div className="title">Siap untuk memulai?</div>
              <div className="title title-memulai"> </div>
              <img className="image-memulai" src="/images/acc/title.svg" />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 m-banner-download">
              <p className="paragraf-memulai">
                Download aplikasi BTN Properti untuk mendapatkan update terkini
                mengenai promo dan listing terbaru.
              </p>
              <div className="d-flex mt-4">
                <div className="me-3">
                  <a
                    href="https://play.google.com/store/apps/details?id=btn.properti.android&hl=in"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img
                      src="/images/link/playstore.png"
                      className="img-fluid"
                    />
                  </a>
                </div>
                <div className="mr-3">
                  <a
                    href="https://apps.apple.com/id/app/btn-properti-mobile/id1641042026?l=id"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img
                      src="/images/link/appstore.png"
                      className="img-fluid"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>

          {/* <div className="acc_2"></div>
                    <div className="acc_3"></div>
                    <div className="acc_1">
                        <img src="/images/acc/download.png" className="img-fluid" />
                    </div> */}
        </div>
      </div>
    </div>
  );
}
