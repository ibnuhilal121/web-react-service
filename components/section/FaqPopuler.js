import ItemFaq from "../data/ItemFaq";
import SearchIcon from "../../components/element/icons/SearchIcon";

export default function FaqPopuler({data}) {
    const NotFound = () => {
        return (
          <div
            style={{
              paddingTop: 100,
              textAlign: "center",
            }}
          >
            <SearchIcon />
            <div
              style={{
                fontFamily: "Helvetica",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "16px",
                lineHeight: "150%",
                marginTop: 40,
              }}
            >
              Maaf, Keyword yang kamu cari tidak ditemukan
            </div>
          </div>
        );
    };

    return (
        <section className="faq_populer">
            <div className="container">
                <div className="row">
                    <div className="col-md-10 offset-md-1">
                        <h4 className="text-center title_section">Pertanyaan yang Sering Ditanyakan</h4>
                        <div className="accordion" id="accordionExample">
                            {(data && data.length > 0) ?
                            (
                                data.map((item, index) =>
                                <div key={index}>
                                    <ItemFaq  data={item} />
                                </div>)   
                            ) : NotFound()}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
