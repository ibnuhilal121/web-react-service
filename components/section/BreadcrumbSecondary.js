import React from 'react'
import Link from "next/link";

export default function BreadcrumbSecondary(props) {
    const { active } = props;
  return (
    <div>
      <div
        className={`breadcrumb_page ${props.classes?.page}`}
        style={{ fontFamily: "Helvetica", fontWeight: "bold", lineHeight: "150%" }}
      >
        <div className="container">
          <nav
            aria-label="breadcrumb"
            style={{ fontFamily: "Helvetica", fontWeight: "bold", lineHeight: "150%" }}
          >
            <ol className="breadcrumb" style={{ flexWrap: 'nowrap' }}>
              <li
                className="breadcrumb-item"
                style={{
                  fontSize: "12px",
                  fontFamily: "Helvetica",
                  fontWeight: "bold",
                  lineHeight: "150%",
                }}
              >
                <Link
                  href="/"
                  style={{ fontFamily: "Helvetica", fontWeight: "bold" }}
                >
                  Beranda
                </Link>
              </li>
              {props.children}
              <li
                className="breadcrumb-item active"
                aria-current="page"
                style={{
                  fontSize: "12px",
                  fontFamily: "Helvetica",
                  fontWeight: "bold",
                  lineHeight: "150%",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis"
                }}
              >
                {active}
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  );
}
