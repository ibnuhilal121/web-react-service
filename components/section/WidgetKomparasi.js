import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import Link from "next/link";
import data_comparasi from "../../sample_data/data_komparasi";
import ReactImageFallback from "react-image-fallback";
import AddComparasi from "../static/AddComparasi";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";

export default function WidgetKomparasi(props) {
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const { data,deleteCompare } = props;
  const router = useRouter();
  const [dataCompare, setDataCompare] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [selectImage, setSelectImage] = useState(false);

  const setValue = async () => {
    if (data?.length < 2) {
      console.log("Pilih 2 hingga 4 properti!");
    } else {
      let array = [];
      setDataCompare([]);
      var promises = data.map((data, index) => {
        array.push(data.ID);
      });
      await Promise.all(promises).then(() => {
        //setDataCompare(dataCompare => [...dataCompare,data.id])
        setDataCompare(array);
      });
    }
  };

  useEffect(() => {
    if (data?.length >= 0) {
      setSelectImage(true)
    }
  }, []);

  useEffect(() => {
    //submitCompare()
    if (dataCompare.length >= 2) {
      router.push({
        pathname: "/tools/komparasi",
        query: { data: dataCompare },
      });
    }
  }, [dataCompare]);

  // const DeleteButton = (props) => {
  //   const { id } = props;
  //   console.log(id)
  //   return (
      
  //   );
  // };

  return (
    <Layout title="Daftar Properti | BTN Properti" isLoaderOpen={isLoading}>
      <div 
        id="widget_komparasi" 
        className="fixed-bottom" 
        style={isTabletOrMobile ? { borderRadius: '32px 32px 0px 0px' } : {}}
      >
        <div className="container-fluid">
          <div className="row d-flex align-items-center">
            <div className="col-md-3">
              <h5
                style={{
                  fontFamily: isTabletOrMobile ? "Futura" : "FuturaBT",
                  fontWeight: "700",
                  fontSize: isTabletOrMobile ? "16px" : "20px",
                  color: isTabletOrMobile ? "#00193E" : "#000000",
                  marginTop: isTabletOrMobile ? '24px' : 0,
                  textAlign: isTabletOrMobile ? 'center' : 'left'
                }}
              >
                Bandingkan Properti
              </h5>
              <p
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  fontSize: "14px",
                  color: isTabletOrMobile ? "#00193E" : "#666666",
                  textAlign: isTabletOrMobile ? 'center' : 'left'
                }}
              >
                Pilih 2 hingga 4 properti yang kamu inginkan untuk lihat
                perbandingannya
              </p>
            </div>
            <div className="col-md-9">
              <div className={`d-flex ${isTabletOrMobile ? 'flex-column justify-content-center' : 'justify-content-between'} align-items-center`}>
                {data?.length > 0 ? (
                  <div 
                    className={`list_komparasi ${isTabletOrMobile ? 'd-flex justify-content-center align-items-center' : ''}`}
                  >
                    {data?.map((data, index) => {
                      return (
                        <div key={index} className="item_thumb_komparasi" style={isTabletOrMobile ? { height: '61.5px', width: '61.5px', padding: 0, margin: '0 5px' } : {}}>
                          <div className="img_thumb">
                            {/* <img src={data.images} className="img-fluid"/> */}
                            <ReactImageFallback
                              src={
                                `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                                data.GBR1?.split("|")[1]
                              }
                              fallbackImage="/images/thumb-placeholder.png"
                              className="img-fluid"
                              style={selectImage ? { height: '72px', width: '116px', objectFit: 'cover', } : {}}
                            />
                           <div
                              onClick={() => deleteCompare(data)}
                              style={{ position: "absolute", top: "-10px", right: "-25px" }}
                              >
                              <svg
                                width="40"
                                height="40"
                                viewBox="0 0 40 40"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                >
                                <circle cx="10" cy="10" r="10" fill="#EDF4FF" />
                                <path
                                  fill-rule="evenodd"
                                  clip-rule="evenodd"
                                  d="M6.3673 13.3673C6.18133 13.1813 6.18133 12.8798 6.3673 12.6939L9.061 10.0002L6.36713 7.3063C6.18117 7.12034 6.18117 6.81883 6.36713 6.63287C6.5531 6.4469 6.85461 6.4469 7.04057 6.63287L9.73443 9.32673L12.7649 6.29623C12.9509 6.11027 13.2524 6.11027 13.4384 6.29623C13.6243 6.48219 13.6243 6.7837 13.4384 6.96967L10.4079 10.0002L13.4382 13.0305C13.6242 13.2165 13.6242 13.518 13.4382 13.7039C13.2522 13.8899 12.9507 13.8899 12.7648 13.7039L9.73443 10.6736L7.04073 13.3673C6.85477 13.5533 6.55326 13.5533 6.3673 13.3673Z"
                                  fill="#0061A7"
                                />
                              </svg>
                            </div>
                          </div>

                          <h5
                            style={{
                              fontFamily: "Helvetica",
                              fontWeight: "700",
                              fontSize: "12px",
                              color: "#000000",
                            }}
                            className="title"
                          >
                            {data.NAMA_PROPER}
                          </h5>
                        </div>
                      );
                    })}
                    {data.length >= 4 ? null : (
                      <div className="item_thumb_komparasi" style={isTabletOrMobile ? { height: '61.5px', width: '61.5px', padding: 0, margin: '0 5px' } : {}}>
                        <AddComparasi className="p-0" addWidget={true} />
                      </div>
                    )}
                  </div>
                ) : (
                  <div 
                    className={`list_komparasi ${isTabletOrMobile ? 'd-flex justify-content-center align-items-center' : ''}`} 
                    style={isTabletOrMobile ? { height: '61.5px', width: '61.5px' } : {}}
                  >
                    <div 
                      className="item_thumb_komparasi" 
                      style={isTabletOrMobile ? { height: '100%', width: '100%', padding: 0 } : {}}
                    >
                      <AddComparasi className="p-0" addWidget={true} />
                    </div>
                  </div>
                )}

                {/* <Link href="/tools/komparasi">
                                    <a className="bnt btn-main rounded">
                                        Lihat Perbandingan
                                    </a>
                                </Link> */}
                {!isTabletOrMobile &&
                (
                  <a
                    className="bnt btn-main"
                    id="lihatPerbandingan"
                    style={{
                      cursor:"pointer"
                    }}
                    onClick={() => {
                      setValue();
                    }}
                  >
                    Lihat Perbandingan
                  </a>
                )}
              </div>
              {isTabletOrMobile &&
              (
                <a
                  className="btn btn-main d-flex align-items-center justify-content-center"
                  id="lihatPerbandingan"
                  style={{
                    width: "100%",
                    marginBottom: "8px",
                    marginTop: "48px"
                  }}
                  onClick={() => {
                    setValue();
                  }}
                >
                  Lihat Perbandingan
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
