/* eslint-disable react/jsx-key */

import {
  options,
  signIn,
  signOut,
  useSession,
  providers,
} from "next-auth/client";
import Cookies from "js-cookie";
import { doLogin, getEkycData } from "../../../services/auth";
import { useMediaQuery } from "react-responsive";
import { googleLogin,twitterLogin, facebookLogin  } from "../../../helpers/auth"; 

import Link from "next/link";
import NumberFormat from "react-number-format";
import React, { useEffect, useRef, useState,useCallback } from "react";
import numberToTupiah from "../../../utils/numberToRupiah";
import { useAppContext } from "../../../context";
import { useRouter } from "next/dist/client/router";
import axios from "axios";
import QueryString from "qs";
import { checkIsNumber } from "../../../pages/tools/simulasi_kpr";
import Lottie from "react-lottie";
import * as animationData from "../../../public/animate_home.json";
import { defaultHeaders } from "../../../utils/defaultHeaders"
import { validateProfileData } from "../../../helpers/validateProfileData";

let settings;
if (typeof window !== "undefined") {
  settings = JSON.parse(sessionStorage.getItem("settings"));
}

const defData = {
  propertyName: "Amarine Mozia Tipe Sudio B",
  lokasi: "BSD City",
  lb: 95,
  lt: 100,
  price: 2500000000,
  sukuBunga: 8.29,
  sukuBungaSyariah: 8.29,
  jumlahKamarTidur: 2,
  jumlahKamarMandi: 1,
  pmt_konvensional: 1000000,
  pmt_syariah: 1000000,
};

export default function OverflowKpr({
  data = defData,
  dataKavling = [],
  selectedKav = 0,
  onChangeSelectedKav,
  isLoading,
  dataDetail
}) {
  const { userKey, userProfile } = useAppContext();
  const {isVerified = false} = userProfile || {};
  const [jenisKPR, setJenisKpr] = useState(1);
  const [hargaProperti, setHargaProperti] = useState(null);
  const [jumlahPinjaman, setJumlahPinjaman] = useState(0);
  const [jangkawaktu, setJangkaWaktu] = useState(15);
  const [angsuranPerbulan, setaAngsuranPerbulan] = useState(0);
  const [angsuranPerbulanSyariah, setaAngsuranPerbulanSyariah] = useState(0);
  const [uangMuka, setUangMuka] = useState(0);
  const [persenUangMuka, setPersenUangMuka] = useState(15);

  const btnAjukan = useRef();
  const btnCloseAjukan = useRef();
  const router = useRouter();
  const { id } = router.query;
  const showAjukan = useRef();
  const isTabletOrDesktop = useMediaQuery({ query: `(max-width: 768px)` });

  useEffect(() => {
    if (!isLoading) {
      setTimeout(() => {
        btnAjukan?.current?.click();
      }, 100);
    } else {
      setTimeout(() => {
        btnCloseAjukan?.current?.click();
      }, 100);
    }
  }, [btnAjukan, btnCloseAjukan, isLoading]);

  useEffect(() => {
    if (sessionStorage.getItem('previous') == '/tools/komparasi' && numberToTupiah(hargaProperti) !== '-') {
      showAjukan?.current?.click();
      sessionStorage.removeItem('previous');
    };
  }, [hargaProperti]);

  const getUangMuka = () => {
    if (uangMuka == 0) {
      setUangMuka(Number(Math.round(Number(hargaProperti) * (Number(persenUangMuka) / 100))))
    }
  }

  useEffect(() => {
    // console.log("+++ jangkawaktu", jangkawaktu);
    if (dataKavling.length > 0) {
      const dp = uangMuka;
      const jmlPinjaman = dataKavling[selectedKav].HARGA - dp;
      const totalBunga = jmlPinjaman * (dataKavling[selectedKav].sk_bga / 100);
      const totalBungaSyariah =
        jmlPinjaman * (dataKavling[selectedKav].sk_bga_syariah / 100);
      const angsuranTahunan = (jmlPinjaman + totalBunga) / jangkawaktu;
      const angsuranTahunanSyariah =
        (jmlPinjaman + totalBungaSyariah) / jangkawaktu;
      const angsuranBulanan = angsuranTahunan / 12;
      const angsuranBulananSyariah = angsuranTahunanSyariah / 12;

      setHargaProperti(dataKavling[selectedKav].HARGA);
      setJumlahPinjaman(jmlPinjaman);
      setaAngsuranPerbulan(angsuranBulanan);
      setaAngsuranPerbulanSyariah(angsuranBulananSyariah);
    }
  }, [jangkawaktu, data, selectedKav, dataKavling, uangMuka]);

  function handleAjukanKPR() {
    sessionStorage.removeItem('back_log');
    if(userKey)sessionStorage.setItem('log-rts-mmbr', 'kredit') 
    
    const dataHit = {
      id_properti: dataKavling[selectedKav].ID,
      type_pengajuan: jenisKPR === 1 ? 1 : 2,
      harga_properti: hargaProperti * 1,
      uang_muka: uangMuka,
      nilai_pengajuan: jumlahPinjaman,
      jangka_waktu: jangkawaktu * 12,
      angsuran: jenisKPR === 1 ? angsuranPerbulan : angsuranPerbulanSyariah,
      bunga: jenisKPR === 1 ? data.sukuBunga * 1 : data.sukuBungaSyariah * 1,
      member_id: userProfile?.id,
      AccessKey_Member: userKey,
      // id : ''
    };
    // console.log('~ data', data)
    // console.log('~ userProfile', userProfile)
    // console.log("~ dataHit", dataHit);
    axios({
      url: `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v2/pengajuan/stok/setpengajuan`,
      method: "POST",
      data: QueryString.stringify(dataHit),
      headers: {
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
    })
      .then((res) => {
        if (res.data.status) {
          console.log("~ res", res);
          btnCloseAjukan?.current?.click();
          router.push(`/ajukan_kpr?id=${res.data.kpr_id}`);
        } else {
          throw res.data;
        }
      })
      .catch((err) => {
        console.log("~ err", err);
      });
  }

  const urlParamsSimulasi = `&simulasi%5Bhargaproperti%5D=${hargaProperti}&simulasi[jenissubsidi]=${data.jenisSubsidi}&simulasi%5Buangmuka%5D=${
    uangMuka != 0
      ? Number(uangMuka)
      : Number(
          Math.round(
            Number(hargaProperti) *
              (Number(persenUangMuka) / 100)
          )
        )
    }&simulasi[lamapinjaman]=${jangkawaktu}`;

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const formatter = (e) => {
    const million = 1000000;
    if (e >= million) {
      return `${Math.floor(e / million)}jt/bln`;
    } else if(e == 0){
      return "-"
    } else {
      return `${Math.round(numberWithCommas(e))}rb/bln`;
    } 
  };

  const storeData = () => {
    localStorage.setItem('itemCompare', JSON.stringify([dataDetail]))
  }
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <div className="" id="propertyPageOverflowKPR">
      <div className="widget_bottom_unit">
        <div className="container">
          <div className="row d-flex align-items-center ">
            <div id="widgetKpr_info" className="col-md-4 col-6 info">
              {/* <div id="widgetKpr_mobile" className="lokasi">
                {data.lokasi}
              </div> */}
              <h4>{data.propertyName}</h4>
              <div id="widgetKpr_web" className="lokasi">
                {data.lokasi}
              </div>
              <div className="fasilitas">
                <div className="item"><span> LB: </span> <span>{data.lb}m </span><sup style={{marginTop: 12}}>2</sup></div>
                <div className="item"><span>LT: </span> <span>{data.lt}m</span><sup style={{marginTop: 12}}>2</sup></div>
                <div className="item">
                  <svg
                    width="22"
                    height="22"
                    viewBox="0 0 22 22"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M6.41699 11.9167C7.92949 11.9167 9.16699 10.6792 9.16699 9.16669C9.16699 7.65419 7.92949 6.41669 6.41699 6.41669C4.90449 6.41669 3.66699 7.65419 3.66699 9.16669C3.66699 10.6792 4.90449 11.9167 6.41699 11.9167ZM17.417 6.41669H10.0837V12.8334H2.75033V6.41669H0.916992V13.5834C0.916992 14.6879 1.81242 15.5834 2.91699 15.5834H19.0837C20.1882 15.5834 21.0837 14.6879 21.0837 13.5834V10.0834C21.0837 8.05752 19.4428 6.41669 17.417 6.41669Z"
                      fill="#666666"
                    />
                  </svg>

                  <span>{data.jumlahKamarTidur}</span>
                </div>
                <div className="item">
                  <svg
                    width="22"
                    height="22"
                    viewBox="0 0 22 22"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M10.3793 5.6219L10.3793 5.62191L10.3793 5.62192C9.41876 6.16393 9.3843 6.18338 9.17737 6.33509C8.66783 5.7311 7.91804 5.35718 7.10513 5.35169C5.58868 5.3415 4.35169 6.57222 4.35169 8.08632V16.9752C4.35169 17.0833 4.2639 17.1711 4.15572 17.1711H2.19597C2.0878 17.1711 2 17.0833 2 16.9752V8.08632C2 5.28193 4.28193 3 7.08632 3C8.75328 3 10.2909 3.81486 11.2327 5.14004L11.2271 5.14318L11.2214 5.14631C10.8804 5.33916 10.6045 5.49488 10.3793 5.6219ZM13.545 16.1047C13.5984 16.4249 13.3821 16.7279 13.0618 16.7813C12.7415 16.8347 12.4386 16.6183 12.3852 16.2981L12.2944 15.7535C12.241 15.4332 12.4573 15.1303 12.7776 15.0769C13.0979 15.0235 13.4008 15.2399 13.4542 15.5601L13.545 16.1047ZM16.0442 15.1478C16.3273 14.9889 16.4281 14.6305 16.2691 14.3473L16.0149 13.8944C15.856 13.6113 15.4977 13.5105 15.2145 13.6695C14.9314 13.8284 14.8306 14.1868 14.9896 14.4699L15.2437 14.9228C15.4028 15.2063 15.7614 15.3065 16.0442 15.1478ZM18.9323 12.6242C19.1779 12.8366 19.2047 13.2079 18.9923 13.4535C18.7798 13.699 18.4086 13.7259 18.163 13.5134L17.7454 13.1522C17.4999 12.9398 17.473 12.5685 17.6855 12.3229C17.8979 12.0774 18.2692 12.0506 18.5148 12.263L18.9323 12.6242ZM9.28182 11.2694L15.8278 7.5667L15.6408 7.23589C14.8778 5.88598 13.1633 5.39981 11.8005 6.17018C11.4587 6.36344 11.182 6.51935 10.9562 6.64658C10.0144 7.17721 9.95777 7.2091 9.75961 7.37151C8.74023 8.20416 8.39496 9.70266 9.09447 10.9382L9.28182 11.2694ZM17.914 8.41795L18.1735 8.87653C18.3906 9.26032 18.2549 9.74568 17.872 9.96262L10.2831 14.2549C9.89948 14.4714 9.41386 14.3367 9.19703 13.9534L8.93756 13.4945C8.77588 13.2081 8.87987 12.8465 9.16136 12.6878C9.31944 12.5984 16.7675 8.38557 17.1074 8.19375C17.3896 8.034 17.7534 8.13257 17.914 8.41795Z"
                      fill="#666666"
                    />
                  </svg>
                  <span>{data.jumlahKamarMandi}</span>
                </div>
              </div>
            </div>
            <div id="widgetKpr_harga" className="col-md-4 col-6 ps-0 harga pt-3">
              {/* <h5>{numberToTupiah(hargaProperti)}</h5> */}
              <h5>{numberToTupiah(dataKavling[selectedKav]?.HARGA)}</h5>
              <div className="item">
                <p>Cicilan dari</p>
                <h6 style={{ color: "#00193e" }}>
                  {jenisKPR === 1
                    ? `${formatter(data.pmt_konvensional || 0) || "-"}`
                    : `${formatter(data.pmt_syariah || 0)|| "-"}` }
                  {/* Juta/ bln */}
                </h6>
              </div>
              <div className="item">
                <p>Suku Bunga dari </p>
                <h6 style={{ color: "#00193e" }}>
                  {jenisKPR === 1 ? data.sukuBunga : data.sukuBungaSyariah} %
                </h6>
              </div>
            </div>

            <div
              className="col-md-4"
              style={{ display: "flex", justifyContent: "center", }}
            >
              <button
                type="button"
                className="btn btn-main"
                data-bs-target="#ajukanKPR"
                data-bs-toggle="modal"
                ref={showAjukan}
                onClick={getUangMuka}
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: 700,
                  fontSize: 14,
                  width: 166,
                  height: 48,
                  padding: 0,
                }}
                disabled={numberToTupiah(hargaProperti) === '-'}
              >
                Ajukan KPR
              </button>
              <Link href={`/tools/komparasi?data=${data.id}`}>
                <a
                  className="btn btn-outline-main"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    fontSize: 14,
                    width: 166,
                    height: 48,
                    padding: 0,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginLeft: 12,
                  }}
                  onClick={storeData}
                >
                  Bandingkan
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="ajukanKPR"
        tabIndex="-1"
        aria-labelledby="ajukanKPRLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog"
          // style={{
          //   width: "100%",
          //   maxWidth: "600px",
          // }}
        >
          <div id="modal-kpr" className="modal-content content-pengajuanKPR">
            <button
              type="button"
              style={{
                background: "none",
                border: "none",
                marginRight: "33px",
                transform: "translateY(38px)",
                textAlign: "end",
              }}
              data-bs-dismiss="modal"
              aria-label="Close"
              data-bs-target="#modalSuccessRegister"
              
            >
              <svg
                id="pengajuankpr-btn"
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                  fill="#0061A7"
                />
              </svg>
            </button>
            {/*  */}
            <div className="modal-header header-pengajuanKPR">
              <h5 className="modal-title" id="ajukanKPRLabel">
                Pengajuan KPR
              </h5>
            </div>
            <div className="modal-body body-pengajuan">
              <div className="pengajuan-penawaran" id="pengajuanpenawaran">
                <form
                  className="form-default form-validation mb-0"
                  id="form-pengajuan-kpr"
                  noValidate="novalidate"
                >
                  <div className="form-group w-100 mx-0">
                    <label className="form-control-label">Harga Properti</label>
                    <div className="input-group">
                      <span className="prefix-label">Rp</span>
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        value={hargaProperti}
                        disabled
                      />
                    </div>
                  </div>
                  <div
                    className="form-group inp-uangmuka w-100 mx-0"
                    data-releated="[name='ajukan[hargaproperti]']"
                  >
                    <label className="form-control-label">Uang Muka</label>
                    <div className="input-group">
                      <span className="prefix-label">Rp</span>
                      <input
                        type="text"
                        name="simulasi[uangmuka]"
                        data-priceformat-notag="true"
                        placeholder="0"
                        className="form-control uang-muka"
                        value={
                          uangMuka != 0
                            ? Number(uangMuka).toLocaleString("id")
                            : Number(
                                Math.round(
                                  Number(hargaProperti) *
                                    (Number(persenUangMuka) / 100)
                                )
                              ).toLocaleString("id")
                        }
                        onChange={(e) => {
                          const value = e.target.value.split(".").join("");
                          if (checkIsNumber(value)) {
                            setUangMuka(Math.round(value));
                            const percentage =
                              (Number(value) / Number(hargaProperti)) * 100;
                            if (isNaN(percentage) || percentage == Infinity) {
                              setPersenUangMuka("0");
                              return;
                            }
                            setPersenUangMuka(Math.round(percentage));
                          }
                        }}
                      />
                      <input
                        type="text"
                        value={persenUangMuka}
                        placeholder="0"
                        className="form-control uang-muka-persen"
                        onChange={(e) => {
                          if (checkIsNumber(e.target.value)) {
                            setPersenUangMuka(e.target.value);
                            const resultUangMuka =
                              (Number(hargaProperti) * Number(e.target.value)) /
                              100;
                            setUangMuka(Math.round(resultUangMuka));
                          }
                        }}
                      />
                      <span className="postfix-label">%</span>
                    </div>
                  </div>
                  <div className="form-group w-100 mx-0">
                    <label className="form-control-label">
                      Jumlah Pinjaman
                    </label>
                    <div className="input-group">
                      <span className="prefix-label">Rp</span>
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        value={jumlahPinjaman}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group w-100 mx-0">
                    <label className="form-control-label">Jangka Waktu</label>
                    <div
                      className="input-select"
                      style={{ position: "relative" }}
                    >
                      <select
                        value={jangkawaktu}
                        onChange={(e) => setJangkaWaktu(e.target.value)}
                        className="form-control select2 JangkaWaktu JangkaWaktuSidebar select2-hidden-accessible w-100 mx-0"
                        name="ajukan[jangkawaktu]"
                        required=""
                        tabIndex="-1"
                        aria-hidden="true"
                      >
                        <option value="01">1 Tahun</option>
                        <option value="02">2 Tahun</option>
                        <option value="03">3 Tahun</option>
                        <option value="04">4 Tahun</option>
                        <option value="05">5 Tahun</option>
                        <option value="06">6 Tahun</option>
                        <option value="07">7 Tahun</option>
                        <option value="08">8 Tahun</option>
                        <option value="09">9 Tahun</option>
                        <option value="10">10 Tahun</option>
                        <option value="11">11 Tahun</option>
                        <option value="12">12 Tahun</option>
                        <option value="13">13 Tahun</option>
                        <option value="14">14 Tahun</option>
                        <option value="15">15 Tahun</option>
                        <option value="16">16 Tahun</option>
                        <option value="17">17 Tahun</option>
                        <option value="18">18 Tahun</option>
                        <option value="19">19 Tahun</option>
                        <option value="20">20 Tahun</option>
                        <option value="21">21 Tahun</option>
                        <option value="22">22 Tahun</option>
                        <option value="23">23 Tahun</option>
                        <option value="24">24 Tahun</option>
                        <option value="25">25 Tahun</option>
                        <option value="26">26 Tahun</option>
                        <option value="27">27 Tahun</option>
                        <option value="28">28 Tahun</option>
                        <option value="29">29 Tahun</option>
                        <option value="30" selected="">
                          30 Tahun
                        </option>
                      </select>
                      <div style={{ position: "absolute", top: 16, right: 21 }}>
                        <svg
                          width="11"
                          height="5"
                          viewBox="0 0 11 5"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M0.998047 0L5.99805 5L10.998 0H0.998047Z"
                            fill="#00193E"
                          />
                        </svg>
                      </div>
                    </div>
                  </div>
                  <div className="form-group w-100 mx-0">
                    <label className="form-control-label">Pilih Kavling</label>
                    <div
                      className="input-select"
                      style={{ position: "relative" }}
                    >
                      <select
                        value={selectedKav}
                        onChange={(e) =>
                          onChangeSelectedKav(parseInt(e.target.value))
                        }
                        className="form-control select2 PilihKavling PilihKavlingSidebar select2-hidden-accessible w-100 mx-0"
                        name="ajukan[pilihkavling]"
                        required=""
                        tabIndex="-1"
                        aria-hidden="true"
                      >
                        {dataKavling.map((item, index) => {
                          return <option value={index}>No. {item.NO}</option>;
                        })}
                      </select>
                      <div style={{ position: "absolute", top: 16, right: 21 }}>
                        <svg
                          width="11"
                          height="5"
                          viewBox="0 0 11 5"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M0.998047 0L5.99805 5L10.998 0H0.998047Z"
                            fill="#00193E"
                          />
                        </svg>
                      </div>
                    </div>
                  </div>
                  <div className="form-group w-100 mx-0">
                    <label className="form-control-label">
                      Pilih Jenis KPR
                    </label>
                    <ul className="list-unstyled list-radio-vertical">
                      <li className="list-item">
                        <label
                          htmlFor="jenis_kpr"
                          className="radio-main icheck-label icheck[0m9cm]"
                        >
                          <div
                            style={{
                              display: "flex",
                              alignItems: "flex-start",
                            }}
                          >
                            <div
                              className="iradio_square-blue icheck-item icheck[0m9cm] checked"
                              onClick={() => setJenisKpr(1)}
                              style={{ paddingTop: 8 }}
                            >
                              <Check isChecked={jenisKPR === 1} />
                            </div>
                            <img
                              style={{
                                margin: " 0 16px",
                                width: "39.92px",
                                height: 40,
                              }}
                              src="/images/acc/kpr-konvensional.png"
                              alt=""
                            />
                            <div className="media-body">
                              <h5 className="list-item-title">
                                KPR BTN Konvensional
                              </h5>
                              <span className="list-item-subtitle text-green KprKonvensionalMulai">
                                {numberToTupiah(angsuranPerbulan)} / Bulan
                              </span>
                              <span className="list-item-subtitle text-green KprKonvensionalSukuBunga">
                                {data.sukuBunga}% 
                              </span>
                              <Link
                                href={
                                  `/tools/simulasi_kpr?jeniskpr=kprKonvensional&simulasi[sukubunga]=${data.sukuBunga}` +
                                  urlParamsSimulasi
                                }
                              >
                                <a onClick={() => btnCloseAjukan?.current?.click()}>
                                  <span style={{ marginRight: 4 }}>
                                    Lihat Detail Rincian
                                  </span>
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M10.8008 15.06L13.8541 12L10.8008 8.94L11.7408 8L15.7408 12L11.7408 16L10.8008 15.06Z"
                                      fill="#0061A7"
                                    />
                                  </svg>
                                </a>
                              </Link>
                            </div>
                          </div>
                        </label>
                      </li>
                      <li className="list-item">
                        <label
                          htmlFor="jenis_kpr"
                          className="radio-main icheck-label icheck[tz90d]"
                        >
                          <div
                            style={{
                              display: "flex",
                              alignItems: "flex-start",
                            }}
                          >
                            <div
                              className="iradio_square-blue icheck-item icheck[tz90d]"
                              onClick={() => setJenisKpr(2)}
                              style={{ paddingTop: 8 }}
                            >
                              <Check isChecked={jenisKPR !== 1} />
                            </div>
                            <img
                              style={{
                                margin: " 0 16px",
                                width: "39.92px",
                                height: 40,
                              }}
                              src="/images/acc/kpr-syariah.png"
                              alt=""
                            />
                            <div className="media-body">
                              <h5 className="list-item-title">
                                KPR BTN Syariah
                              </h5>
                              <span className="list-item-subtitle text-green KprSyariahMulai">
                                {numberToTupiah(angsuranPerbulanSyariah)} /
                                Bulan
                              </span>
                              <span className="list-item-subtitle text-green KprSyariahSukuBunga">
                                {data.sukuBungaSyariah}% 
                              </span>
                              <Link
                                href={
                                  `/tools/simulasi_kpr?jeniskpr=kprSyariah&simulasi[sukubunga]=${data.sukuBungaSyariah}` + 
                                  urlParamsSimulasi
                                }
                              >
                                <a onClick={() => btnCloseAjukan?.current?.click()}>
                                  <span style={{ marginRight: 4 }}>
                                    Lihat Detail Rincian
                                  </span>
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M10.8008 15.06L13.8541 12L10.8008 8.94L11.7408 8L15.7408 12L11.7408 16L10.8008 15.06Z"
                                      fill="#0061A7"
                                    />
                                  </svg>
                                </a>
                              </Link>
                            </div>
                          </div>
                        </label>
                      </li>
                      <li className="list-item">
                        <p className="revamp-note-simulasi">
                          *Catatan: Perhitungan ini adalah hasil perkiraan
                          aplikasi KPR secara umum. Data perhitungan di atas
                          dapat berbeda dengan perhitungan bank. Untuk
                          perhitungan yang akurat, silakan hubungi kantor
                          cabang kami.
                        </p>
                      </li>
                    </ul>
                  </div>
                </form>
              </div>
            </div>
            <div className="modal-footer footer-pengajuan" id="pengajuanfooter">
              {/* <Link href={userKey ? "/ajukan_kpr" : "#"}> */}
              <button
                type="button"
                className="btn btn-main"
                data-bs-toggle={!userKey ? "modal" : isVerified && validateProfileData(userProfile) ? null : "modal"}
                data-bs-target={!userKey ? "#modalLogin" : isVerified === false ? "#verifiedModal": (validateProfileData(userProfile) ? "" : "#phoneModal")}
                // data-bs-target={!userKey ? "#modalLoginAjukan" : null}
                onClick={isVerified ? (validateProfileData(userProfile) ? handleAjukanKPR : null) : null }
                // onClick={!userKey ? ()=>{} : handleAjukanKPR}
              >
                Ajukan KPR
              </button>
              {/* </Link> */}
              
            </div>
          </div>
        </div>
      </div>

      {/* modal isVerified */}
      {/* <div className="modal fade" id="verifiedModal" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered mx-auto px-0">
          <div className="modal-content modal_delete h-100" style={{padding:"20px 29px"}}>
            <div className="modal-body text-center pt-3 px-0">
            <div className="text-center">
              <h4 className="modal-title mb-3" style={{fontFamily:"Futura",fontStyle:"normal",lineHeight:"130%", fontWeight:"700"}}>Lakukan e-KYC</h4>
              <div className="desc text-center mb-0" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                Silakan lakukan e-KYC melalui Mobile App
              </div>
              {isTabletOrDesktop ? (<Lottie options={defaultOptions} isStopped={false} isPaused={false} />) 
              : (<Lottie options={defaultOptions} height={146} width={212} isStopped={false} isPaused={false} style={{marginBottom:"121px",marginTop:"125px"}} />)}
              <button
                type="buttom"
                className="btn btn-main form-control btn-back px-5"
                style={{
                  width:"100%",
                  maxWidth: "540px",
                  height: "48px",
                  fontSize: "14px",
                  fontFamily: "Helvetica",
                  fontWeight: 700,
                  backgroundColor: "#0061A7",
                  }}
                data-bs-dismiss="modal"
                onClick={(()=>{window.location.replace("/member/verified/")})}
              >
                Oke
              </button>
            </div> 
            </div>
          </div>
        </div>
      </div> */}
      <Login handleAjukanKPR={handleAjukanKPR}/>
    </div>
  );
}

const Check = ({ isChecked }) => {
  return (
    <span style={{ cursor: "pointer" }}>
      {isChecked && (
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect
            x="0.5"
            y="0.5"
            width="23"
            height="23"
            rx="3.5"
            stroke="#0061A7"
          />
          <path
            d="M18.7 7.20002C18.3 6.80002 17.7 6.80002 17.3 7.20002L9.8 14.7L6.7 11.6C6.3 11.2 5.7 11.2 5.3 11.6C4.9 12 4.9 12.6 5.3 13L9.1 16.8C9.3 17 9.5 17.1 9.8 17.1C10.1 17.1 10.3 17 10.5 16.8L18.7 8.60002C19.1 8.20002 19.1 7.60002 18.7 7.20002Z"
            fill="#0061A7"
          />
        </svg>
      )}
      {!isChecked && (
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect
            x="0.5"
            y="0.5"
            width="23"
            height="23"
            rx="3.5"
            stroke="#AAAAAA"
          />
        </svg>
      )}
    </span>
  );
};





function Login({handleAjukanKPR}) {
  const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
  const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
  const isMobile = useMediaQuery({ query: `(max-width: 768px)` });
  const [validated, setValidated] = useState(false);
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [isEmailErr, setEmailErr] = useState(false);
  const [isPasswordErr, setPasswordErr] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const { setUserKey, setUserProfile } = useAppContext();
  const emailInput = useRef(null);
  const passInput = useRef(null);
  const [remember, setRemember] = useState(false);
  
  // const [errorLogin, setErrorLogin] = useState(false);

  // useEffect(() => {
  //   onAuthStateChanged(auth, (user) => {
  //     if (user) {
  //       // setUser({
  //       //   name: user.displayName,
  //       //   email: user.email || user.providerData[0].email,
  //       //   image: user.photoURL,
  //       //   id: user.uid
  //       // })
  //       console.log('~ data user : ', user)
  //       // setLoggedIn(true)
  //     } else {
  //       // setUser(null)
  //       // setLoggedIn(false)
  //     }
  //   })
  // }, [])

  const submitLogin = (e) => {
    e.preventDefault();
    if (!isEmailErr && !isPasswordErr) {
      setEmailErrorMessage("")
      setPasswordErrorMessage("")
      doLogin(email, password)
        .then((data) => {
          if (data.IsError) {
            throw data.ErrToUser;
          } else {
            getEkycData(data.Profile[0].id).then((verif)=>{
              const tempProfile = {
                n: data.Profile[0].n,
                gmbr: data.Profile[0].gmbr,
                id: data.Profile[0].id,
                agen: data.Profile[0].is_agn,
                idAgen: data.Profile[0].i_agn,
                namaAgensi: data.Profile[0].n_agn,
                isVerified: verif.data?.isVerified,
                currentStep: verif.data?.currentStep,
                counterFailedVerification:verif.data?.counterFailedVerification,
                email:data.Profile[0].e,
              };
    
              Cookies.set("user", JSON.stringify(tempProfile), {
                secure: false,
                expires: 7
              });
              Cookies.set("keyMember", JSON.stringify(data.AccessKey), {
                secure: false,
                expires: 7
              });
              sessionStorage.setItem("user", JSON.stringify(tempProfile));
              sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
              setUserProfile(data.Profile[0]);
              setUserKey(data.AccessKey);
              localStorage.removeItem("err_log");
              const redirect = sessionStorage.getItem("redirect_url");
              const urlFromEmail = localStorage.getItem("urlFromEmail");
              // document.getElementsByClassName("login-btn")?.setAttribute("data-bs-dismiss", "modal")
              if (urlFromEmail) {
                window.location.replace(urlFromEmail);
                localStorage.removeItem("urlFromEmail");
                sessionStorage.removeItem("redirect_url");
              } else if (redirect) {
                window.location.replace(redirect);
                sessionStorage.removeItem("redirect_url");
              } else {
                if (router.pathname.includes('tools/komparasi') && router.query?.data) {
                  window.location.reload();
                } else if(router.pathname.includes('tools/konsultasi')){
                  window.location.reload();
                }
              };
              if(verif.data?.isVerified){
                handleAjukanKPR();
              }else{
                window.location.replace("/member/verified/");
              }
            }).catch((error)=>{
              const tempProfile = {
                n: data.Profile[0].n,
                gmbr: data.Profile[0].gmbr,
                id: data.Profile[0].id,
                agen: data.Profile[0].is_agn,
                idAgen: data.Profile[0].i_agn,
                namaAgensi: data.Profile[0].n_agn,
                isVerified: false,
                currentStep: 0,
                counterFailedVerification:0,
                email:data.Profile[0].e,
              };
    
              Cookies.set("user", JSON.stringify(tempProfile), {
                secure: false,
                expires: 7
              });
              Cookies.set("keyMember", JSON.stringify(data.AccessKey), {
                secure: false,
                expires: 7
              });
              sessionStorage.setItem("user", JSON.stringify(tempProfile));
              sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
              setUserProfile(data.Profile[0]);
              setUserKey(data.AccessKey);
              localStorage.removeItem("err_log");
              const redirect = sessionStorage.getItem("redirect_url");
              const urlFromEmail = localStorage.getItem("urlFromEmail");
              // document.getElementsByClassName("login-btn")?.setAttribute("data-bs-dismiss", "modal")
              if (urlFromEmail) {
                window.location.replace(urlFromEmail);
                localStorage.removeItem("urlFromEmail");
                sessionStorage.removeItem("redirect_url");
              } else if (redirect) {
                window.location.replace(redirect);
                sessionStorage.removeItem("redirect_url");
              } else {
                if (router.pathname.includes('tools/komparasi') && router.query?.data) {
                  window.location.reload();
                } else if(router.pathname.includes('tools/konsultasi')){
                  window.location.reload();
                }
              };
                window.location.replace("/member/verified/");
              
            })
          }
        })
        .catch((error) => {
          setEmailErrorMessage(error);
          setEmailErr(true);
  
          const storage = localStorage.getItem("err_log");
          if (storage) {
            let temp = Number(storage);
            if (temp + 1 === 3) {
              submitError();
            } else {
              localStorage.setItem("err_log", temp + 1);
            }
          } else {
            localStorage.setItem("err_log", 1);
          }
        });
    }
  };

  const submitError = () => {
    const payload = {
      Email: email,
    };

    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login/attempt`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        AccessKey_App: localStorage.getItem("accessKey"),
        ...defaultHeaders
      },
      body: new URLSearchParams(payload),
    })
      .then(async (response) => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        return data;
      })
      .then((data) => {
        if (data.Output !== "") {
          setPasswordErr(true);
          setEmailErr(false);
          setPasswordErrorMessage(data.Output);
        } else if (data.ErrToUser !== "") {
          setPasswordErr(false);
          setEmailErr(true);
          setEmailErrorMessage(data.ErrToUser);
        }
      })
      .catch((error) => {
        console.log("error get attempt " + error);
      })
      .finally(() => {
        localStorage.removeItem("err_log");
      });
  };

  const customLoginGoogle = () =>{
    googleLogin(setUserKey, setUserProfile,handleAjukanKPR).then((res)=>{
    })

  }

  const handleChangeEmail = (e) => {
    setEmail(e.target.value);
    setEmailErr(false)
    setPasswordErr(false)
  };
  const handleChangePassword = (e) => {
    setPassword(e.target.value);
    setEmailErr(false)
    setPasswordErr(false)
  };

  const handleBlurEmail = (value) => {
    // const value = e.target.value;
    const RFC5322 =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // checking empty input
    if (value.trim() === "") {
      setEmailErrorMessage("Isian tidak boleh kosong");
      return setEmailErr(true);
    }
    // checking email format
    if (value.trim() && !RFC5322.test(value.trim())) {
      setEmailErrorMessage("Isi dengan alamat email");
      return setEmailErr(true);
    }

    setEmailErr(false);
    setEmailErrorMessage("");
  };


  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      handleBlurEmail(email);
      // handleBlurPassword(password);
    }
  };

  function redirectProfil(e) {
    e.preventDefault();
    router.push("/member/profil");
  }

  useEffect(() => {
    //   if (isEmailErr || password.trim() === '') {
    //     console.log('isValidated: ', false)
    //     return setValidated(false)
    //   }
    //   console.log('isValidated: ', true)
    //   setValidated(true)
    // }, [isEmailErr, password]);
    if (password && email) {
      setValidated(true);
    } else {
      setValidated(false);
    }
    // if (password && email && !isInvalid('password', password)) {
    //   setValidated(true);
    // } else {
    //   setValidated(false);
    // };
  }, [password, email]);

  useEffect(() => {
    setValidated(false);
  }, []);

  const loginAuth = (id) => {
    signIn(id);
  };

  const changeRemember = () => {
    if (remember) {
      setRemember(false);
    } else {
      setRemember(true);
    }
  };

  const resetValidation = useCallback(() => {
    setEmail("");
    setPassword("");
    setEmailErr(false);
    setPasswordErr(false);
  }, [email, password, isEmailErr, isPasswordErr]);

  return (
    <div
      className="modal fade m-atas"
      id="modalLoginAjukan"
      aria-hidden="true"
      aria-labelledby="modalLoginAjukan"
      tabIndex="-1"
    >
      <div 
        id="modal-log-in" 
        className="modal-dialog modal-auth" 
        style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)' } : {}}
      >
        <div 
          className="modal-content" 
          id="loginpopup"
          style={isSmallDevice || isMediumDevice ? { height: '100%' } : {}}
        >
          <div className="modal-header">
            <img src="/images/bg/bg_icon.svg" className="acc img-fluid" />
            <div className="text-center">
              <h5
                style={{ lineHeight: "41.6px", fontFamily: "FuturaBT" }}
                className="modal-title mb-3"
                id="exampleModalLabel"
              >
                Masuk
              </h5>
              <div className="desc">
                Temukan berbagai fitur dan layanan menarik dari BTN Properti
                dengan masuk terlebih dahulu.
              </div>
            </div>

            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <i className="bi bi-x-lg"></i>
            </button>
          </div>
          <div className="modal-body" style={{ borderRadius: "16px" }}>
            <div className="modal-form">
              <form id="form-login" onSubmit={submitLogin}>
                {/* {errorLogin && <div  className="error-login">{errorMessage}</div>} */}
                <div className="floating-label-wrap ">
                  <input
                    type="email"
                    className="floating-label-field"
                    id="login_email"
                    placeholder="Email"
                    value={email}
                    onChange={handleChangeEmail}
                    // onBlur={(e) => handleBlurEmail(e, e.target.id)}
                    onBlur={(e) => handleBlurEmail(e.target.value)}
                    onKeyPress={handleKeyPress}
                    autoComplete={remember ? "on" : "off"}
                    required
                    style={{
                      borderColor: isEmailErr ? "red" : "#aaaaaa",
                      color: "#00193E",
                    }}
                  />
                  <label htmlFor="login_email" className="floating-label">
                    tes
                  </label>
                  {emailErrorMessage && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {emailErrorMessage}
                    </div>
                  )}
                </div>
                <div className="floating-label-wrap mb-2">
                  <input
                    type="password"
                    className="floating-label-field"
                    id="login_password"
                    placeholder="password"
                    value={password}
                    onChange={handleChangePassword}
                    // onBlur={(e) => handleBlurPassword(e.target.value)}
                    required
                    style={{
                      borderColor: isPasswordErr ? "red" : "#aaaaaa",
                      color: "#00193E",
                    }}
                    onKeyPress={handleKeyPress}
                    autoComplete={remember ? "on" : "off"}
                  />
                  <label htmlFor="login_password" className="floating-label">
                    Password
                  </label>
                  {passwordErrorMessage && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {passwordErrorMessage}
                    </div>
                  )}
                </div>

                <div className="form-group">
                  <div className="text-end lupa_password pb-2 mb-4 mt-2">
                    <a
                      href="#"
                      onClick={resetValidation}
                      className="modal-link"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      data-bs-target="#modalResetPassword"
                      data-bs-toggle="modal"
                    >
                      Lupa Password?
                    </a>
                  </div>
                </div>

                <div
                  className="d-grid gap-2 container-btn-login"
                  style={{ opacity: validated ? 1 : 0.25 }}
                >
                  <button
                    style={{
                      marginTop: 0,
                      marginBottom: 0,
                    }}
                    className="login-btn btn btn-main btn-block"
                    // data-bs-dismiss="modal"
                    onClick={submitLogin}
                    disabled={validated ? false : true}
                  >
                    Masuk Sekarang
                  </button>
                </div>
                <div
                  className="remember-me mt-16"
                  style={{
                    marginTop: "16px",
                  }}
                >
                  <label>
                    <input
                      type="checkbox"
                      name="login[remember]"
                      checked={remember}
                      onChange={changeRemember}
                    />
                    <span
                      className="form-check-label font-sm-4 opacity-8 ms-1"
                      style={{
                        fontSize: 16,
                        fontFamily: "Helvetica",
                        fontStyle: "normal",
                        fontWeight: "normal",
                        lineHeight: "160%",
                        margintop: "16px",
                      }}
                    >
                      Ingat Akun Saya
                    </span>
                  </label>
                </div>
              </form>
            </div>
            <div
              style={{
                width: isMobile ? `calc(100% + 60px)` : `calc(100% + 120px)`,
                marginLeft:
                  typeof window !== "undefined"
                    ? window.innerWidth < 768
                      ? -30
                      : -60
                    : -60,
                marginTop: "24px",
                marginBottom: "24px",
                borderBottom: "2px solid #EEEEEE",
                pb: "5px",
              }}
            ></div>
            <div className="line-on-side text-center">
              <span
                className="login-with"
                style={{
                  fontSize: 14,
                  fontFamily: "Helvetica",
                  fontStyle: "normal",
                  fontWeight: "normal",
                  lineHeight: "130%",
                  marginBottom: 16,
                }}
              >
                Atau Masuk dengan
              </span>
            </div>
            <div className="media-login">
              <ul className="nav justify-content-center my-3">
                <li className="nav-item">
                  <div className="nav-link">
                    <img src="/images/icons/auth_google.png" onClick={customLoginGoogle} />
                  </div>
                </li>
                <li className="nav-item">
                  <a className="nav-link">
                    <img src="/images/icons/auth_facebook.png" onClick={facebookLogin} />
                  </a>
                </li>
                {/* <li className="nav-item">
                  <a className="nav-link" onClick={twitterLogin}>
                    <img src="/images/icons/auth_twitter.png" />
                  </a>
                </li> */}
              </ul>
              <div className="modal-block ">
                <div
                  className="modal-block text-center register-label"
                  style={{ fontFamily: "Helvetica", fontWeight: 400 }}
                >
                  Belum punya akun?
                  <a
                    href="#"
                    onClick={resetValidation}
                    className="modal-link me-1"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalRegister"
                    data-bs-toggle="modal"
                    style={{fontWeight:"400"}}
                  >
                    {" "}
                    Daftar sekarang
                  </a>
                  |
                  <a
                    href="#"
                    onClick={resetValidation}
                    className="modal-link ms-1"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalLinkAktivasi"
                    data-bs-toggle="modal"
                    style={{fontWeight:"400"}}
                  >
                    Kirim Ulang Link Aktivasi
                  </a>
                </div>
              </div>

              {/* <div className="d-grid gap-2 ">
                  {validated === true ? (
                    <button type="submit" className="btn btn-main btn-block">
                      Masuk Sekarang
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-main btn-block"
                      disabled
                    >
                      Masuk Sekarang
                    </button>
                  )}
                </div> */}
              {/* <div className="remember-me mt-2">
                  <label>
                    <input type="checkbox" name="login[remember]" />
                    <span className="font-sm-4 opacity-8 ms-1">
                      Ingat Akun Saya
                    </span>
                  </label>
                </div> */}
              {/* </form>
            </div> */}

              {/* <div
              style={{
                marginTop: "24px",
                marginBottom: "24px",
                marginLeft: -60,
                width: "125%",
                height: "1px",
                backgroundColor: "#EEEEEE",
              }}
            ></div>
            <div className="line-on-side text-center">
              <span>Atau Masuk dengan</span>
            </div> */}
              {/* <div className="media-login">
              <ul className="nav justify-content-center my-3"> */}
              {/* <li className="nav-item">
                  <a className="nav-link" href="#">
                    <img src="/images/icons/auth_google.png" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <img src="/images/icons/auth_facebook.png" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <img src="/images/icons/auth_twitter.png" />
                  </a>
                </li> */}
              {/* </ul>
              <div className="modal-block ">
                <div className="modal-block text-center">
                  Belum punya akun?
                  <a
                    href="#"
                    className="modal-link"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalRegister"
                    data-bs-toggle="modal"
                  >
                    {" "}
                    Daftar sekarang
                  </a>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
