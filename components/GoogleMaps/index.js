import { LoadScriptNext, GoogleMap, InfoWindow, Marker, Polyline, useGoogleMap } from "@react-google-maps/api";
import React, { useCallback, useState } from "react";
import { useEffect } from "react";
import styles from "./googleMaps.module.scss";
import _ from "lodash";

const defPosition = {
    lat: -6.2088,
    lng: 106.8456,
};

export default function GoogleMaps({ position = defPosition, }) {
    const [center, setCenter] = useState(defPosition);
    useEffect(() => {
        // console.log("~ position", position);
    }, [position]);
    return (
        <div className={styles.mapsContainer}>
            <LoadScriptNext googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY} libraries={["places"]}>
                <GoogleMap
                    id="circle-example"
                    mapContainerStyle={{
                        width: "100%",
                        height: "100%",
                    }}
                    center={position}
                    zoom={10}
                    options={{
                        streetViewControl: false,
                        draggable: true, // make map draggable
                        keyboardShortcuts: false, // disable keyboard shortcuts
                        scaleControl: true, // allow scale controle
                        scrollwheel: false, // allow scroll wheel
                    }}
                    // onLoad={onLoadMap}
                >
                    <Marker position={position}></Marker>
                </GoogleMap>
            </LoadScriptNext>
        </div>
    );
}
