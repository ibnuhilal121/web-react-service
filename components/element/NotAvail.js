import React from "react";

const NotAvail = ({message}) => {
    return (
        <div
            style={{
                paddingTop: 120,
                textAlign: "center",
            }}
        >
            <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M53.3337 3.3335H13.3337C9.66699 3.3335 6.66699 6.3335 6.66699 10.0002V56.6668H13.3337V10.0002H53.3337V3.3335ZM50.0003 16.6668L70.0003 36.6668V70.0002C70.0003 73.6668 67.0003 76.6668 63.3337 76.6668H26.6337C22.967 76.6668 20.0003 73.6668 20.0003 70.0002L20.0337 23.3335C20.0337 19.6668 23.0003 16.6668 26.667 16.6668H50.0003ZM46.667 40.0002H65.0003L46.667 21.6668V40.0002Z" fill="#0061A7" fill-opacity="0.75"/>
            </svg>

            <div
                style={{
                    fontFamily: "Helvetica",
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "16px",
                    lineHeight: "150%",
                    marginTop: 40,
                }}
            >
                {message ? message : "Maaf, Keyword yang kamu cari tidak ditemukan"}
            </div>
        </div>
    );
};

export default NotAvail;
