import Link from "next/link";
import styles from "./modal_confirmation.module.scss";
import Lottie from "react-lottie";
import animationHome from "./homeanimation.json"

export default function ModalConfirmation(props) {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationHome,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  return (
    <div className={styles.boxModal}>
      <div className={styles.boxModal__Modal}>
        {/* <div className="modal-close-kpr text-end">
          <Link href="/member/kredit/"> */}
        <div style={{
          float: 'right',
          marginRight: '-40px',
          marginTop: '-20px',
          cursor: 'pointer'
        }}>
          <Link href="/">
          <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                    fill="#2b2b2b"
                  />
                </svg>
          </Link>
        </div>
        <h2 className="mb-3  modal-title" style={{ fontFamily: 'FuturaBT', fontWeight: 700, fontSize: '32px' }}>Terima Kasih</h2>
        <p
          className="text-center modal_content"
          style={{ fontFamily: "Helvetica", fontWeight: "400", fontSize: '16px' }}
        >
          Pengajuan telah berhasil terkirim. Selanjutnya tim BTN akan mencoba
          menghubungi kamu untuk proses berikutnya. Silakan cek inbox untuk
          melihat kembali pengajuan kamu.
        </p>
        <div>
          <Lottie style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
            options={defaultOptions}
            className="gif-modal-kpr"
            // style={{ width: '350px', height: '250px' }}
          />
        </div>

        {/* <Lottie
          options={defaultOptions}
          height={200}
          width={200}
        /> */}
        <div className={`w-100 ${styles.boxModal__Modal__Footer}`}>

          <Link href="/">
            <button
              type="buttom"
              className="btn btn-main form-control btn-back px-5 kembali-ke-branda w-100"
              style={{ maxWidth: '490px', height: '48px' }}
            >
              Kembali ke Beranda
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}
