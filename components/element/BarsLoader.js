const BarsLoader = () => {
    return (
        <div className="text-center py-5">
            <img src="/images/bars.gif" style={{width: 100, height: 'auto'}} alt="" />
        </div>
    )
}

export default BarsLoader
