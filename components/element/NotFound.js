import React from "react";
import SearchIcon from './icons/SearchIcon'

const NotFound = ({message}) => {
    return (
        <div
            style={{
                paddingTop: 250,
                textAlign: "center",
            }}
        >
            <SearchIcon />
            <div
                style={{
                    fontFamily: "Helvetica",
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "16px",
                    lineHeight: "150%",
                    marginTop: 40,
                }}
            >
                {message ? message : "Maaf, Keyword yang kamu cari tidak ditemukan"}
            </div>
        </div>
    );
};

export default NotFound;
