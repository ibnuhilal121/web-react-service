import styles from "./Credit.module.scss";
export default function SliderProgress(props) {
  return (
    <div className={`${styles.sliderProgress} ms-3 ms-lg-0`}>
      <div
        className={`${styles.sliderProgress__activeBar}`}
        style={{
          width: props.showPercent > 0 ? `${props.showPercent}%` : `50px`,
        }}
      >
        <span style={{ top: "-70%" }}>{props.showPercent}%</span>
      </div>
    </div>
  );
}
