import React from "react";
import { useAppContext } from "../../context";
import LoveIcon from "./icons/LoveIcon";
import LoveOutlinedIcon from "./icons/LoveOutlinedIcon";

const LikeButton = ({ propId }) => {
    const { userFavorites, likeAction, userKey } = useAppContext();
    function handleLike() {
        likeAction(propId);
    }
    return (
        <div 
            className="like d-flex justify-content-center align-items-center" 
            style={{ lineHeight: "35px" }} 
            onClick={userKey ? handleLike : null}
            data-bs-toggle={userKey ? "" : "modal"}
            data-bs-target={userKey ? "" : "#modalLogin"}
        >
            {userFavorites?.includes(propId) ? <LoveIcon /> : <LoveOutlinedIcon />}
        </div>
    );
};

export default LikeButton;
