export default function DoorOpenIcon() {
    return (
        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="40" height="40" rx="20" fill="#0061A7" fill-opacity="0.1"/>
<path d="M22 14V29H11V27H13V11H22V12H27V27H29V29H25V14H22ZM18 19V21H20V19H18Z" fill="#0061A7"/>
</svg>

    );
}
