export default function KprSyar() {
  return (
    <svg
      width="36"
      height="32"
      viewBox="0 0 36 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <rect width="36" height="32" fill="url(#pattern0)" />
      <defs>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width="1"
          height="1"
        >
          <use
            xlinkHref="#image0_1498:1928"
            transform="translate(0 -0.0625) scale(0.0208333 0.0234375)"
          />
        </pattern>
        <image
          id="image0_1498:1928"
          width="48"
          height="48"
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAFLUlEQVRoBe1Yb2gbZRi/UgZFIS2pXdfUJJd/y+Vyl+bukrv8b2vS1SVN2zRJ/6QjrEMnjhVWmbVu/hsqYp1TcOBEGSLsS+dAPzjmB1FEp6Afnd86qcpgCBUtQ4Vq5Rc4OLNrl9G7EiSBl7z33nvP+zzP+zy/3/O+BNH4NTzQ8EDDAw0PNDygkwcMxu5Qa4f7bZO7/517O8g9Oi2ji9hmG504zkUm/2KlwgYXLa10djp267KSHkIdnvgkFPdJo8dYqfCN25+8qMc6usg0mUz3MMGxH5lA9jwW4KLFT/sGy5d1WUwPoVZnmPNHJv9pb7ekIN/NpF7whSbWWlt3O/RYT3OZ3U4xzEdLvyOBIdxodBr4yMQyQolic2E8EyTZovnCWgkUhL77uNjEutkZelSW2W4iKX948ktGzK3BED429bVsoDynrv57gqNv9UTGf+FDKY9SMTZRLHkDo0ClX+s6pACZrDR2FcmMHLC6xIzZzs+wgdGPWDG/amV6c0rD6rKPXACUYie46NQNLla6Ce+zkdxMXSpcrZTFIS76Y6Vlo9HiRWtr6+7x8NmfvVLumeq5dfcMLmClwjVGyL2uVE6IT51CWBEE0aQcr7s+YhzhY6N7r9L80BUg0MNHn/8Y6INxOx1P1p3SUKjLzARYMfeuL1Tc8EmFm1xi+nOnb+CyxSkuebmBiiEYRy5QXPocwqpuDEFsg3EROjZP9CHEvZpyGGcDw0d9Uv46CA/1ktq8HRur1D7C8BIqz2A0t1Azy5JkCyAW3w1kD71HEMSuHVNasVATijZWKqx3mukRxXjNXTsVzyMvxNjYuZo/0moiQgGFWw8f2xYxofxG3pCuyM5xBE5YjJhfo4XMaS0cQlL9F0B6QCi7XdxrdYs2giCatZCtKsPFpl4Du273qIiaCE7w8Nlb2AXsKBcr/YE+LQx/ostJzmC43wiWtTqTZ1Wtq2EQirmY5CKcgPrIw6cfxxlCbghL5Eb/g+UjNYjbegqQprW1i29r67JiJvrwEuodtS/BB2Bhb2D4UnVjhOElkBvCDzUSkAgOUZPj7tm/DPZWe1fzWLk8122l9n2GEhiHcxwRwbB7/dmN6UeepAVB+A/8wUjEMh3Ifk8JQxc9QuaD6uYNZi/Q3NBjskM2UWYXDBD7Jp/d5H1tw3MLp8+QnsFVwJ3FEZr1Bcbf94dL1+BBsXf6b5y2KlAayc3gkGJzx07Cs9vNDfCCZgbY6P2/4awrm4xjoaHdHAT2M+L4ix4++xW4ALsENkapYPck3rS4pMM4mSmbxSkdMJkcZlnWFv/aGCCHEFABdc4m9UszjAIEYgceSB+8hF1Colc3JG3laJkolrZQHq+0MQCSkMQgGbl+QcgAq7dSAImJGFc2eB61UCUH+KErdyirmzUJIaWSMKSSB1L+OvAaSHMnQ5TfK/uQpXy+rU+SLZQ//cO2k/g2wQRBAMNxdQj8BuLw4cwrWhAObjSw02ZX/EM2NP4tcimaLJ9Q00GTMYRJ7+D0KaAOmPTQ4aeeRs7cjXBcubADxYOAWqAaIxZX8wfmv0ilS+e77cKr1TcbdyO75rmoW4BGTHDkT4ob+QmJXLnAUpfQhPkWV2geB54KE0uFStkA1AJJqn+2A6NIUjo09gbglBXHbgA+UetgpwC7IC9AbqXeiZZWwMiYowFnaGsdkAZ1ktW7b93GpFdIb+Y7cImFSt5C1dmxhy3W9cWW7I5jCy+Rc/MvH5l7YvHE8ZNnsrOzzxnkd43/hgcaHmh44P/hgX8Brz6qZgDwBbAAAAAASUVORK5CYII="
        />
      </defs>
    </svg>
  );
}
