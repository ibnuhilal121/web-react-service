export default function IconEdit() {
  return (
    <svg
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3 18.2466V21.9966H6.75L17.81 10.9366L14.06 7.18664L3 18.2466ZM20.71 8.03664C21.1 7.64664 21.1 7.01664 20.71 6.62664L18.37 4.28664C17.98 3.89664 17.35 3.89664 16.96 4.28664L15.13 6.11664L18.88 9.86664L20.71 8.03664Z"
        fill="#0061A7"
      />
    </svg>
  );
}
