export default function ChevronRight() {
  return (
    <svg width="7" height="10" viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M0.666504 8.56968L4.22873 4.99967L0.666504 1.42967L1.76317 0.333008L6.42984 4.99967L1.76317 9.66634L0.666504 8.56968Z"
        fill="#0061A7"
      />
    </svg>
  );
}