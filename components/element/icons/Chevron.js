export default function IconPlay() {
  return (
    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M11.6665 17.57L15.2287 14L11.6665 10.43L12.7632 9.33334L17.4298 14L12.7632 18.6667L11.6665 17.57Z" fill="#0061A7"/>
    </svg>
  )
}