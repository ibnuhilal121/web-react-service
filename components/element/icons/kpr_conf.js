export default function KprKonf() {
  return (
    <svg
      width="36"
      height="36"
      viewBox="0 0 36 36"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <rect width="36" height="36" fill="url(#pattern0)" />
      <defs>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width="1"
          height="1"
        >
          <use xlinkHref="#image0_1498:1017" transform="scale(0.0208333)" />
        </pattern>
        <image
          id="image0_1498:1017"
          width="48"
          height="48"
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAGD0lEQVRoBe1YfWgbZRgPpUVRdKxdP2ya74/m7nKXu0suzfXSbP1YZ+qlSZOmXbOt27q5le2PTapjHQyqOFQYCOIfOhhDQftHYVDQsq0ypyKdbOBUmB+DDfyCDdFNy9QqRH4xB2m4pGmbtvsjgZc3977Pve/z/N7n+T3PexpN6VdCoIRACYESAiUE1giBw8+d4EdGT+4cHh6rWSMVlrxteZWWGTVTXXN1ltbfDUTHV49s0NUvebXVfLGmxlTLS1vPss1bkzACp9DSOfSDS+z/QKPRlK2mLoveq85AtTJi301OSvxkJgJdygIk097iEvv+tbNbTihjD1xP+2LPcP7EfYKVp+rqHMZsBU1k4FnG15fMNCxbZk2e4TKUJzwBhOmm8PP53ARyXEviF0NjwLQmymZvauPaAlzrtpsub9+deh3VnT2f/VxXZ62GixFs1yWNRlORPb+qz2YicIgSYknS03tJzWVyKVNZaWhmfL1ztNDzUi6ZFR1vaKAqBf+ecTsbSu7ed/yVYDD4UOaGRqPxYaNN3K+3+saqqgxE5pzyn3F3H+KkRNJCbOxRxlal1xoZH+2NXW/k5Dtqm0N50CUjxn8DE9G+2G28o6acg5PfoYToLbiV2nzRx7wtPcMusW9WbN/x4eGjYymWMdmEAJDWm4V2KA/+p73RpKHR24bERbp7fnR6o2+oKYNTYpsH/npCT7vV5os2ptU6qihP+G34u1OInISitbXMo0ab72UYxEkD34MeQaOKAeD9ysrKx3MZAFnKE0mS/FNnVjSY7VSzsLFz1zcOPnKvK7KzT0EF48i0jDc2Y7eLWpKXTxO8/B2MpT3Rq05v7FfGF/8CBmQiDHdxCuFx+D+CGGAoaxa9h3sAYUaMX6mqtzuyNiijm2JH4S5OKb6HEqJvku7uP2AAGIn29twD08BQ5T0rI6XiB3GhFj+KXNF6vcXzHt+SuJEHpTIEItjIKfTMwQBOStyy0W2vwjhOSvxjdvgTUCjl71JiFgG+rkZnKZqS+RZCcLJS4u76WjOdSw6UGkmMfA5WaqSD2+xMxyml3kFgb+occMDPESMwbEX9XUXJcvgz3dT7JYJWZT41BCXh8wj0TBkYDsoFCNZGfzxzruj/kXAeW6+VMxuO2i3t01vo4N2nD74ItshZCoM2ETOKYi5PaD9QJ3n5ssEZUE1miuyy+2oj8yQf2P4zmCGzsc0Dt0Gb8GcHH/pbZxWHFtosRZ28fBoMRXvDr+U7uYXWKnieaYqcw40JFSIoEQ3/9TbvEWRKMAp8HIwElKEkKlA0JCuwDk4L3K9QqBK8BSuxHEGTU/5Ea/JMqK0BBVHH40aFRIaSGcwEtkGpgIaTgp9jHtyvQrnzlsapgFLhdnkYbt47eR/MTOijBrNwNp/QyMjYhmodexByWoswmepNngkYrjUL4zqT5y0r3X4NOSOLacoRXyi1aSF6nORDF11i75+4+KB3ifHzYLJ8ey84V4gBCy6i0WiUxGd0tOzSmYXdg3uPvOvvHLqCE0JsISv/X8CFDqCkthGbOnBqgi/aX8j6OWUWYUCFgiQhyC9kNzAO6huwD+ojgot8hpOBMekbWOYFpgKuSbijyXo90ZFTuUImCjEAvgqe5/zbU0iSfOhr1D7zGitPoUgDq+XJthXIyi5f/Ftk8MG9o8ey7xOF6DxPphADEMyctON9ZNu0j5ene6CqtHnrZj4AADBTOrHNwpUMNl9x8kOKhSzCZAGcDaUX+ytDFgY7wbVQiWZWp4tdTFUen0FwnEhWBC+fRxbNV/+oLqIyiM8nKLdR1GGPXDczlVcXN0SSLOsVewcpIXSA5OVJVJb49OH29c+goEsfdcHog104sf/j1H2hKXIOH7sWp9EypVHTgwphDBJWqqZxd19z8MFjoL9cyQeZGFk9Fei8fBnBvExVlv86gtfKdoTpptip1EXdn7iP6lRnFl4H7+OGhYyKAEeWBu+nvw/lLPyWr9USVwDyQJXiN5+xMfJ1A7Fl1kgFk+iRhdP1z4OnuJq909NX1124MCNNX/w0jH5q6sa8b0Nq75TGSgiUECghUEJgTRD4D/vf/xvW1Hd3AAAAAElFTkSuQmCC"
        />
      </defs>
    </svg>
  );
}
