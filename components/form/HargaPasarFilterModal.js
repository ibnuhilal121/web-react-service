import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";

export default function HargaPasarFilter(props) {
  const [sort, setSort] = useState("");
  const [mounth, setMounth] = useState("");
  const [year, setYear] = useState("");
  const [panjangBangunan, setPanjangBangunan] = useState(0);
  const [lebarBangunan, setLebarBangunan] = useState(0);
  const [panjangTanah, setPanjangTanah] = useState(0);
  const [lebarTanah, setLebarTanah] = useState(0);
  const [errLuasTanah, setErrLuasTanah] = useState(false);
  const [errLuasBangunan, setErrLuasBangunan] = useState(false);

  useEffect(() => {
    props.setDataFilter({
      sort,
      mounth,
      year,
      panjangBangunan,
      lebarBangunan,
      panjangTanah,
      lebarTanah,
    });
  }, [sort, mounth, year, panjangBangunan, lebarBangunan, panjangTanah, lebarTanah]);

  useEffect(() => {
    if(parseInt(panjangTanah) > parseInt(lebarTanah)){
      setErrLuasTanah(true)
    }
    if(parseInt(panjangTanah) < parseInt(lebarTanah) || parseInt(lebarTanah) == 0 ){
      setErrLuasTanah(false)
    }
    if(parseInt(panjangBangunan) > parseInt(lebarBangunan)){
      setErrLuasBangunan(true)
    }
    if(parseInt(panjangBangunan) < parseInt(lebarBangunan) || parseInt(lebarBangunan) == 0 ){
      setErrLuasBangunan(false)
    }
  },[lebarBangunan, lebarTanah, panjangTanah, panjangBangunan])

  const handleChange = (e) => {
    setSort(e.target.value);
  };

  const handleChangeMounth = (e) => {
    setMounth(e.target.value);
  };

  const handleChangeYear = (e) => {
    setYear(e.target.value);
  };

  const handleReset = (e) => {
    props.setReqBody(prevData => ({
      ...prevData,
      sort: "",
      bln: "",
      thn: "",
      ls_tnh_min: 0,
      ls_tnh_max: 0,
      ls_bgn_min: 0,
      ls_bgn_max: 0,
    }));
    e.preventDefault();
    setSort("");
    setMounth("");
    setYear("");
    setPanjangBangunan(0);
    setLebarBangunan(0);
    setPanjangTanah(0);
    setLebarTanah(0);
  }

  // console.log(panjangBangunan, lebarBangunan, panjangTanah, lebarTanah);

  return (
    <div style={{ paddingTop: "74px" }} className="modal fade gray-bg" id="modalFilter" aria-hidden="true" aria-labelledby="modalFilter" tabIndex="-1">
      <div id="card_filter_hrg" className="card card_filter modal-dialog modal-fullscreen-md-down">
        <div style={{ marginBottom: "35px" }} className="card-header p-2 d-flex flex-column">
          <button ref={props.refBtn} type="button" className="btn-close align-self-end m-2" data-bs-dismiss="modal" aria-label="Close">
            {/* <i className="bi bi-x-lg"></i> */}
          </button>
          <h5
            className="mt-0 m-3 row"
            style={{
              fontFamily: "FuturaBT",
              fontSize: "16px",
              fontWeight: "700",
            }}
          >
            Filter
          </h5>
        </div>
        <div className="card-body modal-content gray-bg">
          <form>
            <div className="item_form">
              <a data-bs-toggle="collapse" className="filter_title" href="#filter_urutan" role="button" aria-expanded="false" aria-controls="collapseExample">
                <h5>Urutkan</h5>
              </a>
              <div className="collapse show" id="filter_urutan">
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="urutan" value={"j_vw DESC"} checked={sort == "j_vw DESC"} id="urutan_Relevansi" onChange={handleChange} />
                  <label className="form-check-label" htmlFor="urutan_Relevansi">
                    Relevansi
                  </label>
                </div>
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="urutan" id="urutan_Terbaru" value={"thn DESC"} checked={sort == "thn DESC"} onChange={handleChange} />
                  <label className="form-check-label" htmlFor="urutan_Terbaru">
                    Terbaru
                  </label>
                </div>
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="urutan" id="urutan_Termurah" value={"hrg ASC"} checked={sort == "hrg ASC"} onChange={handleChange} />
                  <label className="form-check-label" htmlFor="urutan_Termurah">
                    Termurah
                  </label>
                </div>
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="urutan" id="urutan_Termahal" value={"hrg DESC"} checked={sort == "hrg DESC"} onChange={handleChange} />
                  <label className="form-check-label" htmlFor="urutan_Termahal">
                    Termahal
                  </label>
                </div>
              </div>
            </div>

            <div className="item_form">
              <a data-bs-toggle="collapse" className="filter_title" href="#filter_bulan" role="button" aria-expanded="false" aria-controls="collapseExample">
                <h5>Bulan</h5>
              </a>
              <div className="collapse show" id="filter_bulan">
                <div className="form-group">
                  <select
                    id="card_filter_input"
                    style={{
                      height: "40px",
                      borderRadius: "8px",

                      backgroundColor: "#fafafa",
                    }}
                    className="form-select custom-select"
                    name="search[bulan]"
                    onChange={handleChangeMounth}
                    value={mounth}
                  >
                    <option value="">Semua</option>
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a data-bs-toggle="collapse" className="filter_title" href="#filter_Tahun" role="button" aria-expanded="false" aria-controls="collapseExample">
                <h5>Tahun</h5>
              </a>
              <div className="collapse show" id="filter_Tahun">
                <div className="form-group">
                  <select
                    id="card_filter_input"
                    style={{
                      height: "40px",
                      borderRadius: "8px",

                      backgroundColor: "#fafafa",
                    }}
                    className="form-select custom-select"
                    name="search[tahun]"
                    onChange={handleChangeYear}
                    value={year}
                  >
                    <option value="">Semua</option>
                    <option value="2016">2016 </option>
                    <option value="2017">2017 </option>
                    <option value="2018">2018 </option>
                    <option value="2019">2019 </option>
                    <option value="2020">2020 </option>
                    <option value="2021">2021 </option>
                  </select>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a data-bs-toggle="collapse" className="filter_title" href="#filter_Luas_Tanah" role="button" aria-expanded="false" aria-controls="collapseExample">
                <h5>Luas Tanah</h5>
              </a>
              <div className="collapse show" id="filter_Luas_Tanah align-items-center">
                <div className="d-flex align-items-center justify-content-between">
                  <div className="">
                    <div
                      id="card_filter_input_m"
                      className="input-group"
                      style={{
                        height: "40px",
                      }}
                    >
                      <NumberFormat thousandSeparator="." decimalSeparator="," className="form-control" value={panjangTanah} onChange={(event) => setPanjangTanah(event.target.value)} />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      width: "12px",
                      height: "2px",
                      backgroundColor: "#AAAAAA",
                      borderRadius: "21px",
                      margin: "0px 5px",
                    }}
                  ></div>
                  <div className="">
                    <div id="card_filter_input_m" className="input-group" style={{ height: "40px" }}>
                      <NumberFormat thousandSeparator="." decimalSeparator="," className="form-control" value={lebarTanah} onChange={(event) => setLebarTanah(event.target.value)} />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {errLuasTanah && (
                <div
                  className="error-input"
                  >
                    <i
                      class="bi bi-exclamation-circle-fill"
                      style={{ marginRight: "5px" }}
                      ></i>
                      angka max tidak boleh lebih kecil dari angka min
                </div>
              )}
            </div>
            <div className="item_form">
              <a data-bs-toggle="collapse" className="filter_title" href="#filter_Luas_Bangunan" role="button" aria-expanded="false" aria-controls="collapseExample">
                <h5>Luas Bangunan</h5>
              </a>
              <div className="collapse show" id="filter_Luas_Bangunan">
                <div className="d-flex align-items-center justify-content-between">
                  <div className="">
                    <div id="card_filter_input_m" className="input-group" style={{ height: "40px" }}>
                      <NumberFormat thousandSeparator="." decimalSeparator="," className="form-control" value={panjangBangunan} onChange={(event) => setPanjangBangunan(event.target.value)} />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      width: "12px",
                      height: "2px",
                      backgroundColor: "#AAAAAA",
                      borderRadius: "21px",
                      margin: "0px 5px",
                    }}
                  ></div>
                  <div>
                    <div id="card_filter_input_m" className="input-group" style={{ height: "40px" }}>
                      <NumberFormat thousandSeparator="." decimalSeparator="," className="form-control" value={lebarBangunan} onChange={(event) => setLebarBangunan(event.target.value)} />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {errLuasBangunan && (
                <div
                  className="error-input"
                  >
                    <i
                      class="bi bi-exclamation-circle-fill"
                      style={{ marginRight: "5px" }}
                      ></i>
                      angka max tidak boleh lebih kecil dari angka min
                </div>
              )}
            </div>

            <div className="modal-footer d-flex justify-content-between">
              <button onClick={handleReset} className="btn" style={{ color: "#0061A7" }}>
                Reset
              </button>
              <button className="btn btn-main btn-apply-filter" onClick={props.onSearch}>Terapkan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
