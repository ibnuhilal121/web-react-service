import Link from "next/link";
import { useEffect, useState } from "react";

export default function DeveloperFilterModal({
  setJenisProperti = () => {},
  setUrutkan = () => {},
  onSearch = () => {},
  closeRef
}) {
  const [rumah, setRumah] = useState(false);
  const [apartemen, setApartemen] = useState(false);
  const [ruko, setRuko] = useState(false);
  const [kantor, setKantor] = useState(false);
  const [sort, setSort] = useState(null);

  useEffect(() => {
    const jenisPropertiList = []
    if (rumah) {
      jenisPropertiList.push(1);
    }
    if (apartemen) {
      jenisPropertiList.push(2);
    }
    if (ruko) {
      jenisPropertiList.push(6);
    }
    if (kantor) {
      jenisPropertiList.push(7);
    }
    setJenisProperti(jenisPropertiList.join("%2C"));
  }, [rumah, apartemen, ruko, kantor]);

  return (
    <div
      style={{ marginTop: 0 }}
      className="modal fade"
      id="modalFilterDeveloper"
      aria-hidden="true"
      aria-labelledby="modalFilter"
      tabIndex="-1"
    >
      <div
        className="card card_filter modal-dialog modal-fullscreen-md-down"
        style={{
          height: "auto",
          position: "absolute",
          bottom: 0,
          backgroundColor: "#fafafa",
          borderRadius: "32px 32px 0 0",
          overflow: "hidden",
          padding: "10px 24px 0px"
        }}
      >
        <div className="card-header p-2 d-flex flex-column" style={{marginBottom: '10px' }}>
          <button
            type="button"
            className="btn-close align-self-end m-2 me-0 mt-0"
            data-bs-dismiss="modal"
            aria-label="Close"
            ref={closeRef}
          >
            {/* <i className="bi bi-x-lg"></i> */}
          </button>
          <h5
            className="mt-0 m-2 row"
            style={{
              fontFamily: "FuturaBT",
              fontSize: "16px",
              fontWeight: "700",
            }}
          >
            Filter
          </h5>
        </div>
        <div className="card-body modal-content" style={{backgroundColor: "#fafafa"}}>
          <form>
            <div className="item_form no-border-bottom" style={{ marginTop: 10}}>
              <div className="filter_title developer_filter">
                <h5>Tipe Properti</h5>
              </div>
              <div className="collapse show" id="filter_tipe">
                <div className="form-check d-flex align-items-center" style={{ marginBottom: "16px" }}>
                  <input
                    className="form-check-input mt-0 me-1"
                    type="checkbox"
                    checked={rumah}
                    onChange={(e) => setRumah(e.target.checked)}
                    id="filterRumah"
                  />
                  <label
                    className="form-check-label"
                    htmlFor="filterRumah"
                    style={{ fontWeight: 400 }}
                  >
                    Rumah
                  </label>
                </div>
                <div className="form-check d-flex align-items-center" style={{ marginBottom: "16px" }}>
                  <input
                    className="form-check-input mt-0 me-1"
                    type="checkbox"
                    value=""
                    id="filterApartemen"
                    checked={apartemen}
                    onChange={(e) => setApartemen(e.target.checked)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="filterApartemen"
                  >
                    Apartemen
                  </label>
                </div>
                <div className="form-check d-flex align-items-center" style={{ marginBottom: "16px" }}>
                  <input
                    className="form-check-input mt-0 me-1"
                    type="checkbox"
                    value=""
                    id="filterRuko"
                    checked={ruko}
                    onChange={(e) => setRuko(e.target.checked)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="filterRuko"
                  >
                    Ruko
                  </label>
                </div>
                <div className="form-check d-flex align-items-center" style={{ marginBottom: "16px" }}>
                  <input
                    className="form-check-input mt-0 me-1"
                    type="checkbox"
                    value=""
                    id="filterKantor"
                    checked={kantor}
                    onChange={(e) => setKantor(e.target.checked)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="filterKantor"
                  >
                    Kantor
                  </label>
                </div>
              </div>
            </div>
            <div className="item_form no-border-bottom">
              <div className="filter_title developer_filter">
                <h5 style={{ marginBottom: "14px" }}>Urutkan</h5>
              </div>
              <div className="collapse show" id="filter_urutan">
                <div className="form-check" style={{ marginBottom: "14px" }}>
                  <input
                    className="form-check-input me-1"
                    type="radio"
                    name="urutan"
                    id="urutan_Relevansi"
                    onChange={(e) => setUrutkan(1)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="urutan_Relevansi"
                  >
                    Terpopuler
                  </label>
                </div>
                <div className="form-check" style={{ marginBottom: "14px" }}>
                  <input
                    className="form-check-input me-1"
                    type="radio"
                    name="urutan"
                    id="urutan_Terbaru"
                    onChange={(e) => setUrutkan(2)}
                  />
                  <label className="form-check-label" htmlFor="urutan_Terbaru">
                    Terbaru
                  </label>
                </div>
                <div className="form-check" style={{ marginBottom: "14px" }}>
                  <input
                    className="form-check-input me-1"
                    type="radio"
                    name="urutan"
                    id="urutan_A-Z"
                    onChange={(e) => setUrutkan(3)}
                  />
                  <label className="form-check-label" htmlFor="urutan_A-Z">
                    (A - Z)
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input me-1"
                    type="radio"
                    name="urutan"
                    id="urutan_Z-A"
                    onChange={(e) => setUrutkan(4)}
                  />
                  <label className="form-check-label" htmlFor="urutan_Z-A">
                    (Z - A)
                  </label>
                </div>
              </div>
            </div>
            <div className="modal-footer d-flex justify-content-between" style={{ border: "none"}}>
              <button
                style={{ width: "100%" }}
                className="btn btn-main btn-apply-filter"
                onClick={(e) => {
                  e.preventDefault();
                  onSearch();
                }}
              >
                Terapkan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
