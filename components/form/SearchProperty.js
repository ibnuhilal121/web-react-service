import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import { types } from "../../helpers/pencarianProperti";
import { getFasilitas } from "../../services/master";

export default function SearchProperty({ handleChange, values = {} }) {
  const [akses, setAkses] = useState([])
  const [fasilitases, setFasilitases] = useState([])
  const [kelengkapanRumahs, setKelengkapanRumahs] = useState([])
  const { fasilitas = [] } = values;
  const { subsidi = [] } = values;

  function changeByKey(key, value) {
    handleChange({ key, value });
  }
  function handleChangeHargaMin(val) {
    changeByKey("hargaMin", val.floatValue);
  }
  function handleChangeHargaMax(val) {
    changeByKey("hargaMax", val.floatValue);
  }
  function handleChangeKmTidur(val) {
    changeByKey("kamarTidur", val);
  }
  function handleChangeKmMandi(val) {
    changeByKey("kamarMandi", val);
  }
  function handleChangeLTMin(val) {
    changeByKey("luasTanahMin", val.floatValue);
  }
  function handleChangeLTMax(val) {
    changeByKey("luasTanahMax", val.floatValue);
  }
  function handleChangeLBMin(val) {
    changeByKey("luasBangunanMin", val.floatValue);
  }
  function handleChangeLBMax(val) {
    changeByKey("luasBangunanMax", val.floatValue);
  }
  function handleCheck(e, id) {
    let checked = e.target.checked;
    if (checked) {
      changeByKey("fasilitas", [...fasilitas, id]);
    } else {
      changeByKey(
        "fasilitas",
        fasilitas.filter((val) => val != id)
      );
    }
  }
  function handleClickCheckTipe(e) {
    const { checked, value } = e.target;
    // console.log('~ checked, name', checked, name)
    const { tipeProperti = [] } = values;
    let newSelectedTipes;
    if (checked) {
      newSelectedTipes = [...tipeProperti, value];
    } else {
      newSelectedTipes = tipeProperti.filter((el) => el != value);
    }
    changeByKey("tipeProperti", newSelectedTipes);
  }
  function handleClickCheckJenis(e) {
    const { checked, name } = e.target;
    // console.log('~ checked, name', checked, name)
    const id = name === "filterSubsidi" ? "1" : name === "filterNonSubsidi" ? "0" : "";
    const { subsidi = "" } = values;
    if (checked) {
      changeByKey("subsidi", id);
    } else {
      changeByKey("subsidi", subsidi.replace(id, ""));
    }
  }
  function handleChangeSort(e) {
    changeByKey("sort", e.target.value);
  }
  function handleChangeVirtual(e) {
    if (e.target.checked) {
      changeByKey("virtual", "1");
    } else {
      changeByKey("virtual", "");
    }
  }
  const radioOptions = [
    {
      value: "1",
      label: "Relevansi",
    },
    {
      value: "2",
      label: "Terbaru",
    },
    {
      value: "3",
      label: "Termurah",
    },
    {
      value: "4",
      label: "Termahal",
    },
  ];
  useEffect(() => {
    getFasilitas()
      .then((data) => {
        // console.log('~ data', data)
        const { Data } = data
        const idAkses = Data?.find(val => val.n?.toLowerCase()?.includes('akses'))?.id
        const idFasilitas = Data?.find(val => val.n?.toLowerCase()?.includes('fasilitas'))?.id
        const idKelengkapan = Data?.find(val => val.n?.toLowerCase()?.includes('rumah'))?.id
        if (idAkses) {
          setAkses(Data?.filter(val => val.i_par === idAkses))
        }
        if (idFasilitas) {
          setFasilitases(Data?.filter(val => val.i_par === idFasilitas))
        }
        if (idKelengkapan) {
          setKelengkapanRumahs(Data?.filter(val => val.i_par === idKelengkapan))
        }
      }).catch((err) => {
        console.log('~ err', err)
      });
  }, [])
  return (
    <div>
      <div className="card card_filter" id="card_filter_property">
        <div className="card-header">
          <h4 className="section_title mb-0">Filter</h4>
        </div>
        <div className="card-body">
          <form>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_urutan"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <div>
                  <h5 style={{ fontFamily: "FuturaBT" }}>Urutkan </h5>
                </div>
              </a>
              <div className="collapse show" id="filter_urutan">
                {radioOptions.map((val) => {
                  return (
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        id={val.label}
                        onChange={handleChangeSort}
                        value={val.value}
                        name="sort"
                        checked={values.sort == val.value}
                        style={{ width: "20px", height: "20px" }}
                      />
                      <label className="form-check-label mt-0" htmlFor={val.label}>
                        {val.label}
                      </label>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_harga"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Harga</h5>
              </a>
              <div className="collapse show" id="filter_harga">
                <div className="input-container">
                  <div className="">
                    <div className="input-group mb-0">
                      <span
                        className="input-group-text pe-1 pe-xl-2 ps-1 ps-md-2 ps-xl-3"
                        style={{ background: "transparent" }}
                      >
                        Rp
                      </span>
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control px-0 pe-xl-3"
                        placeholder=" Minimum"
                        onValueChange={handleChangeHargaMin}
                        value={values.hargaMin}
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      borderTop: "1px solid #AAAAAA",
                      width: 12,
                      height: 2,
                      margin: "0 4px",
                    }}
                  ></div>
                  <div className="">
                    <div className="input-group mb-0">
                      <span
                        className="input-group-text pe-1 pe-xl-2 ps-1 ps-md-2 ps-xl-3"
                        style={{ background: "transparent" }}
                      >
                        Rp
                      </span>
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control px-0 pe-xl-3"
                        placeholder="Maximum"
                        onValueChange={handleChangeHargaMax}
                        value={values.hargaMax}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_tipe"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Tipe Properti</h5>
              </a>
              <div className="collapse show" id="filter_tipe">
                {types.map(({ id, slug, label }) => {
                  return (
                    <div className="form-check">
                      <input
                        className="form-check-input form-pencarian-radio"
                        style={{ width: "20px", height: "20px" }}
                        type="checkbox"
                        id={slug}
                        name={slug}
                        value={id}
                        checked={values?.tipeProperti?.includes(id)}
                        onChange={handleClickCheckTipe}
                      />
                      <label className="form-check-label" htmlFor={slug}>
                        {label}
                      </label>
                    </div>
                  );
                })}
                <hr
                  style={{
                    color: "rgba(102, 102, 102, 0.4)",

                    width: "100%",
                  }}
                />
                <div className="form-check" style={{ display: "flex" }}>
                  <input
                    className="form-check-input form-pencarian-radio"
                    type="checkbox"
                    id="filter_virtual"
                    onChange={handleChangeVirtual}
                    checked={values.virtual}
                  />
                  <label className="form-check-label" htmlFor="filter_virtual">
                    Lihat perumahan yang memiliki Virtual Tur
                  </label>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_KT"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Kamar Tidur</h5>
              </a>
              <div className="collapse show" id="filter_KT">
                <Stepper value={values.kamarTidur} handleChangeCount={handleChangeKmTidur} />
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_KM"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Kamar Mandi</h5>
              </a>
              <div className="collapse show" id="filter_KM">
                <Stepper value={values.KamarMandi} handleChangeCount={handleChangeKmMandi} />
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Luas_Tanah"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Luas Tanah</h5>
              </a>
              <div className="collapse show" id="filter_Luas_Tanah">
                <div className="input-container">
                  <div className="">
                    <div className="input-group">
                      <NumberFormat
                        placeholder="0"
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        onValueChange={handleChangeLTMin}
                        value={values.luasTanahMin}
                      />
                      <span
                        style={{ fontWeight: "700", color: "#00193E" }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m<sup>2</sup>
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      borderTop: "2px solid #AAAAAA",
                      width: 12,
                      height: 2,
                      margin: "0 4px",
                    }}
                  ></div>
                  <div className="">
                    <div className="input-group">
                      <NumberFormat
                        placeholder="0"
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        onValueChange={handleChangeLTMax}
                        value={values.luasTanahMax}
                      />
                      <span
                        style={{ fontWeight: "700", color: "#00193E" }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m<sup>2</sup>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Luas_Bangunan"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Luas Bangunan</h5>
              </a>
              <div className="collapse show" id="filter_Luas_Bangunan">
                <div className="input-container">
                  <div className="">
                    <div className="input-group">
                      <NumberFormat
                        placeholder="0"
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        onValueChange={handleChangeLBMin}
                        value={values.luasBangunanMin}
                      />
                      <span
                        style={{ fontWeight: "700", color: "#00193E" }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m<sup>2</sup>
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      borderTop: "2px solid #AAAAAA",
                      width: 12,
                      height: 2,
                      margin: "0 4px",
                    }}
                  ></div>
                  <div className="">
                    <div className="input-group">
                      <NumberFormat
                        placeholder="0"
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        onValueChange={handleChangeLBMax}
                        value={values.luasBangunanMax}
                      />
                      <span
                        style={{ fontWeight: "700", color: "#00193E" }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m<sup>2</sup>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Jenis"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Jenis Properti</h5>
              </a>
              <div className="collapse show" id="filter_Jenis">
                <div className="form-check">
                  <input
                    className="form-check-input form-pencarian-radio"
                    type="checkbox"
                    id="filter_Subsidi"
                    name="filterSubsidi"
                    checked={subsidi === "1" ? true : false}
                    onChange={handleClickCheckJenis}
                  />
                  <label className="form-check-label" htmlFor="filter_Subsidi">
                    Subsidi
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input form-pencarian-radio"
                    type="checkbox"
                    id="filter_NonSubsidi"
                    name="filterNonSubsidi"
                    checked={subsidi === "0" ? true : false}
                    onChange={handleClickCheckJenis}
                  />
                  <label className="form-check-label" htmlFor="filter_NonSubsidi">
                    NonSubsidi
                  </label>
                </div>
              </div>
            </div>

            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Kelengkapan"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Kelengkapan Rumah</h5>
              </a>
              <div className="collapse show" id="filter_Kelengkapan">
                <div className="row">
                  {kelengkapanRumahs.map(({ n, id }) => (
                    <div className="col-md-6" key={id}>
                      <div className="form-check">
                        <input
                          className="form-check-input form-pencarian-radio"
                          id="checkboxprop"
                          style={{ width: 20, height: 20 }}
                          type="checkbox"
                          id={id + n}
                          onChange={(e) => handleCheck(e, id)}
                          checked={fasilitas.includes(id + '')}
                        />
                        <label className="form-check-label" htmlFor={id + n}>
                          {n}
                        </label>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Akses"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Akses</h5>
              </a>
              <div className="collapse show" id="filter_Akses">
                <div className="row">
                  {akses.map(({ n, id }) => (
                    <div className="col-md-6" key={id}>
                      <div className="form-check">
                        <input
                          className="form-check-input form-pencarian-radio"
                          type="checkbox"
                          id={id + n}
                          onChange={(e) => handleCheck(e, id)}
                          checked={fasilitas.includes(id + '')}
                        />
                        <label className="form-check-label" htmlFor={id + n}>
                          {n}
                        </label>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Fasilitas"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5 style={{ fontFamily: "FuturaBT" }}>Fasilitas</h5>
              </a>
              <div className="collapse show" id="filter_Fasilitas">
                <div className="row">
                  {fasilitases.map(({ n, id }) => (
                    <div className="col-md-6" key={id}>
                      <div className="form-check">
                        <input
                          className="form-check-input form-pencarian-radio"
                          type="checkbox"
                          id={id + n}
                          onChange={(e) => handleCheck(e, id)}
                          checked={fasilitas.includes(id + '')}
                        />
                        <label className="form-check-label" htmlFor={id + n}>
                          {n}
                        </label>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="mobile_filter" id="mobile_filter_property" >
        <button className="btn" style={{ color: "#0061A7" }} data-bs-toggle="modal" data-bs-target="#filterModal">
          <i className="bi bi-sliders"></i> Filter Properti
        </button>
      </div>
      <div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="filterModalLabel" aria-hidden="true" style={{
        zIndex: 100001
      }}>
        <div class="modal-dialog card_filter modal-fullscreen m-auto">
          <div class="modal-content ">
            <div class="modal-header py-4">
              <h4 className="section_title mb-0">Filter</h4>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body hide-scrollbar" >

              <div className="">
                <form>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_urutan"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <div>
                        <h5 style={{ fontFamily: "FuturaBT" }}>Urutkan </h5>
                      </div>
                    </a>
                    <div className="collapse show" id="filter_urutan">
                      {radioOptions.map((val) => {
                        return (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              id={val.label}
                              onChange={handleChangeSort}
                              value={val.value}
                              name="sort"
                              checked={values.sort == val.value}
                              style={{ width: "20px", height: "20px" }}
                            />
                            <label className="form-check-label mt-0" htmlFor={val.label}>
                              {val.label}
                            </label>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_harga"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Harga</h5>
                    </a>
                    <div className="collapse show" id="filter_harga">
                      <div className="input-container">
                        <div className="">
                          <div className="input-group mb-0">
                            <span
                              className="input-group-text pe-1 pe-xl-2 ps-1 ps-md-2 ps-xl-3"
                              style={{ background: "transparent" }}
                            >
                              Rp
                            </span>
                            <NumberFormat
                              thousandSeparator="."
                              decimalSeparator=","
                              className="form-control px-0 pe-xl-3"
                              placeholder=" Minimum"
                              onValueChange={handleChangeHargaMin}
                              value={values.hargaMin}
                            />
                          </div>
                        </div>
                        <div
                          style={{
                            borderTop: "1px solid #AAAAAA",
                            width: 12,
                            height: 2,
                            margin: "0 4px",
                          }}
                        ></div>
                        <div className="">
                          <div className="input-group mb-0">
                            <span
                              className="input-group-text pe-1 pe-xl-2 ps-1 ps-md-2 ps-xl-3"
                              style={{ background: "transparent" }}
                            >
                              Rp
                            </span>
                            <NumberFormat
                              thousandSeparator="."
                              decimalSeparator=","
                              className="form-control px-0 pe-xl-3"
                              placeholder="Maximum"
                              onValueChange={handleChangeHargaMax}
                              value={values.hargaMax}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_tipe"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Tipe Properti</h5>
                    </a>
                    <div className="collapse show" id="filter_tipe">
                      {types.map(({ id, slug, label }) => {
                        return (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              style={{ width: "20px", height: "20px" }}
                              type="checkbox"
                              id={slug}
                              name={slug}
                              value={id}
                              checked={values?.tipeProperti?.includes(id)}
                              onChange={handleClickCheckTipe}
                            />
                            <label className="form-check-label" htmlFor={slug}>
                              {label}
                            </label>
                          </div>
                        );
                      })}
                      <hr
                        style={{
                          color: "rgba(102, 102, 102, 0.4)",

                          width: "100%",
                        }}
                      />
                      <div className="form-check" style={{ display: "flex" }}>
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="filter_virtual"
                          onChange={handleChangeVirtual}
                          checked={values.virtual}
                        />
                        <label className="form-check-label" htmlFor="filter_virtual">
                          Lihat perumahan yang memiliki Virtual Tur
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_KT"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Kamar Tidur</h5>
                    </a>
                    <div className="collapse show" id="filter_KT">
                      <Stepper value={values.kamarTidur} handleChangeCount={handleChangeKmTidur} />
                    </div>
                  </div>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_KM"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Kamar Mandi</h5>
                    </a>
                    <div className="collapse show" id="filter_KM">
                      <Stepper value={values.KamarMandi} handleChangeCount={handleChangeKmMandi} />
                    </div>
                  </div>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_Luas_Tanah"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Luas Tanah</h5>
                    </a>
                    <div className="collapse show" id="filter_Luas_Tanah">
                      <div className="input-container">
                        <div className="">
                          <div className="input-group">
                            <NumberFormat
                              placeholder="0"
                              thousandSeparator="."
                              decimalSeparator=","
                              className="form-control"
                              onValueChange={handleChangeLTMin}
                              value={values.luasTanahMin}
                            />
                            <span
                              style={{ fontWeight: "700", color: "#00193E" }}
                              className="input-group-text"
                              id="basic-addon2"
                            >
                              m<sup>2</sup>
                            </span>
                          </div>
                        </div>
                        <div
                          style={{
                            borderTop: "2px solid #AAAAAA",
                            width: 12,
                            height: 2,
                            margin: "0 4px",
                          }}
                        ></div>
                        <div className="">
                          <div className="input-group">
                            <NumberFormat
                              placeholder="0"
                              thousandSeparator="."
                              decimalSeparator=","
                              className="form-control"
                              onValueChange={handleChangeLTMax}
                              value={values.luasTanahMax}
                            />
                            <span
                              style={{ fontWeight: "700", color: "#00193E" }}
                              className="input-group-text"
                              id="basic-addon2"
                            >
                              m<sup>2</sup>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_Luas_Bangunan"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Luas Bangunan</h5>
                    </a>
                    <div className="collapse show" id="filter_Luas_Bangunan">
                      <div className="input-container">
                        <div className="">
                          <div className="input-group">
                            <NumberFormat
                              placeholder="0"
                              thousandSeparator="."
                              decimalSeparator=","
                              className="form-control"
                              onValueChange={handleChangeLBMin}
                              value={values.luasBangunanMin}
                            />
                            <span
                              style={{ fontWeight: "700", color: "#00193E" }}
                              className="input-group-text"
                              id="basic-addon2"
                            >
                              m<sup>2</sup>
                            </span>
                          </div>
                        </div>
                        <div
                          style={{
                            borderTop: "2px solid #AAAAAA",
                            width: 12,
                            height: 2,
                            margin: "0 4px",
                          }}
                        ></div>
                        <div className="">
                          <div className="input-group">
                            <NumberFormat
                              placeholder="0"
                              thousandSeparator="."
                              decimalSeparator=","
                              className="form-control"
                              onValueChange={handleChangeLBMax}
                              value={values.luasBangunanMax}
                            />
                            <span
                              style={{ fontWeight: "700", color: "#00193E" }}
                              className="input-group-text"
                              id="basic-addon2"
                            >
                              m<sup>2</sup>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_Jenis"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Jenis Properti</h5>
                    </a>
                    <div className="collapse show" id="filter_Jenis">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="filter_Subsidi"
                          name="filterSubsidi"
                          onChange={handleClickCheckJenis}
                        />
                        <label className="form-check-label" htmlFor="filter_Subsidi">
                          Subsidi
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="filter_NonSubsidi"
                          name="filterNonSubsidi"
                          onChange={handleClickCheckJenis}
                        />
                        <label className="form-check-label" htmlFor="filter_NonSubsidi">
                          NonSubsidi
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_Kelengkapan"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Kelengkapan Rumah</h5>
                    </a>
                    <div className="collapse show" id="filter_Kelengkapan">
                      <div className="row">
                        {kelengkapanRumahs.map(({ n, id }) => (
                          <div className="col-6" key={id}>
                            <div className="form-check">
                              <input
                                className="form-check-input me-1"
                                id="checkboxprop"
                                style={{ width: 20, height: 20 }}
                                type="checkbox"
                                id={id + n}
                                onChange={(e) => handleCheck(e, id)}
                                checked={fasilitas.includes(id + '')}
                              />
                              <label className="form-check-label" htmlFor={id + n}>
                                {n}
                              </label>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>

                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_Akses"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Akses</h5>
                    </a>
                    <div className="collapse show" id="filter_Akses">
                      <div className="row">
                        {akses.map(({ n, id }) => (
                          <div className="col-6" key={id}>
                            <div className="form-check">
                              <input
                                className="form-check-input me-1"
                                type="checkbox"
                                id={id + n}
                                onChange={(e) => handleCheck(e, id)}
                                checked={fasilitas.includes(id + '')}
                              />
                              <label className="form-check-label" htmlFor={id + n}>
                                {n}
                              </label>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>

                  <div className="item_form">
                    <a
                      data-bs-toggle="collapse"
                      className="filter_title"
                      data-bs-target="#filter_Fasilitas"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                    >
                      <h5 style={{ fontFamily: "FuturaBT" }}>Fasilitas</h5>
                    </a>
                    <div className="collapse show" id="filter_Fasilitas">
                      <div className="row">
                        {fasilitases.map(({ n, id }) => (
                          <div className="col-6" key={id}>
                            <div className="form-check">
                              <input
                                className="form-check-input me-1"
                                type="checkbox"
                                id={id + n}
                                onChange={(e) => handleCheck(e, id)}
                                checked={fasilitas.includes(id + '')}
                              />
                              <label className="form-check-label" htmlFor={id + n}>
                                {n}
                              </label>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
}

SearchProperty.defaultProps = {
  handleChange: () => { },
};

/* --------------------------------- STEPPER -------------------------------- */

const Stepper = ({ handleChangeCount, value }) => {
  const [count, setCount] = useState(0);
  useEffect(() => {
    handleChangeCount(count || null);
  }, [count]);
  useEffect(() => {
    setCount(value ?? 0);
  }, [value]);

  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          width: 40,
          height: 40,
          backgroundColor: "#0061A7",
          borderRadius: "8px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          cursor: "pointer",
        }}
        onClick={() => {
          if (count > 0) setCount(count - 1);
        }}
      >
        <svg
          width="12"
          height="2"
          viewBox="0 0 12 2"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect width="12" height="2" rx="1" fill="white" />
        </svg>
      </div>
      <div
        style={{
          width: 102,
          height: 40,
          border: "1px solid #AAAAAA",
          borderRadius: 8,
          margin: "0 12px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          fontFamily: "Helvetica",
          fontSize: 14,
          fontWeight: 400,
          color: "#666666",
        }}
      >
        {count || "-"}
      </div>
      {count == 10 ? (
        <div
          style={{
            width: 40,
            height: 40,
            backgroundColor: "#666666",
            borderRadius: "8px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor: "not-allowed",
          }}
        >
          <svg
            width="12"
            height="12"
            viewBox="0 0 12 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M6.85714 0.857143C6.85714 0.383756 6.47339 0 6 0C5.52661 0 5.14286 0.383756 5.14286 0.857143V5.14286H0.857143C0.383756 5.14286 0 5.52661 0 6C0 6.47339 0.383756 6.85714 0.857143 6.85714H5.14286V11.1429C5.14286 11.6162 5.52661 12 6 12C6.47339 12 6.85714 11.6162 6.85714 11.1429V6.85714H11.1429C11.6162 6.85714 12 6.47339 12 6C12 5.52661 11.6162 5.14286 11.1429 5.14286H6.85714V0.857143Z"
              fill="white"
            />
          </svg>
        </div>
      ) : (
        <div
          style={{
            width: 40,
            height: 40,
            backgroundColor: "#0061A7",
            borderRadius: "8px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor: "pointer",
          }}
          onClick={() => setCount(count + 1)}
        >
          <svg
            width="12"
            height="12"
            viewBox="0 0 12 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M6.85714 0.857143C6.85714 0.383756 6.47339 0 6 0C5.52661 0 5.14286 0.383756 5.14286 0.857143V5.14286H0.857143C0.383756 5.14286 0 5.52661 0 6C0 6.47339 0.383756 6.85714 0.857143 6.85714H5.14286V11.1429C5.14286 11.6162 5.52661 12 6 12C6.47339 12 6.85714 11.6162 6.85714 11.1429V6.85714H11.1429C11.6162 6.85714 12 6.47339 12 6C12 5.52661 11.6162 5.14286 11.1429 5.14286H6.85714V0.857143Z"
              fill="white"
            />
          </svg>
        </div>
      )}
    </div>
  );
};
