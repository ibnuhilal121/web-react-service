import React, { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import { useAppContext } from "../../context";
import BarsLoader from "../../components/element/BarsLoader";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import Lottie from "react-lottie";
import styles from "../../components/element/modal_confirmation//modal_confirmation.module.scss";
import * as animationData from "../../public/animate_home.json";
import { useMediaQuery } from "react-responsive";
import Link from "next/link";
import { defaultHeaders } from "../../utils/defaultHeaders";

const Informasibisnis = (props) => {
  const { data, validateData, dataCheckbox, updateData, modalSuc, setModalSuc, closeSuccessModal } = props;
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 1024px)` });
  const isWidth400 = useMediaQuery({ query: `(max-width: 350px)` });
  const { userKey } = useAppContext();
  const [active, setActive] = useState();
  const [loaded, setLoaded] = useState(false);
  const [selectedProvince, setSelectedProvince] = useState("");
  const [allProvince, setAllProvince] = useState([]);
  const [allCity, setAllCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState("");
  const [allKecamatan, setAllKecamatan] = useState([]);
  const [selectedKecamatan, setSelectedKecamatan] = useState("");
  const [allKelurahan, setAllKelurahan] = useState([]);
  const [selectedKelurahan, setSelectedKelurahan] = useState("");
  const [posCode, setPosCode] = useState("");
  const [listCheckbox, setListCheckbox] = useState([]);
  const [listCheckbox2, setListCheckbox2] = useState([]);

  const [isEmailErr, setEmailErr] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [validateNoFax, setValidateNoFax] = useState(false);
  const [validateNoTelp, setValidateNoTelp] = useState(false);
  // const [modalSuc, setModalSuc] = useState(false);
  const [loading, setLoading] = useState(false);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  // handle input change Status
  const ChangeValueCheckbox = (e) => {
    console.log(e.target.checked);
    if (e.target.checked) {
      props.changeProfil("st", 1);
    } else {
      props.changeProfil("st", 0);
    }
  };

  // handle click event of the Remove button
  const handleRemoveClick = (index) => {
    const list = [...data.bya_js];
    list.splice(index, 1);
    props.changeProfil("bya_js", list);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    const value = [...data.bya_js, { jasa: "", hrg_rndh: "", hrg_tggi: "" }];
    props.changeProfil("bya_js", value);
    console.log("Added Fields", data.bya_js.length + 1);
  };

  // handle click list grup
  const handleListClick = (value, param) => {
    setActive(value);
    setListCheckbox([]);
    setListCheckbox2([]);
    getListDataLayanan(param);
  };

  // cek if data as checked
  const validateChecked = (value) => {
    const result = dataCheckbox.find((element) => element === value);
    if (result) {
      return true;
    } else {
      return false;
    }
  };

  // cek validate email
  const handleBlurEmail = (value) => {
    const RFC5322 =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // checking empty input
    if (value.trim() === "") {
      setEmailErrorMessage("Isian tidak boleh kosong");
      return setEmailErr(true);
    }
    // checking email format
    if (value.trim() && !RFC5322.test(value.trim())) {
      setEmailErrorMessage("Isi dengan alamat email");
      return setEmailErr(true);
    }

    setEmailErr(false);
    setEmailErrorMessage("");
  };

  // cek validate number
  const handleBlurNumber = (value, box) => {
    let reg = /^\d+$/;
    if (!reg.test(value) && box == "no_tlp") {
      setValidateNoTelp(true);
    } else {
      setValidateNoTelp(false);
    }

    if (!reg.test(value) && box == "no_fax") {
      setValidateNoFax(true);
    } else {
      setValidateNoFax(false);
    }
  }

  const validateSubmit = () => {
    if (isEmailErr || validateNoTelp || validateNoFax) {
      window.scrollTo(0, 200);
    } else {
      props.submitProfil();
    }
  }

  const getListDataLayanan = async (param) => {
    try {
      //setIsLoading(true);
      let accessKey = userKey;
      if (!userKey) {
        accessKey = JSON.parse(sessionStorage.getItem("keyMember")) || JSON.parse(Cookies.get("keyMember"));
      }
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/profesional/layanan/show`;
      const loadData = await fetch(endpoint, {
        method: "POST",
        headers: {
          AccessKey_App: accessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams({
          Sort: "n ASC",
          i_par: param,
          Limit: 30,
        }),
      });
      const response = await loadData.json();
      console.log(response);
      if (!response.IsError) {
        console.log("Success get data Layanan", response.Data);
        if (response.Data.length) {
          let list = [];
          let list2 = [];
          response.Data.map((data, index) => {
            if (index % 2) {
              list2 = [
                ...list2,
                {
                  doc_tbh: data.doc_tbh,
                  doc_wjb: data.doc_wjb,
                  gmbr: data.gmbr,
                  i_par: data.i_par,
                  id: data.id,
                  jm_prfl: data.jm_prfl,
                  n: data.n,
                  seo: data.seo,
                  st: data.st,
                },
              ];
              setListCheckbox2(list2);
            } else {
              list = [
                ...list,
                {
                  doc_tbh: data.doc_tbh,
                  doc_wjb: data.doc_wjb,
                  gmbr: data.gmbr,
                  i_par: data.i_par,
                  id: data.id,
                  jm_prfl: data.jm_prfl,
                  n: data.n,
                  seo: data.seo,
                  st: data.st,
                },
              ];
              setListCheckbox(list);
            }
          });
        }
        //setIsLoading(false);
      } else {
        throw {
          message: response.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
      //setIsLoading(false);
    } finally {
    }
  };

  /* ----------------------------- Get all data alamat ----------------------------- */

  useEffect(() => {
    handleListClick("profesional", 42);
    getAllProvinceData();
    //getListDataLayanan(45);
  }, [data]);


  useEffect(() => {
    /* ------------------ GET CITIES WHEN PROVINCE IS SELECTED ------------------ */
    if (selectedProvince) {
      getCityData();
    } else {
      setAllCity([]);
      setAllKecamatan([]);
      setAllKelurahan([]);
    }
  }, [selectedProvince]);


  useEffect(() => {
    /* ------------------- GET KECAMATAN WHEN CITY IS SELECTED ------------------ */
    if (selectedCity) {
      getKecamatanData();
    } else {
      setAllKecamatan([]);
      setAllKelurahan([]);
    }
  }, [selectedCity]);


  useEffect(() => {
    /* ------------------- GET KELURAHAN WHEN KECAMATAN IS SELECTED ------------------ */
    if (selectedKecamatan) {
      getKelurahanData();
    } else {
      setAllKelurahan([]);
    }
  }, [selectedKecamatan]);

  useEffect(() => {
        setPosCode(getPosCode(selectedKelurahan))
}, [selectedKelurahan])

  
  const getPosCode = (id) => {
    /* ------------------ GET POS WHEN KECAAMATAN IS SELECTED ------------------ */
    console.log("POSTCODE", allKelurahan)
    return allKelurahan.filter((item) => item.id == id).map((data, idx) => data.pos);
   // return allKelurahan.filter((item) => item.id == id).map((data, idx) => props.changeProfil("pos", data.pos));
  };




  const getAllProvinceData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/propinsi/show`;
      const propinsi = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          Sort: "n ASC",
        }),
      });
      const dataPropinsi = await propinsi.json();
      if (!dataPropinsi.IsError) {
        //console.log("GET PROPINSI", dataPropinsi.Data);
        setAllProvince(dataPropinsi.Data);
        if (data?.i_prop != null) {
          setSelectedProvince(data.i_prop);
        }
      } else {
        throw {
          message: dataPropinsi.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoaded(true);
    }
  };

  const getCityData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kota/show`;
      const cities = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_prop: selectedProvince,
          Sort: "n ASC",
        }),
      });
      const dataCities = await cities.json();
      if (!dataCities.IsError) {
        //console.log("GET CITY", dataCities.Data);
        setAllCity(dataCities.Data);
        if (data?.i_kot != null) {
          setSelectedCity(data.i_kot);
        }
      } else {
        throw {
          message: dataCities.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  const getKecamatanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kecamatan/show`;
      const kecamatan = await fetch(endpoint, {
        method: "POST",
        headers: getHeaderWithAccessKey(),
        body: new URLSearchParams({
          i_kot: selectedCity,
          Sort: "n ASC",
        }),
      });
      const dataKecamatan = await kecamatan.json();
      if (!dataKecamatan.IsError) {
        //console.log("GET KECAMATAN", dataKecamatan.Data);
        setAllKecamatan(dataKecamatan.Data);
        if (data?.i_kec != null) {
          setSelectedKecamatan(data.i_kec);
        }
      } else {
        throw {
          message: dataKecamatan.ErrToUser,
        };
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  const getKelurahanData = async () => {
    try {
      const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/master/kelurahan/show`;
      const kelurahan = await fetch(endpoint, {
          method: "POST",
          headers: getHeaderWithAccessKey(),
          body: new URLSearchParams({
              i_kec: selectedKecamatan,
              Sort: "n ASC",
          }),
      });
      const dataKelurahan = await kelurahan.json();
      if (!dataKelurahan.IsError) {
          console.log("GET KELURAHAN", dataKelurahan.Data);
          setAllKelurahan(dataKelurahan.Data);
          if (data?.i_kel != null) {
            setSelectedKelurahan(data.i_kel);
          }
      } else {
          throw {
              message: dataKelurahan.ErrToUser,
          };
      }
  } catch (error) {
      console.log(error.message);
    } finally {
      // setLoaded(true);
    }
  };

  if (!loaded) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
      </div>
    );
  }

  const animStr = (i) => `fadeIn ${1500}ms ease-out ${0 * (i + 1)}ms forwards`;

  return (
    <div>
      <div className="row g-md-0">
        <div className="col-md-7">
          <form className="form-validation" id="form-profesional" noValidate="novalidate">
            <div className="floating-label-wrap" style={{ maxWidth: 480 }}>
              <input type="text" className="floating-label-field" name="informasi[nana]" placeholder="Nama Perusahaan" style={{ height: 48 }} onChange={(e) => props.changeProfil("n", e.target.value)} value={data?.n} />

              <label className="floating-label">Nama Perusahaan<span className="modal-required">*</span></label>
              {validateData && !data?.n && (
                <div className="error-input">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isian tidak boleh kosong
                </div>
              )}
            </div>
            <div className="floating-label-wrap">
              <textarea
                className="floating-label-field mobile-text-field floating-label-textarea"
                name="informasi[nana]"
                rows="5"
                placeholder="Deskripsi"
                style={{ maxWidth: 480, height: 200 }}
                onChange={(e) => props.changeProfil("dsk", e.target.value)}
                value={data?.dsk}
              />
              <label className="floating-label" style={{ transform: "translateY(-80%)" }}>
                Deskripsi<span className="modal-required">*</span>
              </label>
              {validateData && !data?.dsk && (
                <div className="error-input">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isian tidak boleh kosong
                </div>
              )}
            </div>
            <div className="row md-0">
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <input
                    type="text"
                    onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    className="floating-label-field"
                    name="informasi[nana]"
                    placeholder="Nomor Telepon"
                    style={{ height: 48, borderColor: validateNoTelp ? "red" : "#aaaaaa" }}
                    onChange={(e) => {
                      props.changeProfil("no_tlp", e.target.value);
                      handleBlurNumber(e.target.value, "no_tlp");
                    }}
                    value={data?.no_tlp} />
                  <label className="floating-label">Nomor Telepon
                  {/* <span className="modal-required">*</span> */}
                  </label>
                  {/* {validateData && !data?.no_tlp && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )} */}
                  {validateNoTelp && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {data?.no_tlp == "" ? setValidateNoTelp(false) : "Format nomor salah"}
                    </div>
                  )}
                </div>
              </div>
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <input
                    type="text"
                    onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    className="floating-label-field"
                    name="informasi[nana]"
                    placeholder="Nomor Fax"
                    style={{ height: 48, borderColor: validateNoFax ? "red" : "#aaaaaa" }}
                    onChange={(e) => {
                      props.changeProfil("no_fax", e.target.value);
                      handleBlurNumber(e.target.value, "no_fax");
                    }}
                    value={data?.no_fax} />
                  <label className="floating-label">Nomor Fax
                  {/* <span className="modal-required">*</span> */}
                  </label>
                  {/* {validateData && !data?.no_fax && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )} */}
                  {validateNoFax && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {data?.no_fax == "" ? setValidateNoFax(false) : "Format nomor salah"}
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="row md-0">
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <input
                    type="email"
                    className="floating-label-field"
                    name="informasi[nana]"
                    placeholder="Email"
                    style={{ height: 48, borderColor: isEmailErr ? "red" : "#aaaaaa" }}
                    onChange={(e) => {
                      props.changeProfil("eml", e.target.value);
                      handleBlurEmail(e.target.value)
                    }}
                    value={data?.eml} />
                  <label className="floating-label">Email
                  {/* <span className="modal-required">*</span> */}
                  </label>
                  {isEmailErr && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {emailErrorMessage}
                    </div>
                  )}
                </div>
              </div>
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <input type="text" className="floating-label-field" name="informasi[nana]" placeholder="Website" style={{ height: 48 }} onChange={(e) => props.changeProfil("web", e.target.value)} value={data?.web} />
                  <label className="floating-label">Website
                  {/* <span className="modal-required">*</span> */}
                  </label>
                  {/* {validateData && !data?.web && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )} */}
                </div>
              </div>
            </div>
            <hr
              style={{
                background: "#EEEEEE",
                height: 1,
                opacity: 1,
                marginTop: 0,
                marginBottom: 10,
              }}
            />
            <h5 style={{ fontSize: "20px" }} className="title_akun">
              Sosial Media
            </h5>
            <div className="floating-label-wrap" style={{ maxWidth: 480 }}>
              <input type="text" className="floating-label-field" name="informasi[fb]" placeholder="Facebook" style={{ height: 48 }} onChange={(e) => props.changeProfil("fb", e.target.value)} value={data?.fb} />
              <label className="floating-label">Facebook</label>
            </div>
            <div className="floating-label-wrap" style={{ maxWidth: 480 }}>
              <input type="text" className="floating-label-field" name="informasi[tw]" placeholder="Twitter" style={{ height: 48 }} onChange={(e) => props.changeProfil("twt", e.target.value)} value={data?.twt} />
              <label className="floating-label">Twitter</label>
            </div>
            <div className="floating-label-wrap" style={{ maxWidth: 480, marginBottom: 0 }}>
              <input type="text" className="floating-label-field" name="informasi[tw]" placeholder="Google+" style={{ height: 48 }} onChange={(e) => props.changeProfil("gpl", e.target.value)} value={data?.gpl} />
              <label className="floating-label">Google+</label>
            </div>
            <div class="checkbox-container">
              <input class="checkbox-input" id="syarat" type="checkbox" onChange={ChangeValueCheckbox} checked={data?.st == 0 ? false : true} />
              <label id="checkbox-member" class="checkbox" for="syarat">
                <span id="checkbox-professional">
                  <svg width="12px" height="10px">
                    <use xlinkHref="#check"></use>
                  </svg>
                </span>
                <span className="label-checkbox-professional">Tampilkan profil di portal profesional</span>
              </label>
            </div>
            <hr
              style={{
                background: "#EEEEEE",
                height: 1,
                opacity: 1,
                marginTop: 0,
                marginBottom: 10,
              }}
            />
            <h5 style={{ fontSize: "20px" }} className="title_akun">
              Layanan yang disediakan
            </h5>
            <div className={isTabletOrMobile ? "" : "d-flex"} style={{ position: "relative" }}>

              { isTabletOrMobile ? (<div className={!isWidth400 ? "row md-0" : null}>
                <div className={isTabletOrMobile ? !isWidth400 ? "col-5" : "col-12" : "col-4"}>
                  <div className="list-group-custom">
                    <ul className="list-group menu_member">
                      <li onClick={() => handleListClick("profesional", 42)} className={active == "profesional" ? "list-group-item active" : "list-group-item"}>
                        Profesional
                      </li>
                      <li onClick={() => handleListClick("pertukangan", 43)} className={active == "pertukangan" ? "list-group-item active" : "list-group-item"}>
                        Pertukangan
                      </li>
                      <li onClick={() => handleListClick("ahli", 44)} className={active == "ahli" ? "list-group-item active" : "list-group-item"}>
                        Ahli
                      </li>
                      <li onClick={() => handleListClick("agen", 45)} className={active == "agen" ? "list-group-item active" : "list-group-item"}>
                        Agen
                      </li>
                      <li onClick={() => handleListClick("lainnya", 46)} className={active == "lainnya" ? "list-group-item active" : "list-group-item"}>
                        Lainnya
                      </li>
                    </ul>
                  </div>
                </div>
                <div className={isTabletOrMobile ? !isWidth400 ? "col-7" : "col-12" : "d-flex ms-2"}>
                  <div>
                    {listCheckbox?.map((data, index) => (
                      <div style={{ height: "40px", animation: animStr(index) }} class="checkbox-container">
                        <input class="checkbox-input" id={data.id} type="checkbox" onChange={(e) => props.cekedLayanan(e, data.id)} checked={validateChecked(data.id)} />
                        <label class="checkbox" for={data.id}>
                          <span id="checkbox-professional">
                            <svg width="12px" height="10px">
                              <use xlinkHref="#check"></use>
                            </svg>
                          </span>
                          <span className="label-checkbox-professional">{data.n}</span>
                        </label>
                      </div>
                    ))}
                  </div>
                  <div className="">
                    {listCheckbox2?.map((data, index) => (
                      <div style={{ height: "40px", animation: animStr(index) }} class="checkbox-container">
                        <input class="checkbox-input" id={data.id} type="checkbox" onChange={(e) => props.cekedLayanan(e, data.id)} checked={validateChecked(data.id)} />
                        <label class="checkbox" for={data.id}>
                          <span id="checkbox-professional">
                            <svg width="12px" height="10px">
                              <use xlinkHref="#check"></use>
                            </svg>
                          </span>
                          <span className="label-checkbox-professional">{data.n}</span>
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              ):(
                <>
                <div className={isTabletOrMobile ? !isWidth400 ? "col-4" : "col-12" : "col-4"}>
                  <div className="list-group-custom">
                    <ul className="list-group menu_member">
                      <li onClick={() => handleListClick("profesional", 42)} className={active == "profesional" ? "list-group-item active" : "list-group-item"}>
                        Profesional
                      </li>
                      <li onClick={() => handleListClick("pertukangan", 43)} className={active == "pertukangan" ? "list-group-item active" : "list-group-item"}>
                        Pertukangan
                      </li>
                      <li onClick={() => handleListClick("ahli", 44)} className={active == "ahli" ? "list-group-item active" : "list-group-item"}>
                        Ahli
                      </li>
                      <li onClick={() => handleListClick("agen", 45)} className={active == "agen" ? "list-group-item active" : "list-group-item"}>
                        Agen
                      </li>
                      <li onClick={() => handleListClick("lainnya", 46)} className={active == "lainnya" ? "list-group-item active" : "list-group-item"}>
                        Lainnya
                      </li>
                    </ul>
                  </div>
                </div>
                <div className={isTabletOrMobile ? !isWidth400 ? "col-4" : "col-12" : "d-flex ms-2"}>
                  <div>
                    {listCheckbox?.map((data, index) => (
                      <div style={{ height: "40px", animation: animStr(index) }} class="checkbox-container">
                        <input class="checkbox-input" id={data.id} type="checkbox" onChange={(e) => props.cekedLayanan(e, data.id)} checked={validateChecked(data.id)} />
                        <label class="checkbox" for={data.id}>
                          <span id="checkbox-professional">
                            <svg width="12px" height="10px">
                              <use xlinkHref="#check"></use>
                            </svg>
                          </span>
                          <span className="label-checkbox-professional">{data.n}</span>
                        </label>
                      </div>
                    ))}
                  </div>
                  <div className="">
                    {listCheckbox2?.map((data, index) => (
                      <div style={{ height: "40px", animation: animStr(index) }} class="checkbox-container">
                        <input class="checkbox-input" id={data.id} type="checkbox" onChange={(e) => props.cekedLayanan(e, data.id)} checked={validateChecked(data.id)} />
                        <label class="checkbox" for={data.id}>
                          <span id="checkbox-professional">
                            <svg width="12px" height="10px">
                              <use xlinkHref="#check"></use>
                            </svg>
                          </span>
                          <span className="label-checkbox-professional">{data.n}</span>
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
                </>
              )}
            </div>
            <hr
              style={{
                background: "#EEEEEE",
                height: 1,
                opacity: 1,
                marginTop: 0,
                marginBottom: 10,
              }}
            />
            <h5 style={{ fontSize: "20px", paddingBottom: "10px" }} className="title_akun">
              Lokasi Layanan
            </h5>
            <div className="floating-label-wrap">
              <textarea
                className="floating-label-field mobile-text-field floating-label-textarea"
                name="informasi[nana]"
                placeholder="Alamat"
                rows="5"
                style={{ maxWidth: 480, height: 128 }}
                onChange={(e) => props.changeProfil("almt", e.target.value)}
                value={data?.almt}
              />
              {validateData && !data?.almt && (
              <div className="error-input">
                <i
                  class="bi bi-exclamation-circle-fill"
                  style={{ marginRight: "5px" }}
                ></i>
                Isian tidak boleh kosong
              </div>
            )}
              <label className="floating-label" style={{ paddingTop: "10px" }}>
                Alamat<span className="modal-required">*</span>
              </label>
            </div>
            
            <div className="row md-0">
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <select
                    className="floating-label-select custom-select-profile form-select"
                    name="profil[i_prop]"
                    id="provinsi"
                    required
                    value={data?.i_prop}
                    onChange={(e) => {
                      props.changeProfil("i_prop", e.target.value);
                      setSelectedProvince(e.target.value);
                      props.changeProfil("i_kot","");
                      props.changeProfil("i_kec","");
                      props.changeProfil("i_kel","");
                      props.changeProfil("pos", "");
                      setPosCode("");
                    }}
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                  >
                    <option value="">Pilih Provinsi</option>
                    {allProvince.length && allProvince.map((province) => <option value={province.id}>{province.n}</option>)}
                  </select>
                  <label className="floating-label">
                    Provinsi<span className="modal-required">*</span>
                  </label>
                  {validateData && !data?.i_prop && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )}
                </div>
              </div>
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <select
                    className="floating-label-select custom-select-profile form-select"
                    name="profil[i_kot]"
                    id="kota"
                    required
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                    value={data?.i_kot}
                    onChange={(e) => {
                      props.changeProfil("i_kot", e.target.value);
                      setSelectedCity(e.target.value);
                      props.changeProfil("i_kec","");
                      props.changeProfil("i_kel","");
                      props.changeProfil("pos", "");
                      setPosCode("");
                    }}
                  >
                    <option value="">Pilih Kota/Kabupaten</option>
                    {allCity.length && allCity.map((city) => <option value={city.id}>{city.n}</option>)}
                  </select>
                  <label className="floating-label">
                    Kota/Kabupaten<span className="modal-required">*</span>
                  </label>
                  {validateData && !data?.i_kot && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )}
                </div>
              </div>
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <select
                    className="floating-label-select custom-select-profile form-select"
                    name="profil[i_kec]"
                    id="kecamatan"
                    required
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                    value={data?.i_kec}
                    onChange={(e) => {
                      props.changeProfil("i_kec", e.target.value);
                      setSelectedKecamatan(e.target.value);
                      props.changeProfil("i_kel","");
                      props.changeProfil("pos", "");
                      setPosCode("");
                    }}
                  >
                    <option value="">Pilih Kecamatan</option>
                    {allKecamatan.length && allKecamatan.map((kecamatan) => <option value={kecamatan.id}>{kecamatan.n}</option>)}
                  </select>
                  <label className="floating-label">
                    Kecamatan<span className="modal-required">*</span>
                  </label>
                  {validateData && !data?.i_kec && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )}
                </div>
              </div>
              <div className="col-6" style={{ maxWidth: "250px" }}>
                <div className="floating-label-wrap" style={{ maxWidth: "228px" }}>
                  <select
                    className="floating-label-select custom-select-profile form-select"
                    name="profil[i_kel]"
                    id="kelurahan"
                    required
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                    }}
                    value={data?.i_kel}
                    onChange={(e) => {
                      setPosCode(getPosCode(e.target.value))
                      props.changeProfil("i_kel", e.target.value);
                      props.changeProfil("pos", getPosCode(e.target.value))
                      setSelectedKelurahan(e.target.value);
                    }}
                  >
                    <option value="">Pilih Kelurahan</option>
                    {allKelurahan.length && allKelurahan.map((kelurahan) => <option value={kelurahan.id}>{kelurahan.n}</option>)}
                  </select>
                  <label className="floating-label">
                    Kelurahan<span className="modal-required">*</span>
                  </label>
                  {validateData && !data?.i_kel && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      Isian tidak boleh kosong
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="floating-label-wrap" style={{ maxWidth: 480 }}>
              <input type="text" className="floating-label-field" name="profil[pos]" id="kodepos" placeholder="Kodepos" value={posCode} 
              onChange={(e) => {
                props.changeProfil("pos", e.target.value)
                setPosCode(e.target.value)
              }
              } />
              <label className="floating-label">
                KodePos<span className="modal-required">*</span>
              </label>
              {validateData && !data?.pos && (
                <div className="error-input">
                  <i
                    class="bi bi-exclamation-circle-fill"
                    style={{ marginRight: "5px" }}
                  ></i>
                  Isian tidak boleh kosong
                </div>
              )}
            </div>
            <hr
              style={{
                background: "#EEEEEE",
                height: 1,
                opacity: 1,
                marginTop: 0,
                marginBottom: 10,
              }}
            />
            <h5 style={{ fontSize: "20px", marginBottom: "15px" }} className="title_akun">
              Biaya Pekerjaan
            </h5>

            {data?.bya_js.length > 0
              ? data.bya_js.map((x, i) => {
                return (
                  <div style={{ opacity: 0, animation: animStr(i) }}>
                    <label
                      className=""
                      style={{
                        fontStyle: "normal",
                        fontFamily: "FuturaBT",
                        fontSize: "14px",
                        color: "#00193e",
                        fontWeight: "700",
                        lineHeight: "130%",
                      }}
                    >
                      Jasa<span className="modal-required">*</span>
                    </label>
                    <div
                      className="floating-label-wrap"
                      style={{
                        paddingTop: 0,
                        maxWidth: 480,
                        paddingBottom: 0,
                        marginBottom: "15px",
                      }}
                    >
                      <input
                        type="text"
                        className="floating-label-field custom-jasa-profesional"
                        name="informasi[fb]"
                        placeholder="Masukan jasa yang anda tawarkan"
                        style={{ height: 48 }}
                        onChange={(e) => props.changeJasa(e.target.value, i, "jasa")}
                        value={x.jasa}
                      />
                      {validateData && !x.jasa && (
                        <div className="error-input">
                          <i
                            class="bi bi-exclamation-circle-fill"
                            style={{ marginRight: "5px" }}
                          ></i>
                          Isian tidak boleh kosong
                        </div>
                      )}
                    </div>
                    <div className="row md-0 align-items-center">
                      <div className="col-5" style={{ maxWidth: "228px" }}>
                        <label
                          style={{
                            fontStyle: "normal",
                            fontFamily: "FuturaBT",
                            fontSize: "14px",
                            color: "#00193e",
                            fontWeight: "700",
                            lineHeight: "130%",
                          }}
                          className=""
                          htmlFor="mulai"
                        >
                          Biaya Mulai<span className="modal-required">*</span>
                        </label>
                        <div
                          className="floating-label-wrap"
                          style={{
                            paddingTop: 0,
                            // width: 198.55,
                            paddingBottom: 0,
                            marginBottom: "25px",
                          }}
                        >
                          <NumberFormat
                            thousandSeparator="."
                            decimalSeparator=","
                            allowNegative={false}
                            className="floating-label-field custom-jasa-profesional"
                            placeholder="Contoh; Rp. 10.000.000"
                            value={x.hrg_rndh}
                            onValueChange={(e) => props.changeJasa(e.value, i, "hrg_rndh")}
                            prefix="Rp. "
                          // style={{
                          //   fontFamily: "Futura",
                          //   fontWeight: 700,
                          // }}
                          />
                          {validateData && !x.hrg_rndh && (
                            <div className="error-input">
                              <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                              ></i>
                              Isian tidak boleh kosong
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="col-5" style={{ maxWidth: "228px" }}>
                        <label
                          style={{
                            fontStyle: "normal",
                            fontFamily: "FuturaBT",
                            fontSize: "14px",
                            color: "#00193e",
                            fontWeight: "700",
                            lineHeight: "130%",
                          }}
                          className=""
                          htmlFor="sampai"
                        >
                          Biaya Sampai
                          <span className="modal-required">*</span>
                        </label>
                        <div
                          className="floating-label-wrap"
                          style={{
                            paddingTop: 0,
                            // width: 198.55,
                            paddingBottom: 0,
                            marginBottom: "25px",
                          }}
                        >
                          <NumberFormat
                            thousandSeparator="."
                            decimalSeparator=","
                            allowNegative={false}
                            className="floating-label-field custom-jasa-profesional"
                            placeholder="Contoh; Rp. 60.000.000"
                            value={x.hrg_tggi}
                            onValueChange={(e) => props.changeJasa(e.value, i, "hrg_tggi")}
                            prefix="Rp. "
                          // style={{
                          //   fontFamily: "Futura",
                          //   fontWeight: 700,
                          // }}
                          />
                          {validateData && !x.hrg_tggi && (
                            <div className="error-input">
                              <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                              ></i>
                              Isian tidak boleh kosong
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="col-2" style={{textAlign: "left", maxWidth: "228px", paddingLeft: 0, marginBottom: 5}}>
                      {data.bya_js.length - 1 !== i && (
                          <div className="form-group button-submit-area text-right m-md-0" style={{ padding: 0, margin: 0 }}>
                            <div
                              type="submit"
                              className="btn btn-main btn-profesional iconPlus-ibisnis"
                              onClick={() => handleRemoveClick(i)}
                            >
                              <i class="bi bi-x"></i>
                            </div>
                          </div>
                        )}
                        {data.bya_js.length - 1 === i && (
                          <div className="form-group button-submit-area text-right m-md-0" style={{ padding: 0, margin: 0 }}>
                            <div
                              type="submit"
                              className="btn btn-main btn-profesional iconPlus-ibisnis"
                              onClick={handleAddClick}
                            >
                              <i class="bi bi-plus"></i>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                );
              })
              : null}

            <hr
              style={{
                background: "#EEEEEE",
                height: 1,
                opacity: 1,
                marginTop: 0,
                marginBottom: 10,
              }}
            />
            <h5 style={{ fontSize: "20px", marginBottom: "12px" }} className="title_akun">
              Penjelasan Pembayaran
              <span className="modal-required">*</span>
            </h5>
            <div className="floating-label-wrap">
              <textarea
                className="floating-label-field mobile-text-field custom-plc-profesional"
                name="informasi[nana]"
                placeholder="Berikan penjelasan cara pembayaran yang harus dilakukan konsumen"
                rows="5"
                style={{ maxWidth: 480, height: 200 }}
                onChange={(e) => props.changeProfil("dsk_byr", e.target.value)}
                value={data?.dsk_byr}
              />
            </div>
            {validateData && !data?.dsk_byr && (
              <div className="error-input" style={{marginTop: -28, marginBottom: 10}}>
                <i
                  class="bi bi-exclamation-circle-fill"
                  style={{ marginRight: "5px" }}
                ></i>
                Isian tidak boleh kosong
              </div>
            )}
            <div className="form-group button-submit-area text-right d-flex justify-content-end" style={{ maxWidth: 480 }}>
              <div
                type="submit"
                className="btn btn-main btn-profesional padding-ibisnis"
                onClick={() => props.submitProfil()}
                //onClick={() => setModalSuc(true)}
                //onClick={updateData}
                //onClick={validateSubmit}
                data-bs-toogle="modal"
                data-bs-target="#modalSuccessRegister"
              >
                Simpan Profesional
              </div>
            </div>
            {modalSuc && (
              <div className={styles.boxModal}>
                <div className={styles.boxModal__Modal}>
                  <div className="text-center">
                    <button
                      type="button"
                      style={{ background: "none", border: "none" }}
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      data-bs-target="#modalSuccessRegister"
                      onClick={() => {
                        setModalSuc(true);
                        closeSuccessModal(true);
                        router.reload();
                      }}
                    >
                      <svg style={{ marginLeft: 485 }} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                          fill="#0061A7"
                        />
                      </svg>
                    </button>
                    <h4 className="modal-title mb-3">Terima Kasih</h4>

                    {loading && <div className="spinner-border text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />}
                    <Lottie options={defaultOptions} height={250} width={350} isStopped={false} isPaused={false} />
                    <div className="desc text-center mb-5" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                      Data Berhasil Disimpan.
                    </div>
                    <div className={styles.boxModal__Modal__Footer}>
                      <Link href="/">
                        <button
                          type="buttom"
                          className="btn btn-main form-control btn-back px-5"
                          style={{
                            width: "490px",
                            height: "48px",
                            fontSize: "14px",
                            fontFamily: "Helvetica",
                            fontWeight: 700,
                            backgroundColor: "#0061A7",
                          }}
                        >
                          Kembali ke Beranda
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>

      <svg class="checkbox-symbol">
        <symbol id="check" viewbox="0 0 12 10">
          <polyline points="1.5 6 4.5 9 10.5 1" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></polyline>
        </symbol>
      </svg>
    </div>
  );
};

export default Informasibisnis;
