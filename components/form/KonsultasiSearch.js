import React from "react";

export default function KonsultasiSearch({ keyword, setKeyword, category, setCategory, getKonsultasi }) {
  return (
    <div>
      <div className="container main-form">
        <div className="row justify-content-center">
          <div
            className="col-12 col-md-10 p-0"
            id="responsive-konsul-search"
            style={{
              height: "74px",
            }}
          >
            <form
              className="pe-2 w-100 m-auto"
              id="cari-konsultasi"
              onSubmit={(e) => {
                e.preventDefault();
                getKonsultasi();
              }}
            >
              <div className="row align-items-center">
                <div className="col-12">
                  <div className="field-form row no-gutters">
                    <div className="col-6  form-group">
                      <label className="form-control-label title-search" style={{ fontFamily: "FuturaBT", opacity: "75%" }}>
                        Cari
                      </label>
                      <input type="text" className="form-control main-type-input" name="search[keyword]" autoComplete="off" placeholder="Masukkan pertanyaan" value={keyword} onChange={(e) => setKeyword(e.target.value)} />
                    </div>
                    <div className="col-4 form-group ">
                      <label className="form-control-label" style={{ fontFamily: "FuturaBT", opacity: "75%" }}>
                        Kategori{" "}
                      </label>

                      <select id="kategori-profesional" className="form-select resp-select" name="search[kategori]" value={category} onChange={(e) => setCategory(e.target.value)}>
                        <option value="">Semua Kategori</option>
                        <option value="3">KPR</option>
                        <option value="1">Konsultasi Desain</option>
                        <option value="4">Kredit Komersial</option>
                        <option value="2">Perencanaan Keuangan</option>
                        <option value="5">Lainnya</option>
                      </select>
                      {/* <img src="/images/icons/arrow-down.svg" alt="" /> */}
                    </div>
                    <div className="col-2 ps-0 search-icon-container pe-sm-3 d-flex justify-content-end">
                      <button
                        type="submit"
                        className="btn btn-block btn-main font-weight-semibold p-1 d-flex justify-content-center align-items-center"
                      >
                        <i
                          style={{
                            fontSize: "17.49px",
                          }}
                          className="bi bi-search d-flex align-items-center justify-content-center"
                        ></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
