import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";

export default function HargaPasarFilter(props) {
  const [sort, setSort] = useState("");
  const [mounth, setMounth] = useState("");
  const [year, setYear] = useState("");
  const [panjangBangunan, setPanjangBangunan] = useState();
  const [lebarBangunan, setLebarBangunan] = useState();
  const [panjangTanah, setPanjangTanah] = useState();
  const [lebarTanah, setLebarTanah] = useState();
  const [errLuasTanah, setErrLuasTanah] = useState(false);
  const [errLuasBangunan, setErrLuasBangunan] = useState(false);
  const [years] = useState(()=>{
    const max = new Date().getFullYear()
    const min = max - 20
    const tahun = []
    for (let i = min; i <= max; i++) {
      tahun.push({tahun:i.toString()})
    }
    return tahun
  });
  

  const { id } = props;


  useEffect(() => {
    props.setDataFilter({
      sort,
      mounth,
      year,
      panjangBangunan,
      lebarBangunan,
      panjangTanah,
      lebarTanah,
    });
  }, [
    sort,
    mounth,
    year,
    panjangBangunan,
    lebarBangunan,
    panjangTanah,
    lebarTanah,
  ]);

  useEffect(() => {
    if(parseInt(panjangTanah) > parseInt(lebarTanah)){
      setErrLuasTanah(true)
    }
    if(parseInt(panjangTanah) < parseInt(lebarTanah) || parseInt(lebarTanah) == 0 ){
      setErrLuasTanah(false)
    }
    if(parseInt(panjangBangunan) > parseInt(lebarBangunan)){
      setErrLuasBangunan(true)
    }
    if(parseInt(panjangBangunan) < parseInt(lebarBangunan) || parseInt(lebarBangunan) == 0 ){
      setErrLuasBangunan(false)
    }
  },[lebarBangunan, lebarTanah, panjangTanah, panjangBangunan])

  const handleChange = (e) => {
    setSort(e.target.value);
  };

  const handleChangeMounth = (e) => {
    setMounth(e.target.value);
  };

  const handleChangeYear = (e) => {
    setYear(e.target.value);
  };

  // console.log("isi lebar tanah dan bangunan -->",panjangBangunan, lebarBangunan,errLuasBangunan, panjangTanah, lebarTanah, errLuasTanah);

  return (
    <div className="sticky-top mobile-kumpulan">
      <div id="card_filter_hrg" className="card card_filter">
        <div className="card-header">
          <h5
            className="mb-0"
            style={{
              fontFamily: "FuturaBT",
              fontSize: "16px",
              fontWeight: "700",
            }}
          >
            Filter
          </h5>
        </div>
        <div className="card-body">
          <form>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_urutan"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5>Urutkan</h5>
              </a>
              <div className="collapse show" id="filter_urutan">
                <div className="form-check">
                  {id === "terpopuler" ? 
                    <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    value={"j_vw DESC"}
                    id="urutan_Relevansi"
                    onChange={handleChange}
                    checked={true}
                    style={{ width: "20px", height: "20px" }}
                    /> :
                    <input
                          className="form-check-input me-2"
                          type="radio"
                          name="urutan"
                          value={"j_vw DESC"}
                          id="urutan_Relevansi"
                          onChange={handleChange}
                          style={{ width: "20px", height: "20px" }}
                        /> 
                  }
                  
                  <label
                    className="form-check-label m-auto"
                    htmlFor="urutan_Relevansi"
                  >
                    Relevansi
                  </label>
                </div>
                <div className="form-check">
                  {id === "terbaru" ? 
                    <input
                      className="form-check-input me-2"
                      type="radio"
                      name="urutan"
                      id="urutan_Terbaru"
                      value={"thn DESC"}
                      onChange={handleChange} 
                      checked={true}
                      style={{ width: "20px", height: "20px" }}
                    /> :
                    <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Terbaru"
                    value={"thn DESC"}
                    onChange={handleChange}
                    style={{ width: "20px", height: "20px" }}
                    />
                  }
                  <label className="form-check-label m-auto" htmlFor="urutan_Terbaru">
                    Terbaru
                  </label>
                </div>
                <div className="form-check">
                  {id === "termurah" ?
                    <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Termurah"
                    value={"hrg ASC"}
                    onChange={handleChange}
                    checked={true}
                    style={{ width: "20px", height: "20px" }}
                    /> :
                    <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Termurah"
                    value={"hrg ASC"}
                    onChange={handleChange}
                    style={{ width: "20px", height: "20px" }}
                    />
                  }
                  
                  <label className="form-check-label m-auto" htmlFor="urutan_Termurah">
                    Termurah
                  </label>
                </div>
                <div className="form-check">
                  {id === "termahal" ?
                    <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Termahal"
                    value={"hrg DESC"}
                    onChange={handleChange}
                    checked={true}
                    style={{ width: "20px", height: "20px" }}
                    /> :
                    <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Termahal"
                    value={"hrg DESC"}
                    onChange={handleChange}
                    style={{ width: "20px", height: "20px" }}
                   />
                  }
                  <label className="form-check-label m-auto" htmlFor="urutan_Termahal">
                    Termahal
                  </label>
                </div>
              </div>
            </div>

            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                data-bs-target="#filter_bulan"
                className="filter_title"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5>Bulan</h5>
              </a>
              <div className="collapse show" id="filter_bulan">
                <div className="form-group">
                  <select
                    id="card_filter_input"
                    style={{
                      height: "40px",
                      borderRadius: "8px",

                      backgroundColor: "#fafafa",
                    }}
                    className="form-select custom-select"
                    name="search[bulan]"
                    onChange={handleChangeMounth}
                  >
                    <option value="">Semua</option>
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Tahun"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5>Tahun</h5>
              </a>
              <div className="collapse show" id="filter_Tahun">
                <div className="form-group">
                  <select
                    id="card_filter_input"
                    style={{
                      height: "40px",
                      borderRadius: "8px",

                      backgroundColor: "#fafafa",
                    }}
                    className="form-select custom-select"
                    name="search[tahun]"
                    onChange={handleChangeYear}
                  >
                   <option value="">Semua</option>
                    {years.map((item,index)=>{
                      return(
                      <option key={index} value={item.tahun}>{item.tahun}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Luas_Tanah"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5>Luas Tanah</h5>
              </a>
              <div
                className="collapse show"
                id="filter_Luas_Tanah"
              >
                <div className="d-flex align-items-center justify-content-between">
                  <div className="">
                    <div
                      id="card_filter_input_m"
                      className="input-group"
                      style={{
                        height: "40px",
                      }}
                    >
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        value={panjangTanah}
                        placeholder="0"
                        onChange={(event) =>
                          setPanjangTanah(event.target.value)
                        }
                      />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      width: "12px",
                      height: "2px",
                      backgroundColor: "#AAAAAA",
                      borderRadius: "21px",
                      margin: "0px 5px",
                    }}
                  ></div>
                  <div className="">
                    <div
                      id="card_filter_input_m"
                      className="input-group"
                      style={{ height: "40px" }}
                    >
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        value={lebarTanah}
                        placeholder="0"
                        onChange={(event) => setLebarTanah(event.target.value)}
                      />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {errLuasTanah && (
                <div
                  className="error-input-harga-pasar"
                  >
                    <i
                      class="bi bi-exclamation-circle-fill"
                      style={{ marginRight: "5px" }}
                      ></i>
                      angka max tidak boleh lebih kecil dari angka min
                </div>
              )}
            </div>
            <div className="item_form">
              <a
                data-bs-toggle="collapse"
                className="filter_title"
                data-bs-target="#filter_Luas_Bangunan"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample"
              >
                <h5>Luas Bangunan</h5>
              </a>
              <div className="collapse show" id="filter_Luas_Bangunan">
                <div className="d-flex align-items-center justify-content-between">
                  <div className="">
                    <div
                      id="card_filter_input_m"
                      className="input-group"
                      style={{ height: "40px" }}
                    >
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        value={panjangBangunan}
                        placeholder="0"
                        onChange={(event) =>
                          setPanjangBangunan(event.target.value)
                        }
                      />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      width: "12px",
                      height: "2px",
                      backgroundColor: "#AAAAAA",
                      borderRadius: "21px",
                      margin: "0px 5px",
                    }}
                  ></div>
                  <div>
                    <div
                      id="card_filter_input_m"
                      className="input-group"
                      style={{ height: "40px" }}
                    >
                      <NumberFormat
                        thousandSeparator="."
                        decimalSeparator=","
                        className="form-control"
                        value={lebarBangunan}
                        placeholder="0"
                        onChange={(event) =>
                          setLebarBangunan(event.target.value)
                        }
                      />
                      <span
                        style={{
                          fontWeight: "700",
                          color: "#00193e",
                          backgroundColor: "#fafafa",
                        }}
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        m
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {errLuasBangunan && (
                <div
                  className="error-input-harga-pasar"
                  >
                    <i
                      class="bi bi-exclamation-circle-fill"
                      style={{ marginRight: "5px" }}
                      ></i>
                      angka max tidak boleh lebih kecil dari angka min
                </div>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
