import Link from "next/link";
import { useEffect, useState } from "react";

export default function DeveloperFilter({ setJenisProperti = () => { }, setUrutkan = () => { }, setReqBody = () =>{ } }) {
  const [rumah, setRumah] = useState(false);
  const [apartemen, setApartemen] = useState(false);
  const [ruko, setRuko] = useState(false);
  const [kantor, setKantor] = useState(false);
  const [sort, setSort] = useState(null);

  useEffect(() => {
    const jenisPropertiList = []
    if (rumah) {
      jenisPropertiList.push(1);
    }
    if (apartemen) {
      jenisPropertiList.push(2);
    }
    if (ruko) {
      jenisPropertiList.push(6);
    }
    if (kantor) {
      jenisPropertiList.push(7);
    }
    setJenisProperti(jenisPropertiList.join("%2C"));
    setReqBody({ Page: 1 });
  }, [rumah, apartemen, ruko, kantor]);

  return (
    <div className="sticky-top">
      <div className="card card_filter ">
        <div className="card-header filter_title" style={{ padding: "24px 16px" }}>
          <h4
            className="mb-0"
            style={{
              fontSize: "16px",
              fontFamily: "Futura",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              color: "#000000",
            }}
          >
            Filter
          </h4>
        </div>
        <div className="card-body">
          <form>
            <div className="item_form">
              <div className="filter_title developer_filter">
                <h5>Tipe Properti</h5>
              </div>
              <div className="collapse show" id="filter_tipe">
                <div className="form-check mb-2">
                  <input
                    className="form-check-input me-2"
                    type="checkbox"
                    checked={rumah}
                    onChange={(e) => setRumah(e.target.checked)}
                    id="filter_rumah"
                  />
                  <label
                    className="form-check-label"
                    htmlFor="filter_rumah"
                    style={{ fontWeight: 400 }}
                  >
                    Rumah
                  </label>
                </div>
                <div className="form-check mb-2">
                  <input
                    className="form-check-input me-2"
                    type="checkbox"
                    value=""
                    id="filter_apartemen"
                    checked={apartemen}
                    onChange={(e) => setApartemen(e.target.checked)}
                  />
                  <label className="form-check-label" htmlFor="filter_apartemen">
                    Apartemen
                  </label>
                </div>
                <div className="form-check mb-2">
                  <input
                    className="form-check-input me-2"
                    type="checkbox"
                    value=""
                    id="filter_ruko"
                    checked={ruko}
                    onChange={(e) => setRuko(e.target.checked)}
                  />
                  <label className="form-check-label" htmlFor="filter_ruko">
                    Ruko
                  </label>
                </div>
                <div className="form-check mb-2">
                  <input
                    className="form-check-input me-2"
                    type="checkbox"
                    value=""
                    id="filter_kantor"
                    checked={kantor}
                    onChange={(e) => setKantor(e.target.checked)}
                  />
                  <label className="form-check-label" htmlFor="filter_kantor">
                    Kantor
                  </label>
                </div>
              </div>
            </div>
            <div className="item_form">
              <div className="filter_title developer_filter">
                <h5 style={{ marginBottom: "14px" }}>Urutkan</h5>
              </div>
              <div className="collapse show" id="filter_urutan">
                <div className="form-check " style={{ marginBottom: "14px" }}>
                  <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Relevansi"
                    onChange={(e) =>{
                       setUrutkan(1)
                       setReqBody({ Page: 1 });
                      }}
                  />
                  <label className="form-check-label mt-0" htmlFor="urutan_Relevansi">
                    Terpopuler
                  </label>
                </div>
                <div className="form-check" style={{ marginBottom: "14px" }}>
                  <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Terbaru"
                    onChange={(e) => {
                      setUrutkan(2);
                      setReqBody({ Page: 1 });
                    }}
                  />
                  <label className="form-check-label mt-0" htmlFor="urutan_Terbaru">
                    Terbaru
                  </label>
                </div>
                <div className="form-check" style={{ marginBottom: "14px" }}>
                  <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_A-Z"
                    onChange={(e) => {
                      setUrutkan(3)
                      setReqBody({ Page: 1 });
                    }}
                  />
                  <label className="form-check-label mt-0" htmlFor="urutan_A-Z">
                    (A - Z)
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input me-2"
                    type="radio"
                    name="urutan"
                    id="urutan_Z-A"
                    onChange={(e) => {
                      setUrutkan(4)
                      setReqBody({ Page: 1 });
                    }}
                  />
                  <label className="form-check-label mt-0" htmlFor="urutan_Z-A">
                    (Z - A)
                  </label>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div
        id="card_newslater_developer"
        className="card card_newslater "
        style={{ minHeight: "183px", marginTop: 16 }}
      >
        <div
          className="card-body card-mitra"
          style={{
            fontFamily: "Futura",
            fontSize: "20px",
            fontWeight: "bold",
            fontStyle: "normal",
            lineHeight: "150%",
          }}
        >
          <h4
            className="title text-start"
            style={{
              fontFamily: "FuturaBT",
              fontSize: "20px",
              fontWeight: "700",
              fontStyle: "normal",
              lineHeight: "150%",
            }}
          >
            Ingin Menjadi Mitra Developer BTN Properti?
          </h4>
          <Link href="/developer/ajukan">
            <a
              className="btn btn-outline-white mt-38 btn_circle"
              style={{
                display: "flex",
                alignItems: "center",
                fontFamily: "Helvetica",
                fontStyle: "normal",
                fontWeight: 700,
                fontSize: 16,
                lineHeight: "160%",
                maxWidth: 163,
                height: 50,
                position: "static",
                justifyContent: "center",
                marginTop: "28px",
                padding: "0px 0px 0px",
              }}
            >
              <div>Daftar Sekarang</div>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
}
