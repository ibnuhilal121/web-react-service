import React, { createRef } from "react";
import { useState, useEffect } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { useRouter } from "next/router";
import Lottie from "react-lottie";
import * as animationData from "../../public/animate_home.json";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";

export default function Kontak({ setLoading }) {
    const router = useRouter();
    const [selectedImage, setSelectedImage] = useState();
    const [modalSuc, setModalSuc] = useState(false);
    const [input, setInput] = useState({
        name: "",
        email: "",
        phone: "",
        category: "",
        description: "",
    });
    const [captchaCode, setCaptchaCode] = useState("");
    const recaptchaRef = createRef();
    const [imageConverted, setImageConverted] = useState("");
    const [errMessage, setErrMessage] = useState({
        name: false,
        email: {
            show: false,
            message: "",
        },
        phone: false,
        category: false,
        description: false,
    });
    const [btnInvalid, setBtnInvalid] = useState(true);
    const [errImage, setErrImage] = useState(false);
    const [textErr, setTextErr] = useState("");

    const handleKeyPress = (e) => {
        if (e.key === "Enter") {
            let errTemp = { ...errMessage };
            for (let prop in input) {
                switch (prop) {
                    case "email":
                        if (!input[prop]) {
                            errTemp.email.show = true;
                            errTemp.email.message = "Isian Tidak Boleh Kosong";
                        } else {
                            const mailFormat =
                                /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                            if (!input[prop].match(mailFormat)) {
                                errTemp.email.show = true;
                                errTemp.email.message = "Isian dengan alamat email";
                            } else {
                                errTemp.email.show = false;
                                errTemp.email.message = "";
                            }
                        }
                        break;
                    default:
                        if (!input[prop]) {
                            errTemp[prop] = true;
                        } else {
                            errTemp[prop] = false;
                        }
                        break;
                }
            }
            setErrMessage(errTemp);
        }
    };

    useEffect(() => {
        let inputValid = true;
        for (let prop in errMessage) {
            if (prop === "email") {
                if (errMessage[prop].show) {
                    inputValid = false;
                }
            } else {
                if (errMessage[prop]) {
                    inputValid = false;
                }
            }
        }

        for (let prop in input) {
            if (!input[prop]) {
                inputValid = false;
            }
        }

        if (inputValid && captchaCode) {
            setBtnInvalid(false);
        } else {
            setBtnInvalid(true);
        }
    }, [errMessage, captchaCode, input]);

    // This function will be triggered when the file field change
    const imageChange = (e, maxSize) => {
        if (
            (e.target.files[0].type.includes("jpeg") ||
                e.target.files[0].type.includes("jpg") ||
                e.target.files[0].type.includes("png")) &&
            // (e.target.files[0].size / 1048576).toFixed(2) < 3
            +(e.target.files[0]?.size) <= (maxSize * 1000000)
        ) {
            // setErrImage("");
            setTextErr("");
            if (e.target.files && e.target.files.length > 0) {
                setSelectedImage(e.target.files[0]);
                var reader = new FileReader();
                reader.readAsDataURL(e.target.files[0]);

                reader.onload = function () {
                    setImageConverted(reader.result);
                };

                reader.onerror = function (error) {
                    console.log("error attach image : ", error);
                };
            }
        } else {
            setErrImage(true);
            if (!e.target.files[0]?.type.includes("jpeg") &&
            !e.target.files[0]?.type.includes("jpg") &&
            !e.target.files[0]?.type.includes("png")) {
                setTextErr("File yang dimasukan tidak sesuai format");
            } else if (+(e.target.files[0]?.size) > (maxSize * 1000000)) {
                setTextErr("Image lebih dari 3MB");
            };
        };
    };

    // This function will be triggered when the "Remove This Image" button is clicked
    const removeSelectedImage = () => {
        setSelectedImage();
    };

    const validateInput = (type, value) => {
        let errTemp = { ...errMessage };
        switch (type) {
            case "email":
                if (!value) {
                    errTemp.email.show = true;
                    errTemp.email.message = "Isian Tidak Boleh Kosong";
                } else {
                    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                    if (!value.match(mailFormat)) {
                        errTemp.email.show = true;
                        errTemp.email.message = "Isian dengan alamat email";
                    } else {
                        errTemp.email.show = false;
                        errTemp.email.message = "";
                    }
                }
                break;
            default:
                if (!value) {
                    errTemp[type] = true;
                } else {
                    errTemp[type] = false;
                }
                break;
        }
        setErrMessage(errTemp);
    };

    const changeInput = (type, value) => {
        let temp = { ...input };
        if (type === "phone") {
            temp[type] = value.replace(/[^\d]/g, "");
        } else {
            temp[type] = value;
        }
        setInput(temp);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (captchaCode) {
            setLoading(true);
            if (imageConverted) {
                const uploadPayload = {
                    FolderName: "hubungi-kami",
                    FileExtension: selectedImage.name.split(".").pop(),
                    FileBase64: imageConverted.split("base64,")[1],
                };

                fetch(`${process.env.NEXT_PUBLIC_API_HOST}/file/upload`, {
                    method: "POST",
                    headers: getHeaderWithAccessKey(),
                    body: new URLSearchParams(uploadPayload),
                })
                    .then(async (response) => {
                        const data = await response.json();
                        if (!response.ok) {
                            const error = (data && data.message) || response.status;
                            return Promise.reject(error);
                        }
                        return data;
                    })
                    .then((data) => {
                        if (!data.IsError) {
                            return data.Output;
                        } else {
                            throw `error upload file : ${data.ErrToUser}`;
                        }
                    })
                    .then((output) => {
                        const payload = {
                            n: input.name,
                            eml: input.email,
                            no_hp: input.phone,
                            i_kat: input.category,
                            psn: input.description,
                            fl: output,
                        };

                        return fetch(
                            `${process.env.NEXT_PUBLIC_API_HOST}/hubungikami/insert`,
                            {
                                method: "POST",
                                headers: getHeaderWithAccessKey(),
                                body: new URLSearchParams(payload),
                            }
                        );
                    })
                    .then(async (response) => {
                        const data = await response.json();
                        if (!response.ok) {
                            const error = (data && data.message) || response.status;
                            return Promise.reject(error);
                        }
                        return data;
                    })
                    .then((data) => {
                        if (!data.IsError) {
                            setModalSuc(true)
                        } else {
                            throw data.ErrToUser;
                        }
                    })
                    .catch((error) => {
                        // console.log(error);
                    })
                    .finally(() => {
                        setLoading(false);
                    });
            } else {
                const payload = {
                    n: input.name,
                    eml: input.email,
                    no_hp: input.phone,
                    i_kat: input.category,
                    psn: input.description,
                    fl: "",
                };

                fetch(`${process.env.NEXT_PUBLIC_API_HOST}/hubungikami/insert`, {
                    method: "POST",
                    headers: getHeaderWithAccessKey(),
                    body: new URLSearchParams(payload),
                })
                    .then(async (response) => {
                        const data = await response.json();
                        if (!response.ok) {
                            const error = (data && data.message) || response.status;
                            return Promise.reject(error);
                        }
                        return data;
                    })
                    .then((data) => {
                        if (!data.IsError) {
                            setModalSuc(true);
                            
                        } else {
                            throw data.ErrToUser;
                        }
                    })
                    .catch((error) => {
                        // console.log(error);
                    })
                    .finally(() => {
                        setLoading(false);
                    });
            }
        } else {
            console.log("Silakan verifikasi captcha terlebih dahulu");
        }

        /*
            fetch('/api/recaptcha', {
                method: 'POST',
                headers: {
                    captcha: code
                }
            })
                .then(async response => {
                    const data = await response.json()
                    return data
                })
                .then(data => {
                    if (!data.valid) {
                        throw Error
                    } else {
                        const payload = {
                            n: input.name,
                            eml: input.email,
                            no_hp: input.phone,
                            i_kat: input.category,
                            psn: input.description,
                            fl: selectedImage
                        };
    
                        return fetch(`${process.env.NEXT_PUBLIC_API_HOST}/hubungikami/insert`, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                                AccessKey_App: localStorage.getItem('accessKey')
                            },
                            body: new URLSearchParams(payload)
                        })
                    }
                })
                .then(async response => {
                    const data = await response.json();
                    if (!response.ok) {
                        const error = (data && data.message) || response.status;
                        return Promise.reject(error);
                    };
                    return data;
                })
                .then(data => {
                    if (!data.IsError) {
                        console.log(data.Output);
                        router.push('/');
                    } else {
                        throw Error;
                    }
                })
                .catch(error => {
                    console.log('Gagal Kirim Data')
                    console.log('gagal kirim data : ', error)
                })
            */
    };

    const ChangeValueRecaptcha = (isHuman) => {
        setCaptchaCode(isHuman);
    };
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice",
        },
      };
    return (
        <div>
            <form role="form" id="contact-form" method="post" onSubmit={handleSubmit}>
                <div className="floating-label-wrap ">
                    <input
                        type="text"
                        style={{ maxWidth: "480px" }}
                        // style={{ width: "480px" }}
                        className="floating-label-field contact-nama w-kontak-field w-100"
                        id="kontak_nama"
                        placeholder="Nama"
                        value={input.name}
                        onChange={(e) => changeInput("name", e.target.value)}
                        onBlur={() => validateInput("name", input.name)}
                        onKeyPress={handleKeyPress}
                        required
                    />
                    <label htmlFor="kontak_nama" className="floating-label">
                        Nama
                    </label>
                    {errMessage.name && (
                        <div className="error-input-kpr">
                            <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                            ></i>
                            Isian Tidak Boleh Kosong
                        </div>
                    )}
                </div>

                <div className="floating-label-wrap">
                    <input
                        type="email"
                        style={{ maxWidth: "480px" }}
                        // style={{ width: "480px" }}
                        className="floating-label-field contact-email w-kontak-field w-100"
                        id="kontak_email"
                        placeholder="Email"
                        value={input.email}
                        onChange={(e) => changeInput("email", e.target.value)}
                        onBlur={() => validateInput("email", input.email)}
                        onKeyPress={handleKeyPress}
                        required
                    />
                    <label htmlFor="kontak_email" className="floating-label" style={{ border: "none", overflow: "hidden" }}>
                        Email
                    </label>
                    {errMessage.email.show && (
                        <div className="error-input-kpr">
                            <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                            ></i>
                            {errMessage.email.message}
                        </div>
                    )}
                </div>
                <div className="floating-label-wrap ">
                    <input
                        type="text"
                        style={{ maxWidth: "480px" }}
                       
                        className="floating-label-field contact-nohp w-kontak-field w-100"
                        id="kontak_no_phone"
                        placeholder="No Handphone"
                        value={input.phone}
                        onChange={(e) => changeInput("phone", e.target.value)}
                        onBlur={() => validateInput("phone", input.phone)}
                        onKeyPress={handleKeyPress}
                        required
                    />
                    <label htmlFor="kontak_no_phone" className="floating-label" style={{ border: "none", overflow: "hidden" }}>
                        No Handphone
                    </label>
                    {errMessage.phone && (
                        <div className="error-input-kpr">
                            <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                            ></i>
                            Isian Tidak Boleh Kosong
                        </div>
                    )}
                </div>

                <div className="floating-label-wrap">
                    <select
                        name="kontak[kategori]"
                        style={{ maxWidth: "480px", color: "#00193e", paddingTop: 0, paddingBottom: 0 }}
                        id="kategori"
                        className="floating-label-select form-select custom-select contact-pertanyaan w-kontak-field w-100"
                        value={input.category}
                        onChange={(e) => changeInput("category", e.target.value)}
                        onBlur={() => validateInput("category", input.category)}
                        onKeyPress={handleKeyPress}
                        required
                    >
                        <option value="">Kategori Pertanyaan</option>
                        <option value="1">Pengajuan Kredit</option>
                        <option value="2">Produk Kredit</option>
                        <option value="3">Masukan/Usulan</option>
                        <option value="4">Kendala Teknis</option>
                        <option value="5">Pasang Banner Iklan</option>
                        <option value="6">Pengembang</option>
                        <option value="7">Lainnya</option>
                    </select>
                    <label htmlFor="kategori" className="floating-label">
                        Kategori Pertanyaan
                    </label>
                    {errMessage.category && (
                        <div className="error-input-kpr">
                            <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                            ></i>
                            Isian Tidak Boleh Kosong
                        </div>
                    )}
                </div>
                <div className="floating-label-wrap ">
                    <textarea
                        name="kontak[message]"
                        style={{ height: "208px", maxWidth: "480px" }}
                        // style={{ height: "208px" }}
                        // className="floating-label-field contact-deskripsi w-kontak-field w-100"
                        className="floating-label-field floating-label-textarea contact-deskripsi w-100"
                        id="message"
                        rows="6"
                        placeholder="Deskripsi"
                        value={input.description}
                        onChange={(e) => changeInput("description", e.target.value)}
                        onBlur={() => validateInput("description", input.description)}
                    />

                    <label htmlFor="message" className="floating-label">
                        Deskripsi
                    </label>
                    {errMessage.description && (
                        <div className="error-input-kpr">
                            <i
                                class="bi bi-exclamation-circle-fill"
                                style={{ marginRight: "5px" }}
                            ></i>
                            Isian Tidak Boleh Kosong
                        </div>
                    )}
                </div>
                <div className="form-group mb-3 unggah-dokumen">
                    <label className="form-control-label mb-3 upload-doc">
                        Unggah dokumen
                    </label>
                    <div className="d-flex">
                        {selectedImage && (
                            <div
                                className="me-3"
                                style={{
                                    width: "100px",
                                    height: "100px",
                                    marginBottom: "15px",
                                }}
                            >
                                <div
                                    className={"ratio ratio-1x1 photo_input"}
                                    style={{ height: "100px", width: "100px" }}
                                >
                                    <img
                                        src={imageConverted}
                                        // className="img-fluid me-3" width="100px" height="100px"
                                        style={{
                                            height: "100%",
                                            width: "100%",
                                            border: "1px solid #aaaaaa",
                                            borderRadius: "8px",
                                        }}
                                        alt="Thumb"
                                    />
                                </div>
                                {/* <button onClick={removeSelectedImage}>
                                    Remove
                                </button> */}
                            </div>
                        )}
                        {!selectedImage && (
                            <img
                                src="/images/icons/thumb.png"
                                className="img-fluid me-3 contact-img"
                                width="100px"
                                height="100px"
                            />
                        )}
                        <div className="desc-contact">
                            <p
                                className="mb-2"
                                style={{
                                    fontFamily: "Helvetica",
                                    fontWeight: "400",
                                    fontStyle: "normal",
                                    color: "#000000",
                                }}
                            >
                                Unggah file dalam format JPG, JPEG, atau PNG dengan ukuran
                                maksimal 3MB.
                            </p>
                            {errImage && (
                                <div
                                    className="error-input"
                                    style={{ marginBottom: "10px", marginTop: "-10px" }}
                                >
                                    <i
                                    class="bi bi-exclamation-circle-fill"
                                    style={{ marginRight: "5px" }}
                                    ></i>
                                    {textErr}
                                </div>
                            )}
                            <label
                                style={{
                                    maxWidth: "158px",
                                }}
                                htmlFor="upload"
                                className="mpilih-file btn btn-main text-white px-2 px-lg-4 w-100"
                            >
                                Pilih File
                            </label>
                        </div>
                    </div>
                    <input
                        type="file"
                        className="form-control"
                        accept="image/jpeg,image/jpg,image/png,application/pdf"
                        required=""
                        id="upload"
                        onChange={(e) => imageChange(e, 3)}
                        hidden
                    />
                </div>

                {/* <div className="form-group">
                    <img src="/images/icons/captcha.png" className="img-fluid" />
                </div> */}
                {/* <ReCAPTCHA 
                    size="normal" 
                    sitekey='6LehLz0dAAAAAGFdtw0RYECr3MOIecU5nDDwF0Xu' 
                    // ref={recaptchaRef}
                    // onChange={changeRecaptcha}
                /> */}
                <div className="form-group contact-captcha">
                    <ReCAPTCHA
                        ref={recaptchaRef}
                        size="normal"
                        sitekey="6Lel4Z4UAAAAAOa8LO1Q9mqKRUiMYl_00o5mXJrR"
                        onChange={ChangeValueRecaptcha}
                    />
                    {/* <ReCAPTCHA size="normal" sitekey="6LehLz0dAAAAAGFdtw0RYECr3MOIecU5nDDwF0Xu" /> */}
                </div>
                <div className="submit w-kontak-field">
                    <button
                        style={{
                            height: "52px",
                            fontFamily: "Helvetica",
                            fontWeight: "700",
                            fontSize: "16px",
                            // maxWidth: "480px",
                            // width: "480px",
                        }}
                        type="submit"
                        className="btn btn-main w-100 pull-right button-loading contact-button w-kontak-field"
                        data-style="zoom-in"
                        disabled={btnInvalid}
                    >
                        <span className="ladda-label">Kirim</span>
                        <span className="ladda-spinner"></span>
                    </button>
                </div>
            </form>
            <div className={`modal fade d-${modalSuc ? "block show" : "none"}`} id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
style={{zIndex:"9999",background:"#00193e69"}}
>
  <div className="modal-dialog modal-dialog-centered px-3 mx-auto">
    <div className="modal-content" style={{padding:"20px 20px",borderRadius:"8px"}}>
      <div className="modal-body">
        <div className="row justify-content-end">
          <button type="button" className="btn-close" aria-label="Close"
        onClick={()=>{
            window.location.reload();
            // setModalSuc(false);
            // setInput({
            //     name: "",
            //     email: "",
            //     phone: "",
            //     category: "",
            //     description: "",
            // })
            // // setSelectedImage()
            // removeSelectedImage()
            // setCaptchaCode("")
        }}
           ></button>
        </div>
        <h2 className="text-center">Email Terkirim</h2>
        <Lottie
                options={defaultOptions}
                height={280}
                width={"100%"}
                isStopped={false}
                isPaused={false}
                max-width={350}
              />
        <div className="row g-0">
        <div className="row g-0">
         <p style={{
           fontFamily:"Helvetica",
           fontSize:"14px"
         }}>
        
         </p>
        </div>
          <button type="button" className="btn btn-primary" data-bs-dismiss="modal"
          style={{
            background:"#0061a7",
            borderRadius:"200px",
            display:"flex",
            justifyContent:"center",
            alignItems:"center",
            height:"40px"
          }}
         onClick={()=>{setModalSuc(false);router.push("/");}}
        >Kembali Ke Beranda</button>
         
        </div>
      </div>
    </div>
  </div>
</div>
            
        </div>
    );
}
