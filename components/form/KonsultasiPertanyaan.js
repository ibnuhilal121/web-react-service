import React, { useEffect, useRef } from "react";
import Router, { useRouter } from "next/router";
import { useState } from "react";
import CheckIcon from "../../components/element/icons/CheckIcon";
import { useAppContext } from "../../context";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";
import { appLogin } from "../../services/master";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function KonsultasiPertanyaan({ setLoadingKirim }) {
  const { userKey } = useAppContext();
  const [selectedImage, setSelectedImage] = useState();
  const [checked, setChecked] = useState(false);
  const [validated, setValidated] = useState(false);

  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });

  const [kategori, setKategori] = useState("");
  const [pertanyaan, setPertanyaan] = useState("");
  const kategoriInput = useRef(null);
  const pertanyaanInput = useRef(null);
  const btnModal = useRef(null);
  const [input, setInput] = useState({
    category: "",
    question: "",
  });
  const [imageConverted, setImageConverted] = useState("");
  const [outputFile, setOutputFile] = useState("");
  const router = useRouter();
  const { org } = router.query;
  const [errAttachment, setErrAttachment] = useState("");

  useEffect(() => {
    if (input.category && input.question) {
      if (!checked) {
        if (userKey) {
          setValidated(true);
        } else {
          setValidated(false);
        }
      } else {
        setValidated(true);
      }
    } else {
      setValidated(false);
    }
  }, [input, checked]);

  useEffect(() => {
    pertanyaanInput.current.click();
    kategoriInput.current?.click();
    if (org === "faq") {
      setTimeout(() => {
        btnModal.current.click();
      }, 500);
    }
  }, []);

  useEffect(() => {
    if (outputFile) {
      if (checked) {
        submitAnonymous(outputFile);
      } else {
        handleSubmit(outputFile);
      }
    }
  }, [outputFile]);

  // This function will be triggered when the file field change
  const imageChange = (e) => {
    if ((e.target.files[0].type.includes("jpeg") || e.target.files[0].type.includes("jpg") || e.target.files[0].type.includes("png")) && (e.target.files[0].size / 1048576).toFixed(2) < 3) {
      setErrAttachment("");
      if (e.target.files && e.target.files.length > 0) {
        setSelectedImage(e.target.files[0]);
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = function () {
          setImageConverted(reader.result);
        };

        reader.onerror = function (error) {
          console.log("error attach image : ", error);
        };
      }
    } else {
      setErrAttachment("File tidak sesuai format");
    }
  };

  // This function will be triggered when the "Remove This Image" button is clicked
  const removeSelectedImage = () => {
    setSelectedImage();
  };

  function handleCheck(e) {
    setChecked(e.target.checked);
  }
  const changeInput = (type, value) => {
    let temp = { ...input };
    temp[type] = value;
    setInput(temp);
  };

  const kirimKonsultasi = (e) => {
    e.preventDefault();
    if (selectedImage) {
      uploadDocument();
    } else {
      if (checked) {
        submitAnonymous();
      } else {
        handleSubmit();
      }
    }
  };

  const uploadDocument = async () => {
    try {
      const keyResponse = await appLogin();
      if (!keyResponse.IsError) {
        localStorage.setItem("accessKey", keyResponse.AccessKey);
      } else {
        throw keyResponse;
      }

      const uploadPayload = {
        FolderName: "konsultasi",
        FileExtension: selectedImage.name.split(".").pop(),
        FileBase64: imageConverted.split("base64,")[1],
      };

      const processUpload = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/file/upload`, {
        method: "POST",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
          AccessKey_App: keyResponse.AccessKey,
          ...defaultHeaders
        },
        body: new URLSearchParams(uploadPayload),
      });

      const responseUpload = await processUpload.json();
      if (!responseUpload.IsError) {
        setOutputFile(responseUpload.Output);
      } else {
        throw `error upload file : ${responseUpload.ErrToUser}`;
      }
    } catch (error) {
      console.log(error);
    }
  };

  const submitAnonymous = (output) => {
    const postPayload = {
      kategori_id: input.category,
      pertanyaan: input.question,
      lampiran: output,
    };

    fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/konsultasi/set`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        app_key: process.env.NEXT_PUBLIC_APP_KEY,
        secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
        ...defaultHeaders
      },
      body: new URLSearchParams(postPayload),
    })
      .then(async (response) => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        return data;
      })
      .then((data) => {
        if (!data.IsError) {
          window.location.href = `${window.location.origin}/tools/konsultasi`;
        } else {
          throw `error submit konsultasi : ${data.ErrToUser}`;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSubmit = (output) => {
    const postPayload = {
      i_kat: input.category,
      prtnyn: input.question,
      fl: output,
    };

    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/konsultasi/insert`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
      body: new URLSearchParams(postPayload),
    })
      .then(async (response) => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        return data;
      })
      .then((data) => {
        if (!data.IsError) {
          window.location.href = `${window.location.origin}/tools/konsultasi`;
        } else {
          throw `error submit konsultasi : ${data.ErrToUser}`;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10">
            <div className="card konsultasi-question ">
              <div className="card-body question-konsultasi">
                <div className="row d-md-flex align-items-center">
                  <div className="col-12 col-md-8">
                    <div className="d-flex align-items-center">
                      <div className="icon">
                        <img src="/images/icons/chat_left.png" className="img-fluid" />
                      </div>
                      <h4 className="card-title mt-3 title-pertanyaan" style={{ fontFamily: "Helvetica", fontWeight: "700", textTransform: "none" }}>
                        Kamu memiliki pertanyaan? Tanyakan ke ahlinya sekarang!
                      </h4>
                    </div>
                  </div>
                  <div className="col-12 col-md-4 text-end button-konsultasi">
                    <button
                      id="btnModalKonsultasi"
                      ref={btnModal}
                      type="button"
                      className="btn btn-main btn-custom float-md-right"
                      data-bs-toggle="modal"
                      data-bs-target={userKey ? "#modalKonsultasi" : "#modalLogin"}
                      style={{
                        width: "164px",
                        height: "48px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                      }}
                    >
                      Kirim Pertanyaan
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="modal" id="modalKonsultasi" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document" style={isTabletOrMobile ? { margin: 0, marginTop: 68 } : {}}>
          <div className="modal-content" style={{
            width: isTabletOrMobile ? "100%" : "600px",
            height: isTabletOrMobile ? "auto" : "710px",
            border: isTabletOrMobile ? "0" : "1px solid rgba(0,0,0,.2)",
            borderRadius: isTabletOrMobile ? "0" : ".3rem"
          }}>
            <div className="modal-header" style={isTabletOrMobile ? {paddingLeft: 16, paddingRight: 16} : {}}>
              <h5 className="modal-title">Kirim Pertanyaan</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" style={{ color: " #0061A7" }}></button>
            </div>
            <div className="modal-body" style={{ paddingLeft: isTabletOrMobile ? 16 : "60px", paddingRight: isTabletOrMobile ? 16 : "60px" }}>
              <div className="modal-form">
                {/* <form className="form-validation" id="form-konsultasi"  noValidate>
                                    <div className="floating-label-wrap mt-3">
                                        <select name="konsultasi[kategori]" className="floating-label-select custom-select form-select" id="kategori" required ref={kategoriInput}
                    onChange={(e) => setKategori(e.target.value)}> */}
                <form className="form-validation" id="form-konsultasi" onSubmit={kirimKonsultasi} noValidate="novalidate">
                  {/* <form className="form-validation" id="form-konsultasi" noValidate> */}
                  <div className="floating-label-wrap mt-3">
                    <select
                      name="konsultasi[kategori]"
                      className="floating-label-select custom-select form-select"
                      id="kategori"
                      required
                      ref={kategoriInput}
                      onChange={(e) => changeInput("category", e.target.value)}
                      style={{
                        height: 48,
                        width: isTabletOrMobile ? "100%" : "480px",
                        padding: "0px 16px",
                        fontWeight: "700",
                      }}
                    >
                      <option value="">Kategori</option>
                      <option value="3">KPR</option>
                      <option value="1">Konsultasi Desain</option>
                      <option value="4">Kredit Komersial</option>
                      <option value="5">Lainnya</option>
                      <option value="2">Perencanaan Keuangan</option>
                    </select>
                    <label htmlFor="kategori" className="floating-label">
                      Kategori
                    </label>
                  </div>
                  <div className="floating-label-wrap mb-3">
                    <textarea
                      name="konsultasi[pertanyaan]"
                      rows="4"
                      className="floating-label-field floating-label-textarea"
                      placeholder="Pertanyaan"
                      id="konsultasi_pertanyaan"
                      ref={pertanyaanInput}
                      onChange={(e) => changeInput("question", e.target.value)}
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "400",
                        fontSize: "14px",
                        color: "#00193e",
                        height: 138,
                        width: isTabletOrMobile ? "100%" : "480px",
                      }}
                      required
                    >
                      {input.question}
                    </textarea>
                    {/* onChange={(e) => setPertanyaan(e.target.value)} style={{ fontFamily: "FuturaTb" }}>{pertanyaan}</textarea> */}
                    <label htmlFor="konsultasi_pertanyaan" className="floating-label" style={{ fontFamily: "FuturaBT", fontWeight: "700" }}>
                      Pertanyaan
                    </label>
                  </div>

                  {/* 
                        <div className="form-group">
                            <label className="form-control-label mb-3 upload-label">Unggah dokumen</label>
                            <div className="d-flex">
                                {selectedImage && (
                                    <div>
                                        <img
                                        // src={selectedImage}
                                        src={URL.createObjectURL(selectedImage)}
                                        className="img-fluid me-3" width="100px"
                                        alt="Thumb"
                                        />
                                        <button onClick={removeSelectedImage} >
                                            Remove Image
                                        </button> 
                                        <textarea name="konsultasi[pertanyaan]" rows="4" className="floating-label-field" placeholder="Pertanyaan" id="konsultasi_pertanyaan" ref={pertanyaanInput}
                    onChange={(e) => setPertanyaan(e.target.value)} style={{fontFamily: pertanyaan ? "Helvetica" : "FuturaTb", height: 138}}>{pertanyaan}</textarea>
                                        <label htmlFor="konsultasi_pertanyaan" className="floating-label">Pertanyaan</label>
                                    </div> 
                                )}
                                {!selectedImage && (
                                    <img src="/images/icons/thumb.png" className="img-fluid me-3" width="100px" height="100px" />
                                )}
                                <div className="">
                                    <p className="upload-desc">Unggah file dalam format JPG, JPEG, atau PNG dengan ukuran maksimal 3MB.</p>
                                    <label htmlFor="upload" className="btn btn-main text-white px-5 upload-btn">Pilih File</label>
                                </div>
                            </div>
                            <div className="form-check mt-3 d-flex align-items-center ps-0">
                                <input className="form-check-input checkbox" type="checkbox" value="" id="flexCheckDefault" onChange={handleCheck} checked={checked} />
                                <label className="checkmark" htmlFor="flexCheckDefault" style={{ borderColor: checked ? "#0061A7" : null }} >{checked && <CheckIcon />}</label>
                                <label className="form-check-label check-label" htmlFor="flexCheckDefault">
                                    Kirim Sebagai Anonymous
                                </label>
                            </div>
                            <input 
                                type="file" 
                                className="form-control" 
                                accept="image/jpeg,image/jpg,image/png,application/pdf" 
                                required="" 
                                id="upload" 
                                onChange={imageChange} 
                                hidden 
                            />

                        </div>

                        <button type="submit" className={`login-btn btn btn-main btn-submit btn-block w-100 ${validated ? 'validated' : 'invalidated'}`} disabled={!validated}>
                            <span className="ladda-label" style={{ fontWeight: "bold" }}>Kirim</span><span className="ladda-spinner"></span>
                        </button>

                    </form>
                </div> */}

                  <div className="form-group">
                    <label className="form-control-label mb-3 upload-label">Unggah dokumen</label>
                    <div className="d-flex">
                      {selectedImage && (
                        <div style={{width:"100px", height:"100px"}}>
                          <img
                            // src={selectedImage}
                            src={URL.createObjectURL(selectedImage)}
                            className="img-fluid"
                            alt="Thumb"
                            style={{
                              width: "100px",
                              height: "100px",
                              maxWidth:"100px",
                              objectFit:"cover"
                            }}
                          />
                        </div>
                      )}
                      {!selectedImage && (
                        <img
                          src="/images/icons/thumb.png"
                          className="img-fluid"
                          style={{
                            width: "100px",
                            height: "100px",
                          }}
                        />
                      )}
                      <div className="ps-2 ps-sm-3">
                        <p className="upload-desc">Unggah file dalam format JPG, JPEG, atau PNG dengan ukuran maksimal 3MB.</p>
                        {errAttachment && (
                          <div className="error-input" style={{ marginTop: "-1rem", marginBottom: "5px" }}>
                            <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }}></i>
                            {errAttachment}
                          </div>
                        )}
                        <label
                          htmlFor="upload"
                          className="btn btn-main text-white px-5 upload-btn"
                          style={{
                            width: "158px",
                            height: "48px",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          Pilih File
                        </label>
                      </div>
                    </div>
                    <div className="form-check mt-3 d-flex align-items-center ps-0">
                      <input className="form-check-input checkbox" type="checkbox" value="" id="flexCheckDefault" onChange={handleCheck} checked={checked} />
                      <label className="checkmark" htmlFor="flexCheckDefault" style={{ borderColor: checked ? "#0061A7" : null }}>
                        {checked && <CheckIcon />}
                      </label>
                      <label className="form-check-label check-label" htmlFor="flexCheckDefault">
                        Kirim Sebagai Anonymous
                      </label>
                    </div>
                    <input type="file" className="form-control" accept="image/jpeg,image/jpg,image/png,application/pdf" required="" id="upload" onChange={imageChange} hidden />
                  </div>

                  <button
                    type="submit"
                    className={`login-btn btn btn-main btn-submit btn-block w-100 ${validated ? "validated" : "invalidated"}`}
                    disabled={!validated}
                    style={{
                      width: "480px",
                      height: "48px",
                    }}
                  >
                    <span className="ladda-label" style={{ fontWeight: "bold" }}>
                      Kirim
                    </span>
                    <span className="ladda-spinner"></span>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
