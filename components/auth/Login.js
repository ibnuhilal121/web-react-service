import React, { useCallback, useEffect, useRef } from "react";
import Router, { useRouter } from "next/router";
import { useState } from "react";
import { baseUrl } from "../../utils/baseUrl";
import md5 from "md5";
import {
  options,
  signIn,
  signOut,
  useSession,
  providers,
} from "next-auth/client";
import { useAppContext } from "../../context";
import { getPageFiles } from "next/dist/server/get-page-files";
import { sha256 } from "js-sha256";
import Cookies from "js-cookie";
import { isInvalid } from "../../utils/validation.register";
import { doLogin,getEkycData } from "../../services/auth";
import { useMediaQuery } from "react-responsive";
import { googleLogin, twitterLogin, facebookLogin } from "../../helpers/auth";
import { auth } from "../../utils/FirebaseSetup";
import { onAuthStateChanged } from "firebase/auth";
import { defaultHeaders } from "../../utils/defaultHeaders";
import { getGenerateLinkPreApproval, getGenerateHomeServiceToken, getGenerateProfesionaListingToken, getGenerateHomeServiceLink, getGenerateProfesionaListingLink} from "../../services/konten";

export default function Login({}) {
  const [loading, setLoading] = useState(true);
  const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
  const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
  const isMobile = useMediaQuery({ query: `(max-width: 768px)` });
  const [validated, setValidated] = useState(false);
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [isEmailErr, setEmailErr] = useState(false);
  const [isPasswordErr, setPasswordErr] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [passwordType1, setPasswordType1] = useState("password");
  const { setUserKey, setUserProfile, userProfile } = useAppContext();
  const emailInput = useRef(null);
  const passInput = useRef(null);
  const [remember, setRemember] = useState(false);
  const buttonRef = useRef();

  // const [errorLogin, setErrorLogin] = useState(false);

  // useEffect(() => {
  //   onAuthStateChanged(auth, (user) => {
  //     if (user) {
  //       // setUser({
  //       //   name: user.displayName,
  //       //   email: user.email || user.providerData[0].email,
  //       //   image: user.photoURL,
  //       //   id: user.uid
  //       // })
  //       console.log('~ data user : ', user)
  //       // setLoggedIn(true)
  //     } else {
  //       // setUser(null)
  //       // setLoggedIn(false)
  //     }
  //   })
  // }, [])
  

  const submitLogin = (e) => {
    
    e.preventDefault();
    buttonRef.current.disabled = true;
    
    if (!isEmailErr && !isPasswordErr) {
      setEmailErrorMessage("")
      setPasswordErrorMessage("")
      doLogin(email, password)
        .then( async (data) => {
          if (data.IsError) {
            throw data.ErrToUser;
          } else {
            const isVerified = await getEkycData(data?.Profile[0]?.id).then((result)=>{
              return result.data?.isVerified
              })

            let data_profile = data?.Profile[0]
            const tempData = {
              ...userProfile,
              mobilePhoneNumber: data_profile?.no_hp,
              provinceId: data_profile?.i_prop,
              districtId: data_profile?.i_kot,
              subDistrictId: data_profile?.i_kec,
              address: data_profile?.almt,
              postalCode: data_profile?.pos,
              isVerified
            }

            const hide = document.getElementById("modal-log-in");
            hide.style.display = "none";

            const hideiconload = document.getElementById("pop-loading");
            hideiconload.style.display = "flex";
            // const hidebtnicon = document.getElementById("poup-loading");
            // if (loading) {
            //   return (
               
            //   );
            // }
            // console.log("tes hide modal:" + hide);
            // const tempProfile = {
            //   n: data.Profile[0].n,
            //   gmbr: data.Profile[0].gmbr,
            //   id: data.Profile[0].id,
            //   agen: data.Profile[0].is_agn,
            //   idAgen: data.Profile[0].i_agn,
            //   namaAgensi: data.Profile[0].n_agn,
            //   email:data.Profile[0].e
            // }
            //body untuk dapat generate url
            const preApproval = {
              NAMA: data.Profile[0].n,
              EMAIL: data.Profile[0].e
            }
            //body untuk dapat token home services
            const HomeServicesToken = {
              grant_type: "client_credentials",
              client_id: "BTNProperti",
              client_secret: "8d14a409384eed23adbe474d46fcd04a"
            }

            const ProfesionaListingToken = {
              grant_type: "client_credentials",
              client_id: "BTNProperti",
              client_secret: "8d14a409384eed23adbe474d46fcd04a"
            }

            const HomeServicesLink = {
              "platform_id": "53239e80-54d5-11ed-89b5-506b8d8a08aa",
              "webview": "HOME_SERVICE",
              "user": {
                "name": data.Profile[0].n,
                "msisdn": data.Profile[0].no_hp,
                "email": data.Profile[0].e
              }
            }

            const ProfesionaListingLink = {
              "platform_id": "53239e80-54d5-11ed-89b5-506b8d8a08aa",
              "webview": "PROFESIONALISTING",
              "user": {
                "name": data.Profile[0].n,
                "msisdn": data.Profile[0].no_hp,
                "email": data.Profile[0].e
              }
            }

            // jangan dihapus bang..
            getEkycData(data.Profile[0].id).then((verif)=>{
              const tempProfile = {
                n: data.Profile[0].n,
                gmbr: data.Profile[0].gmbr,
                id: data.Profile[0].id,
                agen: data.Profile[0].is_agn,
                idAgen: data.Profile[0].i_agn,
                namaAgensi: data.Profile[0].n_agn,
                isVerified: verif.data?.isVerified,
                currentStep: verif.data?.currentStep,
                counterFailedVerification:verif.data?.counterFailedVerification,
                email:data.Profile[0].e
              }
            
                Cookies.set("user", JSON.stringify({...tempProfile, ...tempData}), {
                  secure: false,
                  expires: 7
                });
                Cookies.set("keyMember", JSON.stringify(data.AccessKey), {
                  secure: false,
                  expires: 7
                });
                sessionStorage.setItem("user", JSON.stringify({...tempProfile, ...tempData}));
                sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
              setUserProfile({...data.Profile[0], ...tempData});
            setUserKey(data.AccessKey);
            localStorage.removeItem("err_log");
            
            }).catch((error)=>{
              const tempProfile = {
                n: data.Profile[0].n,
                gmbr: data.Profile[0].gmbr,
                id: data.Profile[0].id,
                agen: data.Profile[0].is_agn,
                idAgen: data.Profile[0].i_agn,
                namaAgensi: data.Profile[0].n_agn,
                isVerified: false,
                currentStep: 0,
                counterFailedVerification:0,
                email:data.Profile[0].e
              }
              // if (remember) {
              //   Cookies.set("user", JSON.stringify(tempProfile));
              //   Cookies.set("keyMember", JSON.stringify(data.AccessKey));
              // } else {
              //   sessionStorage.setItem("user", JSON.stringify(tempProfile));
              //   sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
              // }
              Cookies.set("user", JSON.stringify({...tempProfile, ...tempData}));
              Cookies.set("keyMember", JSON.stringify(data.AccessKey));
              setUserProfile(data.Profile[0]);
            setUserKey(data.AccessKey);
            localStorage.removeItem("err_log");
            
            })
              // Cookies.set("user", JSON.stringify(tempProfile), {
              //   secure: false,
              //   expires: 7
              // });
              // Cookies.set("keyMember", JSON.stringify(data.AccessKey), {
              //   secure: false,
              //   expires: 7
              // });
              // sessionStorage.setItem("user", JSON.stringify(tempProfile));
              // sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
            
            // setUserProfile(data.Profile[0]);
            //Membuat body menjadi objek
            let bodypreapproval = JSON.stringify(preApproval);
            localStorage.setItem("body-preapproval",bodypreapproval);

            let bodyHomeSerciceToken = JSON.stringify(HomeServicesToken);
            localStorage.setItem("body-homeservice-token",bodyHomeSerciceToken);

            let bodyProfesionalListingToken = JSON.stringify(ProfesionaListingToken);
            localStorage.setItem("body-profesionallisting-token",bodyProfesionalListingToken);

            let bodyHomeSerciceLink = JSON.stringify(HomeServicesLink);
            localStorage.setItem("body-homeservice-link",bodyHomeSerciceLink);

            let bodyProfesionalListingLink = JSON.stringify(ProfesionaListingLink);
            localStorage.setItem("body-profesionallisting-link",bodyProfesionalListingLink);

            let approval = await getGenerateLinkPreApproval(preApproval);
            let home_token = await getGenerateHomeServiceToken(HomeServicesToken);
            let profesiona_listing_token = await getGenerateProfesionaListingToken(ProfesionaListingToken);
            localStorage.setItem("generate-token",home_token.data.access_token);
            localStorage.setItem("generate-token-profesiona-listing",profesiona_listing_token.data.access_token);
            let home_service_link = await getGenerateHomeServiceLink(JSON.stringify(HomeServicesLink));
            let profesiona_listing_link = await getGenerateProfesionaListingLink(JSON.stringify(ProfesionaListingLink));


            //console menampilkan generate url
            console.log(approval.url)

            //console menampilkan token home services
            console.log(home_token.data.access_token)

             //console menampilkan token home services
             console.log(home_service_link)
             console.log(home_service_link.data.link)

            localStorage.setItem("generate-url",approval.url);
            localStorage.setItem("url-home-services",home_service_link.data.link);
            localStorage.setItem("url-profesiona-listing",profesiona_listing_link.data.link);
            // console.log(homeServiceLink)
            console.log(getGenerateHomeServiceToken)
            // console.log(getGenerateLinkPreApproval(preApproval))
            console.log("sini 7")
          setUserKey(data.AccessKey);
          localStorage.removeItem("err_log");
          const redirect = sessionStorage.getItem("redirect_url");
          const urlFromEmail = localStorage.getItem("urlFromEmail");
          const navigate = localStorage.getItem("navigate");
          const title = localStorage.getItem("title");
          localStorage.removeItem("navigate")
          localStorage.removeItem("title")
          // document.getElementsByClassName("login-btn")?.setAttribute("data-bs-dismiss", "modal")
          if (urlFromEmail) {
            window.location.replace(urlFromEmail);
            localStorage.removeItem("urlFromEmail");
            sessionStorage.removeItem("redirect_url");
          } else if (redirect) {
            window.location.replace(redirect);
            sessionStorage.removeItem("redirect_url");
          } else {
            if (router.pathname.includes('tools/komparasi') && router.query?.data) {
              window.location.reload();
            } else if(router.pathname.includes('tools/konsultasi')){
              window.location.reload();
            } else if(navigate){
              if (title == "Virtual Expo") {
                window.location.assign(navigate + approval.url)
                localStorage.removeItem("navigate")
                console.log(navigate + approval.url);
              } else if(title == "Pre Approval"){
                window.location.assign(navigate + "/link/" + approval.url)
                localStorage.removeItem("navigate")
                console.log(navigate + approval.url);
              } else if(title == "Home Service"){
                window.location.assign(localStorage.getItem("url-home-services"))
                localStorage.removeItem("navigate")
                console.log(home_service_link.data.link);
              } else if(title == "Jasa Profesional"){
                window.location.assign(localStorage.getItem("url-profesiona-listing"))
                localStorage.removeItem("navigate")
                console.log(profesiona_listing_link.data.link);
              } else {
                window.location.assign(navigate)
              }
            } else {
              window.location.assign("/member/profil");
            };
          };
            
            
          }
          if(sessionStorage.getItem("user").length) {
            setUserProfile(JSON.parse(sessionStorage.getItem("user")))
          }
          if(Cookies.get("user").length) {
            setUserProfile(JSON.parse(Cookies.get("user")))
          }
        })
        .catch((error) => {
          setEmailErrorMessage(error);
          setEmailErr(true);
  
          const storage = localStorage.getItem("err_log");
          if (storage) {
            let temp = Number(storage);
            if (temp + 1 === 3) {
              submitError();
            } else {
              localStorage.setItem("err_log", temp + 1);
            }
          } else {
            localStorage.setItem("err_log", 1);
          }
        });
    }
  };

  const submitError = () => {
    const payload = {
      Email: email,
    };

    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login/attempt`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        AccessKey_App: localStorage.getItem("accessKey"),
        // defaultHeaders
      },
      body: new URLSearchParams(payload),
    })
      .then(async (response) => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        return data;
      })
      .then((data) => {
        if (data.Output !== "") {
          setPasswordErr(true);
          setEmailErr(false);
          setPasswordErrorMessage(data.Output);
        } else if (data.ErrToUser !== "") {
          setPasswordErr(false);
          setEmailErr(true);
          setEmailErrorMessage(data.ErrToUser);
        }
      })
      .catch((error) => {
        console.log("error get attempt " + error);
      })
      .finally(() => {
        localStorage.removeItem("err_log");
        localStorage.removeItem("navigate");
          localStorage.removeItem("title");
          localStorage.removeItem("generate-url");
      });
  };

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    event.preventDefault();
    router.push("");
  };

  const handleChangeEmail = (e) => {
    setEmail(e.target.value.replace(/[^a-zA-Z0-9 _@.~-]+/,''));
    setEmailErr(false)
    setPasswordErr(false)
  };
  const handleChangePassword = (e) => {
    setPassword(handleBlurPassword(e.target.value));
    setEmailErr(false)
    setPasswordErr(false)
  };

  const handleBlurEmail = (value) => {
    // const value = e.target.value;
    const RFC5322 =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // checking empty input
    if (value.trim() === "") {
      setEmailErrorMessage("Isian tidak boleh kosong");
      return setEmailErr(true);
    }
    // checking email format
    if (value.trim() && !RFC5322.test(value.trim())) {
      setEmailErrorMessage("Isi dengan alamat email");
      return setEmailErr(true);
    }

    setEmailErr(false);
    setEmailErrorMessage("");
  };

  const handleBlurPassword = (value) => {
    // ====== Just for testing ======
    if (value) {
      if (value.trim() === '') {
        setPasswordErrorMessage('Isian tidak boleh kosong')
        return setPasswordErr(true)
      } 
      // else if (isInvalid('password', value)) {
      //   setPasswordErrorMessage('Password minimal 8 karakter, terdiri dari huruf besar, huruf kecil, angka dan karakter khusus (.@$!%*?&)')
      //   return setPasswordErr(true)
      // }
      }else{
      setPasswordErrorMessage('Isian tidak boleh kosong')
      return setPasswordErr(true)
    }
    setPasswordErr(false);
    setPasswordErrorMessage("");

    return value
  };

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      handleBlurEmail(email);
      // handleBlurPassword(password);
    }
  };

  function redirectProfil(e) {
    e.preventDefault();
    router.push("/member/profil");
  }

  useEffect(() => {
    //   if (isEmailErr || password.trim() === '') {
    //     console.log('isValidated: ', false)
    //     return setValidated(false)
    //   }
    //   console.log('isValidated: ', true)
    //   setValidated(true)
    // }, [isEmailErr, password]);
    if (password && email) {
      setValidated(true);
    } else {
      setValidated(false);
    }
    // if (password && email && !isInvalid('password', password)) {
    //   setValidated(true);
    // } else {
    //   setValidated(false);
    // };
  }, [password, email]);

  useEffect(() => {
    setValidated(false);
  }, []);

  const loginAuth = (id) => {
    signIn(id);
  };

  const changeRemember = () => {
    if (remember) {
      setRemember(false);
    } else {
      setRemember(true);
    }
  };

  const resetValidation = useCallback(() => {
    setEmail("");
    setPassword("");
    setEmailErr(false);
    setPasswordErr(false);
  }, [email, password, isEmailErr, isPasswordErr]);

  const togglePassword1 = () => {
    if (passwordType1 === "password") {
      setPasswordType1("text")
      return;
    }
    setPasswordType1("password")
  }

  return (
    <div
      className="modal fade m-atas"
      id="modalLogin"
      aria-hidden="true"
      aria-labelledby="modalLogin"
      tabIndex="-1"
    >
       <div id="pop-loading"
                  style={{
                    display: "none",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100vh",
                  }}
                >
                  {/* <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" /> */}
        </div>
      <div 
        id="modal-log-in" 
        className="modal-dialog modal-auth" 
        style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)' } : {}}
      >
        <div 
          className="modal-content" 
          id="loginpopup"
          style={isSmallDevice || isMediumDevice ? { height: '100%' } : {}}
        >
          <div className="modal-header">
            <img src="/images/bg/bg_icon.svg" className="acc img-fluid" />
            <div className="text-center">
              <h5
                style={{ lineHeight: "41.6px", fontFamily: "FuturaBT" }}
                className="modal-title mb-3"
                id="exampleModalLabel"
              >
                Masuk
              </h5>
              <div className="desc">
                Temukan berbagai fitur dan layanan menarik dari BTN Properti
                dengan masuk terlebih dahulu.
              </div>
            </div>

            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <i className="bi bi-x-lg"></i>
            </button>
          </div>
          <div className="modal-body" style={{ borderRadius: "16px" }}>
            <div className="modal-form">
              <form id="form-login" onSubmit={submitLogin}>
                {/* {errorLogin && <div  className="error-login">{errorMessage}</div>} */}
                <div className="floating-label-wrap ">
                  <input
                    type="email"
                    className="floating-label-field"
                    id="login_email"
                    placeholder="Email"
                    value={email}
                    onChange={handleChangeEmail}
                    // onBlur={(e) => handleBlurEmail(e, e.target.id)}
                    onBlur={(e) => handleBlurEmail(e.target.value)}
                    onKeyPress={handleKeyPress}
                    autoComplete={remember ? "on" : "off"}
                    required
                    style={{
                      borderColor: isEmailErr ? "red" : "#aaaaaa",
                      color: "#00193E",
                    }}
                  />
                  <label htmlFor="login_email" className="floating-label">
                    Email
                  </label>
                  {emailErrorMessage && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {emailErrorMessage}
                    </div>
                  )}
                </div>
                <div className="floating-label-wrap mb-2">
                  <input
                    type={passwordType1}
                    className="floating-label-field"
                    id="login_password"
                    placeholder="password"
                    value={password}
                    onBlur={(e) => handleBlurPassword(e.target.value)}
                    onChange={handleChangePassword}
                    // onBlur={(e) => handleBlurPassword(e.target.value)}
                    required
                    style={{
                      borderColor: isPasswordErr ? "red" : "#aaaaaa",
                      color: "#00193E",
                    }}
                    onKeyPress={handleKeyPress}
                    autoComplete={remember ? "on" : "off"}
                  />
                  <label htmlFor="login_password" className="floating-label">
                    Password
                  </label>
                  <div className="eye-password" onClick={togglePassword1}>
                      {passwordType1 === "password" ?
                        <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                        <img src="/images/icons/eye.svg" width="20" height="20" />
                      } 
                  </div>
                  {passwordErrorMessage && (
                    <div className="error-input">
                      <i
                        class="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      ></i>
                      {passwordErrorMessage}
                    </div>
                  )}
                </div>

                <div className="form-group">
                  <div className="text-end lupa_password pb-2 mb-4 mt-2">
                    <a
                      href="#"
                      onClick={resetValidation}
                      className="modal-link"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      data-bs-target="#modalResetPassword"
                      data-bs-toggle="modal"
                    >
                      Lupa Password?
                    </a>
                  </div>
                </div>

                <div
                  className="d-grid gap-2 container-btn-login"
                  style={{ opacity: validated ? 1 : 0.25 }}
                >
                  <button
                    style={{
                      marginTop: 0,
                      marginBottom: 0,
                    }}
                    className="login-btn btn btn-main btn-block"
                    // data-bs-dismiss="modal"
                    onClick={submitLogin}
                    ref={buttonRef}
                    disabled={validated && !isPasswordErr && !isEmailErr ? false : true}
                  >
                    Masuk Sekarang
                  </button>
                </div>
                <div
                  className="remember-me mt-16"
                  style={{
                    marginTop: "16px",
                  }}
                >
                  <label>
                    <input
                      type="checkbox"
                      name="login[remember]"
                      checked={remember}
                      onChange={changeRemember}
                    />
                    <span
                      className="form-check-label font-sm-4 opacity-8 ms-1"
                      style={{
                        fontSize: 16,
                        fontFamily: "Helvetica",
                        fontStyle: "normal",
                        fontWeight: "normal",
                        lineHeight: "160%",
                        margintop: "16px",
                      }}
                    >
                      Ingat Akun Saya
                    </span>
                  </label>
                </div>
              </form>
            </div>
            <div
              style={{
                width: isMobile ? `calc(100% + 60px)` : `calc(100% + 120px)`,
                marginLeft:
                  typeof window !== "undefined"
                    ? window.innerWidth < 768
                      ? -30
                      : -60
                    : -60,
                marginTop: "24px",
                marginBottom: "24px",
                borderBottom: "2px solid #EEEEEE",
                pb: "5px",
              }}
            ></div>
            <div className="line-on-side text-center">
              <span
                className="login-with"
                style={{
                  fontSize: 14,
                  fontFamily: "Helvetica",
                  fontStyle: "normal",
                  fontWeight: "normal",
                  lineHeight: "130%",
                  marginBottom: 16,
                }}
              >
                Atau Masuk dengan
              </span>
            </div>
            <div className="media-login">
              <ul className="nav justify-content-center my-3">
                <li className="nav-item">
                  <div className="nav-link">
                    <img src="/images/icons/auth_google.png" onClick={()=>{googleLogin(setUserKey, setUserProfile)}} />
                  </div>
                </li>
                <li className="nav-item">
                  <a className="nav-link">
                    <img src="/images/icons/auth_facebook.png" onClick={facebookLogin} />
                  </a>
                </li>
                {/* <li className="nav-item">
                  <a className="nav-link" onClick={twitterLogin}>
                    <img src="/images/icons/auth_twitter.png" />
                  </a>
                </li> */}
              </ul>
              <div className="modal-block ">
                <div
                  className="modal-block text-center register-label"
                  style={{ fontFamily: "Helvetica", fontWeight: 400 }}
                >
                  Belum punya akun?
                  <a
                    href="#"
                    onClick={resetValidation}
                    className="modal-link me-1"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalRegister"
                    data-bs-toggle="modal"
                    style={{fontWeight:"400"}}
                  >
                    {" "}
                    Daftar sekarang
                  </a>
                  |
                  <a
                    href="#"
                    onClick={resetValidation}
                    className="modal-link ms-1"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalLinkAktivasi"
                    data-bs-toggle="modal"
                    style={{fontWeight:"400"}}
                  >
                    Kirim Ulang Link Aktivasi
                  </a>
                </div>
              </div>

              {/* <div className="d-grid gap-2 ">
                  {validated === true ? (
                    <button type="submit" className="btn btn-main btn-block">
                      Masuk Sekarang
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-main btn-block"
                      disabled
                    >
                      Masuk Sekarang
                    </button>
                  )}
                </div> */}
              {/* <div className="remember-me mt-2">
                  <label>
                    <input type="checkbox" name="login[remember]" />
                    <span className="font-sm-4 opacity-8 ms-1">
                      Ingat Akun Saya
                    </span>
                  </label>
                </div> */}
              {/* </form>
            </div> */}

              {/* <div
              style={{
                marginTop: "24px",
                marginBottom: "24px",
                marginLeft: -60,
                width: "125%",
                height: "1px",
                backgroundColor: "#EEEEEE",
              }}
            ></div>
            <div className="line-on-side text-center">
              <span>Atau Masuk dengan</span>
            </div> */}
              {/* <div className="media-login">
              <ul className="nav justify-content-center my-3"> */}
              {/* <li className="nav-item">
                  <a className="nav-link" href="#">
                    <img src="/images/icons/auth_google.png" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <img src="/images/icons/auth_facebook.png" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <img src="/images/icons/auth_twitter.png" />
                  </a>
                </li> */}
              {/* </ul>
              <div className="modal-block ">
                <div className="modal-block text-center">
                  Belum punya akun?
                  <a
                    href="#"
                    className="modal-link"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalRegister"
                    data-bs-toggle="modal"
                  >
                    {" "}
                    Daftar sekarang
                  </a>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
