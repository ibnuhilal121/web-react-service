import Link from "next/link";
import React from "react";

const ConfirmSuccess = ()=>{
    return(
    <>
        <h1 style={{fontFamily:"FuturaBT",fontWeight:"700",color:"#00193E"}}>
            Aktivasi Member Berhasil
        </h1>
        <p style={{fontFamily:"Helvetica"}}>
            Member anda sudah terverifikasi, anda dapat memulai menemukan properti impian
        </p>
        <Link href="/">
            <div className="btn btn-main btn-profesional d-flex justify-content-center">
                Kembali ke beranda
            </div>
        </Link>
    </>
    )
}

export default ConfirmSuccess;