import React, { useEffect, useState } from 'react'
import Lottie from 'react-lottie'
import * as animationData from "../../public/animate_home.json"
import * as animationDataFailed from "../../public/x_animation.json"
import Link from 'next/link'
import { defaultHeaders } from '../../utils/defaultHeaders'

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
const defaultOptionsFailed = {
    loop: true,
    autoplay: true,
    animationData: animationDataFailed,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

export default function AktivasiMember(props) {
    const [resError, setResError] = useState(false)
    const checkActivation = async () => {
        try {
            const payload = {
                Code: `${props.code}`
            }
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/confirmation`, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                    AccessKey_App: localStorage.getItem('accessKey'),
                    ...defaultHeaders
                },
                body: new URLSearchParams(payload) 
            })
            const response = await res.json()
            console.log(response);
            if(!response.IsError) {
                setResError(false)
            } else {
                setResError(true)
            }
        } catch (error) {
            console.log(error);
            
        }
    }
    useEffect(() => {
        checkActivation()
    }, [])

  return (
    <div className='aktivasi-member-container'>
        <div className="aktivasi-member-wrapper">
            <div style={{display: 'flex'}} className="aktivasi-member-flex">
                <Lottie 
                    options={resError ? defaultOptionsFailed : defaultOptions}
                    height={resError ? 300: 250}
                    width={resError ? 300: 350}
                    isStopped={false}
                    isPaused={false}
                    style={resError ? {margin: 0, minWidth: 300, minHeight: 250} : {margin: 0, minWidth: 250, minHeight: 250}}
                />
                <div className="aktivasi-member-text-group">
                    <h1 className="aktivasi-member-title">{resError ? "Aktivasi Member Gagal" : "Aktivasi Member Berhasil"}</h1>
                    {resError ?
                    <p className="aktivasi-member-text">
                        Kode verifikasi sudah digunakan, Silakan klik{" "}
                    <span
                        style={{color: "#0000FF", cursor: "pointer"}}
                        data-bs-dismiss="modal"
                        aria-label="Close"
                        data-bs-target="#modalLinkAktivasi"
                        data-bs-toggle="modal"
                        >
                            disini
                        </span>
                        {" "}
                         untuk mengirim ulang link aktivasi
                         </p>
                    :
                    <p className="aktivasi-member-text">Member anda sudah terverifikasi, anda dapat mulai menemukan properti impian</p>
                    }
                    
                </div>
            </div>
            <Link  href="/">
             <button className="aktivasi-member-btn-beranda">Beranda</button>
            </Link>
        </div>
        
    </div>
  )
}
