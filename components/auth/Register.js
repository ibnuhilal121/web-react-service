/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useRef, useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Lottie from "react-lottie";
import ReCAPTCHA from "react-google-recaptcha";
import {
  doCheckRegisterMemberEmail,
  doMemberRegister,
  doResendActivation,
} from "../../services/memberArea";
import checkIsValidEmail from "../../utils/checkIsValidEmail";
import { isInvalid } from "../../utils/validation.register";
import * as animationData from "../../public/animate_home.json";
import { Alert } from "react-bootstrap";
import { useMediaQuery } from "react-responsive";
import { encryptPass } from "../../services/auth";
// import Navbar from "../Navbar";

/* ------------ inits/configs ---------- */
const INITIAL_FORM = {
  fullname: null,
  email: null,
  phoneNumber: null,
  password: null,
  repassword: null,
  checkedSnk: false,
};
const INITIAL_ERROR_REQUIRED = {
  fullname: false,
  email: false,
  phoneNumber: false,
  password: false,
  repassword: false,
  checkedSnk: false,
};
const defMailMessage = "Please insert valid email";
const defErrorMessage = "Required";
const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
const UNFILLED = "Isian tidak boleh kosong";
const WRONG_PWD =
  "Password minimal 8 karakter, terdiri dari huruf besar, huruf kecil, angka dan karakter khusus (.@$!%*?&)";
function print(a) {
  return "Isi dengan " + a;
}

/* ---------------------- START COMP ---------------------- */
export default function Register() {
  /* ------------ hook: states ---------- */
  const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
  const isMediumDevice = useMediaQuery({
    query: `(min-width: 401px) and (max-width: 576px)`,
  });
  const [loading, setLoading] = useState(false);
  const [isMailValid, setIsMailValid] = useState(true);
  const [isMatchPassword, setMatchPassword] = useState(true);
  const [mailErrorMessage, setMailErrorMessage] = useState(defMailMessage);
  const [valueNama, SetValueNama] = useState("");
  const [valueEmail, SetValueEmail] = useState("");
  const [valueNoTelphon, SetValueNoTelphon] = useState("");
  const [valuePassword, SetValuePassword] = useState("");
  const [valueConfirPassword, SetConfirValuePassword] = useState("");
  const [valueCaptcha, setValueCaptcha] = useState(null);
  const [valueCheckbox, SetValueCheckbox] = useState("");
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [passwordConfirmErrorMessage, setPasswordConfirmErrorMessage] =
    useState("");
  const [isFullnameValid, setFullnameValid] = useState(true);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isPhoneValid, setPhoneValid] = useState(true);
  const [isPasswordValid, setPasswordValid] = useState(true);
  const [isPasswordConfirmValid, setPasswordConfirmValid] = useState(true);
  const [isDisabled, setDisabled] = useState(true);
  const [formState, setFormState] = useState(INITIAL_FORM);
  const [errorRequired, setErrorRequired] = useState(INITIAL_ERROR_REQUIRED);
  const [passwordType1, setPasswordType1] = useState("password");
  const [passwordType2, setPasswordType2] = useState("password");
  const [jarak, setJarak ] = useState("5px");
  const [jarakCapcha, setJarakCapcha ] = useState("0");

  /* ------------ hook: refs ---------- */
  const btnModalSukses = useRef();
  const btnCloseModalRegister = useRef();
  const recaptchaRef = React.createRef();

  /* ------------ nextjs hook: router ---------- */
  const router = useRouter();

  /* ------------ hook: effects ---------- */
  useEffect(() => {
    if (
      isFullnameValid &&
      isEmailValid &&
      isPasswordValid &&
      isPhoneValid &&
      valueCheckbox &&
      valueCaptcha
    ) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [
    valueNama,
    isFullnameValid,
    valueEmail,
    isEmailValid,
    valuePassword,
    isPasswordValid,
    valueNoTelphon,
    isPhoneValid,
    valueCheckbox,
    valueCaptcha,
  ]);

  useEffect(() => {
    // console.log('~ errorRequired', errorRequired)
  }, [errorRequired]);

  /* ------------ handlers ---------- */
  const handleChangeFullname = (e) => {
    SetValueNama(e.target.value);
    handleRequired("fullname", false);
  };

  const handleChangeEmail = (e) => {
    SetValueEmail(e.target.value.replace(/[^a-zA-Z0-9 _@.~-]+/, ''));
    handleRequired("email", false);
    setIsMailValid(true);
  };

  const handleChangePhone = (e) => {
    const value = e.target.value;
    setPhoneValid(true)
    if (!isInvalid("phone", value)) {
      SetValueNoTelphon(value);
    }
    handleRequired("phoneNumber", false);
  };

  const handleChangePassword = (e) => {
    SetValuePassword(e.target.value);
    handleRequired("password", false);
  };

  const handleChangeConfirmPassword = (e) => {
    SetConfirValuePassword(e.target.value);
    handleRequired("repassword", false);
  };

  const handleChangeCaptcha = (isHuman) => {
    setValueCaptcha(isHuman);
  };

  const handleChangeCheckbox = (e) => {
    SetValueCheckbox(e.target.checked);
    handleChangeForm("checkedSnk", e.target.checked);
  };

  const handleChangeForm = (attribut, newVal) => {
    setFormState((prevValue) => {
      return {
        ...prevValue,
        [attribut]: newVal,
      };
    });
  };

  const handleBlurFullname = (e) => {
    const value = valueNama.trim();
    if (!value) return setFullnameValid(false);
    setFullnameValid(true);
    handleChangeForm("fullname", value);
  };

  const handleBlurEmail = (e) => {
    const id = e.target.id;
    const value = valueEmail.trim();

    if (!value) {
      setEmailErrorMessage(UNFILLED);
      return setEmailValid(false);
    }
    if (value && isInvalid(id, value)) {
      setEmailErrorMessage(print("alamat email"));
      return setEmailValid(false);
    }
    if (!isEmailValid) setEmailValid(true);

    handleChangeForm("email", value);
  };

  const handleBlurPhone = (e) => {
    const id = e.target.id;
    const value = valueNoTelphon.trim();

    if (!value) {
      setPhoneErrorMessage(UNFILLED);
      return setPhoneValid(false);
    } else if (isInvalid(id, value)) {
      setPhoneErrorMessage(print("angka"));
      return setPhoneValid(false);
    } else if (isInvalid('phone_validation', value) || isInvalid('phone_length_validation', value)) {
      setPhoneErrorMessage("Nomor telepon harus format 08XXXX, minimal 10 digit, dan maksimal 14 digit");
      return setPhoneValid(false);
    }
    

    setPhoneValid(true);
    handleChangeForm("phoneNumber", value);
  };

  const handleBlurPassword = (e) => {
    const id = e.target.id;
    const value = valuePassword.trim();

    if (!value) {
      setPasswordErrorMessage(UNFILLED);
      setJarak("5px")
      return setPasswordValid(false);
    }
    if (value && isInvalid(id, value)) {
      setPasswordErrorMessage(WRONG_PWD);
      setJarak("22px")
      return setPasswordValid(false);
    }
    if (!isPasswordValid) setPasswordValid(true);
    handleChangeForm("password", value);
  };

  const handleBlurConfirmPassword = (e) => {
    const id = e.target.id;
    const value = valueConfirPassword.trim();

    if (!value) {
      setPasswordConfirmErrorMessage(UNFILLED);
      setJarakCapcha("5px")
      return setPasswordConfirmValid(false);
    }
    if (value && isInvalid(id, value)) {
      setPasswordConfirmErrorMessage(WRONG_PWD);
      setJarakCapcha("30px")
      return setPasswordConfirmValid(false);
    }
    if (value !== valuePassword) {
      setPasswordConfirmErrorMessage("Isi data yang sama dengan password");
      setJarakCapcha("5px")
      return setPasswordConfirmValid(false);
    }
    if (!isPasswordConfirmValid) {
      setJarakCapcha("0")
      setPasswordConfirmValid(true);
    }
    handleChangeForm("repassword", value);
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();
    // console.log('+++ formState', formState);
    const errorCounter = await validateForm();
    // console.log('+++ errorCounter', errorCounter);
    // console.log('+++ errorRequired', errorRequired);
    if (errorCounter < 1) {
      const payloadEmailCheck = {
        Email: formState.email,
      };
      //   console.log("+++ payloadRegister", payloadEmailCheck);
      doCheckRegisterMemberEmail(loadingHandler, payloadEmailCheck)
        .then((response) => {
          //   console.log("+++ response", response);
          if (!response?.IsError) {
            const payloadRegister = {
              n: formState.fullname,
              e: formState.email,
              hp: formState.phoneNumber,
              pwd: encryptPass(formState.password),
            };
            // console.log("+++ payloadRegister", payloadRegister);
            doMemberRegister(loadingHandler, payloadRegister)
              .then((responseRegis) => {
                //   console.log("+++ response", response);
                if (!responseRegis?.IsError) {
                  //   console.log(responseRegis.Output);
                  btnModalSukses.current.click();
                  btnCloseModalRegister.current.click();
                  // btnModalSukses.current.modal('show');
                } else {
                  console.log(responseRegis.ErrToUser);
                }
              })
              .catch((err) => {
                console.log(`Something wrong:${err}`);
                loadingHandler(false);
              });
          } else {
            setMailErrorMessage(
              response.ErrToUser || "Check email error, Please try again"
            );
            // console.log(response.ErrToUser);
            setIsMailValid(false);
            document.getElementById("modalRegister").scrollTo({
              top: 1,
              left: 1,
            });
          }
        })
        .catch((err) => {
          console.log(`Something wrong:${err}`);
          loadingHandler(false);
        });
    }
  };

  const handleResetStates = useCallback(() => {
    SetValueNama("");
    SetValueEmail("");
    SetValueNoTelphon("");
    SetValuePassword("");
    SetConfirValuePassword("");
    setFullnameValid(true);
    setEmailValid(true);
    setPhoneValid(true);
    setPasswordValid(true);
    setPasswordConfirmValid(true);
    setIsMailValid(true);
    setMailErrorMessage("");
  }, [
    valueNama,
    valueEmail,
    valueNoTelphon,
    valuePassword,
    valueConfirPassword,
    isFullnameValid,
    isEmailValid,
    isPhoneValid,
    isPasswordValid,
    isPasswordConfirmValid,
    isMailValid,
    mailErrorMessage,
  ]);

  const handleClickToPage = (e) => {
    e.preventDefault();
    router.push("/terms");
  };

  const handleKeyPress = (e) => {
    const fullname = valueNama.trim();
    const email = valueEmail.trim();
    const phone = valueNoTelphon.trim();
    const password = valuePassword.trim();
    const repassword = valueConfirPassword.trim();

    if (e.key === "Enter") {
      if (!fullname) {
        setFullnameValid(false);
      } else if (fullname && isInvalid("fullname", fullname)) {
        setFullnameValid(false);
      } else setFullnameValid(true);

      if (!email) {
        setEmailErrorMessage(UNFILLED);
        setEmailValid(false);
      } else if (email && isInvalid("email", email)) {
        setEmailErrorMessage(print("alamat email"));
        setEmailValid(false);
      } else setEmailValid(true);

      if (!phone) {
        setPhoneErrorMessage(UNFILLED);
        setPhoneValid(false);
      } else if (phone && isInvalid("phone", phone)) {
        setPhoneErrorMessage(print("angka"));
        setPhoneValid(false);
      } else setPhoneValid(true);

      if (!password) {
        setPasswordErrorMessage(UNFILLED);
        setJarak("5px")
        setPasswordValid(false);
      } else if (password && isInvalid("password", password)) {
        setPasswordErrorMessage(WRONG_PWD);
        setJarak("22px")
        setPasswordValid(false);
      } else setPasswordValid(true);

      if (!repassword) {
        setPasswordConfirmErrorMessage(UNFILLED);
        setPasswordConfirmValid(false);
        setJarakCapcha("5px")
      } else if (repassword && isInvalid("repassword", repassword)) {
        setPasswordConfirmErrorMessage(WRONG_PWD);
        setPasswordConfirmValid(false);
        setJarakCapcha("30px")
      } else {
        setPasswordConfirmValid(true);
        setJarakCapcha("0")
      }
    }
  };

  // VALIDASI MASING-MASING FIELD SAAT USER MENINGGALKAN FIELD TSB
  function onBlurNama() {
    if (formState.fullname) {
      handleRequired("fullname", false);
    } else {
      handleRequired("fullname", true);
    }
  }

  function onBlurEmail() {
    if (formState.email) {
      handleRequired("email", false);
    } else {
      handleRequired("email", true);
    }
  }

  function onBlurTelepon() {
    if (formState.phoneNumber) {
      handleRequired("phoneNumber", false);
    } else {
      handleRequired("phoneNumber", true);
    }
  }

  function onBlurPassword() {
    if (formState.password) {
      handleRequired("password", false);
    } else {
      handleRequired("password", true);
    }
  }

  function onBlurRepassword() {
    if (formState.repassword) {
      handleRequired("repassword", false);
    } else {
      handleRequired("repassword", true);
    }
  }

  /* ------------ utils/helpers ---------- */
  let isBtnDisabled = true;
  if (
    valueNama &&
    valueEmail &&
    valueNoTelphon &&
    valuePassword &&
    valueConfirPassword &&
    valueCheckbox &&
    formState.captcha
  ) {
    isBtnDisabled = false;
  }

  // VALIDASI AFTER KLIK TOMBOL REGISTER
  async function validateForm() {
    let errorCount = 0;
    if (formState.fullname === null || formState.fullname === "") {
      // console.log("+++ fullname error")
      handleRequired("fullname", true);
      errorCount += 1;
    }
    if (formState.email === null || formState.email === "") {
      // console.log("+++ email error")
      handleRequired("email", true);
      errorCount += 1;
    }
    if (formState.phoneNumber === null || formState.phoneNumber === "") {
      // console.log("+++ password error")
      handleRequired("phoneNumber", true);
      errorCount += 1;
    }
    if (formState.password === null || formState.password === "") {
      // console.log("+++ email error")
      handleRequired("password", true);
      errorCount += 1;
    }
    if (formState.repassword === null || formState.repassword === "") {
      // console.log("+++ email error")
      handleRequired("repassword", true);
      errorCount += 1;
    }
    if (!formState.checkedSnk) {
      // console.log("+++ checkedSnk error")
      handleRequired("checkedSnk", true);
      errorCount += 1;
    }
    if (!isMatchPassword || formState.password !== formState.repassword) {
      // console.log("+++ isMatchPassword error")
      errorCount += 1;
    }
    if (!isMailValid) {
      // console.log("+++ isMailValid error")
      errorCount += 1;
    }
    return errorCount;
  }

  const resendActivation = () => {
    const payload = {
      Email: formState.email,
    };
    doResendActivation(loadingHandler, payload)
      .then((responseRegis) => {
        //   console.log("+++ response", response);
        if (!responseRegis?.IsError) {
          console.log(responseRegis.Output);
        } else {
          console.log(responseRegis.ErrToUser);
        }
      })
      .catch((err) => {
        console.log(`Something wrong:${err}`);
        loadingHandler(false);
      });
  };

  // HANDLER FOR STATE
  function loadingHandler(bool) {
    setLoading(bool);
  }

  function handleRequired(keyname, bool) {
    setErrorRequired((prevVal) => {
      return {
        ...prevVal,
        [keyname]: bool,
      };
    });
  }

  // show password
  const togglePassword1 = () => {
    if (passwordType1 === "password") {
      setPasswordType1("text")
      return;
    }
    setPasswordType1("password")
  }
  const togglePassword2 = () => {
    if (passwordType2 === "password") {
      setPasswordType2("text")
      return;
    }
    setPasswordType2("password")
  }
  return (
    <div>
      {/* <Navbar /> */}
      <div className="modal fade" id="modalRegister" aria-hidden="true" aria-labelledby="modalRegister" tabIndex="-1">
        <div 
          id="modal-responsive" 
          className="modal-dialog modal-auth" 
          style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)' } : {}}
        >
          <div id="modal-content-register" className="modal-content">
            <div className="modal-header">
              <img src="/images/bg/bg_icon.svg" className="acc img-fluid" />
              <div className="text-center">
                <h5 className="modal-title" id="exampleModalLabel">
                  Daftar
                </h5>
                <div className="desc">
                  Bergabung untuk terhubung dengan berbagai fitur dan layanan
                  menarik dari BTN Properti.
                </div>
              </div>

              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                data-bs-target="#modalRegister"
                ref={btnCloseModalRegister}
                onClick={handleResetStates}
              >
                <i className="bi bi-x-lg"></i>
              </button>
            </div>
            <div className="modal-body" style={{ borderRadius: "16px" }}>
              <div className="modal-form" style={{ borderRadius: "16px" }}>
                {!isMailValid && (
                  <Alert
                    variant="danger"
                    style={{ marginTop: "-20px", marginBottom: "50px" }}
                  >
                    {mailErrorMessage}
                  </Alert>
                )}
                <form
                  className="form-validation"
                  id="form-Register"
                  onSubmit={handleSubmitForm}
                >
                  <div
                    id="modal-daftar-sekarang"
                    className="floating-label-wrap "
                    style={{
                      transform: "translateY(-28px)",
                      marginTop: "38px",
                    }}
                  >
                    <input
                      type="text"
                      className="floating-label-field"
                      id="fullname"
                      placeholder="Nama Lengkap"
                      required
                      value={valueNama}
                      onChange={handleChangeFullname}
                      onBlur={handleBlurFullname}
                      onKeyPress={handleKeyPress}
                      style={{
                        border: isFullnameValid
                          ? "1px solid #aaaaaa"
                          : "1px solid red",
                      }}
                    />
                    <label htmlFor="fullname" className="floating-label">
                      Nama Lengkap
                    </label>
                    <span
                      className="error-input e001"
                      style={{ opacity: isFullnameValid ? 0 : 1 }}
                    >
                      <i
                        className="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      />
                      Isian tidak boleh kosong
                    </span>
                  </div>
                  <div
                    id="modal-daftar-sekarang"
                    className="floating-label-wrap "
                    style={{ top: "-20px" }}
                  >
                    <input
                      type="email"
                      className="floating-label-field"
                      id="email"
                      placeholder="Email"
                      required
                      value={valueEmail}
                      onChange={handleChangeEmail}
                      onBlur={handleBlurEmail}
                      onKeyPress={handleKeyPress}
                      style={{
                        border: isEmailValid
                          ? "1px solid #aaaaaa"
                          : "1px solid red",
                        color: "#00193E",
                      }}
                    />
                    <label htmlFor="email" className="floating-label">
                      Email
                    </label>
                    <span
                      className="error-input e001"
                      style={{ opacity: isEmailValid ? 0 : 1 }}
                    >
                      <i
                        className="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      />
                      {emailErrorMessage}
                    </span>
                  </div>

                  <div
                    id="modal-daftar-sekarang"
                    className="floating-label-wrap "
                    style={{ top: "-13px" }}
                  >
                    <input
                      type="text"
                      className="floating-label-field"
                      id="phone"
                      placeholder="No Telepon"
                      required
                      value={valueNoTelphon}
                      onChange={handleChangePhone}
                      onBlur={handleBlurPhone}
                      onKeyPress={handleKeyPress}
                      maxLength={14}
                      style={{
                        border: isPhoneValid
                          ? "1px solid #aaaaaa"
                          : "1px solid red",
                      }}
                      
                    />
                    <label htmlFor="phone" className="floating-label">
                      No Telepon
                    </label>
                    <span
                      className="error-input e001"
                      style={{ opacity: isPhoneValid ? 0 : 1 }}
                    >
                      <i
                        className="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      />
                      {phoneErrorMessage}
                    </span>
                  </div>
                  <div
                    id="modal-daftar-password"
                    className="floating-label-wrap"
                    style={{ top: "-5px" }}
                  >
                    <input
                      type={passwordType1}
                      className="floating-label-field"
                      id="password"
                      placeholder="password"
                      required
                      value={valuePassword}
                      onChange={handleChangePassword}
                      onBlur={handleBlurPassword}
                      onKeyPress={handleKeyPress}
                      style={{
                        border: isPasswordValid
                          ? "1px solid #aaaaaa"
                          : "1px solid red",
                      }}
                    />
                    <label htmlFor="password" className="floating-label">
                      Password
                    </label>
                    <div className="eye-password" onClick={togglePassword1}>
                      {passwordType1 === "password" ?
                        <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                        <img src="/images/icons/eye.svg" width="20" height="20" />
                      } 
                    </div>
                    <span
                      className="error-input e001"
                      style={{ opacity: isPasswordValid ? 0 : 1 }}
                    >
                      <i
                        className="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      />
                      {passwordErrorMessage}
                    </span>
                  </div>
                  <div
                    id="modal-daftar-password"
                    className="floating-label-wrap"
                    style={{
                      transform: `translateY(${jarak})`,
                      marginBottom: 15
                    }}
                  >
                    <input
                      type={passwordType2}
                      className="floating-label-field"
                      id="repassword"
                      placeholder="Konfirmasi Password"
                      required
                      value={valueConfirPassword}
                      onChange={handleChangeConfirmPassword}
                      onBlur={handleBlurConfirmPassword}
                      onKeyPress={handleKeyPress}
                      style={{
                        border: isPasswordConfirmValid
                          ? "1px solid #aaaaaa"
                          : "1px solid red",
                      }}
                    />
                    <label htmlFor="repassword" className="floating-label">
                      Konfirmasi Password
                    </label>
                    <div className="eye-password" onClick={togglePassword2}>
                      {passwordType2 === "password" ?
                        <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                        <img src="/images/icons/eye.svg" width="20" height="20" />
                      } 
                    </div>
                    <span
                      className="error-input e001"
                      style={{ opacity: isPasswordConfirmValid ? 0 : 1 }}
                    >
                      <i
                        className="bi bi-exclamation-circle-fill"
                        style={{ marginRight: "5px" }}
                      />
                      {passwordConfirmErrorMessage}
                    </span>
                  </div>

                  <div
                    className="form-group"
                    style={{
                      width: "auto",
                      maxWidth: "100%",
                      height: "auto",
                      marginTop: `${jarakCapcha}`,
                      transform: "translateY(23px)",
                      display: "flex",
                      placeContent: "center"
                    }}
                  >
                    {/* <img src="/images/icons/captcha.png" className="img-fluid" /> */}
                    <ReCAPTCHA
                      ref={recaptchaRef}
                      onChange={handleChangeCaptcha}
                      size="normal"
                      style={{
                        transform: isSmallDevice
                          ? "scale(0.77)"
                          : isMediumDevice
                          ? "scale(0.9)"
                          : "scale(1)",
                        WebkitTransform: isSmallDevice
                          ? "scale(0.77)"
                          : isMediumDevice
                          ? "scale(0.9)"
                          : "scale(1)",
                        //transformOrigin: "0 0",
                        //WebkitTransformOrigin: "0 0",
                      }}
                      sitekey="6Lel4Z4UAAAAAOa8LO1Q9mqKRUiMYl_00o5mXJrR"
                    />            
                  </div>
                  <div
                    className="syarat-me mt-3"
                    style={{ transform: "translateY(15px)" }}
                  >
                    <label>
                      <input
                        type="checkbox"
                        name="Register[syarat]"
                        value={valueCheckbox}
                        onChange={handleChangeCheckbox}
                      />
                      <span
                        className="font-sm-4 opacity-8"
                        style={{
                          marginLeft: 12,
                          fontFamily: "Helvetica",
                          fontWeight: 400,
                          color: "#00193E",
                        }}
                      >
                        Dengan ini saya menyetujui
                        <a
                          href="/terms"
                          // onClick={handleClickToPage}
                          // data-bs-dismiss="modal"
                        >
                          {" "}
                          syarat dan ketentuan
                        </a>{" "}
                        yang berlaku.
                      </span>
                    </label>
                  </div>
                  <div
                    id="button_modal_reg"
                    className="d-grid gap-2 my-3"
                    style={{
                      height: "48px",
                      transform: "translateY(15px)",
                    }}
                  >
                    <button
                      type="submit"
                      className="btn btn-main btn-block"
                      disabled={isDisabled}
                      aria-label="Close"
                      // data-bs-target="#modalSuccessRegister"
                      onClick={handleSubmitForm}
                    >
                      Daftar Sekarang
                    </button>
                  </div>
                </form>
              </div>
              <div className="media-Register">
                <div className="modal-block ">
                  <div
                    className="modal-block text-center"
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: 400,
                      color: "black",
                      transform: "translateY(18px)",
                    }}
                  >
                    Anda sudah mempunyai akun?{" "}
                    <a
                      href="#"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      data-bs-target="#modalLogin"
                      data-bs-toggle="modal"
                      onClick={handleResetStates}
                    >
                      Masuk sekarang
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal fade"
        id="modalSuccessRegister"
        aria-hidden="true"
        aria-labelledby="modalSuccessRegister"
        tabIndex="-1"
      >
        <div className="modal-dialog">
          <div
            className="modal-content"
            style={{
              maxWidth: "600px",
              minHeight: "576px",
              borderRadius: "8px",
              // marginLeft: "250px",
            }}
          >
            <div
              className="modal-header border-bottom-0"
              style={{ transform: "translateX(530px)", marginTop: "8px" }}
            >
              {/* <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" style={{ color: "#0061A7" }}></button> */}
              <button
                type="button"
                style={{ background: "none", border: "none" }}
                data-bs-dismiss="modal"
                aria-label="Close"
                data-bs-target="#modalSuccessRegister"
              >
                <svg
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                    fill="#0061A7"
                  />
                </svg>
              </button>
            </div>
            <div className="modal-body">
              <div className="text-center">
                <h4 className="modal-title mb-3">Terima Kasih</h4>
                <div
                  className="desc text-center mb-5"
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: "16px",
                    fontWeight: 400,
                    color: "black",
                    marginTop: "-5px",
                  }}
                >
                  Pendaftaran berhasil dan link aktivasi pendaftaran
                  <br />
                  sudah dikirimkan ke email kamu. Silakan periksa inbox email
                  kamu
                  <br />
                  untuk melakukan aktivasi member.
                </div>
                {loading && (
                  <div
                    className="spinner-border text-primary"
                    style={{ marginLeft: 10 }}
                    role="status"
                    aria-hidden="true"
                  />
                )}
                <Lottie
                  options={defaultOptions}
                  height={250}
                  width={350}
                  isStopped={false}
                  isPaused={false}
                />
                <div
                  className="py-3 pt-5"
                  size="16px"
                  style={{
                    fontWeight: 400,
                    fontFamily: "Helvetica",
                    fontSize: "14px",
                    color: "#000000",
                    transform: "translateY(-21px)",
                  }}
                >
                  Tidak menerima link aktivasi?{" "}
                  <a
                    onClick={resendActivation}
                    href="#"
                    style={{
                      color: "#0061A7",
                      fontFamily: "Helvetica",
                      fontSize: "14px",
                      fontWeight: "400px",
                    }}
                  >
                    Kirim ulang
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <button
        style={{ display: "none" }}
        data-bs-target="#modalSuccessRegister"
        data-bs-toggle="modal"
        ref={btnModalSukses}
      >
        Modal Button
      </button>
    </div>
  );
}
