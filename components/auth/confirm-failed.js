import React from "react";

const ConfirmFailed = ()=>{
    return(
    <>
        <h1>
            Aktivasi Member Gagal
        </h1>
        <p  style={{fontFamily:"Helvetica"}}>
            Kode verifikasi sudah digunakan. Silakan klik 
            <a style={{color:"#0061a7"}}> di sini </a> untuk mengirim ulang link aktivasi
        </p>
        <Link href="/">
            <div className="btn btn-main btn-profesional d-flex justify-content-center">
                Kembali ke beranda
            </div>
        </Link>
    </>
    )
}

export default ConfirmFailed;