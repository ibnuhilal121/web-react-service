import { useEffect, useRef, useState } from "react"
import axios from 'axios';
import qs from 'qs';
import Lottie from "react-lottie";
import { doSendForgotPassword } from "../../services/memberArea";
import checkIsValidEmail from "../../utils/checkIsValidEmail";
import { isInvalid } from "../../utils/validation.register"
import { useAppContext } from "../../context";
import * as animationData from "../../public/succes_animation.json"
import * as animationDataFailed from "../../public/x_animation.json"
import { useMediaQuery } from "react-responsive";
import ReCAPTCHA from "react-google-recaptcha";
import { defaultHeaders } from "../../utils/defaultHeaders";

const defMailMessage = "Please insert valid email";
const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
const defaultOptionsFailed = {
  loop: true,
  autoplay: true,
  animationData: animationDataFailed,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};



const LinkAktivasi = ()=>{
    const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
    const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
  
    const { userKey } = useAppContext();
  
    const [loading, setLoading] = useState(false)
    const [inputVal, setInputVal] = useState('');
    const [email, setEmail] = useState(null)
    const [isMailValid, setIsMailValid] = useState(true)
    const [responseSuccess, setResponseSuccess] = useState(true)
    const [responseMessage, setResponseMessage] = useState('')
    const [errorRequiredMail, setErrorRequiredMail] = useState(false)
    const [mailErrorMessage, setMailErrorMessage] = useState(defMailMessage)
    const [emailErrorMessage, setEmailErrorMessage] = useState('');
    const [isEmailValid, setEmailValid] = useState(true)
    const [isDisabled, setDisabled] = useState(true)
    const [isToggleSubmit, setToggleSubmit] = useState(false)
    const [rechaptaVal, setRechaptaVal] = useState('')
    const [abletToHit,setAbleToHit] = useState(true)
    const [timer,setTimer] = useState(0)
    const [timerMessage,setTimerMessage] = useState("")
  
    const handleChange = (e) => {
      setInputVal(e.target.value)
      // if(inputVal) {
      //   setDisabled(false)
        
      // } else {
      //   setDisabled(true)
      // }
    }

    const handleCountDown = (sec) => {
      let hours = Math.floor(sec / 3600);
      let divisorForMinutes = sec % 3600;
      let minutes = Math.floor(divisorForMinutes / 60);
      let divisorForSeconds = sec % 60;
      let seconds = Math.ceil(divisorForSeconds);
    
      return  minutes + " menit " + seconds + " detik"
    }
    const handleChangeRechapta = (val) => {
      setRechaptaVal(val)
      if(val) {
        if(inputVal) {
            setDisabled(false)

        }
      }
    }

    useEffect(() => {
      if(sessionStorage.getItem("timer")) {
        setTimer(sessionStorage.getItem("timer"))
      }
      
    }, [])

    useEffect(()=>{
      // const timeLeft = typeof window !== 'undefined' ? (sessionStorage.getItem("timer") || 300) : 300;
      if(timer > 0){
        setTimeout(() => {
        setDisabled(true)
        setTimer(timer - 1)
        window.sessionStorage.setItem("timer", timer - 1);
        }, 1000);
      }else{
        setAbleToHit(true)
        setTimerMessage("")
        window.sessionStorage.removeItem("timer");
        setDisabled(false)
      }
    },[timer])


    
    const handleSubmit = async (e) => {
      // if(abletToHit){
      //   setDisabled(false)
      //   setEmailErrorMessage('')
      try {
        e.preventDefault()
        setAbleToHit(false)
        const payload = {
          Email: inputVal
        }
        console.log(payload);
        if(window.sessionStorage.getItem("AKTIVASI_LIMIT") == 3) {
          setTimer(300);
          setTimerMessage("Mohon tunggu 5 menit")
          window.sessionStorage.removeItem("AKTIVASI_LIMIT")
        } else {
          const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/resendactivation`, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
              AccessKey_App: localStorage.getItem("accessKey"),
              ...defaultHeaders
            },
            body: new URLSearchParams(payload)
          })
          window.sessionStorage.setItem("AKTIVASI_LIMIT", window.sessionStorage.getItem("AKTIVASI_LIMIT") ? parseInt(window.sessionStorage.getItem("AKTIVASI_LIMIT")) + 1 : 1)
          const response = await res.json()
          console.log(response);
          if(!response.IsError) {
            setInputVal('')
            setResponseSuccess(true)
            setResponseMessage(response.Output)
            btnModalSukses.current.click()
            btnCloseModalForgotPwd.current.click()
          } else {
            setInputVal('')
            setResponseSuccess(false)
            setResponseMessage(response.ErrToUser)
            btnModalSukses.current.click()
            btnCloseModalForgotPwd.current.click()
          }
        }
        
      } catch (error) {
        console.log(error);
      } finally{
        e.preventDefault();
      }
    // }else{
    //   e.preventDefault()
    //   setDisabled(true)
    // }
  }
    const btnModalSukses = useRef();
    const btnCloseModalForgotPwd = useRef();
    const handleBlur = (e) => {
        const value = inputVal;
    
        if (!value) {
          setDisabled(true)
          setEmailErrorMessage('Isian tidak boleh kosong')
          return setEmailValid(false)}
        if (value && isInvalid("email", value)) {
          setDisabled(true)
          setEmailErrorMessage('Isi dengan alamat email')
          return setEmailValid(false)}
        if (!isEmailValid) setEmailValid(true)
        if(!rechaptaVal) {
          return setDisabled(true)
        }
        setEmail(inputVal)
        setDisabled(false)
      }
      const handleChangeEmail = (e) => {
        setInputVal(e.target.value.replace(/[^a-zA-Z0-9 _@.~-]+/, ''));
        if(inputVal) {
          setDisabled(false);
        }
        else {
          setDisabled(true);
        }
       
      };

    return (
        <>
          <div 
            className="modal fade m-atas" 
            id="modalLinkAktivasi" 
            aria-hidden="true" 
            aria-labelledby="modalLinkAktivasi" 
            tabIndex="-1"
          >
            <div 
              className="modal-dialog modal-auth" 
              style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)' } : {}}

            >
              <div 
                className="modal-content" 
                id="resetpasspopup"
                style={isSmallDevice || isMediumDevice ? { height: '100%' } : {}}
              >
                <div className="modal-header justify-content-center">
                  <img src="/images/bg/bg_icon.svg" className="acc img-fluid" style={{top:"10px"}} />
                  <div className="text-center">
                    <h5 className="modal-title" id="exampleModalLabel">Kirim Ulang Aktivasi</h5>
                    <div className="desc">
                        Silakan isi email anda untuk mengirim ulang aktivasi
                    </div>
                  </div>
    
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalLinkAktivasi"
                    ref={btnCloseModalForgotPwd}
                  >
                    <i className="bi bi-x-lg"></i>
                  </button>
                </div>
                <div className="modal-body" style={{ paddingTop: 62, borderRadius: '16px', paddingBottom: 0 }}>
                  <div className="modal-form">
                    <form onSubmit={e => handleSubmit(e)} className="form-validation" id="form-ResetPassword">
                      <div className="floating-label-wrap " style={{ height: 48 }}>
                        <input type="email" className="floating-label-field" id="login_email" placeholder="Email" required style={{ height: 48, border: isEmailValid ? '1px solid #aaaaaa' : '1px solid red' }}
                          onChange={(event) => {
                            handleChangeEmail(event)
                          }}
                          onBlur={handleBlur}
                          // onKeyDown={handleKeyDown}
                          value={inputVal}
                          disabled={timer > 0 ? true : false}
                        />
                        <label htmlFor="login_email" className="floating-label">Email</label>
                        <span className="error-input e001" style={{ opacity: isEmailValid ? 0 : 1 }}>
                          <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }} />
                          {emailErrorMessage}
                        </span>
                        {/* {errorRequiredMail && <div  className="error-input">Required!</div>}
                                            {!isMailValid && <div  className="error-input">{mailErrorMessage}</div>} */}
                      </div>
                        <ReCAPTCHA
                        //   ref={recaptchaRef}
                          onChange={(val) => handleChangeRechapta(val)}
                          onExpired={() => setDisabled(true)}
                        aria-checked={true}
                        size="normal"
                        style={{
                            transform: isSmallDevice
                            ? "scale(0.77)"
                            : isMediumDevice
                            ? "scale(0.9)"
                            : "scale(1)",
                            WebkitTransform: isSmallDevice
                            ? "scale(0.77)"
                            : isMediumDevice
                            ? "scale(0.9)"
                            : "scale(1)",
                            transformOrigin: "0 0",
                            WebkitTransformOrigin: "0 0",
                        }}
                        sitekey="6Lel4Z4UAAAAAOa8LO1Q9mqKRUiMYl_00o5mXJrR"
                        hidden={timer > 0 ? true : false}
                            />
                      <div className="reset-button mt-4" style={{ marginBottom: 0, paddingBottom: 80 }}>
                        <button
                          type="submit"
                          disabled={isDisabled && timer > 0}
                        //   onClick={handleSubmit}
                          className="btn btn-main btn-block w-100"
                          style={{ height: 480, height: 48, fontFamily: 'Helvetica', fontWeight: 700 }}
                        >
                          Submit
                          {loading && (<div className="spinner-border spinner-border-sm text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />)}
                        </button>
                        {timer > 0 ?
                         <span className="error-input e001">
                         <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }} />
                         Coba lagi dalam {handleCountDown(timer)}
                       </span>
                       : ""
                        }
                       
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
          <div
            className="modal fade"
            id="modalSuccessActivation"
            aria-hidden="true"
            aria-labelledby="modalSuccessActivation"
            tabIndex="-1"
          >
            <div className="modal-dialog">
              <div className="modal-content" 
              style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)' } : { padding: '0 3rem 3rem'}}
              // style={{ width: "600px", height: "576px", borderRadius: "8px" }}
              >
                <div className="modal-header border-bottom-0">
                {/* <button
                    type="button"
                    style={{ background: "none", border: "none", marginLeft:'535px' }}
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    data-bs-target="#modalSuccessRegister"
                  >
                    <svg
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        clip-rule="evenodd"
                        d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                        fill="#0061A7"
                      />
                    </svg>
                  </button> */}
                </div>
                <div className="modal-body">
                  <div className="text-center">
                    <h4 className="modal-title mb-3">{ responseMessage}</h4>
                    {loading && (
                      <div
                        className="spinner-border text-primary"
                        style={{ marginLeft: 10 }}
                        role="status"
                        aria-hidden="true"
                      />
                    )}
                    <Lottie
                      options={responseSuccess ? defaultOptions : defaultOptionsFailed}
                      height={300}
                      width={isSmallDevice? 250 : 300}
                      isStopped={false}
                      isPaused={false}
                    />
                    <div className="desc text-center mb-5" style={{fontFamily:'Helvetica', fontWeight:400}}>
                      Silakan periksa email secara berkala untuk
                      <br />
                      melakukan aktivasi akun
                    </div>
                    <button
                      type="button"
                      data-bs-target="#modalSuccessActivation"
                      data-bs-dismiss="modal"
                      style={{width: '100%', background: '#0061a7', border: 'none',borderRadius: 8, padding: 8, color: "white", fontSize: isSmallDevice ? 14 : 16}}
                    >
                      Kembali
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
          <button
            style={{ display: "none" }}
            data-bs-target="#modalSuccessActivation"
            data-bs-toggle="modal"
            ref={btnModalSukses}
          >
            Modal Button
          </button>
        
        </>
      );
}

export default LinkAktivasi;