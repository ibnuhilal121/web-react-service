import axios from 'axios';
import { useEffect, useRef, useState } from 'react'
import { useRouter } from 'next/router'
import BreadcrumbSecondary from '../../components/section/BreadcrumbSecondary'
import ExpiredCode from './ExpiredCode';
import md5 from 'md5';
import * as animationData from "../../public/animate_home.json"
import { encryptPass } from '../../services/auth';
import LoaderOverlay from '../LoaderOverlay';
import Lottie from 'react-lottie';
import { useMediaQuery } from 'react-responsive';
import Link from 'next/link';
import { isInvalid } from '../../utils/validation.register';
import { defaultHeaders } from "../../utils/defaultHeaders";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function ResetPasswordNew(props) {
  const {title,subTitle} = props;
  const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
  const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
  
    const [isLoading, setIsLoading] = useState(false);
    const [loading, setLoading] = useState(false)

    const [isPasswordErr, setIsPasswordErr] = useState(false);
    const [passwordErrMsg, setPasswordErrMsg] = useState('Password minimal 8 karakter, terdiri dari huruf besar, huruf kecil, angka dan karakter khusus (.@$!%*?&)');
    const [isConfirmPasswordErr, setIsConfirmPasswordErr] = useState(false);
    const [confirmPasswordErrMsg, setConfirmPasswordErrMsg] = useState('Konfirmasi Kata Sandi Tidak Cocok');
    const [isExpired, setIsExpired] = useState(false);
    const [passwords, setPasswords] = useState({})
    const [responseSuccess, setResponseSuccess] = useState(true)
    const [responseMessage, setResponseMessage] = useState('')
    const [passwordType1, setPasswordType1] = useState("password");
    const [passwordType2, setPasswordType2] = useState("password");

    const btnModalSukses = useRef();
    const router = useRouter()

    // useEffect(() => {
    //   setIsPasswordErr(false);
    //   setIsConfirmPasswordErr(false);
    // }, [passwords]);

 
    const checkIsExpired = async () => {
      try {
        const payload = {
          Code: `${props.code}`
        }

        let res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/forgotpassword/check`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_App: localStorage.getItem("accessKey"),
            ...defaultHeaders
          },
          body: new URLSearchParams(payload)
        })
        res = await res.json()
        console.log("RESET PASSWORD", res.data);
        console.log(payload);
        setIsExpired(res.IsError ? true : false)

      } catch (error) {
        console.warn("Error reset password check", error);
      }
    }

    const setNoError = () => {
      setIsConfirmPasswordErr(false)
      setIsPasswordErr(false)
    }

    const handleChange = (e) => {
      const value = e.target.value.replace(/[^a-zA-Z0-9 _@.~-]+/, '');
      setPasswords({
        ...passwords,
        [e.target.name]: value
      })
      setIsConfirmPasswordErr(false)

    }
    const handleBlur = (e) => {
      const value = e.target.value;
      if(e.target.name == "password") {
        if(isInvalid("password", value)) {
          console.log("b");
          setPasswordErrMsg('Password minimal 8 karakter, terdiri dari huruf besar, huruf kecil, angka dan karakter khusus (.@$!%*?&)')
          setIsPasswordErr(true);
        } else {
          setIsPasswordErr(false);
        }
      }
    }
    
    const isPasswordMatch = (pass, confirmPass) => {
      return pass === confirmPass ? true : false;
    }

    const handleSubmit = async (e) => {
      try {
        e.preventDefault()
        setNoError()
        if(isInvalid("password", passwords.password)) {
          setPasswordErrMsg('Password minimal 8 karakter, terdiri dari huruf besar, huruf kecil, angka dan karakter khusus (.@$!%*?&)')
          setIsPasswordErr(true)
          return
        }
        const isPassMatch = isPasswordMatch(passwords.password, passwords.confirm_password)
        if(isPassMatch) {
          setIsLoading(true)
          
          const payload = {
            Code: props.code,
            Pwd: encryptPass(passwords.password)
          }
          console.log(payload);
          const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/resetpassword`, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
              AccessKey_App: localStorage.getItem("accessKey"),
              ...defaultHeaders
            },
            body: new URLSearchParams(payload)
          })
          const response = await res.json()
          console.log("res", response);
          if(response.IsError) {
            setPasswordErrMsg(response.ErrToUser)
            setIsPasswordErr(true)
          } else {
            btnModalSukses.current.click()
          }
          setIsLoading(false)
        } else {
          setIsConfirmPasswordErr(true)
        }
        
      } catch (error) {
        console.log(error);
      }
    }

    useEffect(() => {
      checkIsExpired()
    }, [])
 
    const togglePassword1 = () => {
      if (passwordType1 === "password") {
        setPasswordType1("text")
        return;
      }
      setPasswordType1("password")
    }
    const togglePassword2 = () => {
      if (passwordType2 === "password") {
        setPasswordType2("text")
        return;
      }
      setPasswordType2("password")
    }
    console.log(isExpired);
  return (
        <div className="reset-pass-container">
            <div className='reset-pass-flex px-0 px-md-3'>
                <div className="reset-pass-welcome px-0 py-4">
                    <h1 className='reset-pass-welcome-title'>{title}</h1>
                    <p className="reset-pass-welcome-text">{subTitle}</p>
                </div> 
                <div className="reset-pass-content px-0 py-4" style={{alignItems:"center",}}>
                {isExpired ? 
                ////ui kode expired////
                  <div className="p-5 text-center" style={{alignSelf:"center",background:"#fafafa",fontSize:"14px"}}>
                    <Lottie 
                      options={defaultOptions}
                      height={250}
                      width={350}
                      isStopped={false}
                      isPaused={false}
                      style={{minWidth: 250, minHeight: 250}}
                  />
                    <h3>
                      Link sudah kadaluarsa silakan melakukan reset ulang lagi
                    </h3>
                  </div>
                 : 

                  ////form reset password////
                  <form onSubmit={e => handleSubmit(e)}>
                    <div className="reset-pass-input-group" style={{marginBottom: '1.3rem'}}>
                      <div className="floating-label-wrap input-group" id="credit-namalengkap">
                        <input
                          type={passwordType1}
                          className={`floating-label-field step-2-ipond border-active ubah-pass-input ${
                            isPasswordErr ? "error-border" : ""
                          }`}
                          placeholder="Kata Sandi Baru"
                          name='password'
                          onChange={(e) => handleChange(e)}
                          onBlur={(e) => handleBlur(e)}
                          autoComplete="off"
                          // onKeyPress={handlePressEnter}
                          required
                          style={{ color: "#00193E",fontFamily:"Helvetica" }}
                        />
                        <label className="floating-label">Kata Sandi Baru </label>
                        <div className="eye-password" onClick={togglePassword1}>
                            {passwordType1 === "password" ?
                              <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                              <img src="/images/icons/eye.svg" width="20" height="20" />
                            } 
                        </div>
                      </div>
                        {isPasswordErr && (
                        <div className="error-input-kpr">
                        <i
                          class="bi bi-exclamation-circle-fill"
                          style={{ marginRight: "5px" }}
                        ></i>
                        {passwordErrMsg}
                    </div>
                          )}
                    </div> 
                    <div className="reset-pass-input-group" style={{marginBottom: '1rem'}}>
                      <div className="floating-label-wrap input-group" id="credit-namalengkap">
                          <input
                            type={passwordType2}
                            className={`floating-label-field step-2-ipond border-active ubah-pass-input ${
                              isPasswordErr ? "error-border" : ""
                            }`}
                            placeholder="Konfirmasi Kata Sandi Baru"
                            name='confirm_password'
                            onChange={(e) => handleChange(e)}
                            autoComplete="off"
                            // onKeyPress={handlePressEnter}
                            required
                            style={{ color: "#00193E",fontFamily:"Helvetica" }}
                          />
                          <label className="floating-label">Konfirmasi Kata Sandi Baru </label>
                          <div className="eye-password" onClick={togglePassword2}>
                            {passwordType2 === "password" ?
                              <img src="/images/icons/eye-slash.svg" width="20" height="20" /> :
                              <img src="/images/icons/eye.svg" width="20" height="20" />
                            } 
                        </div>
                        </div>
                        {isConfirmPasswordErr && (
                        <div className="error-input-kpr">
                        <i
                          class="bi bi-exclamation-circle-fill"
                          style={{ marginRight: "5px" }}
                        ></i>
                       {confirmPasswordErrMsg}
                      </div>
                      )}
                    </div>
                    

                <button type='submit' className="reset-pass-content-btn">Simpan Perubahan</button>
                </form>
                }
            </div>
        </div>

        <div
            className="modal fade m-atas"
            id="modalSuccessActivation"
            aria-hidden="true"
            aria-labelledby="modalSuccessActivation"
            tabIndex="-1"
          >
            <div className="modal-dialog" style={{padding: 0}}>
              <div className="modal-content" 
              style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)', width: "95%" } : { padding: '0 3rem 3rem'}}
              // style={{ width: "600px", height: "576px", borderRadius: "8px" }}
              >
                <div className="modal-header border-bottom-0">
                </div>
                <div className="modal-body">
                  <div className="text-center">
                    <h4 className="modal-title mb-3">Reset Password Berhasil</h4>
                    {loading && (
                      <div
                        className="spinner-border text-primary"
                        style={{ marginLeft: 10 }}
                        role="status"
                        aria-hidden="true"
                      />
                    )}
                    <Lottie
                      options={responseSuccess ? defaultOptions : defaultOptionsFailed}
                      height={isSmallDevice ? 150: 250}
                      width={isSmallDevice? 250 : 300}
                      isStopped={false}
                      isPaused={false}
                    />
                    <div className="desc text-center mb-5" style={{fontFamily:'Helvetica', fontWeight:400}}>
                      Silakan periksa email secara berkala untuk
                      <br />
                      melakukan aktivasi akun
                    </div>
                      <button
                        type="button"
                        data-bs-target="#modalSuccessActivation"
                        data-bs-dismiss="modal"
                        onClick={() => router.push('/')}
                        style={{width: '100%', background: '#0061a7', border: 'none',borderRadius: 8, padding: 8, color: "white", fontSize: isSmallDevice ? 14 : 16}}
                      >
                        Beranda
                      </button>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
    
          <button
            style={{ display: "none" }}
            data-bs-target="#modalSuccessActivation"
            data-bs-toggle="modal"
            ref={btnModalSukses}
          >
            Modal Button
          </button>

        <LoaderOverlay open={isLoading} />
    </div>
  )
}
