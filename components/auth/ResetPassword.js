import { useEffect, useRef, useState } from "react"
import axios from 'axios';
import qs from 'qs';
import Lottie from "react-lottie";
import { doSendForgotPassword } from "../../services/memberArea";
import checkIsValidEmail from "../../utils/checkIsValidEmail";
import { isInvalid } from "../../utils/validation.register"
import { useAppContext } from "../../context";
import * as animationData from "../../public/succes_animation.json"
import * as animationDataFailed from "../../public/x_animation.json"
import { useMediaQuery } from "react-responsive";

const defMailMessage = "Please insert valid email";
const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
const defaultOptionsFailed = {
  loop: true,
  autoplay: true,
  animationData: animationDataFailed,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function ResetPassword() {
  const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
  const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });

  const { userKey } = useAppContext();

  const [loading, setLoading] = useState(false)
  const [inputVal, setInputVal] = useState('');
  const [email, setEmail] = useState(null)
  const [isMailValid, setIsMailValid] = useState(true)
  const [errorRequiredMail, setErrorRequiredMail] = useState(false)
  const [mailErrorMessage, setMailErrorMessage] = useState(defMailMessage)
  const [emailErrorMessage, setEmailErrorMessage] = useState('');
  const [responseMessage, setResponseMessage] = useState('');
  const [responseStatus, setResponseStatus] = useState(true);
  const [isEmailValid, setEmailValid] = useState(true)
  const [isDisabled, setDisabled] = useState(true)
  const [isToggleSubmit, setToggleSubmit] = useState(false);

  const [counter, setCounter] = useState(0)


  const btnModalSukses = useRef();
  const btnCloseModalForgotPwd = useRef();

  const handleCountDown = (sec) => {
    let hours = Math.floor(sec / 3600);
    let divisorForMinutes = sec % 3600;
    let minutes = Math.floor(divisorForMinutes / 60);
    let divisorForSeconds = sec % 60;
    let seconds = Math.ceil(divisorForSeconds);

    return {
        hours,
        minutes,
        seconds
    }
  }

  const { hours, minutes, seconds } = handleCountDown(counter)


  // HANDLER FOR STATE
  function loadingHandler(bool) {
    setLoading(bool);
  }
  // VALIDASI AFTER KLIK TOMBOL REGISTER
  async function validateForm() {
    let errorCount = 0;
    if (email === null || email === '') {
      // console.log("+++ email error")
      setErrorRequiredMail(true)
      errorCount += 1;
    }
    if (!isMailValid) {
      // console.log("+++ isMailValid error")
      errorCount += 1;
    }
    return (errorCount);
  }

  useEffect(() => {
    if(sessionStorage.getItem("FORGOT_PASS_COUNTER")) {
      setCounter(sessionStorage.getItem("FORGOT_PASS_COUNTER"))
    }
    
  }, [])

  useEffect(() => {
    if (counter > 0) {
      setTimeout(() => {
        setDisabled(true)
        setCounter(counter-1)
        window.sessionStorage.setItem("FORGOT_PASS_COUNTER", counter - 1);

      }, 1000)
    } else {
      window.sessionStorage.removeItem("FORGOT_PASS_COUNTER");
      setDisabled(false)
    }
  }, [counter])

  const handleSubmit = async (e) => {
    e.preventDefault()
    if(window.sessionStorage.getItem("FORGOT_PASS_LIMIT") == 3) {
      setCounter(300)
      window.sessionStorage.removeItem("FORGOT_PASS_LIMIT")
    } else {
      const res = await doSendForgotPassword(loadingHandler, {e: email})
      if( !res.IsError ) {
        setInputVal('')
        setResponseStatus(true)
        setResponseMessage(res.Output)
        window.sessionStorage.setItem("FORGOT_PASS_LIMIT", window.sessionStorage.getItem("FORGOT_PASS_LIMIT") ? parseInt(window.sessionStorage.getItem("FORGOT_PASS_LIMIT")) + 1 : 1)
        btnCloseModalForgotPwd.current.click()
        btnModalSukses.current.click()
      } else {
        setInputVal('')
        setResponseStatus(false)
        setResponseMessage(res.ErrToUser)
        btnCloseModalForgotPwd.current.click()
        btnModalSukses.current.click()
      }
    }
    
  }

  // const handleSubmit = async (event) => {
  //   event.preventDefault();
  //   setLoading(true)
  //   setToggleSubmit(prev => !prev)
  //   // console.log('+++ formState', formState);
  //   const errorCounter = await validateForm();
  //   // console.log('+++ errorCounter', errorCounter);
  //   if(errorCounter < 1){
  //       const payloadEmailForgot = {
  //           e: email,
  //       };
  //       doSendForgotPassword(loadingHandler, payloadEmailForgot)
  //       .then((responseReset) => {
  //       //   console.log("+++ response", response);
  //         if (!responseReset?.IsError) {
  //             console.log(responseReset.Output);
  //           // btnModalSukses.current.modal('show');
  //         }else{
  //           console.log(responseReset.ErrToUser);
  //         }
  //       }).catch((err) => {
  //         console.log(`Something wrong:${err}`);
  //         loadingHandler(false);
  //       });
  //   }
  // }

  // const handleKeyDown = (e) => {
  //   const value = e.target.value
  //   const RFC5322 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  //   if (e.key === "Backspace") {
  //     if (!value) {
  //       setEmailErrorMessage('Isian tidak boleh kosong')
  //       return setEmailValid(false)
  //     }
  //     if (value && !RFC5322.test(value)) {
  //       setEmailErrorMessage('Isi dengan alamat email')
  //       return setEmailValid(false)
  //     }
  //     if (!isEmailValid) setEmailValid(true)
  //   }
  // }

  const handleChange = (e) => {
    setInputVal(e.target.value.replace(/[^a-zA-Z0-9 _@.~-]+/, '').trim())
  }

  const handleBlur = (e) => {
    const value = inputVal;

    if (!value) {
      setDisabled(true)
      setEmailErrorMessage('Isian tidak boleh kosong')
      return setEmailValid(false)}
    if (value && isInvalid("email", value)) {
      setDisabled(true)
      setEmailErrorMessage('Isi dengan alamat email')
      return setEmailValid(false)}
    if (!isEmailValid) setEmailValid(true)

    setEmail(inputVal)
    counter <= 0 && setDisabled(false)
  }

  useEffect(() => {
    
  }, [])

  useEffect(() => {
    if (email && isEmailValid) {
      doSendForgotPassword(loadingHandler, { e: email })
        .then(res => {
          if (res.IsError) {
            setEmailErrorMessage(res.ErrToUser)
            return setEmailValid(false)
          }
          btnCloseModalForgotPwd.current.click()
          btnModalSukses.current.click()
        })
        .catch(err => console.log(err.message))
    }
  }, [isToggleSubmit])

  return (
    <>
      <div 
        className="modal fade m-atas" 
        id="modalResetPassword" 
        aria-hidden="true" 
        aria-labelledby="modalResetPassword" 
        tabIndex="-1"
      >
        <div 
          className="modal-dialog modal-auth" 
          style={isSmallDevice || isMediumDevice ? { margin: 0, paddingBottom: 0, height: 'calc(100% - 70px)', overflowY: 'auto' } : { paddingBottom: 0 }}
        >
          <div 
            className="modal-content" 
            id="resetpasspopup"
            style={isSmallDevice || isMediumDevice ? { height: '100%' } : {}}
          >
            <div className="modal-header">
              <img src="/images/bg/bg_icon.svg" className="acc img-fluid" />
              <div className="text-center">
                <h5 className="modal-title" id="exampleModalLabel">Reset Password</h5>
                <div className="desc">
                  Kami akan mengirim link untuk melakukan penggantian password melalui email kamu yang sudah terdaftar.
                </div>
              </div>

              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                data-bs-target="#modalResetPassword"
                ref={btnCloseModalForgotPwd}
              >
                <i className="bi bi-x-lg"></i>
              </button>
            </div>
            <div className="modal-body" style={{ paddingTop: 62, borderRadius: '16px', paddingBottom: 0 }}>
              <div className="modal-form">
                <form className="form-validation" id="form-ResetPassword">
                  <div className="floating-label-wrap " style={{ height: 48 }}>
                    <input type="email" className="floating-label-field" id="login_email" placeholder="Email" required style={{ height: 48, border: isEmailValid ? '1px solid #aaaaaa' : '1px solid red' }}
                      onChange={(event) => {
                        handleChange(event)
                        // const {value} = event.target;
                        // const isValidEmail = checkIsValidEmail(value)
                        // setIsMailValid(isValidEmail);
                        // if(!isValidEmail){
                        //     setMailErrorMessage(defMailMessage);
                        // }
                        // setEmail(value);
                        // if(value){
                        //     setErrorRequiredMail(false)
                        // }
                      }}
                      onBlur={handleBlur}
                      // onKeyDown={handleKeyDown}
                      value={inputVal}
                    />
                    <label htmlFor="login_email" className="floating-label">Email</label>
                    <span className="error-input e001" style={{ opacity: isEmailValid ? 0 : 1 }}>
                      <i class="bi bi-exclamation-circle-fill" style={{ marginRight: "5px" }} />
                      {emailErrorMessage}
                    </span>
                    {/* {errorRequiredMail && <div  className="error-input">Required!</div>}
                                        {!isMailValid && <div  className="error-input">{mailErrorMessage}</div>} */}
                  </div>

                  <div className="d-grid gap-2 reset-button" style={{ marginBottom: 0, paddingBottom: 80 }}>
                  {counter > 0 && (<div style={{textAlign: "center"}}><span style={{fontSize: 16, textAlign: "center"}}>Coba lagi dalam</span><span className="countdown font-FuturaHvBt"> {minutes < 10 && '0'}{minutes}:{seconds < 10 && '0'}{seconds}</span></div>)}
                    <button
                      type="submit"
                      disabled={isDisabled}
                      onClick={handleSubmit}
                      className="btn btn-main btn-block "
                      style={{ height: 480, height: 48, fontFamily: 'Helvetica', fontWeight: 700 }}
                    >
                      Kirim
                      {loading && (<div className="spinner-border spinner-border-sm text-primary" style={{ marginLeft: 10 }} role="status" aria-hidden="true" />)}
                    </button>
                  </div>

                  {/* return (
    <div
      className="modal fade"
      id="modalResetPassword"
      aria-hidden="true"
      aria-labelledby="modalResetPassword"
      tabIndex="-1"
    >
      <div className="modal-dialog modal-auth" style={{ paddingBottom: 0 }}>
        <div className="modal-content">
          <div className="modal-header">
            <img src="/images/bg/bg_icon.svg" className="acc img-fluid" />
            <div className="text-center">
              <h5 className="modal-title" id="exampleModalLabel">
                Reset Password
              </h5>
              <div className="desc">
                Kami akan mengirim link untuk melakukan penggantian password
                melalui email kamu yang sudah terdaftar.
              </div>
            </div>

            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <i className="bi bi-x-lg"></i>
            </button>
          </div>
          <div
            className="modal-body"
            style={{ paddingTop: 62, borderRadius: "16px", paddingBottom: 0 }}
          >
            <div className="modal-form">
              <form className="form-validation" id="form-ResetPassword">
                <div className="floating-label-wrap " style={{ height: 48 }}>
                  <input
                    type="email"
                    className="floating-label-field"
                    id="login_email"
                    placeholder="Email"
                    required
                    style={{ height: 48 }}
                    onChange={(event) => {
                      const { value } = event.target;
                      const isValidEmail = checkIsValidEmail(value);
                      setIsMailValid(isValidEmail);
                      if (!isValidEmail) {
                        setMailErrorMessage(defMailMessage);
                      }
                      setEmail(value);
                      if (value) {
                        setErrorRequiredMail(false);
                      }
                    }}
                  />
                  <label htmlFor="login_email" className="floating-label">
                    Email
                  </label>
                  {errorRequiredMail && (
                    <div className="error-input">Required!</div>
                  )}
                  {!isMailValid && (
                    <div className="error-input">{mailErrorMessage}</div>
                  )}
                </div>

                <div
                  className="d-grid gap-2"
                  style={{ marginBottom: 0, marginTop: 337, paddingBottom: 80 }}
                >
                  <button
                    type="submit"
                    onClick={handleSubmit}
                    className="btn btn-main btn-block"
                    style={{
                      height: 480,
                      height: 48,
                      fontFamily: "Helvetica",
                      fontWeight: 700,
                    }}
                  >
                    Masuk Sekarang
                    {loading && (
                      <div
                        className="spinner-border spinner-border-sm text-primary"
                        style={{ marginLeft: 10 }}
                        role="status"
                        aria-hidden="true"
                      />
                    )}
                  </button>
                </div> */}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="modal fade"
        id="modalSuccessReset"
        aria-hidden="true"
        aria-labelledby="modalSuccessReset"
        tabIndex="-1"
      >
        <div className="modal-dialog">
          <div className="modal-content" 
          style={isSmallDevice || isMediumDevice ? { margin: 0, height: 'calc(100% - 70px)' } : { padding: '0 3rem 3rem'}}
          
          >
            <div className="modal-header border-bottom-0">
            {/* <button
                type="button"
                style={{ background: "none", border: "none", marginLeft:'535px' }}
                data-bs-dismiss="modal"
                aria-label="Close"
                data-bs-target="#modalSuccessRegister"
              >
                <svg
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                    fill="#0061A7"
                  />
                </svg>
              </button> */}
            </div>
            <div className="modal-body">
              <div className="text-center">
                <h4 className="modal-title mb-3">{responseMessage}</h4>
                {loading && (
                  <div
                    className="spinner-border text-primary"
                    style={{ marginLeft: 10 }}
                    role="status"
                    aria-hidden="true"
                  />
                )}
                <Lottie
                  options={responseStatus ? defaultOptions : defaultOptionsFailed}
                  height={350}
                  width={350}
                  isStopped={false}
                  isPaused={false}
                  style={responseStatus ? {} : {marginBottom: '1rem'}}
                />
                <div className="desc text-center mb-5" style={{fontFamily:'Helvetica', fontWeight:400}}>
                  Silakan periksa email secara berkala untuk
                  <br />
                  menyetel ulang password
                </div>
                <button
                      type="button"
                      data-bs-target="#modalSuccessReset"
                      data-bs-dismiss="modal"
                      style={{width: '100%', background: '#0061a7', border: 'none',borderRadius: 8, padding: 8, color: "white", fontSize: isSmallDevice ? 14 : 16}}
                    >
                      Kembali
                    </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <button
        style={{ display: "none" }}
        data-bs-target="#modalSuccessReset"
        data-bs-toggle="modal"
        ref={btnModalSukses}
      >
        Modal Button
      </button>
    </>
  );
}
