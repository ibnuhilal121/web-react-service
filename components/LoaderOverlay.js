import React from "react";

const LoaderOverlay = ({ open }) => {
    return open ? (
        <div
            style={{
                position: "fixed",
                width: "100%",
                top: 0,
                left: 0,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
                backgroundColor: "rgba(0, 25, 62, 0.5)",
                zIndex: 1200, // tambahi bila kurang tinggi
            }}
        >
            <img alt="..." style={{ height: 75 }} src="/images/spinner.gif" />
        </div>
    ) : null;
};

export default LoaderOverlay;
