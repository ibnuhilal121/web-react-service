import {
    LoadScriptNext,
    GoogleMap,
    InfoWindow,
    Marker,
    Polyline,
    useGoogleMap,
    InfoBox,
    OverlayView,
} from "@react-google-maps/api";
import React, { useCallback, useState } from "react";
import { useMediaQuery } from "react-responsive";
import { useEffect } from "react";
import styles from "./googleMaps.module.scss";
import _ from "lodash";
import NumberFormat from "react-number-format";

const defPosition = {
    lat: -6.2088,
    lng: 106.8456,
};

export default function GoogleMaps({ position = defPosition, markers, mapCenter = defPosition, userLoc }) {
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` })
    const [center, setCenter] = useState(defPosition);
    const [openedId, setOpenedId] = useState(null);
    const [selectedCard, setSelectedCard] = useState(null);

    useEffect(() => {
        // console.log("~ position", position);
    }, [position]);
    useEffect(() => {
        console.log("~ markers", markers);
    //     if (markers) {
    //         setCenter({
    //             lat: _.mean(markers.filter((val) => val.lat != 0 && val.lat != null).map((val) => val.lat)),
    //             lng: _.mean(markers.filter((val) => val.lng != 0 && val.lng != null).map((val) => val.lng)),
    //         });
    //     } else {
    //         setCenter(defPosition);
    //     }
    }, [markers]);
    useEffect(() => {
        if (mapCenter?.lat && mapCenter?.lng) {
            setCenter(mapCenter);
        } else {
            setCenter(defPosition);
        }
    }, [mapCenter]);
    function handleClick(id) {
        // console.log("~ id", id);
        setOpenedId(openedId == id ? null : id);
    }

    const selected = markers?.find((val) => openedId == val.id);
    return (
        <div className={styles.mapsContainer}>
            <LoadScriptNext googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY} libraries={["places"]}>
                <GoogleMap
                    id="circle-example"
                    mapContainerStyle={{
                        width: "100%",
                        height: "100%",
                    }}
                    center={center}
                    zoom={markers ? 5 : 10}
                    options={{
                        streetViewControl: false,
                        draggable: true, // make map draggable
                        keyboardShortcuts: false, // disable keyboard shortcuts
                        scaleControl: true, // allow scale controle
                        scrollwheel: false, // allow scroll wheel
                    }}
                >
                    {markers ? 
                    (   !isTabletOrMobile ?
                        markers.map((val) => (
                            <>
                                <OverlayView
                                    position={{ lat: val.lat, lng: val.lng }}
                                    mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                                >
                                    <div>
                                        <div
                                            className={`${styles.overlayInner} ${
                                                openedId == val.id && styles.overlayInnerBlue
                                            }`}
                                            onClick={() => handleClick(val.id)}
                                        >
                                            <NumberFormat
                                                value={
                                                    val.priceString >= 1000000000
                                                        ? (val.priceString * 1) / 1000000000
                                                        : (val.priceString * 1) / 1000000
                                                }
                                                decimalSeparator=","
                                                thousandSeparator="."
                                                prefix="Rp"
                                                displayType="text"
                                                decimalScale={2}
                                            />
                                            {val.priceString >= 1000000000 ? " M" : " JT"}
                                        </div>
                                        
                                        { openedId == val.id && (
                                                <div
                                                    style={{
                                                        position: "absolute",
                                                        width: "400px",
                                                        left: "100%",
                                                        top: "50%",
                                                        transform: "translateY(-50%)",
                                                        zIndex: 1,
                                                    }}
                                                >
                                                    {selected?.card}
                                                </div>
                                            )
                                        }
                                    </div>
                                    
                                </OverlayView>
                            </>
                        )) :
                        markers.map((val) => (
                            <>
                                <OverlayView
                                    position={{ lat: val.lat, lng: val.lng }}
                                    mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                                >
                                    <div>
                                        <div
                                            className={`${styles.overlayInner} ${
                                                openedId == val.id && styles.overlayInnerBlue
                                            }`}
                                            onClick={() => handleClick(val.id)}
                                        >
                                            <NumberFormat
                                                value={
                                                    val.priceString >= 1000000000
                                                        ? (val.priceString * 1) / 1000000000
                                                        : (val.priceString * 1) / 1000000
                                                }
                                                decimalSeparator=","
                                                thousandSeparator="."
                                                prefix="Rp"
                                                displayType="text"
                                                decimalScale={2}
                                            />
                                            {val.priceString >= 1000000000 ? " M" : " JT"}
                                        </div>
                                        
                                        {/* { !isTabletOrMobile ?  
                                            openedId == val.id && (
                                                <div
                                                    style={{
                                                        position: "absolute",
                                                        width: "400px",
                                                        left: "100%",
                                                        top: "50%",
                                                        transform: "translateY(-50%)",
                                                        zIndex: 1,
                                                    }}
                                                >
                                                    {selected?.card}
                                                </div>
                                            ) :
                                            openedId == val.id && (
                                                <div
                                                    style={{
                                                        position: "absolute",
                                                        minWidth: "100%",
                                                        // transform: "translateY(-50%)",
                                                        // bottom: "0",
                                                        // zIndex: 1,
                                                    }}
                                                >
                                                    {selected?.card}
                                                </div>
                                            )
                                        } */}
                                    </div>
                                    
                                </OverlayView>
                                {   openedId == val.id && (
                                        <div
                                            style={{
                                                position: "absolute",
                                                minWidth: "100%",
                                                // transform: "translateY(-50%)",
                                                bottom: "0",
                                                zIndex: 1,
                                            }}
                                        >
                                            {selected?.card}
                                        </div>
                                )}
                            </>
                        ))
                    ) : (
                        <Marker position={position}></Marker>
                    )}
                    
                    {userLoc?.lat && userLoc?.lng && <Marker position={userLoc}></Marker>}
                </GoogleMap>
            </LoadScriptNext>
        </div>
    );
}
