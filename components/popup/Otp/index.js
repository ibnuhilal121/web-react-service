import React, { useEffect, useRef, useState } from 'react'
import { useMediaQuery } from "react-responsive";
import OtpInput from 'react-otp-input';
import { handleCountDown } from '../../../helpers/countdown';
import axios from 'axios';

function index({submitCredit, setLoadingStep}) {
    const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
    const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
    const [loading, setLoading] = useState(false)
    const modalCloseRef = useRef(null)
    const [otp, setOtp] = useState("")
    const [isError, setIsError] = useState(false)
    const [time, setTime] = useState(75)

    useEffect(() => {
      if(time > 0) {
        setTimeout(() => {
          setTime(time -1)
        }, 1000)
      }
      
    }, [time])
    
    const { hours, minutes, seconds } = handleCountDown(time)

    const verifyOtp = async () => {
      setLoadingStep(true)
      try {
        const payload = {
          otp
        }
        const { data } = await axios(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
          },
          body: new URLSearchParams(payload),
        })

        if(data.status) {
          submitCredit()
        }
        // setLoadingStep(false)
      } catch (error) {
        submitCredit()
        console.log(error);
        modalCloseRef.current.click()
        // setLoadingStep(false)
      }
    }
    

  return (
    <>
      <div class="modal fade" id="modalOtp" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog popup-content">
          <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
            
            <div class="popup-body" >
            <div className="text-center">
                    <h4 className="modal-title mb-3 font-Futura popup-title">Verifikasi OTP</h4>
                    
                    <img src="/icons/icons/phone-message.svg" alt="gambar handphone" style={{marginBottom: 39, marginRight: -36}} />

                    <span className="countdown font-FuturaHvBt">{minutes < 10 && '0'}{minutes}:{seconds < 10 && '0'}{seconds}</span>
                    <p className="popup-description">
                      Kode OTP telah dikirimkan melalui SMS ke <br />
                      +6282 9876 543222
                    </p>

                    <OtpInput
                      value={otp}
                      onChange={(otp) => setOtp(otp)}
                      numInputs={6}
                      separator={<span style={{width: 20}}></span>}
                      containerStyle={{
                        justifyContent: 'center',
                        marginBottom: 26
                      }}
                      inputStyle={{
                        border: 'none',
                        borderBottom: isError ? '1px solid rgba(222, 41, 37, 1)' : '1px solid black',
                        width: isSmallDevice ? 25 : 30,
                        height: isSmallDevice ? 30 : 40
                      }}
                    />

                    { isError &&
                      <p className="text-error">
                        Kode OTP tidak sesuai
                      </p>
                    }

                    <p className="desc text-center mb-5" style={{fontFamily:"Helvetica", fontWeight:400}}>
                      Tidak menerima kode OTP? <span className="kirim-ulang">Kirim Ulang</span> 
                    </p>
                    
                    <button
                      type="button"
                      // data-bs-target="#modalOtp"
                      // data-bs-dismiss="modal"
                      className='popup-btn font-FuturaHvBt'
                      onClick={() => verifyOtp()}
                      style={{fontSize: isSmallDevice ? 12 : 14}}
                    >
                      Verifikasi
                    </button>
                  </div>
            </div>
          </div>
        </div>
        <button
        type='button'
        data-bs-target="#modalOtp"
        data-bs-dismiss="modal"
        style={{display: 'none'}}
        ref={modalCloseRef}
        ></button>
      </div>
    </>
  )
}

export default index