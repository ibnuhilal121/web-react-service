import React from 'react'
import Lottie from "react-lottie";
import * as animationData from "../../../public/animate_home.json"
import * as xAnimation from "../../../public/x_animation.json"
import { useMediaQuery } from "react-responsive";
import { useRouter } from 'next/router';
import { useAppContext } from '../../../context';


const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
    },
};
const errorOptions = {
    loop: true,
    autoplay: true,
    animationData: xAnimation,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
    },
};

const Alert = () => {
  return (
    <div class="alert alert-danger" role="alert">
      A simple danger alert—check it out!
    </div>
  )
}


function index({isSuccess, downloadPDF, url, errorRegister}) {
  const {userProfile} = useAppContext();
    const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
    const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
    const router = useRouter()
    const responseHandler = (id) => {
      switch(id) {
        case -6:
              return (
                <>
                  <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog popup-content bottom-rounded">
                      <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                        
                        <div class="popup-body" >
                        <div className="text-center">
                                <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                                <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                                Terjadi kesalahan saat Tanda tangan Form Aplikasi Kredit<br></br> Silahkan ulangi sekali lagi.
                                </p>
          
                                <div style={{marginBottom: 100, marginTop: 50}}>
                                  <Lottie
                                    options={defaultOptions}
                                    width={isSmallDevice? 250: 328}
                                    isStopped={false}
                                    isPaused={false}
                                  />
                                </div>
          
                                <button
                                  type="button"
                                  data-bs-target="#modalGeneratePdf"
                                  data-bs-dismiss="modal"
                                  className='popup-btn font-Futura'
                                  style={{fontSize: isSmallDevice ? 12 : 14}}
                                  // onClick={() => router.push('/')}
                                >
                                  OK
                                </button>
                              </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              );
        case -5:
              return (
                <>
                  <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog popup-content bottom-rounded">
                      <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                        
                        <div class="popup-body" >
                        <div className="text-center">
                                <h4 className="modal-title mb-3 font-Futura popup-title">Biodata Tidak Sesuai</h4>
                                <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                                Pastikan anda mengisi biodata di profil dengan benar.
                                </p>
          
                                <div style={{marginBottom: 100, marginTop: 50}}>
                                  <Lottie
                                    options={defaultOptions}
                                    width={isSmallDevice? 250: 328}
                                    isStopped={false}
                                    isPaused={false}
                                  />
                                </div>
          
                                <button
                                  type="button"
                                  data-bs-target="#modalGeneratePdf"
                                  data-bs-dismiss="modal"
                                  className='popup-btn font-Futura'
                                  style={{fontSize: isSmallDevice ? 12 : 14}}
                                  // onClick={() => router.push('/')}
                                >
                                  Tutup
                                </button>
                              </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              );
        case -4:
            return (
              <>
                <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog popup-content bottom-rounded">
                    <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                      
                      <div class="popup-body" >
                      <div className="text-center">
                              <h4 className="modal-title mb-3 font-Futura popup-title">Nomor KTP tidak ditemukan</h4>
                              <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                              Pastikan anda mengisi nomor KTP dengan benar.
                              </p>
        
                              <div style={{marginBottom: 100, marginTop: 50}}>
                                <Lottie
                                  options={defaultOptions}
                                  width={isSmallDevice? 250: 328}
                                  isStopped={false}
                                  isPaused={false}
                                />
                              </div>
        
                              <button
                                type="button"
                                data-bs-target="#modalGeneratePdf"
                                data-bs-dismiss="modal"
                                className='popup-btn font-Futura'
                                style={{fontSize: isSmallDevice ? 12 : 14}}
                                // onClick={() => router.push('/')}
                              >
                                Tutup
                              </button>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
        case -3:
            return (
              <>
                <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog popup-content bottom-rounded">
                    <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                    <button
                      style={{position: 'absolute', right: 10, top: 15, borderColor: "#C4C4C4"}}
                          type="button"
                          className="btn-close"
                          data-bs-target="#modalGeneratePdf"
                          data-bs-dismiss="modal"
                          >
                    </button>

                      <div class="popup-body" >
                      <div className="text-center">
                              <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                              <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                              Silakan melakukan tanda tangan dan masukkan OTP yang sudah kami kirimkan ke email{" "+ userProfile?.email}
                              </p>
        
                              <div style={{marginBottom: 100, marginTop: 50}}>
                                <Lottie
                                  options={defaultOptions}
                                  width={isSmallDevice? 250: 328}
                                  isStopped={false}
                                  isPaused={false}
                                />
                              </div>
        
                              <a href={url} target="_blank">
                              <button
                                type="button"
                                // data-bs-target="#modalGeneratePdf"
                                // data-bs-dismiss="modal"
                                className='popup-btn font-Futura'
                                style={{fontSize: isSmallDevice ? 12 : 14}}
                                // onClick={() => router.push('/')}
                              >
                                Lanjut ke Tanda tangan Form Aplikasi
                              </button>
                              </a>
                              
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
        case -2:
            return (
              <>
                <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog popup-content bottom-rounded">
                    <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                      
                      <div class="popup-body" >
                      <div className="text-center">
                              <h4 className="modal-title mb-3 font-Futura popup-title">Anda Belum Melakukan Tanda Tangan</h4>
                              <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                              Silakan melakukan tanda tangan digital melalui email.
                              </p>
        
                              <div style={{marginBottom: 100, marginTop: 50}}>
                                <Lottie
                                  options={defaultOptions}
                                  width={isSmallDevice? 250: 328}
                                  isStopped={false}
                                  isPaused={false}
                                />
                              </div>
        
                              <button
                                type="button"
                                data-bs-target="#modalGeneratePdf"
                                data-bs-dismiss="modal"
                                className='popup-btn font-Futura'
                                style={{fontSize: isSmallDevice ? 12 : 14}}
                                // onClick={() => router.push('/')}
                              >
                                Tutup
                              </button>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
        case -1:
            return (
              <>
                <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog popup-content bottom-rounded">
                    <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                      
                      <div class="popup-body" >
                      <div className="text-center">
                              <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf </h4>
                              <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                              Terjadi kesalahan saat generate PDF, silakan ulangi sekali lagi
                              </p>
        
                              <div style={{marginBottom: 100, marginTop: 50}}>
                                <Lottie
                                  options={defaultOptions}
                                  width={isSmallDevice? 250: 328}
                                  isStopped={false}
                                  isPaused={false}
                                />
                              </div>
        
                              <button
                                type="button"
                                data-bs-target="#modalGeneratePdf"
                                data-bs-dismiss="modal"
                                className='popup-btn font-Futura'
                                style={{fontSize: isSmallDevice ? 12 : 14}}
                                // onClick={() => router.push('/')}
                              >
                                Tutup
                              </button>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
        case 0:
            return (
              <>
                <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog popup-content bottom-rounded">
                    <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                      
                      <div class="popup-body" >
                      <div className="text-center">
                              <h4 className="modal-title mb-3 font-Futura popup-title">Kamu belum melakukan aktivasi sertifikat digital.</h4>
                              <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                              Silakan melakukan aktivasi sertifikat digital yang sudah dikirim ke email {userProfile?.email}.
                              </p>
        
                              <div style={{marginBottom: 100, marginTop: 50}}>
                                <Lottie
                                  options={defaultOptions}
                                  width={isSmallDevice? 250: 328}
                                  isStopped={false}
                                  isPaused={false}
                                />
                              </div>
        
                              <button
                                type="button"
                                data-bs-target="#modalGeneratePdf"
                                data-bs-dismiss="modal"
                                className='popup-btn font-Futura'
                                style={{fontSize: isSmallDevice ? 12 : 14}}
                                // onClick={() => router.push('/')}
                              >
                                OK
                              </button>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
        case 1:
          return (
            <>
              <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog popup-content bottom-rounded">
                  <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                    
                    <div class="popup-body" >
                    <button 
                      type="button"
                      data-bs-target="#modalGeneratePdf"
                      data-bs-dismiss="modal"
                      class="btn-close position-absolute" 
                      style={{right: 10, top: 10}}
                       aria-label="Close"
                       >
                      </button>


                    <div className="text-center">
                            <h4 className="modal-title mb-3 font-Futura popup-title">Terima Kasih</h4>
                            <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                            Kami sudah mengirimkan kode OTP melalui email.
                            Silakan melakukan verifikasi melalui tombol dibawah.
                            </p>
      
                            <div style={{marginBottom: 100, marginTop: 50}}>
                              <Lottie
                                options={defaultOptions}
                                width={isSmallDevice? 250: 328}
                                isStopped={false}
                                isPaused={false}
                              />
                            </div>
                            <a href={url} target="_blank">
                            <button
                              type="button"
                              className='popup-btn font-Futura'
                              style={{fontSize: isSmallDevice ? 12 : 14}}
                            >
                              Verifikasi
                            </button>
                            </a>
                          </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        case 2:
          return (
            <>
              <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog popup-content bottom-rounded">
                  <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                    
                    <div class="popup-body" >
                    <div className="text-center">

                            <h4 className="modal-title mb-3 font-Futura popup-title">Dokumen Kamu Belum Siap</h4>
                            <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                            Dokumen belum siap. Pastikan kamu telah memasukkan OTP yang didapat melalui email {userProfile?.email} untuk penandatanganan Form Aplikasi Kredit.
                            </p>
      
                            <div style={{marginBottom: 100, marginTop: 50}}>
                              <Lottie
                                options={defaultOptions}
                                width={isSmallDevice? 250: 328}
                                isStopped={false}
                                isPaused={false}
                              />
                            </div>
      
                            <button
                              type="button"
                              data-bs-target="#modalGeneratePdf"
                              data-bs-dismiss="modal"
                              className='popup-btn font-Futura'
                              style={{fontSize: isSmallDevice ? 12 : 14}}
                              // onClick={downloadPDF}
                            >
                              OK
                            </button>
                          </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        case 3:
          return (
            <>
              <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog popup-content bottom-rounded">
                  <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                    
                    <div class="popup-body" >
                    <div className="text-center">

                            <h4 className="modal-title mb-3 font-Futura popup-title">Document Sedang di Proses</h4>
                            <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                            Silakan tunggu beberapa saat lagi.
                            </p>
      
                            <div style={{marginBottom: 100, marginTop: 50}}>
                              <Lottie
                                options={defaultOptions}
                                width={isSmallDevice? 250: 328}
                                isStopped={false}
                                isPaused={false}
                              />
                            </div>
      
                            <button
                              type="button"
                              data-bs-target="#modalGeneratePdf"
                              data-bs-dismiss="modal"
                              className='popup-btn font-Futura'
                              style={{fontSize: isSmallDevice ? 12 : 14}}
                              // onClick={downloadPDF}
                            >
                              Tutup
                            </button>
                          </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        case 4:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Terima Kasih</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Silakan melakukan download document dengan menekan tombol dibawah.
                          </p>
    
                          <div style={{marginBottom: 100, marginTop: 50}}>
                            <Lottie
                              options={defaultOptions}
                              width={isSmallDevice? 250: 328}
                              isStopped={false}
                              isPaused={false}
                            />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                            onClick={downloadPDF}
                          >
                            Download PDF
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 5:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Terima Kasih</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Berhasil download dokumen pengajuan
                          </p>
    
                          <div style={{marginBottom: 100, marginTop: 50}}>
                            <Lottie
                              options={defaultOptions}
                              width={isSmallDevice? 250: 328}
                              isStopped={false}
                              isPaused={false}
                            />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 202:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Terjadi kesalahan pada sistem. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/202.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 303:
          return (
            <>
              <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog popup-content bottom-rounded">
                  <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                    
                    <div class="popup-body" >
                    <div className="text-center">
                            <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                            <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                            Terjadi kesalahan pada sistem. Mohon dicoba beberapa saat kemudian.
                            </p>
      
                            <div style={{marginBottom: 20, marginTop: 50}}>
                              <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/303.png" alt="" />
                            </div>
      
                            <button
                              type="button"
                              data-bs-target="#modalGeneratePdf"
                              data-bs-dismiss="modal"
                              className='popup-btn font-Futura'
                              style={{fontSize: isSmallDevice ? 12 : 14}}
                              // onClick={() => router.push('/')}
                            >
                              OK
                            </button>
                          </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        case 304:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Email dan NIK yang kamu gunakan tidak sesuai dengan data mitra BTN. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/304.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 503:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Email dan NIK yang kamu gunakan tidak sesuai dengan data mitra BTN. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/503.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 505:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Terjadi kesalahan pada sistem. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/505.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 606:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Terjadi kesalahan pada sistem. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/606.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 707:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Terjadi kesalahan pada sistem. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/707.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 808:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Terjadi kesalahan pada sistem. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/808.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        case 909:
        return (
          <>
            <div class="modal fade" id="modalGeneratePdf" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog popup-content bottom-rounded">
                <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                  
                  <div class="popup-body" >
                  <div className="text-center">
                          <h4 className="modal-title mb-3 font-Futura popup-title">Mohon Maaf</h4>
                          <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                          Terjadi kesalahan pada sistem. Hubungi Contact Center BTN <b>(1500286)</b> untuk informasi lebih lanjut. <br />
                           
                          </p>
    
                          <div style={{marginBottom: 20, marginTop: 50}}>
                          <img style={{maxWidth: isSmallDevice? 250: 400, width: "100%", height: "auto"}} src="/images/modal/909.png" alt="" />
                          </div>
    
                          <button
                            type="button"
                            data-bs-target="#modalGeneratePdf"
                            data-bs-dismiss="modal"
                            className='popup-btn font-Futura'
                            style={{fontSize: isSmallDevice ? 12 : 14}}
                          >
                            OK
                          </button>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
        default:
            return (
              <>
                <div class="modal fade" id="modalGeneratePdf" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog popup-content bottom-rounded">
                    <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
                      
                      <div class="popup-body" >
                      <div className="text-center">
                              <h4 className="modal-title mb-3 font-Futura popup-title">Mohon maaf</h4>
                              <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                              Saat ini sistem kami sedang mengalami gangguan. Silakan coba beberapa saat lagi.
                              </p>
        
                              <div style={{marginBottom: 20, marginTop: 50}}>
                                <Lottie
                                  options={defaultOptions}
                                  width={isSmallDevice? 250: 328}
                                  isStopped={false}
                                  isPaused={false}
                                />
                              </div>
        
                              <button
                                type="button"
                                data-bs-target="#modalGeneratePdf"
                                data-bs-dismiss="modal"
                                className='popup-btn font-Futura'
                                style={{fontSize: isSmallDevice ? 12 : 14}}
                                // onClick={() => router.push('/')}
                              >
                                OK
                              </button>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )
    
      }
    }
    return responseHandler(isSuccess)
}

export default index

