import React from 'react'
import Lottie from "react-lottie";
import * as animationData from "../../../public/animate_home.json"
import { useMediaQuery } from "react-responsive";
import { useRouter } from 'next/router';


const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
    },
};


function index({kprId}) {
    const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
    const isMediumDevice = useMediaQuery({ query: `(min-width: 401px) and (max-width: 576px)` });
    const router = useRouter()
  return (
    
    <>
    <div class="modal fade" id="modalKprSuccess" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog popup-content">
        <div class="modal-content" style={{border: 'none', borderRadius: 0, borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
          
          <div class="popup-body" >
          <div className="text-center">
                  <h4 className="modal-title mb-3 font-Futura popup-title">Terima Kasih</h4>
                  {/* <h3 className="popup-title-status mb-3 font-FuturaHvBt">Pengajuan KPR <span style={{color: "rgba(0, 97, 167, 1)"}}>Berhasil</span></h3> */}
                  {/* <p className="desc text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90}}>
                  Selamat! Pengajuan KPR Anda berhasil di submit
                  </p> */}
                  <p className="text-center" style={{fontFamily:"Helvetica", fontWeight:400, marginBottom: 90, maxWidth: 480, width: '100%', fontSize: '1em', margin: '0 auto', color: "#000000"}}>
                  Pengajuan telah berhasil terkirim. Selanjutnya tim BTN akan mencoba menghubungi kamu untuk proses berikutnya. 
                  Silakan cek email untuk melihat kembali pengajuan kamu dan lakukan aktivasi sertifikat digital
                  </p>

                  <div style={{marginBottom: 60, marginTop: 50}}>
                    <Lottie
                      options={defaultOptions}
                      width={isSmallDevice? 250: 328}
                      isStopped={false}
                      isPaused={false}
                    />
                  </div>

                  <button
                    type="button"
                    data-bs-target="#modalKprSuccess"
                    data-bs-dismiss="modal"
                    className='popup-btn font-Futura'
                    style={{fontSize: isSmallDevice ? 12 : 14}}
                    onClick={() => router.push('/upload_doc/?kprId=' + kprId)}
                  >
                    Lanjut ke Tanda tangan Form Aplikasi Kredit
                  </button>
                </div>
          </div>
        </div>
      </div>
    </div>
  </>
  )
}

export default index