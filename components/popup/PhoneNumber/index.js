import React from 'react'

import Lottie from "react-lottie";
import * as animationData from "../../../public/animate_home.json"
import * as xAnimation from "../../../public/x_animation.json"
import { useMediaQuery } from "react-responsive";
import { useRouter } from 'next/router';
import { useAppContext } from '../../../context';


const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
    },
};


function index() {
    const isSmallDevice = useMediaQuery({ query: `(max-width: 400px)` });
    const handleClick = () => {
        if(window.location.pathname.includes("/member/profil")) {
            return
        } else {
            window.location.replace("/member/profil/")
        }
        console.log();
    }
  return (
    <div className="modal fade" id="phoneModal" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" style={{zIndex:"9991"}}>
        <div className="modal-dialog modal-dialog-centered m-auto px-0">
            <div className="modal-content modal_delete h-100" style={{padding:"20px 29px"}}>  
                <div className="modal-body text-center pt-3 px-0">
                    <div className="text-center">        
                    <h4 className="modal-title mb-3" style={{fontFamily:"Futura",fontStyle:"normal",lineHeight:"130%", fontWeight:"700"}}>Profile Kamu Belum Lengkap</h4>
                    <div className="desc text-center mb-0" style={{ fontFamily: "Helvetica", fontWeight: 400 }}>
                    Lengkapi Informasi Data Diri dan Informasi Kontak Kamu
                    </div>
                    <div style={{marginBottom: 100, marginTop: 50}}>
                        <Lottie
                            options={defaultOptions}
                            width={isSmallDevice? 250: 328}
                            isStopped={false}
                            isPaused={false}
                        />
                    </div>
                    <button
                        type="buttom"
                        className="btn btn-main form-control btn-back px-5"
                        style={{
                        width:"100%",
                        maxWidth: "540px",
                        height: "48px",
                        fontSize: "14px",
                        fontFamily: "Helvetica",
                        fontWeight: 700,
                        backgroundColor: "#0061A7",
                        }}
                        data-bs-dismiss="modal"
                        onClick={handleClick}
                    >
                        Oke
                    </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default index