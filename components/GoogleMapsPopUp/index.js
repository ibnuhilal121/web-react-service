import { LoadScriptNext, GoogleMap, InfoWindow, Marker, Polyline, useGoogleMap } from "@react-google-maps/api";
import React, { useCallback, useState } from "react";
import { useEffect } from "react";
import styles from "./googleMapsPopUp.module.scss";
import _ from "lodash";

const defPosition = {
    lat: -6.2088,
    lng: 106.8456,
};

export default function GoogleMapsPopUp({ 
    position = defPosition, 
    dinamicPosition = false, 
    index, 
    ...props
}) {    
    if (dinamicPosition) {
        // console.log('~ position : ', position)
        if (!position || !position?.lat || !position?.lng) {
            position = {
                lat: -6.2037667,
                lng: 106.8306111
            };
        };
    };
    
    const clickMap = (e) => {
        if (dinamicPosition) {
            if (index === '') {
                props.changeSekitar("koordinat", `${e.latLng.lat()},${e.latLng.lng()}`)
            } else {
                props.changeAreaSekitar("koordinat", index, `${e.latLng.lat()},${e.latLng.lng()}`)
            };
            // props.handleChange("latlon", [e.latLng.lat(), e.latLng.lng()]);
        };
    };

    return (
        <div className={styles.mapsContainer}>
            <LoadScriptNext 
                googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY} 
                libraries={["places"]}
            >
                <GoogleMap
                    id="circle-example"
                    mapContainerStyle={{
                        width: "100%",
                        height: "100%",
                    }}
                    center={position}
                    zoom={10}
                    options={{
                        streetViewControl: false,
                        draggable: true, // make map draggable
                        keyboardShortcuts: false, // disable keyboard shortcuts
                        scaleControl: true, // allow scale controle
                        scrollwheel: false, // allow scroll wheel
                    }}
                    onClick={(e) => clickMap(e)}
                    // onLoad={onLoadMap}
                >
                    <Marker position={position} draggable={true} onDragEnd={(e) => clickMap(e)}></Marker>
                </GoogleMap>
            </LoadScriptNext>
        </div>
    );
}
