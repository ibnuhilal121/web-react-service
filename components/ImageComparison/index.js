import React from 'react';
import ReactImageFallback from "react-image-fallback";

export default function ImageComparison({ data }) {
  return (
    <div className="card card_komparasi" style={{ margin: '0 10px' }}>
      <ReactImageFallback 
        src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.gbr1}
        fallbackImage="/images/thumb-placeholder.png"
        className="img-fluid img_thumb" 
      />
    </div>
  )
}