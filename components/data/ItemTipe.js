import Link from "next/link";
import NumberFormat from "react-number-format";
import ReactImageFallback from "react-image-fallback";
import { useRouter } from "next/router";
import { useState } from "react";
import { useEffect } from "react";
import LikeButton from "../element/LikeButton";
import { deleteIklan } from "../../services/member";
import { useAppContext } from "../../context";
import { useMediaQuery } from "react-responsive";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

const setupStatus = {
  0: {
    status: 'Draft',
    style: 'bg-secondary'
  },
  1: {
    status: 'Menunggu Konfirmasi',
    style: 'bg-info'
  },
  2: {
    status: 'Revisi',
    style: 'bg-warning text-dark'
  },
  3: {
    status: 'Dalam Proses',
    style: 'bg-info text-dark'
  },
  4: {
    status: 'Disetujui',
    style: 'bg-success'
  },
  5: {
    status: 'Ditolak',
    style: 'bg-danger'
  },
  6: {
    status: 'Dibatalkan',
    style: 'bg-danger'
  }
}

export default function ItemUnit(props) {
  const isTabletOrMobile = useMediaQuery({
    query: "(min-width: 768px) and (max-width: 948px)",
  });
  const isMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const { data, compare, index, selected, getProperti,deleteCompare,setCompare } = props;
  const { JENIS, KLASTER, ID, NAMA_PROPER } = data;

  const generateCategory = (value) => {
    const valueStr = value?.toString() || "";
    switch (valueStr) {
      case "1":
        return "RUMAH";
      case "2":
        return "APARTEMEN";
      case "3":
        return "RUMAH BEKAS";
      case "4":
        return "LELANG";
      case "5":
        return "APARTEMEN BEKAS";
      case "6":
        return "RUKO";
      case "7":
        return "KANTOR";
      default:
        return "";
    };
  };

  // const image = new Image();
  // image.src = generateImageUrl(data.GBR1?.split("|")[1]);
  // const imageFit = image.width === image.height;

  const [imageUrl, setImageUrl] = useState("/images/thumb-placeholder.png");
  const [imageFit, setImageFit] = useState(true);
  const [imageLandscape, setImageLandscape] = useState(true);
  useEffect(() => {
    if (!data.GBR1) return;
    generateImageUrl(data.GBR1?.split("|")[1], (url) => {
      setImageUrl(url);
      const image = new Image();
      image.src = url;
      setImageLandscape(image.width >= image.height)
      setImageFit(image.width === image.height);
    })
  }, []);


  const { userKey, userProfile } = useAppContext();
  const router = useRouter();
  const { pathname } = router
  const { komparasi } = router.query;
  const tipeProperti = generateCategory(JENIS);
  const tipeLow = tipeProperti.toLowerCase();
  const tipeSlug = tipeLow != "uncategorized" ? tipeLow : "";
  const [borderColor, SetBorderColor] = useState(false);
  const seo = KLASTER ? `${KLASTER.split(" ").join("-")}-${ID}`: `${ID}`;
  const seoPerum = NAMA_PROPER ? `${NAMA_PROPER.split(" ").join("-")}-${ID}`:`${ID}`;
  console.log("seoPerum ", seoPerum)
  console.log("seo ", seo)

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const formatter = (e) => {
    const million = 1000000;
    if (e >= million) {
      return `${Math.floor(e / million)}jt/bln`;
    } else {
      return `${Math.round(numberWithCommas(e))}rb/bln`;
    }
  };
  const CustomTooltips = () => (
    <OverlayTrigger
      placement="right"
      overlay={
        <Tooltip id="tooltip-right">
          {
            <>
              <NumberFormat value={data.pmt_konvensional != null ? data.pmt_konvensional : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
              /Bln
            </>
          }
        </Tooltip>
      }
    >
      {data.pmt_konvensional != null ? (
        <p
          className="fw-bold m-0"
          style={{
            color: "#00193E",
            fontSize: "12px",
            cursor: "pointer",
            display: "inline-block",
          }}
        >
          Rp{formatter(data.pmt_konvensional) || "-"}
        </p>
      ) : (
        <p
          className="fw-bold m-0"
          style={{
            color: "#00193E",
            cursor: "pointer",
            display: "inline-block",
          }}
        >
          <NumberFormat value={data.pmt_konvensional != null ? data.pmt_konvensional : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
        </p>
      )}
    </OverlayTrigger>
  );

  const CSSVariables = {
    border: {
      border: "2px solid rgba(0, 97, 167, 0.75)",
    },
    noBorder: {
      border: "none",
    },
  };

  const compareSelect = (data) => {
    if (!borderColor) {
      if (compare.length != 4) {
        //SetBorderColor(true);
        props.dataCompare(data);
      }
    }else{
      deleteCompare(data)
    }
  };

  useEffect(() => {
    if (komparasi == "1") {
      SetBorderColor(false);
      if (compare?.length > 0) {
        compare
          .filter((list) => list.ID.includes(data.ID))
          .map((filtered) => {
            SetBorderColor(true);
          });
      }
    }
  }, [compare]);

  const handleDelete = (id) => {
    deleteIklan(id, userKey)
      .then((res) => {
        // getProperti();
        window.location.reload();
      })
      .catch((error) => {});
  };

  return (
    <>
      {komparasi == "1" ? (
        <div key={data.ID} className="card card_unit_properti" onClick={() => compareSelect(data)} style={borderColor ? CSSVariables.border : CSSVariables.noBorder}>
          <div className="card_img" 
          // style={{minHeight: "200px"}}
          >
            <div className="ratio ratio-1x1">
              <a>
                <div
                  style={{
                    background: `url(${imageUrl})`,
                  }}
                  className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                />
                <div className="container-img-card" style={{ height: "100%" }}>  
                  <ReactImageFallback
                    src={imageUrl}
                    fallbackImage="/images/thumb-placeholder.png"
                    className={`${imageLandscape ? "w-100" : "h-100"} `}
                  />
                </div>
              </a>
            </div>
            <a className="link">
              <div id="kategori-pencarian" className={"kategori " + (tipeSlug || "rumah")} style={{ fontSize: "12px", textTransform: "uppercase" }}>
                {tipeProperti}
              </div>
            </a>
            <LikeButton propId={data.ID} />
          </div>

          <div className="card-body">
            <h6
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: "10px",
                lineHeight: "130%",
              }}
              className="lokasi one-line-flex"
            >
              {data.lokasi || data.KOTA || "-"}
            </h6>
            <a>
              <h5
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "700",
                  fontSize: "20px",
                  lineHeight: "150%",
                }}
                className="title"
              >
                {data.NAMA_PROPER}
              </h5>
            </a>
            <h5
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: isTabletOrMobile ? "13px" : "16px",
                lineHeight: "160%",
              }}
              className="price one-line-flex"
            >
              <NumberFormat value={data.HARGA_MULAI} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
            </h5>
            {isMobile ?
            (
              <div>
                <div
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                  className="cicilan"
                >
                  Cicilan dari{" "}
                  <span
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "12px",
                      lineHeight: "160%",
                    }}
                  >
                    {/* <NumberFormat value={data.pmt_konvensional ? formatter(data.pmt_konvensional) : ""} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," suffix=" Jt/bln" /> */}
                    <CustomTooltips />
                  </span>
                </div>

                <div
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                  className="cicilan"
                >
                  Suku Bunga dari{" "}
                  <span
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "12px",
                      lineHeight: "160%",
                    }}
                    // className="price one-line-flex"
                  >
                    {data.sk_bga}%
                  </span>
                </div>
              </div>
            ) :
            (
              <div
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  fontSize: "12px",
                  lineHeight: "160%",
                  display:"flex"
                }}
                className="cicilan"
              >
                <div className="cicilan">
                Cicilan dari{" "}
                <span
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  {/* <NumberFormat value={data.pmt_konvensional ? formatter(data.pmt_konvensional) : ""} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," suffix=" Jt/bln" /> */}
                  <CustomTooltips />
                </span>
                </div>
                <span
                  className="divider"
                  style={{
                    borderRight: "1px solid #666666",
                    height: "12px",
                    margin: "0 8px",
                    display: "inline-block",
                    transform: "translateY(2px)",
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                />
                <div className="cicilan">
                Suku Bunga dari{" "}
                <span
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                  className="price"
                >
                  {data.sk_bga}%
                </span>
                </div>
              </div>
            )}
            <a>
              <div className="developer py-2">
                <div className="logo">
                  <ReactImageFallback 
                    src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.G_DEV?.replace("1|", "")}
                    fallbackImage='/images/thumb-placeholder.png'
                    className="img-fluid"
                  />
                </div>
                <div
                  className="developer-name"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  {data.NAMA_DEV}
                </div>
              </div>
            </a>
          </div>
          {props.detail == "true" && (
            <div className="card-footer">
              <ul id="nav-item-unit" className="nav nav-pills nav-fill">
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="kamar-tidur" src="/images/icons/bed.png" className="img-fluid" height="22px" width="22px" /> {data.KAMAR_TIDUR}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="kamar-mandi" src="/images/icons/shower.png" className="img-fluid" height="17.14px" width="17.14px" />
                  {data.KAMAR_MANDI}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="luas-bangunan" src="/images/icons/lb.png" className="img-fluid" height="22px" width="22px" />
                  {data.LUAS_BANGUNAN}m<sup>2</sup>
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="luas-tanah" src="/images/icons/lt.png" className="img-fluid" height="22px" width="22px" />
                  {data.LUAS_TANAH}m<sup>2</sup>
                </li>
              </ul>
            </div>
          )}
        </div>
      ) : (
        <div className="card card_unit_properti">
          <div className="card_img" style={{ overflow: "hidden"}}>
            <div className="ratio ratio-1x1">
              <Link href={pathname === "/member/favorit" || pathname === "/member/favorit/" ? 
              (data.ID.substring(0, 3) === "TPR" ? {pathname: "/property/tipe/detail", query: {id: seo}} : {pathname: "/property/perumahan/detail", query: {id: seoPerum, page: 1}}) :
              {pathname: "/property/tipe/detail", query: {id: seo}}}>
                {router.pathname === '/member/properti' ?
                (
                  <a>
                    <div
                      style={{ background: `url(${imageUrl})`}}
                      className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                    />
                    <div className="container-img-card">
                      <img
                        src={imageUrl}
                        className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                      />
                    </div>
                  </a>
                ) :
                (
                  <a>
                    <div
                      style={{
                        background: `url(${imageUrl})`,
                      }}
                      className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                    />
                    <div className="container-img-card" style={{ height: "100%" }}>  
                      <ReactImageFallback
                        src={imageUrl}
                        fallbackImage="/images/thumb-placeholder.png"
                        className={`${imageLandscape ? "w-100" : "h-100"} `}
                        title={data.KLASTER + " " + data.NAMA_PROPER}
                      />
                    </div>
                  </a>
                )}
              </Link>
            </div>
            <Link
              href={{
                pathname: "/property",
                query: { tipe: tipeSlug },
              }}
              passHref
            >
              <a className="link">
                <div id="kategori-pencarian" className={"kategori " + (tipeSlug || "rumah")} style={{ fontSize: "12px", textTransform: "uppercase" }}>
                  {data.kategori != null ? data.kategori : tipeProperti}
                </div>
              </a>
            </Link>
            <LikeButton propId={data.ID} />
          </div>

          <div className="card-body">
            <h6
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: "10px",
                lineHeight: "130%",
              }}
              className="lokasi"
            >
              {data.lokasi || data.KOTA || "-"}
            </h6>
            {pathname === "/member/favorit" || pathname === "/member/favorit/" ? (
              <Link href={data.ID.substring(0, 3) === "TPR" ? {pathname: "/property/tipe/detail", query: {id: seo, page: 1}} : {pathname: "/property/perumahan/detail", query: {id: seoPerum, page: 1}}}>
                {pathname === "/member/favorit" || pathname === "/member/favorit/" ? ( 
                  <a>
                    {data.ID.substring(0, 3) === "TPR" ? (
                      <h5
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: "700",
                          fontSize: "20px",
                          lineHeight: "150%",
                        }}
                        className="title"
                        title={`${data.NAMA_PROPER} | ${data.KLASTER} | ${data.BLOK}`}
                      >
                        {data.NAMA_PROPER} | {data.KLASTER} | {data.BLOK}
                      </h5>
                    ) : (
                      <h5
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: "700",
                          fontSize: "20px",
                          lineHeight: "150%",
                        }}
                        className="title"
                        title={data.title || data.jdl}
                      >
                        {data.n || data.jdl}
                      </h5>
                    )}
                  </a>
                  // END
                ) : tipeProperti == "RUMAH" || tipeProperti == "APARTEMEN" ? (
                  <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.NAMA_PROPER} | {data.BLOK} | {data.KLASTER}
                    </h5>
                  </a>
                ) :  data.title != null || data.jdl != null ? (
                  <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.title || data.jdl}
                    </h5>
                  </a>
                ) : (
                  <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={`${data.NAMA_PROPER} | ${data.BLOK} | ${data.KLASTER}`}
                    >
                      {data.NAMA_PROPER} | {data.BLOK} | {data.KLASTER}
                    </h5>
                  </a>
                )}
              </Link>
            ) : (
              <Link href={{pathname: "/property/tipe/detail", query: {id: seo}}} passHref>
              {pathname === "/property/tipe" || pathname === "/property/tipe/" ? (
                // khusus untuk dipakai di halaman pencarian property tipe
                // untuk halaman lain silakan pakai markup di bawahnya
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={`${data.NAMA_PROPER} | ${data.KLASTER} | ${data.BLOK}`}
                  >
                    {data.NAMA_PROPER} | {data.KLASTER} | {data.BLOK}
                  </h5>
                </a>
                // end of untuk dipakai di halaman pencarian property tipe

                // khusus untuk dipakai di halaman Favorite !!!
              ) : pathname === "/member/favorit" || pathname === "/member/favorit/" ? ( 
                <a>
                  {data.id.substring(0, 3) == "TPR" ? (
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={`${data.NAMA_PROPER} | ${data.KLASTER} | ${data.BLOK}`}
                    >
                      {data.NAMA_PROPER} | {data.KLASTER} | {data.BLOK}
                    </h5>
                  ) : (
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.NAMA_PROPER || data.jdl}
                    </h5>
                  )}
                </a>
                // END
              ) : pathname === "/member/properti" || pathname === "/member/properti/" ? ( 
                <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.NAMA_PROPER || data.jdl}
                    </h5>
                </a>
                // END
              ) : tipeProperti == "RUMAH" || tipeProperti == "APARTEMEN" ? (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={data.title || data.jdl}
                  >
                     {data.NAMA_PROPER} | {data.BLOK} | {data.KLASTER}
                  </h5>
                </a>
              ) :  data.title != null || data.jdl != null ? (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={data.title || data.jdl}
                  >
                    {data.title || data.jdl}
                  </h5>
                </a>
              ) : (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={`${data.NAMA_PROPER} | ${data.BLOK} | ${data.KLASTER}`}
                  >
                    {data.NAMA_PROPER} | {data.BLOK} | {data.KLASTER}
                  </h5>
                </a>
              )}
            </Link>
            )}
            {router.pathname === '/member/properti' &&
            (
              <div 
                className={`badge rounded-pill d-flex justify-content-center align-items-center ${setupStatus[data.st_pgjn].style}`} 
                style={{height: '15px', width: 'fit-content'}}
              >
                <span style={{
                  fontSize: '10px', 
                  verticalAlign: 'middle',
                  fontFamily: "Helvetica"
                }}>
                  {setupStatus[data.st_pgjn].status}
                </span>
              </div>
            )}
            <h5
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: isTabletOrMobile ? "13px" : "16px",
                lineHeight: "160%",
              }}
              className="price one-line-flex"
            >
              <NumberFormat value={data.price != null || data.hrg != null ? data.price || data.hrg : data.HARGA_MULAI != null ? +(data.HARGA_MULAI) * 1 : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
            </h5>
            <div className="d-flex justify-content-md-start flex-column flex-md-row ">
              <div
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  fontSize: "12px",
                  lineHeight: "160%",
                }}
                className="cicilan"
              >
                Cicilan dari{" "}
                <span
                  id="item_cicilan"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <CustomTooltips />
                </span>
              </div>
              <span
                id="divider_card"
                style={{
                  position: "relative",
                  top: "4px",

                  borderRight: "1px #666666 solid",
                  height: "12px",
                  width: "0px",
                }}
              />
              <div
                id="item_bunga"
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  fontSize: "12px",
                  color: "#666666",
                }}
              >
                Suku Bunga dari{" "}
                <span
                  style={{
                    color: "#00193e",
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                  className="price"
                >
                  {data.suku_bunga != null ? data.suku_bunga : data.sk_bga}%
                </span>{" "}
              </div>
            </div>
            <Link href={
                {
                  pathname:"/developer/detail",
                  query: {
                    id: (data.ID_DEV || data.i_memb)
                  }
                }
              }
              passHref
              >
              <a>
                <div className="developer py-2">
                  <div className="logo">
                    {/* <img
                      src={ data.developer_avatar != null ? data.developer_avatar :
                        `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                        data.g_dev?.replace("1|", "")
                      }
                      className="img-fluid"
                    /> */}
                    <ReactImageFallback
                      src={data.developer_avatar || data.G_DEV ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + (data.developer_avatar || data.G_DEV).replace("1|", "") : null}
                      fallbackImage="/images/thumb-placeholder.png"
                      className="img-fluid"
                    />
                  </div>
                  <div
                    className="developer-name"
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "400",
                      fontSize: "12px",
                      lineHeight: "160%",
                    }}
                  >
                    {data.developer_name != null ? data.developer_name : data.NAMA_DEV || data.n_memb}
                  </div>
                </div>
              </a>
            </Link>
          </div>
          {props.detail == "true" && (
            <div className="card-footer"
            //  style={ {display: pathname === "/member/favorit" || pathname === "/member/favorit/" ? "none":""}}
            >
              <ul id="nav-item-unit" className="nav nav-pills nav-fill">
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="kamar-tidur" src="/images/icons/bed.png" className="img-fluid" height="22px" width="22px" /> {data.kamar_tidur != null ? data.kamar_tidur : data.KAMAR_TIDUR}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="kamar-mandi" src="/images/icons/shower.png" className="img-fluid" height="17.14px" width="17.14px" />
                  {data.kamar_mandi != null ? data.kamar_mandi : (data.KAMAR_MANDI ? data.KAMAR_MANDI : "-")}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="luas-bangunan" src="/images/icons/lb.png" className="img-fluid" height="22px" width="22px" />
                  {data.lb != null ? data.lb : (data.LUAS_BANGUNAN ? data.LUAS_BANGUNAN : "-")}m<sup>2</sup>
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="luas-tanah" src="/images/icons/lt.png" className="img-fluid" height="22px" width="22px" />
                  {data.ls_tnh != null ? (data.ls_tnh ? data.ls_tnh : "-") : data.LUAS_TANAH}m<sup>2</sup>
                </li>
              </ul>
            </div>
          )}
          {props.action == "true" && (
            <div className="card-action">
              <div className="row">
                <div className="col">
                  <Link href={`/member/update-properti?id=${data.id}`}>Edit</Link>
                </div>
                <div className="col">
                  <Link href="#">
                    <a href="" data-bs-toggle="modal" data-bs-target={"#delete" + data.id}>
                      Hapus
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          )}
          {props.banding == "true" && (
            <div className="bandingkan" onClick={()=>{setCompare(data)}} style={{cursor:"pointer"}}>
              <a 
              style={{
                color: "#0061a7",
                fontFamily: "Helvetica",
                fontWeight: 700,
                fontSize: "12px",
                fontStyle: "normal",
                lineHeight: "160%"
              }}
              >Bandingkan</a>
            </div>
          )}
        </div>
      )}
      <div className="modal fade" id={"delete" + data.id} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered ">
          <div className="modal-content modal_delete">
            <div className="close-modal" data-bs-dismiss="modal">
              <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <div className="modal-body text-center pt-3">
              <h5 className="modal-title">Hapus Iklan Properti</h5>
              <p>Apakah Anda akan menghapus iklan ini?</p>

              <button onClick={() => handleDelete(data.id)} type="button" data-bs-dismiss="modal" className="btn btn-primary w-100 mt-4 mb-3">
                Hapus
              </button>
              <a type="button" data-bs-dismiss="modal">
                Batal
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

// const generateImageUrl = (url) => {
//   if (!url) return "/images/thumb-placeholder.png";

//   let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
//   fullUrl = fullUrl.replaceAll(" ", "%20");

//   var img = new Image();
//   img.src = fullUrl;
//   const isImageExist = img.height != 0;

//   if (isImageExist) {
//     return fullUrl;
//   } else {
//     return "/images/thumb-placeholder.png";
//   }
// };

const generateImageUrl = (url, callback) => {
  if (!url) return "/images/thumb-placeholder.png";

  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;
  img.onload = () => {
    callback(fullUrl);
  };
  // img.onerror = () => {
  //   callback("/images/thumb-placeholder.png");
  // };
};