import React from 'react'

export default function ItemRewardPenukaran(props) {
    const { data } = props;
    const date = new Date(data.tgl_aktivitas)
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    const formattedDate = date.toLocaleDateString("id", options)
    
    return (
        <div>
            <div className="card card-reward-penukaran">
                <div className="card-body d-flex justify-content-between align-items-center">
                    <div className="content">
                        <div style={{fontFamily:"Helvetica", fontWeight:"400", fontSize:"12px"}} className="date">{formattedDate}</div>
                        <div style={{fontFamily:"Helvetica", fontWeight:"700", fontSize:"14px"}} className="title">{data.debet ? data.guide : "Redeem " + data.n}</div>
                    </div>
                    <div style={{fontFamily:"Helvetica", fontWeight:"700", fontSize:"14px"}} className={data.debet == 0 ? "danger":"success"}>
                        {data.debet ? ("+" + data.debet) : ("-" + data.kredit)}</div>
                </div>
            </div>
        </div>
    )
}
