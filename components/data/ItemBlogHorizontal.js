import Link from "next/link"

export default function ItemBlogHorizontal() {
    return (
        <div className="card card_news_horizontal mb-3 w-100">
            <div className="row g-0 d-flex align-items-center">
                <div className="col-md-6">
                    <Link href="blog/Interior Rumah Warna Hijau Tosca yang Menarik Ini Bikin Nyaman dan"><img src="/images/blog/big.png" className="img-fluid rounded-start" alt="..."/></Link>
                </div>
                <div className="col-md-6">
                    <div className="card-body">
                        <Link href="blog/Interior Rumah Warna Hijau Tosca yang Menarik Ini Bikin Nyaman dan">
                            <h5 className="card-title">Interior Rumah Warna Hijau Tosca yang Menarik Ini Bikin Nyaman dan Santai</h5>
                        </Link>
                        <div className="d-flex meta_post">
                            <h6>Inspirasi</h6>
                            / 14 Juni 2021
                        </div>
                        <p className="card-text">Interior Rumah Warna Hijau Tosca bisa menjadi ide menarik pada hunian untuk memberikan tampilan lebih segar dan menarik. Penerapan interior rumah dengan sentuhan warna hijau tosca tentunya akan menambah...</p>
                        <Link href="blog/Interior Rumah Warna Hijau Tosca yang Menarik Ini Bikin Nyaman dan">Lihat lebih</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
