import Link from "next/link";
import { useState } from "react";
import { useEffect } from "react";
import { useMediaQuery } from "react-responsive";

export default function ItemDeveloper(props) {
  const { data } = props;
  const { id, n } = data;
  const seo = `${n?.split(" ").join("-")}-${id}`
  const [imageUrl, setImageUrl] = useState("/images/thumb-placeholder.png");
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 820px)` });

  const image = new Image();
  image.src = imageUrl;
  const imageFit = image.width === image.height;
  const imageLandscape = image.width > image.height;

  useEffect(() => {
    generateImageUrl(data.logo, (url) => {
      setImageUrl(url);
    })
  }, []);

  return (
    <div className="card card_item_developer" style={{ overflow: "hidden" }}>
      <div className="card_img" style={{ height: "auto" }}>
        <div className="ratio ratio-1x1">
          <Link
            href={{
              pathname:"/developer/detail",
              query:{id: seo
              }}}>
            
            <a style={{ cursor: "pointer" }}>
              <div
                style={{
                  background: `url(${imageUrl}`,
                }}
                className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
              />
              <div className="container-img-card" style={{height: "100%", overflow: "hidden"}}>
              {/* <div className="container-img-card h-100"> */}
                <img
                  src={imageUrl}
                  className={imageLandscape ? "w-100" : "h-100"}
                />
              </div>
            </a>
          </Link>
        </div>
        <div id="ItemDeveloperCard" className="px-2">
          {data.label.includes("apartement") && (
            <Link
              href={{
                pathname: "/property",
                query: { kategori: "apartemen" },
              }}
              passHref
            >
              <div
                id="kategoriapart"
                className={"kategori " + "apartemen"}
                style={{ fontSize: isTabletOrMobile ? "8px": 12, cursor: "pointer", marginRight: "8px" }}
              >
                {"APARTEMEN"}
              </div>
            </Link>
          )}
          {data.label.includes("rumah") && (
            <Link
              href={{
                pathname: "/property",
                query: { kategori: "rumah" },
              }}
              passHref
            >
              <div
                id="kategorirumah"
                className={"kategori " + "rumah"}
                style={{ fontSize: isTabletOrMobile ? "8px": 12, cursor: "pointer", marginRight: "8px" }}
              >
                {"RUMAH"}
              </div>
            </Link>
          )}
        </div>
      </div>

      <div className="card-body">
        <Link href={
          {
            pathname:"/developer/detail",
            query: {
              id: seo
            }
          }
        }>
          <a style={{ cursor: "pointer" }}>
            <h5 className="title">{data.n}</h5>
          </a>
        </Link>
        <div className="jumlah">{data.jml_prpt} Properti</div>
        <Link href={
          {
            pathname:"/developer/detail",
            query: {
              id: seo
            }
          }
        }>
          <a style={{ cursor: "pointer" }}>
            <span style={{ marginRight: 12 }}>Lihat Selengkapnya</span>
            {/* <img src="/images/icons/right_arrow.svg" className="img-fluid" /> */}
            <svg
              width="5.76"
              height="9.33"
              viewBox="0 0 7 10"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.666504 8.57004L4.22873 5.00004L0.666504 1.43004L1.76317 0.333374L6.42984 5.00004L1.76317 9.66671L0.666504 8.57004Z"
                fill="#0061A7"
              />
            </svg>
          </a>
        </Link>
      </div>
    </div>
  );
}

const generateImageUrl = (url, callback) => {
  if (!url) return "/images/thumb-placeholder.png";

  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.replace("1|", "")}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;
  img.onload = () => {
    callback(fullUrl);
  };
  img.onerror = () => {
    callback("/images/thumb-placeholder.png");
  };
};
