import NumberFormat from "react-number-format";
import { useEffect, useState } from "react";
import Link from "next/link";

export default function MinimalProperty(props) {
  const { data } = props;
  const seo = `${data?.NAMA_PROPER?.split(" ").join("-")}-${data.ID}`
  // const image = new Image();
  // image.src = generateImageUrl(data.lst_gmbr?.split("|")[1]);
  // const imageFit = image.width === image.height;

  const [imageUrl, setImageUrl] = useState("/images/thumb-placeholder.png");
  const [imageFit, setImageFit] = useState(true);
  const [imageLandscape, setImageLandscape] = useState(true);

  useEffect(() => {
    if (!data.lst_gmbr) return;
    generateImageUrl(data.lst_gmbr?.split("|")[1], (url) => {
      setImageUrl(url);
      const image = new Image();
      image.src = url;
      setImageLandscape(image.width >= image.height)
      setImageFit(image.width === image.height);
    })
  }, []);

  return (
    <div
      className="card card_property_minimal"
      style={{ boxShadow: "0 0 0", background: "transparent" }}
    >
      <div className="card_img"style={{ overflow: "hidden"}}>
        <div className="ratio ratio-1x1">
          <Link href={{pathname: "/property/perumahan/detail", query: {id: seo, page: 1}}}>
            <a>
              <img
                  src={imageUrl}
                className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
              />
              <div className="container-img-card" style={{ height: "100%" }}>
                <img
                  src={imageUrl}
                  className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                  style={{borderRadius: 0}}
                />
              </div>
            </a>
          </Link>
        </div>
        <Link
          href={{
            pathname: "/property",
            query: {
              kategori: data.JENIS == 1 ? "rumah" : data.JENIS == 2 ? "apartemen" : "",
            },
          }}
          passHref
        >
          <a className="link">
            <div
              className={
                "kategori " + (data.JENIS == 1 ? "rumah" : data.JENIS == 2 ? "apartemen" : "")
              }
            >
              {data.JENIS == 1 ? "Rumah" : data.JENIS == 2 ? "Apartement" : ""}
            </div>
          </a>
        </Link>
      </div>

      <div className="card-content">
        <h6
          className="lokasi"
          style={{
            fontWeight: "700",
          }}
        >
          {data.KOTA}
        </h6>
        <Link href={{pathname: "/property/perumahan/detail", query: {id: seo, page: 1}}} passHref>
          <a>
            <h5 className="title">{data.NAMA_PROPER}</h5>
          </a>
        </Link>
        <h5 className="price">
          <NumberFormat
            value={data.HARGA_MULAI}
            displayType={"text"}
            prefix={"Rp"}
            thousandSeparator="."
            decimalSeparator=","
          />
        </h5>
      </div>
    </div>
  );
}

// const generateImageUrl = (url) => {
//   if (!url) return "/images/thumb-placeholder.png";

//   let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
//   fullUrl = fullUrl.replaceAll(" ", "%20");

//   var img = new Image();
//   img.src = fullUrl;
//   const isImageExist = img.height != 0;

//   if (isImageExist) {
//     return fullUrl;
//   } else {
//     return "/images/thumb-placeholder.png";
//   }
// };

const generateImageUrl = (url, callback) => {
  if (!url) return "/images/thumb-placeholder.png";

  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;
  img.onload = () => {
    callback(fullUrl);
  };
  // img.onerror = () => {
  //   callback("/images/thumb-placeholder.png");
  // };
};
