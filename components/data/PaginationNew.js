import React from "react";
export default function PaginationNew({ length = 1, current = 1, onChangePage = () => {} }) {
  let pattern = null;
  pattern = [...new Array(length)].map((_, i) => i + 1);

  switch (true) {
    case length < 7:
      pattern = [...new Array(length)].map((_, i) => i + 1);
      break;
    case current < 4:
      pattern = [1, 2, 3, 4, 5, "...", length];
      break;
    case current > length - 4:
      pattern = [1, "...", length - 4, length - 3, length - 2, length - 1, length];
      break;
    default:
      pattern = [1, "...", current - 1, current, current + 1, "...", length];
  }

  function handleClickPage(e, val) {
    e.preventDefault();
    onChangePage(val);
  }

  function handleClickPrev(e) {
    e.preventDefault();
    onChangePage(current - 1);
  }

  function handleClickNext(e) {
    e.preventDefault();
    onChangePage(current + 1);
  }
  return (
    <div>
      <nav aria-label="Page navigation pagination_data">
        <ul className="pagination justify-content-center">
          <li className={`page-item ${current == 1 ? "disabled" : ""}`}>
            <a
              className="page-link"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
              onClick={handleClickPrev}
            >
              <i className="bi bi-chevron-left icon" style={{ fontSize: "12px" }}></i>
            </a>
          </li>
          {pattern.map((val, i) => {
            return (
              <>
               {val === "..." ? 
              <li key={i} className="page-item page-item-disabled">
                <a
                  className="page-link"
                  style={{ fontSize: "12px", width: "auto", minWidth: 24, lineHeight: "15.6px",cursor:"default" }}
                >
                  {val}
                </a>
              </li>
              :
              <li key={i} className={`page-item ${current == val ? "active" : ""}`}>
                <a
                  className="page-link"
                  style={{ fontSize: "12px", width: "auto", minWidth: 24, lineHeight: "15.6px" }}
                  onClick={(e) => handleClickPage(e, val)}
                >
                  {val}
                </a>
              </li>
          }
              </>
            );
          })}
          <li
            className={`page-item ${current == length ? "disabled" : ""}`}
            style={{ width: "auto" }}
          >
            <a
              className="page-link"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "start",
              }}
              onClick={handleClickNext}
            >
              <i className="bi bi-chevron-right icon" style={{ fontSize: "12px" }}></i>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}
