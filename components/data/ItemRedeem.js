import React from "react";
import Link from "next/link";

export default function ItemReward(props) {
  const { data } = props;
  return (
    <>
      <div
        className="card card-reward"
        //   style={{ minWidth: "308px", minHeight: "148px", overflow: "hidden" }}
      >
        <img
          src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.gbr.replace(
            "1|",
            ""
          )}`}
          alt=""
          className="image-redem"
        />
        <div className="card-body detail-redem">
          {/* style={{ minWidth: "308px" }} */}
          <div
            className="title title-detail"
            // style={{
            //   fontFamily: "Helvetica",
            //   fontWeight: "700",
            //   lineHeight: "150%",
            //   fontSize: "20px",
            // }}
          >
            {data.n}
          </div>
          <div className="d-flex justify-content-between">
            <div
              className="item py-0 detail-tukar"
              //   style={{
              //     fontFamily: "Helvetica",
              //     fontWeight: "400",
              //     lineHeight: "160%",
              //     fontSize: "14px",
              //   }}
            >
              Tukar untuk{" "}
              <span style={{ fontWeight: "700" }}>{data.jml_point} Poin</span>
            </div>
          </div>
          <div
            className="bottom-redem"
            // style={{
            //   minWidth: "306px",
            //   height: "55px",
            //   marginLeft: "-16px",
            //   marginBottom: "-16px",
            //   background: "#F7FAFF",
            //   borderRadius: "0 0 8px 8px",
            // }}
          >
            <div
              className="d-flex justify-content-between align-items-center nambah-pad"
              //   style={{ padding: "15px" }}
            >
              <Link href={{pathname:"/member/redeem/detail",
              query:{id:data.id}
            }}>
                <div
                  className="btn btn-sm btn_belum_selesai detail-btn"
                  //   style={{
                  //     margin: "auto",
                  //     fontFamily: "Helvetica",
                  //     fontSize: "12px",
                  //   }}
                >
                  TUKAR POIN
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
