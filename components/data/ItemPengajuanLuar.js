import axios from "axios";
import Link from "next/link";
import { useState, useEffect } from "react";
import NumberFormat from "react-number-format";
import { useAppContext } from "../../context";

export default function ItemPengajuanLuar(props) {
    const { data } = props;
    const {userProfile} = useAppContext()
    const listJenisKredit = data.typ_pgjn == 1 ? data.jenisKreditKonvensional : data.jenisKreditSyariah
    const nameJenisKredit = listJenisKredit?.find(item => item.kd == (data.jns_krdt || data.jns_pmbyn))
    const [statusDoc, setStatusDoc] = useState({})
    // process.env.NEXT_PUBLIC_UNIX_TIMESTAMP didapat dari Date.now() saat akan promote digital signature
    const d = new Date(data?.tgl_eln?.toString()).valueOf()


    

    const checkDocument = async (id) => {
        try {
            const body = {
                userId: userProfile.id,
                kprId: id
              }
            const {data} = await axios.post(process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE + "/documentCheck", body)
            console.log({data});
            setStatusDoc(data?.code)
        } catch (error) {
            console.log(error);
            setStatusDoc(error.response.data.code);

            // setIsSuccess(-1)
        }
    }
    // hide-digital-signature
    useEffect(() => {
        if(data.st_pgjn_eloan !== "Disimpan" ) {
            checkDocument(data.id)
        }
    }, [data.id])

    const date = new Date(data.t_ins)
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    const formattedDate = date.toLocaleDateString("id", options)
    return (
        <div id="card_detail_pengajuan">
            <div className="card card_pengajuan">
                <div className="card-header ps-3">
                    <div
                        className="kode"
                        style={{fontSize:"10px", fontFamily: "FuturaBT", fontWeight: "700" }}
                    >
                        {data.id ? data.id : "-"} &middot; <span>{formattedDate ? formattedDate : "-"}</span>
                    </div>
                    <div
                        className="status"
                        style={{ fontFamily: "FuturaBT", fontWeight: "700", opacity: 0.6,maxWidth:"25%" }}
                    >
                        {data.st_pgjn_eloan ? data.st_pgjn_eloan : "-"}
                    </div>
                </div>
                <div className="card-body ps-3">
                    <div className="row">
                        <div className="col-6 item">
                            <label>Nama</label>
                            <strong>{data.nm_lgkp ? data.nm_lgkp : "-"}</strong>
                        </div>
                        <div className="col-6 item">
                            <label>Jenis KPR</label>
                            <strong>{data.typ_pgjn ? (data.typ_pgjn == 1 ? "Konvensional" : "Syariah") : "-"}</strong>
                        </div>
                        <div className="col-6 item">
                            <label>Jenis kredit</label>
                            <strong style={{textTransform: "none"}}>{nameJenisKredit ? nameJenisKredit.nl : "-"}</strong>
                        </div>
                        <div className="col-6 item">
                            <label>nilai Pengajuan</label>
                            <strong>
                                <NumberFormat
                                    value={data.nl_pgjn ? data.nl_pgjn : "-"}
                                    displayType={"text"}
                                    prefix={"Rp"}
                                    thousandSeparator="."
                                    decimalSeparator=","
                                />
                            </strong>
                        </div>
                    </div>
                </div>
                <div
                    className="card-footer"
                    style={{ marginLeft: 24, marginRight: 24, paddingRight: 0 }}
                >
                    {/* hide detail KPR */}
                    {data.st_pgjn_eloan === "Disimpan" ? (
                        <Link href={"/ajukan_kpr?id=" + data.id}>
                            <a className="btn btn-main" style={{ width: 179, height: 48 }}>
                                Lanjutkan Pengisian
                            </a>
                        </Link>
                    ) : (
                        
                        <div style={{display: "flex", justifyContent: "flex-end"}}>
                            {/* digital signature part =========== comment the code below to hide-digital-signature ================== */}
                            {
                                statusDoc === "DOCUMENT_NOT_FOUND" && d > process.env.NEXT_PUBLIC_UNIX_TIMESTAMP &&
                            <a
                            href={"/upload_doc/?kprId=" + data.id}
                            className="btn btn-outline-main btn_rounded  w-100"
                            style={{
                            maxWidth: "200px",
                            padding: "12px",
                            fontFamily: "Helvetica",
                            fontSize: "14px",
                            fontWeight: 700,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: "200px",
                            marginRight: 30
                            }}
                            >
                                Penandatanganan
                            </a> 
                            }
                            {/* ==================================================================================== */}

                                
                                <a href={"/member/kredit/detail?id=" + data.id} className="btn btn-main" style={{ width: 179, height: 48 }}>
                                    Lihat Detail
                                </a>
                        </div>
                        
                    )}
                    {/* {data.st_pgjn_eloan === "Disimpan" ? (
                        <Link href={"/ajukan_kpr?id=" + data.id}>
                            <a className="btn btn-main" style={{ width: 179, height: 48 }}>
                                Lanjutkan Pengisian
                            </a>
                        </Link>
                    ) : (
                        <Link href={"/member/kredit/detail?id=" + data.id}>
                            <a className="btn btn-main" style={{ width: 179, height: 48 }}>
                                Lihat Detail
                            </a>
                        </Link>
                    )} */}
                </div>
            </div>
        </div>
    );
}
