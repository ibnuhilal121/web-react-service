import React, { useEffect } from 'react';
import Link from "next/link";
import { convertDate } from "../../utils/DateConverter";
import ReactImageFallback from "react-image-fallback";
import parse from "html-react-parser";

export default function ItemBlogVertical(props) {
  const { data, index } = props;
  const seo = `${data.seo.split(" ").join("-")}-${data.id}`
  useEffect(() => {
    console.log('data dan index ', data, index)
  })

  return (
    <div className={`${ index < 1 ? "banner-blog" : "card card_news_vertical me-md-2 mini-card"}`}>
      <div className={`${ index < 1 ? "img" :"card_img customBlog w-100"}`}>
        <Link
          href={{
            pathname:"/blog/detail",
            query:{id: seo
            }}}
          >
          <a>
            <ReactImageFallback
              src={
                data.gmbr?.includes("http")
                  ? data.gmbr
                  : `${process.env.NEXT_PUBLIC_IMAGE_URL}/${
                      data.gmbr?.split("|")[1]
                    }`
              }
              fallbackImage="/images/thumb-placeholder.png"
              className={`${ index < 1 ? "customBlog tinggi" :"customBlog"}`}
              style={{
                width: "100%",
                objectFit: "cover",
              }}
            />
          </a>
        </Link>
      </div>

      <div className={`${ index < 1 ? "isi" : "card-content"}`}>
        <Link
          href={{
            pathname:"/blog/detail",
            query:{id: seo
            }}}
          >
          <a>
            <h5 className={`${ index < 1 ? "isi-judul" :"card-title"}`}>{data.jdl}</h5>
          </a>
        </Link>
        <div className={`${ index < 1 ? "d-flex tanggal" : "d-flex meta_post"}`}>
          <h6>{data.n_kat}</h6> / {convertDate(data.t_pst?.split("T")[0])}
        </div>
        <div className={`${ index < 1 ? "text-isi" : "card-text"}`}>
          {data.rgksn ? parse(data.rgksn.replace(/(<([^>]+)>)/gi, "")) : ""}
        </div>
        <Link
          href={{
            pathname:"/blog/detail",
            query:{id: seo
            }}}
          >Lihat lebih</Link>
      </div>
    </div>
  );
}
