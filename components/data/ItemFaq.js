import Link from "next/link";
import ChevronRight from "../element/icons/chevron_right";
import styles from "../../styles/Faq.module.scss";
import parse from 'html-react-parser';

export default function ItemFaq(props) {
  const { data } = props;
  return (
    <div>
      <div className="faq_item">
        <a
          className="faq_link collapsed"
          data-bs-toggle="collapse"
          data-bs-target={"#faq" + data.ID}
          aria-expanded="true"
          aria-controls={"faq" + data.ID}
        >
          <div
            className="faq_head"
            id={"heading" + data.ID}
            style={{ fontSize: "14px" }}
          >
            {data.PERTANYAAN}
          </div>
        </a>

        <div
          id={"faq" + data.ID}
          className="accordion-collapse collapse"
          aria-labelledby={"heading" + data.ID}
          data-bs-parent="#accordionExample"
        >
          <div className="accordion-body px-0 pb-0">
            <hr class={styles.solid} />
            <div className={styles.faq_accordion_content}>{data.JAWABAN ? parse(data.JAWABAN) : ''}</div>
            <div className="d-flex justify-content-end">
              <Link href={{ 
                pathname:`/faq/detail/`,
                query: { id: data.ID } 
              }} passHref>
                <span className="d-flex align-items-center">
                  <span className={`fw-bold color_main ${styles.selengkapnya}`}>
                    Selengkapnya
                  </span>
                  <div
                    className={`d-flex align-items-center justify-content-center ${styles.chevron}`}
                  >
                    <ChevronRight />
                  </div>
                </span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
