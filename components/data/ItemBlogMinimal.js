import Link from 'next/link';
import { useEffect, useState } from 'react';
import ReactImageFallback from 'react-image-fallback';



export default function ItemBlogMinimal(props) {
    const {data} = props;
    const seo = `${data.seo.split(" ").join("-")}-${data.id}`
    const [imageUrl, setImageUrl] = useState("/images/thumb-placeholder.png");
    const [imageFit, setImageFit] = useState(true);
    const [imageLandscape, setImageLandscape] = useState(true);    

    useEffect(() => {
        if (!data.images) return console.log("tes" + data.images);
        console.log(data.images)
        generateImageUrl(data.images?.split("|")[1], (url) => {
          setImageUrl(url);
          const image = new Image();
          image.src = url;
          setImageLandscape(image.width >= image.height)
          setImageFit(image.width === image.height);
        })
      }, []);
    
    return (
        <div className="card card_news_minimal">
            <div className="card_img" style={{ overflow: "hidden", borderRadius: 8 }}>
                <div className="ratio ratio-1x1">
                    <Link
                        href={{
                            pathname:"/blog/detail",
                            query:{id: seo
                            }}}
                        >
                        <a>
                            <div 
                                style={{
                                background: `url(${imageUrl})`
                                }}
                                className={`bg-img-card ${imageFit ? 'unblurred' : 'blurred'}`}
                            />
                            <div className="container-img-card">
                                <img 
                                src={imageUrl} 
                                className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                                />
                            </div>
                            {/* <ReactImageFallback 
                                src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.images?.split('|')[1]}`}
                                fallbackImage='/images/thumb-placeholder.png'
                                className="img-fluid"
                            /> */}
                            {/* <img 
                                src={data.images ? 
                                    `${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.images.split('|')[1]}` : 
                                    '/images/thumb-placeholder.png'
                                } 
                                className="img-fluid"
                            /> */}
                        </a>
                    </Link>
                </div>
            </div>
            
            <div className="card-content">
                <Link
                    href={{
                        pathname:"/blog/detail",
                        query:{id: seo
                        }}}
                    >
                    <a><h5 className="title">{data.title}</h5></a></Link>
                <Link
                    href={{
                        pathname:"/blog/detail",
                        query:{id: seo
                        }}}
                    >
                    Lihat lebih
                </Link>
            </div>
        </div>
    )
}
const generateImageUrl = (url,callback) => {
    if (!url) return "/images/thumb-placeholder.png";
  
    const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
    var img = new Image();
    img.src = fullUrl;
    const isImageExist = img.height != 0;
    img.onload = () => {
        callback(fullUrl);
      };
   
    // img.onerror = () => {
    //   callback("/images/thumb-placeholder.png");
    // };
  };