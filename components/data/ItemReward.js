import React from 'react'
import { useMediaQuery } from 'react-responsive';

export default function ItemReward(props) {
    const {data} = props;
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1024px)' })
    const isGalaxyFold = useMediaQuery({ query: '(max-width: 280px)' })
    return (
        <>
            <div className="card card-reward" style={
                isTabletOrMobile ? { minWidth:"100%", minHeight:"148px", boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.1)", border: "1px solid #EEEEEE"} :
                           { minWidth:"308px", minHeight:"148px", boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.1)", border: "1px solid #EEEEEE"}}>
                <div className="card-body" style={{minWidth: isGalaxyFold ? "" : "308px"}}>
                    <div className="title" style={{fontFamily:"Helvetica", fontWeight:"700", lineHeight: "150%", fontSize:"20px"}}>
                        {data.n}
                    </div>
                    <div className="d-flex justify-content-between">
                        <div className="item" style={{fontFamily:"Helvetica", fontWeight:"400", lineHeight: "160%", fontSize:"14px"}}>
                            Dapatkan <span style={{fontWeight:"700"}}>{data.jml_point} Poin</span></div>
                        <div className="item" style={{fontFamily:"Helvetica", fontWeight:"400", lineHeight: "160%", fontSize:"14px"}}>
                            Klaim <span style={{fontWeight:"700"}}>{data.jns_wkt == 1 ? "Sekali" : data.jns_wkt == 2 ? "Pertanggal" : "Setiap Hari"}</span></div>
                    </div>
                    <div style={{minWidth: isGalaxyFold ? "250px" : "306px", height: "55px", marginLeft: "-16px", marginBottom: "-16px", background: "#F7FAFF", borderRadius: "0 0 8px 8px"}}>
                        <div className="d-flex justify-content-between align-items-center" style={{padding : "15px"}}>
                            <div className="status" style={{fontFamily:"Helvetica", fontWeight:"700", lineHeight: "160%", fontSize:"14px"}}>Status</div>
                            <div className={data.st == 1 ? "btn btn-sm btn_belum_selesai" : "btn btn-sm btn_selesai"} style={{cursor: "default"}}>
                                {data.st_aktivitas == 0 ? "BELUM SELESAI" : "SELESAI"}</div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
