import Image from "next/image";
import { useState } from "react";
import { useAppContext } from "../../context";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function ItemKonsultasiPertanyaan(props) {
  const { userKey, userProfile } = useAppContext();
  const { data, getKonsultasi } = props;
  const [open, setOpen] = useState(false);
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });

  const submitLike = async () => {
    try {
      const likeData = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/konsultasi/addlike`,
        {
          method: "POST",
          headers: {
            // "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            app_key: process.env.NEXT_PUBLIC_APP_KEY,
            secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
            ...defaultHeaders
            // AccessKey_Member: userKey,
          },
          body: new URLSearchParams({
            id: data.id,
            id_member: userProfile.id,
          }),
        }
      );
      const responseLike = await likeData.json();
      if (!responseLike.status) {
        throw responseLike.message;
      } else {
        getKonsultasi();
      }
    } catch (error) {
      console.log("error add like");
    }
  };

  return (
    <div className="card card_konsultasi mb-3">
      <div className="card-body border-konsultasi">
        <div className={`d-flex ${isTabletOrMobile ? 'flex-column-reverse justify-content-center align-items-start' : 'justify-content-between align-items-center'}`}>
          <div className="d-flex align-items-center user">
            <img
              // src={data.g_usr}
              src={data.g_memb || "/icons/icons/icon_user.svg"}
              onError={(e) => {
                e.target.onerror = null;
                e.target.src = "/images/user/avatar.svg";
              }}
              className="img-fluid circle"
              height="48"
              width="48"
              style={{ borderRadius: 40 }}
              // alt={data.n_memb}
            />
            <div className="ms-3">
              <h5 className="name">{data.n_memb || "Anonymous"}</h5>
              <p className="date">{data.t_tny?.split(" ")[0]}</p>
            </div>
          </div>
          <div className={`kategori ${isTabletOrMobile ? 'mb-2' : ''}`}>
            {data.i_kat === "1"
              ? "Konsultasi Desain"
              : data.i_kat === "2"
              ? "Perencanaan Keuangan"
              : data.i_kat === "3"
              ? "KPR"
              : data.i_kat === "4"
              ? "Kredit Komersial"
              : data.i_kat === "5"
              ? "Lainnya"
              : null}
          </div>
        </div>
        <div className="desc py-3">{data.prtnyn}</div>
        <div className="footer">
          <div className="d-flex justify-content-between align-items-center">
            <a
              onClick={() => setOpen(!open)}
              data-bs-toggle="collapse"
              data-bs-target={"#balasan" + data.id}
              aria-expanded="true"
              aria-controls={"balasan" + data.id}
              style={{color:"#0061a7"}}
            >
              Lihat Balasan{" "}
              <i
                className={`bi bi-caret-${open ? "up" : "down"}-fill ms-2`}
              ></i>
            </a>
            <div
              className="like"
              style={{ cursor: "pointer" }}
              onClick={userKey ? submitLike : null}
              data-bs-toggle={userKey ? "" : "modal"}
              data-bs-target={userKey ? "" : "#modalLogin"}
            >
              <div className="d-flex align-items-center">
                <div className="like-thumb d-flex align-items-center justify-content-center">
                  <svg
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0.666504 13.9993H3.33317V5.99935H0.666504V13.9993ZM15.3332 6.66602C15.3332 5.93268 14.7332 5.33268 13.9998 5.33268H9.79317L10.4265 2.28602L10.4465 2.07268C10.4465 1.79935 10.3332 1.54602 10.1532 1.36602L9.4465 0.666016L5.05984 5.05935C4.81317 5.29935 4.6665 5.63268 4.6665 5.99935V12.666C4.6665 13.3993 5.2665 13.9993 5.99984 13.9993H11.9998C12.5532 13.9993 13.0265 13.666 13.2265 13.186L15.2398 8.48601C15.2998 8.33268 15.3332 8.17268 15.3332 7.99935V6.66602Z"
                      fill="#0061A7"
                    />
                  </svg>
                </div>
                <span
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    fontSize: "14px",
                    color: "#666666",
                  }}
                  className="ms-1"
                >
                  {data.like}
                </span>
              </div>
              {/* <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0.666504 13.9993H3.33317V5.99935H0.666504V13.9993ZM15.3332 6.66602C15.3332 5.93268 14.7332 5.33268 13.9998 5.33268H9.79317L10.4265 2.28602L10.4465 2.07268C10.4465 1.79935 10.3332 1.54602 10.1532 1.36602L9.4465 0.666016L5.05984 5.05935C4.81317 5.29935 4.6665 5.63268 4.6665 5.99935V12.666C4.6665 13.3993 5.2665 13.9993 5.99984 13.9993H11.9998C12.5532 13.9993 13.0265 13.666 13.2265 13.186L15.2398 8.48601C15.2998 8.33268 15.3332 8.17268 15.3332 7.99935V6.66602Z" fill="#0061A7"/>
</svg>
<span className="ms-1">{data.like}</span> */}
            </div>
          </div>
        </div>
      </div>
      {data.jwbn && (
        <div
          id={"balasan" + data.id}
          className="accordion-collapse collapse "
          aria-labelledby="headingOne"
          data-bs-parent="#accordionKonsultasi"
        >
          <div className="accordion-body card-balasan">
            <div className="d-flex justify-content-between align-items-center">
              <div className="d-flex align-items-center user">
                <img
                  src={data.g_usr}
                  onError={(e) => {
                    e.target.onerror = null;
                    e.target.src = "/images/user/avatar.svg";
                  }}
                  className="img-fluid circle"
                  height="48"
                  width="48"
                  style={{ borderRadius: 40 }}
                  // alt={data.n_usr}
                />
                <div className="ms-3">
                  <h5 className="name">{data.n_usr}</h5>
                  <p className="date">{data.t_jwb.split("T")[0]}</p>
                </div>
              </div>
            </div>
            <div className="py-3">{data.jwbn}</div>
          </div>
          {/* { data.balasan.map((balas, index) =>
                    <div key={index} className="accordion-body card-balasan">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="d-flex align-items-center user">
                                <img 
                                    src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.g_usr.split('|')[1]}` || '/icons/icons/icon_user.svg'} 
                                    className="img-fluid rounded " 
                                    height="48" 
                                    width="48" 
                                    // alt={data.n_usr}
                                />
                                <div className="ms-3">
                                    <h5 className="name">{data.n_usr}</h5>
                                    <p className="date">{data.t_jwb?.split(' ')[0]}</p>
                                </div>
                            </div>
                            
                        </div>
                        <div className="py-3">
                            {data.jwbn}
                        </div>
                    </div>
                    {/* { data.balasan.map((balas, index) =>
                        <div key={index} className="accordion-body card-balasan">
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="d-flex align-items-center user">
                                    <img src={balas.images} className="img-fluid circle " height="48" width="48" alt={data.name} style={{borderRadius: 24}} />
                                    <div className="ms-3">
                                        <h5 className="name">{balas.name}</h5>
                                        <p className="date">{balas.date}</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div className="desc py-3">
                                {balas.content}
                            </div>
                        </div>
                    )} */}
        </div>
      )}
    </div>
  );
}
