import React from "react";
import PropTypes from "prop-types";
import ReactPaginate from "react-paginate";

export default function Pagination({ length, current, onChangePage }) {
  return (
    <div>
      <nav aria-label="Page navigation pagination_data">
        <ReactPaginate
          pageCount={length}
          forcePage={current - 1}
          className="pagination justify-content-center"
          pageClassName={`page-item`}
          activeClassName={"active"}
          pageLinkClassName={"page-link"}
          onPageChange={(obj) => onChangePage(obj.selected + 1)}
          previousLabel={<i className="bi bi-chevron-left icon"></i>}
          nextLabel={<i className="bi bi-chevron-right icon"></i>}
          disableInitialCallback
          previousClassName="page-item"
          previousLinkClassName="page-link chevron"
          nextClassName="page-item"
          nextLinkClassName="page-link chevron"
          disabledClassName="disabled"
        />
      </nav>
    </div>
  );
}

Pagination.PropTypes = {
  current: PropTypes.number,
  length: PropTypes.number,
  onChangePage: PropTypes.func,
};

Pagination.defaultProps = {
  current: 1,
  length: 1,
  onChangePage: () => {},
};
