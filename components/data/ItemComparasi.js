import React, {useEffect, useRef, useState } from 'react'
import Link from 'next/link'
import NumberFormat from 'react-number-format';
import ReactImageFallback from "react-image-fallback";
import { useMediaQuery } from 'react-responsive';
import { useAppContext } from '../../context';

export default function ItemComparasi(props) {
    const { userKey } = useAppContext();
    const {data,index,initHeight} = props;
    const seo = data?.clstr ? `${data.clstr.split(" ").join("-")}-${data.id}` : "";
    const isTablet = useMediaQuery({ query: `(min-width: 768px) and (max-width: 820px)` });
    const facility = (data?.fslts? JSON.parse(data?.fslts) :"")
    const [facilityList, setFacilityList] = useState([])
    const areaSekitarList = data.area_sktr ? JSON.parse(data.area_sktr) : [];
    const [selectImage, setSelectImage] = useState(false);
    const refCluster = useRef(null)
    const refAlamat = useRef(null)
    const refFasilitas = useRef(null)
    const refArea = useRef(null)
    console.log("fasilitas",facility)
    useEffect(() => {
        setValue();
    },[])

    useEffect(() => {
      if (index >= 0) {
        setSelectImage(true)
      }
    }, [index]);

    const setValue = async () => {
        var promises = Object.keys(facility? facility["Kelengkapan Rumah"]:"").map((k) => facility["Kelengkapan Rumah"][k]);
        await Promise.all(promises).then(() => {
            setFacilityList(promises);
            props.changeHeight("cluster",refCluster?.current?.clientHeight);
            props.changeHeight("alamat",refAlamat?.current?.clientHeight);
            props.changeHeight("flexFasilitas",refFasilitas?.current?.clientHeight);
            props.changeHeight("area",refArea?.current?.clientHeight);
        });
    };

    const addHistory = () => {
      sessionStorage.setItem('previous', '/tools/komparasi');
    };
    return (
      <div>
        <div className="card card_komparasi">
          <div>
            {/* <img src={data.images} className="img-fluid img_thumb"/> */}
            {props.image === `don't show` ? null :
            (
                <ReactImageFallback
                    src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.gbr1?.split("|")[1]}
                    fallbackImage="/images/thumb-placeholder.png"
                    className="img-fluid img_thumb" 
                    style={selectImage ? { height: '320px', width: '311px', objectFit: 'cover', } : {}}
                />
            )}
            <h5 className="title">
              {data.clstr} {data.n}
            </h5>
            <h6 className="lokasi">{data.n_kot ? data.n_kot : "-"}</h6>
            <h5 className="price">
              <NumberFormat
                value={data.hrg}
                displayType={"text"}
                prefix={"Rp"}
                thousandSeparator="."
                decimalSeparator=","
              />
            </h5>
            <table className="table table-borderless table_info">
              <tr>
                <td width="45%">Kamar Tidur</td>
                <td className="data">{data.km_tdr}</td>
              </tr>
              <tr>
                <td width="45%">Kamar Mandi</td>
                <td className="data">{data.km_mnd}</td>
              </tr>
              <tr>
                <td width="45%">Luas Bangunan</td>
                <td className="data">
                  {data.ls_bgn}m<sup style={{marginLeft: -6}}>2</sup>
                </td>
              </tr>
              <tr>
                <td width="45%">Luas Tanah</td>
                <td className="data">
                  {data.ls_tnh}m<sup style={{marginLeft: -6}}>2</sup>
                </td>
              </tr>
            </table>
            <div className="row">
            <div className="grey_line mb-0 mt-3s" ></div>
            </div>
          </div>

          <div>
            <h5
              style={{ opacity: index == 0 ? "100" : "0"}}
              className="title_info"
            >
              Informasi Properti
            </h5>
            <table className={`table table-borderless table_info ${isTablet ? 'd-flex flex-column' : ''}`}>
              <tr
                ref={refCluster}
                style={isTablet ? { minHeight: `${initHeight?.cluster}px` } : { height: `${initHeight?.cluster}px` }}
                className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}
              >
                <td 
                  className="align-text-top" 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Cluster/Tower
                </td>
                <td 
                  className="data align-text-top" 
                  style={isTablet ? { paddingTop: 0 } : {}}
                >
                  {data.clstr}
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Blok/Lantai
                </td>
                <td 
                  className="data"
                  style={isTablet ? { paddingTop: 0 } : {}}
                >
                  {data.blk}
                </td>
              </tr>
              <tr 
                ref={refAlamat} 
                style={isTablet ? { minHeight: `${initHeight?.alamat}px` } : { height: `${initHeight?.alamat}px` }}
                className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}
              >
                <td 
                  className="align-text-top" 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Alamat
                </td>
                <td 
                  className="data align-text-top"
                  style={isTablet ? { paddingTop: 0 } : {}}
                >
                  {data.almt}
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Jenis Properti
                </td>
                <td className="data" style={isTablet ? { paddingTop: 0 } : {}}>
                  {data.jns == "1"
                    ? "Rumah"
                    : data.jns == "2"
                    ? "Apartemen"
                    : "Uncategorized"}
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Daya Listrik
                </td>
                <td className="data" style={isTablet ? { paddingTop: 0 } : {}}>
                  {data.dy_lstr} Watt
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Jalur PDAM
                </td>
                <td className="data" style={isTablet ? { paddingTop: 0 } : {}}>
                  {facility ? facility["Kelengkapan Rumah"]["6"] ? "Ada" : "Tidak Ada" : "Tidak Ada"}
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Jalur Telepon
                </td>
                <td className="data" style={isTablet ? { paddingTop: 0 } : {}}>
                  {facility? facility["Kelengkapan Rumah"]["4"] ? "Ada" : "Tidak Ada" : "Tidak Ada"}
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Sertifikat Unit
                </td>
                <td className="data" style={isTablet ? { paddingTop: 0 } : {}}>
                  {data.srtfkt}
                </td>
              </tr>
              <tr className={`${isTablet ? 'd-flex flex-column mb-1' : ''}`}>
                <td 
                  width={isTablet ? "100%" : "45%"}
                  style={isTablet ? { paddingBottom: 0 } : {}}
                >
                  Status Properti
                </td>
                <td className="data" style={isTablet ? { paddingTop: 0 } : {}}>
                  {data.st_ssd == "0" ? "Non Subsidi" : "Subsidi"}
                </td>
              </tr>
            </table>
          </div>

          <div className="row" >
            <div className="grey_line mb-0 mt-3s" ></div>
          </div>

          <div>
            <div style={{ height: `${initHeight?.flexFasilitas + 70}px`, marginBottom: "1rem" }}>
              <h5
                style={{ opacity: index == 0 ? "100" : "0" }}
                className="title_info"
              >
                Fasilitas
              </h5>
              <table
                ref={refFasilitas}
                className="table table-borderless table_fasilitas"
              >
                {facilityList.includes("Tempat Parkir") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M13 3H6V21H10V15H13C16.31 15 19 12.31 19 9C19 5.69 16.31 3 13 3ZM13.2 11H10V7H13.2C14.3 7 15.2 7.9 15.2 9C15.2 10.1 14.3 11 13.2 11Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>Tempat Parkir</td>
                  </tr>
                )}
                {facilityList.includes("AC") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M22 11H17.83L21.07 7.76L19.66 6.34L15 11H13V9L17.66 4.34L16.24 2.93L13 6.17V2H11V6.17L7.76 2.93L6.34 4.34L11 9V11H9L4.34 6.34L2.93 7.76L6.17 11H2V13H6.17L2.93 16.24L4.34 17.66L9 13H11V15L6.34 19.66L7.76 21.07L11 17.83V22H13V17.83L16.24 21.07L17.66 19.66L13 15V13H15L19.66 17.66L21.07 16.24L17.83 13H22V11Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>AC</td>
                  </tr>
                )}
                {facilityList.includes("Jalur Listrik") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M7.00006 12L13.3922 3L13.3924 12L17.3926 12L11.0004 21L11.0003 12H7.00006Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>Jalur Listrik</td>
                  </tr>
                )}
                {facilityList.includes("Jalur PDAM") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M17.66 7.93002L12 2.27002L6.34 7.93002C3.22 11.05 3.22 16.12 6.34 19.24C7.9 20.8 9.95 21.58 12 21.58C14.05 21.58 16.1 20.8 17.66 19.24C20.78 16.12 20.78 11.05 17.66 7.93002ZM12 19.59C10.4 19.59 8.89 18.97 7.76 17.83C6.62 16.69 6 15.19 6 13.59C6 11.99 6.62 10.48 7.76 9.35002L12 5.10002V19.59Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>Jalur PDAM</td>
                  </tr>
                )}
                {facilityList.includes("Jalur Telepon") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M6.62 10.79C8.06 13.62 10.38 15.93 13.21 17.38L15.41 15.18C15.68 14.91 16.08 14.82 16.43 14.94C17.55 15.31 18.76 15.51 20 15.51C20.55 15.51 21 15.96 21 16.51V20C21 20.55 20.55 21 20 21C10.61 21 3 13.39 3 4C3 3.45 3.45 3 4 3H7.5C8.05 3 8.5 3.45 8.5 4C8.5 5.25 8.7 6.45 9.07 7.57C9.18 7.92 9.1 8.31 8.82 8.59L6.62 10.79Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>Jalur Telepon</td>
                  </tr>
                )}
                {facilityList.includes("Kolam Renang") && (
                  <tr>
                    <td width="24px">
                      <svg
                        width="24px"
                        height="42px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M22 21C20.89 21 20.27 20.63 19.82 20.36C19.45 20.14 19.22 20 18.67 20C18.11 20 17.89 20.13 17.52 20.36C17.06 20.63 16.45 21 15.34 21C14.23 21 13.61 20.63 13.16 20.36C12.79 20.14 12.56 20 12.01 20C11.45 20 11.23 20.13 10.86 20.36C10.4 20.63 9.78 21 8.67 21C7.56 21 6.94 20.63 6.49 20.36C6.12 20.13 5.89 20 5.34 20C4.79 20 4.56 20.13 4.19 20.36C3.73 20.63 3.11 21 2 21V19C2.56 19 2.78 18.87 3.15 18.64C3.61 18.37 4.23 18 5.34 18C6.45 18 7.07 18.37 7.52 18.64C7.89 18.87 8.11 19 8.67 19C9.23 19 9.45 18.87 9.82 18.64C10.28 18.37 10.9 18 12.01 18C13.12 18 13.74 18.37 14.19 18.64C14.56 18.86 14.79 19 15.34 19C15.89 19 16.12 18.87 16.49 18.64C16.94 18.37 17.56 18 18.67 18C19.78 18 20.4 18.37 20.85 18.64C21.22 18.87 21.44 19 22 19V21ZM22 16.5C20.89 16.5 20.27 16.13 19.82 15.86C19.45 15.64 19.22 15.5 18.67 15.5C18.11 15.5 17.89 15.63 17.52 15.86C17.07 16.13 16.45 16.5 15.34 16.5C14.23 16.5 13.61 16.13 13.16 15.86C12.79 15.64 12.56 15.5 12.01 15.5C11.45 15.5 11.23 15.63 10.86 15.86C10.41 16.13 9.79 16.5 8.68 16.5C7.57 16.5 6.95 16.13 6.5 15.86C6.13 15.64 5.9 15.5 5.35 15.5C4.8 15.5 4.57 15.63 4.2 15.86C3.73 16.13 3.11 16.5 2 16.5V14.5C2.56 14.5 2.78 14.37 3.15 14.14C3.6 13.87 4.22 13.5 5.33 13.5C6.44 13.5 7.06 13.87 7.51 14.14C7.88 14.36 8.11 14.5 8.66 14.5C9.22 14.5 9.44 14.37 9.81 14.14C10.26 13.87 10.88 13.5 11.99 13.5C13.1 13.5 13.72 13.87 14.17 14.14C14.54 14.36 14.77 14.5 15.32 14.5C15.87 14.5 16.1 14.37 16.47 14.14C16.92 13.87 17.54 13.5 18.65 13.5C19.76 13.5 20.38 13.87 20.83 14.14C21.2 14.36 21.43 14.5 21.98 14.5V16.5H22ZM8.67 12C9.23 12 9.45 11.87 9.82 11.64C10.28 11.37 10.9 11 12.01 11C13.12 11 13.74 11.37 14.19 11.64C14.56 11.86 14.79 12 15.34 12C15.89 12 16.12 11.87 16.49 11.64C16.61 11.57 16.75 11.49 16.9 11.41L10.48 4.99998C8.93 3.44998 7.5 2.98998 5 2.99998V5.49998C6.82 5.48998 7.89 5.88998 9 6.99998L10 7.99998L6.75 11.25C7.06 11.37 7.31 11.52 7.52 11.64C7.89 11.87 8.11 12 8.67 12Z"
                          fill="#00193E"
                        />
                        <path
                          d="M16.5 8C17.8807 8 19 6.88071 19 5.5C19 4.11929 17.8807 3 16.5 3C15.1193 3 14 4.11929 14 5.5C14 6.88071 15.1193 8 16.5 8Z"
                          fill="black"
                        />
                      </svg>
                    </td>
                    <td>Kolam Renang</td>
                  </tr>
                )}
                {facilityList.includes("wifi") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M1 9.00001L3 11C7.97 6.03001 16.03 6.03001 21 11L23 9.00001C16.93 2.93001 7.08 2.93001 1 9.00001ZM9 17L12 20L15 17C13.35 15.34 10.66 15.34 9 17ZM5 13L7 15C9.76 12.24 14.24 12.24 17 15L19 13C15.14 9.14001 8.87 9.14001 5 13Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>wifi</td>
                  </tr>
                )}
                {facilityList.includes("Ruang Kerja") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M20 6H16V4C16 2.89 15.11 2 14 2H10C8.89 2 8 2.89 8 4V6H4C2.89 6 2.01 6.89 2.01 8L2 19C2 20.11 2.89 21 4 21H20C21.11 21 22 20.11 22 19V8C22 6.89 21.11 6 20 6ZM14 6H10V4H14V6Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>Ruang Kerja</td>
                  </tr>
                )}
                {facilityList.includes("Ruang Keluarga") && (
                  <tr>
                    <td width="40px">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M20 6H16V4C16 2.89 15.11 2 14 2H10C8.89 2 8 2.89 8 4V6H4C2.89 6 2.01 6.89 2.01 8L2 19C2 20.11 2.89 21 4 21H20C21.11 21 22 20.11 22 19V8C22 6.89 21.11 6 20 6ZM14 6H10V4H14V6Z"
                          fill="#00193E"
                        />
                      </svg>
                    </td>
                    <td>Ruang Keluarga</td>
                  </tr>
                )}
              </table>
            </div>
          </div>

          <div>
          <div className="row" >
            <div className="grey_line mb-0 mt-3s" ></div>
          </div>

            <h5
              style={{ opacity: index == 0 ? "100" : "0", marginTop: 30 }}
              className="title_info"
            >
              Area Sekitar
            </h5>
            <table className="table table-borderless table_area_sekitar">
              <tr
                ref={refArea}
                style={{ height: `${initHeight?.area}px` }}
                className="active"
              >
                <td width="40px" style={{display:"flex",paddingTop:"4px"}}>
                  <svg
                    width="24px"
                    height="24px"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M12 2C8.13 2 5 5.13 5 9C5 14.25 12 22 12 22C12 22 19 14.25 19 9C19 5.13 15.87 2 12 2ZM12 11.5C10.62 11.5 9.5 10.38 9.5 9C9.5 7.62 10.62 6.5 12 6.5C13.38 6.5 14.5 7.62 14.5 9C14.5 10.38 13.38 11.5 12 11.5Z"
                      fill="#0061A7"
                    />
                  </svg>
                </td>
                <td
                  className="align-top"
                  style={{ color: "#0061A7", fontWeight: "700" }}
                >
                  {data.almt}
                  <i className="bi bi-chevron-right"></i>
                </td>
              </tr>
              {areaSekitarList.map((area) => (
                <tr>
                  <td width="40px">
                    <svg
                      width="42px"
                      height="42px"
                      viewBox="0 0 32 32"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <circle cx="16" cy="16" r="16" fill="#00193E" />
                      <path
                        d="M21.3333 10.6667H10.6667V12H21.3333V10.6667ZM22 17.3334V16L21.3333 12.6667H10.6667L10 16V17.3334H10.6667V21.3334H17.3333V17.3334H20V21.3334H21.3333V17.3334H22ZM16 20H12V17.3334H16V20Z"
                        fill="white"
                      />
                    </svg>
                  </td>
                  <td>{area.n}</td>
                </tr>
              ))}
            </table>
          </div>
          <div className="text-center">
            <Link href={!userKey ? "" : {pathname: "/property/tipe/detail", query: {id: seo}}}>
              <a 
                className="btn btn-outline-main px-5"
                data-bs-toggle={!userKey ? "modal" : null}
                data-bs-target={!userKey ? "#modalLogin" : null}
                onClick={!userKey ? null : addHistory}
              >
                Ajukan KPR
              </a>
            </Link>
          </div>
        </div>
      </div>
    );
}
