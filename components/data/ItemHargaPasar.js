import Link from "next/link";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import getHeaderWithAccessKey from "../../utils/getHeaderWithAccessKey";

export default function ItemHargaPasar(props) {
  const { data } = props;

  const [place, setPlace] = useState(`${data.kywd}`);
  const [yearsData, setYearsData] = useState([]);
  const slice = place.split(",");

  const bulan = [
    { id: 1, value: "Januari" },
    { id: 2, value: "Februari" },
    { id: 3, value: "Maret" },
    { id: 4, value: "April" },
    { id: 5, value: "Mei" },
    { id: 6, value: "Juni" },
    { id: 7, value: "Juli" },
    { id: 8, value: "Agustus" },
    { id: 9, value: "September" },
    { id: 10, value: "Oktober" },
    { id: 11, value: "November" },
    { id: 12, value: "Desember" },
  ];


  const returnBulan = bulan
    .filter((item) => item.id === data.bln)
    .map((item) => item.value);

    const getyears3Data = async () => {
      try {
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/hargapasar/show/statisticyear`;
        const res = await fetch(endpoint, {
          method: "POST",
          headers: getHeaderWithAccessKey(),
          body: new URLSearchParams({
            n_kot: slice[0],
            n_kec: slice[1],
            n_kel: slice[2],
          }),
        });
        const resData = await res.json();
        if (!resData.IsError) {
          console.log("GET DATA year", resData.Data[0]);
          setYearsData(resData.Data[0]);
        } else {
          throw {
            message: resData.ErrToUser,
          };
        }
      } catch (error) {
        console.log(error.message);
      } finally {
      }
    };
  
    useEffect(()=>{
      getyears3Data();
    },[])

  return (
    <div className="">
      <Link href={{
        pathname: "/tools/harga_pasar/detail",
        query: { 
          id: data.kywd,
          dataId: data.id },
        }}>
        <div className="card card_harga_pasar mb-3">
          <div className="card-body border-shadow">
            <div className="date">
              {returnBulan} {data.thn}
            </div>
            <div className="d-flex justify-content-between align-items-center">
              <h5
                style={{ textTransform: "capitalize", fontFamily: "Helvetica" }}
              >
                {data.kywd}
              </h5>
              <div id="hargaPasar_id" className="price price-hide">
              {
                !yearsData ? <img src="/images/bars.gif" style={{width: 20, height: 20}} alt="" />
                :
                yearsData.thn0 > yearsData.thn1 ? 
                <i
                  style={{ color: "#77E3AF" }}
                  className="bi bi-caret-up-fill text-success me-1"></i>
                :
                <i style={{ color: "#cf1527" }} className="bi bi-caret-down-fill danger"></i>
                
              }
               
                <NumberFormat
                  value={data.hrg}
                  displayType={"text"}
                  prefix={"Rp"}
                  thousandSeparator="."
                  decimalSeparator=","
                />
              </div>
            </div>
            <div className="display-flex-hargapasar">
              <div
                id="desc_id_hargaPasar"
                className="d-flex desc align-items-center "
              >
                <div className="item" style={{ fontWeight: "400" }}>
                  <strong style={{ fontWeight: 700 }}>LB</strong> {data.ls_bgn} m
                  <sup>2</sup>
                </div>
                <div className="item" style={{ fontWeight: "400" }}>
                  <strong style={{ fontWeight: 700 }}>LT</strong> {data.ls_tnh} m
                  <sup>2</sup>
                </div>
              </div>
              <div id="price_none" className="price">
              {
                !yearsData.thn1 ? <img src="/images/bars.gif" style={{width: 20, height: 20}} alt="" />
                :
                yearsData.thn0 > yearsData.thn1 ? 
                <i
                  style={{ color: "#77E3AF" }}
                  className="bi bi-caret-up-fill text-success me-1"></i>
                :
                <i style={{ color: "#cf1527" }} className="bi bi-caret-down-fill danger"></i>
              }
                <NumberFormat
                  value={data.hrg}
                  displayType={"text"}
                  prefix={"Rp"}
                  thousandSeparator="."
                  decimalSeparator=","
                />
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}
