import axios from "axios";
import Link from "next/link";
import { useState, useEffect } from "react";
import NumberFormat from "react-number-format";
import { useAppContext } from "../../context";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function ItemPengajuanDalam(props) {
  const { data } = props;
  const {userProfile} = useAppContext()
  const listJenisKredit = data.typ_pgjn == 1 ? data.jenisKreditKonvensional : data.jenisKreditSyariah
  const nameJenisKredit = listJenisKredit?.find(item => item.kd == (data.jns_krdt || data.sft_krdt))
  const [statusDoc, setStatusDoc] = useState({})
  // process.env.NEXT_PUBLIC_UNIX_TIMESTAMP didapat dari Date.now() saat akan promote digital signature
  const d = new Date(data?.tgl_eln?.toString()).valueOf()
  console.log({d})
  console.log(d > process.env.NEXT_PUBLIC_UNIX_TIMESTAMP)

  /* --------------------------- FETCH IMAGE -------------------------- */
  const [coverImage, setCoverImage] = useState("");
  const [isFetchingCoverImage, setIsFetchingCoverImage] = useState(true);

  useEffect(() => {
      // if (data) {
        fetchCoverImage()
      // }
  }, [])

  const checkDocument = async (id) => {
      try {
        const body = {
          userId: userProfile.id,
          kprId: id
        }
          const {data} = await axios.post(process.env.NEXT_PUBLIC_API_HOST_DIGITAL_SIGNATURE + "/documentCheck", body)

          setStatusDoc(data?.code)
      } catch (error) {
          console.log(error);
          setStatusDoc(error.response.data.code);
          // setIsSuccess(-1)
      }
  }
  // hide-digital-signature
  useEffect(() => {
      if(data.st_pgjn_eloan !== "Disimpan") {
          checkDocument(data.id)
      }
  }, [data.id])
  
  const fetchCoverImage = async () => {
      try {
          setIsFetchingCoverImage(true);
          const params = `id_kav=${data.i_prpt}`
          const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST_OPEN_API}/v1/manstok/getTipeRumahByIdKav?${params}`, {
              method: 'GET',
              headers: {
                  app_key: process.env.NEXT_PUBLIC_APP_KEY,
                  secret_key: process.env.NEXT_PUBLIC_APP_SECRET,
                  ...defaultHeaders
              },
          });
          const resData = await res.json();
          setIsFetchingCoverImage(false);
          if (!resData.IsError) {
            setCoverImage(resData.data.GBR1);
            console.log("GAMBAR", resData.data.GBR1)
          } else {
              throw {
                  message: resData.ErrToUser
              };
          };
      } catch (error) {
          console.log("Error fetch properti search " + error.message);
      } finally {
          // setIsFetchingCoverImage(false);
      }
  }


  const date = new Date(data.t_ins)
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  const formattedDate = date.toLocaleDateString("id", options)
  return (
    <div id="card_detail_pengajuan">
      <div className="card card_pengajuan">
        <div className="card-header ">
          <div
            className="kode"
            style={{fontSize:"10px", fontFamily: "FuturaBT", fontWeight: "700" }}
          >
            {data.id ? data.id : "-"} &middot; <span>{formattedDate ? formattedDate : "-"}</span>
          </div>
          <div
            className="status"
            style={{ fontFamily: "FuturaBT", fontWeight: "700", opacity: 0.6 }}
          >
            {data.st_pgjn_eloan ? data.st_pgjn_eloan : "-"}
          </div>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-md-4">
              <div className="card_properti">
                <img src={coverImage && generateImageUrl(coverImage)} className="img-fluid" />
                <div className="detail">
                  <h6>{data.n_prmhn}</h6>
                  <h5>
                    <NumberFormat
                      value={data.hrg_jl}
                      displayType={"text"}
                      prefix={"Rp"}
                      thousandSeparator="."
                      decimalSeparator=","
                    />
                  </h5>
                  <Link href={{
                    pathname: "/property/perumahan/detail",
                    query: {id: data.i_prmhn, page: 1}
                  }}>Lihat Properti</Link>
                </div>
              </div>
            </div>
            <div className="col-md-7 offset-md-1">
              <div className="row">
                <div className="col-6 item">
                  <label>Nama</label>
                  <strong>{data.nm_lgkp ? data.nm_lgkp : "-"}</strong>
                </div>
                <div className="col-6 item">
                  <label>Jenis KPR</label>
                  <strong>{data.typ_pgjn ? (data.typ_pgjn == 1 ? "Konvensional" : "Syariah") : "-"}</strong>
                </div>
                <div className="col-6 item">
                  <label>Jenis kredit</label>
                  <strong>{nameJenisKredit ? nameJenisKredit.nl : "-"}</strong>
                </div>
                <div className="col-6 item">
                  <label>nilai Pengajuan</label>
                  <strong>
                    <NumberFormat
                      value={data.nl_pgjn ? data.nl_pgjn : "-"}
                      displayType={"text"}
                      prefix={"Rp"}
                      thousandSeparator="."
                      decimalSeparator=","
                    />
                  </strong>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="card-footer"
          style={{
            marginLeft: "24px",
            marginRight: "24px",
          }}
        >
          {/* hide detail KPR */}
            {/* <Link href={"/ajukan_kpr?id=" + data.id}>
              <a
                style={{ width: "177px", height: "48px", padding: "11px" }}
                className="btn btn-main"
              >
                Lanjutkan Pengisian
              </a>
            </Link> */}
          {data.st_pgjn_eloan === "Disimpan" ? (
            <Link href={"/ajukan_kpr?id=" + data.id}>
              <a
                style={{ width: "177px", height: "48px", padding: "11px" }}
                className="btn btn-main"
              >
                Lanjutkan Pengisian
              </a>
            </Link>
          ) : (
                        
            <div style={{display: "flex", justifyContent: "flex-end"}}>
              {/* digital signature part =========== comment the code below to hide-digital-signature ================== */}
                {
                    statusDoc === "DOCUMENT_NOT_FOUND" && d > process.env.NEXT_PUBLIC_UNIX_TIMESTAMP &&
                <a
                href={"/upload_doc/?kprId=" + data.id}
                className="btn btn-outline-main btn_rounded  w-100"
                style={{
                maxWidth: "200px",
                padding: "12px",
                fontFamily: "Helvetica",
                fontSize: "14px",
                fontWeight: 700,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "200px",
                marginRight: 30
                }}
                >
                    Penandatanganan
                </a> 
                }
                {/* ==================================================================================== */}

                    
                    <a href={"/member/kredit/detail?id=" + data.id} className="btn btn-main" style={{ width: 179, height: 48 }}>
                        Lihat Detail
                    </a>
            </div>
            
        )}
        </div>
      </div>
    </div>
  );
}


const generateImageUrl = (url) => {
  if (!url) return '/images/thumb-placeholder.png';
  console.log("URL", url)
  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.replace("1|", "")}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;
  
  if (isImageExist) {
    return fullUrl
  } else {
    return '/images/thumb-placeholder.png';
  }
}