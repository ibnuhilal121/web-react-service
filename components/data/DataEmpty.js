import React from 'react'

export default function DataEmpty(props) {
    const {title} = props;
    return (
        <div>
            <div className="text-center" style={{marginTop:"240px"}}>
                <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M53.3337 3.33398H13.3337C9.66699 3.33398 6.66699 6.33398 6.66699 10.0007V56.6673H13.3337V10.0007H53.3337V3.33398ZM50.0003 16.6673L70.0003 36.6673V70.0006C70.0003 73.6673 67.0003 76.6673 63.3337 76.6673H26.6337C22.967 76.6673 20.0003 73.6673 20.0003 70.0006L20.0337 23.334C20.0337 19.6673 23.0003 16.6673 26.667 16.6673H50.0003ZM46.667 40.0006H65.0003L46.667 21.6673V40.0006Z" fill="#0061A7" fill-opacity="0.75"/>
                </svg>
                <h5 style={{fontFamily: "Helvetica",
fontStyle: "normal",
fontWeight: "bold",
fontSize: "16px",
lineHeight: "150%",
marginTop:"40px",
color: "#000000"}}>{title}</h5>

            </div>
        </div>
    )
}
