import Link from "next/link";
import Image from "next/image";
import limitString from "../../utils/limitString";
import styles from "./ItemPromo.module.scss";
import ReactImageFallback from "react-image-fallback";
import { useMediaQuery } from "react-responsive";

export default function ItemPromo(props) {
  const { data } = props;

  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const date = data.date.split("/");
  const year = date[2] ? date[2].split(" ") : date;
  const pathseo = data.seo ? `${data.seo}-${data.id}` : data.title ? `${data.title.split(" ").join("-")}-${data.id}` : data.id;
  // const year = date[2].slice(0, 4);

  // console.log("ini tanggal dan tahun", year);
  console.log('data promo : ', data)

  const bulan = [
    { id: 1, value: "Januari" },
    { id: 2, value: "Februari" },
    { id: 3, value: "Maret" },
    { id: 4, value: "April" },
    { id: 5, value: "Mei" },
    { id: 6, value: "Juni" },
    { id: 7, value: "Juli" },
    { id: 8, value: "Agustus" },
    { id: 9, value: "September" },
    { id: 10, value: "Oktober" },
    { id: 11, value: "November" },
    { id: 12, value: "Desember" },
  ];

  const mounth = bulan
    .filter((item) => item.id == date[1])
    .map((item) => item.value);

  console.log("ini bulan", mounth);

  return (
    <div className="card card_news_vertical custom w-100" style={{padding: isTabletOrMobile ? "10px" : "0px"}}>
      <div className="card_img" style={{
        //maxWidth: "308px"
      }}>
         <Link href={{pathname:`/promo/detail`,
                    query:{
                      id: pathseo
                    }
        }}>
          <a>
            <div id="promo_image" className={`w-100 ${styles.blurImageParent}`}>
              {/* IMAGE FOR BLUR MASK */}
              {/* <img
              src={data.images}
              alt={data.title}
              className={styles.blurImageChild}
              layout="responsive"
            /> */}
              <ReactImageFallback
                src={
                  data.images
                    ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                    data.images.replace("1|", "")
                    : null
                }
                fallbackImage="/images/thumb-placeholder.png"
                className={`w-100 ${styles.blurImageChild}`}
              />

              {/* IMAGE FOR CONTENT */}
              {/* <img
              src={data.images}
              alt={data.title}
              className={styles.blurImageChild}
              layout="responsive"
            /> */}
              <ReactImageFallback
                src={
                  data.images
                    ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                    data.images.replace("1|", "")
                    : null
                }
                fallbackImage="/images/thumb-placeholder.png"
                className={`w-100 ${styles.blurImageChild}`}
                layout="responsive"
              />
            </div>
          </a>
        </Link>
      </div>

      <div id="promo_content" className="card-content" style={{padding: isTabletOrMobile ? "10px" : "0px"}}>
        <Link href={{pathname:`/promo/detail`,
                    query:{
                      id: pathseo
                    }
        }}>
          <a>
            <h5
              className="card-title"
              style={{
                fontFamily: "Helvetica",
                fontSize: 20,
                fontStyle: "normal",
                fontWeight: "700",
                lineHeight: "150%",
                marginBottom: 4,
              }}
            >
              {data.title}
            </h5>
          </a>
        </Link>
        <div className="d-flex meta_post">
          <h6
            style={{
              fontFamily: "Helvetica",
              fontSize: 12,
              fontStyle: "normal",
              fontWeight: "700",
              lineHeight: "150%",
            }}
          >
            {data.kategori}
          </h6>
          <div
            style={{
              fontFamily: "Helvetica",
              fontSize: 12,
              fontStyle: "normal",
              fontWeight: "400",
              lineHeight: "150%",
            }}
          >
            / {date[0]} {mounth} {year[0]}
          </div>
        </div>
        <Link
          href={{
            pathname:"/promo/detail",
            query:{id: pathseo
            }}}
          // href={
            
          //   parhName:/promo/
          //   `/promo/${pathseo}`}
          style={{
            fontFamily: "Helvetica",
            fontSize: 14,
            fontStyle: "normal",
            fontWeight: "700",
            lineHeight: "24px",
            marginTop: 14,
          }}
        >
          Lihat lebih
        </Link>
      </div>
    </div>
  );
}
