import Link from "next/link";
import NumberFormat from "react-number-format";
import ReactImageFallback from "react-image-fallback";
import LikeButton from "../element/LikeButton";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { useMediaQuery } from "react-responsive";
import { breakWord } from "../../helpers/breakWord";
import { useIdToLabel } from "../../helpers/pencarianProperti";
// import { generateImageUrl } from "../../helpers/generateImageUrl";
import { useEffect, useState } from "react";

export default function ItemUnit(props) {
  const { data, wrapperClass, view } = props;
  const { JENIS, ID, NAMA_PROPER, NAMA_DEV, ID_DEV } = data;

  const devSeo = `${NAMA_DEV?.split(" ").join("-")}-${ID_DEV}`

  const generateCategory = (value) => {
    const valueStr = value?.toString() || "";
    switch (valueStr) {
      case "1":
        return "RUMAH";
      case "2":
        return "RUMAH BEKAS";
      case "3":
        return "APARTEMEN";
      case "4":
        return "LELANG";
      case "5":
        return "APARTEMEN BEKAS";
      case "6":
        return "RUKO";
      case "7":
        return "KANTOR";
      default:
        return "LAIN-LAIN";
    };
  };

  const idToLabel = useIdToLabel();
  const tipeProperti = idToLabel(JENIS);
  const seo = `${NAMA_PROPER?.split(" ").join("-")}-${ID}`
  const tipeLow = tipeProperti.toLowerCase();
  const tipeSlug = tipeLow != "uncategorized" ? tipeLow : "";
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  // console.log("seo ", seo)
  
  const [imageUrl, setImageUrl] = useState("/images/thumb-placeholder.png");
  const [imageFit, setImageFit] = useState(true);
  const [imageLandscape, setImageLandscape] = useState(true);

  const [logoUrl, setLogoUrl] = useState("/images/thumb-placeholder.png");

  useEffect(() => {
    if (!data.GBR1) return;
    generateImageUrl(data.GBR1?.split("|")[1], (url) => {
      setImageUrl(url);
      const image = new Image();
      image.src = url;
      setImageLandscape(image.width >= image.height)
      setImageFit(image.width === image.height);
    })
    if (!data.LOGO) return;
    generateImageUrl(data.LOGO.split("|")[1], (url) => {
      setLogoUrl(url);
    })
  }, []);

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const formatter = (e) => {
    const million = 1000000;
    if (e > 999 && e < million) {
      return (e / 1000).toFixed(0) + ' rb/bln';
    } else if (e > million) {
      return (e / million).toFixed(1) + ' jt/bln';
    } else if (e < 900) {
      return e; // if value < 1000, nothing to do
    }
  };

  const CustomTooltips = () => (
    <OverlayTrigger
      placement="right"
      overlay={
        <Tooltip id="tooltip-right">
          {
            <>
              <NumberFormat value={data.pmt_konvensional != null ? data.pmt_konvensional : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
              /Bln
            </>
          }
        </Tooltip>
      }
    >
      {data.pmt_konvensional != null ? (
        <p
          className="fw-bold m-0"
          style={{
            color: "#00193E",
            fontSize: "12px",
            cursor: "pointer",
            display: "inline-block",
          }}
        >
          Rp{formatter(data.pmt_konvensional) || "-"}
        </p>
      ) : (
        <p
          className="fw-bold m-0"
          style={{
            color: "#00193E",
            cursor: "pointer",
            display: "inline-block",
          }}
        >
          <NumberFormat value={data.pmt_konvensional != null ? data.pmt_konvensional : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
        </p>
      )}
    </OverlayTrigger>
  );
  return (
    <div className={wrapperClass ? `${wrapperClass} card card_unit_properti` : "card card_unit_properti"}>
      <div className="card_img"
        style={isTabletOrMobile ? { minWidth: '130px', overflow: "hidden" } : {overflow: "hidden"}}>
      {/* <div className="card_img" style={isTabletOrMobile ? { minWidth: '120px' } : {}}> */}
        {!isTabletOrMobile ?
          <div className="ratio ratio-1x1">
            <Link href={{
              pathname:"/property/perumahan/detail",
              query:{id: seo, page: 1
              }
            }}>
              <a>
                <div
                  style={{
                    background: `url(${imageUrl})`,
                  }}
                  className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                />
                <div className="container-img-card d-flex" style={{ height: "100%" }}>
                  <img
                    src={imageUrl}
                    className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                    style={{ width: imageLandscape ? "100%" : "auto", borderRadius: 0 }}
                  />
                </div>
              </a>
              {/* <a>
                <ReactImageFallback src={data.GBR1 ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.GBR1.replace("1|", "") : null} fallbackImage="/images/thumb-placeholder.png" className="img-fluid" title={data.NAMA_PROPER} />
              </a> */}
            </Link>
          </div> :
          <div id="image-cover" className="background-cover ratio ratio-1x1"
            style={{ minHeight: '200px' }}>
            <Link href={{
              pathname:"/property/perumahan/detail",
              query:{id: seo, page: 1
              }
            }}>
              <a>
                <div
                  style={{
                    background: `url(${imageUrl})`,
                  }}
                  className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                />
                <div className="container-img-card">
                  <img
                    src={imageUrl}
                    className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                  />
                </div>
              </a>
              {/* <a>
                <ReactImageFallback style={{
                  width: "100%",
                  objectFit: "cover",
                  minHeight: "200px"
                }} src={data.GBR1 ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.GBR1.replace("1|", "") : null} fallbackImage="/images/thumb-placeholder.png" className="img-fluid" title={data.NAMA_PROPER} />
              </a> */}
            </Link>
          </div>
        }
        {tipeProperti && (
        <Link
          href={{
            pathname: "/property",
            query: { tipeProperti : JENIS },
          }}
          passHref
        >
          <a className="link">
            <div id="kategori-pencarian" className={"kategori text-center " + (tipeSlug || "rumah")} style={isTabletOrMobile ? { top: '17px' } : {}}>
              {tipeProperti}
            </div>
          </a>
        </Link>
        )}
        <LikeButton propId={data.ID} />
        {data.warranty == 1 && (
          <div className="warranty">
            <img src="/images/acc/warranty.png" className="img-fluid" />
          </div>
        )}
      </div>

      <div style={{overflow:"hidden"}} className="card-body">
        <h6 style={{ fontSize: "10px" }} className="lokasi">
          {data.KOTA? data.KOTA : "-"}
        </h6>
        <Link href={{
          pathname: "/property/perumahan/detail",
          query: {id: seo, page: 1}
        }} passHref>
          <a>
            <h5 className="title" title={data.NAMA_PROPER} style={(isTabletOrMobile && view === 'map') ? { whiteSpace: 'nowrap' } : {}}>
              {(isTabletOrMobile && view === 'map') ? breakWord(data.NAMA_PROPER, 15) : data.NAMA_PROPER}
            </h5>
          </a>
        </Link>
        <h5
          style={{
            fontSize: "16px",
            flexWrap: "wrap"
          }}
          className={`price d-flex ${isTabletOrMobile ? 'flex-column justify-content-start align-items-start' : ''}`}
        >
          <div className="mulai d-flex me-1" style={{ fontSize: "12px",alignItems:"center"}}>
            <p className="m-auto">
            Mulai dari
            </p>
          </div> <NumberFormat style={{overflow:"hidden",display:"flex",alignItems:"center"}} value={data.HARGA_MULAI != null ? data.HARGA_MULAI * 1 : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
        </h5>

        <div className="d-flex justify-content-start flex-column flex-xl-row ">
          <div id="item_cicilan">
            <p className="one-line-flex"
              style={{
                margin: "0px",
                color: "#666666 ",
              }}
            >
              Cicilan dari <CustomTooltips />{" "}
            </p>
          </div>
          <span
            id="divider_card"
            style={{
              position: "relative",
              top: "3px",

              borderRight: "1px #666666 solid",
              height: "12px",
              width: "0px",
            }}
          />
          <div id="item_bunga">
            <p className="one-line-flex"
              style={{
                
                margin: "0px",
                color: "#666666 ",
              }}
            >
              Suku Bunga dari <b style={{ color: "#00193E" }}> {data.sk_bga || "-"}% </b>
            </p>
          </div>
        </div>
        <Link href={
          {
            pathname:"/developer/detail",
            query: {
              id: devSeo
            }
          }
        } passHref>
          <a>
            <div className="developer">
              <div className="logo">
                <img src={logoUrl} className="img-fluid" />
              </div>
              <div
                style={{
                  width: "80%",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
                className="developer-name"
              >
                {(isTabletOrMobile && view === 'map') ? breakWord(data.NAMA_DEV, 15) : data.NAMA_DEV}
              </div>
            </div>
          </a>
        </Link>
      </div>
    </div>
  );
}

const generateImageUrl = (url, callback) => {
  if (!url) return "/images/thumb-placeholder.png";

  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url?.split('"')[0]}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;
  img.onload = () => {
    callback(fullUrl);
  };
  // img.onerror = () => {
  //   callback("/images/thumb-placeholder.png");
  // };
};