import Link from "next/link";
import NumberFormat from "react-number-format";
import ReactImageFallback from "react-image-fallback";
import { useRouter } from "next/router";
import { useState } from "react";
import { useEffect } from "react";
import LoveIcon from "../element/icons/LoveIcon";
import LoveOutlinedIcon from "../element/icons/LoveOutlinedIcon";

import { deleteIklan } from "../../services/member";
import { useAppContext } from "../../context";
import { useMediaQuery } from "react-responsive";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

const setupStatus = {
  0: {
    status: 'Draft',
    style: 'bg-secondary'
  },
  1: {
    status: 'Menunggu Konfirmasi',
    style: 'bg-info'
  },
  2: {
    status: 'Revisi',
    style: 'bg-warning text-dark'
  },
  3: {
    status: 'Dalam Proses',
    style: 'bg-info text-dark'
  },
  4: {
    status: 'Disetujui',
    style: 'bg-success'
  },
  5: {
    status: 'Ditolak',
    style: 'bg-danger'
  },
  6: {
    status: 'Dibatalkan',
    style: 'bg-danger'
  }
}



export default function ItemUnitFavorite(props) {
  const isTabletOrMobile = useMediaQuery({
    query: "(min-width: 768px) and (max-width: 948px)",
  });
  const { data, compare, index, selected, getProperti,setCompare, disableDev,handleLikeButton } = props;
  const { jns, clstr, id, n } = data;
  const [imageUrl, setImageUrl] = useState("/images/thumb-placeholder.png");
  const [imageFit, setImageFit] = useState(true);
  const [imageLandscape, setImageLandscape] = useState(true);
  useEffect(() => {
    if (!data.gbr1) return;
    generateImageUrl(data.gbr1?.split("|")[1], (url) => {
      setImageUrl(url);
      const image = new Image();
      image.src = url;
      setImageLandscape(image.width >= image.height)
      setImageFit(image.width === image.height);
    })
  }, []);

  
  const generateCategory = (value) => {
    const valueStr = value?.toString() || "";
    switch (valueStr) {
      case "1":
        return "RUMAH";
      case "2":
        return "RUMAH BEKAS";
      case "3":
        return "APARTEMEN";
      case "4":
        return "LELANG";
      case "5":
        return "APARTEMEN BEKAS";
      case "6":
        return "RUKO";
      case "7":
        return "KANTOR";
      default:
        return "";
    };
  };

  const { userKey, userProfile,likeAction } = useAppContext();
  const router = useRouter();
  const { pathname } = router
  const { komparasi } = router.query;
  // let tipeProperti;
  // if(data.id.substring(0, 3) === "TPR"){
  //    tipeProperti = jns == 1 ? "RUMAH BARU" : jns == 2 ? "RUMAH SECOND" : jns == 3 ? "APARTEMEN" :jns == 4 ? "LELANG" : "UNCATEGORIZED";
  // } else {
  //    tipeProperti = jns == "1" ? "RUMAH" : jns == "2" ? "APARTEMEN" : "UNCATEGORIZED";
  // }
  const tipeProperti = generateCategory(jns);
  const tipeLow = tipeProperti.toLowerCase();
  const tipeSlug = tipeLow != "uncategorized" ? tipeLow : "";
  const [borderColor, SetBorderColor] = useState(false);
  const seo = clstr ? `${clstr.split(" ").join("-")}-${id}`: `${id}`;
  const seoPerum = n ? `${n.split(" ").join("-")}-${id}`:`${id}`;
  console.log("seoPerum ", seoPerum)
  console.log("seo ", seo)

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const formatter = (e) => {
    const million = 1000000;
    if (e >= million) {
      return `${Math.floor(e / million)}jt/bln`;
    } else {
      return `${Math.round(numberWithCommas(e))}rb/bln`;
    }
  };
  const CustomTooltips = () => (
    <OverlayTrigger
      placement="right"
      overlay={
        <Tooltip id="tooltip-right">
          {
            <>
              <NumberFormat value={data.pmt_konvensional != null ? data.pmt_konvensional : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
              /Bln
            </>
          }
        </Tooltip>
      }
    >
      {data.pmt_konvensional != null ? (
        <p
          className="fw-bold m-0"
          style={{
            color: "#00193E",
            fontSize: "12px",
            cursor: "pointer",
            display: "inline-block",
          }}
        >
          Rp{formatter(data.pmt_konvensional) || "-"}
        </p>
      ) : (
        <p
          className="fw-bold m-0"
          style={{
            color: "#00193E",
            cursor: "pointer",
            display: "inline-block",
          }}
        >
          <NumberFormat value={data.pmt_konvensional != null ? data.pmt_konvensional : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
        </p>
      )}
    </OverlayTrigger>
  );

  const CSSVariables = {
    border: {
      border: "2px solid rgba(0, 97, 167, 0.75)",
    },
    noBorder: {
      border: "none",
    },
  };

  const compareSelect = (data) => {
    if (!borderColor) {
      if (compare.length != 4) {
        //SetBorderColor(true);
        props.dataCompare(data);
      }
    }
  };

  useEffect(() => {
    if (komparasi == "1") {
      SetBorderColor(false);
      if (compare?.length > 0) {
        compare
          .filter((list) => list.id.includes(data.id))
          .map((filtered) => {
            SetBorderColor(true);
          });
      }
    }
  }, [compare]);

  const handleDelete = (id) => {
    deleteIklan(id, userKey)
      .then((res) => {
        // getProperti();
        window.location.reload();
      })
      .catch((error) => {});
  };

 
  return (
    <>
     
        <div className="card card_unit_properti">
          <div className="card_img"
            style={{ overflow: "hidden"}}
          >
            <div className="ratio ratio-1x1">
            <Link href={pathname === "/member/favorit" || pathname === "/member/favorit/" ? 
              (data.id.substring(0, 3) === "TPR" ? {pathname: "/property/tipe/detail", query: {id: seo}} : {pathname: "/property/perumahan/detail", query: {id: seoPerum, page: 1}}) :
              {pathname: "/property/tipe/detail", query: {id: seo}}}>
                {router.pathname === '/member/properti' ?
                (
                  <a>
                    <div
                      style={{
                        background: `url(${imageUrl})`,
                      }}
                      className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                    />
                    <div className="container-img-card" style={{ height: "100%" }}>
                      <img
                        src={imageUrl}
                        className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                        style={{borderRadius: 0}}
                      />
                    </div>
                  </a>
                ) :
                (
                  <a>
                     <div
                      style={{
                        background: `url(${imageUrl})`,
                      }}
                      className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
                    />
                    <div className="container-img-card" style={{ height: "100%" }}>
                      <img
                        src={imageUrl}
                        className={`img-fluid ${imageLandscape ? "w-100" : "h-100"} `}
                        style={{borderRadius: 0}}
                      />
                    </div>
                  </a>
                )}
              </Link>
            </div>
            {jns && (
            <Link
              href={{
                pathname: "/property/tipe",
                query: { tipeProperti : jns == 1 || jns == 2 ? '1' : jns == 3 ? '2' : '' },
              }}
              passHref
            >
              <a className="link">
                <div id="kategori-pencarian" className={"kategori " + (tipeSlug || "rumah")} style={{ fontSize: "12px", textTransform: "uppercase" }}>
                  {data.kategori != null ? data.kategori : tipeProperti}
                </div>
              </a>
            </Link>
            )}
            <LikeButton handleLike={()=>{handleLikeButton(data.id)}} propId={data.id} />
          </div>

          <div className="card-body">
            <h6
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: "10px",
                lineHeight: "130%",
              }}
              className="lokasi"
            >
              {data.lokasi || data.n_kot || "-"}
            </h6>
            {pathname === "/member/favorit" || pathname === "/member/favorit/" ? (
              <Link href={data.id.substring(0, 3) === "TPR" ? {pathname: "/property/tipe/detail", query: {id: seo}} : {pathname: "/property/perumahan/detail", query: {id: seoPerum, page: 1}}}>
                {pathname === "/member/favorit" || pathname === "/member/favorit/" ? ( 
                  <a>
                    {data.id.substring(0, 3) === "TPR" ? (
                      <h5
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: "700",
                          fontSize: "20px",
                          lineHeight: "150%",
                        }}
                        className="title"
                        title={`${data.n_perum} | ${data.clstr} | ${data.blk}`}
                      >
                        {data.n_perum} | {data.clstr} | {data.blk}
                      </h5>
                    ) : (
                      <h5
                        style={{
                          fontFamily: "Helvetica",
                          fontWeight: "700",
                          fontSize: "20px",
                          lineHeight: "150%",
                        }}
                        className="title"
                        title={data.title || data.jdl}
                      >
                        {data.n || data.jdl}
                      </h5>
                    )}
                  </a>
                  // END
                ) : tipeProperti == "RUMAH" || tipeProperti == "APARTEMEN" ? (
                  <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.n} | {data.BLOK} | {data.KLASTER}
                    </h5>
                  </a>
                ) :  data.title != null || data.jdl != null ? (
                  <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.title || data.jdl}
                    </h5>
                  </a>
                ) : (
                  <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={`${data.clstr} | ${data.BLOK} | ${data.NAMA}`}
                    >
                      {data.clstr} | {data.BLOK} | {data.NAMA}
                    </h5>
                  </a>
                )}
              </Link>
            ) : (
              <Link href={{pathname: "/property/tipe/detail", query: {id: seo}}} passHref>
              {pathname === "/property/tipe" || pathname === "/property/tipe/" ? (
                // khusus untuk dipakai di halaman pencarian property tipe
                // untuk halaman lain silakan pakai markup di bawahnya
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={`${data.n_perum} | ${data.clstr} | ${data.blk}`}
                  >
                    {data.n_perum} | {data.clstr} | {data.blk}
                  </h5>
                </a>
                // end of untuk dipakai di halaman pencarian property tipe

                // khusus untuk dipakai di halaman Favorite !!!
              ) : pathname === "/member/favorit" || pathname === "/member/favorit/" ? ( 
                <a>
                  {data.id.substring(0, 3) == "TPR" ? (
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={`${data.n_perum} | ${data.clstr} | ${data.blk}`}
                    >
                      {data.n_perum} | {data.clstr} | {data.blk}
                    </h5>
                  ) : (
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.n || data.jdl}
                    </h5>
                  )}
                </a>
                // END
              ) : pathname === "/member/properti" || pathname === "/member/properti/" ? ( 
                <a>
                    <h5
                      style={{
                        fontFamily: "Helvetica",
                        fontWeight: "700",
                        fontSize: "20px",
                        lineHeight: "150%",
                      }}
                      className="title"
                      title={data.title || data.jdl}
                    >
                      {data.n || data.jdl}
                    </h5>
                </a>
                // END
              ) : tipeProperti == "RUMAH" || tipeProperti == "APARTEMEN" || tipeProperti =="RUMAH BEKAS" ? (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={`${data.n} | ${data.BLOK} | ${data.KLASTER}`}
                  >
                     {data.n} | {data.BLOK} | {data.KLASTER}
                  </h5>
                </a>
              ) :  data.title != null || data.jdl != null ? (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={data.title || data.jdl}
                  >
                    {data.title || data.jdl}
                  </h5>
                </a>
              ) : (
                <a>
                  <h5
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "700",
                      fontSize: "20px",
                      lineHeight: "150%",
                    }}
                    className="title"
                    title={`${data.NAMA} | ${data.BLOK} | ${data.clstr}`}
                  >
                    {data.NAMA} | {data.BLOK} | {data.clstr}
                  </h5>
                </a>
              )}
            </Link>
            )}
            {router.pathname === '/member/properti' &&
            (
              <div 
                className={`badge rounded-pill d-flex justify-content-center align-items-center ${setupStatus[data.st_pgjn].style}`} 
                style={{height: '15px', width: 'fit-content'}}
              >
                <span style={{
                  fontSize: '10px', 
                  verticalAlign: 'middle',
                  fontFamily: "Helvetica"
                }}>
                  {setupStatus[data.st_pgjn].status}
                </span>
              </div>
            )}
            <h5
              style={{
                fontFamily: "FuturaBT",
                fontWeight: "700",
                fontSize: isTabletOrMobile ? "13px" : "16px",
                lineHeight: "160%",
              }}
              className="price one-line-flex"
            >
              <NumberFormat value={data.price != null || data.hrg != null ? data.price || data.hrg : data.hrg_rdh != null ? data.hrg_rdh * 1 : "-"} displayType={"text"} prefix={"Rp"} thousandSeparator="." decimalSeparator="," />
            </h5>
            <div className="d-flex justify-content-md-start flex-column flex-md-row ">
              <div
              id="cicilan_amount"
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                 
                  lineHeight: "160%",
                }}
                className="cicilan"
              >
                Cicilan dari{" "}
                <span
                  id="item_cicilan"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    
                    lineHeight: "160%",
                  }}
                >
                  <CustomTooltips />
                </span>
              </div>
              <span
                id="divider_card"
                style={{
                  position: "relative",
                  top: "4px",

                  borderRight: "1px #666666 solid",
                  height: "12px",
                  width: "0px",
                }}
              />
              <div
                id="item_bunga"
                style={{
                  fontFamily: "Helvetica",
                  fontWeight: "400",
                  color: "#666666",
                  whiteSpace:"nowrap"
                }}
              >
                Suku Bunga dari{" "}
                <span
                  style={{
                    color: "#00193e",
                    fontFamily: "Helvetica",
                    fontWeight: "700",
                    lineHeight: "160%",
                  }}
                  className="price"
                >
                  {data.suku_bunga != null ? data.suku_bunga : data.sk_bga}%
                </span>{" "}
              </div>
            </div>
            {disableDev ? 
              <div>
                <div className="developer py-2 my-0">
                  <div className="logo">
                    {/* <img
                      src={ data.developer_avatar != null ? data.developer_avatar :
                        `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                        data.g_dev?.replace("1|", "")
                      }
                      className="img-fluid"
                    /> */}
                    <ReactImageFallback
                      src={data.developer_avatar || data.g_memb ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + (data.developer_avatar || data.g_memb).replace("1|", "") : null}
                      fallbackImage="/images/thumb-placeholder.png"
                      className="img-fluid"
                      style={{maxWidth:"20px",maxHeight:"20px"}}
                    />
                  </div>
                  <div
                    className="developer-name"
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "400",
                      fontSize: "12px",
                      lineHeight: "160%",
                    }}
                  >
                    {data.developer_name != null ? data.developer_name : data.n_dev || data.n_memb}
                  </div>
                </div>
              </div> :
            <Link href={
              {
                pathname:"/developer/detail",
                query: {
                  id: (data.i_dev || data.i_memb)
                }
              }
            } passHref>
              <a>
                <div className="developer py-2 my-0">
                  <div className="logo">
                    {/* <img
                      src={ data.developer_avatar != null ? data.developer_avatar :
                        `${process.env.NEXT_PUBLIC_IMAGE_URL}/` +
                        data.g_dev?.replace("1|", "")
                      }
                      className="img-fluid"
                    /> */}
                    <ReactImageFallback
                      src={data.developer_avatar || data.g_memb ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + (data.developer_avatar || data.g_memb).replace("1|", "") : null}
                      fallbackImage="/images/thumb-placeholder.png"
                      className="img-fluid"
                      style={{maxWidth:"20px",maxHeight:"20px"}}
                    />
                  </div>
                  <div
                    className="developer-name"
                    style={{
                      fontFamily: "Helvetica",
                      fontWeight: "400",
                      fontSize: "12px",
                      lineHeight: "160%",
                    }}
                  >
                    {data.developer_name != null ? data.developer_name : data.n_dev || data.n_memb}
                  </div>
                </div>
              </a>
            </Link> }
          </div>
          {props.detail == "true" && (
            <div className="card-footer"
            //  style={ {display: pathname === "/member/favorit" || pathname === "/member/favorit/" ? "none":""}}
            >
              <ul id="nav-item-unit" className="nav nav-pills nav-fill">
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="kamar-tidur" src="/images/icons/bed.png" className="img-fluid" height="22px" width="22px" /> {data.kamar_tidur != null ? data.kamar_tidur : data.km_tdr}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="kamar-mandi" src="/images/icons/shower.png" className="img-fluid" height="17.14px" width="17.14px" />
                  {data.kamar_mandi != null ? data.kamar_mandi : (data.km_mnd ? data.km_mnd : "-")}
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="luas-bangunan" src="/images/icons/lb.png" className="img-fluid" height="22px" width="22px" />
                  {data.lb != null ? data.lb : (data.ls_bgn ? data.ls_bgn : "-")}m<sup>2</sup>
                </li>
                <li
                  className="nav-item"
                  style={{
                    fontFamily: "Helvetica",
                    fontWeight: "400",
                    fontSize: "12px",
                    lineHeight: "160%",
                  }}
                >
                  <img id="luas-tanah" src="/images/icons/lt.png" className="img-fluid" height="22px" width="22px" />
                  {data.ls_tnh != null ? (data.ls_tnh ? data.ls_tnh : "-") : data.lt}m<sup>2</sup>
                </li>
              </ul>
            </div>
          )}
          {props.action == "true" && (
            <div className="card-action">
              <div className="row">
                <div className="col">
                  <Link href={`/member/update-properti?id=${data.id}`}>Edit</Link>
                </div>
                <div className="col">
                  <Link href="#">
                    <a href="" data-bs-toggle="modal" data-bs-target={"#delete" + data.id}>
                      Hapus
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          )}
          {props.banding == "true" && (
            <div className="bandingkan" onClick={()=>{setCompare(data)}} style={{cursor:"pointer"}}>
            <a 
              style={{
                color: "#0061a7",
                fontFamily: "Helvetica",
                fontWeight: 700,
                fontSize: "12px",
                fontStyle: "normal",
                lineHeight: "160%"
              }}
              >Bandingkan</a>
            </div>
          )}
        </div>
      
      <div className="modal fade" id={"delete" + data.id} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered mx-auto px-2">
          <div className="modal-content modal_delete h-100" style={{padding:"44px"}}>
            <div className="close-modal" data-bs-dismiss="modal">
              <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <div className="modal-body text-center pt-3">
              <h5 className="modal-title">Hapus Iklan Properti</h5>
              <p className="mb-5">Apakah Anda akan menghapus iklan ini?</p>

              <button onClick={() => handleDelete(data.id)} type="button" data-bs-dismiss="modal" className="btn btn-primary w-100 mt-4">
                Hapus
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const generateImageUrl = (url, callback) => {
  if (!url) return "/images/thumb-placeholder.png";

  const fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;
  img.onload = () => {
    callback(fullUrl);
  };
  // img.onerror = () => {
  //   callback("/images/thumb-placeholder.png");
  // };
};

const LikeButton = ({handleLike,propId }) => {
    const { userFavorites,userKey } = useAppContext();


    return (
        <div 
            className="like d-flex justify-content-center align-items-center" 
            style={{ lineHeight: "35px" }} 
            onClick={userKey ? handleLike : null}
            data-bs-toggle={userKey ? "" : "modal"}
            data-bs-target={userKey ? "" : "#modalLogin"}
        >
          {userFavorites?.includes(propId) ? <LoveIcon /> : <LoveOutlinedIcon />}
        </div>
    );
};