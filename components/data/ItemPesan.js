import Link from "next/link";
import axios from "axios";
import qs from "qs";
import { useEffect, useState } from "react";
import { useAppContext } from "../../context";
import parse from "html-react-parser";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function ItemPesan(props) {
  const { userKey, userProfile } = useAppContext();
  const { data, setLoading } = props;

  const [isChatWillDelete, setChatWillDelete] = useState(false);
  const [isUserDeleting, setUserDeleting] = useState(false);

  const handleDelete = (id) => {
    let endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pesan/delete`;
    let body = {
      i_cht: id,
    };
    let header = {
      headers: {
        AccessKey_Member: userKey,
        ...defaultHeaders
      },
    };

    axios
      .post(endpoint, qs.stringify(body), header)
      .then((res) => {
        if (res.data.IsError) {
          console.log(res.data.ErrToDev);
          // return console.log(res.data.ErrToUser)
        }
      })
      .catch((err) => {})
      .finally(() => {
        window.location.href = `${window.location.origin}/member/pesan?rld=pesan`;
      });
  };

  const saveToStorage = () => {
    const tempData = {
      judul: data.jdl,
      isi: data.lst_msg,
      kategori: "pesan",
    };
    sessionStorage.setItem("detailPesan", JSON.stringify(tempData));
  };

  return (
    <div id="card_profile_container" onClick={saveToStorage}>
      <Link href={{
            pathname:"/member/pesan/detail",
            query:{id: data.id
            }}} passHref> 
      
        <a href="#">
          <div id="card_pesan_profile" className={data.st_msg && JSON.parse(data.st_msg)[`M${userProfile?.id}`] === true ? "card card_pesan" : "card card_pesan dibaca"} style={{ height: '100%' }}>
            <div className="card-header ">
              <div style={{ opacity: "0.6" }} className="title">
                {data.lst_wkt
                  .split("T")
                  .join(", ")
                  .slice(0, data.lst_wkt.length - 2)}
              </div>
              <div style={{ top: "18px", left: "120px", position: "absolute" }} className={`badge ${data.lst_nm === "System" ? data.st_msg && JSON.parse(data.st_msg)[`M${userProfile?.id}`] === JSON.parse(data?.st_msg)[data?.lst_i_usr] ? "terkirim" : "dibalas" : "terkirim"}`}>
                {data.lst_nm === "System" ? "dibalas" : "terkirim"}
              </div>
              <div className="status">
                <a href="" data-bs-toggle="modal" data-bs-target={"#delete_pesan" + data.id}>
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M5.00033 15.8333C5.00033 16.75 5.75033 17.5 6.66699 17.5H13.3337C14.2503 17.5 15.0003 16.75 15.0003 15.8333V5.83333H5.00033V15.8333ZM15.8337 3.33333H12.917L12.0837 2.5H7.91699L7.08366 3.33333H4.16699V5H15.8337V3.33333Z"
                      fill="#0061A7"
                    />
                  </svg>
                </a>
              </div>
            </div>
            <div className="card-body">
              <h5>{data.jdl}</h5>
              <p dangerouslySetInnerHTML={{ __html: `${data.lst_msg}` }} style={{marginBottom: 0}}></p>
            </div>
          </div>
        </a>
      </Link>

      <div className="modal fade" id={"delete_pesan" + data.id} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered mx-auto px-2">
          <div className="modal-content modal_delete h-100" style={{padding:"44px"}}>
            <div className="close-modal" data-bs-dismiss="modal">
              <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.69217 0.290313C1.30509 -0.0967711 0.677502 -0.0967711 0.29042 0.290313C-0.0966614 0.677398 -0.096662 1.30498 0.29042 1.69207L6.59825 7.99994L0.290311 14.3079C-0.0967704 14.695 -0.0967704 15.3226 0.290311 15.7097C0.677393 16.0968 1.30498 16.0968 1.69206 15.7097L8 9.4017L14.3079 15.7096C14.695 16.0967 15.3226 16.0967 15.7096 15.7096C16.0967 15.3226 16.0967 14.695 15.7096 14.3079L9.40174 7.99994L15.7095 1.69212C16.0966 1.30503 16.0966 0.677444 15.7095 0.29036C15.3224 -0.0967244 14.6949 -0.096725 14.3078 0.290359L8 6.59819L1.69217 0.290313Z"
                />
              </svg>
            </div>
            <div className="modal-body text-center pt-3">
              <h5 className="modal-title">Hapus Pesan</h5>
              <p className="mb-5" >Yakin ingin menghapus Pesan?</p>

              <button onClick={() => handleDelete(data.id)} data-bs-dismiss="modal" type="button" className="btn btn-primary w-100 mt-4">
                Hapus
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
