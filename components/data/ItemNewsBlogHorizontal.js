import Link from "next/link";
import { convertDate } from "../../utils/DateConverter";
import ReactImageFallback from "react-image-fallback";
import parse from "html-react-parser";

export default function ItemNewsBlogHorizontal(props) {
  const { data } = props;

  return (
    <div style={{ width: "308px", marginRight: "16px" }} className="card card_news_vertical me-md-2">
      <div className="card_img">
        <Link
          href={{
            pathname:"/blog/detail",
            query:{id: data.id
            }}}
          >
          <a>
            <ReactImageFallback
              src={data.gmbr?.includes("http") ? data.gmbr : `${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.gmbr?.split("|")[1]}`}
              fallbackImage="/images/thumb-placeholder.png"
              style={{ width: "308px", marginRight: "8px" }}
            />
          </a>
        </Link>
      </div>

      <div className="card-content">
        <Link
          href={{
            pathname:"/blog/detail",
            query:{id: data.id
            }}}
          >
          <a>
            <h5 className="card-title">{data.jdl}</h5>
          </a>
        </Link>
        <div className="d-flex meta_post">
          <h6>{data.n_kat}</h6>/ {convertDate(data.t_pst?.split("T")[0])}
        </div>
        <div className="card-text">{data.rgksn ? parse(data.rgksn.replace(/(<([^>]+)>)/gi, "")) : ""}</div>
        <Link
          href={{
            pathname:"/blog/detail",
            query:{id: data.id
            }}}
          >Lihat lebih</Link>
      </div>
    </div>
  );
}
