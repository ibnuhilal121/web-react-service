import React from "react";
import ReactImageFallback from "react-image-fallback";
import styles from "../../components/data/ItemPromo.module.scss";
import Link from "next/link";

export default function ProfesionalPortofolio(props) {
  const { data } = props;
  const image = new Image();
  image.src = generateImageUrl(data.lst_fto ? data.lst_fto[0]?.replace("1|", "") : null);
  const imageFit = image.width === image.height;

  return (
    <div id="portofolio_card" className="card">
      <div className={`card_img w-100 ${styles.blurImageParent}`}>
        {/* <img src={data.images} className="img-fluid" /> */}
        <div className="ratio ratio-1x1">
          {/* <Link href={"/member/portofolio/" + data.id}> */}
          <Link href={{
            pathname: "/member/portofolio/detail",
            query: {
              id: data.id
            },
          }}>
            {/* <a>
              <div
                style={{
                  background: `url(${generateImageUrl(data.lst_fto ? data.lst_fto[0].replace("1|", "") : null)})`,
                }}
                className={`bg-img-card ${imageFit ? "unblurred" : "blurred"}`}
              />
              <div className="container-img-card">
                <img
                  src={generateImageUrl(data.lst_fto ? data.lst_fto[0].replace("1|", "") : null)}
                  className={`img-fluid ${image.width >= image.height ? "w-100" : "h-100"} `}
                />
              </div>
            </a> */}
            <a>
              <ReactImageFallback src={data.lst_fto ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.lst_fto[0]?.replace("1|", "") : null} fallbackImage="/images/thumb-placeholder.png" className={`img-fluid ${image.width >= image.height ? "w-100" : "h-100"} ${styles.blurImageChild} `}  title={data.n ? data.n : "-"} style={{height: "300px"}}/>
              <ReactImageFallback src={data.lst_fto ? `${process.env.NEXT_PUBLIC_IMAGE_URL}/` + data.lst_fto[0]?.replace("1|", "") : null} fallbackImage="/images/thumb-placeholder.png" className={`img-fluid ${image.width >= image.height ? "w-100" : "h-100"} ${styles.blurImageChild} `}  title={data.n ? data.n : "-"} style={{height: "300px"}}/>
            </a>
          </Link>
        </div>
      </div>

      <div className="card-body">
        <h5
          className="card-title"
          style={{
            fontFamily: "Helvetica",
            fontSize: "20px",
            fontWeight: 700,
            color: "#00193E",
          }}
        >
          {data.n ? data.n : "-"}
        </h5>

        <div className="card-text" style={{ fontFamily: "Helvetica", fontWeight: 400, color: "#666666" }}>
          {data.dsk ? data.dsk : "-"}
        </div>
        <div className="card-text" style={{ paddingTop: "10px", lineHeight: "130%", fontStyle: "normal", opacity: 0.6, fontSize: 12, fontFamily: "FuturaBT", fontWeight: 700, color: "#00193E" }}>
          {data.almt ? data.almt : "-"}
        </div>
      </div>
    </div>
  );
}

const generateImageUrl = (url) => {
  if (!url) return "/images/thumb-placeholder.png";

  let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  fullUrl = fullUrl.replaceAll(" ", "%20");

  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/thumb-placeholder.png";
  }
};
