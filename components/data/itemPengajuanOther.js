import Link from "next/link";
import NumberFormat from "react-number-format";

export default function ItemPengajuanOther(props) {
    const { data } = props;



    return (
        <div id="card_detail_pengajuan">
            <div className="card card_pengajuan">
                <div className="card-header ps-3">
                    <div
                        className="kode"
                        style={{fontSize:"10px", fontFamily: "FuturaBT", fontWeight: "700" }}
                    >
                        {data.ID ? data.ID : "-"} &middot; <span>{data.TGL_INSERT ? data.TGL_INSERT.split(' ')[0] : "-"}</span>
                    </div>
                    <div
                        className="status"
                        style={{ fontFamily: "FuturaBT", fontWeight: "700", opacity: 0.6,maxWidth:"25%" }}
                    >
                        {data.STATUS_PENGAJUAN ? data.STATUS_PENGAJUAN : "-"}
                    </div>
                </div>
                <div className="card-body ps-3">
                    <div className="row">
                        <div className="col-6 item">
                            <label>Nama</label>
                            <strong>{data.NAMA ? data.NAMA : "-"}</strong>
                        </div>
                        <div className="col-6 item">
                            <label>Jenis KPR</label>
                            <strong> - </strong>
                        </div>
                        <div className="col-6 item">
                            <label>Jenis Kredit</label>
                            <strong> {data.PRODUK ? data.PRODUK : "-"} </strong>
                            
                        </div>
                        
                        <div className="col-6 item">
                            <label>Nilai Pengajuan</label>
                            <strong>
                            <NumberFormat
                                    value= {data.NILAI_DIAJUKAN ? data.NILAI_DIAJUKAN : "-"}
                                    displayType={"text"}
                                    prefix={"Rp"}
                                    thousandSeparator="."
                                    decimalSeparator=","
                                />
                                </strong>
                        </div>
                        
                    </div>
                </div>
                <div
                    className="card-footer"
                    style={{ marginLeft: 24, marginRight: 24, paddingRight: 0 }}
                >
                     <Link href={"/member/kredit/detail?id=" + data.ID}>
                     <a href={"/member/kredit/detail?id=" + data.ID} className="btn btn-main" style={{ width: 179, height: 48 }}>
                                    Lihat Detail
                                </a>
                        </Link>
                </div>
            </div>
        </div>
    );
}
