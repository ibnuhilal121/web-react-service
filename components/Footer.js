import Link from "next/link";
import { useEffect, useState } from "react";
import { useAppContext } from "../context";
import { useRouter } from "next/router";
import { validateProfileData } from "../helpers/validateProfileData";

export default function Footer(props) {
  const { userKey, userProfile } = useAppContext();
  const { isVerified = false } = userProfile || {}
  const router = useRouter();
  const [show, setShow] = useState(false);

  useEffect(() => {
    document.addEventListener("click", () => {
      const collapse = document.getElementById("showBantuan");
      if (collapse) {
        collapse.classList.remove("show");
      }
    });
  }, []);

  const goToAjukanKPR = (tipe) => {
    window.location.href = `${window.location.origin}/ajukan_kpr?tipe=${tipe}`;
  };

  return (
    <div>
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <div className="row row-cols-2 row-cols-3 row-cols-lg-5 g-2 g-lg-3">
                <div className="col">
                  <div className="item_footer">
                    <h5>CARI PROPERTI</h5>
                    <ul className="nav flex-column">
                      <li className="nav-item">
                        <Link href="/property?tipeProperti=1">
                          <a className="nav-link">Rumah</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/property?tipeProperti=2">
                          <a className="nav-link">Apartemen</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/property?tipeProperti=6">
                          <a className="nav-link">Ruko</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/property?tipeProperti=7">
                          <a className="nav-link">Kantor</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col">
                  <div className="item_footer">
                    <h5>AJUKAN KPR</h5>
                    <ul className="nav flex-column">
                      <li className="nav-item">
                        <Link
                          href={!userKey ? "" : !(router.pathname.includes("/ajukan_kpr")) ? isVerified ? "/ajukan_kpr?tipe=1" : "#" : "" }
                        >
                          <a
                            onClick={router.pathname.includes("/ajukan_kpr") ? (() => goToAjukanKPR("1")) : null}
                            className="nav-link"
                            data-bs-toggle={!userKey ? "modal" : isVerified && validateProfileData(userProfile) ? null : "modal"}
                            data-bs-target={!userKey ? "#modalLogin" : isVerified === false ? "#verifiedModal": (validateProfileData(userProfile) ? "" : "#phoneModal")}
                          >
                            Kredit Konvensional
                          </a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link
                          href={!userKey ? "" : !(router.pathname.includes("/ajukan_kpr")) ?  isVerified ? "/ajukan_kpr?tipe=2" : "#" : ""}
                        >
                          <a
                            onClick={router.pathname.includes("/ajukan_kpr") ? (() => goToAjukanKPR("2")) : null}
                            className="nav-link"
                            data-bs-toggle={!userKey ? "modal" : isVerified && validateProfileData(userProfile) ? null : "modal"}
                            data-bs-target={!userKey ? "#modalLogin" : isVerified === false ? "#verifiedModal": (validateProfileData(userProfile) ? "" : "#phoneModal")}
                          >
                            Pembiayaan Syariah
                          </a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col">
                  <div className="item_footer">
                    <h5>TOOLS</h5>
                    <ul className="nav flex-column">
                      <li className="nav-item">
                        <Link href="/tools/hitung_harga">
                          <a className="nav-link">Hitung Harga</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/tools/harga_pasar/?pg=1">
                          <a className="nav-link">Harga Pasar</a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/tools/simulasi_kpr">
                          <a className="nav-link">Simulasi KPR</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/tools/komparasi">
                          <a className="nav-link" onClick={() => localStorage.removeItem('itemCompare')}>Komparasi Properti</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/tools/konsultasi">
                          <a className="nav-link">Konsultasi</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col">
                  <div className=" item_footer_bold">
                    <ul className="nav flex-column">
                      <li className="nav-item">
                        <Link
                          href={{
                            pathname: "/blog",
                            // query: { kategori: "semua" },
                          }}
                        >
                          <a className="nav-link">BLOG</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link
                          href={{
                            pathname: "/promo",
                            // query: { kategori: "semua" },
                          }}
                        >
                          <a className="nav-link">PROMO</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/developer">
                          <a className="nav-link">LIST DEVELOPER</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col">
                  <div className=" item_footer_bold">
                    <ul className="nav flex-column">
                      <li className="nav-item">
                        <Link href="/tentang">
                          <a className="nav-link">TENTANG</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/faq">
                          <a className="nav-link">FAQ</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link href="/hubungi_kami">
                          <a className="nav-link">KONTAK</a>
                        </Link>
                      </li>
                      <li className="nav-item">
                        <a
                          href="https://www.btn.co.id/"
                          className="nav-link"
                          target="blank"
                        >
                          BTN.CO.ID
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container" style={{ marginBottom: 30 }}>
          <div style={props.mb_tipe_properti && {marginBottom: props.mb_tipe_properti}} id="footer_bottom_dekstop">
            <div className="bottom_left d-flex align-items-center">
              <span>© 2021 BTN Properti. All rights reserved.</span>
              <ul className="nav">
                <li className="nav-item">
                  <Link href="/terms">
                    <a
                      style={{ fontFamily: "Helvetica", fontWeight: "700" }}
                      className="nav-link mx-4"
                    >
                      Syarat dan Ketentuan
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/privacy">
                    <a
                      style={{ fontFamily: "Helvetica", fontWeight: "700" }}
                      className="nav-link"
                    >
                      Kebijakan Privasi
                    </a>
                  </Link>
                </li>

                <li className="nav-item">
                  <Link href="/sitemap">
                    <a
                      style={{ fontFamily: "Helvetica", fontWeight: "700" }}
                      className="nav-link ms-4"
                      href="#"
                    >
                      Sitemap
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <ul className="nav">
              <li className="">
                <a
                  className="nav-link px-2"
                  href="https://www.facebook.com/bankbtn/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-facebook" />
                </a>
              </li>
              <li className="">
                <a
                  className="nav-link px-2"
                  href="https://twitter.com/bankbtn"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-twitter" />
                </a>
              </li>
              <li className="">
                <a
                  className="nav-link px-2"
                  href="https://www.instagram.com/bankbtn/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-instagram" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link px-2"
                  href="https://www.youtube.com/channel/UCWpG0IG6UaUfBy4FWlJfOJA"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-youtube" />
                </a>
              </li>
            </ul>
          </div>
          <div id="footer_bottom_mobile">
            <ul className="nav justify-content-center mobile_footer_info">
              <li className="nav-item">
                <Link href="/terms">
                  <a className="nav-link" href="#">
                    Syarat dan ketentuan
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/privacy">
                  <a className="nav-link" href="#">
                    kebijakan Privasi
                  </a>
                </Link>
              </li>
              {/* <li className="nav-item">
                <a
                  className="nav-link"
                  target="_blank"
                  rel="noreferrer"
                  href="https://open-account.btn.co.id/"
                >
                  Open Account
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.btn.co.id/id/Locations?LocationType=bbea81b6-cace-4d12-820e-e897d0d348cf&City=&Keyword="
                >
                  Lokasi ATM
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.btn.co.id/id/Locations?LocationType=e8fd9eed-05f6-49e7-b894-7631893ff71c&City=&Keyword="
                >
                  Kantor Cabang
                </a>
              </li> */}
              <li className="nav-item">
                <Link href="/sitemap">
                  <a className="nav-link" href="#">
                    Sitemap
                  </a>
                </Link>
              </li>
            </ul>

            <ul className="nav justify-content-center mobile_footer_sosmed">
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="https://www.facebook.com/bankbtn/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-facebook" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="https://twitter.com/bankbtn"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-twitter" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="https://www.instagram.com/bankbtn/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-instagram" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="https://www.youtube.com/channel/UCWpG0IG6UaUfBy4FWlJfOJA"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="bi bi-youtube" />
                </a>
              </li>
            </ul>
            <div className="d-flex justify-content-center">
              <span>© 2021 BTN Properti. All rights reserved.</span>
            </div>
          </div>
        </div>
      </footer>
      <div id="fixed_bottom" className={router.pathname == `${"/property/tipe/[id]"}` ? "detail-properti-floating-btn " : ""}>
        {show && (<div  id="showBantuan">
          <div className="card card-body">
            <h6>Butuh bantuan?</h6>
            <div className="row">
              <div className="col-12 ">
                <a href="#"  data-bs-toggle="modal" data-bs-target="#modalLiveChat">
                  <div className="d-flex align-items-center item_live">
                    <img
                      src="/images/icons/live_chat.png"
                      className="img-fluid me-2"
                    />
                    Live Chat
                  </div>
                </a>
              </div>
              <div className="col-12 ">
                <a
                  href="https://api.whatsapp.com/send/?phone=6287771500286&text&app_absent=0"
                  target="_blank"
                  rel="noreferrer"
                >
                  <div className="d-flex align-items-center item_live">
                    <img
                      src="/images/icons/live_whatsapp.png"
                      className="img-fluid me-2"
                    />
                    WhatsApp
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div> )}
        
        <div
          className="btn_wa"
          // data-bs-toggle="collapse"
          // href="#showBantuan"
          role="button"
          onClick={() => setShow(!show)}
          aria-expanded="false"
          aria-controls="collapseExample"
        >
          <img src={`/images/icons/${show ? "close-icon.svg" : "chat.svg"}`} />
        </div>
      </div>
                    {/* modal live chat */}
            <div className={`modal fade`} id="modalLiveChat" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
          style={{zIndex:"9999"}}>
          <div className="modal-dialog modal-dialog-centered px-md-3 ">
              <div className="modal-content" style={{padding:"20px 60px",borderRadius:"8px"}}>
                <div className="modal-body">
                  <div className="row justify-content-end mb-2">
                    <button type="button" className="btn-close" aria-label="Close" data-bs-dismiss="modal"

                    ></button>
                  </div>
              <embed src="https://tawk.to/chat/560bb398f13b449d607f4256/default" height={"95%"} width={"100%"}/>
            </div>
          </div>
        </div>
        </div>
    </div>
  );
}
