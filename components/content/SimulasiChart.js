import React from "react";
import { Doughnut } from "react-chartjs-2";

const Simulasichart = ({ porsiPokok, porsiBunga }) => {
  const data = {
    // labels: [
    //     'Pokok',
    //     'Bunga'

    // ],
    datasets: [
      {
        data: [porsiBunga, porsiPokok],
        backgroundColor: ["#0B4773", "#D9E8FF"],
        // hoverBackgroundColor: [
        //     '#0B4773',
        //     '#D9E8FF'
        // ]
      },
    ],
  };

  return (
    <div style={{ height: "264px", maxWidth: "264px" }}>
      <Doughnut
        data={data}
        options={{
          responsive: true,
          width: "264px",
          height: "264px",
          maintainAspectRatio: true,
          tooltips: {
            enabled: false,
          },
          cutout: "75%",
        }}
      />
    </div>
  );
};

export default Simulasichart;
