import React, { useState } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import data_sejarah from "../../sample_data/data_sejarah";
import { useMediaQuery } from "react-responsive";

export default function SlideSejarah({data=[], step, setStep}) {
  const is1440 = useMediaQuery({ query: `(min-width: 1440px)` });
  const is1024 = useMediaQuery({ query: `(min-width: 1024px)` });
  const isTab = useMediaQuery({ query: `(min-width: 576px)` });
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });

  const generateStyle = () => {
    if (is1440) {
      return {
        itemContainer: {
          transition: "1s ease-in-out",
          marginLeft: ((step ) * -82) + "%",
            // step == 2 ? "-78%" : step == 3 ? "-165%" : step == 4 ? "-247%" : "",
          marginBottom: 90,
        },
        itemSejarah: {
          maxWidth: 1080,
        },
        timelineButton: {
          marginTop: 90,
          transition: "1s ease-in-out",
          marginLeft: ((step ) * -18) + "%",
            // step == 2 ? "-18%" : step == 3 ? "-46%" : step == 4 ? "-64%" : "4%",
          width: 720,
        },
        timelineBorder: {
          transition: "1s ease-in-out",
          pointerEvents: "none",
          marginLeft: ((step ) * -18) + "%",
            // step == 2 ? "-18%" : step == 3 ? "-46%" : step == 4 ? "-64%" : "4%",
          width: 700,
        }
      }
    }
    if (is1024) {
      return {
        itemContainer: {
          transition: "1s ease-in-out",
          marginLeft: ((step ) * -82) + "%",
            // step == 2 ? "-78%" : step == 3 ? "-165%" : step == 4 ? "-299.5%" : "",
          marginBottom: 90,
        },
        itemSejarah: {
          maxWidth: 825,
        },
        timelineButton: {
          marginTop: 90,
          transition: "1s ease-in-out",
          marginLeft: ((step ) * -16) + "%",
            // step == 2 ? "-30%" : step == 3 ? "-72%" : step == 4 ? "-104%" : "4%",
          width: 520,
        },
        timelineBorder: {
          transition: "1s ease-in-out",
          pointerEvents: "none",
          marginLeft: ((step ) * -16) + "%",
            // step == 2 ? "-30%" : step == 3 ? "-72%" : step == 4 ? "-104%" : "4%",
          width: 500,
        }
      }
    }
    if (isTab) {
      return {
        itemContainer: {
          transition: "1s ease-in-out",
          marginLeft: ((step ) * -82) + "%",
            // step == 2 ? "-78%" : step == 3 ? "-165%" : step == 4 ? "-299.5%" : "",
          marginBottom: 90,
        },
        itemSejarah: {
          maxWidth: 325,
        },
        timelineButton: {
          marginTop: 90,
          transition: "1s ease-in-out",
          marginLeft: ((step ) * -12) + "%",
            // step == 2 ? "-30%" : step == 3 ? "-72%" : step == 4 ? "-104%" : "4%",
          width: 220,
        },
        timelineBorder: {
          transition: "1s ease-in-out",
          pointerEvents: "none",
          marginLeft: ((step ) * -12) + "%",
            // step == 2 ? "-30%" : step == 3 ? "-72%" : step == 4 ? "-104%" : "4%",
          width: 200,
        }
      }
    }

    if (isTabletOrMobile) {
      return {
        itemContainer: {
          transition: "1s ease-in-out",
          marginLeft:  ((step ) * -107) + "%",
            // step == 2 ? "-107%" : step == 3 ? "-213%" : step == 4 ? "-299.5%" : "",
          marginBottom: 30,  
        },
        itemSejarah: {
          maxWidth: "100vw",
        },
        timelineButton: {
          marginTop: 90,
          transition: "1s ease-in-out",
          marginLeft: (((step ) * -15) - 115) + "%",
            // step == 2 ? "-133%" : step == 3 ? "-172%" : step == 4 ? "-204%" : "-105%",
          width: 200,
        },
        timelineBorder: {
          transition: "1s ease-in-out",
          pointerEvents: "none",
          marginLeft: (((step ) * -15) - 118) + "%",
            // step == 2 ? "-136%" : step == 3 ? "-175%" : step == 4 ? "-204%" : "-107%",
          width: 190,
        }
      }
    }
  }

  return (
    <>
      {/* <div
        class="hiddenDiv"
        style={generateStyle()?.itemContainer}
      >
        {data.map((data, index) => (
          <>
            <div key={index} className="item_sejarah" style={generateStyle()?.itemSejarah}>
              <div className="row ">
                <div className="col-md-12 text-center">
                  <h3
                    style={{
                      lineHeight: isTabletOrMobile ? "12px" : "106px",
                      fontSize: isTabletOrMobile ? 36 : "80px",
                      fontFamily: "FuturaBT",
                      fontWeight: "700",
                      marginTop: isTabletOrMobile ? 8 : "auto",
                    }}
                  >
                    {data.TANGGAL}
                  </h3>
                </div>
              </div>
              <div style={{
                display: "flex",
                alignItems: "flex-start",
                gap: isTabletOrMobile ? 20 : 80,
                paddingRight: isTabletOrMobile ? 20 : 80
              }}>
                <img
                  src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/${data.GAMBAR.replace("1|", "")}`} alt=""
                  style={{
                    // position: "absolute",
                    maxWidth: isTabletOrMobile ? "50%" : 250
                  }}
                />
    
                  <div className="body">
                    <h5
                      style={{
                        fontSize: isTabletOrMobile ? 20 : "24px",
                        fontFamily: "FuturaBT",
                        fontWeight: "700",
                      }}
                    >
                      {data.JUDUL}
                    </h5>
                    <p>{data.DESKRIPSI}</p>
                  </div>

              </div>
            </div>
          </>
        ))}
      </div> */}
      <div
        className="timelineButton"
        style={generateStyle()?.timelineButton}
      >
        {data.map((data, index) => (
          <div
            class={`sliderButton ${step === index ? "activeSlider" : ""} `}
            onClick={() => setStep(index)}
            style={{ position: "relative" }}
          >
            {index == step && (
              <p
                style={{
                  position: "absolute",
                  fontFamily: "Futura",
                  fonStyle: "normal",
                  fontWeight: "bold",
                  fontSize: isTabletOrMobile ? 12 : 16,
                  color: "#00193E",
                  bottom: "15px",
                  left: "-40px",
                  width: 200
                }}
              >
                {data.TANGGAL}
              </p>
            )}
          </div>
        ))}
      </div>
      <div
        className="timelineBorder"
        style={generateStyle()?.timelineBorder}
      ></div>
    </>
  );
}
