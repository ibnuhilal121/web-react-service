import React, { Component } from "react";
import pdfMake from "pdfmake/build/pdfmake";
import vfsFonts from "pdfmake/build/vfs_fonts";

pdfMake.vfs = vfsFonts.pdfMake ? vfsFonts.pdfMake.vfs : pdfMake.vfs;

export class SimulasiPdf extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  openmypdf = () => {
    const { simulasiData } = this.props;
    const dataTable = simulasiData?.detail.map((data) => [
      { text: data.bulan, alignment: "right" },
      {
        text: "Rp" + Math.round(Number(data.sisa_pinjaman)).toLocaleString("id"),
        alignment: "right",
      },
      {
        text: "Rp" + Math.round(Number(data.porsi_pokok)).toLocaleString("id"),
        alignment: "right",
      },
      {
        text: "Rp" + Math.round(Number(data.porsi_bunga)).toLocaleString("id"),
        alignment: "right",
      },
      {
        text: "Rp" + Math.round(Number(data.angsuran_perbulan)).toLocaleString("id"),
        alignment: "right",
      },
      { text: data.suku_bunga + "%", alignment: "right" },
    ]);

    var dd = {
      content: [
        {
          text: "Simulasi KPR " + this.props.tipe,
          bold: "true",
          alignment: "center",
          fontSize: 22,
        },
        {
          text: "www.btnproperti.co.id",
          alignment: "center",
          fontSize: 12,
        },
        "\n\n",
        {
          text: "Rincian Pinjaman",
          bold: "true",
          fontSize: 18,
        },
        "\n",
        {
          columns: [
            {
              text: "Harga Properti",
              width: 255,
            },
            {
              text:
                "Rp" +
                (
                  Number(simulasiData?.rincian?.jml_pinjam_max) +
                  Number(simulasiData?.rincian?.uang_muka)
                ).toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text:
                this.props.tipe === "Konvensional"
                  ? "Suku bunga (per tahun)"
                  : "Margin (per tahun)",
              width: 255,
            },
            {
              text: simulasiData?.rincian?.suku_bunga + "%",
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text:
                this.props.tipe === "Konvensional"
                  ? "Suku Bunga Floating (per tahun)"
                  : "Margin floating (per tahun)",
              width: 255,
            },
            {
              text: simulasiData?.rincian?.suku_bunga_float + "%",
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Kredit Fix",
              width: 255,
            },
            {
              text: simulasiData?.rincian?.kredit_fix + " Bulan",
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Lama Pinjaman",
              width: 255,
            },
            {
              text: simulasiData?.rincian?.lama_pinjam + " Bulan",
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Uang Muka (DP)",
              width: 255,
            },
            {
              text: "Rp" + Number(simulasiData?.rincian?.uang_muka).toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Jumlah Pinjaman Maksimal",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.jml_pinjam_max.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        "\n\n",
        {
          text: "Biaya Bank",
          bold: true,
        },
        {
          columns: [
            {
              text: "Appraisal",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_bank_appraisal.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Administrasi",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_bank_admin.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Proses",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_bank_proses.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Provisi",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_bank_provisi.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Asuransi",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_bank_asuransi.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Total Biaya Bank",
              width: 255,
              bold: true,
            },
            {
              text: "Rp" + simulasiData?.rincian?.total_biaya_bank.toLocaleString("id"),
              width: 200,
              bold: true,
            },
          ],
        },
        "\n",
        {
          text: "Biaya Notaris",
          bold: true,
        },
        {
          columns: [
            {
              text: "Akta Jual Beli",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_notaris_aktejualbeli.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Bea Balik Nama",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_notaris_baliknama.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "SKMHT",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_notaris_akte_skmht.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "APHT",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_notaris_akte_apht.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Perjanjian HT",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_notaris_perjanjian_ht.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Cek Sertifikat, ZNT, PNBP HT",
              width: 255,
            },
            {
              text: "Rp" + simulasiData?.rincian?.b_notaris_cek_sertif.toLocaleString("id"),
              width: 200,
            },
          ],
        },
        {
          columns: [
            {
              text: "Total Biaya Notaris",
              width: 255,
              bold: true,
            },
            {
              text: "Rp" + simulasiData?.rincian?.total_biaya_notaris.toLocaleString("id"),
              width: 200,
              bold: true,
            },
          ],
        },
        "\n",
        {
          columns: [
            {
              text: "Angsuran per bulan",
              width: 255,
              bold: true,
            },
            {
              text: "Rp" + simulasiData?.rincian?.angsuran_perbulan.toLocaleString("id"),
              width: 200,
              bold: true,
            },
          ],
        },
        {
          columns: [
            {
              text: "Pembayaran Pertama (Angsuran + Uang Muka + Total Biaya Bank + Total Biaya Notaris)",
              width: 255,
              bold: true,
            },
            {
              text: "Rp" + simulasiData?.rincian?.pembayaran_pertama.toLocaleString("id"),
              width: 200,
              bold: true,
            },
          ],
        },
        "\n",
        {
          text: "Catatan: Perhitungan ini berdasarkan asumsi kami pada aplikasi KPR secara umum.",
          style: ["quote", "small"],
        },

        /* ---------------------------------- HAL 2 --------------------------------- */
        {
          text: "\n\n",
          pageBreak: "after",
        },
        {
          text: "Detail Angsuran",
          bold: "true",
          fontSize: 18,
        },
        "\n",
        {
          // layout: "lightHorizontalLines", // optional
          table: {
            headerRows: 1,
            widths: ["7%", "23%", "20%", "20%", "20%", "10%"],

            body: [
              [
                { text: "Bulan", bold: true, alignment: "right" },
                { text: "Sisa Pinjaman", bold: true, alignment: "right" },
                { text: "Porsi Pokok", bold: true, alignment: "right" },
                {
                  text: this.props.tipe === "Konvensional" ? "Porsi Bunga" : "Porsi Margin",
                  bold: true,
                  alignment: "right",
                },
                { text: "Angsuran", bold: true, alignment: "right" },
                { text: "Bunga", bold: true, alignment: "right" },
              ],
              ...dataTable,
            ],
          },
        },
      ],
      defaultStyle: {
        // font: "Courier"
        lineHeight: 1.2,
      },
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 10,
        },
      },
    };

    pdfMake.createPdf(dd).download("simulasi-kpr");
  };

  render() {
    return (
      <a
        href="#"
        className="btn btn-outline-main btn_rounded"
        style={{
          height: 48,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          fontFamily: "Helvetica",
          fontWeight: 700,
        }}
        onClick={() => this.openmypdf()}
        value="Open PDF"
      >
        Download PDF
      </a>
    );
  }
}
