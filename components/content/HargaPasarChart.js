import { Line } from "react-chartjs-2";
import HargaPasarFilter from "../form/HargaPasarFilter";
import React, { useEffect, useState } from "react";
import NumberFormat from "react-number-format";


const HargaPasarChart = (props) => {
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }

  const currentYear = new Date().getFullYear();
  const data = {
    labels: [currentYear - 2, currentYear -1, currentYear],
    datasets: [
      {
        label: "",
        fill: false,
        lineTension: 0.3,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(0, 96, 239, 1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(0, 96, 239, 1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(0, 96, 239, 1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [props.th2, props.th1, props.th0],
      },
    ],
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        ticks: {
          // Include a dollar sign in the ticks
          callback: function (value, index, values) {
            return `Rp. ${numberWithCommas(value)}`;
          },
        },
      },
    },
  };

  return (
    <div>
      <h3
        style={{
          fontFamily: "Helvetica",
          fontStyle: "normal",
          fontWeight: "bold",
          fontSize: "20px",
          lineHeight: "150%",
          color: "#00193E",
        }}
      >
        Statistik Tren Harga 3 Tahun Terakhir
      </h3>
      <Line options={options} data={data} width="100%" height="50px" />
    </div>
  );
};

export default HargaPasarChart;
