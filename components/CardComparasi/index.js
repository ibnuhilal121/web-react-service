import Link from 'next/link'
import { useMediaQuery } from 'react-responsive'
import styles from './CardComparasi.module.scss';
import NumberFormat from 'react-number-format';

export default function CardComparasi(props) {
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
    const isSmallDevice = useMediaQuery({ query: `(max-width: 374px)`});
    const isTinyDevice = useMediaQuery({ query: `(max-width: 332px)`});

    return (
        <div className={`${!isTabletOrMobile ? 'mx-2' : 'card card_komparasi'}`} {...props}>
                {props.data ?
                (
                  <div className={`card ${styles.cardComparasi}`}>
                    <h6 className="lokasi">{props.data.n_kot ? props.data.n_kot : "-"}</h6>
                    <h5 
                        className="title" 
                        style={isTinyDevice ?
                            { fontSize: '8px' } : isSmallDevice ? 
                            { fontSize: '12px' } : 
                            { fontSize: '14px' }
                        }
                    >
                        {props.data.clstr} {props.data.n}
                    </h5>
                    <h5 
                        className="price" 
                        style={isTinyDevice ?
                            { marginBottom: '10px', fontSize: '10px' } : isSmallDevice ? 
                            { marginBottom: '10px', fontSize: '14px' } : 
                            { marginBottom: '10px', fontSize: '16px' }
                        }
                    >
                        <NumberFormat value={props.data.hrg} displayType={'text'} prefix={'Rp'} thousandSeparator="." decimalSeparator=","/>
                    </h5>
                  </div>
                ) :
                (
                    <Link href={{
                        pathname: '/property/tipe',
                        query: { komparasi:'1' },
                    }}>
                        <div className={`card ${styles.cardComparasi}`}>
                            <div className="d-flex justify-content-center align-items-center" style={{ height: '100%', width: '100%' }}>
                                <i 
                                    className="bi bi-plus-circle-fill" 
                                    style={isTabletOrMobile ? { marginBottom: '-12px', color: '#AAAAAA', fontSize: '20px' } : {}}
                                ></i>
                            </div>
                        </div>
                    </Link>
                )}
        </div>
    )
}
