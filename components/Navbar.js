import React, { useEffect, useState, useRef, useReducer } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import Head from "next/head";
import Cookies from "js-cookie";
import { useAppContext } from "../context";
import { useSession } from "next-auth/client";
import { useMediaQuery } from "react-responsive";
import { signOut } from "next-auth/client";
import SearchForm from "./section/SearchForm";
import { logout } from "../helpers/auth";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../utils/FirebaseSetup";
import Lottie from "react-lottie";
import * as animationData from "../public/animate_home.json";
import { getDetailMember, getVerifyStatus } from "../services/auth";
import LoaderOverlay from "./LoaderOverlay";
import { getEkycData } from "../services/auth";
import { validateProfileData } from "../helpers/validateProfileData";


export default function Navbar() {
  const [scrolled, setScrolled] = React.useState(false);
  const router = useRouter();
  const { userKey, setUserKey, setUserProfile, userProfile, setKprList } = useAppContext();
  const {isVerified = false} = userProfile || {}
  const buttonRef = useRef();
  const { stat } = router.query;
  const [session, loading] = useSession();
  const [keyword, setKeyword] = useState();
  const [showSearch, setShowSearch] = useState(false);
  const [lokasi, setLokasi] = useState("");
  const [tipe, setTipe] = useState("");
  const [budget, setBudget] = useState("");
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
  const [user, setUser] = useState(null);
  const [verifyStatus, setVerifyStatus] = useState(false);
  const [isLoading,setIsLoading] = useState(true)

  const redirect = (res) => {
    localStorage.removeItem("navigate")
    localStorage.removeItem("title")
    localStorage.removeItem("generate-url")

    
    // if (!userKey && res.isAuth) {
    //   history.push("")
    // } else if(title == "Pre Approval"){
    //   history.push(res.navigateWeb + "link" + localStorage.getItem("key"))
    // } else{
    //   history.push(res.navigateWeb)
    // }
  }



  
  const hidden = () => {
    const backDropShadow = document.getElementsByClassName('modal-backdrop')
    backDropShadow.style.visibility = "hidden";
  }

  const isTabletOrDesktop = useMediaQuery({ query: `(max-width: 768px)` });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setUser({
          provider: user.providerData[0].providerId,
          name: user.providerData[0].displayName,
          email: user.providerData[0].email,
          image: user.providerData[0].photoURL,
          id: user.providerData[0].uid,
          token: user.accessToken
        })
        // setLoggedIn(true)
      } else {
        setUser(null)
        // setLoggedIn(false)
      }
    })
  }, [])

  useEffect(() => {
    if (!userKey) {
      if (Cookies.get("user")) {
        setUserProfile(JSON.parse(Cookies.get("user")));
        setUserKey(JSON.parse(Cookies.get("keyMember")));
      } else {
        setUserProfile(JSON.parse(sessionStorage.getItem("user")));
        setUserKey(JSON.parse(sessionStorage.getItem("keyMember")));

      }
    }
    getEkycData(userProfile?.id).then((result)=>{
      const tempProfile = {
        n: userProfile?.n,
        gmbr: userProfile?.gmbr,
        id: userProfile?.id,
        agen: userProfile?.agen,
        idAgen: userProfile?.idAgen,
        namaAgensi: userProfile?.namaAgensi,
        isVerified: result.data?.isVerified,
        currentStep: result.data?.currentStep,
        counterFailedVerification:result.data?.counterFailedVerification,
        email:userProfile?.email
      }
      sessionStorage.setItem("user", JSON.stringify(tempProfile));
      })
      if(userKey) {
        getDetailMember(userKey).then((data) => {
          const tempData = {
            ...userProfile,
            mobilePhoneNumber: data?.no_hp,
            provinceId: data?.i_prop,
            districtId: data?.i_kot,
            subDistrictId: data?.i_kec,
            address: data?.almt,
            postalCode: data?.pos,
          }
          setUserProfile(tempData)
        })
      }
  }, []);

  useEffect(() => {
    if (stat === "relog" && router.pathname === "/") {
      setTimeout(() => {
        buttonRef?.current?.click();
      }, 1000);
    }
  }, [stat, router]);

  const handleScroll = () => {
    const offset = window.scrollY;

    if (offset > 200) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };
  useEffect(() => {
    
      setIsLoading(false)
    

    window.addEventListener("scroll", handleScroll);

    var myCollapsible = document.getElementById("nav_main_mobile");
    var menubar = document.getElementById("menubar");
    myCollapsible.addEventListener("show.bs.collapse", function () {
      menubar.classList.remove("bi-list");
      menubar.classList.add("bi-x-lg");
    });
    myCollapsible.addEventListener("hide.bs.collapse", function () {
      menubar.classList.remove("bi-x-lg");
      menubar.classList.add("bi-list");
    });
  }, []);

  const onChangeKeyword = (e) => {
    setKeyword(e.target.value);
    if (e.target.value) {
      document.getElementById("searchbar-logo").style.display = "none";
    } else {
      document.getElementById("searchbar-logo").style.display = "inline-block";
    }
  };

  const submitSearch = async (e) => {
    e.preventDefault();
    router.push(`/property?lokasi=${keyword}`);
  };

  const submitLogout = () => {
    if (user) {
      logout()
        // .then((res) => console.log("~ sukses sign out"))
        // .catch((err) => console.log("~ err sign out"))
        // .finally(() => {
        // });
      Cookies.remove("user");
      Cookies.remove("keyMember");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("keyMember");
      localStorage.removeItem("user");
      localStorage.removeItem("keyMember");
      localStorage.removeItem("generate-url");
      localStorage.removeItem("generate-token");
      localStorage.removeItem("generate-token-profesiona-listing");
      localStorage.removeItem("body-homeservice-link");
      localStorage.removeItem("body-homeservice-token");
      localStorage.removeItem("body-profesionallisting-token");
      localStorage.removeItem("body-profesionallisting-link");
      localStorage.removeItem("body-preapproval");
      localStorage.removeItem("url-home-services");
      localStorage.removeItem("url-profesiona-listing");
      localStorage.removeItem("title");
      localStorage.removeItem("navigate");
      setUserKey(null);
      setUserProfile(null);
      setUser(null);
      setKprList([])
      router.push("/");
    } else {
      logoutUser();
    }
  };

  const logoutUser = async () => {
    try {
      const logoutAction = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/member/logout`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_Member: userKey,
            'Access-Control-Allow-Origin': '*',
            ...defaultHeaders
          },
        }
      );
      const logoutResponse = await logoutAction.json();
      if (logoutResponse.IsError) {
        throw logoutResponse.ErrToUser;
      }
    } catch (error) {
      // console.log()
    } finally {
      if (router.pathname.includes('/ajukan_kpr')) {
        const KPRid = localStorage.getItem("kpr_id");
        // if (KPRid) {
        //   sessionStorage.setItem("redirect_url", `/ajukan_kpr?id=${KPRid}`);
        // } else {
        //   sessionStorage.setItem("redirect_url", router.pathname);
        // };
      };
      Cookies.remove("user");
      Cookies.remove("keyMember");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("keyMember");
      localStorage.removeItem("user");
      localStorage.removeItem("keyMember");
      localStorage.removeItem("generate-url");
      localStorage.removeItem("generate-token");
      localStorage.removeItem("generate-token-profesiona-listing");
      localStorage.removeItem("body-homeservice-link");
      localStorage.removeItem("body-homeservice-token");
      localStorage.removeItem("body-profesionallisting-token");
      localStorage.removeItem("body-profesionallisting-link");
      localStorage.removeItem("body-preapproval");
      localStorage.removeItem("url-home-services");
      localStorage.removeItem("url-profesiona-listing");
      localStorage.removeItem("title");
      localStorage.removeItem("navigate");
      setUserKey(null);
      setUserProfile(null);
      setKprList([])
      router.push("/");
      //window.location.href = `${window.location.origin}?stat=relog`;
    }
  };

  const showFormSearch = () => {
    setShowSearch(!showSearch);

    console.log("show search ", showSearch);

    let menubar = document.getElementById("menubar");
    if (!showSearch) {
      menubar.classList.remove("bi-list");
      menubar.classList.add("bi-x-lg");
    } else {
      menubar.classList.remove("bi-x-lg");
      menubar.classList.add("bi-list");
    }
  };

  const goToAjukanKPR = () => {
    window.location.href = `${window.location.origin}/ajukan_kpr`;
  };

  const closeMenubar = () => {
    document.getElementById("nav_main_mobile").classList.remove("show");
    let menubar = document.getElementById("menubar");
    menubar.classList.remove("bi-x-lg");
    menubar.classList.add("bi-list");
  };

  const showProfileImage = () => {
    console.log('~ session image : ', session?.user?.image)
    console.log('~ user profile image : ', userProfile?.gmbr)
    if (session?.user?.image) return session.user.image;
    if (user?.image) return user.image;
    if (userProfile?.gmbr) {
      if (userProfile.gmbr.includes('https://')) {
        if (userProfile.gmbr.includes('https://lh3.googleusercontent.com/') || userProfile.gmbr.includes('https://pbs.twimg.com/')) {
          return userProfile.gmbr;
        } else {
          return "/images/user/member-profile.jpg";      
        };
      } else {
        return `${process.env.NEXT_PUBLIC_IMAGE_URL}/${userProfile.gmbr.split('|')[1]}`;
      };
    };

    return "/images/user/member-profile.jpg"; 
  };
  return (
    <div>
      <Head></Head>
      <nav
        className={
          router.pathname == "/"
            ? scrolled
              ? "navbar navbar-expand-lg fixed-top p-0"
              : "navbar navbar-expand-lg fixed-top header_welcome"
            : "navbar navbar-expand-lg fixed-top"
        }
        id="header_top_dekstop"
      >
        <div className="container">
          <div className="navbar-brand p-0 text-center">
            {router.pathname == "/" && (
              <Link href="/" passHref>
                <a>
                  <img
                    src="/images/logo_home.svg"
                    className="img-fluid logo_home"
                  />
                </a>
              </Link>
            )}
            {router.pathname != "/" && (
              <Link href="/" passHref>
                <a>
                  <img
                    src="/images/Logo.svg"
                    className="img-fluid nav-img-custom"
                  />
                </a>
              </Link>
            )}
          </div>
          {/* <Link href="/" passHref>
          </Link> */}

          <div className="" id="nav_main">
            <ul className="navbar-nav me-auto"></ul>
            <ul className="nav align-items-center nav-ipad-pt">
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Tools
                </a>
                <ul
                  className="dropdown-menu dropdown_tools res-nav"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <li>
                    <Link href="/tools/hitung_harga">
                      <a className="dropdown-item">Hitung Harga</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/tools/harga_pasar/?pg=1">
                      <a className="dropdown-item">Harga Pasar</a>
                    </Link>
                  </li>

                  <li>
                    <Link href="/tools/simulasi_kpr">
                      <a className="dropdown-item">Simulasi KPR</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/tools/komparasi">
                      <a
                        className="dropdown-item"
                        onClick={() => localStorage.removeItem("itemCompare")}
                      >
                        Komparasi Properti
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/tools/konsultasi">
                      <a className="dropdown-item">Konsultasi</a>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
              <Link href="/blog/?pg=1&kategori=">
                  <a className="nav-link">Blog</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  href={{
                    pathname: "/promo",
                    // query: { kategori: "semua" },
                  }}
                >
                  <a className="nav-link">Promo</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/developer">
                  <a className="nav-link">List Developer</a>
                </Link>
              </li>
              <li className="nav-item">
              <Link
                  href={
                    !userKey
                      ? ""
                      : router.pathname === "/ajukan_kpr"
                      ? ""
                      : isVerified ? (validateProfileData(userProfile) ? "/ajukan_kpr" : "") : ""
                  }
                >
                  <a
                    className="nav-link"
                    data-bs-toggle={!userKey ? "modal" : isVerified && validateProfileData(userProfile) ? null : "modal"}
                    data-bs-target={!userKey ? "#modalLogin" : isVerified === false ? "#verifiedModal": (validateProfileData(userProfile) ? "" : "#phoneModal")}
                    onClick={() => console.log(validateProfileData(userProfile))}
                  >
                    Ajukan KPR
                  </a>
                </Link>
              </li>

              {router.pathname == "/member" && (
                <li className="nav-item">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdownMenuLink"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    member
                  </a>
                  <ul
                    className="dropdown-menu "
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <li>
                      <Link href="/tools/hitung_harga">
                        <a className="dropdown-item">Hitung Harga</a>
                      </Link>
                    </li>
                  </ul>
                </li>
              )}
              {userProfile ? (
                <li className="nav-item dropdown dropdown-avatar">
                  <a
                    className="nav-link"
                    href="#"
                    id="navbarDropdownMenuLink"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <div className="navbar-avatar">
                      <img
                        src={showProfileImage()}
                        width="40"
                        height="40"
                      />
                      <p>{userProfile.n}</p>
                    </div>
                  </a>
                  <ul
                    className="dropdown-menu dropdown_tools"
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <li>
                      <Link href="/member/profil">
                        <a className="dropdown-item">Profil</a>
                      </Link>
                    </li>
                    <li>
                      <Link href={"/member/kredit"}>
                        <a className="dropdown-item" 
                        >Pengajuan KPR</a>
                      </Link>

                    </li>

                    <li>
                      <Link href="/member/reward">
                        <a className="dropdown-item">Reward</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/member/pesan">
                        <a className="dropdown-item">Pesan</a>
                      </Link>
                    </li>
                    <li>
                      <a
                        className="dropdown-item logout"
                        onClick={submitLogout}
                      >
                        <p>Keluar</p>
                        <i className="bi bi-power"></i>
                      </a>
                    </li>
                  </ul>
                </li>
              ) : (
                <li className="nav-item ms-custom">
                  <a
                    ref={buttonRef}
                    type="button"
                    className="btn btn-main text-white"
                    data-bs-toggle="modal"
                    data-bs-target="#modalLogin"
                    onClick={() => redirect()}
                    
                  >
                    Daftar atau Masuk
                  </a>
                </li>
              )}
            </ul>
          </div>
        </div>
      </nav>
      <div
        id="menu_mobile"
        className={
          router.pathname == "/"
            ? scrolled
              ? "menu_mobile row nav_closed"
              : "menu_mobile header_welcome row nav_closed"
            : "menu_mobile row nav_closed"
        }
      >
        <div className="d-flex justify-content-between align-items-center w-100 p-0">
          <button
            className="iconMenu"
            id="iconMenu"
            type="button"
            data-bs-toggle="collapse"
            onClick={showSearch ? showFormSearch : () => {}}
            data-bs-target={
              showSearch ? "#search_navbar_form" : "#nav_main_mobile"
            }
            aria-controls="nav_main_mobile"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="bi bi-list" id="menubar"></i>
          </button>
          <div id="front_search" className="pe-3 w-100 align-self-center">
            <div
              id="mobile_search"
              className="d-flex align-items-center justify-content-between "
              // onClick={() => setShowSearch(prev => !prev)}
            >
              {/* <img
                src="/images/logo_horizontal.svg"
                style={{ maxWidth: "73px", maxHeight: "17px" }}
                alt=""
                onClick={() => setShowSearch(prev => !prev)}
              />
              <img
                src="/icons/icons/search.svg"
                alt=""
                style={{ maxWidth: "18px", maxHeight: "18px", marginEnd: "0px" }}
                onClick={() => setShowSearch(prev => !prev)}
              /> */}
              <Link href="/" passHref>
                <img
                  src="/images/logo_horizontal.svg"
                  id="searchbar-logo"
                  style={{ 
                            // maxWidth: "50%", 
                            // height: "30%", 
                            maxWidth: "73px",
                            height: "17px",
                            position: 'absolute' }}
                  alt="Logo BTN Properti"
                />
              </Link>
              {/* <form onSubmit={showFormSearch} className="w-100"> */}
              <div className="input-group" onClick={showFormSearch}>
                <input
                  type="text"
                  className="form-control"
                  value={keyword}
                  onChange={onChangeKeyword}
                  disabled={true}
                  onClick={showFormSearch}
                />
                <button
                  type="submit"
                  className="input-group-text"
                  id="basic-addon2"
                  onClick={showFormSearch}
                >
                  {/* <i className="bi bi-search"></i> */}
                  <img
                    src="/icons/icons/search.svg"
                    alt=""
                    style={{
                      maxWidth: "18px",
                      maxHeight: "18px",
                      marginEnd: "0px",
                    }}
                  />
                </button>
              </div>
              {/* </form> */}
            </div>
          </div>
          {isTabletOrMobile ? (
            <Link href={!userKey ? "" : "/member/dashboard"}>
              <a
                className="nav-link"
                data-bs-toggle={!userKey ? "modal" : null}
                data-bs-target={!userKey ? "#modalLogin" : null}
              >
                <img
                  src="/images/icons/avatar.png"
                  className="img-fluid avatar"
                />
              </a>
            </Link>
          ) : (
            <Link href={!userKey ? "" : "/member/profil"}>
              <a
                className="nav-link"
                data-bs-toggle={!userKey ? "modal" : null}
                data-bs-target={!userKey ? "#modalLogin" : null}
              >
                <img
                  src="/images/icons/avatar.png"
                  className="img-fluid avatar"
                />
              </a>
            </Link>
          )}
        </div>
        {showSearch ? (
          <SearchForm
            lokasi={lokasi}
            tipe={tipe}
            budget={budget}
            setLokasi={(e) => setLokasi(e.target.value)}
            setTipe={(e) => setTipe(e.target.value)}
            setBudget={(e) => setBudget(e.target.value)}
            onSubmit={() =>
              router.push(
                `/property?lokasi=${lokasi}&tipeProperti=${tipe}&budget=${budget}`
              )
            }
          />
        ) : (
          <div></div>
        )}
      </div>
      <div className="collapse" id="nav_main_mobile">
        <div className="card card-body">
          <ul className="navbar-nav" id="menu_main_mobile">
            <div
             style={{paddingLeft:"14px"}}
              id="menu_mobile"
              className={
                router.pathname == "/"
                  ? scrolled
                    ? "menu_mobile "
                    : "menu_mobile header_welcome"
                  : "menu_mobile"
              }
            >
              <button
              style={{marginRight:"8px"}}
                className="iconMenu"
                id="iconMenu"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target={
                  showSearch ? "#search_navbar_form" : "#nav_main_mobile"
                }
                aria-controls="nav_main_mobile"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="bi bi-list" id="menubar"></i>
              </button>
              <div className="w-100">
                <div
                  id="mobile_search"
                  className="d-flex align-items-center justify-content-between"
                  style={{width:"93%"}}
                >
                  {/* <img
                    src="/images/logo_horizontal.svg"
                    style={{ maxWidth: "73px", maxHeight: "17px" }}
                    alt=""
                  />
                  <img
                    src="/icons/icons/search.svg"
                    alt=""
                    style={{
                      maxWidth: "18px",
                      maxHeight: "18px",
                      marginEnd: "0px",
                    }}
                  /> */}
                  <Link href="/" passHref>
                    <img
                      src="/images/logo_horizontal.svg"
                      id="searchbar-logo"
                      style={{ maxWidth: "73px", maxHeight: "17px" }}
                      alt="Logo BTN Properti"
                    />
                  </Link>
                  <form onSubmit={submitSearch} className="w-100">
                    <div className="input-group">
                      <input
                        type="text"
                        className="form-control"
                        value={keyword}
                        onChange={onChangeKeyword}
                        disabled={true}
                      />
                      <button
                        type="submit"
                        className="input-group-text"
                        id="basic-addon2"
                      >
                        {/* <i className="bi bi-search"></i> */}
                        <img
                          src="/icons/icons/search.svg"
                          alt=""
                          style={{
                            maxWidth: "18px",
                            maxHeight: "18px",
                            marginEnd: "0px",
                          }}
                        />
                      </button>
                    </div>
                  </form>
                </div>
              </div>

              {isTabletOrMobile ? (
                <Link href={!userKey ? "" : "/member/dashboard"}>
                  <a
                    className="nav-link"
                    data-bs-toggle={!userKey ? "modal" : null}
                    data-bs-target={!userKey ? "#modalLogin" : null}
                    onClick={closeMenubar}
                  >
                    <img
                    id="avatar_navbar"
                      src="/images/icons/avatar.png"
                      className="img-fluid avatar"
                    />
                  </a>
                </Link>
              ) : (
                <Link href={!userKey ? "" : "/member/profil"}>
                  <a
                    className="nav-link"
                    data-bs-toggle={!userKey ? "modal" : null}
                    data-bs-target={!userKey ? "#modalLogin" : null}
                  >
                    <img
                    id="avatar_navbar"
                      src="/images/icons/avatar.png"
                      className="img-fluid avatar"
                    />
                  </a>
                </Link>
              )}
            </div>
            <div
            style={{display:"none"}}
              id="menu_mobile"
              className={
                router.pathname == "/"
                  ? scrolled
                    ? "menu_mobile "
                    : "menu_mobile header_welcome"
                  : "menu_mobile"
              }
            >
              <button
                className="iconMenu d-flex justify-content-center align-items-center avatar"
                id="iconMenu"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target={
                  showSearch ? "#search_navbar_form" : "#nav_main_mobile"
                }
                aria-controls="nav_main_mobile"
                aria-expanded="false"
                aria-label="Close"
                style={{ height: 'auto', margin: 0, padding: 0 }}
              >
                <i className="bi bi-x-lg" style={{ marginTop: '4px' }}></i>
              </button>
              <div className="w-100">
                <div
                  id="mobile_search"
                  className="d-flex align-items-center justify-content-between"
                >
                  {/* <img
                    src="/images/logo_horizontal.svg"
                    style={{ maxWidth: "73px", maxHeight: "17px" }}
                    alt=""
                  />
                  <img
                    src="/icons/icons/search.svg"
                    alt=""
                    style={{
                      maxWidth: "18px",
                      maxHeight: "18px",
                      marginEnd: "0px",
                    }}
                  /> */}
                  <Link href="/" passHref>
                    <img
                      src="/images/logo_horizontal.svg"
                      id="searchbar-logo"
                      style={{ maxWidth: "73px", maxHeight: "17px" }}
                      alt="Logo BTN Properti"
                    />
                  </Link>
                  <form onSubmit={submitSearch} className="w-100">
                    <div className="input-group">
                      <input
                        type="text"
                        className="form-control"
                        value={keyword}
                        onChange={onChangeKeyword}
                        disabled={true}
                      />
                      <button
                        type="submit"
                        className="input-group-text"
                        id="basic-addon2"
                        // onClick={submitSearch}
                      >
                        {/* <i className="bi bi-search"></i> */}
                        <img
                          src="/icons/icons/search.svg"
                          alt=""
                          style={{
                            maxWidth: "18px",
                            maxHeight: "18px",
                            marginEnd: "0px",
                          }}
                        />
                      </button>
                    </div>
                  </form>
                </div>
              </div>

              {isTabletOrMobile ? (
                <Link href={!userKey ? "" : "/member/dashboard"}>
                  <a
                    className="nav-link"
                    data-bs-toggle={!userKey ? "modal" : null}
                    data-bs-target={!userKey ? "#modalLogin" : null}
                  >
                    <img
                      src="/images/icons/avatar.png"
                      className="img-fluid avatar"
                    />
                  </a>
                </Link>
              ) : (
                <Link href={!userKey ? "" : "/member/profil"}>
                  <a
                    className="nav-link"
                    data-bs-toggle={!userKey ? "modal" : null}
                    data-bs-target={!userKey ? "#modalLogin" : null}
                  >
                    <img
                      src="/images/icons/avatar.png"
                      className="img-fluid avatar"
                    />
                  </a>
                </Link>
              )}
            </div>
            <li className="nav-item mt-5">
              <Link href="/">
                <a onClick={hidden}  className="nav-link">Beranda</a>
              </Link>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Tools
              </a>
              <ul
                className="dropdown-menu "
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link href="/tools/hitung_harga">
                    <a className="dropdown-item">Hitung Harga</a>
                  </Link>
                </li>
                <li>
                  <Link href="/tools/harga_pasar/?pg=1">
                    <a className="dropdown-item">Harga Pasar</a>
                  </Link>
                </li>

                <li>
                  <Link href="/tools/simulasi_kpr">
                    <a className="dropdown-item">Simulasi KPR</a>
                  </Link>
                </li>
                <li>
                  <Link href="/tools/komparasi">
                    <a
                      className="dropdown-item"
                      onClick={() => localStorage.removeItem("itemCompare")}
                    >
                      Komparasi Properti
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/tools/konsultasi">
                    <a className="dropdown-item">Konsultasi</a>
                  </Link>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <Link
                 href="/blog/?pg=1&kategori="
              >
                <a className="nav-link">Blog</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                href={{
                  pathname: "/promo",
                  // query: { kategori: "semua" },
                }}
              >
                <a className="nav-link">Promo</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/developer">
                <a className="nav-link">List Developer</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                href={
                  !userKey
                    ? ""
                    : router.pathname === "/ajukan_kpr"
                    ? ""
                    :  "/ajukan_kpr" 
                    
                }
              >
                <a
                  data-bs-toggle={!userKey ? "modal" : isVerified && validateProfileData(userProfile) ? null : "modal"}
                  data-bs-target={!userKey ? "#modalLogin" : isVerified === false ? "#verifiedModal": (validateProfileData(userProfile) ? "" : "#phoneModal")}
                  onClick={()=>{closeMenubar()}}
                  className="nav-link"
                  href={
                    !userKey
                      ? ""
                      : router.pathname === "/ajukan_kpr"
                      ? ""
                      : isVerified ? (validateProfileData(userProfile) ? "/ajukan_kpr" : "") : ""
                  }
                >
                  Ajukan KPR
                </a>
              </Link>
            </li>
          </ul>
          <ul className="nav justify-content-center nav_about">
            <li className="nav-item">
              <Link href="/tentang">
                <a className="nav-link">TENTANG</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/faq">
                <a className="nav-link">FAQ</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/hubungi_kami">
                <a className="nav-link">KONTAK</a>
              </Link>
            </li>
            <li className="nav-item">
              <a
                href="https://www.btn.co.id/"
                className="nav-link"
                target="blank"
              >
                BTN.CO.ID
              </a>
            </li>
          </ul>
          <ul className="nav nav_mobile_sosmed">
            <li className="nav-item">
              <a
                className="nav-link"
                href="https://www.facebook.com/bankbtn/"
                target="_blank"
                rel="noreferrer"
              >
                <i className="bi bi-facebook" />
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                href="https://twitter.com/bankbtn"
                target="_blank"
                rel="noreferrer"
              >
                <i className="bi bi-twitter" />
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                href="https://www.instagram.com/bankbtn/"
                target="_blank"
                rel="noreferrer"
              >
                <i className="bi bi-instagram" />
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                href="https://www.youtube.com/channel/UCWpG0IG6UaUfBy4FWlJfOJA"
                target="_blank"
                rel="noreferrer"
              >
                <i className="bi bi-youtube" />
              </a>
            </li>
          </ul>
          <ul className="nav nav_syarat_kebijakan">
            <li className="nav-item">
              <Link href="/terms">
                <a className="nav-link">Syarat dan Ketentuan</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/privacy">
                <a className="nav-link">Kebijakan Privasi</a>
              </Link>
            </li>

            <li className="nav-item">
              <a
                className="nav-link"
                target="_blank"
                rel="noreferrer"
                href="https://www.btn.co.id/id/Locations?LocationType=bbea81b6-cace-4d12-820e-e897d0d348cf&City=&Keyword="
              >
                Lokasi ATM
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                target="_blank"
                rel="noreferrer"
                href="https://www.btn.co.id/id/Locations?LocationType=e8fd9eed-05f6-49e7-b894-7631893ff71c&City=&Keyword="
              >
                Kantor Cabang
              </a>
            </li>
            <li className="nav-item">
              <Link href="/sitemap">
                <a className="nav-link" href="#">
                  Sitemap
                </a>
              </Link>
            </li>
          </ul>
          <div className="copyright_menu">
            © 2021 BTN Properti. Hak cipta dilindungi undang-undang.
          </div>
        </div>
      </div>
      <div className="modal fade" id="verifiedModal" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" style={{zIndex:"9991"}}>
          <div className="modal-dialog modal-dialog-centered m-auto px-0">
            <div className="modal-content modal_delete h-100" style={{padding:"20px 29px"}}>  
              <div className="modal-body text-center pt-3 px-0">
                <div className="text-center">        
                  <h4 className="modal-title mb-3" style={{fontFamily:"Futura",fontStyle:"normal",lineHeight:"130%", fontWeight:"700",color:"#000000"}}>Lakukan e-KYC</h4>
                    <div className="desc text-center mb-0" style={{ fontFamily: "Helvetica", fontWeight: 400,fontSize:"16px",color:'#000000' }}>
                      Silakan lakukan e-KYC melalui Mobile App BTN Properti
                    </div>
                            {isTabletOrDesktop ? (<Lottie options={defaultOptions} isStopped={false} isPaused={false} />) 
                            : (<Lottie options={defaultOptions} height={146} width={212} isStopped={false} isPaused={false} style={{marginBottom:"121px",marginTop:"125px"}} />)}
                    <button
                      type="buttom"
                      className="btn btn-main form-control btn-back px-5"
                      style={{
                      width:"100%",
                      maxWidth: "540px",
                      height: "48px",
                      fontSize: "14px",
                      fontFamily: "Helvetica",
                      fontWeight: 700,
                      backgroundColor: "#0061A7",
                      }}
                      data-bs-dismiss="modal"
                      onClick={(()=>{window.location.replace("/member/verified/")})}
                    >
                      Oke
                    </button>
             </div>
          </div>
        </div>
      </div>
  </div>
  <LoaderOverlay open={isLoading}/>
    </div>
    
  );
}

  

const generateImageUrl = (url) => {
  if (!url) return "/images/user/member-profile.jpg";

  let fullUrl = `${process.env.NEXT_PUBLIC_IMAGE_URL}/${url.split('"')[0]}`;
  fullUrl = fullUrl.replaceAll(" ", "%20");

  var img = new Image();
  img.src = fullUrl;
  const isImageExist = img.height != 0;

  if (isImageExist) {
    return fullUrl;
  } else {
    return "/images/user/member-profile.jpg";
  }
};
