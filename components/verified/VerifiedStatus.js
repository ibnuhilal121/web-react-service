


const VerifiedStatus = (props) => {

    const {title,subTitle} = props;
    return (
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-8 text-center">
          <img
            id="harga-img"
            src="/images/acc/hitung_harga.png"
            className="img-fluid w-100 h-100 mb-4"
            style={{ maxHeight: "235px", maxWidth: "442px" }}
          />
          <div style={{ maxWidth: "445px",textAlign:"center",margin:"auto " }}>
            <h3 style={{ fontFamily: "FuturaHvBt", fontWeight: "400", fontSize: "24px", color: "#00193E" }}>
            {title.substring(0, title.lastIndexOf(" "))} <span style={{color:"#0061A7"}}> {title.split(' ')[title.split(' ').length - 1]} </span>
            </h3>
            <p style={{ fontFamily: "Helvetica", fontSize: "12px", fontWeight: "400", color: "#666666" }}>
            {subTitle}
            </p>
          </div>
        </div>
      </div>
    );
  };

  export default VerifiedStatus;