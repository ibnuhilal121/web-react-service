import { getBenefitEkyc, getTncEkyc } from "../../services/auth";
import { useState, useEffect } from "react";
import parse from "html-react-parser";
const Unverified = (props) => {
  const { showBanner, setShowBanner } = props;
  const [benefit, setBenefit] = useState([]);
  const [tnc, setTnc] = useState("");

  useEffect(() => {
    let data = [];
    getBenefitEkyc()
      .then((response) => {
        data.push(response.data.description);
        setBenefit(data);
      })
      .catch((error) => {
        console.log("error benefit -->", error);
      });
  }, []);

  useEffect(() => {
    let data = [];
    getTncEkyc()
      .then((response) => {
        data.push(response.data.description);
        setTnc(data);
      })
      .catch((error) => {
        console.log("error tnc -->", error);
      });
  }, []);
  return (
    <div>
      <div
        className={`row g-0 py-4 px-3 w-100 mb-4 ${!showBanner ? "d-none" : ""}  `}
        style={{ backgroundColor: "#EDF4FF", maxWidth: "939px", minHeight: "177px", borderRadius: "8px" }}
      >
        <div className="d-flex flex-col">
          <div className="row">
            <h3
              style={{
                fontFamily: "Futura",
                lineHeight: "18px",
                fontWeight: "700",
                fontSize: "18px",
                color: "#000000",
                marginBottom: "12px",
              }}
            >
              Kamu belum melakukan verifikasi e&#8209;KYC
            </h3>
            <div className="d-flex" style={{ marginBottom: "16px" }}>
              <p
                style={{
                  fontFamily: "FuturaBkBt",
                  fontWeight: "400",
                  lineHeight: "130%",
                  fontStyle: "normal",
                  fontSize: "14px",
                  height: "18px",
                  color: "#000000",
                  margin: 0,
                }}
              >
                Lakukan e-KYC untuk dapatkan semua benefit dibawah ini. Download BTN Properti Sekarang!
              </p>
            </div>
            <div className="row g-0 mt-4 mt-md-0" id="verification-banner">
              <div className="col-6" style={{maxWidth:'212px'}}>
              <a
                className="d-flex w-auto px-0 me-3 ps-sm-3"
                href="https://apps.apple.com/id/app/btn-properti-mobile/id1641042026?l=id"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src="/images/link/appstore.png"
                  fallbackImage="/images/thumb-placeholder.png"
                  style={{ width: "180px", height: "52px" }}
                />
              </a>
              </div>
              <div className="col-6">
              <a
                className="d-flex w-auto p-0 "
                href="https://play.google.com/store/apps/details?id=btn.properti.android"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src="/images/link/playstore.png"
                  fallbackImage="/images/thumb-placeholder.png"
                  style={{ width: "180px", height: "52px" }}
                />
              </a>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <h3 className="mb-4" style={{ fontFamily: "Futura", fontSize: "18px", fontWeight: "700", color: "#000000" }}>
        Benefit e-KYC
      </h3>
      <div className="tab-menu d-md-flex justify-content-between align-items-start" id="nav-tabs-verified">
        <ul className="nav nav-tabs" id="menu-verified" style={{ fontFamily: "Helvetica" }}>
          <li className="nav-item me-2">
            <a
              href="#"
              className="nav-link active px-0 py-1"
              data-bs-toggle="tab"
              data-bs-target="#tab-benefit"
              role="tab"
              style={{ color: "#000000" }}
            >
              <span style={{ fontFamily: "Helvetica" }}>Benefit</span>
            </a>
          </li>
          <li className="nav-item">
            <a
              href="#"
              className="nav-link px-0 py-1"
              data-bs-toggle="tab"
              data-bs-target="#tab-syarat"
              role="tab"
              style={{ color: "#000000" }}
            >
              <span style={{ fontFamily: "Helvetica" }}>Syarat dan Ketentuan</span>
            </a>
          </li>
        </ul>
      </div>
      <div className="tab-content revamp-ajukan-kprtipe">
        <div className="tab-pane fade show active" id="tab-benefit" role="tabpanel">
          <div
            style={{ fontFamily: "Futura", color: "#40536E", fontWeight: "400", fontSize: "14px", paddingLeft: "5px" }}
          >
            <div dangerouslySetInnerHTML={{ __html: benefit }} />
          </div>
        </div>
        <div className="tab-pane fade" id="tab-syarat" role="tabpanel">
          <div
            style={{ fontFamily: "Futura", color: "#40536E", fontWeight: "400", fontSize: "14px", paddingLeft: "5px" }}
          >
            <div dangerouslySetInnerHTML={{ __html: tnc }} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Unverified;
