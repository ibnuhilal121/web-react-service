const VerfiedFailed3Times = (props) => {
    const { title, subTitle } = props;
    return (
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-8 text-center">
          <img
            id="harga-img"
            src="/images/acc/hitung_harga.png"
            className="img-fluid w-100 h-100 mb-4"
            style={{ maxHeight: "235px", maxWidth: "442px" }}
          />
          <div style={{ maxWidth: "445px", textAlign: "center",margin:"auto" }}>
            <h3 style={{ fontFamily: "FuturaHvBt", fontWeight: "400", fontSize: "24px",color:"#00193E",lineHeight:"36px"}}>
              {title.substring(0, title.lastIndexOf(" "))}{" "}
              <span style={{ color: "#de2925" }}> {title.split(" ")[title.split(" ").length - 1]} </span>
            </h3>
            <p style={{ fontFamily: "Helvetica", fontSize: "12px", fontWeight: "400", color: "#666666" }}>{subTitle}</p>
            <a
              type="button"
              href="/hubungi_kami"
              className="btn btn-main form-control btn-back px-5"
              style={{
                maxWidth: "315px",
                height: "48px",
                fontSize: "14px",
                fontFamily: "Helvetica",
                fontWeight: 700,
                backgroundColor: "#0061A7",
              }}
            >
              Hubungi Kami
            </a>
          </div>
        </div>
      </div>
    );
  };
  
  export default VerfiedFailed3Times;
  