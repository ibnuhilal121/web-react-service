import { useState } from "react";
import { useMediaQuery } from "react-responsive";
import Lottie from "react-lottie";
import * as animationData from "../../public/animate_home.json";

const VerifiedFailed = (props) => {
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 768px)` });
  const { title, subTitle } = props;
  const [osType, setOsType] = useState(getMobileOperatingSystem());

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  function getMobileOperatingSystem() {
    let userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }
    if (/android/i.test(userAgent)) {
        return "Android";
    }
    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "Ios";
    }

    return "unknown";
}
  return (
    <div className="row h-100 justify-content-center align-items-center">
      <div className="col-md-8 text-center">
        <img
          id="harga-img"
          src="/images/acc/hitung_harga.png"
          className="img-fluid w-100 h-100 mb-4"
          style={{ maxHeight: "235px", maxWidth: "442px" }}
        />
        <div style={{ maxWidth: "445px", textAlign: "center",margin:"auto" }}>
          <h3 style={{ fontFamily: "FuturaHvBt", fontWeight: "400", fontSize: "24px", color:"#00193E" }}>
            {title.substring(0, title.lastIndexOf(" "))}{" "}
            <span style={{ color: "#de2925" }}> {title.split(" ")[title.split(" ").length - 1]} </span>
          </h3>
          <p style={{ fontFamily: "Helvetica", fontSize: "12px", fontWeight: "400", color: "#666666" }}>{subTitle}</p>
        
          <div className="row g-0 mt-4 justify-content-center">
              <a
                className="d-flex w-auto px-0 me-3"
                href="https://apps.apple.com/id/app/btn-properti-mobile/id1641042026?l=id"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src="/images/link/appstore.png"
                  fallbackImage="/images/thumb-placeholder.png"
                  style={{ width: "180px", height: "52px" }}
                />
              </a>
              <a
                className="d-flex w-auto p-0 "
                href="https://play.google.com/store/apps/details?id=btn.properti.android"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src="/images/link/playstore.png"
                  fallbackImage="/images/thumb-placeholder.png"
                  style={{ width: "180px", height: "52px" }}
                />
              </a>
            </div>
          
        </div>
      </div>
      <div className="modal fade" id="redirectAppModall" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered m-auto px-0">
          <div className="modal-content modal_delete h-100" style={{padding:"20px 29px"}}>
            
            <div className="modal-body text-center pt-3 px-0">
            <div className="text-center">
                        
                          <h4 className="modal-title mb-3" style={{fontFamily:"Futura",fontStyle:"normal",lineHeight:"130%", fontWeight:"700",color:"#000000"}}>Lakukan e-KYC</h4>
                          <div className="desc text-center mb-0" style={{ fontFamily: "Helvetica", fontWeight: 400,fontSize:"16px",color:'#000000' }}>
                          Silakan lakukan e-KYC melalui Mobile App BTN Properti
                          </div>
                          {isTabletOrMobile ? (<Lottie options={defaultOptions} isStopped={false} isPaused={false} />) 
                          : (<Lottie options={defaultOptions} height={146} width={212} isStopped={false} isPaused={false} style={{marginBottom:"121px",marginTop:"125px"}} />)}
                          
                         
                          
                            <button
                              type="buttom"
                              className="btn btn-main form-control btn-back px-5"
                              style={{
                                width:"100%",
                                maxWidth: "540px",
                                height: "48px",
                                fontSize: "14px",
                                fontFamily: "Helvetica",
                                fontWeight: 700,
                                backgroundColor: "#0061A7",
                              }}
                              data-bs-dismiss="modal"
                            >
                              Oke
                            </button>
                            {/* <Link href="/">
                            </Link> */}
                         
                        </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VerifiedFailed;
