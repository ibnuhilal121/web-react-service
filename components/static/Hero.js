import React from "react";
import Link from "next/link";
import NumberFormat from "react-number-format";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

export default function Hero() {
  const router = useRouter();
  const [value, setValue] = useState("b");
  const [scrolled, setScrolled] = useState(false);
  const [search, setSearch] = useState({
    lokasi: "",
    tipe: "",
    budget: "",
  });

  const inputSearch = (type, value) => {
    let temp = { ...search };
    temp[type] = value;
    setSearch(temp);
  };

  const handleScroll = () => {
    const offset = window.scrollY;

    if (offset > 400) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  const submitSearch = (e) => {
    e.preventDefault();
    router.push(`/property?lokasi=${search.lokasi}&tipeProperti=${search.tipe}&budget=${search.budget.replace(/\./g, "")}`);
  };

  return (
    <div>
      <section id="hero">
        <div className="container sfri">
          <div className="row justify-content-center">
            <div className="col-md-10  text-center">
              <h4 className="title">
                Kemudahan memiliki tempat tinggal
                <br /> impian dimulai dari sini
              </h4>
            </div>
            <div className="col-md-10 col-lg-8 col-xl-7" id="homeSearchBar">
              <form id="form-hero" onSubmit={submitSearch}>
                <div className="row  align-items-center">
                  <div className="col">
                    <label htmlFor="hero_lokasi">LOKASI</label>
                    <input type="text" className="form-control" id="hero_lokasi" placeholder="Daerah, kota atau kodepos" value={search.lokasi} onChange={(e) => inputSearch("lokasi", e.target.value)} style={{fontFamily: "Helvetica"}} />
                  </div>
                  <div className="col-md-3 hero_select">
                    <label htmlFor="hero_tipe">TIPE</label>
                    <select className="form-select custom-select" id="hero_tipe" value={search.tipe} onChange={(e) => inputSearch("tipe", e.target.value)} aria-label="Floating label select example">
                      <option value="">Semua</option>
                      <option value="1">Rumah</option>
                      <option value="2">Apartemen</option>
                      <option value="6">Ruko</option>
                      <option value="7">Kantor</option>
                    </select>
                  </div>
                  <div className="col-md-3">
                    <label htmlFor="hero_budget">BUDGET</label>
                    <NumberFormat thousandSeparator="." decimalSeparator="," className="form-control" placeholder="Kisaran Harga" value={search.budget} onChange={(e) => inputSearch("budget", e.target.value)} style={{fontFamily: "Helvetica"}} />
                  </div>
                  <div className="col-md-1 button-wrapper">
                    <button type="submit" className="btn btn-main btn-circle">
                      <img src="/images/icons/Search.svg" alt="" />
                    </button>
                  </div>
                </div>
              </form>
              <Link href="/property">
                <a className="btn btn-main w-auto" id="hero_btn">
                  Cari properti
                </a>
              </Link>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
