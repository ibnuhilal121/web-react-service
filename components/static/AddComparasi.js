import Link from 'next/link'
import { useMediaQuery } from 'react-responsive'
import ReactImageFallback from "react-image-fallback";

export default function AddComparasi(props) {
    const isTabletOrMobile = useMediaQuery({ query: `(max-width: 576px)` });
    return (
        <div className={`${!isTabletOrMobile ? 'mx-2' : 'card card_komparasi'}`} {...props}>
            {props.data ?
                (
                    <ReactImageFallback
                        src={`${process.env.NEXT_PUBLIC_IMAGE_URL}/` + props.data.GBR1}
                        fallbackImage="/images/thumb-placeholder.png"
                        className="img-fluid img_thumb"
                        onClick={props.onClick}
                    />
                ) :
                (
                    <Link href={{
                        pathname: '/property/tipe',
                        query: { komparasi: '1' },
                    }}>
                        <div className={`card_add_comparasi ratio ${props.addWidget ? 'blank-addcomparasi' : 'blank-addcomparasi-home' }`}>
                            <div className="d-flex justify-content-center align-items-center">
                                <i
                                    className="bi bi-plus-circle-fill"
                                    style={isTabletOrMobile ? 
                                        { marginBottom: '-1px', color: '#AAAAAA', display: 'flex' } : { display: 'flex' }
                                    }
                                ></i>
                            </div>
                        </div>
                    </Link>
                )}
        </div>
    )
}
