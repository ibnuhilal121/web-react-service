import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useAppContext } from "../../context";
import CloseIcon from "../element/icons/CloseIcon";
import { useMediaQuery } from "react-responsive";
import { defaultHeaders } from "../../utils/defaultHeaders";

export default function NavProperty(props) {
  const { query, asPath } = useRouter();
  const { userKey } = useAppContext();
  const isTabletOrMobile = useMediaQuery({ query: `(max-width: 650px)` });
  const [isModalSaveOpen, setisModalSaveOpen] = useState(false);
  const [savedName, setSavedName] = useState("");
  function handleOpenModalSave() {
    setisModalSaveOpen(true);
    setSavedName("");
  }
  const queryUrl = asPath.split("/")
  function handleCloseModalSave() {
    setisModalSaveOpen(false);
    setSavedName("");
  }
  function handleChangeName(e) {
    setSavedName(e.target.value);
  }
  async function handleClickSimpan(e) {
    e.preventDefault();
    if (props.view !== "map" && savedName) {
      try {
        // setLoadingData(true);
        const endpoint = `${process.env.NEXT_PUBLIC_API_HOST}/member/pencarian/insert`;
        const res = await fetch(endpoint, {
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded",
            AccessKey_Member: userKey,
            ...defaultHeaders
          },
          body: new URLSearchParams({
            n: savedName,
            pncrian: JSON.stringify(query),
          }),
        });
        const resData = await res.json();
        if (!resData.IsError) {
          handleCloseModalSave();
        } else {
          throw {
            message: resData.ErrToUser,
          };
        }
      } catch (error) {
        console.log(error.message);
        console.log(error.message);
      }
    }
  }
  async function handleClickSekitar(e) {
    e.preventDefault();
    // console.log("klik cari sekitar");
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(callbackPosition);
    }
  }

  function callbackPosition(position) {
    // console.log('~ position', position)
    const {
      coords: { latitude, longitude },
    } = position;
    props.onFoundCoords({ lat: latitude, lng: longitude });
    axios
      .get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}&language=id&result_type=administrative_area_level_2`)
      .then((res) => {
        // console.log('~ res.data', res.data)
        const longName = res.data.results?.[0]?.address_components?.[0]?.long_name;
        if (longName) {
          props.onChangeLokasi({ target: { value: longName } });
        }
      })
      .catch((err) => {
        console.log("~ err", err);
      });
  }
  function handleChangeView(view) {
    props.setView(view);
  }
  function handleKeyPress(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
      props.handleClickSearch()
    }
  }
  return (
    <div className="mx-md-5 m-lg-0 m-0">
      <form>
        <div className="input-group search_property_top">
          <input
            type="text"
            className="form-control"
            placeholder="Masukan kata pencarian"
            value={props.lokasi}
            onChange={props.onChangeLokasi}
            onKeyPress={handleKeyPress}
          />
          <a className="simpan_hasil" type="button" data-bs-toggle="modal" data-bs-target={!userKey ? "#modalLogin" : null} onClick={!userKey ? null : props.view === "grid" ? handleOpenModalSave : handleClickSekitar}>
            {props.view === "map" ? (
              <>
                <span style={{ marginRight: 9 }}>
                  <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M11 7C8.79 7 7 8.79 7 11C7 13.21 8.79 15 11 15C13.21 15 15 13.21 15 11C15 8.79 13.21 7 11 7ZM19.94 10C19.48 5.83 16.17 2.52 12 2.06V0H10V2.06C5.83 2.52 2.52 5.83 2.06 10H0V12H2.06C2.52 16.17 5.83 19.48 10 19.94V22H12V19.94C16.17 19.48 19.48 16.17 19.94 12H22V10H19.94ZM11 18C7.13 18 4 14.87 4 11C4 7.13 7.13 4 11 4C14.87 4 18 7.13 18 11C18 14.87 14.87 18 11 18Z"
                      fill="#0061A7"
                    />
                  </svg>
                </span>
                Lokasi Dekat Saya
              </>
            ) : (
              "Simpan Hasil Pencarian"
            )}
          </a>
          <button className="cari" type="button" onClick={props.handleClickSearch}>
            <div className="search-icon">
              <svg width="17.49" height="17.49" viewBox="0 0 17.49 17.49" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M12.5 11H11.71L11.43 10.73C12.41 9.59 13 8.11 13 6.5C13 2.91 10.09 0 6.5 0C2.91 0 0 2.91 0 6.5C0 10.09 2.91 13 6.5 13C8.11 13 9.59 12.41 10.73 11.43L11 11.71V12.5L16 17.49L17.49 16L12.5 11ZM6.5 11C4.01 11 2 8.99 2 6.5C2 4.01 4.01 2 6.5 2C8.99 2 11 4.01 11 6.5C11 8.99 8.99 11 6.5 11Z"
                  fill="white"
                />
              </svg>
            </div>
          </button>
        </div>
      </form>
      <div className="align-items-center" id="search_favorite_mobile">
        <a className="simpan_hasil" type="button" data-bs-toggle="modal" data-bs-target={props.view === "grid" && !userKey ? "#modalLogin" : null} onClick={props.view === "grid" && !userKey ? null : handleOpenModalSave}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-star simpan_hasil mx-1" viewBox="0 0 16 16" style={{marginTop:"-5px"}}>
            <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
          </svg>
          Simpan Hasil Pencarian
        </a>
      </div>
      <div className="d-flex justify-content-between align-items-center tabs_property">
        <ul id="nav_property" className="nav nav-tabs mb-0 ps-sm-2" id="properti_nav">
          <li className="nav-item me-1 me-md-4">
            {props.type != "properti" ?
              <Link href={query.komparasi ? "/property/" : "/property/" + queryUrl[Object.keys(queryUrl).length-1]}>
                <a className={"nav-link px-0 me-2 navtab-properti"}>
                  <span id="proyek_perumahan">Proyek Perumahan</span>
                </a>
              </Link>
              :
              <span className={"nav-link active px-0 me-2 navtab-properti"} >
                <span id="proyek_perumahan">Proyek Perumahan</span>
              </span>
            }
          </li>
          <li className="nav-item">
            {props.type != "tipe" ?
              <Link href={"/property/tipe/"+queryUrl[Object.keys(queryUrl).length-1]}>
                <a className={"nav-link px-0 navtab-properti"}>
                  <span id="title_tipe">Tipe</span>
                </a>
              </Link>
              :
              <span className={"nav-link active px-0 navtab-properti"} >
                <span id="Tipe_perumahan">Tipe</span>
              </span>
            }

          </li>
        </ul>
        <div className="btn-group btn_group_view" role="group" aria-label="Basic outlined example">
          <a className={props.view == "grid" ? "btn active" : "btn"} onClick={() => handleChangeView("grid")}>
            <i className="bi bi-grid-fill me-2"></i>
            <span>Grid</span>
          </a>
          <a className={props.view == "map" ? "btn active" : "btn"} onClick={() => handleChangeView("map")}>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M20.5 3L20.34 3.03L15 5.1L9 3L3.36 4.9C3.15 4.97 3 5.15 3 5.38V20.5C3 20.78 3.22 21 3.5 21L3.66 20.97L9 18.9L15 21L20.64 19.1C20.85 19.03 21 18.85 21 18.62V3.5C21 3.22 20.78 3 20.5 3ZM15 19L9 16.89V5L15 7.11V19Z" />
            </svg>

            <span>Peta</span>
          </a>
        </div>
      </div>
      <Modal
        show={isModalSaveOpen}
        onHide={handleCloseModalSave}
        backdropClassName="saveBackdrop"
        dialogClassName="saveDialog"
        contentClassName="custom-modal"
      >
        <Modal.Header>
          <Modal.Title>Simpan Hasil Pencarian</Modal.Title>
          <div className="close">
            <button onClick={handleCloseModalSave}>
              <CloseIcon />
            </button>
          </div>
        </Modal.Header>
        <Modal.Body className="mx-auto" style={isTabletOrMobile ? { width: "90%", padding: "0" } : { width: "480px", padding: "0" }}>
          <h6>Masukkan judul untuk penyimpanan pencarian</h6>
          <Form.Control
            placeholder="Contoh : Rumah favoritku"
            className="d-flex flex-col justify-content-center"
            onChange={handleChangeName}
            value={savedName}
            style={{
              fontFamily: "Helvetica",
              fontWeight: "400",
              fontSize: "16px",
              color: " #00193e",
              height: "48px",
            }}
          />
          <div className="d-grid gap-2">
            <Button onClick={handleClickSimpan} style={{ height: "48px" }}>
              Simpan
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

NavProperty.defaultProps = {
  setView: () => { },
  onFoundCoords: () => { },
};
