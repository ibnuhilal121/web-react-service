import React from "react";

export default function ShareSosmed({judul, setCopied}) {
  const copyClipboard = () => {
    const el = document.createElement("input");
    if (typeof window !== "undefined"){
      el.value = window.location.href;
    }
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
  };

  return (
    <ul className="nav justify-content-center share_sosmed">
      <li className="nav-item">
        <a 
          className="nav-link" 
          href={typeof window !== "undefined" ? `https://facebook.com/share.php?u=${window.location.href}` : '#'}
          target="_blank"
        >
          <img
            width="20px"
            height="19.95px"
            src="/icons/icons/facebook.svg"
            alt="facebook"
          />
        </a>
      </li>
      <li className="nav-item">
        <a 
          className="nav-link" 
          href={typeof window !== "undefined" ? `https://twitter.com/intent/tweet?text=${judul}&url=${window.location.href}` : '#'}
          target="_blank"
        >
          <img
            width="16.39px"
            height="15px"
            src="/icons/icons/twitter.svg"
            alt="twitter"
          />
        </a>
      </li>
      <li className="nav-item">
        <a 
          className="nav-link" 
          href={typeof window !== "undefined" ? `https://www.instagram.com/?url=${window.location.href}` : '#'}
          target="_blank"
        >
          <img
            width="18px"
            height="18px"
            src="/icons/icons/instagram.svg"
            alt="instagram"
          />
        </a>
      </li>
      <li className="nav-item">
        <a 
          className="nav-link" 
          href={typeof window !== "undefined" ? `mailto:?subject=${judul}&body=${window.location.href}` : '#'}
          target="_blank"
        >
          <img
            width="16.2px"
            height="18px"
            src="/icons/icons/open-envelope.svg"
            alt="envelope"
          />
        </a>
      </li>
      <li className="nav-item">
        <div className="nav-link" style={{cursor: 'pointer'}} onClick={copyClipboard}>
          <img
            width="16px"
            height="16px"
            src="/icons/icons/chain.svg"
            alt="chain"
          />
        </div>
      </li>
    </ul>
  );
}
