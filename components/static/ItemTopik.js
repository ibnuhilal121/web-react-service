import Link from "next/link";

export default function ItemTopik(props) {
  const { Icon } = props;
  return (
    <div className="col-3 col-md-3 col_item_topik">
      <Link
        href={{ pathname: "/faq/archive/detail", query: { id: props.slug } }}
        passHref
      >
        <div className="item_topik_faq">
          <div className="flex-shrink-0 icon">
            <Icon />
          </div>
          <div className="flex-grow-1 title_topik_faq">{props.title}</div>
        </div>
      </Link>
    </div>
  );
}
