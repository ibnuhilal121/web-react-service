import Link from "next/link";
import PanduanSlide from "../carousel/PanduanSlide";

export default function Panduan() {
  return (
    <div>
      <section id="home_panduan">
        <div className="container">
          <div className="row d-flex align-items-center">
            <div className="col-md-2 col-lg-3">
              <h6 className="title_panduan">
                Panduan membeli <br />
                rumah impian
              </h6>
            </div>
            <div className="col-md-10 col-lg-9">
              <PanduanSlide />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
