import React, { useEffect, useState, useRef } from "react";
import Head from "next/head";
import Footer from "./Footer";
import Navbar from "./Navbar";
import Script from "next/script";
import Register from "./auth/Register";
import ResetPassword from "./auth/ResetPassword";
import LinkAktivasi from "./auth/LinkAktivasi";
import tawkTo from "tawkto-react";
import {
  useSession,
  signOut,
  getSession,
  getCsrfToken,
} from "next-auth/client";
import Cookies from "js-cookie";
import { useAppContext } from "../context";
import LoaderOverlay from "./LoaderOverlay";
import IdleTimer from "react-idle-timer";
import { useRouter } from "next/router";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../utils/FirebaseSetup";
import { defaultHeaders } from "../utils/defaultHeaders";
import parse from 'html-react-parser';
import dynamic from 'next/dynamic'  
import PhoneNumberModal from "./popup/PhoneNumber"

function renderAnalitycsHead(props) {
  return parse(props)
}

function renderAnalitycsBody(props){
  return parse(props)
}

export default function Layout(props) {
  const [user, setUser] = useState(null);
  const [session, loading] = useSession();
  const { setUserKey, setUserProfile, userProfile, userKey, setKprList } = useAppContext();
  const idleTimerRef = useRef(null);
  const router = useRouter();
  console.log({userProfile});

  /////import login component with ssr disable///////
  //////////////////////////////////////////////////
  const Login = dynamic(() => import('./auth/Login'), {
    ssr: false
  })
  //////////////////////////////////////////////////
  //////////////////////////////////////////////////

  const thisSession = async () => {
    const session = await getSession();
    return session;
  };

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setUser({
          provider: user.providerData[0].providerId,
          name: user.providerData[0].displayName,
          email: user.providerData[0].email,
          image: user.providerData[0].photoURL,
          id: user.providerData[0].uid,
          token: user.accessToken
        })
        // setLoggedIn(true)
      } else {
        setUser(null)
        // setLoggedIn(false)
      }
    })
  }, [])

  useEffect(() => {
    if (user) {
      if (user.provider == "google.com") {
        const payload = {
          GoogleID: user.id,
          Name: user.name,
          Email: user.email,
          Image: user.image,
        };

        fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login/google`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_App: localStorage.getItem("accessKey"),
            'Access-Control-Allow-Origin': '*',
            ...defaultHeaders
          },
          body: new URLSearchParams(payload),
        })
          .then(async (response) => {
            const data = await response.json();
            if (!response.ok) {
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
            }
            return data;
          })
          .then((data) => {
            const tempProfile = {
              n: data.Profile[0].n,
              gmbr: data.Profile[0].gmbr,
              id: data.Profile[0].id,
              agen: data.Profile[0].is_agn,
              idAgen: data.Profile[0].i_agn,
              namaAgensi: data.Profile[0].n_agn,
              email:data.Profile[0].e
            };
            sessionStorage.setItem("user", JSON.stringify(tempProfile));
            sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));

            Cookies.set("user", JSON.stringify(tempProfile));
            Cookies.set("keyMember", JSON.stringify(data.AccessKey));
            setUserProfile(data.Profile[0]);
            setUserKey(data.AccessKey);
            // window.location.replace("/member/profil");
          })
          .catch((error) => {
            console.log("error : ", error);
          });
      } else if (user.provider == "twitter.com") {
        const payload = {
          TwitterID: user.id,
          Name: user.name,
          Email: user.email,
          Image: user.image,
        };

        fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login/twitter`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_App: localStorage.getItem("accessKey"),
            'Access-Control-Allow-Origin': '*',
            ...defaultHeaders
          },
          body: new URLSearchParams(payload),
        })
          .then(async (response) => {
            const data = await response.json();
            if (!response.ok) {
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
            }
            return data;
          })
          .then((data) => {
            const tempProfile = {
              n: data.Profile[0].n,
              gmbr: data.Profile[0].gmbr,
              id: data.Profile[0].id,
              agen: data.Profile[0].is_agn,
              idAgen: data.Profile[0].i_agn,
              namaAgensi: data.Profile[0].n_agn,
              email:data.Profile[0].e
            };
            sessionStorage.setItem("user", JSON.stringify(tempProfile));
            sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
            Cookies.set("user", JSON.stringify(tempProfile));
            Cookies.set("keyMember", JSON.stringify(data.AccessKey));
            setUserProfile(data.Profile[0]);
            setUserKey(data.AccessKey);
            // window.location.replace("/member/profil");
          })
          .catch((error) => {
            console.log("error : ", error);
          });
      } else if (user.provider == "facebook.com") {
        const payload = {
          FacebookID: user.id,
          Name: user.name,
          Email: user.email,
          Image: user.image,
        };

        fetch(`${process.env.NEXT_PUBLIC_API_HOST}/member/login/facebook`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_App: localStorage.getItem("accessKey"),
            'Access-Control-Allow-Origin': '*',
            ...defaultHeaders
          },
          body: new URLSearchParams(payload),
        })
          .then(async (response) => {
            const data = await response.json();
            if (!response.ok) {
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
            }
            return data;
          })
          .then((data) => {
            const tempProfile = {
              n: data.Profile[0].n,
              gmbr: data.Profile[0].gmbr,
              id: data.Profile[0].id,
              agen: data.Profile[0].is_agn,
              idAgen: data.Profile[0].i_agn,
              namaAgensi: data.Profile[0].n_agn,
              email:data.Profile[0].e
            };
            sessionStorage.setItem("user", JSON.stringify(tempProfile));
            sessionStorage.setItem("keyMember", JSON.stringify(data.AccessKey));
            Cookies.set("user", JSON.stringify(tempProfile));
            Cookies.set("keyMember", JSON.stringify(data.AccessKey));
            setUserProfile(data.Profile[0]);
            setUserKey(data.AccessKey);
            // window.location.replace("/member/profil");
          })
          .catch((error) => {
            console.log("error : ", error);
          });
      }
    }
  }, [user]);

  const onIdle = () => {
    if (userKey && !user) {
      logout();
    }
  };

  const logout = async () => {
    try {
      const logoutAction = await fetch(
        `${process.env.NEXT_PUBLIC_API_HOST}/member/logout`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            AccessKey_Member: userKey,
            'Access-Control-Allow-Origin': '*',
            ...defaultHeaders
          },
        }
      );
      const logoutResponse = await logoutAction.json();
      if (logoutResponse.IsError) {
        throw logoutResponse.ErrToUser;
      }
    } catch (error) {
      // console.log()
    } finally {
      // if (router.pathname.includes('/ajukan_kpr')) {
      //   const KPRid = localStorage.getItem("kpr_id");
      //   if (KPRid) {
      //     sessionStorage.setItem("redirect_url", `/ajukan_kpr?id=${KPRid}`);
      //   } else {
      //     sessionStorage.setItem("redirect_url", router.pathname);
      //   };
      // };
      Cookies.remove("user");
      Cookies.remove("keyMember");
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("keyMember");
      localStorage.removeItem("user");
      localStorage.removeItem("keyMember");
      localStorage.removeItem("generate-url");
      localStorage.removeItem("generate-token");
      localStorage.removeItem("generate-token-profesiona-listing");
      localStorage.removeItem("body-homeservice-link");
      localStorage.removeItem("body-homeservice-token");
      localStorage.removeItem("body-profesionallisting-token");
      localStorage.removeItem("body-profesionallisting-link");
      localStorage.removeItem("body-preapproval");
      localStorage.removeItem("url-home-services");
      localStorage.removeItem("url-profesiona-listing");      
      localStorage.removeItem("title");
      localStorage.removeItem("navigate");
      setUserKey(null);
      setUserProfile(null);
      setKprList([])
      window.location.href = `${window.location.origin}?stat=relog`;
    }
  };

  return (
    
    <div>
      <Head>
        {/* {renderAnalitycsHead(headAnalytics)} */}
        {/* <script dangerouslySetInnerHTML={{__html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TSMQ3WV');`}} /> */}
        {/* <script
          async
          src={`https://www.googletagmanager.com/gtag/js?id=GTM-TSMQ3WV`}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'GTM-TSMQ3WV', {
            page_path: window.location.pathname,
          });
        `,
          }}
        /> */}
        <title>{props.title}</title>
        <link rel="icon" href="/images/favicon.png"></link>
        {/* <meta name="viewport" content="width=device-width, initial-scale=1" /> */}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1"
        />

        {/* ----------------------------------- CSP ---------------------------------- */}
        {/* <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" /> */}
        <meta
          http-equiv="Content-Security-Policy"
          content={`img-src blob: 'self' data: ${process.env.NEXT_PUBLIC_IMAGE_URL}/ https://www.facebook.com https://maps.googleapis.com/ https://maps.gstatic.com/ https://lh3.googleusercontent.com/ https://pbs.twimg.com/ https://graph.facebook.com/ https://platform-lookaside.fbsbx.com/ https://www.googletagmanager.com/ https://www.btnproperti.co.id/cdn1/files/ https://analytics.tiktok.com https://*.google-analytics.com/ https://*.googleadservices.com https://*.google.com.sg https://*.google.com https://googleads.g.doubleclick.net`}
        />
        <meta
          http-equiv="Content-Security-Policy"
          content="style-src 'self' https://fonts.googleapis.com/ https://cdn.jsdelivr.net https://cdnjs.cloudflare.com 'unsafe-inline' https://*.google.com.sg https://*.google.com"
        />
        <meta
          http-equiv="Content-Security-Policy"
          content="font-src 'self' https://fonts.googleapis.com/ https://cdn.jsdelivr.net https://cdnjs.cloudflare.com https://fonts.gstatic.com/ https://*.google.com.sg https://*.google.com"
        />
        <meta
          http-equiv="Content-Security-Policy"
          content="script-src 'self' https://cdn.jsdelivr.net https://www.gstatic.com 'unsafe-eval' https://connect.facebook.net https://www.facebook.com https://maps.googleapis.com https://www.google.com/recaptcha/ https://apis.google.com/ 'unsafe-inline' https://*.googletagmanager.com https://*.google-analytics.com/ https://analytics.tiktok.com https://*.googleadservices.com https://*.google.com.sg https://googleads.g.doubleclick.net"
        />
        {/* <meta http-equiv="Content-Security-Policy" content="default-src 'self'" /> */}
        {/* ----------------------------------- END CSP ---------------------------------- */}

        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css"
        ></link>
        {/* <!-- REACT SLICK SLIDER --> */}
        <link
          rel="stylesheet"
          type="text/css"
          charset="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
              </Head>
      {/* {renderAnalitycsBody(bodyAnalytics)} */}
      {/* <noscript dangerouslySetInnerHTML={{__html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSMQ3WV" height="0" width="0" style="display:none;visibility:hidden"></iframe>`}} /> */}

      <Navbar />
      {props.children}
      <IdleTimer ref={idleTimerRef} timeout={900000} onIdle={onIdle} />
      <Footer mb_tipe_properti={props?.mb_tipe_properti} />
      <Login />
      <Register />
      <ResetPassword />
      <LinkAktivasi />
      <LoaderOverlay open={props.isLoaderOpen} />
      <PhoneNumberModal />
      
      
      <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" />
      
      <Script src="/js/main.js" />
    </div>
  );
}
