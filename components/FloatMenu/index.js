import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { Collapse } from 'react-bootstrap';

export default function FloatMenu({ setup }) {
  const [isMenuOpen, setOpenMenu] = useState(false);

  return (
    <div 
      style={{ right: 0, position: 'fixed' }} 
      className="d-flex justify-content-end align-items-start"
    >
      <Collapse in={isMenuOpen}>
        <div
          id="example-collapse-text"
          className={`col-md-4 col-lg-3`}
          style={{
            padding: "6px",
            padding: "0px",
            marginRight: 0,
            marginBottom: "30px",
            maxHeight: "450px",
            overflowY: "auto"
          }}
        >
          <div
            className="list-group"
            style={{
              backgroundColor: "#FAFAFA",
              boxShadow: "0px 4px 16px rgba(0, 0, 0, 0.1)",
              borderRadius: "8px",
              padding: "16px",
              color: "#00193E",
            }}
          >
            <a
              className="list-group-item-action"
              href={setup.title.href}
              style={{
                marginBottom: "16px",
                marginRight: "26px",
                fontSize: "16px",
                fontFamily: "FuturaBT ",
                fontWeight: 700,
                color: "#00193E",
              }}
              onClick={() => setOpenMenu(false)}
            >
              {setup.title.text}
            </a>
            {setup.lists.map((list, index) => {
              return (
                <a
                  key={index}
                  className="list-group-item-action"
                  href={list.href}
                  style={{
                    marginBottom: "8px",
                    fontFamily: "Helvetica",
                    fontWeight: 700,
                    fontSize: "14px",
                    paddingRight: "8px",
                    paddingLeft: "8px",
                    color: "#00193E",
                  }}
                  onClick={() => setOpenMenu(false)}
                >
                  {list.text}
                </a>
              )
            })}
          </div>
        </div>
      </Collapse>
      <Button 
        variant="light" 
        className="d-flex justify-content-center align-items-center shadow" 
        style={{ width: '40px', height: '40px' }}
        onClick={() => setOpenMenu(!isMenuOpen)}
        aria-controls="example-collapse-text"
        aria-expanded={isMenuOpen}
      >
        <i 
          className={`bi ${isMenuOpen ? 'bi-x-lg' : 'bi-list'}`} 
          id="menu-privacy" 
          style={{marginTop: '6px'}}
        ></i>
      </Button>
    </div>
  )
}